﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using Ds_Bot_Reunited_NEWUI.Annotations;
using Ds_Bot_Reunited_NEWUI.Constants;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.Spieldaten;

namespace Ds_Bot_Reunited_NEWUI.Weltensettings {
    public class WeltensettingsViewModel : INotifyPropertyChanged, IDataErrorInfo {
        private TimeSpan _laufzeitt;
        private bool _progressRingActive;
        private int _progressRingWidth;


        public WeltensettingsViewModel(Window window) {
            Welten = new AsyncObservableCollection<string>();
            Serverlist = new AsyncObservableCollection<string>();
            Serverlist.Add(SERVERCONSTANTS.Diestaemme.GetDescription());
            Serverlist.Add(SERVERCONSTANTS.Tribalwarsnet.GetDescription());
            Serverlist.Add(SERVERCONSTANTS.Tribalwarscouk.GetDescription());
            Serverlist.Add(SERVERCONSTANTS.Staemmech.GetDescription());
            Serverlist.Add(SERVERCONSTANTS.tribalwarsus.GetDescription());
            LadeWelten();
            Window = window;
        }

        public AsyncObservableCollection<string> Serverlist { get; set; }

        public AsyncObservableCollection<string> Welten { get; set; }

        public Window Window { get; set; }


        public string Server {
            get => App.Settings.Server.GetDescription();
            set {
                if (SERVERCONSTANTS.Diestaemme.GetDescription().Contains(value))
                    App.Settings.Server = SERVERCONSTANTS.Diestaemme;
                else if (SERVERCONSTANTS.Tribalwarsnet.GetDescription().Contains(value))
                    App.Settings.Server = SERVERCONSTANTS.Tribalwarsnet;
                else if (SERVERCONSTANTS.Tribalwarscouk.GetDescription().Contains(value))
                    App.Settings.Server = SERVERCONSTANTS.Tribalwarscouk;
                else if (SERVERCONSTANTS.Staemmech.GetDescription().Contains(value))
                    App.Settings.Server = SERVERCONSTANTS.Staemmech;
                else if (SERVERCONSTANTS.tribalwarsus.GetDescription().Contains(value))
                    App.Settings.Server = SERVERCONSTANTS.tribalwarsus;

                LadeWelten();
                OnPropertyChanged();

            }
        }

        public string Welt {
            get => App.Settings.Welt;
            set {
                var clear = !string.IsNullOrEmpty(App.Settings.Welt);
                App.Settings.Welt = value;
                LoadWorlddata(clear);
                OnPropertyChanged();
            }
        }

        public int ProgressRingWidth {
            get => _progressRingWidth;
            set {
                _progressRingWidth = value;
                OnPropertyChanged();
            }
        }

        public int NachtbonusVon {
            get => App.Settings.Weltensettings.NachtbonusVon;
            set {
                App.Settings.Weltensettings.NachtbonusVon = value;
                OnPropertyChanged();
            }
        }

        public int Angriffsschutztage {
            get => App.Settings.Weltensettings.Angriffsschutztage;
            set {
                App.Settings.Weltensettings.Angriffsschutztage = value;
                OnPropertyChanged();
            }
        }

        public int NachtbonusBis {
            get => App.Settings.Weltensettings.NachtbonusBis;
            set {
                App.Settings.Weltensettings.NachtbonusBis = value;
                OnPropertyChanged();
            }
        }

        public bool ProgressRingActive {
            get => _progressRingActive;
            set {
                _progressRingActive = value;
                if (value)
                    ProgressRingWidth = 50;
                else
                    ProgressRingWidth = 0;
                OnPropertyChanged("");
            }
        }

        public bool OKButtonClickable => !ProgressRingActive;

        public TimeSpan Laufzeitt {
            get => _laufzeitt;
            set {
                _laufzeitt = value;
                OnPropertyChanged();
            }
        }

        public double Spielgeschwindigkeit {
            get {
                if (App.Settings.Weltensettings.Spielgeschwindigkeit.Equals(0))
                    App.Settings.Weltensettings.Spielgeschwindigkeit = 1;
                return App.Settings.Weltensettings.Spielgeschwindigkeit;
            }
            set {
                App.Settings.Weltensettings.Spielgeschwindigkeit = value;
                OnPropertyChanged();
            }
        }

        public double RessourcenProductionFaktor {
            get {
                if (App.Settings.Weltensettings.RessourcenProductionFaktor.Equals(0))
                    App.Settings.Weltensettings.RessourcenProductionFaktor = 1;
                return App.Settings.Weltensettings.RessourcenProductionFaktor;
            }
            set {
                App.Settings.Weltensettings.RessourcenProductionFaktor = value;
                OnPropertyChanged();
            }
        }

        public bool MillisecondsActive {
            get => App.Settings.Weltensettings.MillisecondsActive;
            set {
                App.Settings.Weltensettings.MillisecondsActive = value;
                OnPropertyChanged();
            }
        }

        public bool BogenschuetzenActive {
            get => App.Settings.Weltensettings.BogenschuetzenActive;
            set {
                App.Settings.Weltensettings.BogenschuetzenActive = value;
                OnPropertyChanged();
            }
        }

        public bool PaladinActive {
            get => App.Settings.Weltensettings.PaladinActive;
            set {
                App.Settings.Weltensettings.PaladinActive = value;
                OnPropertyChanged();
            }
        }

        public bool VerkuerzenActive {
            get => App.Settings.Weltensettings.VerkuerzenActive;
            set {
                App.Settings.Weltensettings.VerkuerzenActive = value;
                OnPropertyChanged();
            }
        }

        public bool MilizActive {
            get => App.Settings.Weltensettings.MilizActive;
            set {
                App.Settings.Weltensettings.MilizActive = value;
                OnPropertyChanged();
            }
        }

        public bool FakeActive {
            get => App.Settings.Weltensettings.FakeActive;
            set {
                App.Settings.Weltensettings.FakeActive = value;
                OnPropertyChanged();
            }
        }

        public double SpeertraegerLaufzeit {
            get {
                if (App.Settings.Weltensettings.SpeertraegerLaufzeit == 0)
                    App.Settings.Weltensettings.SpeertraegerLaufzeit = 18;
                return App.Settings.Weltensettings.SpeertraegerLaufzeit;
            }
            set {
                App.Settings.Weltensettings.SpeertraegerLaufzeit = value;
                OnPropertyChanged();
            }
        }

        public double SchwertkaempferLaufzeit {
            get {
                if (App.Settings.Weltensettings.SchwertkaempferLaufzeit == 0)
                    App.Settings.Weltensettings.SchwertkaempferLaufzeit = 22;
                return App.Settings.Weltensettings.SchwertkaempferLaufzeit;
            }
            set {
                App.Settings.Weltensettings.SchwertkaempferLaufzeit = value;
                OnPropertyChanged();
            }
        }

        public double AxtkaempferLaufzeit {
            get {
                if (App.Settings.Weltensettings.AxtkaempferLaufzeit == 0)
                    App.Settings.Weltensettings.AxtkaempferLaufzeit = 18;
                return App.Settings.Weltensettings.AxtkaempferLaufzeit;
            }
            set {
                App.Settings.Weltensettings.AxtkaempferLaufzeit = value;
                OnPropertyChanged();
            }
        }

        public double BogenschuetzenLaufzeit {
            get {
                if (App.Settings.Weltensettings.BogenschuetzenLaufzeit == 0)
                    App.Settings.Weltensettings.BogenschuetzenLaufzeit = 18;
                return App.Settings.Weltensettings.BogenschuetzenLaufzeit;
            }
            set {
                App.Settings.Weltensettings.BogenschuetzenLaufzeit = value;
                OnPropertyChanged();
            }
        }

        public double SpaeherLaufzeit {
            get {
                if (App.Settings.Weltensettings.SpaeherLaufzeit == 0)
                    App.Settings.Weltensettings.SpaeherLaufzeit = 9;
                return App.Settings.Weltensettings.SpaeherLaufzeit;
            }
            set {
                App.Settings.Weltensettings.SpaeherLaufzeit = value;
                OnPropertyChanged();
            }
        }

        public double LeichteKavallerieLaufzeit {
            get {
                if (App.Settings.Weltensettings.LeichteKavallerieLaufzeit == 0)
                    App.Settings.Weltensettings.LeichteKavallerieLaufzeit = 10;
                return App.Settings.Weltensettings.LeichteKavallerieLaufzeit;
            }
            set {
                App.Settings.Weltensettings.LeichteKavallerieLaufzeit = value;
                OnPropertyChanged();
            }
        }

        public double BeritteneBogenschuetzenLaufzeit {
            get {
                if (App.Settings.Weltensettings.BeritteneBogenschuetzenLaufzeit == 0)
                    App.Settings.Weltensettings.BeritteneBogenschuetzenLaufzeit = 10;
                return App.Settings.Weltensettings.BeritteneBogenschuetzenLaufzeit;
            }
            set {
                App.Settings.Weltensettings.BeritteneBogenschuetzenLaufzeit = value;
                OnPropertyChanged();
            }
        }

        public double SchwereKavallerieLaufzeit {
            get {
                if (App.Settings.Weltensettings.SchwereKavallerieLaufzeit == 0)
                    App.Settings.Weltensettings.SchwereKavallerieLaufzeit = 11;
                return App.Settings.Weltensettings.SchwereKavallerieLaufzeit;
            }
            set {
                App.Settings.Weltensettings.SchwereKavallerieLaufzeit = value;
                OnPropertyChanged();
            }
        }

        public double RammboeckeLaufzeit {
            get {
                if (App.Settings.Weltensettings.RammboeckeLaufzeit == 0)
                    App.Settings.Weltensettings.RammboeckeLaufzeit = 30;
                return App.Settings.Weltensettings.RammboeckeLaufzeit;
            }
            set {
                App.Settings.Weltensettings.RammboeckeLaufzeit = value;
                OnPropertyChanged();
            }
        }

        public double KatapulteLaufzeit {
            get {
                if (App.Settings.Weltensettings.KatapulteLaufzeit == 0)
                    App.Settings.Weltensettings.KatapulteLaufzeit = 30;
                return App.Settings.Weltensettings.KatapulteLaufzeit;
            }
            set {
                App.Settings.Weltensettings.KatapulteLaufzeit = value;
                OnPropertyChanged();
            }
        }

        public double PaladinLaufzeit {
            get {
                if (App.Settings.Weltensettings.PaladinLaufzeit == 0)
                    App.Settings.Weltensettings.PaladinLaufzeit = 10;
                return App.Settings.Weltensettings.PaladinLaufzeit;
            }
            set {
                App.Settings.Weltensettings.PaladinLaufzeit = value;
                OnPropertyChanged();
            }
        }

        public double AdelsgeschlechtLaufzeit {
            get {
                if (App.Settings.Weltensettings.AdelsgeschlechtLaufzeit == 0)
                    App.Settings.Weltensettings.AdelsgeschlechtLaufzeit = 35;
                return App.Settings.Weltensettings.AdelsgeschlechtLaufzeit;
            }
            set {
                App.Settings.Weltensettings.AdelsgeschlechtLaufzeit = value;
                OnPropertyChanged();
            }
        }

        public bool KircheActive {
            get => App.Settings.Weltensettings.KircheActive;
            set {
                App.Settings.Weltensettings.KircheActive = value;
                OnPropertyChanged();
            }
        }

        public bool AhTillLevel3 {
            get => App.Settings.Weltensettings.AhTillLevel3;
            set {
                App.Settings.Weltensettings.AhTillLevel3 = value;
                OnPropertyChanged();
            }
        }

        public bool WatchtowerActive {
            get => App.Settings.Weltensettings.WatchtowerActive;
            set {
                App.Settings.Weltensettings.WatchtowerActive = value;
                OnPropertyChanged();
            }
        }

        public bool TenTechLevel {
            get => App.Settings.Weltensettings.TenTechLevel;
            set {
                App.Settings.Weltensettings.TenTechLevel = value;
                OnPropertyChanged();
            }
        }

        public string this[string columnName] {
            get {
                string result = null;
                if (columnName == "Welt")
                    if (string.IsNullOrEmpty(Welt))
                        result = "Please select a World";
                return result;
            }
        }

        public string Error { get; }


        public event PropertyChangedEventHandler PropertyChanged;

        private async Task LoadWorldsettingsAsync() {
            await DataHandler.GetWorldSettings(App.Settings.Weltensettings);
        }

        private async void LoadWorlddata(bool xd) {
            await Task.Run(async () => {
                               if (xd)
                                   ClearVillages();
                               ProgressRingActive = true;
                               await Worlddata.LoadWorldData();
                               await LoadWorldsettingsAsync();
                               ProgressRingActive = false;
                           });
        }

        private void ClearVillages() {
            foreach (var account in App.Settings.Accountsettingslist)
                account.DoerferSettings.Clear();
            App.VillagelistWholeWorld.Clear();
            App.PlayerlistWholeWorld.Clear();
        }


        private void LadeWelten() {
            Welten.Clear();

            string temp = "";
            if (SERVERCONSTANTS.Diestaemme.GetDescription().Contains(Server)) {
                temp = SERVERCONSTANTS.Diestaemme.GetDescription();
            } else if (SERVERCONSTANTS.Tribalwarsnet.GetDescription().Contains(Server)) {
                temp = SERVERCONSTANTS.Tribalwarsnet.GetDescription();
            } else if (SERVERCONSTANTS.Tribalwarscouk.GetDescription().Contains(Server)) {
                temp = SERVERCONSTANTS.Tribalwarscouk.GetDescription();
            } else if (SERVERCONSTANTS.Staemmech.GetDescription().Contains(Server)) {
                temp = SERVERCONSTANTS.Staemmech.GetDescription();
            }
            else if (SERVERCONSTANTS.tribalwarsus.GetDescription().Contains(Server))
            {
                temp = SERVERCONSTANTS.tribalwarsus.GetDescription();
            }

            WebClient client = new WebClient();
            string downloadString = client.DownloadString("http://www." + temp + "/backend/get_servers.php");
            string[] splitted = downloadString.Split('{');
            string[] weltenGesamt = splitted[1].Split('"');
            List<string> welten = new List<string>();
            for (int i = 1; i < weltenGesamt.Length; i = i + 4) {
                welten.Add(weltenGesamt[i]);
            }
            welten = welten.OrderBy(x => x).ToList();
            Welten.AddRange(welten);
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
