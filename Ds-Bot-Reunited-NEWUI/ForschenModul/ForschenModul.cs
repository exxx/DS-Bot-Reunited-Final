﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Ds_Bot_Reunited_NEWUI.Accountsettings.Ds_Bot_Reunited.Accountsettings;
using Ds_Bot_Reunited_NEWUI.Botcaptcha;
using Ds_Bot_Reunited_NEWUI.BrowserManager;
using Ds_Bot_Reunited_NEWUI.Constants;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.Properties;
using Ds_Bot_Reunited_NEWUI.Rekrutieren;
using Ds_Bot_Reunited_NEWUI.Settings;
using OpenQA.Selenium;

namespace Ds_Bot_Reunited_NEWUI.ForschenModul {
    public static class ForschenModul {
        public static async Task Start(Browser browser, Accountsetting accountsettings) {
            App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + Resources.Forschen + " " + Resources.Started);
            if (accountsettings.ForschenActive) {
                foreach (var dorf in accountsettings.DoerferSettings.Where(x => !string.IsNullOrEmpty(x.Dorf.Dorf.VillageName) && x.Dorf.PlayerId.Equals(accountsettings.Playerid))) {
                    await ForgeVillage(browser, accountsettings, dorf);
                }
                App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + Resources.Forschen + " " + Resources.Ended);
                accountsettings.LastBauenZeitpunkt = DateTime.Now;
            }
        }

        public static async Task ForgeVillage(Browser browser, Accountsetting accountsettings, AccountDorfSettings dorf) {
            if (dorf.ForschenActive && dorf.Dorfinformationen.LastForschenZeitpunkt < DateTime.Now.AddMinutes(-dorf.Forscheninterval)) {
                foreach (var forschenelement in dorf.Forschenschleife.OrderBy(x=>x.Priority)) {
                    Forschen forschen = Forschen.Schwertkaempfer;
                    switch (forschenelement.Ressource) {
                        case Truppeb.Speertraeger:
                            forschen = Forschen.Speertraeger;
                            break;
                        case Truppeb.Schwertkaempfer:
                            forschen = Forschen.Schwertkaempfer;
                            break;
                        case Truppeb.Axtkaempfer:
                            forschen = Forschen.Axtkaempfer;
                            break;
                        case Truppeb.Bogenschuetzen:
                            forschen = Forschen.Bogenschuetzen;
                            break;
                        case Truppeb.Spaeher:
                            forschen = Forschen.Spaeher;
                            break;
                        case Truppeb.LeichteKavallerie:
                            forschen = Forschen.LeichteKavallerie;
                            break;
                        case Truppeb.BeritteneBogenschuetzen:
                            forschen = Forschen.BeritteneBogenschuetzen;
                            break;
                        case Truppeb.SchwereKavallerie:
                            forschen = Forschen.SchwereKavallerie;
                            break;
                        case Truppeb.Rammboecke:
                            forschen = Forschen.Rammboecke;
                            break;
                        case Truppeb.Katapulte:
                            forschen = Forschen.Katapulte;
                            break;
                    }
                    await Forge(browser, dorf, forschen, accountsettings, App.Settings.Weltensettings.TenTechLevel ? 10 : 1);
                }
                dorf.Dorfinformationen.LastForschenZeitpunkt = DateTime.Now;
            }
        }


        public static async Task Forge(Browser browser, AccountDorfSettings dorf, Forschen art, Accountsetting accountsettings, int gewollteStufe = 0) {
            try {
                if (!App.Settings.Weltensettings.BogenschuetzenActive && art == Forschen.BeritteneBogenschuetzen || !App.Settings.Weltensettings.BogenschuetzenActive && art == Forschen.Bogenschuetzen)
                    return;
                if (dorf.BalanceForschenActive || Debugger.IsAttached) {
                    await ForgeEinmal(browser, dorf, art, accountsettings, gewollteStufe);
                } else {
                    bool weiter;
                    do {
                        weiter = await ForgeEinmal(browser, dorf, art, accountsettings, gewollteStufe);
                    } while (weiter);
                }

                
            } catch {
                // ignored
            }
        }

        private static void UpdateTruppenLevel(Browser browser, AccountDorfSettings dorf, Forschen art, Accountsetting accountsettings) {
            //tech_list
            string link = App.Settings.OperateLink + "village=" + dorf.Dorf.VillageId + "" + accountsettings.UvZusatzFuerLink + "&screen=smith";
            if (!browser.WebDriver.Url.Equals(link)) {
                browser.WebDriver.Navigate().GoToUrl(link);
                browser.WebDriver.WaitForPageload();
            }

            var infos = browser.WebDriver.FindElements(By.XPath("//a[@class='unit_link']")).ToList();
            foreach (var text in infos.Select(x => x.Text)) {
                string stufenString = "";
                int index = text.LastIndexOf('(');
                int index2 = text.LastIndexOf(')');
                stufenString = text.Substring(index + 1, index2 - index-1);
                int.TryParse(stufenString, out int stufe);
                if (text.Contains(NAMECONSTANTS.GetNameForServer(Names.Speertraeger))) {
                    dorf.Dorfinformationen.SpeertraegerStufe = stufe;
                }else if (text.Contains(NAMECONSTANTS.GetNameForServer(Names.Schwertkaempfer))) {
                    dorf.Dorfinformationen.SchwertkaempferStufe = stufe;
                    if (stufe > 0) {
                        dorf.Dorfinformationen.SchwertkaempferErforscht = true;
                    }
                } else if (text.Contains(NAMECONSTANTS.GetNameForServer(Names.Axtkaempfer))) {
                    dorf.Dorfinformationen.AxtkaempferStufe = stufe;
                    if (stufe > 0) {
                        dorf.Dorfinformationen.AxtkaempferErforscht = true;
                    }
                } else if (text.Contains(NAMECONSTANTS.GetNameForServer(Names.Bogenschuetzen))) {
                    dorf.Dorfinformationen.BogenschuetzenStufe = stufe;
                    if (stufe > 0) {
                        dorf.Dorfinformationen.BogenschuetzenErforscht = true;
                    }
                } else if (text.Contains(NAMECONSTANTS.GetNameForServer(Names.Spaeher))) {
                    dorf.Dorfinformationen.SpaeherStufe = stufe;
                    if (stufe > 0) {
                        dorf.Dorfinformationen.SpaeherErforscht = true;
                    }
                } else if (text.Contains(NAMECONSTANTS.GetNameForServer(Names.LeichteKavallerie))) {
                    dorf.Dorfinformationen.LeichteKavallerieStufe = stufe;
                    if (stufe > 0) {
                        dorf.Dorfinformationen.LeichteKavallerieErforscht = true;
                    }
                } else if (text.Contains(NAMECONSTANTS.GetNameForServer(Names.BeritteneBogenschuetzen))) {
                    dorf.Dorfinformationen.BeritteneBogenschuetzenStufe = stufe;
                    if (stufe > 0) {
                        dorf.Dorfinformationen.BeritteneBogenschuetzenErforscht = true;
                    }
                } else if (text.Contains(NAMECONSTANTS.GetNameForServer(Names.SchwereKavallerie))) {
                    dorf.Dorfinformationen.SchwereKavallerieStufe = stufe;
                    if (stufe > 0) {
                        dorf.Dorfinformationen.SchwereKavallerieErforscht = true;
                    }
                } else if (text.Contains(NAMECONSTANTS.GetNameForServer(Names.Rammboecke))) {
                    dorf.Dorfinformationen.RammboeckeStufe = stufe;
                    if (stufe > 0) {
                        dorf.Dorfinformationen.RammboeckeErforscht = true;
                    }
                } else if (text.Contains(NAMECONSTANTS.GetNameForServer(Names.Katapulte))) {
                    dorf.Dorfinformationen.KatapulteStufe = stufe;
                    if (stufe > 0) {
                        dorf.Dorfinformationen.KatapulteErforscht = true;
                    }
                }
            }
        }

        private static async Task<bool> ForgeEinmal(Browser browser, AccountDorfSettings dorf, Forschen art, Accountsetting accountsettings, int gewollteStufe) {
            string link = App.Settings.OperateLink + "village=" + dorf.Dorf.VillageId + "" + accountsettings.UvZusatzFuerLink + "&screen=smith";
            if (dorf.Dorfinformationen.SpeertraegerStufe == 0) {
                UpdateTruppenLevel(browser, dorf, art, accountsettings);
            }
            switch (art) {
                case Forschen.Speertraeger:
                    if (!dorf.Dorfinformationen.SchwertkaempferErforscht || (App.Settings.Weltensettings.TenTechLevel && dorf.Dorfinformationen.SchwertkaempferStufe != gewollteStufe)) {
                        if (!browser.WebDriver.Url.Equals(link)) {
                            browser.WebDriver.Navigate().GoToUrl(link);
                            browser.WebDriver.WaitForPageload();
                        }
                        await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings);
                        if (browser.WebDriver.ElementExists(By.XPath(SpielOberflaeche.Schmiede.SCHWERTER_ERFORSCHEN_SELECTOR_XPATH))) {
                            if (browser.ClickByXpath(SpielOberflaeche.Schmiede.SCHWERTER_ERFORSCHEN_SELECTOR_XPATH)) {
                                dorf.Dorfinformationen.SchwertkaempferErforscht = true;
                                dorf.Dorfinformationen.SchwertkaempferStufe += 1;
                            }
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                    break;
                case Forschen.Schwertkaempfer:
                    if (!dorf.Dorfinformationen.SchwertkaempferErforscht || (App.Settings.Weltensettings.TenTechLevel && dorf.Dorfinformationen.SchwertkaempferStufe != gewollteStufe)) {
                        if (!browser.WebDriver.Url.Equals(link)) {
                            browser.WebDriver.Navigate().GoToUrl(link);
                            browser.WebDriver.WaitForPageload();
                        }
                        await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings);
                        if (browser.WebDriver.ElementExists(By.XPath(SpielOberflaeche.Schmiede.SCHWERTER_ERFORSCHEN_SELECTOR_XPATH))) {
                            if (browser.ClickByXpath(SpielOberflaeche.Schmiede.SCHWERTER_ERFORSCHEN_SELECTOR_XPATH)) {
                                dorf.Dorfinformationen.SchwertkaempferErforscht = true;
                                dorf.Dorfinformationen.SchwertkaempferStufe += 1;
                            }
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                    break;
                case Forschen.Axtkaempfer:
                    if (!dorf.Dorfinformationen.AxtkaempferErforscht || (App.Settings.Weltensettings.TenTechLevel && dorf.Dorfinformationen.AxtkaempferStufe != gewollteStufe)) {
                        if (!browser.WebDriver.Url.Equals(link)) {
                            browser.WebDriver.Navigate().GoToUrl(link);
                            browser.WebDriver.WaitForPageload();
                        }
                        await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings);
                        if (browser.WebDriver.ElementExists(By.XPath(SpielOberflaeche.Schmiede.AXTKAEMPFER_ERFORSCHEN_SELECTOR_XPATH))) {
                            if (browser.ClickByXpath(SpielOberflaeche.Schmiede.AXTKAEMPFER_ERFORSCHEN_SELECTOR_XPATH)) {
                                dorf.Dorfinformationen.AxtkaempferErforscht = true;
                                dorf.Dorfinformationen.AxtkaempferStufe += 1;
                            }
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                    break;
                case Forschen.Bogenschuetzen:
                    if (!dorf.Dorfinformationen.BogenschuetzenErforscht || (App.Settings.Weltensettings.TenTechLevel && dorf.Dorfinformationen.BogenschuetzenStufe != gewollteStufe)) {
                        if (!browser.WebDriver.Url.Equals(link)) {
                            browser.WebDriver.Navigate().GoToUrl(link);
                            browser.WebDriver.WaitForPageload();
                        }
                        await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings);
                        if (browser.WebDriver.ElementExists(By.XPath(SpielOberflaeche.Schmiede.BOGENSCHUETZEN_ERFORSCHEN_SELECTOR_XPATH))) {
                            if (browser.ClickByXpath(SpielOberflaeche.Schmiede.BOGENSCHUETZEN_ERFORSCHEN_SELECTOR_XPATH)) {
                                dorf.Dorfinformationen.BogenschuetzenErforscht = true;
                                dorf.Dorfinformationen.BogenschuetzenStufe += 1;
                            }
                        } else {
                            return false;
                        }
                    }
                    break;
                case Forschen.Spaeher:
                    if (!dorf.Dorfinformationen.SpaeherErforscht || (App.Settings.Weltensettings.TenTechLevel && dorf.Dorfinformationen.SpaeherStufe != 1)) {
                        if (!browser.WebDriver.Url.Equals(link)) {
                            browser.WebDriver.Navigate().GoToUrl(link);
                            browser.WebDriver.WaitForPageload();
                        }
                        await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings);
                        if (browser.WebDriver.ElementExists(By.XPath(SpielOberflaeche.Schmiede.SPAEHER_ERFORSCHEN_SELECTOR_XPATH))) {
                            if (browser.ClickByXpath(SpielOberflaeche.Schmiede.SPAEHER_ERFORSCHEN_SELECTOR_XPATH)) {
                                dorf.Dorfinformationen.SpaeherErforscht = true;
                                dorf.Dorfinformationen.SpaeherStufe += 1;
                            }
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                    break;
                case Forschen.LeichteKavallerie:
                    if (!dorf.Dorfinformationen.LeichteKavallerieErforscht || (App.Settings.Weltensettings.TenTechLevel && dorf.Dorfinformationen.LeichteKavallerieStufe != gewollteStufe)) {
                        if (!browser.WebDriver.Url.Equals(link)) {
                            browser.WebDriver.Navigate().GoToUrl(link);
                            browser.WebDriver.WaitForPageload();
                        }
                        await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings);
                        if (browser.WebDriver.ElementExists(By.XPath(SpielOberflaeche.Schmiede.LEICHTEKAVALLERIE_ERFORSCHEN_SELECTOR_XPATH))) {
                            if (browser.ClickByXpath(SpielOberflaeche.Schmiede.LEICHTEKAVALLERIE_ERFORSCHEN_SELECTOR_XPATH)) {
                                dorf.Dorfinformationen.LeichteKavallerieErforscht = true;
                                dorf.Dorfinformationen.LeichteKavallerieStufe += 1;
                            }
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                    break;
                case Forschen.BeritteneBogenschuetzen:
                    if (!dorf.Dorfinformationen.BeritteneBogenschuetzenErforscht || (App.Settings.Weltensettings.TenTechLevel && dorf.Dorfinformationen.BeritteneBogenschuetzenStufe != gewollteStufe)) {
                        if (!browser.WebDriver.Url.Equals(link)) {
                            browser.WebDriver.Navigate().GoToUrl(link);
                            browser.WebDriver.WaitForPageload();
                        }
                        await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings);
                        if (browser.WebDriver.ElementExists(By.XPath(SpielOberflaeche.Schmiede.BERITTENEBOGENSCHUETZEN_ERFORSCHEN_SELECTOR_XPATH))) {
                            if (browser.ClickByXpath(SpielOberflaeche.Schmiede.BERITTENEBOGENSCHUETZEN_ERFORSCHEN_SELECTOR_XPATH)) {
                                dorf.Dorfinformationen.BeritteneBogenschuetzenErforscht = true;
                                dorf.Dorfinformationen.BeritteneBogenschuetzenStufe += 1;
                            }
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                    break;
                case Forschen.SchwereKavallerie:
                    if (!dorf.Dorfinformationen.SchwereKavallerieErforscht || (App.Settings.Weltensettings.TenTechLevel && dorf.Dorfinformationen.SchwereKavallerieStufe != gewollteStufe)) {
                        if (!browser.WebDriver.Url.Equals(link)) {
                            browser.WebDriver.Navigate().GoToUrl(link);
                            browser.WebDriver.WaitForPageload();
                        }
                        await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings);
                        if (browser.WebDriver.ElementExists(By.XPath(SpielOberflaeche.Schmiede.SCHWEREKAVALLERIE_ERFORSCHEN_SELECTOR_XPATH))) {
                            if (browser.ClickByXpath(SpielOberflaeche.Schmiede.SCHWEREKAVALLERIE_ERFORSCHEN_SELECTOR_XPATH)) {
                                dorf.Dorfinformationen.SchwereKavallerieErforscht = true;
                                dorf.Dorfinformationen.SchwereKavallerieStufe += 1;
                            }
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                    break;
                case Forschen.Rammboecke:
                    if (!dorf.Dorfinformationen.RammboeckeErforscht || (App.Settings.Weltensettings.TenTechLevel && dorf.Dorfinformationen.RammboeckeStufe != gewollteStufe)) {
                        if (!browser.WebDriver.Url.Equals(link)) {
                            browser.WebDriver.Navigate().GoToUrl(link);
                            browser.WebDriver.WaitForPageload();
                        }
                        await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings);
                        if (browser.WebDriver.ElementExists(By.XPath(SpielOberflaeche.Schmiede.RAMMBOECKE_ERFORSCHEN_SELECTOR_XPATH))) {
                            if (browser.ClickByXpath(SpielOberflaeche.Schmiede.RAMMBOECKE_ERFORSCHEN_SELECTOR_XPATH)) {
                                dorf.Dorfinformationen.RammboeckeErforscht = true;
                                dorf.Dorfinformationen.RammboeckeStufe += 1;
                            }
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                    break;
                case Forschen.Katapulte:
                    if (!dorf.Dorfinformationen.KatapulteErforscht || (App.Settings.Weltensettings.TenTechLevel && dorf.Dorfinformationen.KatapulteStufe != gewollteStufe)) {
                        if (!browser.WebDriver.Url.Equals(link)) {
                            browser.WebDriver.Navigate().GoToUrl(link);
                            browser.WebDriver.WaitForPageload();
                        }
                        await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings);
                        if (browser.WebDriver.ElementExists(By.XPath(SpielOberflaeche.Schmiede.KATAPULTE_ERFORSCHEN_SELECTOR_XPATH))) {
                            if (browser.ClickByXpath(SpielOberflaeche.Schmiede.KATAPULTE_ERFORSCHEN_SELECTOR_XPATH)) {
                                dorf.Dorfinformationen.KatapulteErforscht = true;
                                dorf.Dorfinformationen.KatapulteStufe += 1;
                            }
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                    break;

            }
            return true;
        }
    }

    public enum Forschen {
        Speertraeger,
        Schwertkaempfer,
        Axtkaempfer,
        Bogenschuetzen,
        Spaeher,
        LeichteKavallerie,
        BeritteneBogenschuetzen,
        SchwereKavallerie,
        Rammboecke,
        Katapulte
    }
}
