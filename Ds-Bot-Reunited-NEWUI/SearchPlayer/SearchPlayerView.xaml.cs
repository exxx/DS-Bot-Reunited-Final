﻿using System.Windows;

namespace Ds_Bot_Reunited_NEWUI.SearchPlayer
{
    /// <summary>
    /// Interaktionslogik für SearchPlayerView.xaml
    /// </summary>
    public partial class SearchPlayerView
    {
        public SearchPlayerView(Window owner)
        {
            Owner = owner;
            InitializeComponent();
        }
    }
}
