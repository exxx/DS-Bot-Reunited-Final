﻿using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Input;
using Ds_Bot_Reunited_NEWUI.Annotations;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.SpieldatenViewModel;

namespace Ds_Bot_Reunited_NEWUI.SearchPlayer {
    public class SearchPlayerViewModel : INotifyPropertyChanged {
        private ICommand _searchCommand;
        private ICollectionView _resultsView;
        private readonly SearchPlayerView _view;
        private ICommand _useCommand;
        private bool _progressRingActive;
        private string _playername;

        public string Playername {
            get => _playername;
            set {
                _playername = value;
                OnPropertyChanged();
            }
        }

        public bool UsePlayer { get; set; }
        public SpielerViewModel SelectedPlayer { get; set; }
        
        private AsyncObservableCollection<SpielerViewModel> _results { get; set; }

        private object resultsLock { get; set; } = new object();

        public bool ProgressRingActive {
            get => _progressRingActive;
            set {
                _progressRingActive = value;
                OnPropertyChanged();
            }
        }

        public ICollectionView ResultsView {
            get => _resultsView;
            set {
                _resultsView = value;
                OnPropertyChanged();
            }
        }

        public ICommand SearchCommand => _searchCommand ?? (_searchCommand = new RelayCommand(Search, () => !ProgressRingActive && !string.IsNullOrEmpty(Playername)));
        public ICommand UseCommand => _useCommand ?? (_useCommand = new RelayCommand(Use));

        public SearchPlayerViewModel(SearchPlayerView view) {
            _results = new AsyncObservableCollection<SpielerViewModel>();
            _resultsView = CollectionViewSource.GetDefaultView(_results);
            BindingOperations.EnableCollectionSynchronization(_results,
                resultsLock);
            _view = view;
        }

        private void Use() {
            UsePlayer = true;
            _view.Close();
        }

        private async void Search() {
            await Task.Run(() => {
                ProgressRingActive = true;
                if (!string.IsNullOrEmpty(Playername)) {
                        _results.Clear();
                        if (App.PlayerlistWholeWorld.Any(x => x.PlayerName.ToLower().Contains(Playername.ToLower()))) {
                            foreach (
                                var player in
                                App.PlayerlistWholeWorld.Where(
                                    x => x.PlayerName.ToLower().Contains(Playername.ToLower()))) {
                                _results.Add(player);
                            }
                        }
                }
                ProgressRingActive = false;
            });
            ResultsView.MoveCurrentToFirst();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}