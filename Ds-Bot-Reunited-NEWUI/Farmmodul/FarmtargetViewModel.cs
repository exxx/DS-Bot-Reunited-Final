﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using Ds_Bot_Reunited_NEWUI.Annotations;
using Ds_Bot_Reunited_NEWUI.BerichtModul;
using Ds_Bot_Reunited_NEWUI.Constants;
using Ds_Bot_Reunited_NEWUI.Dorfinformationen;
using Ds_Bot_Reunited_NEWUI.Farmen;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.Spieldaten;
using Ds_Bot_Reunited_NEWUI.SpieldatenViewModel;
using Ds_Bot_Reunited_NEWUI.TimeModul;

namespace Ds_Bot_Reunited_NEWUI.Farmmodul {
    [Serializable]
    public class AttacksOnFarmTarget {
        public DateTime LandTime { get; set; }
        public int AttackerVillageId { get; set; }
        public int Erwartet { get; set; }
        public TruppenTemplate Truppen { get; set; }

        public AttacksOnFarmTarget() {
            Truppen = new TruppenTemplate();
        }
    }

    [Serializable]
    public class FarmtargetViewModel : INotifyPropertyChanged {
        private int _villageId;
        private Dorf _dorf;
        private int _playerId;
        private Spieler _spieler;
        private double _erfolgsquotient;
        private Beute _erbeutetGesamt;
        private bool _active;
        private bool _isSelected;
        private Gebaeude _gebaeude;
        private bool _getsUsed;
        private List<AttacksOnFarmTarget> _attacks;
        private TruppenTemplate _vorhandeneTruppen;
        private TruppenTemplate _ausserhalbTruppen;
        private bool _inFarmassist;
        private AsyncObservableCollection<DorfViewModel> _onlyGetFarmedByObs;
        private bool _fixStatus;
        private DateTime _setInactiveAfter;
        private DateTime _setActiveAfter;

        public int AnzahlAngriffe { get; set; }

        public bool InFarmassist {
            get => _inFarmassist;
            set {
                _inFarmassist = value;
                OnPropertyChanged();
            }
        }

        public double Entfernung(Dorf dorf, Dorf farmtarget, TruppenTemplate vorausgesehen) {
            return Laufzeit.GetTravelTime(dorf, farmtarget, vorausgesehen).TotalMinutes;
        }

        [System.Reflection.Obfuscation(Exclude = false, Feature = "renaming")]
        public Beute ZuErwarten(Dorf von, TruppenTemplate template) {
            TimeSpan laufzeit = Laufzeit.GetTravelTime(von, Dorf, template);
            Beute zuerwartendeBeute = new Beute();
            DateTime lastattack = Attacks.Any() && Attacks.OrderBy(x => x.LandTime).LastOrDefault() != null ? Attacks.OrderBy(x => x.LandTime).LastOrDefault().LandTime : DateTime.MinValue;
            if (lastattack == DateTime.MinValue && App.Berichte.Any() && App.Berichte.OrderBy(x => x.Kampfzeit).LastOrDefault(x => x.VerteidigerVillageId.Equals(Dorf.VillageId)) != null) {
                lastattack = App.Berichte.OrderBy(x => x.Kampfzeit).LastOrDefault(x => x.VerteidigerVillageId.Equals(Dorf.VillageId)).Kampfzeit;
            }

            TimeSpan tss = DateTime.Now - lastattack;
            double stundenAlter = tss.TotalHours + laufzeit.TotalHours;

            if (stundenAlter <= 0) {
                stundenAlter = 0.5;
            }

            if (stundenAlter > 3 && (Erfolgsquotient < 80 || Dorf.PlayerId.Equals(0) || stundenAlter > 3)) {
                stundenAlter = 1;
            }
            DateTime xx = DateTime.MinValue;
            if (App.Berichte.Any() && App.Berichte.OrderBy(x => x.Kampfzeit).LastOrDefault(x => x.VerteidigerVillageId.Equals(Dorf.VillageId) && x.AngreiferTruppenAnzahl.SpaeherAnzahl > 0 && x.AngreiferTruppenVerluste.SpaeherAnzahl == 0) != null && stundenAlter < 3) {
                xx = App.Berichte.OrderBy(x => x.Kampfzeit).LastOrDefault(x => x.VerteidigerVillageId.Equals(Dorf.VillageId) && x.AngreiferTruppenAnzahl.SpaeherAnzahl > 0 && x.AngreiferTruppenVerluste.SpaeherAnzahl == 0).Kampfzeit;
                TimeSpan ts = DateTime.Now - xx;
                if (ts.TotalHours < 5 && ts.TotalHours >= 0 || Erfolgsquotient > 80 && !Dorf.PlayerId.Equals(0)) {
                    Bericht bericht = App.Berichte.LastOrDefault(x => x.VerteidigerVillageId.Equals(Dorf.VillageId) && x.AngreiferTruppenAnzahl.SpaeherAnzahl > 0);
                    if (bericht.NochDrin != null) {
                        zuerwartendeBeute.Holz += bericht.NochDrin.Holz;
                        zuerwartendeBeute.Lehm += bericht.NochDrin.Lehm;
                        zuerwartendeBeute.Eisen += bericht.NochDrin.Eisen;
                    }
                }
            } else if (App.Berichte.Any() && App.Berichte.OrderBy(x => x.Kampfzeit).LastOrDefault(x => x.VerteidigerVillageId.Equals(Dorf.VillageId) && x.Erfolgsquotient < 100) != null) {
                zuerwartendeBeute.Holz = 0;
                zuerwartendeBeute.Lehm = 0;
                zuerwartendeBeute.Eisen = 0;
            }
            if (xx == DateTime.MinValue) {
                xx = DateTime.Now;
            }

            foreach (var attack in Attacks.Where(x => xx < x.LandTime)) {
                zuerwartendeBeute.Holz -= attack.Erwartet / 3;
                zuerwartendeBeute.Lehm -= attack.Erwartet / 3;
                zuerwartendeBeute.Eisen -= attack.Erwartet / 3;
            }

            zuerwartendeBeute.Holz += (int) (stundenAlter * Production.GetProduction(Gebaeude.Holzfaeller) * App.Settings.Weltensettings.Spielgeschwindigkeit * App.Settings.Weltensettings.RessourcenProductionFaktor * (DateTime.Now.Add(laufzeit).Hour < 6 || DateTime.Now.Add(laufzeit).Hour > 22 ? Erfolgsquotient / 100 + (Erfolgsquotient < 80 ? 0.2 : 0) : Erfolgsquotient / 100));
            zuerwartendeBeute.Lehm += (int) (stundenAlter * Production.GetProduction(Gebaeude.Lehmgrube) * App.Settings.Weltensettings.Spielgeschwindigkeit * App.Settings.Weltensettings.RessourcenProductionFaktor * (DateTime.Now.Add(laufzeit).Hour < 6 || DateTime.Now.Add(laufzeit).Hour > 22 ? Erfolgsquotient / 100 + (Erfolgsquotient < 80 ? 0.2 : 0) : Erfolgsquotient / 100));
            zuerwartendeBeute.Eisen += (int) (stundenAlter * Production.GetProduction(Gebaeude.Eisenmine) * App.Settings.Weltensettings.Spielgeschwindigkeit * App.Settings.Weltensettings.RessourcenProductionFaktor * (DateTime.Now.Add(laufzeit).Hour < 6 || DateTime.Now.Add(laufzeit).Hour > 22 ? Erfolgsquotient / 100 + (Erfolgsquotient < 80 ? 0.2 : 0) : Erfolgsquotient / 100));

            if ((int) stundenAlter == 0 && Attacks.Any()) {
                AttacksOnFarmTarget attack = Attacks.OrderBy(x => x.LandTime).LastOrDefault();
                if (attack != null) {
                    if (zuerwartendeBeute.Holz > attack.Erwartet / 3)
                        zuerwartendeBeute.Holz -= attack.Erwartet / 3;
                    if (zuerwartendeBeute.Lehm > attack.Erwartet / 3)
                        zuerwartendeBeute.Lehm -= attack.Erwartet / 3;
                    if (zuerwartendeBeute.Eisen > attack.Erwartet / 3)
                        zuerwartendeBeute.Eisen -= attack.Erwartet / 3;
                }
            }
            return zuerwartendeBeute;
        }

        public TruppenTemplate VorhandeneTruppen {
            get => _vorhandeneTruppen;
            set {
                _vorhandeneTruppen = value;
                OnPropertyChanged();
            }
        }

        public TruppenTemplate AusserhalbTruppen {
            get => _ausserhalbTruppen;
            set {
                _ausserhalbTruppen = value;
                OnPropertyChanged();
            }
        }

        public List<AttacksOnFarmTarget> Attacks {
            get => _attacks;
            set {
                _attacks = value;
                OnPropertyChanged();
            }
        }

        public AsyncObservableCollection<DorfViewModel> OnlyGetFarmedByObs {
            get => _onlyGetFarmedByObs;
            set {
                _onlyGetFarmedByObs = value;
                OnPropertyChanged();
            }
        }

        public bool GetsUsed {
            get => _getsUsed;
            set {
                _getsUsed = value;
                OnPropertyChanged();
            }
        }

        public Gebaeude Gebaeude {
            get => _gebaeude;
            set {
                _gebaeude = value;
                OnPropertyChanged();
            }
        }

        public int VillageId {
            get => _villageId;
            set {
                _villageId = value;
                OnPropertyChanged();
            }
        }

        public Dorf Dorf {
            get => _dorf;
            set {
                _dorf = value;
                OnPropertyChanged();
            }
        }

        public int PlayerId {
            get => _playerId;
            set {
                _playerId = value;
                OnPropertyChanged();
            }
        }

        public Spieler Spieler {
            get => _spieler;
            set {
                _spieler = value;
                OnPropertyChanged();
            }
        }

        public double Erfolgsquotient {
            get => _erfolgsquotient;
            set {
                _erfolgsquotient = value;
                OnPropertyChanged();
            }
        }

        public Beute ErbeutetGesamt {
            get => _erbeutetGesamt;
            set {
                _erbeutetGesamt = value;
                OnPropertyChanged();
            }
        }

        public bool Active {
            get => _active;
            set {
                _active = value;
                OnPropertyChanged();
            }
        }

        public bool FixStatus {
            get => _fixStatus;
            set {
                _fixStatus = value;
                OnPropertyChanged();
            }
        }

        public bool IsSelected {
            get => _isSelected;
            set {
                _isSelected = value;
                OnPropertyChanged();
            }
        }

        public string LastReport {
            get {
                DateTime date = new DateTime();
                if (App.Settings != null)
                    if (App.Berichte != null)
                        if (App.Berichte.Any(x => x.VerteidigerVillageId.Equals(Dorf.VillageId))) {
                            date = App.Berichte.OrderBy(x => x.Kampfzeit).LastOrDefault(x => x.VerteidigerVillageId.Equals(Dorf.VillageId)).Kampfzeit;
                        }
                return date.ToString("dd.MM.yyyy HH:mm:ss", CultureInfo.InvariantCulture);
            }
            set { }
        }

        public string LastAttack {
            get {
                DateTime date = new DateTime();
                if (Attacks.Any()) {
                    date = Attacks.OrderBy(x => x.LandTime).LastOrDefault().LandTime;
                }
                return date.ToString("dd.MM.yyyy HH:mm:ss", CultureInfo.InvariantCulture);
            }
            set { }
        }

        public DateTime SetInactiveAfter {
            get => _setInactiveAfter;
            set {
                _setInactiveAfter = value;
                OnPropertyChanged();
            }
        }

        public DateTime SetActiveAfter {
            get => _setActiveAfter;
            set {
                _setActiveAfter = value;
                OnPropertyChanged();
            }
        }

        public FarmtargetViewModel() {
            Gebaeude = new Gebaeude();
            Attacks = new List<AttacksOnFarmTarget>();
            AusserhalbTruppen = new TruppenTemplate();
            VorhandeneTruppen = new TruppenTemplate();
            Erfolgsquotient = 100;
            OnlyGetFarmedByObs = new AsyncObservableCollection<DorfViewModel>();
        }

        public FarmtargetViewModel(DorfViewModel dorf, Gebaeude gebaeude, TruppenTemplate erspaehteTruppenAußerhalb, TruppenTemplate erspaehteTruppen) {
            Active = true;
            Dorf = dorf.Dorf;
            ErbeutetGesamt = new Beute();
            Erfolgsquotient = 100;
            IsSelected = false;
            VillageId = dorf.VillageId;
            SpielerViewModel ausgewaehlterSpieler = App.PlayerlistWholeWorld.FirstOrDefault(x => x.PlayerId.Equals(dorf.Dorf.PlayerId));
            Spieler = ausgewaehlterSpieler != null ? ausgewaehlterSpieler.Spieler : new Spieler {PlayerId = 0};
            PlayerId = ausgewaehlterSpieler?.PlayerId ?? 0;
            Gebaeude = gebaeude ?? new Gebaeude();
            VorhandeneTruppen = erspaehteTruppen ?? new TruppenTemplate();
            AusserhalbTruppen = erspaehteTruppenAußerhalb ?? new TruppenTemplate();
            Attacks = new List<AttacksOnFarmTarget>();
            OnlyGetFarmedByObs = new AsyncObservableCollection<DorfViewModel>();
        }

        public FarmtargetViewModel Clone() {
            FarmtargetViewModel newFarmtargetViewModel = new FarmtargetViewModel {Active = Active, Dorf = Dorf.Clone(), ErbeutetGesamt = ErbeutetGesamt, Erfolgsquotient = Erfolgsquotient, PlayerId = PlayerId, IsSelected = IsSelected, VillageId = VillageId, Spieler = Spieler.Clone(), Gebaeude = Gebaeude.Clone(), Attacks = new List<AttacksOnFarmTarget>(), AusserhalbTruppen = AusserhalbTruppen, GetsUsed = GetsUsed, VorhandeneTruppen = VorhandeneTruppen, InFarmassist = InFarmassist, OnlyGetFarmedByObs = new AsyncObservableCollection<DorfViewModel>(), AnzahlAngriffe = AnzahlAngriffe, FixStatus = FixStatus, SetActiveAfter = SetActiveAfter, SetInactiveAfter = SetInactiveAfter};
            foreach (var attack in Attacks) {
                newFarmtargetViewModel.Attacks.Add(attack);
            }
            foreach (var id in OnlyGetFarmedByObs) {
                newFarmtargetViewModel.OnlyGetFarmedByObs.Add(id);
            }
            return newFarmtargetViewModel;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
