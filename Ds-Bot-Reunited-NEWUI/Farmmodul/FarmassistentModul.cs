﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Ds_Bot_Reunited_NEWUI.Accountsettings.Ds_Bot_Reunited.Accountsettings;
using Ds_Bot_Reunited_NEWUI.AngreifenModul;
using Ds_Bot_Reunited_NEWUI.Botcaptcha;
using Ds_Bot_Reunited_NEWUI.BrowserManager;
using Ds_Bot_Reunited_NEWUI.Constants;
using Ds_Bot_Reunited_NEWUI.Dorfinformationen;
using Ds_Bot_Reunited_NEWUI.Farmen;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.Properties;
using Ds_Bot_Reunited_NEWUI.Settings;
using Ds_Bot_Reunited_NEWUI.Spieldaten;
using Ds_Bot_Reunited_NEWUI.TimeModul;
using OpenQA.Selenium;

namespace Ds_Bot_Reunited_NEWUI.Farmmodul {
    public static class FarmassistentModul {
        public static async Task<int> StartFarming(Browser browser, Accountsetting accountsettings, AccountDorfSettings dorf) {
            int attacks = 0;
            await App.WaitForAttacks();
            string link = App.Settings.OperateLink + "village=" + dorf.Dorf.VillageId + "" + accountsettings.UvZusatzFuerLink + "&screen=am_farm";
            browser.WebDriver.Navigate().GoToUrl(link);
            browser.WebDriver.WaitForPageload();
            await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings, link);
            try {
                if (browser.WebDriver.ElementExists(By.XPath("//input[@id='all_village_checkbox' and @checked='checked']"))) {
                    await Rndm.Sleep(500, 1000);
                    browser.ClickByXpath("//input[@id='all_village_checkbox']");
                }
                if (browser.WebDriver.ElementExists(By.XPath("//input[@id='attacked_checkbox' and not(@checked)]"))) {
                    await Rndm.Sleep(500, 1000);
                    browser.ClickByXpath("//input[@id='attacked_checkbox']");
                }
                if (browser.WebDriver.ElementExists(By.XPath("//input[@id='full_hauls_checkbox' and @checked='checked']"))) {
                    await Rndm.Sleep(500, 1000);
                    browser.ClickByXpath("//input[@id='full_hauls_checkbox']");
                }
                if (browser.WebDriver.ElementExists(By.XPath("//input[@id='full_losses_checkbox' and @checked='checked']"))) {
                    await Rndm.Sleep(500, 1000);
                    browser.ClickByXpath("//input[@id='full_losses_checkbox']");
                }
                if (browser.WebDriver.ElementExists(By.XPath("//input[@id='partial_losses_checkbox' and @checked='checked']"))) {
                    await Rndm.Sleep(500, 1000);
                    browser.ClickByXpath("//input[@id='partial_losses_checkbox']");
                }
            } catch {
                // ignored
            }
            List<IWebElement> seiten = browser.WebDriver.FindElements(By.ClassName("paged-nav-item")).ToList();
            int start = 1;
            try {
                start = int.Parse(seiten.Last().Text.Replace("[", "").Replace("]", ""));
            } catch {
                // ignored
            }

            await App.WaitForAttacks();
            IWebElement table = browser.WebDriver.FindElement(By.XPath("//table[@id='plunder_list']"));
            try {
                int entfernungErsteZelle = int.Parse(table.FindElements(By.TagName("td"))[7].Text);
                if (entfernungErsteZelle > 10) {
                    if (browser.WebDriver.ElementExists(By.XPath("//a[contains(@href,'order=distance&dir=asc')]"))) {
                        browser.ClickByXpath("//a[contains(@href,'order=distance&dir=asc')]");
                        await Rndm.Sleep(200);
                    }
                }
            } catch {
                // ignored
            }
            await App.WaitForAttacks();
            int interval = dorf.MinDorfFarmeninterval;
            List<string> arten = new List<string>();
            TruppenTemplate vorhandeneTruppen2 = await GetVerfuegbareTruppen(browser);
            if (dorf.UseATemplateFarmassist) {
                if (TruppenVorhanden(dorf.TemplateA, vorhandeneTruppen2)) {
                    arten.Add("a");
                    var template = GetTemplate(browser, "a");
                    dorf.TemplateA.SpaeherAnzahl = vorhandeneTruppen2.SpaeherAnzahl > 0 && dorf.FarmTruppenTemplate.SpaeherActive ? 1 : 0;
                    await App.WaitForAttacks();
                    if (!TruppenGleich(template, dorf.TemplateA)) {
                        await SetTemplate(browser, "a", dorf.TemplateA);
                    }
                }
            }
            if (dorf.UseBTemplateFarmassist) {
                if (TruppenVorhanden(dorf.TemplateB, vorhandeneTruppen2)) {
                    arten.Add("b");
                    var template = GetTemplate(browser, "b");
                    dorf.TemplateB.SpaeherAnzahl = vorhandeneTruppen2.SpaeherAnzahl > 0 && dorf.FarmTruppenTemplate.SpaeherActive ? 1 : 0;
                    await App.WaitForAttacks();
                    if (!TruppenGleich(template, dorf.TemplateB)) {
                        await SetTemplate(browser, "b", dorf.TemplateB);
                    }
                }
            }
            if (dorf.UseCTemplateFarmassist) {
                arten.Add("c");
            }

            int traveltimetolongCounter = 0;
            for (int i = 1; i < start + 1; i++) {
                var vorhandeneTruppen = await GetVerfuegbareTruppen(browser);
                try {
                    if (browser.WebDriver.FindElements(By.ClassName("paged-nav-item")).Count > 0) {
                        browser.ClickElement(browser.WebDriver.FindElements(By.ClassName("paged-nav-item")).FirstOrDefault(x => x.Text.Equals("[" + i + "]")));
                        browser.WebDriver.WaitForPageload();
                        await App.WaitForAttacks();
                        browser.WebDriver.FindElements(By.ClassName("paged-nav-item")).FirstOrDefault(x => x.Text.Equals("[" + i + "]")).Click();
                    }
                } catch (Exception) {
                    // ignored
                }
                bool fehler = true;
                int fehlercounter = 0;
                int letzterButton = 0;
                int getverfuegbareTruppenCounter = 0;
                if (dorf.MinFarmassistDelay < 5)
                    dorf.MinFarmassistDelay = 5;
                if (dorf.MaxFarmassistDelay < 10)
                    dorf.MaxFarmassistDelay = 10;
                if (arten.Count > 0)
                    while (fehler && fehlercounter < 2) {
                        await App.WaitForAttacks();
                        Stopwatch stopwatch = new Stopwatch();
                        for (int l = 0; l < arten.Count; l++) {
                            App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + " While anfang");
                            TruppenTemplate template = new TruppenTemplate();
                            if (arten[l].Equals("a")) {
                                template = GetTemplate(browser, "a");
                            }
                            if (arten[l].Equals("b")) {
                                template = GetTemplate(browser, "b");
                            }
                            if (arten[l].Equals("c")) {
                                template = new TruppenTemplate {LeichteKavallerieAnzahl = 30};
                            }
                            int beute = FarmModul.GetGesamteBeuteOfTruppentemplate(template);
                            int lowestlaufzeit = FarmModul.GetLowestUnitMaxTravelMinutes(dorf, template);
                            DateTime farmenTroopsMustBeHomeTo = dorf.FarmenTroopsMustBeHomeTo;
                            DateTime farmenTroopsMustBeHomeFrom = dorf.FarmenTroopsMustBeHomeFrom;
                            DateTime farmenInactiveTo = dorf.FarmenInactiveTo;
                            DateTime farmenInactiveFrom = dorf.FarmenInactiveFrom;
                            TruppenTemplate uebrig = AttackPreventionModul.AttackPreventionModul.UnterschiedTemplates(vorhandeneTruppen, template);
                            if ((!dorf.ReserveUnits || dorf.ReserveUnits && TruppenVorhanden(dorf.ReservedUnits, uebrig)) && TruppenVorhanden(template, vorhandeneTruppen) || (arten[l].Equals("c") && FarmModul.HasAnyNormalTroopInIt(vorhandeneTruppen))) {
                                if (dorf.FarmTruppenTemplate.SpaeherActive && vorhandeneTruppen.SpaeherAnzahl > 0 && template.SpaeherAnzahl == 0 && TruppenVorhanden(template, vorhandeneTruppen) && !arten[l].Equals("c")) {
                                    template.SpaeherAnzahl = dorf.FarmTruppenTemplate.SpaeherAnzahl;
                                    await SetTemplate(browser, arten[l], template);
                                }
                                fehler = false;
                                List<IWebElement> buttons = browser.WebDriver.FindElements(By.XPath("//a[contains(@class,'farm_icon_" + arten[l] + "')]")).ToList();
                                List<double> durchschnittsDauer = new List<double>();
                                for (int k = letzterButton; k < buttons.Count; k++) {
                                    await App.WaitForAttacks();
                                    await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings);
                                    IWebElement button = buttons[k];
                                    string classbutton = "";
                                    try {
                                        classbutton = button.GetAttribute("class");
                                    } catch {
                                        // ignored
                                    }
                                    if (string.IsNullOrEmpty(classbutton)) {
                                        k--;
                                    } else {
                                        if (button.Displayed && !classbutton.Contains("farm_icon_disabled") && !classbutton.Contains("decoration")) {
                                            try {
                                                string[] splitted = classbutton.Split(' ');
                                                int villageid = int.Parse(splitted[0].Replace("farm_village_", ""));

                                                FarmtargetViewModel farmdorf = App.Farmliste.FirstOrDefault(x => x.Dorf.VillageId.Equals(villageid));
                                                if (farmdorf == null) {
                                                    var firstOrDefault = App.VillagelistWholeWorld.FirstOrDefault(x => x.Value.VillageId.Equals(villageid)).Value;
                                                    if (firstOrDefault != null)
                                                        farmdorf = new FarmtargetViewModel {Active = true, Dorf = firstOrDefault.Dorf, ErbeutetGesamt = new Beute(), Erfolgsquotient = 100, IsSelected = false, VillageId = firstOrDefault.VillageId, Spieler = new Spieler(), PlayerId = 0, Gebaeude = new Gebaeude(), VorhandeneTruppen = new TruppenTemplate(), AusserhalbTruppen = new TruppenTemplate()};
                                                    App.Farmliste.Add(farmdorf);
                                                }
                                                farmdorf.InFarmassist = true;
                                                if (dorf.FarmTruppenTemplate.SpaeherActive && !GenugSpaeherVorhanden(template, vorhandeneTruppen) && !arten[l].Equals("c")) {
                                                    fehler = true;
                                                    letzterButton = k;
                                                    k = buttons.Count;
                                                    template.SpaeherAnzahl = 0;
                                                    await SetTemplate(browser, arten[l], template);
                                                    await Rndm.Sleep(300);
                                                } else {
                                                    if (farmdorf != null) {
                                                        TimeSpan traveltime = Laufzeit.GetTravelTime(dorf.Dorf.Dorf, farmdorf.Dorf, template);
                                                        if (traveltime.TotalMinutes < lowestlaufzeit) {
                                                            var minutes = interval + 1;
                                                            DateTime landtime = DateTime.Now.Add(traveltime);
                                                            var obergrenze = landtime.AddMinutes(minutes);
                                                            var untergrenze = landtime.AddMinutes(-minutes);
                                                            if (!dorf.UseMinDorfFarmeninterval || !farmdorf.Attacks.Any(x => x.LandTime < obergrenze && x.LandTime > untergrenze)) {
                                                                stopwatch.Start();
                                                                if (!farmdorf.OnlyGetFarmedByObs.Any() || farmdorf.OnlyGetFarmedByObs.Any(o => o.VillageId.Equals(dorf.Dorf.VillageId))) {
                                                                    DateTime returnTime = landtime.Add(traveltime);
                                                                    if ((farmenTroopsMustBeHomeTo <= DateTime.Now && farmenTroopsMustBeHomeFrom <= DateTime.Now || returnTime < farmenTroopsMustBeHomeFrom && returnTime < farmenTroopsMustBeHomeTo) && (farmenInactiveTo <= DateTime.Now && farmenInactiveFrom <= DateTime.Now || returnTime < farmenInactiveFrom && returnTime > farmenInactiveTo)) {
                                                                        if (getverfuegbareTruppenCounter == 0 || getverfuegbareTruppenCounter % 30 == 0) {
                                                                            vorhandeneTruppen = await GetVerfuegbareTruppen(browser);
                                                                        }
                                                                        getverfuegbareTruppenCounter++;
                                                                        if (TruppenVorhanden(template, vorhandeneTruppen) || arten[l].Equals("c")) {
                                                                            browser.ClickElement(button);
                                                                            vorhandeneTruppen = FarmModul.SubstractTemplateFromTemplate(vorhandeneTruppen, template);
                                                                            attacks++;
                                                                            if (dorf.MinFarmassistDelay > 0 && dorf.MaxFarmassistDelay > 0) {
                                                                                await Rndm.Sleep(dorf.MinFarmassistDelay, dorf.MaxFarmassistDelay, false);
                                                                            }
                                                                            fehler = false;
                                                                            if (dorf.UseMinDorfFarmeninterval) {
                                                                                farmdorf.Attacks.Add(new AttacksOnFarmTarget {AttackerVillageId = dorf.Dorf.VillageId, Erwartet = beute, LandTime = landtime});
                                                                            }
                                                                        } else {
                                                                            k = buttons.Count;
                                                                            l = arten.Count;
                                                                            fehlercounter = 3;
                                                                            i = start;
                                                                        }
                                                                    }
                                                                }
                                                                stopwatch.Stop();
                                                                durchschnittsDauer.Add(stopwatch.ElapsedMilliseconds);
                                                                stopwatch.Reset();
                                                            }
                                                        } else {
                                                            traveltimetolongCounter++;
                                                            if (traveltimetolongCounter > 5) {
                                                                i = start;
                                                                k = buttons.Count;
                                                            }
                                                        }
                                                    }
                                                }
                                            } catch (Exception e) {
                                                App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + Resources.ErrorFarmassist + " " + e);
                                                fehler = true;
                                                fehlercounter++;
                                            }
                                        }
                                    }
                                }
                                double xx = durchschnittsDauer.Average(x => x);
                                App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": Durchschnittsdauer pro Angriff " + xx + " ms");
                            } else {
                                l = arten.Count;
                                fehlercounter = 3;
                                i = start;
                            }
                        }
                    }
                else {
                    i = start;
                }
            }
            //if (dorf.AddFarmsToFarmassistlistAutomatically && traveltimetolongCounter > 0) {
            //    await FillFarmlistForFarmassist(browser, accountsettings, dorf, vorhandeneTruppen);
            //}
            return attacks;
        }

        public static async Task FillFarmlistForFarmassist(Browser browser, Accountsetting accountsettings, AccountDorfSettings dorf, TruppenTemplate vorhandeneTruppen) {
            int counter = 0;
            try {
                TruppenTemplate template = new TruppenTemplate {SpaeherAnzahl = dorf.FarmTruppenTemplate.SpaeherAnzahl};
                var lowestlaufzeit = FarmModul.GetLowestUnitMaxTravelMinutes(dorf, template);
                if (FarmModul.HasEnoughTroops(vorhandeneTruppen, template)) {
                    if (App.Farmliste.Any(x => x.Attacks.OrderBy(k => k.LandTime).LastOrDefault()?.LandTime < DateTime.Now.AddDays(2))) {
                        var link = App.Settings.OperateLink + "village=" + dorf.Dorf.VillageId + "" + accountsettings.UvZusatzFuerLink + "&screen=place";
                        if (!browser.WebDriver.Url.Equals(link)) {
                            browser.WebDriver.Navigate().GoToUrl(link);
                            browser.WebDriver.WaitForPageload();
                        }
                        var list = App.Farmliste.Where(x => !x.GetsUsed && x.Active && x.Dorf.PlayerId.Equals(0) && (!x.OnlyGetFarmedByObs.Any() || x.OnlyGetFarmedByObs.Any(l => l.VillageId.Equals(dorf.Dorf.VillageId)))).OrderBy(x => x.Entfernung(dorf.Dorf.Dorf, x.Dorf, template)).ToList();
                        if (list.Any()) {
                            for (int i = 0; i < list.Count; i++) {
                                var farm = list[i];
                                var laufzeit = Laufzeit.GetTravelTime(dorf.Dorf.Dorf, farm.Dorf, template);
                                if (laufzeit.TotalMilliseconds < lowestlaufzeit) {
                                    if (FarmModul.HasEnoughTroops(vorhandeneTruppen, template)) {
                                        await Versammlungsplatzmodul.FillKoordsAndTroops(browser.WebDriver, farm.Dorf.XCoordinate, farm.Dorf.YCoordinate, template);
                                        await App.WaitForAttacks();
                                        await Versammlungsplatzmodul.PressAngreifen(browser, "");
                                        browser.WebDriver.WaitForPageload();
                                        counter++;
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {
                App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + Resources.ErrorFarmassist + " while adding " + e);
            }
            App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + Resources.Farmassist + " added " + counter + " farms to farmassistlist");
        }


        public static async Task<TruppenTemplate> GetVerfuegbareTruppen(Browser browser) {
            TruppenTemplate truppe = new TruppenTemplate();
            bool fehlerfrei = false;
            int fehlercounter = 0;
            int delay = 0;
            while (!fehlerfrei && fehlercounter < 3) {
                fehlerfrei = true;
                try {
                    await Task.Delay(2);
                    if (browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Farmassistent.SPEERTRAEGER_VERFUEGBAR_SELECTOR_XPATH)).Count > 0) {
                        if (delay > 0)
                            await Task.Delay(delay);
                        truppe.SpeertraegerAnzahl = int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Farmassistent.SPEERTRAEGER_VERFUEGBAR_SELECTOR_XPATH)).Text);
                    }
                    await Task.Delay(2);
                    if (browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Farmassistent.SCHWERTKAEMPFER_VERFUEGBAR_SELECTOR_XPATH)).Count > 0) {
                        if (delay > 0)
                            await Task.Delay(delay);
                        truppe.SchwertkaempferAnzahl = int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Farmassistent.SCHWERTKAEMPFER_VERFUEGBAR_SELECTOR_XPATH)).Text);
                    }
                    await Task.Delay(2);
                    if (browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Farmassistent.AXTKAEMPFER_VERFUEGBAR_SELECTOR_XPATH)).Count > 0) {
                        if (delay > 0)
                            await Task.Delay(delay);
                        truppe.AxtkaempferAnzahl = int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Farmassistent.AXTKAEMPFER_VERFUEGBAR_SELECTOR_XPATH)).Text);
                    }
                    await Task.Delay(2);
                    if (browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Farmassistent.BOGENSCHUETZEN_VERFUEGBAR_SELECTOR_XPATH)).Count > 0) {
                        if (delay > 0)
                            await Task.Delay(delay);
                        truppe.BogenschuetzenAnzahl = int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Farmassistent.BOGENSCHUETZEN_VERFUEGBAR_SELECTOR_XPATH)).Text);
                    }
                    await Task.Delay(2);
                    if (browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Farmassistent.SPAEHER_VERFUEGBAR_SELECTOR_XPATH)).Count > 0) {
                        if (delay > 0)
                            await Task.Delay(delay);
                        truppe.SpaeherAnzahl = int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Farmassistent.SPAEHER_VERFUEGBAR_SELECTOR_XPATH)).Text);
                    }
                    await Task.Delay(2);
                    if (browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Farmassistent.LEICHTEKAVALLERIE_VERFUEGBAR_SELECTOR_XPATH)).Count > 0) {
                        if (delay > 0)
                            await Task.Delay(delay);
                        truppe.LeichteKavallerieAnzahl = int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Farmassistent.LEICHTEKAVALLERIE_VERFUEGBAR_SELECTOR_XPATH)).Text);
                    }
                    await Task.Delay(2);
                    if (browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Farmassistent.BERITTENEBOGENSCHUETZEN_VERFUEGBAR_SELECTOR_XPATH)).Count > 0) {
                        if (delay > 0)
                            await Task.Delay(delay);
                        truppe.BeritteneBogenschuetzenAnzahl = int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Farmassistent.BERITTENEBOGENSCHUETZEN_VERFUEGBAR_SELECTOR_XPATH)).Text);
                    }
                    await Task.Delay(2);
                    if (browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Farmassistent.SCHWEREKAVALLERIE_VERFUEGBAR_SELECTOR_XPATH)).Count > 0) {
                        if (delay > 0)
                            await Task.Delay(delay);
                        truppe.SchwereKavallerieAnzahl = int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Farmassistent.SCHWEREKAVALLERIE_VERFUEGBAR_SELECTOR_XPATH)).Text);
                    }
                } catch {
                    App.LogString(Resources.SoftErrorFarmassist + " ");
                    fehlerfrei = false;
                    Thread.Sleep(50);
                    delay = 5;
                }
                fehlercounter++;
            }
            return truppe;
        }

        public static TruppenTemplate GetTemplate(Browser browser, string art) {
            int count = art.Contains("a") ? 0 : 1;
            TruppenTemplate truppe = new TruppenTemplate();
            bool fehlerfrei = false;
            int fehlercounter = 0;
            int delay = 0;
            while (!fehlerfrei && fehlercounter < 3) {
                fehlerfrei = true;
                try {
                    Thread.Sleep(2);
                    if (browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Farmassistent.SPEERTRAEGER_TEMPLATE_SELECTOR_XPATH)).Count > 1) {
                        if (delay > 0)
                            Thread.Sleep(delay);
                        truppe.SpeertraegerAnzahl = int.Parse(browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Farmassistent.SPEERTRAEGER_TEMPLATE_SELECTOR_XPATH))[count].GetAttribute("value"));
                    }

                    Thread.Sleep(2);
                    if (browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Farmassistent.SCHWERTKAEMPFER_TEMPLATE_SELECTOR_XPATH)).Count > 1) {
                        if (delay > 0)
                            Thread.Sleep(delay);
                        truppe.SchwertkaempferAnzahl = int.Parse(browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Farmassistent.SCHWERTKAEMPFER_TEMPLATE_SELECTOR_XPATH))[count].GetAttribute("value"));
                    }
                    Thread.Sleep(2);
                    if (browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Farmassistent.AXTKAEMPFER_TEMPLATE_SELECTOR_XPATH)).Count > 1) {
                        if (delay > 0)
                            Thread.Sleep(delay);
                        truppe.AxtkaempferAnzahl = int.Parse(browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Farmassistent.AXTKAEMPFER_TEMPLATE_SELECTOR_XPATH))[count].GetAttribute("value"));
                    }
                    Thread.Sleep(2);
                    if (browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Farmassistent.BOGENSCHUETZEN_TEMPLATE_SELECTOR_XPATH)).Count > 1) {
                        if (delay > 0)
                            Thread.Sleep(delay);
                        truppe.BogenschuetzenAnzahl = int.Parse(browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Farmassistent.BOGENSCHUETZEN_TEMPLATE_SELECTOR_XPATH))[count].GetAttribute("value"));
                    }
                    Thread.Sleep(2);
                    if (browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Farmassistent.SPAEHER_TEMPLATE_SELECTOR_XPATH)).Count > 1) {
                        if (delay > 0)
                            Thread.Sleep(delay);
                        truppe.SpaeherAnzahl = int.Parse(browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Farmassistent.SPAEHER_TEMPLATE_SELECTOR_XPATH))[count].GetAttribute("value"));
                    }
                    Thread.Sleep(2);
                    if (browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Farmassistent.LEICHTEKAVALLERIE_TEMPLATE_SELECTOR_XPATH)).Count > 1) {
                        if (delay > 0)
                            Thread.Sleep(delay);
                        truppe.LeichteKavallerieAnzahl = int.Parse(browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Farmassistent.LEICHTEKAVALLERIE_TEMPLATE_SELECTOR_XPATH))[count].GetAttribute("value"));
                    }
                    Thread.Sleep(2);
                    if (browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Farmassistent.BERITTENEBOGENSCHUETZEN_TEMPLATE_SELECTOR_XPATH)).Count > 1) {
                        if (delay > 0)
                            Thread.Sleep(delay);
                        truppe.BeritteneBogenschuetzenAnzahl = int.Parse(browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Farmassistent.BERITTENEBOGENSCHUETZEN_TEMPLATE_SELECTOR_XPATH))[count].GetAttribute("value"));
                    }
                    Thread.Sleep(2);
                    if (browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Farmassistent.SCHWEREKAVALLERIE_TEMPLATE_SELECTOR_XPATH)).Count > 1) {
                        if (delay > 0)
                            Thread.Sleep(delay);
                        truppe.SchwereKavallerieAnzahl = int.Parse(browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Farmassistent.SCHWEREKAVALLERIE_TEMPLATE_SELECTOR_XPATH))[count].GetAttribute("value"));
                    }
                } catch {
                    App.LogString(Resources.SoftErrorFarmassist + " ");
                    fehlerfrei = false;
                    Thread.Sleep(50);
                    delay = 5;
                }
                fehlercounter++;
            }
            return truppe;
        }


        public static async Task SetTemplate(Browser browser, string art, TruppenTemplate template) {
            bool fehler;
            int counter = 0;
            do {
                fehler = false;
                counter++;
                await Rndm.Sleep(300, 500);
                try {
                    int count = art.Contains("a") ? 0 : 1;
                    if (browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Farmassistent.SPEERTRAEGER_TEMPLATE_SELECTOR_XPATH)).Count > 1) {
                        browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Farmassistent.SPEERTRAEGER_TEMPLATE_SELECTOR_XPATH))[count].Clear();
                        browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Farmassistent.SPEERTRAEGER_TEMPLATE_SELECTOR_XPATH))[count].SendKeys(template.SpeertraegerAnzahl.ToString());
                        await Rndm.Sleep(100, 200);
                    }

                    if (browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Farmassistent.SCHWERTKAEMPFER_TEMPLATE_SELECTOR_XPATH)).Count > 1) {
                        browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Farmassistent.SCHWERTKAEMPFER_TEMPLATE_SELECTOR_XPATH))[count].Clear();
                        browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Farmassistent.SCHWERTKAEMPFER_TEMPLATE_SELECTOR_XPATH))[count].SendKeys(template.SchwertkaempferAnzahl.ToString());
                        await Rndm.Sleep(100, 200);
                    }
                    if (browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Farmassistent.AXTKAEMPFER_TEMPLATE_SELECTOR_XPATH)).Count > 1) {
                        browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Farmassistent.AXTKAEMPFER_TEMPLATE_SELECTOR_XPATH))[count].Clear();
                        browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Farmassistent.AXTKAEMPFER_TEMPLATE_SELECTOR_XPATH))[count].SendKeys(template.AxtkaempferAnzahl.ToString());
                        await Rndm.Sleep(100, 200);
                    }
                    if (browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Farmassistent.BOGENSCHUETZEN_TEMPLATE_SELECTOR_XPATH)).Count > 1) {
                        browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Farmassistent.BOGENSCHUETZEN_TEMPLATE_SELECTOR_XPATH))[count].Clear();
                        browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Farmassistent.BOGENSCHUETZEN_TEMPLATE_SELECTOR_XPATH))[count].SendKeys(template.BogenschuetzenAnzahl.ToString());
                        await Rndm.Sleep(100, 200);
                    }
                    if (browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Farmassistent.SPAEHER_TEMPLATE_SELECTOR_XPATH)).Count > 1) {
                        browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Farmassistent.SPAEHER_TEMPLATE_SELECTOR_XPATH))[count].Clear();
                        browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Farmassistent.SPAEHER_TEMPLATE_SELECTOR_XPATH))[count].SendKeys(template.SpaeherAnzahl.ToString());
                        await Rndm.Sleep(100, 200);
                    }
                    if (browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Farmassistent.LEICHTEKAVALLERIE_TEMPLATE_SELECTOR_XPATH)).Count > 1) {
                        browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Farmassistent.LEICHTEKAVALLERIE_TEMPLATE_SELECTOR_XPATH))[count].Clear();
                        browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Farmassistent.LEICHTEKAVALLERIE_TEMPLATE_SELECTOR_XPATH))[count].SendKeys(template.LeichteKavallerieAnzahl.ToString());
                        await Rndm.Sleep(100, 200);
                    }
                    if (browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Farmassistent.SCHWEREKAVALLERIE_TEMPLATE_SELECTOR_XPATH)).Count > 1) {
                        browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Farmassistent.SCHWEREKAVALLERIE_TEMPLATE_SELECTOR_XPATH))[count].Clear();
                        browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Farmassistent.SCHWEREKAVALLERIE_TEMPLATE_SELECTOR_XPATH))[count].SendKeys(template.SchwereKavallerieAnzahl.ToString());
                        await Rndm.Sleep(100, 200);
                    }
                    if (browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Farmassistent.BERITTENEBOGENSCHUETZEN_TEMPLATE_SELECTOR_XPATH)).Count > 1) {
                        browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Farmassistent.BERITTENEBOGENSCHUETZEN_TEMPLATE_SELECTOR_XPATH))[count].Clear();
                        browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Farmassistent.BERITTENEBOGENSCHUETZEN_TEMPLATE_SELECTOR_XPATH))[count].SendKeys(template.BeritteneBogenschuetzenAnzahl.ToString());
                        await Rndm.Sleep(100, 200);
                    }

                    if (browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Farmassistent.TEMPLATE_BESTAETIGUNGSBUTTON_SELECTOR_XPATH)).Count > 2) {
                        browser.ClickElement(browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Farmassistent.TEMPLATE_BESTAETIGUNGSBUTTON_SELECTOR_XPATH))[art.Equals("a") ? 0 : 1]);
                        browser.WebDriver.WaitForPageload();
                        browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Farmassistent.TEMPLATE_BESTAETIGUNGSBUTTON_SELECTOR_XPATH))[art.Equals("a") ? 0 : 1].Click();
                        browser.WebDriver.WaitForPageload();
                    }
                } catch (Exception e) {
                    fehler = true;
                    App.LogString(Resources.ErrorFarmassist + " " + e + "\n" + " " + Resources.Botshouldstillwork + "!");
                }
            } while (fehler && counter < 3);
        }

        public static bool GenugSpaeherVorhanden(TruppenTemplate vorlage, TruppenTemplate vorhandeneTruppen) {
            if (vorlage.SpaeherAnzahl > vorhandeneTruppen.SpaeherAnzahl && vorlage.SpaeherAnzahl > 0) {
                return false;
            }
            return true;
        }

        public static bool TruppenVorhanden(TruppenTemplate vorlage, TruppenTemplate vorhandeneTruppen) {
            if (vorlage.SpeertraegerAnzahl > vorhandeneTruppen.SpeertraegerAnzahl && vorlage.SpeertraegerAnzahl > 0) {
                return false;
            }
            if (vorlage.SchwertkaempferAnzahl > vorhandeneTruppen.SchwertkaempferAnzahl && vorlage.SchwertkaempferAnzahl > 0) {
                return false;
            }
            if (vorlage.AxtkaempferAnzahl > vorhandeneTruppen.AxtkaempferAnzahl && vorlage.AxtkaempferAnzahl > 0) {
                return false;
            }
            if (vorlage.BogenschuetzenAnzahl > vorhandeneTruppen.BogenschuetzenAnzahl && vorlage.BogenschuetzenAnzahl > 0) {
                return false;
            }
            if (vorlage.LeichteKavallerieAnzahl > vorhandeneTruppen.LeichteKavallerieAnzahl && vorlage.LeichteKavallerieAnzahl > 0) {
                return false;
            }
            if (vorlage.BeritteneBogenschuetzenAnzahl > vorhandeneTruppen.BeritteneBogenschuetzenAnzahl && vorlage.BeritteneBogenschuetzenAnzahl > 0) {
                return false;
            }
            if (vorlage.SchwereKavallerieAnzahl > vorhandeneTruppen.SchwereKavallerieAnzahl && vorlage.SchwereKavallerieAnzahl > 0) {
                return false;
            }
            if (vorlage.RammboeckeAnzahl > vorhandeneTruppen.RammboeckeAnzahl && vorlage.RammboeckeAnzahl > 0) {
                return false;
            }
            if (vorlage.KatapulteAnzahl > vorhandeneTruppen.LeichteKavallerieAnzahl && vorlage.KatapulteAnzahl > 0) {
                return false;
            }
            if (vorlage.PaladinAnzahl > vorhandeneTruppen.PaladinAnzahl && vorlage.PaladinAnzahl > 0) {
                return false;
            }
            return true;
        }

        public static bool TruppenGleich(TruppenTemplate vorlage, TruppenTemplate vorhandeneTruppen) {
            if (vorlage.SpeertraegerAnzahl != vorhandeneTruppen.SpeertraegerAnzahl) {
                return false;
            }

            if (vorlage.SchwertkaempferAnzahl != vorhandeneTruppen.SchwertkaempferAnzahl) {
                return false;
            }
            if (vorlage.AxtkaempferAnzahl != vorhandeneTruppen.AxtkaempferAnzahl) {
                return false;
            }
            if (vorlage.BogenschuetzenAnzahl != vorhandeneTruppen.BogenschuetzenAnzahl) {
                return false;
            }
            if (vorlage.SpaeherAnzahl != vorhandeneTruppen.SpaeherAnzahl) {
                return false;
            }
            if (vorlage.LeichteKavallerieAnzahl != vorhandeneTruppen.LeichteKavallerieAnzahl) {
                return false;
            }
            if (vorlage.BeritteneBogenschuetzenAnzahl != vorhandeneTruppen.BeritteneBogenschuetzenAnzahl) {
                return false;
            }
            if (vorlage.SchwereKavallerieAnzahl != vorhandeneTruppen.SchwereKavallerieAnzahl) {
                return false;
            }
            if (vorlage.RammboeckeAnzahl != vorhandeneTruppen.RammboeckeAnzahl) {
                return false;
            }
            if (vorlage.KatapulteAnzahl != vorhandeneTruppen.KatapulteAnzahl) {
                return false;
            }
            return true;
        }
    }
}
