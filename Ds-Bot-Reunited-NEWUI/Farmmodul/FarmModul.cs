﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ds_Bot_Reunited_NEWUI.Accountsettings.Ds_Bot_Reunited.Accountsettings;
using Ds_Bot_Reunited_NEWUI.AngreifenModul;
using Ds_Bot_Reunited_NEWUI.BerichtModul;
using Ds_Bot_Reunited_NEWUI.Botcaptcha;
using Ds_Bot_Reunited_NEWUI.BrowserManager;
using Ds_Bot_Reunited_NEWUI.Constants;
using Ds_Bot_Reunited_NEWUI.Farmen;
using Ds_Bot_Reunited_NEWUI.Farmliste;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.Properties;
using Ds_Bot_Reunited_NEWUI.Rekrutieren;
using Ds_Bot_Reunited_NEWUI.Settings;
using Ds_Bot_Reunited_NEWUI.Spieldaten;
using Ds_Bot_Reunited_NEWUI.TimeModul;
using OpenQA.Selenium;

// ReSharper disable EmptyGeneralCatchClause

namespace Ds_Bot_Reunited_NEWUI.Farmmodul {
	public class FarmModul {
		public static async Task StartFarming(Browser browser, Accountsetting accountsettings, int playerpoints) {
			await App.WaitForAttacks();
			App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
				               ? accountsettings.Uvname
				               : accountsettings.Accountname) + ": " + "Farming started");
			FarmlistenVerwaltung.Refresh();
			foreach (var dorf in accountsettings.DoerferSettings
			                                    .Where(x => !string.IsNullOrEmpty(x.Dorf.Dorf.VillageName) &&
			                                                x.Dorf.PlayerId.Equals(accountsettings.Playerid))
			                                    .OrderBy(x => x.FarmOrderNumber)
			                                    .ThenBy(x => x.Dorfinformationen.LastFarmenZeitpunkt)) {
				var attackcounter = 0;
				await App.WaitForAttacks();
				var farmenInactiveTo = dorf.FarmenInactiveTo;
				var farmenInactiveFrom = dorf.FarmenInactiveFrom;
				if (accountsettings.FarmenActive && dorf.FarmenActive &&
				    (farmenInactiveTo < DateTime.Now && farmenInactiveFrom < DateTime.Now ||
				     DateTime.Now < farmenInactiveFrom || DateTime.Now > farmenInactiveTo)) {
					if (dorf.AddVillagesAutomatically)
						App.Farmliste.AddRange(FarmlistenVerwaltung
						                       .GetFarmableVillagesList(dorf.AddVillagesRadiusMax, dorf.Dorf.XCoordinate
						                                                , dorf.Dorf.YCoordinate
						                                                , dorf.AddVillagesPointsMin
						                                                , dorf.AddVillagesPointsMax
						                                                , dorf.AddPlayersAutomatically
						                                                , dorf.AddPlayersPointsMin
						                                                , dorf.AddPlayersPointsMax
						                                                , dorf.AddPlayersMinInactiveDays
						                                                , dorf.AddVillagesRadiusMin)
						                       .Select(x => new FarmtargetViewModel(x, null, null, null)));
					var farminterval = dorf.Farmeninterval;
					var now = DateTime.Now;
					if (dorf.NextAvailableTroopAtHomeAt > DateTime.MinValue && dorf.NextAvailableTroopAtHomeAt < now ||
					    dorf.Dorfinformationen.LastFarmenZeitpunkt < now.AddMinutes(-farminterval)) {
						App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
							               ? accountsettings.Uvname
							               : accountsettings.Accountname) + ": " + "Farming Village: " +
						              dorf.Dorf.VillageId + " Name: " + dorf.Dorf.Name + " Mode " + dorf.FarmMode +
						              " started");
						await App.WaitForAttacks();
						if (string.IsNullOrEmpty(dorf.FarmMode) || dorf.FarmMode.Equals(Resources.Intelligent)) {
							await BerichteModul.StartReadingBerichte(browser, accountsettings);
							var link = App.Settings.OperateLink + "village=" + dorf.Dorf.VillageId + "" +
							           accountsettings.UvZusatzFuerLink + "&screen=place";
							browser.WebDriver.Navigate().GoToUrl(link);
							browser.WebDriver.WaitForPageload();

							await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings, link);
							await App.WaitForAttacks();
							DorfinformationenModul.DorfinformationenModul.ImportMissingAttacks(browser, dorf);
							var verfuegbareTruppen =
								DorfinformationenModul.DorfinformationenModul.GetAvailableTroops(browser, dorf);
							var liste = CanFarmen(dorf, verfuegbareTruppen);
							if (liste.Any() &&
							    (dorf.ReserveUnits && HasEnoughTroops(verfuegbareTruppen, dorf.ReservedUnits) ||
							     !dorf.ReserveUnits)) {
								var interval = dorf.MinDorfFarmeninterval;
								var farmen = App
								             .Farmliste
								             .Where(x => !x.GetsUsed && x.Active && dorf.FarmenActive &&
								                         (x.Dorf.VillagePoints * 20 > playerpoints ||
								                          x.Dorf.PlayerId.Equals(0)) &&
								                         (!x.OnlyGetFarmedByObs.Any() ||
								                          x.OnlyGetFarmedByObs.Any(l =>
									                                                   l
										                                                   .VillageId
										                                                   .Equals(dorf.Dorf.VillageId))
								                         ))
								             .OrderBy /*(x => App.PlayerlistWholeWorld.Any(y => y.PlayerId.Equals(x.PlayerId)) ? 1 : 0).ThenBy*/(x =>
									                                                                                                                 x
										                                                                                                                 .Entfernung(dorf.Dorf.Dorf
										                                                                                                                             , x
											                                                                                                                             .Dorf
										                                                                                                                             , new
										                                                                                                                               TruppenTemplate {
											                                                                                                                                               SpeertraegerAnzahl
												                                                                                                                                               = 1
										                                                                                                                                               }))
								             .ToList();
								for (int i = 0; i < farmen.Count; i++) {
									var farm = farmen[i];
									await App.WaitForAttacks();
									verfuegbareTruppen =
										DorfinformationenModul.DorfinformationenModul.GetAvailableTroops(browser, dorf);
									liste = CanFarmen(dorf, verfuegbareTruppen);
									var counter = 0;
									if (liste.Any() &&
									    (dorf.ReserveUnits && HasEnoughTroops(verfuegbareTruppen, dorf.ReservedUnits) ||
									     !dorf.ReserveUnits)) {
										bool fehler;
										do {
											if (!browser.WebDriver.Url.Contains(App.Settings.OperateLink + "village=" +
											                                    dorf.Dorf.VillageId +
											                                    accountsettings.UvZusatzFuerLink +
											                                    "&screen=place"))
												browser.WebDriver.Navigate()
												       .GoToUrl(App.Settings.OperateLink + "village=" +
												                dorf.Dorf.VillageId +
												                accountsettings.UvZusatzFuerLink + "&screen=place");
											fehler = false;
											var farmdorf = farm;
											try {
												farmdorf.GetsUsed = true;
												var template =
													GetZuversendendeTruppen(accountsettings, dorf, verfuegbareTruppen
													                        , farmdorf, liste, dorf.Dorf.Dorf
													                        , playerpoints);
												if (template != null) {
													var traveltime =
														Laufzeit.GetTravelTime(dorf.Dorf.Dorf, farmdorf.Dorf, template);
													var uebrig =
														AttackPreventionModul
															.AttackPreventionModul
															.UnterschiedTemplates(verfuegbareTruppen, template);
													var maxTravelTime = GetLowestUnitMaxTravelMinutes(dorf, template);
													if (traveltime.TotalMinutes < maxTravelTime &&
													    HasAnyNormalTroopInIt(template) &&
													    (dorf.ReserveUnits &&
													     HasEnoughTroops(uebrig, dorf.ReservedUnits) ||
													     !dorf.ReserveUnits)) {
														var landtime = DateTime.Now.Add(traveltime);

														var attacksOnFarmTarget = farmdorf
														                          .Attacks.OrderBy(x => x.LandTime)
														                          .LastOrDefault(x => x.LandTime <
														                                              landtime);
														double minutes = interval + 1;
														if (attacksOnFarmTarget != null)
															minutes = landtime
															          .Subtract(attacksOnFarmTarget.LandTime)
															          .TotalMinutes;
														var farmenTroopsMustBeHomeTo = dorf.FarmenTroopsMustBeHomeTo;
														var farmenTroopsMustBeHomeFrom =
															dorf.FarmenTroopsMustBeHomeFrom;
														now = DateTime.Now;
														var lowestlaufzeit =
															GetLowestUnitMaxTravelMinutes(dorf, template);
														var minuteszukunft =
															farmdorf.Attacks.Any() &&
															farmdorf.Attacks.OrderBy(x => x.LandTime)
															        .FirstOrDefault(x => x.LandTime > landtime) != null
																? landtime
																  .Subtract(farmdorf.Attacks.OrderBy(x => x.LandTime)
																                    .FirstOrDefault(x => x.LandTime >
																                                         landtime)
																                    .LandTime).TotalMinutes
																: (interval + 1) * -1;
														if (minutes > interval && minuteszukunft < -interval &&
														    (farmenTroopsMustBeHomeTo < now &&
														     farmenTroopsMustBeHomeFrom < now ||
														     now.Add(traveltime).Add(traveltime) <
														     farmenTroopsMustBeHomeFrom &&
														     now.Add(traveltime).Add(traveltime) <
														     farmenTroopsMustBeHomeTo) &&
														    (farmenInactiveTo < now && farmenInactiveFrom < now ||
														     now.Add(traveltime).Add(traveltime) < farmenInactiveFrom &&
														     now.Add(traveltime).Add(traveltime) > farmenInactiveTo) &&
														    traveltime.TotalMinutes < lowestlaufzeit &&
														    (!farmdorf.OnlyGetFarmedByObs.Any() ||
														     farmdorf
															     .OnlyGetFarmedByObs
															     .Any(o => o.VillageId.Equals(dorf.Dorf.VillageId)))) {
															verfuegbareTruppen =
																SubstractTemplateFromTemplate(verfuegbareTruppen
																                              , template);
															await Botschutzerkennung
																.CheckIfBotschutz(browser, accountsettings);
															await App.WaitForAttacks();
															await Versammlungsplatzmodul
																.FillKoordsAndTroops(browser.WebDriver
																                     , farmdorf.Dorf.XCoordinate
																                     , farmdorf.Dorf.YCoordinate
																                     , template);
															await App.WaitForAttacks();
															await Versammlungsplatzmodul
																.PressAngreifen(browser, template.TemplateName);
															await Rndm.Sleep(App.Settings.MinActionDelay
															                 , App.Settings.MaxActionDelay);
															browser.WebDriver.WaitForPageload();
															if (browser.WebDriver.FindElements(By.XPath(SpielOberflaeche
															                                            .Allgemein
															                                            .ERRORBOX_SELECTOR_XPATH))
															           .Count > 0) {
																try {
																	farmdorf.Active = false;
																	App.LogString((!string.IsNullOrEmpty(accountsettings
																		                                     .Uvname)
																		               ? accountsettings.Uvname
																		               : accountsettings.Accountname) +
																	              ": " +
																	              "Farming Village DEACTIVATED because of FAILURE " +
																	              farmdorf.Dorf.XCoordinate + "|" +
																	              farmdorf.Dorf.YCoordinate + " " +
																	              farmdorf.Dorf.VillageName + " " +
																	              browser
																		              .WebDriver
																		              .FindElements(By.XPath("//div[@class='error_box']"))
																		              [0].Text);
																} catch {
																	App.LogString((!string.IsNullOrEmpty(accountsettings
																		                                     .Uvname)
																		               ? accountsettings.Uvname
																		               : accountsettings.Accountname) +
																	              ": " +
																	              "Farming Village DEACTIVATION FAILURE!! " +
																	              farmdorf.Dorf.XCoordinate + "|" +
																	              farmdorf.Dorf.YCoordinate + " " +
																	              farmdorf.Dorf.VillageName);
																}

																try {
																	browser
																		.WebDriver
																		.FindElement(By.XPath("//span[@class='village-info']"))
																		.Click();
																} catch {
																	browser.WebDriver.Navigate()
																	       .GoToUrl(App.Settings.OperateLink +
																	                "village=" + dorf.Dorf.VillageId +
																	                accountsettings.UvZusatzFuerLink +
																	                "&screen=place");
																}

																browser.WebDriver.WaitForPageload();
															} else {
																await Versammlungsplatzmodul.PressOk(browser);
																farmdorf.AnzahlAngriffe++;
																await App.WaitForAttacks();
																App.LogString((!string.IsNullOrEmpty(accountsettings
																	                                     .Uvname)
																	               ? accountsettings.Uvname
																	               : accountsettings.Accountname) +
																              ": " + "Farming Attack sent from " +
																              dorf.Dorf.Name + " to " +
																              farmdorf.Dorf.XCoordinate + "|" +
																              farmdorf.Dorf.YCoordinate + " " +
																              farmdorf.Dorf.VillageName);
																farmdorf.AnzahlAngriffe++;
																attackcounter++;
																await Botschutzerkennung
																	.CheckIfBotschutz(browser, accountsettings);
																var vorraussichtlicheBeute =
																	GetGesamteBeuteOfTruppentemplate(template);
																farmdorf.Attacks.Add(new AttacksOnFarmTarget {
																	                                             AttackerVillageId
																		                                             = dorf
																		                                               .Dorf
																		                                               .VillageId
																	                                             , Erwartet
																		                                             = vorraussichtlicheBeute
																	                                             , LandTime
																		                                             = landtime
																	                                             , Truppen
																		                                             = template
																                                             });
															}
														}
													} else {
														i = farmen.Count;
													}
												}
											} catch (Exception e) {
												App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
													               ? accountsettings.Uvname
													               : accountsettings.Accountname) + ": " +
												              "Farmmodul ERROR! " + e);
												fehler = true;
											}

											counter++;
											farmdorf.GetsUsed = false;
										} while (fehler && counter < 3);
									} else {
										i = farmen.Count;
									}
								}

								var commands = browser
								               .WebDriver
								               .FindElements(By.XPath(SpielOberflaeche
								                                      .Allgemein.COMMANDROW_SELECTOR_XPATH)).ToList();
								if (commands.Any()) {
									var element =
										commands.FirstOrDefault(x =>
											                        x.Text.Contains(NAMECONSTANTS
												                                        .GetNameForServer(Names
													                                                          .Rueckkehr)));
									if (element != null) {
										var temppp = element.FindElements(By.TagName("td"))[1].Text;
										dorf.NextAvailableTroopAtHomeAt =
											TimeModulAttackDetection.GetTimeOfString(temppp);
									}
								}

								//if (CanFarmen(dorf, verfuegbareTruppen).Any())
								//	await AttackNewVillagesOverMap(browser, accountsettings, dorf);
							}
						} else
							attackcounter = await FarmassistentModul.StartFarming(browser, accountsettings, dorf);

						App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
							               ? accountsettings.Uvname
							               : accountsettings.Accountname) + ": " + "Farming Village: " +
						              dorf.Dorf.VillageId + " " + attackcounter + " Attacks sent");


						dorf.Dorfinformationen.LastFarmenZeitpunkt = DateTime.Now;
						if (accountsettings.NextAvailableTroopAtHomeAt > dorf.NextAvailableTroopAtHomeAt ||
						    accountsettings.NextAvailableTroopAtHomeAt == DateTime.MinValue)
							accountsettings.NextAvailableTroopAtHomeAt = dorf.NextAvailableTroopAtHomeAt;
						App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
							               ? accountsettings.Uvname
							               : accountsettings.Accountname) + ": " + "Farming Village: " +
						              dorf.Dorf.VillageId + " " + Resources.Name + ": " + dorf.Dorf.Name + " " +
						              Resources.Mode + " " + dorf.FarmMode + " " + Resources.Ended);
					}
				}
			}

			App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
				               ? accountsettings.Uvname
				               : accountsettings.Accountname) + ": " + " Farming " + Resources.Ended);
			accountsettings.LastFarmenZeitpunkt = DateTime.Now;
		}

		public static async Task<int> AttackNewVillagesOverMap(Browser browser
		                                                       , Accountsetting accountsettings
		                                                       , AccountDorfSettings dorf
		                                                       , int angriffe = 2000
		                                                       , bool unterstuetzen = false
		                                                       , bool ueberFarmassistent = false
		                                                       , int maxpoints = 3000
		                                                       , bool forquest = false) {
			var counterAngriffe = 0;
			if (dorf != null && (angriffe < 2000 && angriffe > 0 && forquest || unterstuetzen)) {
				await QuestModul.QuestModul.CheckIfQuestOpenAndCloseIt(browser);

				var link = App.Settings.OperateLink + accountsettings.UvZusatzFuerLink + "&screen=map";
				browser.WebDriver.Navigate().GoToUrl(link);
				browser.WebDriver.WaitForPageload();
				var rnd = new Random();
				var k = rnd.Next(0, 5);
				var durchlaeufe = 24;
				var ohneangriffcounter = 0;
				for (var i = 0; i < durchlaeufe; i++) {
					var stop = false;
					await QuestModul.QuestModul.CheckIfQuestOpenAndCloseIt(browser);
					var attackpromap = 0;
					await Map.RandomMapNavigation(i, k, browser);
					browser.WebDriver.WaitForAjax();
					while (stop == false) {
						attackpromap = 0;
						try {
							browser.WebDriver.WaitForAjax();
							var doerfer = browser
							              .WebDriver
							              .FindElements(By.XPath(SpielOberflaeche
							                                     .Karte.BARBARENDORF_MINI_IMAGE_LINK_XPATH)).ToList();
							if (maxpoints >= 300)
								doerfer.AddRange(browser.WebDriver.FindElements(By.XPath(SpielOberflaeche
								                                                         .Karte
								                                                         .BARBARENDORF_300_IMAGE_LINK_XPATH)));
							if (maxpoints >= 1500)
								doerfer.AddRange(browser.WebDriver.FindElements(By.XPath(SpielOberflaeche
								                                                         .Karte
								                                                         .BARBARENDORF_1500_IMAGE_LINK_XPATH)));

							//map_navigation
							for (var l = 0; l < doerfer.Count; l++) {
								var mapdorf = doerfer[l];
								try {
									if (!stop) {
										var id = int.Parse(mapdorf.GetAttribute("id").Replace("map_village_", ""));
										if (!App.Farmliste.Any(x => x.VillageId.Equals(id)) && mapdorf.Displayed ||
										    forquest && mapdorf.Displayed) {
											try {
												if (browser.WebDriver.FindElement(By.Id("map_go_home_boundary"))
												           .Displayed) {
													var js = browser.WebDriver as IJavaScriptExecutor;
													js.ExecuteScript("$('#map_go_home_boundary').hide();");
													await Task.Delay(rnd.Next(10, 30));
												}
											} catch (Exception) { }

											try {
												if (browser.WebDriver.FindElement(By.Id("map_village_undefined"))
												           .Displayed) {
													var js = browser.WebDriver as IJavaScriptExecutor;
													js.ExecuteScript("$('#map_village_undefined').hide();");
													await Task.Delay(rnd.Next(10, 30));
												}
											} catch (Exception) { }

											try {
												if (browser.WebDriver.FindElement(By.Id("map_mover")).Displayed) {
													var js = browser.WebDriver as IJavaScriptExecutor;
													js.ExecuteScript("$('#map_mover').hide();");
													await Task.Delay(rnd.Next(10, 30));
												}
											} catch (Exception) { }

											try {
												if (browser.WebDriver.FindElement(By.Id("special_effects_container"))
												           .Displayed) {
													var js = browser.WebDriver as IJavaScriptExecutor;
													js.ExecuteScript("$('#special_effects_container').hide();");
													await Task.Delay(rnd.Next(10, 30));
												}
											} catch (Exception) { }

											try {
												var elements = browser
												               .WebDriver
												               .FindElements(By.XPath("//canvas[contains(@id,'map_canvas')]"))
												               .ToList();
												if (elements.Count > 0) {
													var js = browser.WebDriver as IJavaScriptExecutor;

													foreach (var element in elements) {
														var id2 = element.GetAttribute("id");
														js.ExecuteScript("$('#" + id2 + "').hide();");
													}
												}
											} catch (Exception) { }

											(browser.WebDriver as IJavaScriptExecutor)
												.ExecuteScript("$('#menu_row').hide();");
											(browser.WebDriver as IJavaScriptExecutor)
												.ExecuteScript("$('.top_bar').hide();");
											(browser.WebDriver as IJavaScriptExecutor)
												.ExecuteScript("$('#chat-wrapper').hide();");
											(browser.WebDriver as IJavaScriptExecutor)
												.ExecuteScript("$('#footer').hide();");
											await QuestModul.QuestModul.CheckIfQuestOpenAndCloseIt(browser);
											browser.ClickElement(mapdorf);
											browser.WebDriver.WaitForAjax();
											await QuestModul.QuestModul.CheckIfQuestOpenAndCloseIt(browser);
											if (ueberFarmassistent) {
												await Map.ClickFarmassistA(browser);
												browser.WebDriver.WaitForAjax();
												await QuestModul.QuestModul.CheckIfQuestOpenAndCloseIt(browser);
												try {
													browser.WebDriver.WaitForAjax();
													var text = browser
													           .WebDriver
													           .FindElement(By.XPath(SpielOberflaeche
													                                 .Allgemein
													                                 .ERRORBOX_SELECTOR_XPATH)).Text;
													i = 25;
													l = doerfer.Count + 1;
													stop = true;
												} catch (Exception) {
													counterAngriffe++;
													attackpromap++;
													if (counterAngriffe >= angriffe || unterstuetzen) {
														i = 25;
														l = doerfer.Count + 1;
														stop = true;
													}
												}
											} else {
												await Map.ClickAtt(browser.WebDriver);
												browser.WebDriver.WaitForAjax();
												var verfuegbareTruppen =
													DorfinformationenModul
														.DorfinformationenModul.GetAvailableTroops(browser, dorf);
												if (!CanFarmen(dorf, verfuegbareTruppen).Any())
													i = 25;
												else {
													var liste = CanFarmen(dorf, verfuegbareTruppen);
													var farmdorf =
														new
															FarmtargetViewModel(App.VillagelistWholeWorld.FirstOrDefault(x => x.Value.VillageId.Equals(id)).Value
															                    , null, null, null);
													var zuversenden =
														GetZuversendendeTruppen(accountsettings, dorf
														                        , verfuegbareTruppen, farmdorf, liste
														                        , dorf.Dorf.Dorf, 100000);
													if (forquest)
														zuversenden =
															new TruppenTemplate {
																                    SpeertraegerAnzahl = 2
																                    , SchwertkaempferAnzahl = 2
															                    };
													if (unterstuetzen)
														zuversenden = new TruppenTemplate {SpeertraegerAnzahl = 1};
													if (zuversenden != null) {
														await Rndm.Sleep(200);
														await Versammlungsplatzmodul
															.FillKoordsAndTroops(browser.WebDriver, 0, 0, zuversenden);
														await Botschutzerkennung
															.CheckIfBotschutz(browser, accountsettings);
														if (unterstuetzen)
															await Versammlungsplatzmodul.PressUnterstuetzen(browser);
														else
															await Versammlungsplatzmodul
																.PressAngreifen(browser, zuversenden.TemplateName);
														browser.WebDriver.WaitForAjax();
														await Botschutzerkennung
															.CheckIfBotschutz(browser, accountsettings);
														try {
															browser.WebDriver.WaitForAjax();
															var text = browser
															           .WebDriver
															           .FindElement(By.XPath(SpielOberflaeche
															                                 .Allgemein
															                                 .ERRORBOX_SELECTOR_XPATH))
															           .Text;
															i = 25;
															l = doerfer.Count + 1;
															stop = true;
														} catch (Exception) {
															await Versammlungsplatzmodul.PressOk(browser);
															counterAngriffe++;
															attackpromap++;
															browser.WebDriver.WaitForPageload();
															browser.WebDriver.WaitForAjax();
															await QuestModul
															      .QuestModul.CheckIfQuestOpenAndCloseIt(browser);
															if (counterAngriffe >= angriffe || unterstuetzen) {
																i = 25;
																l = doerfer.Count + 1;
																stop = true;
															}

															App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
																               ? accountsettings.Uvname
																               : accountsettings.Accountname) + ": " +
															              "Farming Attack sent from " + dorf.Dorf.Name +
															              " to " + farmdorf.Dorf.XCoordinate + "|" +
															              farmdorf.Dorf.YCoordinate + " " +
															              farmdorf.Dorf.VillageName);
															await Rndm.Sleep(300);
														}
													} else {
														browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
														                                       .Quest
														                                       .QUESTFENSTER_SCHLIESSEN_BUTTON_SELECTOR_XPATH))
														       .Click();
														await Rndm.Sleep(200);
													}
												}
											}
										}
									}
								} catch (Exception e) {
									App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
										               ? accountsettings.Uvname
										               : accountsettings.Accountname) + ": " +
									              "Farmmodul over Map ERROR! " + e);
								}

								if (forquest) l--;
							}

							stop = true;
						} catch {
							stop = true;
						}
					}

					if (attackpromap == 0) ohneangriffcounter++;
					if (ohneangriffcounter > 5) i = durchlaeufe;
				}
			}

			return counterAngriffe;
		}

		private static bool ValidPaar(FarmTemplate template, TruppenTemplate verfuegbareTruppen) {
			var truppeeins = false;
			var truppezwei = false;
			if (template.TruppeEinsName.Contains("spear") &&
			    verfuegbareTruppen.SpeertraegerAnzahl >= template.TruppeEinsMin)
				truppeeins = true;
			if (template.TruppeEinsName.Contains("sword") &&
			    verfuegbareTruppen.SchwertkaempferAnzahl >= template.TruppeEinsMin)
				truppeeins = true;
			if (template.TruppeEinsName.Contains("axe") &&
			    verfuegbareTruppen.AxtkaempferAnzahl >= template.TruppeEinsMin)
				truppeeins = true;
			if (template.TruppeEinsName.Contains("archer") &&
			    verfuegbareTruppen.BogenschuetzenAnzahl >= template.TruppeEinsMin)
				truppeeins = true;
			if (template.TruppeEinsName.Contains("spy") && verfuegbareTruppen.SpaeherAnzahl >= template.TruppeEinsMin)
				truppeeins = true;
			if (template.TruppeEinsName.Contains("light") &&
			    verfuegbareTruppen.LeichteKavallerieAnzahl >= template.TruppeEinsMin)
				truppeeins = true;
			if (template.TruppeEinsName.Contains("marcher") &&
			    verfuegbareTruppen.BeritteneBogenschuetzenAnzahl >= template.TruppeEinsMin)
				truppeeins = true;
			if (template.TruppeEinsName.Contains("heavy") &&
			    verfuegbareTruppen.SchwereKavallerieAnzahl >= template.TruppeEinsMin)
				truppeeins = true;
			if (template.TruppeEinsName.Contains("ram") && verfuegbareTruppen.RammboeckeAnzahl >= template.TruppeEinsMin
			)
				truppeeins = true;
			if (template.TruppeEinsName.Contains("cat") && verfuegbareTruppen.KatapulteAnzahl >= template.TruppeEinsMin)
				truppeeins = true;
			if (template.TruppeEinsName.Contains("knight") && verfuegbareTruppen.PaladinAnzahl >= template.TruppeEinsMin
			)
				truppeeins = true;

			if (template.TruppeZweiName.Contains("spear") &&
			    verfuegbareTruppen.SpeertraegerAnzahl >= template.TruppeZweiMin)
				truppezwei = true;
			if (template.TruppeZweiName.Contains("sword") &&
			    verfuegbareTruppen.SchwertkaempferAnzahl >= template.TruppeZweiMin)
				truppezwei = true;
			if (template.TruppeZweiName.Contains("axe") &&
			    verfuegbareTruppen.AxtkaempferAnzahl >= template.TruppeZweiMin)
				truppezwei = true;
			if (template.TruppeZweiName.Contains("archer") &&
			    verfuegbareTruppen.BogenschuetzenAnzahl >= template.TruppeZweiMin)
				truppezwei = true;
			if (template.TruppeZweiName.Contains("spy") && verfuegbareTruppen.SpaeherAnzahl >= template.TruppeZweiMin)
				truppezwei = true;
			if (template.TruppeZweiName.Contains("light") &&
			    verfuegbareTruppen.LeichteKavallerieAnzahl >= template.TruppeZweiMin)
				truppezwei = true;
			if (template.TruppeZweiName.Contains("marcher") &&
			    verfuegbareTruppen.BeritteneBogenschuetzenAnzahl >= template.TruppeZweiMin)
				truppezwei = true;
			if (template.TruppeZweiName.Contains("heavy") &&
			    verfuegbareTruppen.SchwereKavallerieAnzahl >= template.TruppeZweiMin)
				truppezwei = true;
			if (template.TruppeZweiName.Contains("ram") && verfuegbareTruppen.RammboeckeAnzahl >= template.TruppeZweiMin
			)
				truppezwei = true;
			if (template.TruppeZweiName.Contains("cat") && verfuegbareTruppen.KatapulteAnzahl >= template.TruppeZweiMin)
				truppezwei = true;
			if (template.TruppeZweiName.Contains("knight") && verfuegbareTruppen.PaladinAnzahl >= template.TruppeZweiMin
			)
				truppezwei = true;
			if (string.IsNullOrEmpty(template.TruppeZweiName)) truppezwei = true;
			return truppeeins && truppezwei;
		}

		public static TruppenTemplate GetZuversendendeTruppen(Accountsetting accountsettings
		                                                      , AccountDorfSettings dorf
		                                                      , TruppenTemplate verfuegbareTruppen
		                                                      , FarmtargetViewModel farmtarget
		                                                      , List<FarmTemplate> paare
		                                                      , Dorf dorfdorf
		                                                      , int spielerpunkte) {
			var now = DateTime.Now;


			foreach (var paar in paare)
				if (ValidPaar(paar, verfuegbareTruppen)) {
					var vorausgesehen = new TruppenTemplate();
					TimeSpan laufzeit;
					if (farmtarget.Dorf.VillagePoints > 100 && farmtarget.Gebaeude.Hauptgebaeude == 0 &&
					    verfuegbareTruppen.SpaeherAnzahl > 0 && dorf.FarmTruppenTemplate.SpaeherActive &&
					    (farmtarget.Dorf.PlayerId.Equals(0) && dorf.FarmTruppenTemplate.SpaeherAgainstBarbsActive ||
					     !farmtarget.Dorf.PlayerId.Equals(0) && dorf.FarmTruppenTemplate.SpaeherAgainstPlayerActive) &&
					    farmtarget.AnzahlAngriffe == 0)
						vorausgesehen.SpaeherAnzahl =
							farmtarget.Dorf.PlayerId.Equals(0) ? 1 : dorf.FarmTruppenTemplate.SpaeherAnzahl;
					else {
						if (paar.TruppeEinsName.Contains("spear") &&
						    (farmtarget.Gebaeude.Wall <= 0 || dorf.FarmTruppenTemplate.SpeertraegerAgainstWallActive) &&
						    (farmtarget.Dorf.PlayerId.Equals(0) &&
						     dorf.FarmTruppenTemplate.SpeertraegerAgainstBarbsActive ||
						     !farmtarget.Dorf.PlayerId.Equals(0) &&
						     dorf.FarmTruppenTemplate.SpeertraegerAgainstPlayerActive)) {
							vorausgesehen.SpeertraegerAnzahl = paar.TruppeEinsMin;
							if (!farmtarget.Attacks.Any() && farmtarget.Dorf.VillagePoints > 100 &&
							    verfuegbareTruppen.SpeertraegerAnzahl >= paar.TruppeEinsMin * 4)
								vorausgesehen.SpeertraegerAnzahl = paar.TruppeEinsMin * 4;
						}

						if (paar.TruppeEinsName.Contains("sword") &&
						    (farmtarget.Gebaeude.Wall <= 0 ||
						     dorf.FarmTruppenTemplate.SchwertkaempferAgainstWallActive) &&
						    (farmtarget.Dorf.PlayerId.Equals(0) &&
						     dorf.FarmTruppenTemplate.SchwertkaempferAgainstBarbsActive ||
						     !farmtarget.Dorf.PlayerId.Equals(0) &&
						     dorf.FarmTruppenTemplate.SchwertkaempferAgainstPlayerActive)) {
							vorausgesehen.SchwertkaempferAnzahl = paar.TruppeEinsMin;
							if (!farmtarget.Attacks.Any() && farmtarget.Dorf.VillagePoints > 100 &&
							    verfuegbareTruppen.SchwertkaempferAnzahl >= paar.TruppeEinsMin * 4)
								vorausgesehen.SchwertkaempferAnzahl = paar.TruppeEinsMin * 4;
						}

						if (paar.TruppeEinsName.Contains("axe") &&
						    (farmtarget.Gebaeude.Wall <= 0 || dorf.FarmTruppenTemplate.AxtkaempferAgainstWallActive) &&
						    (farmtarget.Dorf.PlayerId.Equals(0) &&
						     dorf.FarmTruppenTemplate.AxtkaempferAgainstBarbsActive ||
						     !farmtarget.Dorf.PlayerId.Equals(0) &&
						     dorf.FarmTruppenTemplate.AxtkaempferAgainstPlayerActive)) {
							vorausgesehen.AxtkaempferAnzahl = paar.TruppeEinsMin;
							switch (farmtarget.Gebaeude.Wall) {
								case 0:
									break;
								case 1:
									vorausgesehen.AxtkaempferAnzahl =
										verfuegbareTruppen.AxtkaempferAnzahl >= 7 && vorausgesehen.AxtkaempferAnzahl < 7
											? 7
											: 0;
									break;
								case 2:
									vorausgesehen.AxtkaempferAnzahl =
										verfuegbareTruppen.AxtkaempferAnzahl >= 51 &&
										vorausgesehen.AxtkaempferAnzahl < 51
											? 51
											: 0;
									break;
								case 3:
									vorausgesehen.AxtkaempferAnzahl =
										verfuegbareTruppen.AxtkaempferAnzahl >= 140 &&
										vorausgesehen.AxtkaempferAnzahl < 140
											? 140
											: 0;
									break;
								case 4:
									vorausgesehen.AxtkaempferAnzahl =
										verfuegbareTruppen.AxtkaempferAnzahl >= 273 &&
										vorausgesehen.AxtkaempferAnzahl < 273
											? 273
											: 0;
									break;
								case 5:
									vorausgesehen.AxtkaempferAnzahl =
										verfuegbareTruppen.AxtkaempferAnzahl >= 460 &&
										vorausgesehen.AxtkaempferAnzahl < 460
											? 460
											: 0;
									break;
								case 6:
									vorausgesehen.AxtkaempferAnzahl =
										verfuegbareTruppen.AxtkaempferAnzahl >= 672 &&
										vorausgesehen.AxtkaempferAnzahl < 672
											? 672
											: 0;
									break;
								default:
									vorausgesehen.AxtkaempferAnzahl =
										verfuegbareTruppen.AxtkaempferAnzahl >= 700 &&
										vorausgesehen.AxtkaempferAnzahl < 700 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 7000
											? 700
											: 0;
									break;
							}
						}

						if (paar.TruppeEinsName.Contains("archer") &&
						    (farmtarget.Gebaeude.Wall <= 0 ||
						     dorf.FarmTruppenTemplate.BogenschuetzenAgainstWallActive) &&
						    (farmtarget.Dorf.PlayerId.Equals(0) &&
						     dorf.FarmTruppenTemplate.BogenschuetzenAgainstBarbsActive ||
						     !farmtarget.Dorf.PlayerId.Equals(0) &&
						     dorf.FarmTruppenTemplate.BogenschuetzenAgainstPlayerActive)) {
							vorausgesehen.BogenschuetzenAnzahl = paar.TruppeEinsMin;
							if (!farmtarget.Attacks.Any() && farmtarget.Dorf.VillagePoints > 100 &&
							    verfuegbareTruppen.BogenschuetzenAnzahl >= paar.TruppeEinsMin * 4)
								vorausgesehen.BogenschuetzenAnzahl = paar.TruppeEinsMin * 4;
						}

						if (paar.TruppeEinsName.Contains("spy") &&
						    (farmtarget.Gebaeude.Wall <= 0 || dorf.FarmTruppenTemplate.SpaeherAgainstWallActive))
							vorausgesehen.SpaeherAnzahl = paar.TruppeEinsMin;
						if (paar.TruppeEinsName.Contains("light") &&
						    (farmtarget.Gebaeude.Wall <= 0 ||
						     dorf.FarmTruppenTemplate.LeichteKavallerieAgainstWallActive) &&
						    (farmtarget.Dorf.PlayerId.Equals(0) &&
						     dorf.FarmTruppenTemplate.LeichteKavallerieAgainstBarbsActive ||
						     !farmtarget.Dorf.PlayerId.Equals(0) &&
						     dorf.FarmTruppenTemplate.LeichteKavallerieAgainstPlayerActive)) {
							vorausgesehen.LeichteKavallerieAnzahl = paar.TruppeEinsMin;
							switch (farmtarget.Gebaeude.Wall) {
								case 0:
									break;
								case 1:
									vorausgesehen.LeichteKavallerieAnzahl =
										verfuegbareTruppen.LeichteKavallerieAnzahl >= 4 &&
										vorausgesehen.LeichteKavallerieAnzahl < 4
											? 4
											: 0;
									break;
								case 2:
									vorausgesehen.LeichteKavallerieAnzahl =
										verfuegbareTruppen.LeichteKavallerieAnzahl >= 14 &&
										vorausgesehen.LeichteKavallerieAnzahl < 14 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 1120
											? 14
											: 0;
									break;
								case 3:
									vorausgesehen.LeichteKavallerieAnzahl =
										verfuegbareTruppen.LeichteKavallerieAnzahl >= 20 &&
										vorausgesehen.LeichteKavallerieAnzahl < 20 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 1600
											? 20
											: 0;
									break;
								case 4:
									vorausgesehen.LeichteKavallerieAnzahl =
										verfuegbareTruppen.LeichteKavallerieAnzahl >= 20 &&
										vorausgesehen.LeichteKavallerieAnzahl < 20 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 1600
											? 20
											: 0;
									break;
								case 5:
									vorausgesehen.LeichteKavallerieAnzahl =
										verfuegbareTruppen.LeichteKavallerieAnzahl >= 20 &&
										vorausgesehen.LeichteKavallerieAnzahl < 20 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 1600
											? 20
											: 0;
									break;
								case 6:
									vorausgesehen.LeichteKavallerieAnzahl =
										verfuegbareTruppen.LeichteKavallerieAnzahl >= 20 &&
										vorausgesehen.LeichteKavallerieAnzahl < 20 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 1600
											? 20
											: 0;
									break;
								case 7:
									vorausgesehen.LeichteKavallerieAnzahl =
										verfuegbareTruppen.LeichteKavallerieAnzahl >= 28 &&
										vorausgesehen.LeichteKavallerieAnzahl < 28 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 2240
											? 28
											: 0;
									break;
								case 8:
									vorausgesehen.LeichteKavallerieAnzahl =
										verfuegbareTruppen.LeichteKavallerieAnzahl >= 36 &&
										vorausgesehen.LeichteKavallerieAnzahl < 36 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 2880
											? 36
											: 0;
									break;
								case 9:
									vorausgesehen.LeichteKavallerieAnzahl =
										verfuegbareTruppen.LeichteKavallerieAnzahl >= 46 &&
										vorausgesehen.LeichteKavallerieAnzahl < 46 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 3680
											? 46
											: 0;
									break;
								default:
									vorausgesehen.LeichteKavallerieAnzahl =
										verfuegbareTruppen.LeichteKavallerieAnzahl >= 40 &&
										vorausgesehen.LeichteKavallerieAnzahl < 40 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 3000
											? 40
											: 0;
									break;
							}
						}

						if (paar.TruppeEinsName.Contains("marcher") &&
						    (farmtarget.Gebaeude.Wall <= 0 ||
						     dorf.FarmTruppenTemplate.BeritteneBogenschuetzenAgainstWallActive) &&
						    (farmtarget.Dorf.PlayerId.Equals(0) &&
						     dorf.FarmTruppenTemplate.BeritteneBogenschuetzenAgainstBarbsActive ||
						     !farmtarget.Dorf.PlayerId.Equals(0) &&
						     dorf.FarmTruppenTemplate.BeritteneBogenschuetzenAgainstPlayerActive)) {
							vorausgesehen.BeritteneBogenschuetzenAnzahl = paar.TruppeEinsMin;
							switch (farmtarget.Gebaeude.Wall) {
								case 0:
									break;
								case 1:
									vorausgesehen.BeritteneBogenschuetzenAnzahl =
										verfuegbareTruppen.BeritteneBogenschuetzenAnzahl >= 4 &&
										vorausgesehen.BeritteneBogenschuetzenAnzahl < 4
											? 4
											: 0;
									break;
								case 2:
									vorausgesehen.BeritteneBogenschuetzenAnzahl =
										verfuegbareTruppen.BeritteneBogenschuetzenAnzahl >= 14 &&
										vorausgesehen.BeritteneBogenschuetzenAnzahl < 14 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 1120
											? 14
											: 0;
									break;
								case 3:
									vorausgesehen.BeritteneBogenschuetzenAnzahl =
										verfuegbareTruppen.BeritteneBogenschuetzenAnzahl >= 20 &&
										vorausgesehen.BeritteneBogenschuetzenAnzahl < 20 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 1600
											? 20
											: 0;
									break;
								case 4:
									vorausgesehen.BeritteneBogenschuetzenAnzahl =
										verfuegbareTruppen.BeritteneBogenschuetzenAnzahl >= 20 &&
										vorausgesehen.BeritteneBogenschuetzenAnzahl < 20 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 1600
											? 20
											: 0;
									break;
								case 5:
									vorausgesehen.BeritteneBogenschuetzenAnzahl =
										verfuegbareTruppen.BeritteneBogenschuetzenAnzahl >= 20 &&
										vorausgesehen.BeritteneBogenschuetzenAnzahl < 20 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 1600
											? 20
											: 0;
									break;
								case 6:
									vorausgesehen.BeritteneBogenschuetzenAnzahl =
										verfuegbareTruppen.BeritteneBogenschuetzenAnzahl >= 20 &&
										vorausgesehen.BeritteneBogenschuetzenAnzahl < 20 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 1600
											? 20
											: 0;
									break;
								case 7:
									vorausgesehen.BeritteneBogenschuetzenAnzahl =
										verfuegbareTruppen.BeritteneBogenschuetzenAnzahl >= 28 &&
										vorausgesehen.BeritteneBogenschuetzenAnzahl < 28 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 2240
											? 28
											: 0;
									break;
								case 8:
									vorausgesehen.BeritteneBogenschuetzenAnzahl =
										verfuegbareTruppen.BeritteneBogenschuetzenAnzahl >= 36 &&
										vorausgesehen.BeritteneBogenschuetzenAnzahl < 36 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 2880
											? 36
											: 0;
									break;
								case 9:
									vorausgesehen.BeritteneBogenschuetzenAnzahl =
										verfuegbareTruppen.BeritteneBogenschuetzenAnzahl >= 46 &&
										vorausgesehen.BeritteneBogenschuetzenAnzahl < 46 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 3680
											? 46
											: 0;
									break;
								default:
									vorausgesehen.BeritteneBogenschuetzenAnzahl =
										verfuegbareTruppen.BeritteneBogenschuetzenAnzahl >= 40 &&
										vorausgesehen.BeritteneBogenschuetzenAnzahl < 40 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 3000
											? 40
											: 0;
									break;
							}
						}

						if (paar.TruppeEinsName.Contains("heavy") &&
						    (farmtarget.Gebaeude.Wall <= 0 ||
						     dorf.FarmTruppenTemplate.SchwereKavallerieAgainstWallActive) &&
						    (farmtarget.Dorf.PlayerId.Equals(0) &&
						     dorf.FarmTruppenTemplate.SchwereKavallerieAgainstBarbsActive ||
						     !farmtarget.Dorf.PlayerId.Equals(0) &&
						     dorf.FarmTruppenTemplate.SchwereKavallerieAgainstPlayerActive)) {
							vorausgesehen.SchwereKavallerieAnzahl = paar.TruppeEinsMin;
							switch (farmtarget.Gebaeude.Wall) {
								case 0:
									break;
								case 1:
									vorausgesehen.SchwereKavallerieAnzahl =
										verfuegbareTruppen.SchwereKavallerieAnzahl >= 4 &&
										vorausgesehen.SchwereKavallerieAnzahl < 4
											? 4
											: 0;
									break;
								case 2:
									vorausgesehen.SchwereKavallerieAnzahl =
										verfuegbareTruppen.SchwereKavallerieAnzahl >= 14 &&
										vorausgesehen.SchwereKavallerieAnzahl < 14 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 1120
											? 14
											: 0;
									break;
								case 3:
									vorausgesehen.SchwereKavallerieAnzahl =
										verfuegbareTruppen.SchwereKavallerieAnzahl >= 20 &&
										vorausgesehen.SchwereKavallerieAnzahl < 20 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 1600
											? 20
											: 0;
									break;
								case 4:
									vorausgesehen.SchwereKavallerieAnzahl =
										verfuegbareTruppen.SchwereKavallerieAnzahl >= 20 &&
										vorausgesehen.SchwereKavallerieAnzahl < 20 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 1600
											? 20
											: 0;
									break;
								case 5:
									vorausgesehen.SchwereKavallerieAnzahl =
										verfuegbareTruppen.SchwereKavallerieAnzahl >= 20 &&
										vorausgesehen.SchwereKavallerieAnzahl < 20 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 1600
											? 20
											: 0;
									break;
								case 6:
									vorausgesehen.SchwereKavallerieAnzahl =
										verfuegbareTruppen.SchwereKavallerieAnzahl >= 20 &&
										vorausgesehen.SchwereKavallerieAnzahl < 20 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 1600
											? 20
											: 0;
									break;
								case 7:
									vorausgesehen.SchwereKavallerieAnzahl =
										verfuegbareTruppen.SchwereKavallerieAnzahl >= 28 &&
										vorausgesehen.SchwereKavallerieAnzahl < 28 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 2240
											? 28
											: 0;
									break;
								case 8:
									vorausgesehen.SchwereKavallerieAnzahl =
										verfuegbareTruppen.SchwereKavallerieAnzahl >= 36 &&
										vorausgesehen.SchwereKavallerieAnzahl < 36 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 2880
											? 36
											: 0;
									break;
								case 9:
									vorausgesehen.SchwereKavallerieAnzahl =
										verfuegbareTruppen.SchwereKavallerieAnzahl >= 46 &&
										vorausgesehen.SchwereKavallerieAnzahl < 46 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 3680
											? 46
											: 0;
									break;
								default:
									vorausgesehen.SchwereKavallerieAnzahl =
										verfuegbareTruppen.SchwereKavallerieAnzahl >= 40 &&
										vorausgesehen.SchwereKavallerieAnzahl < 40 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 3000
											? 40
											: 0;
									break;
							}
						}

						if (paar.TruppeEinsName.Contains("knight") &&
						    (farmtarget.Gebaeude.Wall <= 0 || dorf.FarmTruppenTemplate.PaladinAgainstWallActive) &&
						    (farmtarget.Dorf.PlayerId.Equals(0) && dorf.FarmTruppenTemplate.PaladinAgainstBarbsActive ||
						     !farmtarget.Dorf.PlayerId.Equals(0) && dorf.FarmTruppenTemplate.PaladinAgainstPlayerActive)
						)
							vorausgesehen.PaladinAnzahl = paar.TruppeEinsMin;
						if (paar.TruppeZweiName.Contains("spear") &&
						    (farmtarget.Gebaeude.Wall <= 0 || dorf.FarmTruppenTemplate.SpeertraegerAgainstWallActive) &&
						    (farmtarget.Dorf.PlayerId.Equals(0) &&
						     dorf.FarmTruppenTemplate.SpeertraegerAgainstBarbsActive ||
						     !farmtarget.Dorf.PlayerId.Equals(0) &&
						     dorf.FarmTruppenTemplate.SpeertraegerAgainstPlayerActive)) {
							vorausgesehen.SpeertraegerAnzahl = paar.TruppeZweiMin;
							if (!farmtarget.Attacks.Any() && farmtarget.Dorf.VillagePoints > 100 &&
							    verfuegbareTruppen.SpeertraegerAnzahl >= paar.TruppeZweiMin * 4)
								vorausgesehen.SpeertraegerAnzahl = paar.TruppeZweiMin * 4;
						}

						if (paar.TruppeZweiName.Contains("sword") &&
						    (farmtarget.Gebaeude.Wall <= 0 ||
						     dorf.FarmTruppenTemplate.SchwertkaempferAgainstWallActive) &&
						    (farmtarget.Dorf.PlayerId.Equals(0) &&
						     dorf.FarmTruppenTemplate.SchwertkaempferAgainstBarbsActive ||
						     !farmtarget.Dorf.PlayerId.Equals(0) &&
						     dorf.FarmTruppenTemplate.SchwertkaempferAgainstPlayerActive)) {
							vorausgesehen.SchwertkaempferAnzahl = paar.TruppeZweiMin;
							if (!farmtarget.Attacks.Any() && farmtarget.Dorf.VillagePoints > 100 &&
							    verfuegbareTruppen.SchwertkaempferAnzahl >= paar.TruppeZweiMin * 4)
								vorausgesehen.SchwertkaempferAnzahl = paar.TruppeZweiMin * 4;
						}

						if (paar.TruppeZweiName.Contains("axe") &&
						    (farmtarget.Gebaeude.Wall <= 0 || dorf.FarmTruppenTemplate.AxtkaempferAgainstWallActive) &&
						    (farmtarget.Dorf.PlayerId.Equals(0) &&
						     dorf.FarmTruppenTemplate.AxtkaempferAgainstBarbsActive ||
						     !farmtarget.Dorf.PlayerId.Equals(0) &&
						     dorf.FarmTruppenTemplate.AxtkaempferAgainstPlayerActive)) {
							vorausgesehen.AxtkaempferAnzahl = paar.TruppeZweiMin;
							switch (farmtarget.Gebaeude.Wall) {
								case 0:
									break;
								case 1:
									vorausgesehen.AxtkaempferAnzahl =
										verfuegbareTruppen.AxtkaempferAnzahl >= 7 && vorausgesehen.AxtkaempferAnzahl < 7
											? 7
											: 0;
									break;
								case 2:
									vorausgesehen.AxtkaempferAnzahl =
										verfuegbareTruppen.AxtkaempferAnzahl >= 51 &&
										vorausgesehen.AxtkaempferAnzahl < 51
											? 51
											: 0;
									break;
								case 3:
									vorausgesehen.AxtkaempferAnzahl =
										verfuegbareTruppen.AxtkaempferAnzahl >= 140 &&
										vorausgesehen.AxtkaempferAnzahl < 140
											? 140
											: 0;
									break;
								case 4:
									vorausgesehen.AxtkaempferAnzahl =
										verfuegbareTruppen.AxtkaempferAnzahl >= 273 &&
										vorausgesehen.AxtkaempferAnzahl < 273
											? 273
											: 0;
									break;
								case 5:
									vorausgesehen.AxtkaempferAnzahl =
										verfuegbareTruppen.AxtkaempferAnzahl >= 460 &&
										vorausgesehen.AxtkaempferAnzahl < 460
											? 460
											: 0;
									break;
								case 6:
									vorausgesehen.AxtkaempferAnzahl =
										verfuegbareTruppen.AxtkaempferAnzahl >= 672 &&
										vorausgesehen.AxtkaempferAnzahl < 672
											? 672
											: 0;
									break;
								default:
									vorausgesehen.AxtkaempferAnzahl =
										verfuegbareTruppen.AxtkaempferAnzahl >= 700 &&
										vorausgesehen.AxtkaempferAnzahl < 700 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 7000
											? 700
											: 0;
									break;
							}
						}

						if (paar.TruppeZweiName.Contains("archer") &&
						    (farmtarget.Gebaeude.Wall <= 0 ||
						     dorf.FarmTruppenTemplate.BogenschuetzenAgainstWallActive) &&
						    (farmtarget.Dorf.PlayerId.Equals(0) &&
						     dorf.FarmTruppenTemplate.BogenschuetzenAgainstBarbsActive ||
						     !farmtarget.Dorf.PlayerId.Equals(0) &&
						     dorf.FarmTruppenTemplate.BogenschuetzenAgainstPlayerActive)) {
							vorausgesehen.BogenschuetzenAnzahl = paar.TruppeZweiMin;
							if (!farmtarget.Attacks.Any() && farmtarget.Dorf.VillagePoints > 100 &&
							    verfuegbareTruppen.BogenschuetzenAnzahl >= paar.TruppeZweiMin * 4)
								vorausgesehen.BogenschuetzenAnzahl = paar.TruppeZweiMin * 4;
						}

						if (paar.TruppeZweiName.Contains("spy") &&
						    (farmtarget.Gebaeude.Wall <= 0 || dorf.FarmTruppenTemplate.SpaeherAgainstWallActive))
							vorausgesehen.SpaeherAnzahl = paar.TruppeZweiMin;
						if (paar.TruppeZweiName.Contains("light") &&
						    (farmtarget.Gebaeude.Wall <= 0 ||
						     dorf.FarmTruppenTemplate.LeichteKavallerieAgainstWallActive) &&
						    (farmtarget.Dorf.PlayerId.Equals(0) &&
						     dorf.FarmTruppenTemplate.LeichteKavallerieAgainstBarbsActive ||
						     !farmtarget.Dorf.PlayerId.Equals(0) &&
						     dorf.FarmTruppenTemplate.LeichteKavallerieAgainstPlayerActive)) {
							vorausgesehen.LeichteKavallerieAnzahl = paar.TruppeZweiMin;
							switch (farmtarget.Gebaeude.Wall) {
								case 0:
									break;
								case 1:
									vorausgesehen.LeichteKavallerieAnzahl =
										verfuegbareTruppen.LeichteKavallerieAnzahl >= 4 &&
										vorausgesehen.LeichteKavallerieAnzahl < 4
											? 4
											: 0;
									break;
								case 2:
									vorausgesehen.LeichteKavallerieAnzahl =
										verfuegbareTruppen.LeichteKavallerieAnzahl >= 14 &&
										vorausgesehen.LeichteKavallerieAnzahl < 14 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 1120
											? 14
											: 0;
									break;
								case 3:
									vorausgesehen.LeichteKavallerieAnzahl =
										verfuegbareTruppen.LeichteKavallerieAnzahl >= 20 &&
										vorausgesehen.LeichteKavallerieAnzahl < 20 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 1600
											? 20
											: 0;
									break;
								case 4:
									vorausgesehen.LeichteKavallerieAnzahl =
										verfuegbareTruppen.LeichteKavallerieAnzahl >= 20 &&
										vorausgesehen.LeichteKavallerieAnzahl < 20 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 1600
											? 20
											: 0;
									break;
								case 5:
									vorausgesehen.LeichteKavallerieAnzahl =
										verfuegbareTruppen.LeichteKavallerieAnzahl >= 20 &&
										vorausgesehen.LeichteKavallerieAnzahl < 20 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 1600
											? 20
											: 0;
									break;
								case 6:
									vorausgesehen.LeichteKavallerieAnzahl =
										verfuegbareTruppen.LeichteKavallerieAnzahl >= 20 &&
										vorausgesehen.LeichteKavallerieAnzahl < 20 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 1600
											? 20
											: 0;
									break;
								case 7:
									vorausgesehen.LeichteKavallerieAnzahl =
										verfuegbareTruppen.LeichteKavallerieAnzahl >= 28 &&
										vorausgesehen.LeichteKavallerieAnzahl < 28 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 2240
											? 28
											: 0;
									break;
								case 8:
									vorausgesehen.LeichteKavallerieAnzahl =
										verfuegbareTruppen.LeichteKavallerieAnzahl >= 36 &&
										vorausgesehen.LeichteKavallerieAnzahl < 36 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 2880
											? 36
											: 0;
									break;
								case 9:
									vorausgesehen.LeichteKavallerieAnzahl =
										verfuegbareTruppen.LeichteKavallerieAnzahl >= 46 &&
										vorausgesehen.LeichteKavallerieAnzahl < 46 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 3680
											? 46
											: 0;
									break;
								default:
									vorausgesehen.LeichteKavallerieAnzahl =
										verfuegbareTruppen.LeichteKavallerieAnzahl >= 40 &&
										vorausgesehen.LeichteKavallerieAnzahl < 40 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 3000
											? 40
											: 0;
									break;
							}
						}

						if (paar.TruppeZweiName.Contains("marcher") &&
						    (farmtarget.Gebaeude.Wall <= 0 ||
						     dorf.FarmTruppenTemplate.BeritteneBogenschuetzenAgainstWallActive) &&
						    (farmtarget.Dorf.PlayerId.Equals(0) &&
						     dorf.FarmTruppenTemplate.BeritteneBogenschuetzenAgainstBarbsActive ||
						     !farmtarget.Dorf.PlayerId.Equals(0) &&
						     dorf.FarmTruppenTemplate.BeritteneBogenschuetzenAgainstPlayerActive)) {
							vorausgesehen.BeritteneBogenschuetzenAnzahl = paar.TruppeZweiMin;
							switch (farmtarget.Gebaeude.Wall) {
								case 0:
									break;
								case 1:
									vorausgesehen.BeritteneBogenschuetzenAnzahl =
										verfuegbareTruppen.BeritteneBogenschuetzenAnzahl >= 4 &&
										vorausgesehen.BeritteneBogenschuetzenAnzahl < 4
											? 4
											: 0;
									break;
								case 2:
									vorausgesehen.BeritteneBogenschuetzenAnzahl =
										verfuegbareTruppen.BeritteneBogenschuetzenAnzahl >= 14 &&
										vorausgesehen.BeritteneBogenschuetzenAnzahl < 14 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 1120
											? 14
											: 0;
									break;
								case 3:
									vorausgesehen.BeritteneBogenschuetzenAnzahl =
										verfuegbareTruppen.BeritteneBogenschuetzenAnzahl >= 20 &&
										vorausgesehen.BeritteneBogenschuetzenAnzahl < 20 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 1600
											? 20
											: 0;
									break;
								case 4:
									vorausgesehen.BeritteneBogenschuetzenAnzahl =
										verfuegbareTruppen.BeritteneBogenschuetzenAnzahl >= 20 &&
										vorausgesehen.BeritteneBogenschuetzenAnzahl < 20 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 1600
											? 20
											: 0;
									break;
								case 5:
									vorausgesehen.BeritteneBogenschuetzenAnzahl =
										verfuegbareTruppen.BeritteneBogenschuetzenAnzahl >= 20 &&
										vorausgesehen.BeritteneBogenschuetzenAnzahl < 20 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 1600
											? 20
											: 0;
									break;
								case 6:
									vorausgesehen.BeritteneBogenschuetzenAnzahl =
										verfuegbareTruppen.BeritteneBogenschuetzenAnzahl >= 20 &&
										vorausgesehen.BeritteneBogenschuetzenAnzahl < 20 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 1600
											? 20
											: 0;
									break;
								case 7:
									vorausgesehen.BeritteneBogenschuetzenAnzahl =
										verfuegbareTruppen.BeritteneBogenschuetzenAnzahl >= 28 &&
										vorausgesehen.BeritteneBogenschuetzenAnzahl < 28 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 2240
											? 28
											: 0;
									break;
								case 8:
									vorausgesehen.BeritteneBogenschuetzenAnzahl =
										verfuegbareTruppen.BeritteneBogenschuetzenAnzahl >= 36 &&
										vorausgesehen.BeritteneBogenschuetzenAnzahl < 36 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 2880
											? 36
											: 0;
									break;
								case 9:
									vorausgesehen.BeritteneBogenschuetzenAnzahl =
										verfuegbareTruppen.BeritteneBogenschuetzenAnzahl >= 46 &&
										vorausgesehen.BeritteneBogenschuetzenAnzahl < 46 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 3680
											? 46
											: 0;
									break;
								default:
									vorausgesehen.BeritteneBogenschuetzenAnzahl =
										verfuegbareTruppen.BeritteneBogenschuetzenAnzahl >= 40 &&
										vorausgesehen.BeritteneBogenschuetzenAnzahl < 40 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 3000
											? 40
											: 0;
									break;
							}
						}

						if (paar.TruppeZweiName.Contains("heavy") &&
						    (farmtarget.Gebaeude.Wall <= 0 ||
						     dorf.FarmTruppenTemplate.SchwereKavallerieAgainstWallActive) &&
						    (farmtarget.Dorf.PlayerId.Equals(0) &&
						     dorf.FarmTruppenTemplate.SchwereKavallerieAgainstBarbsActive ||
						     !farmtarget.Dorf.PlayerId.Equals(0) &&
						     dorf.FarmTruppenTemplate.SchwereKavallerieAgainstPlayerActive)) {
							vorausgesehen.SchwereKavallerieAnzahl = paar.TruppeZweiMin;
							switch (farmtarget.Gebaeude.Wall) {
								case 0:
									break;
								case 1:
									vorausgesehen.SchwereKavallerieAnzahl =
										verfuegbareTruppen.SchwereKavallerieAnzahl >= 4 &&
										vorausgesehen.SchwereKavallerieAnzahl < 4
											? 4
											: 0;
									break;
								case 2:
									vorausgesehen.SchwereKavallerieAnzahl =
										verfuegbareTruppen.SchwereKavallerieAnzahl >= 14 &&
										vorausgesehen.SchwereKavallerieAnzahl < 14 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 1120
											? 14
											: 0;
									break;
								case 3:
									vorausgesehen.SchwereKavallerieAnzahl =
										verfuegbareTruppen.SchwereKavallerieAnzahl >= 20 &&
										vorausgesehen.SchwereKavallerieAnzahl < 20 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 1600
											? 20
											: 0;
									break;
								case 4:
									vorausgesehen.SchwereKavallerieAnzahl =
										verfuegbareTruppen.SchwereKavallerieAnzahl >= 20 &&
										vorausgesehen.SchwereKavallerieAnzahl < 20 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 1600
											? 20
											: 0;
									break;
								case 5:
									vorausgesehen.SchwereKavallerieAnzahl =
										verfuegbareTruppen.SchwereKavallerieAnzahl >= 20 &&
										vorausgesehen.SchwereKavallerieAnzahl < 20 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 1600
											? 20
											: 0;
									break;
								case 6:
									vorausgesehen.SchwereKavallerieAnzahl =
										verfuegbareTruppen.SchwereKavallerieAnzahl >= 20 &&
										vorausgesehen.SchwereKavallerieAnzahl < 20 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 1600
											? 20
											: 0;
									break;
								case 7:
									vorausgesehen.SchwereKavallerieAnzahl =
										verfuegbareTruppen.SchwereKavallerieAnzahl >= 28 &&
										vorausgesehen.SchwereKavallerieAnzahl < 28 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 2240
											? 28
											: 0;
									break;
								case 8:
									vorausgesehen.SchwereKavallerieAnzahl =
										verfuegbareTruppen.SchwereKavallerieAnzahl >= 36 &&
										vorausgesehen.SchwereKavallerieAnzahl < 36 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 2880
											? 36
											: 0;
									break;
								case 9:
									vorausgesehen.SchwereKavallerieAnzahl =
										verfuegbareTruppen.SchwereKavallerieAnzahl >= 46 &&
										vorausgesehen.SchwereKavallerieAnzahl < 46 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 3680
											? 46
											: 0;
									break;
								default:
									vorausgesehen.SchwereKavallerieAnzahl =
										verfuegbareTruppen.SchwereKavallerieAnzahl >= 40 &&
										vorausgesehen.SchwereKavallerieAnzahl < 40 &&
										farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt > 3000
											? 40
											: 0;
									break;
							}
						}

						if (paar.TruppeZweiName.Contains("knight") &&
						    (farmtarget.Gebaeude.Wall <= 0 || dorf.FarmTruppenTemplate.PaladinAgainstWallActive) &&
						    (farmtarget.Dorf.PlayerId.Equals(0) && dorf.FarmTruppenTemplate.PaladinAgainstBarbsActive ||
						     !farmtarget.Dorf.PlayerId.Equals(0) && dorf.FarmTruppenTemplate.PaladinAgainstPlayerActive)
						)
							vorausgesehen.PaladinAnzahl = paar.TruppeZweiMin;
						if (App.Berichte.LastOrDefault(x => x.VerteidigerVillageId.Equals(farmtarget.Dorf.VillageId)) !=
						    null && !HasAnyNormalTroopInIt(vorausgesehen)) {
							if (verfuegbareTruppen.SpaeherAnzahl > 0 && dorf.FarmTruppenTemplate.SpaeherActive &&
							    vorausgesehen.SpaeherAnzahl < 1 &&
							    (farmtarget.Dorf.PlayerId.Equals(0) &&
							     dorf.FarmTruppenTemplate.SpaeherAgainstBarbsActive ||
							     !App.Berichte.Any(x => x.VerteidigerVillageId.Equals(farmtarget.Dorf.VillageId) &&
							                            x.AngreiferTruppenAnzahl.SpaeherAnzahl > 0 &&
							                            x.AngreiferTruppenVerluste.SpaeherAnzahl == 0 &&
							                            x.Kampfzeit < DateTime.Now.AddHours(-5)) ||
							     !farmtarget.Dorf.PlayerId.Equals(0) &&
							     dorf.FarmTruppenTemplate.SpaeherAgainstPlayerActive))
								vorausgesehen.SpaeherAnzahl = dorf.FarmTruppenTemplate.SpaeherAnzahl;
						} else if (verfuegbareTruppen.SpaeherAnzahl > dorf.FarmTruppenTemplate.SpaeherAnzahl &&
						           dorf.FarmTruppenTemplate.SpaeherActive &&
						           (farmtarget.Dorf.PlayerId.Equals(0) &&
						            dorf.FarmTruppenTemplate.SpaeherAgainstBarbsActive ||
						            !App.Berichte.Any(x => x.VerteidigerVillageId.Equals(farmtarget.Dorf.VillageId) &&
						                                   x.AngreiferTruppenAnzahl.SpaeherAnzahl > 0 &&
						                                   x.AngreiferTruppenVerluste.SpaeherAnzahl == 0 &&
						                                   x.Kampfzeit < DateTime.Now.AddHours(-5)) ||
						            !farmtarget.Dorf.PlayerId.Equals(0) &&
						            dorf.FarmTruppenTemplate.SpaeherAgainstPlayerActive))
							vorausgesehen.SpaeherAnzahl = dorf.FarmTruppenTemplate.SpaeherAnzahl;

						var farmenTroopsMustBeHomeTo = dorf.FarmenTroopsMustBeHomeTo;
						var farmenTroopsMustBeHomeFrom = dorf.FarmenTroopsMustBeHomeFrom;
						var farmenInactiveTo = dorf.FarmenInactiveTo;
						var farmenInactiveFrom = dorf.FarmenInactiveFrom;
						if (farmtarget.Gebaeude.Wall > 0 && dorf.FarmTruppenTemplate.RammboeckeActive &&
						    verfuegbareTruppen.RammboeckeAnzahl > dorf.FarmTruppenTemplate.RammboeckeAnzahl &&
						    HasAnyNormalTroopInIt(vorausgesehen) &&
						    Laufzeit.GetTravelTime(dorfdorf, farmtarget.Dorf, new TruppenTemplate {KatapulteAnzahl = 1})
						            .TotalMinutes < dorf.FarmTruppenTemplate.RammboeckeMaxLaufzeit &&
						    verfuegbareTruppen.RammboeckeAnzahl > 2)
							if (
								(vorausgesehen.SpeertraegerAnzahl == 0 || vorausgesehen.SpeertraegerAnzahl > 0 &&
								 dorf.FarmTruppenTemplate.SpeertraegerWithRamActive) &&
								(vorausgesehen.SchwertkaempferAnzahl == 0 || vorausgesehen.SchwertkaempferAnzahl > 0 &&
								 dorf.FarmTruppenTemplate.SchwertkaempferWithRamActive) &&
								(vorausgesehen.AxtkaempferAnzahl == 0 || vorausgesehen.AxtkaempferAnzahl > 0 &&
								 dorf.FarmTruppenTemplate.AxtkaempferWithRamActive) &&
								(vorausgesehen.BogenschuetzenAnzahl == 0 || vorausgesehen.BogenschuetzenAnzahl > 0 &&
								 dorf.FarmTruppenTemplate.BogenschuetzenWithRamActive) &&
								(vorausgesehen.LeichteKavallerieAnzahl == 0 ||
								 vorausgesehen.LeichteKavallerieAnzahl > 0 &&
								 dorf.FarmTruppenTemplate.LeichteKavallerieWithRamActive) &&
								(vorausgesehen.BeritteneBogenschuetzenAnzahl == 0 ||
								 vorausgesehen.BeritteneBogenschuetzenAnzahl > 0 &&
								 dorf.FarmTruppenTemplate.BeritteneBogenschuetzenWithRamActive) &&
								(vorausgesehen.SchwereKavallerieAnzahl == 0 ||
								 vorausgesehen.SchwereKavallerieAnzahl > 0 &&
								 dorf.FarmTruppenTemplate.SchwereKavallerieWithRamActive)) {
								laufzeit = Laufzeit.GetTravelTime(dorfdorf, farmtarget.Dorf
								                                  , new TruppenTemplate {RammboeckeAnzahl = 1});
								if ((farmenTroopsMustBeHomeTo < now && farmenTroopsMustBeHomeFrom < now ||
								     (now.Add(laufzeit).Add(laufzeit) < farmenTroopsMustBeHomeFrom &&
								      now.Add(laufzeit) < farmenTroopsMustBeHomeFrom ||
								      now.Add(laufzeit).Add(laufzeit) > farmenTroopsMustBeHomeTo &&
								      now.Add(laufzeit) > farmenTroopsMustBeHomeTo) && now < farmenTroopsMustBeHomeTo &&
								     now < farmenTroopsMustBeHomeTo ||
								     now > farmenInactiveFrom && now > farmenInactiveTo) &&
								    (farmenInactiveTo < now && farmenInactiveFrom < now || now < farmenInactiveFrom ||
								     now > farmenInactiveTo) && laufzeit.TotalMinutes <
								    GetLowestUnitMaxTravelMinutes(dorf, vorausgesehen)) {
									var rammeamount = RAMMECONSTANTS.GetAmountForWall(farmtarget.Gebaeude.Wall);
									if (rammeamount > verfuegbareTruppen.RammboeckeAnzahl)
										rammeamount = verfuegbareTruppen.RammboeckeAnzahl;
									vorausgesehen.RammboeckeAnzahl = rammeamount;
									farmtarget.Gebaeude.Wall = 0;
								}
							}

						if ((farmtarget.Gebaeude.Bauernhof > 1 || farmtarget.Gebaeude.Hauptgebaeude > 1 ||
						     farmtarget.Gebaeude.Kaserne > 0 || farmtarget.Gebaeude.Stall > 0 ||
						     farmtarget.Gebaeude.Marktplatz > 0 || farmtarget.Gebaeude.Werkstatt > 0) &&
						    dorf.KatternActive &&
						    verfuegbareTruppen.KatapulteAnzahl > dorf.FarmTruppenTemplate.KatapulteAnzahl &&
						    dorf.FarmTruppenTemplate.KatapulteActive && HasAnyNormalTroopInIt(vorausgesehen) &&
						    dorf.KatternActive &&
						    Laufzeit.GetTravelTime(dorfdorf, farmtarget.Dorf, new TruppenTemplate {KatapulteAnzahl = 1})
						            .TotalMinutes < dorf.FarmTruppenTemplate.KatapulteMaxLaufzeit &&
						    (farmtarget.Dorf.PlayerId.Equals(0) || !farmtarget.Dorf.PlayerId.Equals(0) &&
						     farmtarget.Dorf.VillagePoints > Convert.ToDouble(spielerpunkte) / 20 + 100))
							if (
								(vorausgesehen.SpeertraegerAnzahl == 0 || vorausgesehen.SpeertraegerAnzahl > 0 &&
								 dorf.FarmTruppenTemplate.SpeertraegerWithCatActive) &&
								(vorausgesehen.SchwertkaempferAnzahl == 0 || vorausgesehen.SchwertkaempferAnzahl > 0 &&
								 dorf.FarmTruppenTemplate.SchwertkaempferWithCatActive) &&
								(vorausgesehen.AxtkaempferAnzahl == 0 || vorausgesehen.AxtkaempferAnzahl > 0 &&
								 dorf.FarmTruppenTemplate.AxtkaempferWithCatActive) &&
								(vorausgesehen.BogenschuetzenAnzahl == 0 || vorausgesehen.BogenschuetzenAnzahl > 0 &&
								 dorf.FarmTruppenTemplate.BogenschuetzenWithCatActive) &&
								(vorausgesehen.LeichteKavallerieAnzahl == 0 ||
								 vorausgesehen.LeichteKavallerieAnzahl > 0 &&
								 dorf.FarmTruppenTemplate.LeichteKavallerieWithCatActive) &&
								(vorausgesehen.BeritteneBogenschuetzenAnzahl == 0 ||
								 vorausgesehen.BeritteneBogenschuetzenAnzahl > 0 &&
								 dorf.FarmTruppenTemplate.BeritteneBogenschuetzenWithCatActive) &&
								(vorausgesehen.SchwereKavallerieAnzahl == 0 ||
								 vorausgesehen.SchwereKavallerieAnzahl > 0 &&
								 dorf.FarmTruppenTemplate.SchwereKavallerieWithCatActive)) {
								laufzeit = Laufzeit.GetTravelTime(dorfdorf, farmtarget.Dorf
								                                  , new TruppenTemplate {RammboeckeAnzahl = 1});
								if ((farmenTroopsMustBeHomeTo < now && farmenTroopsMustBeHomeFrom < now ||
								     (now.Add(laufzeit).Add(laufzeit) < farmenTroopsMustBeHomeFrom &&
								      now.Add(laufzeit) < farmenTroopsMustBeHomeFrom ||
								      now.Add(laufzeit).Add(laufzeit) > farmenTroopsMustBeHomeTo &&
								      now.Add(laufzeit) > farmenTroopsMustBeHomeTo) && now < farmenTroopsMustBeHomeTo &&
								     now < farmenTroopsMustBeHomeTo ||
								     now > farmenInactiveFrom && now > farmenInactiveTo) &&
								    (farmenInactiveTo < now && farmenInactiveFrom < now || now < farmenInactiveFrom ||
								     now > farmenInactiveTo) && laufzeit.TotalMinutes <
								    GetLowestUnitMaxTravelMinutes(dorf, vorausgesehen))
									if (farmtarget.Gebaeude.Bauernhof > 1 && !farmtarget.Dorf.PlayerId.Equals(0))
										while (vorausgesehen.KatapulteAnzahl + 4 <=
										       verfuegbareTruppen.KatapulteAnzahl &&
										       farmtarget.Gebaeude.Bauernhof > 1) {
											vorausgesehen.KatapulteAnzahl += 4;
											vorausgesehen.TemplateName = "farm";
											farmtarget.Gebaeude.Bauernhof -= 1;
										}
									else if (farmtarget.Gebaeude.Hauptgebaeude > 1)
										while (vorausgesehen.KatapulteAnzahl + 4 <=
										       verfuegbareTruppen.KatapulteAnzahl &&
										       farmtarget.Gebaeude.Hauptgebaeude > 1) {
											vorausgesehen.KatapulteAnzahl += 4;
											vorausgesehen.TemplateName = "main";
											farmtarget.Gebaeude.Hauptgebaeude -= 1;
										}
									else if (farmtarget.Gebaeude.Kaserne > 0)
										while (vorausgesehen.KatapulteAnzahl + 4 <=
										       verfuegbareTruppen.KatapulteAnzahl && farmtarget.Gebaeude.Kaserne > 0) {
											vorausgesehen.KatapulteAnzahl += 4;
											vorausgesehen.TemplateName = "barracks";
											farmtarget.Gebaeude.Kaserne -= 1;
										}
									else if (farmtarget.Gebaeude.Schmiede > 0)
										while (vorausgesehen.KatapulteAnzahl + 4 <=
										       verfuegbareTruppen.KatapulteAnzahl && farmtarget.Gebaeude.Schmiede > 0) {
											vorausgesehen.KatapulteAnzahl += 4;
											vorausgesehen.TemplateName = "smith";
											farmtarget.Gebaeude.Schmiede -= 1;
										}
									else if (farmtarget.Gebaeude.Stall > 0)
										while (vorausgesehen.KatapulteAnzahl + 4 <=
										       verfuegbareTruppen.KatapulteAnzahl && farmtarget.Gebaeude.Stall > 0) {
											vorausgesehen.KatapulteAnzahl += 4;
											vorausgesehen.TemplateName = "stable";
											farmtarget.Gebaeude.Stall -= 1;
										}
									else if (farmtarget.Gebaeude.Marktplatz > 0)
										while (vorausgesehen.KatapulteAnzahl + 4 <=
										       verfuegbareTruppen.KatapulteAnzahl &&
										       farmtarget.Gebaeude.Marktplatz > 0) {
											vorausgesehen.KatapulteAnzahl += 4;
											vorausgesehen.TemplateName = "market";
											farmtarget.Gebaeude.Marktplatz -= 1;
										}
									else if (farmtarget.Gebaeude.Werkstatt > 0)
										while (vorausgesehen.KatapulteAnzahl + 4 <=
										       verfuegbareTruppen.KatapulteAnzahl &&
										       farmtarget.Gebaeude.Werkstatt > 0) {
											vorausgesehen.KatapulteAnzahl += 4;
											vorausgesehen.TemplateName = "workshop";
											farmtarget.Gebaeude.Werkstatt -= 1;
										}
							}

						laufzeit = Laufzeit.GetTravelTime(dorfdorf, farmtarget.Dorf, vorausgesehen);
						if ((farmenTroopsMustBeHomeTo < now && farmenTroopsMustBeHomeFrom < now ||
						     (now.Add(laufzeit).Add(laufzeit) < farmenTroopsMustBeHomeFrom &&
						      now.Add(laufzeit) < farmenTroopsMustBeHomeFrom ||
						      now.Add(laufzeit).Add(laufzeit) > farmenTroopsMustBeHomeTo &&
						      now.Add(laufzeit) > farmenTroopsMustBeHomeTo) && now < farmenTroopsMustBeHomeTo &&
						     now < farmenTroopsMustBeHomeTo || now > farmenInactiveFrom && now > farmenInactiveTo) &&
						    (farmenInactiveTo < now && farmenInactiveFrom < now ||
						     now < farmenInactiveFrom && now > farmenInactiveTo) &&
						    laufzeit.TotalMinutes < GetLowestUnitMaxTravelMinutes(dorf, vorausgesehen) &&
						    HasAnyNormalTroopInIt(vorausgesehen)) {
							var beute = GetGesamteBeuteOfTruppentemplate(vorausgesehen);


							var gesamt = farmtarget.ZuErwarten(dorfdorf, vorausgesehen).Gesamt;
							var weitermachen = true;
							var unterschied = gesamt - beute;
							var counter = 0;
							if (unterschied < 0 && !farmtarget.Spieler.PlayerId.Equals(0)) return null;
							while (unterschied > 100 && weitermachen && counter < 3 && beute > 0) {
								var teilen = string.IsNullOrEmpty(paar.TruppeZweiName) ? 1 : 2;
								counter++;
								if (paar.TruppeEinsName.Contains("spear") &&
								    vorausgesehen.SpeertraegerAnzahl < verfuegbareTruppen.SpeertraegerAnzahl &&
								    (farmtarget.Gebaeude.Wall <= 0 ||
								     dorf.FarmTruppenTemplate.SpeertraegerAgainstWallActive) &&
								    (farmtarget.Dorf.PlayerId.Equals(0) &&
								     dorf.FarmTruppenTemplate.SpeertraegerAgainstBarbsActive ||
								     !farmtarget.Dorf.PlayerId.Equals(0) &&
								     dorf.FarmTruppenTemplate.SpeertraegerAgainstPlayerActive)) {
									var gewollteTruppe = unterschied / TRUPPENCONSTANTS.BEUTE_SPEERTRAEGER / teilen;
									if (gewollteTruppe + vorausgesehen.SpeertraegerAnzahl >
									    verfuegbareTruppen.SpeertraegerAnzahl)
										vorausgesehen.SpeertraegerAnzahl = verfuegbareTruppen.SpeertraegerAnzahl;
									else
										vorausgesehen.SpeertraegerAnzahl += gewollteTruppe;
									weitermachen = true;
									unterschied = gesamt - GetGesamteBeuteOfTruppentemplate(vorausgesehen);
								}

								if (vorausgesehen.SpeertraegerAnzahl >= verfuegbareTruppen.SpeertraegerAnzahl &&
								    verfuegbareTruppen.SpeertraegerAnzahl != 0)
									weitermachen = false;
								if (paar.TruppeEinsName.Contains("sword") &&
								    vorausgesehen.SchwertkaempferAnzahl < verfuegbareTruppen.SchwertkaempferAnzahl &&
								    (farmtarget.Gebaeude.Wall <= 0 ||
								     dorf.FarmTruppenTemplate.SchwertkaempferAgainstWallActive) &&
								    (farmtarget.Dorf.PlayerId.Equals(0) &&
								     dorf.FarmTruppenTemplate.SchwertkaempferAgainstBarbsActive ||
								     !farmtarget.Dorf.PlayerId.Equals(0) &&
								     dorf.FarmTruppenTemplate.SchwertkaempferAgainstPlayerActive)) {
									var gewollteTruppe = unterschied / TRUPPENCONSTANTS.BEUTE_SCHWERTKAEMPFER / teilen;
									if (gewollteTruppe + vorausgesehen.SchwertkaempferAnzahl >
									    verfuegbareTruppen.SchwertkaempferAnzahl)
										vorausgesehen.SchwertkaempferAnzahl = verfuegbareTruppen.SchwertkaempferAnzahl;
									else
										vorausgesehen.SchwertkaempferAnzahl += gewollteTruppe;
									weitermachen = true;
									unterschied = gesamt - GetGesamteBeuteOfTruppentemplate(vorausgesehen);
								}

								if (vorausgesehen.SchwertkaempferAnzahl >= verfuegbareTruppen.SchwertkaempferAnzahl &&
								    verfuegbareTruppen.SchwertkaempferAnzahl != 0)
									weitermachen = false;
								if (paar.TruppeEinsName.Contains("axe") &&
								    vorausgesehen.AxtkaempferAnzahl < verfuegbareTruppen.AxtkaempferAnzahl &&
								    (farmtarget.Gebaeude.Wall <= 0 ||
								     dorf.FarmTruppenTemplate.AxtkaempferAgainstWallActive) &&
								    (farmtarget.Dorf.PlayerId.Equals(0) &&
								     dorf.FarmTruppenTemplate.AxtkaempferAgainstBarbsActive ||
								     !farmtarget.Dorf.PlayerId.Equals(0) &&
								     dorf.FarmTruppenTemplate.AxtkaempferAgainstPlayerActive)) {
									var gewollteTruppe = unterschied / TRUPPENCONSTANTS.BEUTE_AXTKAEMPFER / teilen;
									if (gewollteTruppe + vorausgesehen.AxtkaempferAnzahl >
									    verfuegbareTruppen.AxtkaempferAnzahl)
										vorausgesehen.AxtkaempferAnzahl = verfuegbareTruppen.AxtkaempferAnzahl;
									else
										vorausgesehen.AxtkaempferAnzahl += gewollteTruppe;
									weitermachen = true;
									unterschied = gesamt - GetGesamteBeuteOfTruppentemplate(vorausgesehen);
								}

								if (vorausgesehen.AxtkaempferAnzahl >= verfuegbareTruppen.AxtkaempferAnzahl &&
								    verfuegbareTruppen.AxtkaempferAnzahl != 0)
									weitermachen = false;
								if (paar.TruppeEinsName.Contains("archer") &&
								    vorausgesehen.BogenschuetzenAnzahl < verfuegbareTruppen.BogenschuetzenAnzahl &&
								    (farmtarget.Gebaeude.Wall <= 0 ||
								     dorf.FarmTruppenTemplate.BogenschuetzenAgainstWallActive) &&
								    (farmtarget.Dorf.PlayerId.Equals(0) &&
								     dorf.FarmTruppenTemplate.BogenschuetzenAgainstBarbsActive ||
								     !farmtarget.Dorf.PlayerId.Equals(0) &&
								     dorf.FarmTruppenTemplate.BogenschuetzenAgainstPlayerActive)) {
									var gewollteTruppe = unterschied / TRUPPENCONSTANTS.BEUTE_BOGENSCHUETZEN / teilen;
									if (gewollteTruppe + vorausgesehen.BogenschuetzenAnzahl >
									    verfuegbareTruppen.BogenschuetzenAnzahl)
										vorausgesehen.BogenschuetzenAnzahl = verfuegbareTruppen.BogenschuetzenAnzahl;
									else
										vorausgesehen.BogenschuetzenAnzahl += gewollteTruppe;
									weitermachen = true;
									unterschied = gesamt - GetGesamteBeuteOfTruppentemplate(vorausgesehen);
								}

								if (vorausgesehen.BogenschuetzenAnzahl >= verfuegbareTruppen.BogenschuetzenAnzahl &&
								    verfuegbareTruppen.BogenschuetzenAnzahl != 0)
									weitermachen = false;
								if (paar.TruppeEinsName.Contains("spy") &&
								    vorausgesehen.SpaeherAnzahl < verfuegbareTruppen.SpaeherAnzahl &&
								    (farmtarget.Gebaeude.Wall <= 0 ||
								     dorf.FarmTruppenTemplate.SpaeherAgainstWallActive)) {
									var gewollteTruppe = unterschied / TRUPPENCONSTANTS.BEUTE_SPAEHER / teilen;
									if (gewollteTruppe + vorausgesehen.SpaeherAnzahl > verfuegbareTruppen.SpaeherAnzahl)
										vorausgesehen.SpaeherAnzahl = verfuegbareTruppen.SpaeherAnzahl;
									else
										vorausgesehen.SpaeherAnzahl += gewollteTruppe;
									weitermachen = true;
									unterschied = gesamt - GetGesamteBeuteOfTruppentemplate(vorausgesehen);
								}

								if (vorausgesehen.SpaeherAnzahl >= verfuegbareTruppen.SpaeherAnzahl &&
								    verfuegbareTruppen.SpaeherAnzahl != 0)
									weitermachen = false;
								if (paar.TruppeEinsName.Contains("light") &&
								    vorausgesehen.LeichteKavallerieAnzahl <
								    verfuegbareTruppen.LeichteKavallerieAnzahl &&
								    (farmtarget.Gebaeude.Wall <= 0 ||
								     dorf.FarmTruppenTemplate.LeichteKavallerieAgainstWallActive) &&
								    (farmtarget.Dorf.PlayerId.Equals(0) &&
								     dorf.FarmTruppenTemplate.LeichteKavallerieAgainstBarbsActive ||
								     !farmtarget.Dorf.PlayerId.Equals(0) &&
								     dorf.FarmTruppenTemplate.LeichteKavallerieAgainstPlayerActive)) {
									var gewollteTruppe =
										unterschied / TRUPPENCONSTANTS.BEUTE_LEICHTEKAVALLERIE / teilen;
									if (gewollteTruppe + vorausgesehen.LeichteKavallerieAnzahl >
									    verfuegbareTruppen.LeichteKavallerieAnzahl)
										vorausgesehen.LeichteKavallerieAnzahl =
											verfuegbareTruppen.LeichteKavallerieAnzahl;
									else
										vorausgesehen.LeichteKavallerieAnzahl += gewollteTruppe;
									weitermachen = true;
									unterschied = gesamt - GetGesamteBeuteOfTruppentemplate(vorausgesehen);
								}

								if (vorausgesehen.LeichteKavallerieAnzahl >=
								    verfuegbareTruppen.LeichteKavallerieAnzahl &&
								    verfuegbareTruppen.LeichteKavallerieAnzahl != 0)
									weitermachen = false;
								if (paar.TruppeEinsName.Contains("marcher") &&
								    vorausgesehen.BeritteneBogenschuetzenAnzahl <
								    verfuegbareTruppen.BeritteneBogenschuetzenAnzahl &&
								    (farmtarget.Gebaeude.Wall <= 0 ||
								     dorf.FarmTruppenTemplate.BeritteneBogenschuetzenAgainstWallActive) &&
								    (farmtarget.Dorf.PlayerId.Equals(0) &&
								     dorf.FarmTruppenTemplate.BeritteneBogenschuetzenAgainstBarbsActive ||
								     !farmtarget.Dorf.PlayerId.Equals(0) &&
								     dorf.FarmTruppenTemplate.BeritteneBogenschuetzenAgainstPlayerActive)) {
									var gewollteTruppe =
										unterschied / TRUPPENCONSTANTS.BEUTE_BERITTENEBOGENSCHUETZEN / teilen;
									if (gewollteTruppe + vorausgesehen.BeritteneBogenschuetzenAnzahl >
									    verfuegbareTruppen.BeritteneBogenschuetzenAnzahl)
										vorausgesehen.BeritteneBogenschuetzenAnzahl =
											verfuegbareTruppen.BeritteneBogenschuetzenAnzahl;
									else
										vorausgesehen.BeritteneBogenschuetzenAnzahl += gewollteTruppe;
									weitermachen = true;
									unterschied = gesamt - GetGesamteBeuteOfTruppentemplate(vorausgesehen);
								}

								if (vorausgesehen.BeritteneBogenschuetzenAnzahl >=
								    verfuegbareTruppen.BeritteneBogenschuetzenAnzahl &&
								    verfuegbareTruppen.BeritteneBogenschuetzenAnzahl != 0)
									weitermachen = false;
								if (paar.TruppeEinsName.Contains("heavy") &&
								    vorausgesehen.SchwereKavallerieAnzahl <
								    verfuegbareTruppen.SchwereKavallerieAnzahl &&
								    (farmtarget.Gebaeude.Wall <= 0 ||
								     dorf.FarmTruppenTemplate.SchwereKavallerieAgainstWallActive) &&
								    (farmtarget.Dorf.PlayerId.Equals(0) &&
								     dorf.FarmTruppenTemplate.SchwereKavallerieAgainstBarbsActive ||
								     !farmtarget.Dorf.PlayerId.Equals(0) &&
								     dorf.FarmTruppenTemplate.SchwereKavallerieAgainstPlayerActive)) {
									var gewollteTruppe =
										unterschied / TRUPPENCONSTANTS.BEUTE_SCHWEREKAVALLERIE / teilen;
									if (gewollteTruppe + vorausgesehen.SchwereKavallerieAnzahl >
									    verfuegbareTruppen.SchwereKavallerieAnzahl)
										vorausgesehen.SchwereKavallerieAnzahl =
											verfuegbareTruppen.SchwereKavallerieAnzahl;
									else
										vorausgesehen.SchwereKavallerieAnzahl += gewollteTruppe;
									weitermachen = true;
									unterschied = gesamt - GetGesamteBeuteOfTruppentemplate(vorausgesehen);
								}

								if (vorausgesehen.SchwereKavallerieAnzahl >=
								    verfuegbareTruppen.SchwereKavallerieAnzahl &&
								    verfuegbareTruppen.SchwereKavallerieAnzahl != 0)
									weitermachen = false;


								if (paar.TruppeZweiName.Contains("spear") &&
								    vorausgesehen.SpeertraegerAnzahl < verfuegbareTruppen.SpeertraegerAnzahl &&
								    (farmtarget.Gebaeude.Wall <= 0 ||
								     dorf.FarmTruppenTemplate.SpeertraegerAgainstWallActive) &&
								    (farmtarget.Dorf.PlayerId.Equals(0) &&
								     dorf.FarmTruppenTemplate.SpeertraegerAgainstBarbsActive ||
								     !farmtarget.Dorf.PlayerId.Equals(0) &&
								     dorf.FarmTruppenTemplate.SpeertraegerAgainstPlayerActive)) {
									var gewollteTruppe = unterschied / TRUPPENCONSTANTS.BEUTE_SPEERTRAEGER / teilen;
									if (gewollteTruppe + vorausgesehen.SpeertraegerAnzahl >
									    verfuegbareTruppen.SpeertraegerAnzahl)
										vorausgesehen.SpeertraegerAnzahl = verfuegbareTruppen.SpeertraegerAnzahl;
									else
										vorausgesehen.SpeertraegerAnzahl += gewollteTruppe;
									weitermachen = true;
									unterschied = gesamt - GetGesamteBeuteOfTruppentemplate(vorausgesehen);
								}

								if (vorausgesehen.SpeertraegerAnzahl >= verfuegbareTruppen.SpeertraegerAnzahl &&
								    verfuegbareTruppen.SpeertraegerAnzahl != 0)
									weitermachen = false;
								if (paar.TruppeZweiName.Contains("sword") &&
								    vorausgesehen.SchwertkaempferAnzahl < verfuegbareTruppen.SchwertkaempferAnzahl &&
								    (farmtarget.Gebaeude.Wall <= 0 ||
								     dorf.FarmTruppenTemplate.SchwertkaempferAgainstWallActive) &&
								    (farmtarget.Dorf.PlayerId.Equals(0) &&
								     dorf.FarmTruppenTemplate.SchwertkaempferAgainstBarbsActive ||
								     !farmtarget.Dorf.PlayerId.Equals(0) &&
								     dorf.FarmTruppenTemplate.SchwertkaempferAgainstPlayerActive)) {
									var gewollteTruppe = unterschied / TRUPPENCONSTANTS.BEUTE_SCHWERTKAEMPFER / teilen;
									if (gewollteTruppe + vorausgesehen.SchwertkaempferAnzahl >
									    verfuegbareTruppen.SchwertkaempferAnzahl)
										vorausgesehen.SchwertkaempferAnzahl = verfuegbareTruppen.SchwertkaempferAnzahl;
									else
										vorausgesehen.SchwertkaempferAnzahl += gewollteTruppe;
									weitermachen = true;
									unterschied = gesamt - GetGesamteBeuteOfTruppentemplate(vorausgesehen);
								}

								if (vorausgesehen.SchwertkaempferAnzahl >= verfuegbareTruppen.SchwertkaempferAnzahl &&
								    verfuegbareTruppen.SchwertkaempferAnzahl != 0)
									weitermachen = false;
								if (paar.TruppeZweiName.Contains("axe") &&
								    vorausgesehen.AxtkaempferAnzahl < verfuegbareTruppen.AxtkaempferAnzahl &&
								    (farmtarget.Gebaeude.Wall <= 0 ||
								     dorf.FarmTruppenTemplate.AxtkaempferAgainstWallActive) &&
								    (farmtarget.Dorf.PlayerId.Equals(0) &&
								     dorf.FarmTruppenTemplate.AxtkaempferAgainstBarbsActive ||
								     !farmtarget.Dorf.PlayerId.Equals(0) &&
								     dorf.FarmTruppenTemplate.AxtkaempferAgainstPlayerActive)) {
									var gewollteTruppe = unterschied / TRUPPENCONSTANTS.BEUTE_AXTKAEMPFER / teilen;
									if (gewollteTruppe + vorausgesehen.AxtkaempferAnzahl >
									    verfuegbareTruppen.AxtkaempferAnzahl)
										vorausgesehen.AxtkaempferAnzahl = verfuegbareTruppen.AxtkaempferAnzahl;
									else
										vorausgesehen.AxtkaempferAnzahl += gewollteTruppe;
									weitermachen = true;
									unterschied = gesamt - GetGesamteBeuteOfTruppentemplate(vorausgesehen);
								}

								if (vorausgesehen.AxtkaempferAnzahl >= verfuegbareTruppen.AxtkaempferAnzahl)
									weitermachen = false;
								if (paar.TruppeZweiName.Contains("archer") &&
								    vorausgesehen.BogenschuetzenAnzahl < verfuegbareTruppen.BogenschuetzenAnzahl &&
								    (farmtarget.Gebaeude.Wall <= 0 ||
								     dorf.FarmTruppenTemplate.BogenschuetzenAgainstWallActive) &&
								    (farmtarget.Dorf.PlayerId.Equals(0) &&
								     dorf.FarmTruppenTemplate.BogenschuetzenAgainstBarbsActive ||
								     !farmtarget.Dorf.PlayerId.Equals(0) &&
								     dorf.FarmTruppenTemplate.BogenschuetzenAgainstPlayerActive)) {
									var gewollteTruppe = unterschied / TRUPPENCONSTANTS.BEUTE_BOGENSCHUETZEN / teilen;
									if (gewollteTruppe + vorausgesehen.BogenschuetzenAnzahl >
									    verfuegbareTruppen.BogenschuetzenAnzahl)
										vorausgesehen.BogenschuetzenAnzahl = verfuegbareTruppen.BogenschuetzenAnzahl;
									else
										vorausgesehen.BogenschuetzenAnzahl += gewollteTruppe;
									weitermachen = true;
									unterschied = gesamt - GetGesamteBeuteOfTruppentemplate(vorausgesehen);
								}

								if (vorausgesehen.BogenschuetzenAnzahl >= verfuegbareTruppen.BogenschuetzenAnzahl &&
								    verfuegbareTruppen.BogenschuetzenAnzahl != 0)
									weitermachen = false;
								if (paar.TruppeZweiName.Contains("light") &&
								    vorausgesehen.LeichteKavallerieAnzahl <
								    verfuegbareTruppen.LeichteKavallerieAnzahl &&
								    (farmtarget.Gebaeude.Wall <= 0 ||
								     dorf.FarmTruppenTemplate.LeichteKavallerieAgainstWallActive) &&
								    (farmtarget.Dorf.PlayerId.Equals(0) &&
								     dorf.FarmTruppenTemplate.LeichteKavallerieAgainstBarbsActive ||
								     !farmtarget.Dorf.PlayerId.Equals(0) &&
								     dorf.FarmTruppenTemplate.LeichteKavallerieAgainstPlayerActive)) {
									var gewollteTruppe =
										unterschied / TRUPPENCONSTANTS.BEUTE_LEICHTEKAVALLERIE / teilen;
									if (gewollteTruppe + vorausgesehen.LeichteKavallerieAnzahl >
									    verfuegbareTruppen.LeichteKavallerieAnzahl)
										vorausgesehen.LeichteKavallerieAnzahl =
											verfuegbareTruppen.LeichteKavallerieAnzahl;
									else
										vorausgesehen.LeichteKavallerieAnzahl += gewollteTruppe;
									weitermachen = true;
									unterschied = gesamt - GetGesamteBeuteOfTruppentemplate(vorausgesehen);
								}

								if (vorausgesehen.LeichteKavallerieAnzahl >=
								    verfuegbareTruppen.LeichteKavallerieAnzahl &&
								    verfuegbareTruppen.LeichteKavallerieAnzahl != 0)
									weitermachen = false;
								if (paar.TruppeZweiName.Contains("marcher") &&
								    vorausgesehen.BeritteneBogenschuetzenAnzahl <
								    verfuegbareTruppen.BeritteneBogenschuetzenAnzahl &&
								    (farmtarget.Gebaeude.Wall <= 0 ||
								     dorf.FarmTruppenTemplate.BeritteneBogenschuetzenAgainstWallActive) &&
								    (farmtarget.Dorf.PlayerId.Equals(0) &&
								     dorf.FarmTruppenTemplate.BeritteneBogenschuetzenAgainstBarbsActive ||
								     !farmtarget.Dorf.PlayerId.Equals(0) &&
								     dorf.FarmTruppenTemplate.BeritteneBogenschuetzenAgainstPlayerActive)) {
									var gewollteTruppe =
										unterschied / TRUPPENCONSTANTS.BEUTE_BERITTENEBOGENSCHUETZEN / teilen;
									if (gewollteTruppe + vorausgesehen.BeritteneBogenschuetzenAnzahl >
									    verfuegbareTruppen.BeritteneBogenschuetzenAnzahl)
										vorausgesehen.BeritteneBogenschuetzenAnzahl =
											verfuegbareTruppen.BeritteneBogenschuetzenAnzahl;
									else
										vorausgesehen.BeritteneBogenschuetzenAnzahl += gewollteTruppe;
									weitermachen = true;
									unterschied = gesamt - GetGesamteBeuteOfTruppentemplate(vorausgesehen);
								}

								if (vorausgesehen.BeritteneBogenschuetzenAnzahl >=
								    verfuegbareTruppen.BeritteneBogenschuetzenAnzahl &&
								    verfuegbareTruppen.BeritteneBogenschuetzenAnzahl != 0)
									weitermachen = false;
								if (paar.TruppeZweiName.Contains("heavy") &&
								    vorausgesehen.SchwereKavallerieAnzahl <
								    verfuegbareTruppen.SchwereKavallerieAnzahl &&
								    (farmtarget.Gebaeude.Wall <= 0 ||
								     dorf.FarmTruppenTemplate.SchwereKavallerieAgainstWallActive) &&
								    (farmtarget.Dorf.PlayerId.Equals(0) &&
								     dorf.FarmTruppenTemplate.SchwereKavallerieAgainstBarbsActive ||
								     !farmtarget.Dorf.PlayerId.Equals(0) &&
								     dorf.FarmTruppenTemplate.SchwereKavallerieAgainstPlayerActive)) {
									var gewollteTruppe =
										unterschied / TRUPPENCONSTANTS.BEUTE_SCHWEREKAVALLERIE / teilen;
									if (gewollteTruppe + vorausgesehen.SchwereKavallerieAnzahl >
									    verfuegbareTruppen.SchwereKavallerieAnzahl)
										vorausgesehen.SchwereKavallerieAnzahl =
											verfuegbareTruppen.SchwereKavallerieAnzahl;
									else
										vorausgesehen.SchwereKavallerieAnzahl += gewollteTruppe;
									weitermachen = true;
									unterschied = gesamt - GetGesamteBeuteOfTruppentemplate(vorausgesehen);
								}

								if (vorausgesehen.SchwereKavallerieAnzahl >=
								    verfuegbareTruppen.SchwereKavallerieAnzahl &&
								    verfuegbareTruppen.SchwereKavallerieAnzahl != 0)
									weitermachen = false;
								beute = GetGesamteBeuteOfTruppentemplate(vorausgesehen);
                            }

                            if (farmtarget.Dorf.PlayerId.Equals(0) || !farmtarget.Dorf.PlayerId.Equals(0) &&
                                GetBhPlaetzeOfTemplate(vorausgesehen) >=
                                Convert.ToDouble(App.PlayerlistWholeWorld
                                                    .FirstOrDefault(x =>
                                                                        x
                                                                            .PlayerName
                                                                            .Equals(!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname
                                                                                    , StringComparison
                                                                                        .CurrentCultureIgnoreCase))
                                                    .Spieler.PlayerPoints / 100))
                                return vorausgesehen;
                        }
                    }
                }

			if (dorf.FarmTruppenTemplate.SpaeherActive &&
			    verfuegbareTruppen.SpaeherAnzahl > dorf.FarmTruppenTemplate.SpaeherAnzahl && farmtarget.Attacks.Any() &&
			    DateTime.Now.Subtract(farmtarget.Attacks.LastOrDefault().LandTime).TotalHours > 4 &&
			    Laufzeit.GetTravelTime(dorf.Dorf.Dorf, farmtarget.Dorf
			                           , new TruppenTemplate {SpaeherAnzahl = dorf.FarmTruppenTemplate.SpaeherAnzahl})
			            .Minutes < dorf.FarmTruppenTemplate.SpaeherMaxLaufzeit)
				return new TruppenTemplate {SpaeherAnzahl = dorf.FarmTruppenTemplate.SpaeherAnzahl};
			return null;
		}

		public static int GetBhPlaetzeOfTemplate(TruppenTemplate template) {
			var gesamt = 0;
			gesamt += template.SpeertraegerAnzahl * TRUPPENCONSTANTS.BAUERNHOFPLAETZE_SPEERTRAEGER;
			gesamt += template.SchwertkaempferAnzahl * TRUPPENCONSTANTS.BAUERNHOFPLAETZE_SCHWERTKAEMPFER;
			gesamt += template.AxtkaempferAnzahl * TRUPPENCONSTANTS.BAUERNHOFPLAETZE_AXTKAEMPFER;
			gesamt += template.BogenschuetzenAnzahl * TRUPPENCONSTANTS.BAUERNHOFPLAETZE_BOGENSCHUETZEN;
			gesamt += template.SpaeherAnzahl * TRUPPENCONSTANTS.BAUERNHOFPLAETZE_SPAEHER;
			gesamt += template.LeichteKavallerieAnzahl * TRUPPENCONSTANTS.BAUERNHOFPLAETZE_LEICHTEKAVALLERIE;
			gesamt += template.BeritteneBogenschuetzenAnzahl *
			          TRUPPENCONSTANTS.BAUERNHOFPLAETZE_BERITTENEBOGENSCHUETZEN;
			gesamt += template.SchwereKavallerieAnzahl * TRUPPENCONSTANTS.BAUERNHOFPLAETZE_SCHWEREKAVALLERIE;
			gesamt += template.RammboeckeAnzahl * TRUPPENCONSTANTS.BAUERNHOFPLAETZE_RAMMBOECKE;
			gesamt += template.KatapulteAnzahl * TRUPPENCONSTANTS.BAUERNHOFPLAETZE_KATAPULTE;
			gesamt += template.PaladinAnzahl * TRUPPENCONSTANTS.BAUERNHOFPLAETZE_PALADIN;
			gesamt += template.AdelsgeschlechterAnzahl * TRUPPENCONSTANTS.BAUERNHOFPLAETZE_ADELSGESCHLECHT;
			return gesamt;
		}

		public static int GetLowestUnitMaxTravelMinutes(AccountDorfSettings settings, TruppenTemplate template) {
			var minutes = 10000;
			if (template.SpeertraegerAnzahl > 0 && minutes > settings.FarmTruppenTemplate.SpeertraegerMaxLaufzeit)
				minutes = settings.FarmTruppenTemplate.SpeertraegerMaxLaufzeit;
			if (template.SchwertkaempferAnzahl > 0 && minutes > settings.FarmTruppenTemplate.SchwertkaempferMaxLaufzeit)
				minutes = settings.FarmTruppenTemplate.SchwertkaempferMaxLaufzeit;
			if (template.AxtkaempferAnzahl > 0 && minutes > settings.FarmTruppenTemplate.AxtkaempferMaxLaufzeit)
				minutes = settings.FarmTruppenTemplate.AxtkaempferMaxLaufzeit;
			if (template.BogenschuetzenAnzahl > 0 && minutes > settings.FarmTruppenTemplate.BogenschuetzenMaxLaufzeit &&
			    App.Settings.Weltensettings.BogenschuetzenActive)
				minutes = settings.FarmTruppenTemplate.BogenschuetzenMaxLaufzeit;
			if (template.SpaeherAnzahl > 0 && minutes > settings.FarmTruppenTemplate.SpaeherMaxLaufzeit)
				minutes = settings.FarmTruppenTemplate.SpaeherMaxLaufzeit;
			if (template.LeichteKavallerieAnzahl > 0 &&
			    minutes > settings.FarmTruppenTemplate.LeichteKavallerieMaxLaufzeit)
				minutes = settings.FarmTruppenTemplate.LeichteKavallerieMaxLaufzeit;
			if (template.BeritteneBogenschuetzenAnzahl > 0 &&
			    minutes > settings.FarmTruppenTemplate.BeritteneBogenschuetzenMaxLaufzeit &&
			    App.Settings.Weltensettings.BogenschuetzenActive)
				minutes = settings.FarmTruppenTemplate.BeritteneBogenschuetzenMaxLaufzeit;
			if (template.SchwereKavallerieAnzahl > 0 &&
			    minutes > settings.FarmTruppenTemplate.SchwereKavallerieMaxLaufzeit)
				minutes = settings.FarmTruppenTemplate.SchwereKavallerieMaxLaufzeit;
			if (template.RammboeckeAnzahl > 0 && minutes > settings.FarmTruppenTemplate.RammboeckeMaxLaufzeit)
				minutes = settings.FarmTruppenTemplate.RammboeckeMaxLaufzeit;
			if (template.KatapulteAnzahl > 0 && minutes > settings.FarmTruppenTemplate.KatapulteMaxLaufzeit)
				minutes = settings.FarmTruppenTemplate.KatapulteMaxLaufzeit;
			if (template.PaladinAnzahl > 0 && minutes > settings.FarmTruppenTemplate.PaladinMaxLaufzeit)
				minutes = settings.FarmTruppenTemplate.PaladinMaxLaufzeit;
			return minutes;
		}

		public static bool HasAnyNormalTroopInIt(TruppenTemplate template) {
			if (template.SpeertraegerAnzahl > 0) return true;
			if (template.SchwertkaempferAnzahl > 0) return true;
			if (template.AxtkaempferAnzahl > 0) return true;
			if (template.BogenschuetzenAnzahl > 0) return true;
			if (template.LeichteKavallerieAnzahl > 0) return true;
			if (template.BeritteneBogenschuetzenAnzahl > 0) return true;
			if (template.SchwereKavallerieAnzahl > 0) return true;
			if (template.PaladinAnzahl > 0) return true;
			return false;
		}

		public static TruppenTemplate SubstractTemplateFromTemplate(TruppenTemplate from2, TruppenTemplate substract) {
			var from = from2;
			from.SpeertraegerAnzahl -= substract.SpeertraegerAnzahl;
			from.SchwertkaempferAnzahl -= substract.SchwertkaempferAnzahl;
			from.AxtkaempferAnzahl -= substract.AxtkaempferAnzahl;
			from.BogenschuetzenAnzahl -= substract.BogenschuetzenAnzahl;
			from.SpaeherAnzahl -= substract.SpaeherAnzahl;
			from.LeichteKavallerieAnzahl -= substract.LeichteKavallerieAnzahl;
			from.BeritteneBogenschuetzenAnzahl -= substract.BeritteneBogenschuetzenAnzahl;
			from.SchwereKavallerieAnzahl -= substract.SchwereKavallerieAnzahl;
			from.RammboeckeAnzahl -= substract.RammboeckeAnzahl;
			from.KatapulteAnzahl -= substract.KatapulteAnzahl;
			from.PaladinAnzahl -= substract.PaladinAnzahl;
			return from;
		}

		public static bool HasEnoughTroops(TruppenTemplate verfuegbar, TruppenTemplate template) {
			if (template.SpeertraegerAnzahl > 0 && template.SpeertraegerAnzahl > verfuegbar.SpeertraegerAnzahl)
				return false;
			if (template.SchwertkaempferAnzahl > 0 && template.SchwertkaempferAnzahl > verfuegbar.SchwertkaempferAnzahl)
				return false;
			if (template.AxtkaempferAnzahl > 0 && template.AxtkaempferAnzahl > verfuegbar.AxtkaempferAnzahl)
				return false;
			if (template.BogenschuetzenAnzahl > 0 && template.BogenschuetzenAnzahl > verfuegbar.BogenschuetzenAnzahl)
				return false;
			if (template.LeichteKavallerieAnzahl > 0 &&
			    template.LeichteKavallerieAnzahl > verfuegbar.LeichteKavallerieAnzahl)
				return false;
			if (template.BeritteneBogenschuetzenAnzahl > 0 &&
			    template.BeritteneBogenschuetzenAnzahl > verfuegbar.BeritteneBogenschuetzenAnzahl)
				return false;
			if (template.SchwereKavallerieAnzahl > 0 &&
			    template.SchwereKavallerieAnzahl > verfuegbar.SchwereKavallerieAnzahl)
				return false;
			if (template.RammboeckeAnzahl > 0 && template.RammboeckeAnzahl > verfuegbar.RammboeckeAnzahl) return false;
			if (template.KatapulteAnzahl > 0 && template.KatapulteAnzahl > verfuegbar.KatapulteAnzahl) return false;
			if (template.PaladinAnzahl > 0 && template.PaladinAnzahl > verfuegbar.PaladinAnzahl) return false;
			return true;
		}

		public static bool HasEnoughTroopsExact(TruppenTemplate verfuegbar, TruppenTemplate template) {
			if (template.SpeertraegerAnzahl > 0 && verfuegbar.SpeertraegerAnzahl > template.SpeertraegerAnzahl) {
				return true;
			}

			if (template.SchwertkaempferAnzahl > 0 &&
			    verfuegbar.SchwertkaempferAnzahl > template.SchwertkaempferAnzahl) {
				return true;
			}

			if (template.AxtkaempferAnzahl > 0 && verfuegbar.AxtkaempferAnzahl > template.AxtkaempferAnzahl) {
				return true;
			}

			if (template.BogenschuetzenAnzahl > 0 && verfuegbar.BogenschuetzenAnzahl > template.BogenschuetzenAnzahl) {
				return true;
			}

			if (template.SpaeherAnzahl > 0 && verfuegbar.SpaeherAnzahl > template.SpaeherAnzahl) {
				return true;
			}

			if (template.LeichteKavallerieAnzahl > 0 &&
			    verfuegbar.LeichteKavallerieAnzahl > template.LeichteKavallerieAnzahl) {
				return true;
			}

			if (template.BeritteneBogenschuetzenAnzahl > 0 &&
			    verfuegbar.BeritteneBogenschuetzenAnzahl > template.BeritteneBogenschuetzenAnzahl) {
				return true;
			}

			if (template.SchwereKavallerieAnzahl > 0 &&
			    verfuegbar.SchwereKavallerieAnzahl > template.SchwereKavallerieAnzahl) {
				return true;
			}

			if (template.RammboeckeAnzahl > 0 && verfuegbar.RammboeckeAnzahl > template.RammboeckeAnzahl) {
				return true;
			}

			if (template.KatapulteAnzahl > 0 && verfuegbar.KatapulteAnzahl > template.KatapulteAnzahl) {
				return true;
			}

			if (template.PaladinAnzahl > 0 && verfuegbar.PaladinAnzahl > template.PaladinAnzahl) {
				return true;
			}

			return false;
		}

		public static bool HasEnoughTroopsForAnyTemplate(TruppenTemplate verfuegbar, List<TruppenTemplate> templates) {
			foreach (var template in templates) {
				if (HasEnoughTroops(verfuegbar, template)) {
					return true;
				}
			}

			return false;
		}

		public static int GetGesamteBeuteOfTruppentemplate(TruppenTemplate template) {
			var temp = 0;
			temp += template.SpeertraegerAnzahl * TRUPPENCONSTANTS.BEUTE_SPEERTRAEGER;
			temp += template.SchwertkaempferAnzahl * TRUPPENCONSTANTS.BEUTE_SCHWERTKAEMPFER;
			temp += template.AxtkaempferAnzahl * TRUPPENCONSTANTS.BEUTE_AXTKAEMPFER;
			temp += template.BogenschuetzenAnzahl * TRUPPENCONSTANTS.BEUTE_BOGENSCHUETZEN;
			temp += template.SpaeherAnzahl * TRUPPENCONSTANTS.BEUTE_SPAEHER;
			temp += template.LeichteKavallerieAnzahl * TRUPPENCONSTANTS.BEUTE_LEICHTEKAVALLERIE;
			temp += template.BeritteneBogenschuetzenAnzahl * TRUPPENCONSTANTS.BEUTE_BERITTENEBOGENSCHUETZEN;
			temp += template.SchwereKavallerieAnzahl * TRUPPENCONSTANTS.BEUTE_SCHWEREKAVALLERIE;
			temp += template.RammboeckeAnzahl * TRUPPENCONSTANTS.BEUTE_RAMMBOECKE;
			temp += template.KatapulteAnzahl * TRUPPENCONSTANTS.BEUTE_KATAPULTE;
			temp += template.PaladinAnzahl * TRUPPENCONSTANTS.BEUTE_PALADIN;
			return temp;
		}

		public static List<FarmTemplate> CanFarmen(AccountDorfSettings dorf, TruppenTemplate verfuegbareTruppen) {
			var list = new List<FarmTemplate>();
			//Speer
			if (verfuegbareTruppen.SpeertraegerAnzahl > 0 && dorf.FarmTruppenTemplate.SpeertraegerActive) {
				if (verfuegbareTruppen.SpeertraegerAnzahl >= dorf.FarmTruppenTemplate.SpeertraegerAnzahl &&
				    dorf.FarmTruppenTemplate.SpeertraegerAnzahl != 0)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.Speertraeger
						                          , TruppeEinsMax = verfuegbareTruppen.SpeertraegerAnzahl
						                          , TruppeEinsMin = dorf.FarmTruppenTemplate.SpeertraegerAnzahl
						                          , TruppeZweiName = ""
						                          , TruppeZweiMax = 0
						                          , TruppeZweiMin = 0
					                          });

				if (
					verfuegbareTruppen.SpeertraegerAnzahl >=
					dorf.FarmTruppenTemplate.SpeertraegerAnzahlMitSchwertkaempfer &&
					dorf.FarmTruppenTemplate.SpeertraegerAnzahlMitSchwertkaempfer != 0 &&
					verfuegbareTruppen.SchwertkaempferAnzahl >=
					dorf.FarmTruppenTemplate.SchwertkaempferAnzahlMitSpeertraeger &&
					dorf.FarmTruppenTemplate.SchwertkaempferAnzahlMitSpeertraeger != 0 &&
					dorf.FarmTruppenTemplate.SchwertkaempferActive)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.Speertraeger
						                          , TruppeEinsMax = verfuegbareTruppen.SpeertraegerAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate.SpeertraegerAnzahlMitSchwertkaempfer
						                          , TruppeZweiName = Truppeb.Schwertkaempfer
						                          , TruppeZweiMax = verfuegbareTruppen.SchwertkaempferAnzahl
						                          , TruppeZweiMin =
							                          dorf.FarmTruppenTemplate.SchwertkaempferAnzahlMitSpeertraeger
					                          });
				if (
					verfuegbareTruppen.SpeertraegerAnzahl >=
					dorf.FarmTruppenTemplate.SpeertraegerAnzahlMitAxtkaempfer &&
					dorf.FarmTruppenTemplate.SpeertraegerAnzahlMitAxtkaempfer != 0 &&
					verfuegbareTruppen.AxtkaempferAnzahl >= dorf.FarmTruppenTemplate.AxtkaempferAnzahlMitSpeertraeger &&
					dorf.FarmTruppenTemplate.AxtkaempferAnzahlMitSpeertraeger != 0 &&
					dorf.FarmTruppenTemplate.AxtkaempferActive)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.Speertraeger
						                          , TruppeEinsMax = verfuegbareTruppen.SpeertraegerAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate.SpeertraegerAnzahlMitSchwertkaempfer
						                          , TruppeZweiName = Truppeb.Axtkaempfer
						                          , TruppeZweiMax = verfuegbareTruppen.AxtkaempferAnzahl
						                          , TruppeZweiMin =
							                          dorf.FarmTruppenTemplate.AxtkaempferAnzahlMitSpeertraeger
					                          });
				if (
					verfuegbareTruppen.SpeertraegerAnzahl >=
					dorf.FarmTruppenTemplate.SpeertraegerAnzahlMitBogenschuetzen &&
					dorf.FarmTruppenTemplate.SpeertraegerAnzahlMitBogenschuetzen != 0 &&
					verfuegbareTruppen.BogenschuetzenAnzahl >=
					dorf.FarmTruppenTemplate.BogenschuetzenAnzahlMitSpeertraeger &&
					dorf.FarmTruppenTemplate.BogenschuetzenAnzahlMitSpeertraeger != 0 &&
					App.Settings.Weltensettings.BogenschuetzenActive && dorf.FarmTruppenTemplate.BogenschuetzenActive)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.Speertraeger
						                          , TruppeEinsMax = verfuegbareTruppen.SpeertraegerAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate.SpeertraegerAnzahlMitSchwertkaempfer
						                          , TruppeZweiName = Truppeb.Bogenschuetzen
						                          , TruppeZweiMax = verfuegbareTruppen.BogenschuetzenAnzahl
						                          , TruppeZweiMin =
							                          dorf.FarmTruppenTemplate.BogenschuetzenAnzahlMitSpeertraeger
					                          });
				if (
					verfuegbareTruppen.SpeertraegerAnzahl >=
					dorf.FarmTruppenTemplate.SpeertraegerAnzahlMitLeichteKavallerie &&
					dorf.FarmTruppenTemplate.SpeertraegerAnzahlMitLeichteKavallerie != 0 &&
					verfuegbareTruppen.LeichteKavallerieAnzahl >=
					dorf.FarmTruppenTemplate.LeichteKavallerieAnzahlMitSpeertraeger &&
					dorf.FarmTruppenTemplate.LeichteKavallerieAnzahlMitSpeertraeger != 0 &&
					dorf.FarmTruppenTemplate.LeichteKavallerieActive)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.Speertraeger
						                          , TruppeEinsMax = verfuegbareTruppen.SpeertraegerAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate.SpeertraegerAnzahlMitLeichteKavallerie
						                          , TruppeZweiName = Truppeb.LeichteKavallerie
						                          , TruppeZweiMax = verfuegbareTruppen.LeichteKavallerieAnzahl
						                          , TruppeZweiMin =
							                          dorf.FarmTruppenTemplate.LeichteKavallerieAnzahlMitSpeertraeger
					                          });
				if (
					verfuegbareTruppen.SpeertraegerAnzahl >=
					dorf.FarmTruppenTemplate.SpeertraegerAnzahlMitBeritteneBogenschuetzen &&
					dorf.FarmTruppenTemplate.SpeertraegerAnzahlMitBeritteneBogenschuetzen != 0 &&
					verfuegbareTruppen.BeritteneBogenschuetzenAnzahl >=
					dorf.FarmTruppenTemplate.BeritteneBogenschuetzenAnzahlMitSpeertraeger &&
					dorf.FarmTruppenTemplate.BeritteneBogenschuetzenAnzahlMitSpeertraeger != 0 &&
					App.Settings.Weltensettings.BogenschuetzenActive &&
					dorf.FarmTruppenTemplate.BeritteneBogenschuetzenActive)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.Speertraeger
						                          , TruppeEinsMax = verfuegbareTruppen.SpeertraegerAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate
							                              .SpeertraegerAnzahlMitBeritteneBogenschuetzen
						                          , TruppeZweiName = Truppeb.BeritteneBogenschuetzen
						                          , TruppeZweiMax = verfuegbareTruppen.BeritteneBogenschuetzenAnzahl
						                          , TruppeZweiMin =
							                          dorf.FarmTruppenTemplate
							                              .BeritteneBogenschuetzenAnzahlMitSpeertraeger
					                          });
				if (
					verfuegbareTruppen.SpeertraegerAnzahl >=
					dorf.FarmTruppenTemplate.SpeertraegerAnzahlMitSchwereKavallerie &&
					dorf.FarmTruppenTemplate.SpeertraegerAnzahlMitSchwereKavallerie != 0 &&
					verfuegbareTruppen.SchwereKavallerieAnzahl >=
					dorf.FarmTruppenTemplate.SchwereKavallerieAnzahlMitSpeertraeger &&
					dorf.FarmTruppenTemplate.SchwereKavallerieAnzahlMitSpeertraeger != 0 &&
					dorf.FarmTruppenTemplate.SchwereKavallerieActive)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.Speertraeger
						                          , TruppeEinsMax = verfuegbareTruppen.SpeertraegerAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate.SpeertraegerAnzahlMitSchwereKavallerie
						                          , TruppeZweiName = Truppeb.SchwereKavallerie
						                          , TruppeZweiMax = verfuegbareTruppen.SchwereKavallerieAnzahl
						                          , TruppeZweiMin =
							                          dorf.FarmTruppenTemplate.SchwereKavallerieAnzahlMitSpeertraeger
					                          });
				if (verfuegbareTruppen.SpeertraegerAnzahl >= dorf.FarmTruppenTemplate.SpeertraegerAnzahlMitPaladin &&
				    dorf.FarmTruppenTemplate.SpeertraegerAnzahlMitPaladin != 0 &&
				    verfuegbareTruppen.PaladinAnzahl >= dorf.FarmTruppenTemplate.PaladinAnzahlMitSpeertraeger &&
				    dorf.FarmTruppenTemplate.PaladinAnzahlMitSpeertraeger != 0 &&
				    dorf.FarmTruppenTemplate.PaladinActive && App.Settings.Weltensettings.PaladinActive)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.Speertraeger
						                          , TruppeEinsMax = verfuegbareTruppen.SpeertraegerAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate.SpeertraegerAnzahlMitPaladin
						                          , TruppeZweiName = Truppeb.Paladin
						                          , TruppeZweiMax = verfuegbareTruppen.PaladinAnzahl
						                          , TruppeZweiMin =
							                          dorf.FarmTruppenTemplate.PaladinAnzahlMitSpeertraeger
					                          });
			}

			//Schwertkaempfer
			if (verfuegbareTruppen.SchwertkaempferAnzahl > 0 && dorf.FarmTruppenTemplate.SchwertkaempferActive) {
				if (verfuegbareTruppen.SchwertkaempferAnzahl >= dorf.FarmTruppenTemplate.SchwertkaempferAnzahl)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.Schwertkaempfer
						                          , TruppeEinsMax = verfuegbareTruppen.SchwertkaempferAnzahl
						                          , TruppeEinsMin = dorf.FarmTruppenTemplate.SchwertkaempferAnzahl
						                          , TruppeZweiName = ""
						                          , TruppeZweiMax = 0
						                          , TruppeZweiMin = 0
					                          });

				if (
					verfuegbareTruppen.SchwertkaempferAnzahl >=
					dorf.FarmTruppenTemplate.SchwertkaempferAnzahlMitLeichteKavallerie &&
					dorf.FarmTruppenTemplate.SchwertkaempferAnzahlMitLeichteKavallerie != 0 &&
					verfuegbareTruppen.LeichteKavallerieAnzahl >=
					dorf.FarmTruppenTemplate.LeichteKavallerieAnzahlMitSchwertkaempfer &&
					dorf.FarmTruppenTemplate.LeichteKavallerieActive &&
					dorf.FarmTruppenTemplate.LeichteKavallerieAnzahlMitSchwertkaempfer != 0)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.LeichteKavallerie
						                          , TruppeEinsMax = verfuegbareTruppen.SchwertkaempferAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate.SchwertkaempferAnzahlMitLeichteKavallerie
						                          , TruppeZweiName = Truppeb.Schwertkaempfer
						                          , TruppeZweiMax = verfuegbareTruppen.LeichteKavallerieAnzahl
						                          , TruppeZweiMin =
							                          dorf.FarmTruppenTemplate.LeichteKavallerieAnzahlMitSchwertkaempfer
					                          });
				if (
					verfuegbareTruppen.SchwertkaempferAnzahl >=
					dorf.FarmTruppenTemplate.SchwertkaempferAnzahlMitAxtkaempfer &&
					dorf.FarmTruppenTemplate.SchwertkaempferAnzahlMitAxtkaempfer != 0 &&
					verfuegbareTruppen.AxtkaempferAnzahl >=
					dorf.FarmTruppenTemplate.AxtkaempferAnzahlMitSchwertkaempfer &&
					dorf.FarmTruppenTemplate.AxtkaempferActive &&
					dorf.FarmTruppenTemplate.AxtkaempferAnzahlMitSchwertkaempfer != 0)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.Schwertkaempfer
						                          , TruppeEinsMax = verfuegbareTruppen.SchwertkaempferAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate.SchwertkaempferAnzahlMitAxtkaempfer
						                          , TruppeZweiName = Truppeb.Axtkaempfer
						                          , TruppeZweiMax = verfuegbareTruppen.AxtkaempferAnzahl
						                          , TruppeZweiMin =
							                          dorf.FarmTruppenTemplate.AxtkaempferAnzahlMitSchwertkaempfer
					                          });
				if (
					verfuegbareTruppen.SchwertkaempferAnzahl >=
					dorf.FarmTruppenTemplate.SchwertkaempferAnzahlMitBogenschuetzen &&
					dorf.FarmTruppenTemplate.SchwertkaempferAnzahl != 0 &&
					verfuegbareTruppen.BogenschuetzenAnzahl >=
					dorf.FarmTruppenTemplate.BogenschuetzenAnzahlMitSchwertkaempfer &&
					App.Settings.Weltensettings.BogenschuetzenActive && dorf.FarmTruppenTemplate.BogenschuetzenActive &&
					dorf.FarmTruppenTemplate.BogenschuetzenAnzahlMitSchwertkaempfer != 0)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.Schwertkaempfer
						                          , TruppeEinsMax = verfuegbareTruppen.SchwertkaempferAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate.SchwertkaempferAnzahlMitBogenschuetzen
						                          , TruppeZweiName = Truppeb.Bogenschuetzen
						                          , TruppeZweiMax = verfuegbareTruppen.BogenschuetzenAnzahl
						                          , TruppeZweiMin =
							                          dorf.FarmTruppenTemplate.BogenschuetzenAnzahlMitSchwertkaempfer
					                          });
				if (
					verfuegbareTruppen.SchwertkaempferAnzahl >=
					dorf.FarmTruppenTemplate.SchwertkaempferAnzahlMitSpeertraeger &&
					dorf.FarmTruppenTemplate.SchwertkaempferAnzahlMitSpeertraeger != 0 &&
					verfuegbareTruppen.SpeertraegerAnzahl >=
					dorf.FarmTruppenTemplate.SpeertraegerAnzahlMitSchwertkaempfer &&
					dorf.FarmTruppenTemplate.SpeertraegerActive &&
					dorf.FarmTruppenTemplate.SpeertraegerAnzahlMitSchwertkaempfer != 0)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.Schwertkaempfer
						                          , TruppeEinsMax = verfuegbareTruppen.SchwertkaempferAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate.SchwertkaempferAnzahlMitSpeertraeger
						                          , TruppeZweiName = Truppeb.Speertraeger
						                          , TruppeZweiMax = verfuegbareTruppen.SpeertraegerAnzahl
						                          , TruppeZweiMin =
							                          dorf.FarmTruppenTemplate.SpeertraegerAnzahlMitSchwertkaempfer
					                          });
				if (
					verfuegbareTruppen.SchwertkaempferAnzahl >=
					dorf.FarmTruppenTemplate.SchwertkaempferAnzahlMitBeritteneBogenschuetzen &&
					dorf.FarmTruppenTemplate.SchwertkaempferAnzahlMitBeritteneBogenschuetzen != 0 &&
					verfuegbareTruppen.BeritteneBogenschuetzenAnzahl >=
					dorf.FarmTruppenTemplate.BeritteneBogenschuetzenAnzahlMitSchwertkaempfer &&
					App.Settings.Weltensettings.BogenschuetzenActive &&
					dorf.FarmTruppenTemplate.BeritteneBogenschuetzenActive &&
					dorf.FarmTruppenTemplate.BeritteneBogenschuetzenAnzahlMitSchwertkaempfer != 0)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.Schwertkaempfer
						                          , TruppeEinsMax = verfuegbareTruppen.SchwertkaempferAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate
							                              .SchwertkaempferAnzahlMitBeritteneBogenschuetzen
						                          , TruppeZweiName = Truppeb.BeritteneBogenschuetzen
						                          , TruppeZweiMax = verfuegbareTruppen.BeritteneBogenschuetzenAnzahl
						                          , TruppeZweiMin =
							                          dorf.FarmTruppenTemplate
							                              .BeritteneBogenschuetzenAnzahlMitSchwertkaempfer
					                          });
				if (
					verfuegbareTruppen.SchwertkaempferAnzahl >=
					dorf.FarmTruppenTemplate.SchwertkaempferAnzahlMitSchwereKavallerie &&
					dorf.FarmTruppenTemplate.SchwertkaempferAnzahlMitSchwereKavallerie != 0 &&
					verfuegbareTruppen.SchwereKavallerieAnzahl >=
					dorf.FarmTruppenTemplate.SchwereKavallerieAnzahlMitSchwertkaempfer &&
					dorf.FarmTruppenTemplate.SchwereKavallerieActive &&
					dorf.FarmTruppenTemplate.SchwereKavallerieAnzahlMitSchwertkaempfer != 0)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.Schwertkaempfer
						                          , TruppeEinsMax = verfuegbareTruppen.SchwertkaempferAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate.SchwertkaempferAnzahlMitSchwereKavallerie
						                          , TruppeZweiName = Truppeb.SchwereKavallerie
						                          , TruppeZweiMax = verfuegbareTruppen.SchwereKavallerieAnzahl
						                          , TruppeZweiMin =
							                          dorf.FarmTruppenTemplate.SchwereKavallerieAnzahlMitSchwertkaempfer
					                          });
				if (
					verfuegbareTruppen.SchwertkaempferAnzahl >=
					dorf.FarmTruppenTemplate.SchwertkaempferAnzahlMitPaladin &&
					dorf.FarmTruppenTemplate.SchwertkaempferAnzahlMitPaladin != 0 &&
					verfuegbareTruppen.PaladinAnzahl >= dorf.FarmTruppenTemplate.PaladinAnzahlMitSchwertkaempfer &&
					dorf.FarmTruppenTemplate.PaladinActive && App.Settings.Weltensettings.PaladinActive &&
					dorf.FarmTruppenTemplate.PaladinAnzahlMitSchwertkaempfer != 0)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.Schwertkaempfer
						                          , TruppeEinsMax = verfuegbareTruppen.SchwertkaempferAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate.SchwertkaempferAnzahlMitPaladin
						                          , TruppeZweiName = Truppeb.Paladin
						                          , TruppeZweiMax = verfuegbareTruppen.PaladinAnzahl
						                          , TruppeZweiMin =
							                          dorf.FarmTruppenTemplate.PaladinAnzahlMitSchwertkaempfer
					                          });
			}

			//Axtkaempfer
			if (verfuegbareTruppen.AxtkaempferAnzahl > 0 && dorf.FarmTruppenTemplate.AxtkaempferActive) {
				if (verfuegbareTruppen.AxtkaempferAnzahl >= dorf.FarmTruppenTemplate.AxtkaempferAnzahl &&
				    dorf.FarmTruppenTemplate.AxtkaempferAnzahl != 0)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.Axtkaempfer
						                          , TruppeEinsMax = verfuegbareTruppen.AxtkaempferAnzahl
						                          , TruppeEinsMin = dorf.FarmTruppenTemplate.AxtkaempferAnzahl
						                          , TruppeZweiName = ""
						                          , TruppeZweiMax = 0
						                          , TruppeZweiMin = 0
					                          });

				if (
					verfuegbareTruppen.AxtkaempferAnzahl >=
					dorf.FarmTruppenTemplate.AxtkaempferAnzahlMitLeichteKavallerie &&
					dorf.FarmTruppenTemplate.AxtkaempferAnzahlMitLeichteKavallerie != 0 &&
					verfuegbareTruppen.LeichteKavallerieAnzahl >=
					dorf.FarmTruppenTemplate.LeichteKavallerieAnzahlMitAxtkaempfer &&
					dorf.FarmTruppenTemplate.LeichteKavallerieAnzahlMitAxtkaempfer != 0 &&
					dorf.FarmTruppenTemplate.LeichteKavallerieActive)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.Axtkaempfer
						                          , TruppeEinsMax = verfuegbareTruppen.AxtkaempferAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate.AxtkaempferAnzahlMitLeichteKavallerie
						                          , TruppeZweiName = Truppeb.LeichteKavallerie
						                          , TruppeZweiMax = verfuegbareTruppen.LeichteKavallerieAnzahl
						                          , TruppeZweiMin =
							                          dorf.FarmTruppenTemplate.LeichteKavallerieAnzahlMitAxtkaempfer
					                          });
				if (
					verfuegbareTruppen.AxtkaempferAnzahl >=
					dorf.FarmTruppenTemplate.AxtkaempferAnzahlMitSchwertkaempfer &&
					dorf.FarmTruppenTemplate.AxtkaempferAnzahlMitSchwertkaempfer != 0 &&
					verfuegbareTruppen.SchwertkaempferAnzahl >=
					dorf.FarmTruppenTemplate.SchwertkaempferAnzahlMitAxtkaempfer &&
					dorf.FarmTruppenTemplate.SchwertkaempferAnzahlMitAxtkaempfer != 0 &&
					dorf.FarmTruppenTemplate.SchwertkaempferActive)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.Axtkaempfer
						                          , TruppeEinsMax = verfuegbareTruppen.AxtkaempferAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate.AxtkaempferAnzahlMitSchwertkaempfer
						                          , TruppeZweiName = Truppeb.Schwertkaempfer
						                          , TruppeZweiMax = verfuegbareTruppen.SchwertkaempferAnzahl
						                          , TruppeZweiMin =
							                          dorf.FarmTruppenTemplate.SchwertkaempferAnzahlMitAxtkaempfer
					                          });
				if (
					verfuegbareTruppen.AxtkaempferAnzahl >=
					dorf.FarmTruppenTemplate.AxtkaempferAnzahlMitBogenschuetzen &&
					dorf.FarmTruppenTemplate.AxtkaempferAnzahlMitBogenschuetzen != 0 &&
					verfuegbareTruppen.BogenschuetzenAnzahl >=
					dorf.FarmTruppenTemplate.BogenschuetzenAnzahlMitAxtkaempfer &&
					dorf.FarmTruppenTemplate.BogenschuetzenAnzahlMitAxtkaempfer != 0 &&
					App.Settings.Weltensettings.BogenschuetzenActive && dorf.FarmTruppenTemplate.BogenschuetzenActive)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.Axtkaempfer
						                          , TruppeEinsMax = verfuegbareTruppen.AxtkaempferAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate.AxtkaempferAnzahlMitBogenschuetzen
						                          , TruppeZweiName = Truppeb.Bogenschuetzen
						                          , TruppeZweiMax = verfuegbareTruppen.BogenschuetzenAnzahl
						                          , TruppeZweiMin =
							                          dorf.FarmTruppenTemplate.BogenschuetzenAnzahlMitAxtkaempfer
					                          });
				if (verfuegbareTruppen.AxtkaempferAnzahl >= dorf.FarmTruppenTemplate.AxtkaempferAnzahlMitSpeertraeger &&
				    dorf.FarmTruppenTemplate.AxtkaempferAnzahlMitSpeertraeger != 0 &&
				    verfuegbareTruppen.SpeertraegerAnzahl >=
				    dorf.FarmTruppenTemplate.SpeertraegerAnzahlMitAxtkaempfer &&
				    dorf.FarmTruppenTemplate.SpeertraegerAnzahlMitAxtkaempfer != 0 &&
				    dorf.FarmTruppenTemplate.SpeertraegerActive)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.Axtkaempfer
						                          , TruppeEinsMax = verfuegbareTruppen.AxtkaempferAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate.AxtkaempferAnzahlMitSpeertraeger
						                          , TruppeZweiName = Truppeb.Speertraeger
						                          , TruppeZweiMax = verfuegbareTruppen.SpeertraegerAnzahl
						                          , TruppeZweiMin =
							                          dorf.FarmTruppenTemplate.SpeertraegerAnzahlMitAxtkaempfer
					                          });
				if (
					verfuegbareTruppen.AxtkaempferAnzahl >=
					dorf.FarmTruppenTemplate.AxtkaempferAnzahlMitBeritteneBogenschuetzen &&
					dorf.FarmTruppenTemplate.AxtkaempferAnzahlMitBeritteneBogenschuetzen != 0 &&
					verfuegbareTruppen.BeritteneBogenschuetzenAnzahl >=
					dorf.FarmTruppenTemplate.BeritteneBogenschuetzenAnzahlMitAxtkaempfer &&
					dorf.FarmTruppenTemplate.BeritteneBogenschuetzenAnzahlMitAxtkaempfer != 0 &&
					App.Settings.Weltensettings.BogenschuetzenActive &&
					dorf.FarmTruppenTemplate.BeritteneBogenschuetzenActive)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.Axtkaempfer
						                          , TruppeEinsMax = verfuegbareTruppen.AxtkaempferAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate
							                              .AxtkaempferAnzahlMitBeritteneBogenschuetzen
						                          , TruppeZweiName = Truppeb.BeritteneBogenschuetzen
						                          , TruppeZweiMax = verfuegbareTruppen.BeritteneBogenschuetzenAnzahl
						                          , TruppeZweiMin =
							                          dorf.FarmTruppenTemplate
							                              .BeritteneBogenschuetzenAnzahlMitAxtkaempfer
					                          });
				if (
					verfuegbareTruppen.AxtkaempferAnzahl >=
					dorf.FarmTruppenTemplate.AxtkaempferAnzahlMitSchwereKavallerie &&
					dorf.FarmTruppenTemplate.AxtkaempferAnzahlMitSchwereKavallerie != 0 &&
					verfuegbareTruppen.SchwereKavallerieAnzahl >=
					dorf.FarmTruppenTemplate.SchwereKavallerieAnzahlMitAxtkaempfer &&
					dorf.FarmTruppenTemplate.SchwereKavallerieAnzahlMitAxtkaempfer != 0 &&
					dorf.FarmTruppenTemplate.SchwereKavallerieActive)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.Axtkaempfer
						                          , TruppeEinsMax = verfuegbareTruppen.AxtkaempferAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate.AxtkaempferAnzahlMitSchwereKavallerie
						                          , TruppeZweiName = Truppeb.SchwereKavallerie
						                          , TruppeZweiMax = verfuegbareTruppen.SchwereKavallerieAnzahl
						                          , TruppeZweiMin =
							                          dorf.FarmTruppenTemplate.SchwereKavallerieAnzahlMitAxtkaempfer
					                          });
				if (verfuegbareTruppen.AxtkaempferAnzahl >= dorf.FarmTruppenTemplate.AxtkaempferAnzahlMitPaladin &&
				    dorf.FarmTruppenTemplate.AxtkaempferAnzahlMitPaladin != 0 &&
				    verfuegbareTruppen.PaladinAnzahl >= dorf.FarmTruppenTemplate.PaladinAnzahlMitAxtkaempfer &&
				    dorf.FarmTruppenTemplate.PaladinAnzahlMitAxtkaempfer != 0 &&
				    dorf.FarmTruppenTemplate.PaladinActive && App.Settings.Weltensettings.PaladinActive)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.Axtkaempfer
						                          , TruppeEinsMax = verfuegbareTruppen.AxtkaempferAnzahl
						                          , TruppeEinsMin = dorf.FarmTruppenTemplate.AxtkaempferAnzahlMitPaladin
						                          , TruppeZweiName = Truppeb.Paladin
						                          , TruppeZweiMax = verfuegbareTruppen.PaladinAnzahl
						                          , TruppeZweiMin = dorf.FarmTruppenTemplate.PaladinAnzahlMitAxtkaempfer
					                          });
			}

			//Bogenschuetzen
			if (verfuegbareTruppen.BogenschuetzenAnzahl > 0 && dorf.FarmTruppenTemplate.BogenschuetzenActive &&
			    App.Settings.Weltensettings.BogenschuetzenActive) {
				if (verfuegbareTruppen.BogenschuetzenAnzahl >= dorf.FarmTruppenTemplate.BogenschuetzenAnzahl &&
				    dorf.FarmTruppenTemplate.BogenschuetzenAnzahl != 0)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.Bogenschuetzen
						                          , TruppeEinsMax = verfuegbareTruppen.BogenschuetzenAnzahl
						                          , TruppeEinsMin = dorf.FarmTruppenTemplate.BogenschuetzenAnzahl
						                          , TruppeZweiName = ""
						                          , TruppeZweiMax = 0
						                          , TruppeZweiMin = 0
					                          });

				if (
					verfuegbareTruppen.BogenschuetzenAnzahl >=
					dorf.FarmTruppenTemplate.BogenschuetzenAnzahlMitLeichteKavallerie &&
					dorf.FarmTruppenTemplate.BogenschuetzenAnzahlMitLeichteKavallerie != 0 &&
					verfuegbareTruppen.LeichteKavallerieAnzahl >=
					dorf.FarmTruppenTemplate.LeichteKavallerieAnzahlMitBogenschuetzen &&
					dorf.FarmTruppenTemplate.LeichteKavallerieAnzahlMitBogenschuetzen != 0 &&
					dorf.FarmTruppenTemplate.LeichteKavallerieActive)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.Bogenschuetzen
						                          , TruppeEinsMax = verfuegbareTruppen.BogenschuetzenAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate.BogenschuetzenAnzahlMitLeichteKavallerie
						                          , TruppeZweiName = Truppeb.LeichteKavallerie
						                          , TruppeZweiMax = verfuegbareTruppen.LeichteKavallerieAnzahl
						                          , TruppeZweiMin =
							                          dorf.FarmTruppenTemplate.LeichteKavallerieAnzahlMitBogenschuetzen
					                          });
				if (
					verfuegbareTruppen.BogenschuetzenAnzahl >=
					dorf.FarmTruppenTemplate.BogenschuetzenAnzahlMitSchwertkaempfer &&
					dorf.FarmTruppenTemplate.BogenschuetzenAnzahlMitSchwertkaempfer != 0 &&
					verfuegbareTruppen.SchwertkaempferAnzahl >=
					dorf.FarmTruppenTemplate.SchwertkaempferAnzahlMitBogenschuetzen &&
					dorf.FarmTruppenTemplate.SchwertkaempferAnzahlMitBogenschuetzen != 0 &&
					dorf.FarmTruppenTemplate.SchwertkaempferActive)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.Bogenschuetzen
						                          , TruppeEinsMax = verfuegbareTruppen.BogenschuetzenAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate.BogenschuetzenAnzahlMitSchwertkaempfer
						                          , TruppeZweiName = Truppeb.Schwertkaempfer
						                          , TruppeZweiMax = verfuegbareTruppen.SchwertkaempferAnzahl
						                          , TruppeZweiMin =
							                          dorf.FarmTruppenTemplate.SchwertkaempferAnzahlMitBogenschuetzen
					                          });
				if (
					verfuegbareTruppen.BogenschuetzenAnzahl >=
					dorf.FarmTruppenTemplate.BogenschuetzenAnzahlMitAxtkaempfer &&
					dorf.FarmTruppenTemplate.BogenschuetzenAnzahlMitAxtkaempfer != 0 &&
					verfuegbareTruppen.AxtkaempferAnzahl >=
					dorf.FarmTruppenTemplate.AxtkaempferAnzahlMitBogenschuetzen &&
					dorf.FarmTruppenTemplate.AxtkaempferAnzahlMitBogenschuetzen != 0 &&
					dorf.FarmTruppenTemplate.AxtkaempferActive)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.Bogenschuetzen
						                          , TruppeEinsMax = verfuegbareTruppen.BogenschuetzenAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate.BogenschuetzenAnzahlMitAxtkaempfer
						                          , TruppeZweiName = Truppeb.Axtkaempfer
						                          , TruppeZweiMax = verfuegbareTruppen.AxtkaempferAnzahl
						                          , TruppeZweiMin =
							                          dorf.FarmTruppenTemplate.AxtkaempferAnzahlMitBogenschuetzen
					                          });
				if (
					verfuegbareTruppen.BogenschuetzenAnzahl >=
					dorf.FarmTruppenTemplate.BogenschuetzenAnzahlMitSpeertraeger &&
					dorf.FarmTruppenTemplate.BogenschuetzenAnzahlMitSpeertraeger != 0 &&
					verfuegbareTruppen.SpeertraegerAnzahl >=
					dorf.FarmTruppenTemplate.SpeertraegerAnzahlMitBogenschuetzen &&
					dorf.FarmTruppenTemplate.SpeertraegerAnzahlMitBogenschuetzen != 0 &&
					dorf.FarmTruppenTemplate.SpeertraegerActive)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.Bogenschuetzen
						                          , TruppeEinsMax = verfuegbareTruppen.BogenschuetzenAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate.BogenschuetzenAnzahlMitSpeertraeger
						                          , TruppeZweiName = Truppeb.Speertraeger
						                          , TruppeZweiMax = verfuegbareTruppen.SpeertraegerAnzahl
						                          , TruppeZweiMin =
							                          dorf.FarmTruppenTemplate.SpeertraegerAnzahlMitBogenschuetzen
					                          });
				if (
					verfuegbareTruppen.BogenschuetzenAnzahl >=
					dorf.FarmTruppenTemplate.BogenschuetzenAnzahlMitBeritteneBogenschuetzen &&
					dorf.FarmTruppenTemplate.BogenschuetzenAnzahlMitBeritteneBogenschuetzen != 0 &&
					verfuegbareTruppen.BeritteneBogenschuetzenAnzahl >=
					dorf.FarmTruppenTemplate.BeritteneBogenschuetzenAnzahlMitBogenschuetzen &&
					dorf.FarmTruppenTemplate.BeritteneBogenschuetzenAnzahlMitBogenschuetzen != 0 &&
					dorf.FarmTruppenTemplate.BeritteneBogenschuetzenActive)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.Bogenschuetzen
						                          , TruppeEinsMax = verfuegbareTruppen.BogenschuetzenAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate
							                              .BogenschuetzenAnzahlMitBeritteneBogenschuetzen
						                          , TruppeZweiName = Truppeb.BeritteneBogenschuetzen
						                          , TruppeZweiMax = verfuegbareTruppen.BeritteneBogenschuetzenAnzahl
						                          , TruppeZweiMin =
							                          dorf.FarmTruppenTemplate
							                              .BeritteneBogenschuetzenAnzahlMitBogenschuetzen
					                          });
				if (
					verfuegbareTruppen.BogenschuetzenAnzahl >=
					dorf.FarmTruppenTemplate.BogenschuetzenAnzahlMitSchwereKavallerie &&
					dorf.FarmTruppenTemplate.BogenschuetzenAnzahlMitSchwereKavallerie != 0 &&
					verfuegbareTruppen.SchwereKavallerieAnzahl >=
					dorf.FarmTruppenTemplate.SchwereKavallerieAnzahlMitBogenschuetzen &&
					dorf.FarmTruppenTemplate.SchwereKavallerieAnzahlMitBogenschuetzen != 0 &&
					dorf.FarmTruppenTemplate.SchwereKavallerieActive)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.Bogenschuetzen
						                          , TruppeEinsMax = verfuegbareTruppen.BogenschuetzenAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate.BogenschuetzenAnzahlMitSchwereKavallerie
						                          , TruppeZweiName = Truppeb.SchwereKavallerie
						                          , TruppeZweiMax = verfuegbareTruppen.SchwereKavallerieAnzahl
						                          , TruppeZweiMin =
							                          dorf.FarmTruppenTemplate.SchwereKavallerieAnzahlMitBogenschuetzen
					                          });
				if (
					verfuegbareTruppen.BogenschuetzenAnzahl >=
					dorf.FarmTruppenTemplate.BogenschuetzenAnzahlMitPaladin &&
					dorf.FarmTruppenTemplate.BogenschuetzenAnzahlMitPaladin != 0 &&
					verfuegbareTruppen.PaladinAnzahl >= dorf.FarmTruppenTemplate.PaladinAnzahlMitBogenschuetzen &&
					dorf.FarmTruppenTemplate.PaladinAnzahlMitBogenschuetzen != 0 &&
					dorf.FarmTruppenTemplate.PaladinActive && App.Settings.Weltensettings.PaladinActive)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.Bogenschuetzen
						                          , TruppeEinsMax = verfuegbareTruppen.BogenschuetzenAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate.BogenschuetzenAnzahlMitPaladin
						                          , TruppeZweiName = Truppeb.Paladin
						                          , TruppeZweiMax = verfuegbareTruppen.PaladinAnzahl
						                          , TruppeZweiMin =
							                          dorf.FarmTruppenTemplate.PaladinAnzahlMitBogenschuetzen
					                          });
			}

			//Leichte Kavallerie
			if (verfuegbareTruppen.LeichteKavallerieAnzahl > 0 && dorf.FarmTruppenTemplate.LeichteKavallerieActive) {
				if (verfuegbareTruppen.LeichteKavallerieAnzahl >= dorf.FarmTruppenTemplate.LeichteKavallerieAnzahl &&
				    dorf.FarmTruppenTemplate.LeichteKavallerieAnzahl != 0)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.LeichteKavallerie
						                          , TruppeEinsMax = verfuegbareTruppen.LeichteKavallerieAnzahl
						                          , TruppeEinsMin = dorf.FarmTruppenTemplate.LeichteKavallerieAnzahl
						                          , TruppeZweiName = ""
						                          , TruppeZweiMax = 0
						                          , TruppeZweiMin = 0
					                          });

				if (
					verfuegbareTruppen.LeichteKavallerieAnzahl >=
					dorf.FarmTruppenTemplate.LeichteKavallerieAnzahlMitSchwertkaempfer &&
					dorf.FarmTruppenTemplate.LeichteKavallerieAnzahlMitSchwertkaempfer != 0 &&
					verfuegbareTruppen.SchwertkaempferAnzahl >=
					dorf.FarmTruppenTemplate.SchwertkaempferAnzahlMitLeichteKavallerie &&
					dorf.FarmTruppenTemplate.SchwertkaempferAnzahlMitLeichteKavallerie != 0 &&
					dorf.FarmTruppenTemplate.SchwertkaempferActive)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.LeichteKavallerie
						                          , TruppeEinsMax = verfuegbareTruppen.LeichteKavallerieAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate.LeichteKavallerieAnzahlMitSchwertkaempfer
						                          , TruppeZweiName = Truppeb.Schwertkaempfer
						                          , TruppeZweiMax = verfuegbareTruppen.SchwertkaempferAnzahl
						                          , TruppeZweiMin =
							                          dorf.FarmTruppenTemplate.SchwertkaempferAnzahlMitLeichteKavallerie
					                          });
				if (
					verfuegbareTruppen.LeichteKavallerieAnzahl >=
					dorf.FarmTruppenTemplate.LeichteKavallerieAnzahlMitAxtkaempfer &&
					dorf.FarmTruppenTemplate.LeichteKavallerieAnzahlMitAxtkaempfer != 0 &&
					verfuegbareTruppen.AxtkaempferAnzahl >=
					dorf.FarmTruppenTemplate.AxtkaempferAnzahlMitLeichteKavallerie &&
					dorf.FarmTruppenTemplate.AxtkaempferAnzahlMitLeichteKavallerie != 0 &&
					dorf.FarmTruppenTemplate.AxtkaempferActive)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.LeichteKavallerie
						                          , TruppeEinsMax = verfuegbareTruppen.LeichteKavallerieAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate.LeichteKavallerieAnzahlMitAxtkaempfer
						                          , TruppeZweiName = Truppeb.Axtkaempfer
						                          , TruppeZweiMax = verfuegbareTruppen.AxtkaempferAnzahl
						                          , TruppeZweiMin =
							                          dorf.FarmTruppenTemplate.AxtkaempferAnzahlMitLeichteKavallerie
					                          });
				if (
					verfuegbareTruppen.LeichteKavallerieAnzahl >=
					dorf.FarmTruppenTemplate.LeichteKavallerieAnzahlMitBogenschuetzen &&
					dorf.FarmTruppenTemplate.LeichteKavallerieAnzahlMitBogenschuetzen != 0 &&
					verfuegbareTruppen.BogenschuetzenAnzahl >=
					dorf.FarmTruppenTemplate.BogenschuetzenAnzahlMitLeichteKavallerie &&
					dorf.FarmTruppenTemplate.BogenschuetzenAnzahlMitLeichteKavallerie != 0 &&
					App.Settings.Weltensettings.BogenschuetzenActive && dorf.FarmTruppenTemplate.BogenschuetzenActive)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.LeichteKavallerie
						                          , TruppeEinsMax = verfuegbareTruppen.LeichteKavallerieAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate.LeichteKavallerieAnzahlMitBogenschuetzen
						                          , TruppeZweiName = Truppeb.Bogenschuetzen
						                          , TruppeZweiMax = verfuegbareTruppen.BogenschuetzenAnzahl
						                          , TruppeZweiMin =
							                          dorf.FarmTruppenTemplate.BogenschuetzenAnzahlMitLeichteKavallerie
					                          });
				if (
					verfuegbareTruppen.LeichteKavallerieAnzahl >=
					dorf.FarmTruppenTemplate.LeichteKavallerieAnzahlMitSpeertraeger &&
					dorf.FarmTruppenTemplate.LeichteKavallerieAnzahlMitSpeertraeger != 0 &&
					verfuegbareTruppen.LeichteKavallerieAnzahl >=
					dorf.FarmTruppenTemplate.SpeertraegerAnzahlMitLeichteKavallerie &&
					dorf.FarmTruppenTemplate.SpeertraegerAnzahlMitLeichteKavallerie != 0 &&
					dorf.FarmTruppenTemplate.SpeertraegerActive)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.LeichteKavallerie
						                          , TruppeEinsMax = verfuegbareTruppen.LeichteKavallerieAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate.LeichteKavallerieAnzahlMitSpeertraeger
						                          , TruppeZweiName = Truppeb.Speertraeger
						                          , TruppeZweiMax = verfuegbareTruppen.SpeertraegerAnzahl
						                          , TruppeZweiMin =
							                          dorf.FarmTruppenTemplate.SpeertraegerAnzahlMitLeichteKavallerie
					                          });
				if (
					verfuegbareTruppen.LeichteKavallerieAnzahl >=
					dorf.FarmTruppenTemplate.LeichteKavallerieAnzahlMitBeritteneBogenschuetzen &&
					dorf.FarmTruppenTemplate.LeichteKavallerieAnzahlMitBeritteneBogenschuetzen != 0 &&
					verfuegbareTruppen.BeritteneBogenschuetzenAnzahl >=
					dorf.FarmTruppenTemplate.BeritteneBogenschuetzenAnzahlMitLeichteKavallerie &&
					dorf.FarmTruppenTemplate.BeritteneBogenschuetzenAnzahlMitLeichteKavallerie != 0 &&
					App.Settings.Weltensettings.BogenschuetzenActive &&
					dorf.FarmTruppenTemplate.BeritteneBogenschuetzenActive)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.LeichteKavallerie
						                          , TruppeEinsMax = verfuegbareTruppen.LeichteKavallerieAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate
							                              .LeichteKavallerieAnzahlMitBeritteneBogenschuetzen
						                          , TruppeZweiName = Truppeb.BeritteneBogenschuetzen
						                          , TruppeZweiMax = verfuegbareTruppen.BeritteneBogenschuetzenAnzahl
						                          , TruppeZweiMin =
							                          dorf.FarmTruppenTemplate
							                              .BeritteneBogenschuetzenAnzahlMitLeichteKavallerie
					                          });
				if (
					verfuegbareTruppen.LeichteKavallerieAnzahl >=
					dorf.FarmTruppenTemplate.LeichteKavallerieAnzahlMitSchwereKavallerie &&
					dorf.FarmTruppenTemplate.LeichteKavallerieAnzahlMitSchwereKavallerie != 0 &&
					verfuegbareTruppen.SchwereKavallerieAnzahl >=
					dorf.FarmTruppenTemplate.SchwereKavallerieAnzahlMitLeichteKavallerie &&
					dorf.FarmTruppenTemplate.SchwereKavallerieAnzahlMitLeichteKavallerie != 0 &&
					dorf.FarmTruppenTemplate.SchwereKavallerieActive)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.LeichteKavallerie
						                          , TruppeEinsMax = verfuegbareTruppen.LeichteKavallerieAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate
							                              .LeichteKavallerieAnzahlMitSchwereKavallerie
						                          , TruppeZweiName = Truppeb.SchwereKavallerie
						                          , TruppeZweiMax = verfuegbareTruppen.SchwereKavallerieAnzahl
						                          , TruppeZweiMin =
							                          dorf.FarmTruppenTemplate
							                              .SchwereKavallerieAnzahlMitLeichteKavallerie
					                          });

				if (
					verfuegbareTruppen.LeichteKavallerieAnzahl >=
					dorf.FarmTruppenTemplate.LeichteKavallerieAnzahlMitPaladin &&
					dorf.FarmTruppenTemplate.LeichteKavallerieAnzahlMitPaladin != 0 &&
					verfuegbareTruppen.PaladinAnzahl >= dorf.FarmTruppenTemplate.PaladinAnzahlMitLeichteKavallerie &&
					dorf.FarmTruppenTemplate.PaladinAnzahlMitLeichteKavallerie != 0 &&
					dorf.FarmTruppenTemplate.PaladinActive && App.Settings.Weltensettings.PaladinActive)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.LeichteKavallerie
						                          , TruppeEinsMax = verfuegbareTruppen.LeichteKavallerieAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate.LeichteKavallerieAnzahlMitPaladin
						                          , TruppeZweiName = Truppeb.Paladin
						                          , TruppeZweiMax = verfuegbareTruppen.PaladinAnzahl
						                          , TruppeZweiMin =
							                          dorf.FarmTruppenTemplate.PaladinAnzahlMitLeichteKavallerie
					                          });
			}

			//Berittene Bogenschuetzen
			if (verfuegbareTruppen.BeritteneBogenschuetzenAnzahl > 0 &&
			    dorf.FarmTruppenTemplate.BeritteneBogenschuetzenActive &&
			    App.Settings.Weltensettings.BogenschuetzenActive) {
				if (verfuegbareTruppen.BeritteneBogenschuetzenAnzahl >=
				    dorf.FarmTruppenTemplate.BeritteneBogenschuetzenAnzahl &&
				    dorf.FarmTruppenTemplate.BeritteneBogenschuetzenAnzahl != 0)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.BeritteneBogenschuetzen
						                          , TruppeEinsMax = verfuegbareTruppen.BeritteneBogenschuetzenAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate.BeritteneBogenschuetzenAnzahl
						                          , TruppeZweiName = ""
						                          , TruppeZweiMax = 0
						                          , TruppeZweiMin = 0
					                          });

				if (
					verfuegbareTruppen.BeritteneBogenschuetzenAnzahl >=
					dorf.FarmTruppenTemplate.BeritteneBogenschuetzenAnzahlMitLeichteKavallerie &&
					dorf.FarmTruppenTemplate.BeritteneBogenschuetzenAnzahlMitLeichteKavallerie != 0 &&
					verfuegbareTruppen.LeichteKavallerieAnzahl >=
					dorf.FarmTruppenTemplate.LeichteKavallerieAnzahlMitBeritteneBogenschuetzen &&
					dorf.FarmTruppenTemplate.LeichteKavallerieAnzahlMitBeritteneBogenschuetzen != 0 &&
					dorf.FarmTruppenTemplate.LeichteKavallerieActive)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.BeritteneBogenschuetzen
						                          , TruppeEinsMax = verfuegbareTruppen.BeritteneBogenschuetzenAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate
							                              .BeritteneBogenschuetzenAnzahlMitLeichteKavallerie
						                          , TruppeZweiName = Truppeb.LeichteKavallerie
						                          , TruppeZweiMax = verfuegbareTruppen.LeichteKavallerieAnzahl
						                          , TruppeZweiMin =
							                          dorf.FarmTruppenTemplate
							                              .LeichteKavallerieAnzahlMitBeritteneBogenschuetzen
					                          });
				if (
					verfuegbareTruppen.BeritteneBogenschuetzenAnzahl >=
					dorf.FarmTruppenTemplate.BeritteneBogenschuetzenAnzahlMitSchwertkaempfer &&
					dorf.FarmTruppenTemplate.BeritteneBogenschuetzenAnzahlMitSchwertkaempfer != 0 &&
					verfuegbareTruppen.SchwertkaempferAnzahl >=
					dorf.FarmTruppenTemplate.SchwertkaempferAnzahlMitBeritteneBogenschuetzen &&
					dorf.FarmTruppenTemplate.SchwertkaempferAnzahlMitBeritteneBogenschuetzen != 0 &&
					dorf.FarmTruppenTemplate.SchwertkaempferActive)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.BeritteneBogenschuetzen
						                          , TruppeEinsMax = verfuegbareTruppen.BeritteneBogenschuetzenAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate
							                              .BeritteneBogenschuetzenAnzahlMitSchwertkaempfer
						                          , TruppeZweiName = Truppeb.Schwertkaempfer
						                          , TruppeZweiMax = verfuegbareTruppen.SchwertkaempferAnzahl
						                          , TruppeZweiMin =
							                          dorf.FarmTruppenTemplate
							                              .SchwertkaempferAnzahlMitBeritteneBogenschuetzen
					                          });
				if (
					verfuegbareTruppen.BeritteneBogenschuetzenAnzahl >=
					dorf.FarmTruppenTemplate.BeritteneBogenschuetzenAnzahlMitAxtkaempfer &&
					dorf.FarmTruppenTemplate.BeritteneBogenschuetzenAnzahlMitAxtkaempfer != 0 &&
					verfuegbareTruppen.AxtkaempferAnzahl >=
					dorf.FarmTruppenTemplate.AxtkaempferAnzahlMitBeritteneBogenschuetzen &&
					dorf.FarmTruppenTemplate.AxtkaempferAnzahlMitBeritteneBogenschuetzen != 0 &&
					dorf.FarmTruppenTemplate.AxtkaempferActive)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.BeritteneBogenschuetzen
						                          , TruppeEinsMax = verfuegbareTruppen.BeritteneBogenschuetzenAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate
							                              .BeritteneBogenschuetzenAnzahlMitAxtkaempfer
						                          , TruppeZweiName = Truppeb.Axtkaempfer
						                          , TruppeZweiMax = verfuegbareTruppen.AxtkaempferAnzahl
						                          , TruppeZweiMin =
							                          dorf.FarmTruppenTemplate
							                              .AxtkaempferAnzahlMitBeritteneBogenschuetzen
					                          });
				if (
					verfuegbareTruppen.BeritteneBogenschuetzenAnzahl >=
					dorf.FarmTruppenTemplate.BeritteneBogenschuetzenAnzahlMitSpeertraeger &&
					dorf.FarmTruppenTemplate.BeritteneBogenschuetzenAnzahlMitSpeertraeger != 0 &&
					verfuegbareTruppen.SpeertraegerAnzahl >=
					dorf.FarmTruppenTemplate.SpeertraegerAnzahlMitBeritteneBogenschuetzen &&
					dorf.FarmTruppenTemplate.SpeertraegerAnzahlMitBeritteneBogenschuetzen != 0 &&
					dorf.FarmTruppenTemplate.SpeertraegerActive)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.BeritteneBogenschuetzen
						                          , TruppeEinsMax = verfuegbareTruppen.BeritteneBogenschuetzenAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate
							                              .BeritteneBogenschuetzenAnzahlMitSpeertraeger
						                          , TruppeZweiName = Truppeb.Speertraeger
						                          , TruppeZweiMax = verfuegbareTruppen.SpeertraegerAnzahl
						                          , TruppeZweiMin =
							                          dorf.FarmTruppenTemplate
							                              .SpeertraegerAnzahlMitBeritteneBogenschuetzen
					                          });
				if (
					verfuegbareTruppen.BeritteneBogenschuetzenAnzahl >=
					dorf.FarmTruppenTemplate.BeritteneBogenschuetzenAnzahlMitBogenschuetzen &&
					dorf.FarmTruppenTemplate.BeritteneBogenschuetzenAnzahlMitBogenschuetzen != 0 &&
					verfuegbareTruppen.BogenschuetzenAnzahl >=
					dorf.FarmTruppenTemplate.BogenschuetzenAnzahlMitBeritteneBogenschuetzen &&
					dorf.FarmTruppenTemplate.BogenschuetzenAnzahlMitBeritteneBogenschuetzen != 0 &&
					dorf.FarmTruppenTemplate.BogenschuetzenActive)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.BeritteneBogenschuetzen
						                          , TruppeEinsMax = verfuegbareTruppen.BeritteneBogenschuetzenAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate
							                              .BeritteneBogenschuetzenAnzahlMitBogenschuetzen
						                          , TruppeZweiName = Truppeb.Bogenschuetzen
						                          , TruppeZweiMax = verfuegbareTruppen.BogenschuetzenAnzahl
						                          , TruppeZweiMin =
							                          dorf.FarmTruppenTemplate
							                              .BogenschuetzenAnzahlMitBeritteneBogenschuetzen
					                          });
				if (
					verfuegbareTruppen.BeritteneBogenschuetzenAnzahl >=
					dorf.FarmTruppenTemplate.BeritteneBogenschuetzenAnzahlMitSchwereKavallerie &&
					dorf.FarmTruppenTemplate.BeritteneBogenschuetzenAnzahlMitSchwereKavallerie != 0 &&
					verfuegbareTruppen.SchwereKavallerieAnzahl >=
					dorf.FarmTruppenTemplate.SchwereKavallerieAnzahlMitBeritteneBogenschuetzen &&
					dorf.FarmTruppenTemplate.SchwereKavallerieAnzahlMitBeritteneBogenschuetzen != 0 &&
					dorf.FarmTruppenTemplate.SchwereKavallerieActive)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.BeritteneBogenschuetzen
						                          , TruppeEinsMax = verfuegbareTruppen.BeritteneBogenschuetzenAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate
							                              .BeritteneBogenschuetzenAnzahlMitSchwereKavallerie
						                          , TruppeZweiName = Truppeb.SchwereKavallerie
						                          , TruppeZweiMax = verfuegbareTruppen.SchwereKavallerieAnzahl
						                          , TruppeZweiMin =
							                          dorf.FarmTruppenTemplate
							                              .SchwereKavallerieAnzahlMitBeritteneBogenschuetzen
					                          });

				if (
					verfuegbareTruppen.BeritteneBogenschuetzenAnzahl >=
					dorf.FarmTruppenTemplate.BeritteneBogenschuetzenAnzahlMitPaladin &&
					dorf.FarmTruppenTemplate.BeritteneBogenschuetzenAnzahlMitPaladin != 0 &&
					verfuegbareTruppen.PaladinAnzahl >=
					dorf.FarmTruppenTemplate.PaladinAnzahlMitBeritteneBogenschuetzen &&
					dorf.FarmTruppenTemplate.PaladinAnzahlMitBeritteneBogenschuetzen != 0 &&
					dorf.FarmTruppenTemplate.PaladinActive && App.Settings.Weltensettings.PaladinActive)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.BeritteneBogenschuetzen
						                          , TruppeEinsMax = verfuegbareTruppen.BeritteneBogenschuetzenAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate.BeritteneBogenschuetzenAnzahlMitPaladin
						                          , TruppeZweiName = Truppeb.Paladin
						                          , TruppeZweiMax = verfuegbareTruppen.PaladinAnzahl
						                          , TruppeZweiMin =
							                          dorf.FarmTruppenTemplate.PaladinAnzahlMitBeritteneBogenschuetzen
					                          });
			}

			//Schwere Kavallerie
			if (verfuegbareTruppen.SchwereKavallerieAnzahl > 0 && dorf.FarmTruppenTemplate.SchwereKavallerieActive) {
				if (verfuegbareTruppen.SchwereKavallerieAnzahl >= dorf.FarmTruppenTemplate.SchwereKavallerieAnzahl &&
				    dorf.FarmTruppenTemplate.SchwereKavallerieAnzahl != 0)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.SchwereKavallerie
						                          , TruppeEinsMax = verfuegbareTruppen.SchwereKavallerieAnzahl
						                          , TruppeEinsMin = dorf.FarmTruppenTemplate.SchwereKavallerieAnzahl
						                          , TruppeZweiName = ""
						                          , TruppeZweiMax = 0
						                          , TruppeZweiMin = 0
					                          });

				if (
					verfuegbareTruppen.SchwereKavallerieAnzahl >=
					dorf.FarmTruppenTemplate.SchwereKavallerieAnzahlMitLeichteKavallerie &&
					dorf.FarmTruppenTemplate.SchwereKavallerieAnzahlMitLeichteKavallerie != 0 &&
					verfuegbareTruppen.LeichteKavallerieAnzahl >=
					dorf.FarmTruppenTemplate.LeichteKavallerieAnzahlMitSchwereKavallerie &&
					dorf.FarmTruppenTemplate.LeichteKavallerieAnzahlMitSchwereKavallerie != 0 &&
					dorf.FarmTruppenTemplate.LeichteKavallerieActive)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.SchwereKavallerie
						                          , TruppeEinsMax = verfuegbareTruppen.SchwereKavallerieAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate
							                              .SchwereKavallerieAnzahlMitLeichteKavallerie
						                          , TruppeZweiName = Truppeb.LeichteKavallerie
						                          , TruppeZweiMax = verfuegbareTruppen.LeichteKavallerieAnzahl
						                          , TruppeZweiMin =
							                          dorf.FarmTruppenTemplate
							                              .LeichteKavallerieAnzahlMitSchwereKavallerie
					                          });
				if (
					verfuegbareTruppen.SchwereKavallerieAnzahl >=
					dorf.FarmTruppenTemplate.SchwereKavallerieAnzahlMitSchwertkaempfer &&
					dorf.FarmTruppenTemplate.SchwereKavallerieAnzahlMitSchwertkaempfer != 0 &&
					verfuegbareTruppen.SchwertkaempferAnzahl >=
					dorf.FarmTruppenTemplate.SchwertkaempferAnzahlMitSchwereKavallerie &&
					dorf.FarmTruppenTemplate.SchwertkaempferAnzahlMitSchwereKavallerie != 0 &&
					dorf.FarmTruppenTemplate.SchwertkaempferActive)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.SchwereKavallerie
						                          , TruppeEinsMax = verfuegbareTruppen.SchwereKavallerieAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate.SchwereKavallerieAnzahlMitSchwertkaempfer
						                          , TruppeZweiName = Truppeb.Schwertkaempfer
						                          , TruppeZweiMax = verfuegbareTruppen.SchwertkaempferAnzahl
						                          , TruppeZweiMin =
							                          dorf.FarmTruppenTemplate.SchwertkaempferAnzahlMitSchwereKavallerie
					                          });
				if (
					verfuegbareTruppen.SchwereKavallerieAnzahl >=
					dorf.FarmTruppenTemplate.SchwereKavallerieAnzahlMitBogenschuetzen &&
					dorf.FarmTruppenTemplate.SchwereKavallerieAnzahlMitBogenschuetzen != 0 &&
					verfuegbareTruppen.BogenschuetzenAnzahl >=
					dorf.FarmTruppenTemplate.BogenschuetzenAnzahlMitSchwereKavallerie &&
					dorf.FarmTruppenTemplate.BogenschuetzenAnzahlMitSchwereKavallerie != 0 &&
					App.Settings.Weltensettings.BogenschuetzenActive && dorf.FarmTruppenTemplate.BogenschuetzenActive)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.SchwereKavallerie
						                          , TruppeEinsMax = verfuegbareTruppen.SchwereKavallerieAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate.SchwereKavallerieAnzahlMitBogenschuetzen
						                          , TruppeZweiName = Truppeb.Bogenschuetzen
						                          , TruppeZweiMax = verfuegbareTruppen.BogenschuetzenAnzahl
						                          , TruppeZweiMin =
							                          dorf.FarmTruppenTemplate.BogenschuetzenAnzahlMitSchwereKavallerie
					                          });
				if (
					verfuegbareTruppen.SchwereKavallerieAnzahl >=
					dorf.FarmTruppenTemplate.SchwereKavallerieAnzahlMitSpeertraeger &&
					dorf.FarmTruppenTemplate.SchwereKavallerieAnzahlMitSpeertraeger != 0 &&
					verfuegbareTruppen.SpeertraegerAnzahl >=
					dorf.FarmTruppenTemplate.SpeertraegerAnzahlMitSchwereKavallerie &&
					dorf.FarmTruppenTemplate.SpeertraegerAnzahlMitSchwereKavallerie != 0 &&
					dorf.FarmTruppenTemplate.SpeertraegerActive)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.SchwereKavallerie
						                          , TruppeEinsMax = verfuegbareTruppen.SchwereKavallerieAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate.SchwereKavallerieAnzahlMitSpeertraeger
						                          , TruppeZweiName = Truppeb.Speertraeger
						                          , TruppeZweiMax = verfuegbareTruppen.SpeertraegerAnzahl
						                          , TruppeZweiMin =
							                          dorf.FarmTruppenTemplate.SpeertraegerAnzahlMitSchwereKavallerie
					                          });
				if (
					verfuegbareTruppen.SchwereKavallerieAnzahl >=
					dorf.FarmTruppenTemplate.SchwereKavallerieAnzahlMitBeritteneBogenschuetzen &&
					dorf.FarmTruppenTemplate.SchwereKavallerieAnzahlMitBeritteneBogenschuetzen != 0 &&
					verfuegbareTruppen.BeritteneBogenschuetzenAnzahl >=
					dorf.FarmTruppenTemplate.BeritteneBogenschuetzenAnzahlMitSchwereKavallerie &&
					dorf.FarmTruppenTemplate.BeritteneBogenschuetzenAnzahlMitSchwereKavallerie != 0 &&
					App.Settings.Weltensettings.BogenschuetzenActive &&
					dorf.FarmTruppenTemplate.BeritteneBogenschuetzenActive)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.SchwereKavallerie
						                          , TruppeEinsMax = verfuegbareTruppen.SchwereKavallerieAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate
							                              .SchwereKavallerieAnzahlMitBeritteneBogenschuetzen
						                          , TruppeZweiName = Truppeb.BeritteneBogenschuetzen
						                          , TruppeZweiMax = verfuegbareTruppen.BeritteneBogenschuetzenAnzahl
						                          , TruppeZweiMin =
							                          dorf.FarmTruppenTemplate
							                              .BeritteneBogenschuetzenAnzahlMitSchwereKavallerie
					                          });
				if (
					verfuegbareTruppen.SchwereKavallerieAnzahl >=
					dorf.FarmTruppenTemplate.SchwereKavallerieAnzahlMitAxtkaempfer &&
					dorf.FarmTruppenTemplate.SchwereKavallerieAnzahlMitAxtkaempfer != 0 &&
					verfuegbareTruppen.AxtkaempferAnzahl >=
					dorf.FarmTruppenTemplate.AxtkaempferAnzahlMitSchwereKavallerie &&
					dorf.FarmTruppenTemplate.AxtkaempferAnzahlMitSchwereKavallerie != 0 &&
					dorf.FarmTruppenTemplate.AxtkaempferActive)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.SchwereKavallerie
						                          , TruppeEinsMax = verfuegbareTruppen.SchwereKavallerieAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate.SchwereKavallerieAnzahlMitAxtkaempfer
						                          , TruppeZweiName = Truppeb.SchwereKavallerie
						                          , TruppeZweiMax = verfuegbareTruppen.SchwereKavallerieAnzahl
						                          , TruppeZweiMin =
							                          dorf.FarmTruppenTemplate.AxtkaempferAnzahlMitSchwereKavallerie
					                          });

				if (
					verfuegbareTruppen.SchwereKavallerieAnzahl >=
					dorf.FarmTruppenTemplate.SchwereKavallerieAnzahlMitPaladin &&
					dorf.FarmTruppenTemplate.SchwereKavallerieAnzahlMitPaladin != 0 &&
					verfuegbareTruppen.PaladinAnzahl >= dorf.FarmTruppenTemplate.PaladinAnzahlMitSchwereKavallerie &&
					dorf.FarmTruppenTemplate.PaladinAnzahlMitSchwereKavallerie != 0 &&
					dorf.FarmTruppenTemplate.PaladinActive && App.Settings.Weltensettings.PaladinActive)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.SchwereKavallerie
						                          , TruppeEinsMax = verfuegbareTruppen.SchwereKavallerieAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate.SchwereKavallerieAnzahlMitPaladin
						                          , TruppeZweiName = Truppeb.Paladin
						                          , TruppeZweiMax = verfuegbareTruppen.PaladinAnzahl
						                          , TruppeZweiMin =
							                          dorf.FarmTruppenTemplate.PaladinAnzahlMitSchwereKavallerie
					                          });
			}


			//Paladin
			if (verfuegbareTruppen.PaladinAnzahl > 0 && dorf.FarmTruppenTemplate.PaladinActive) {
				if (verfuegbareTruppen.PaladinAnzahl >= dorf.FarmTruppenTemplate.PaladinAnzahl &&
				    dorf.FarmTruppenTemplate.PaladinAnzahl != 0)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.Paladin
						                          , TruppeEinsMax = verfuegbareTruppen.PaladinAnzahl
						                          , TruppeEinsMin = dorf.FarmTruppenTemplate.PaladinAnzahl
						                          , TruppeZweiName = ""
						                          , TruppeZweiMax = 0
						                          , TruppeZweiMin = 0
					                          });

				if (verfuegbareTruppen.PaladinAnzahl >= dorf.FarmTruppenTemplate.PaladinAnzahlMitLeichteKavallerie &&
				    dorf.FarmTruppenTemplate.PaladinAnzahlMitLeichteKavallerie != 0 &&
				    verfuegbareTruppen.LeichteKavallerieAnzahl >=
				    dorf.FarmTruppenTemplate.LeichteKavallerieAnzahlMitPaladin &&
				    dorf.FarmTruppenTemplate.LeichteKavallerieAnzahlMitPaladin != 0 &&
				    dorf.FarmTruppenTemplate.LeichteKavallerieActive)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.Paladin
						                          , TruppeEinsMax = verfuegbareTruppen.PaladinAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate.PaladinAnzahlMitLeichteKavallerie
						                          , TruppeZweiName = Truppeb.LeichteKavallerie
						                          , TruppeZweiMax = verfuegbareTruppen.LeichteKavallerieAnzahl
						                          , TruppeZweiMin =
							                          dorf.FarmTruppenTemplate.LeichteKavallerieAnzahlMitPaladin
					                          });
				if (verfuegbareTruppen.PaladinAnzahl >= dorf.FarmTruppenTemplate.PaladinAnzahlMitSchwertkaempfer &&
				    dorf.FarmTruppenTemplate.PaladinAnzahlMitSchwertkaempfer != 0 &&
				    verfuegbareTruppen.SchwertkaempferAnzahl >=
				    dorf.FarmTruppenTemplate.SchwertkaempferAnzahlMitPaladin &&
				    dorf.FarmTruppenTemplate.SchwertkaempferAnzahlMitPaladin != 0 &&
				    dorf.FarmTruppenTemplate.SchwertkaempferActive)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.Paladin
						                          , TruppeEinsMax = verfuegbareTruppen.PaladinAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate.PaladinAnzahlMitSchwertkaempfer
						                          , TruppeZweiName = Truppeb.Schwertkaempfer
						                          , TruppeZweiMax = verfuegbareTruppen.SchwertkaempferAnzahl
						                          , TruppeZweiMin =
							                          dorf.FarmTruppenTemplate.SchwertkaempferAnzahlMitPaladin
					                          });
				if (verfuegbareTruppen.PaladinAnzahl >= dorf.FarmTruppenTemplate.PaladinAnzahlMitBogenschuetzen &&
				    dorf.FarmTruppenTemplate.PaladinAnzahlMitBogenschuetzen != 0 &&
				    verfuegbareTruppen.BogenschuetzenAnzahl >=
				    dorf.FarmTruppenTemplate.BogenschuetzenAnzahlMitPaladin &&
				    dorf.FarmTruppenTemplate.BogenschuetzenAnzahlMitPaladin != 0 &&
				    App.Settings.Weltensettings.BogenschuetzenActive && dorf.FarmTruppenTemplate.BogenschuetzenActive)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.Paladin
						                          , TruppeEinsMax = verfuegbareTruppen.PaladinAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate.PaladinAnzahlMitBogenschuetzen
						                          , TruppeZweiName = Truppeb.Bogenschuetzen
						                          , TruppeZweiMax = verfuegbareTruppen.BogenschuetzenAnzahl
						                          , TruppeZweiMin =
							                          dorf.FarmTruppenTemplate.BogenschuetzenAnzahlMitPaladin
					                          });
				if (verfuegbareTruppen.PaladinAnzahl >= dorf.FarmTruppenTemplate.PaladinAnzahlMitSpeertraeger &&
				    dorf.FarmTruppenTemplate.PaladinAnzahlMitSpeertraeger != 0 &&
				    verfuegbareTruppen.SpeertraegerAnzahl >= dorf.FarmTruppenTemplate.SpeertraegerAnzahlMitPaladin &&
				    dorf.FarmTruppenTemplate.SpeertraegerAnzahlMitPaladin != 0 &&
				    dorf.FarmTruppenTemplate.SpeertraegerActive)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.Paladin
						                          , TruppeEinsMax = verfuegbareTruppen.PaladinAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate.PaladinAnzahlMitSpeertraeger
						                          , TruppeZweiName = Truppeb.Speertraeger
						                          , TruppeZweiMax = verfuegbareTruppen.SpeertraegerAnzahl
						                          , TruppeZweiMin =
							                          dorf.FarmTruppenTemplate.SpeertraegerAnzahlMitPaladin
					                          });
				if (
					verfuegbareTruppen.PaladinAnzahl >=
					dorf.FarmTruppenTemplate.PaladinAnzahlMitBeritteneBogenschuetzen &&
					dorf.FarmTruppenTemplate.PaladinAnzahlMitBeritteneBogenschuetzen != 0 &&
					verfuegbareTruppen.BeritteneBogenschuetzenAnzahl >=
					dorf.FarmTruppenTemplate.BeritteneBogenschuetzenAnzahlMitPaladin &&
					dorf.FarmTruppenTemplate.BeritteneBogenschuetzenAnzahlMitPaladin != 0 &&
					App.Settings.Weltensettings.BogenschuetzenActive &&
					dorf.FarmTruppenTemplate.BeritteneBogenschuetzenActive)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.Paladin
						                          , TruppeEinsMax = verfuegbareTruppen.PaladinAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate.PaladinAnzahlMitBeritteneBogenschuetzen
						                          , TruppeZweiName = Truppeb.BeritteneBogenschuetzen
						                          , TruppeZweiMax = verfuegbareTruppen.BeritteneBogenschuetzenAnzahl
						                          , TruppeZweiMin =
							                          dorf.FarmTruppenTemplate.BeritteneBogenschuetzenAnzahlMitPaladin
					                          });
				if (verfuegbareTruppen.PaladinAnzahl >= dorf.FarmTruppenTemplate.PaladinAnzahlMitSchwereKavallerie &&
				    dorf.FarmTruppenTemplate.PaladinAnzahlMitSchwereKavallerie != 0 &&
				    verfuegbareTruppen.SchwereKavallerieAnzahl >=
				    dorf.FarmTruppenTemplate.SchwereKavallerieAnzahlMitPaladin &&
				    dorf.FarmTruppenTemplate.SchwereKavallerieAnzahlMitPaladin != 0 &&
				    dorf.FarmTruppenTemplate.SchwereKavallerieActive)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.Paladin
						                          , TruppeEinsMax = verfuegbareTruppen.PaladinAnzahl
						                          , TruppeEinsMin =
							                          dorf.FarmTruppenTemplate.PaladinAnzahlMitSchwereKavallerie
						                          , TruppeZweiName = Truppeb.SchwereKavallerie
						                          , TruppeZweiMax = verfuegbareTruppen.SchwereKavallerieAnzahl
						                          , TruppeZweiMin =
							                          dorf.FarmTruppenTemplate.SchwereKavallerieAnzahlMitPaladin
					                          });
				if (verfuegbareTruppen.PaladinAnzahl >= dorf.FarmTruppenTemplate.PaladinAnzahlMitRammboecke &&
				    dorf.FarmTruppenTemplate.PaladinAnzahlMitRammboecke != 0 &&
				    verfuegbareTruppen.RammboeckeAnzahl >= dorf.FarmTruppenTemplate.RammboeckeAnzahlMitPaladin &&
				    dorf.FarmTruppenTemplate.RammboeckeAnzahlMitPaladin != 0 &&
				    dorf.FarmTruppenTemplate.RammboeckeActive)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.Paladin
						                          , TruppeEinsMax = verfuegbareTruppen.PaladinAnzahl
						                          , TruppeEinsMin = dorf.FarmTruppenTemplate.PaladinAnzahlMitRammboecke
						                          , TruppeZweiName = Truppeb.Rammboecke
						                          , TruppeZweiMax = verfuegbareTruppen.RammboeckeAnzahl
						                          , TruppeZweiMin = dorf.FarmTruppenTemplate.RammboeckeAnzahlMitPaladin
					                          });
				if (verfuegbareTruppen.PaladinAnzahl >= dorf.FarmTruppenTemplate.PaladinAnzahlMitKatapulte &&
				    dorf.FarmTruppenTemplate.PaladinAnzahlMitKatapulte != 0 &&
				    verfuegbareTruppen.KatapulteAnzahl >= dorf.FarmTruppenTemplate.KatapulteAnzahlMitPaladin &&
				    dorf.FarmTruppenTemplate.KatapulteAnzahlMitPaladin != 0 && dorf.FarmTruppenTemplate.KatapulteActive)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.Paladin
						                          , TruppeEinsMax = verfuegbareTruppen.PaladinAnzahl
						                          , TruppeEinsMin = dorf.FarmTruppenTemplate.PaladinAnzahlMitKatapulte
						                          , TruppeZweiName = Truppeb.Katapulte
						                          , TruppeZweiMax = verfuegbareTruppen.KatapulteAnzahl
						                          , TruppeZweiMin = dorf.FarmTruppenTemplate.KatapulteAnzahlMitPaladin
					                          });
				if (verfuegbareTruppen.PaladinAnzahl >= dorf.FarmTruppenTemplate.PaladinAnzahlMitAxtkaempfer &&
				    dorf.FarmTruppenTemplate.PaladinAnzahlMitAxtkaempfer != 0 &&
				    verfuegbareTruppen.PaladinAnzahl >= dorf.FarmTruppenTemplate.AxtkaempferAnzahlMitPaladin &&
				    dorf.FarmTruppenTemplate.AxtkaempferAnzahlMitPaladin != 0 &&
				    dorf.FarmTruppenTemplate.PaladinActive && App.Settings.Weltensettings.PaladinActive)
					list.Add(new FarmTemplate {
						                          TruppeEinsName = Truppeb.Paladin
						                          , TruppeEinsMax = verfuegbareTruppen.PaladinAnzahl
						                          , TruppeEinsMin = dorf.FarmTruppenTemplate.PaladinAnzahlMitAxtkaempfer
						                          , TruppeZweiName = Truppeb.Axtkaempfer
						                          , TruppeZweiMax = verfuegbareTruppen.AxtkaempferAnzahl
						                          , TruppeZweiMin = dorf.FarmTruppenTemplate.AxtkaempferAnzahlMitPaladin
					                          });
			}


			return list;
		}
	}
}