﻿using System.Linq;
using System.Threading.Tasks;
using Ds_Bot_Reunited_NEWUI.BrowserManager;
using Ds_Bot_Reunited_NEWUI.Constants;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using OpenQA.Selenium;

namespace Ds_Bot_Reunited_NEWUI.Farmmodul {
    public class Map {
        public static async Task ClickAtt(IWebDriver webDriver) {
            int counter = 0;
            while (webDriver.FindElements(By.XPath(SpielOberflaeche.Karte.Kontextmenu.DORF_ANGREIFEN_SELECTOR_XPATH)).Count == 0 && counter < 3) {
                counter++;
                await Task.Delay(200);
            }
            webDriver.FindElement(By.XPath("//a[@id='mp_att']")).Click();
        }

        public static async Task ClickFarmassistA(Browser browser) {
            int counter = 0;
            while (browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Karte.Kontextmenu.FARMASSISTENT_A_SELECTOR_XPATH)).Count == 0 && counter < 3) {
                counter++;
                await Task.Delay(200);
            }
            browser.ClickByXpath(SpielOberflaeche.Karte.Kontextmenu.FARMASSISTENT_A_SELECTOR_XPATH);
        }

        public static async Task RandomMapNavigation(int i, int random, Browser browser) {
            int counter = 0;
            bool ohnefehler = true;
            do {
                counter++;
                try {
                    switch (random) {
                        case 0: //Start = Rechts, gegen Uhrzeigersinn, Start = Rechts unten, gegen Uhrzeigersinn
                            switch (i) {
                                case 1:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 300);
                                    break;
                                case 2:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 300);
                                    break;
                                case 3:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 4:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 5:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 6:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 7:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 8: //rechts unten
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 9:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 10:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 11:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 12:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 13:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 14:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 15:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 16:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 17:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 18:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 19:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 20:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 21:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 22:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 23:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 24:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                            }
                            break;
                        case 1: //Start = Rechts, mit dem Uhrzeigersinn, Start = Rechts oben, mit dem Uhrzeigersinn
                            switch (i) {
                                case 1:
                                    await ClickMapRight(browser);
                                    break;
                                case 2:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 3:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 4:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 5:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 6:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 7:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 8: //rechts oben
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 9:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 10:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 11:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 12:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 13:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 14:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 15:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 16:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 17:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 18:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 19:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 20:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 21:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 22:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 23:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 24:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                            }
                            break;
                        case 2: //Start = Oben, mit dem Uhrzeigersinn, Start = Oben links, mit dem Uhrzeigersinn
                            switch (i) {
                                case 1:
                                    await ClickMapUp(browser);
                                    break;
                                case 2:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 3:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 4:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 5:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 6:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 7:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 8: // Links oben
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 9:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 10:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 11:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 12:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 13:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 14:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 15:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 16:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 17:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 18:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 19:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 20:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 21:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 22:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 23:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 24:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                            }
                            break;
                        case 3: //Start = Oben, gegen den Uhrzeigersinn, Start = Oben rechts, gegen den Uhrzeigersinn
                            switch (i) {
                                case 1:
                                    await ClickMapUp(browser);
                                    break;
                                case 2:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 3:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 4:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 5:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 6:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 7:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 8: //Oben rechts
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 9:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 10:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 11:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 12:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 13:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 14:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 15:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 16:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 17:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 18:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 19:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 20:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 21:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 22:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 23:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 24:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                            }
                            break;
                        case 4: //Start = Links, mit dem Uhrzeigersinn, Start = Unten links, mit dem Uhrzeigersinn
                            switch (i) {
                                case 1:
                                    await ClickMapLeft(browser);
                                    break;
                                case 2:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 3:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 4:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 5:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 6:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 7:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 8:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 9:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 10:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 11:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 12:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 13:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 14:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 15:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 16:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 17:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 18:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 19:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 20:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 21:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 22:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 23:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 24:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                            }
                            break;
                        case 5: //Start = Links, gegen den Uhrzeigersinn,Start = unten links, gegen den Uhrzeigersinn
                            switch (i) {
                                case 1:
                                    await ClickMapLeft(browser);
                                    break;
                                case 2:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 3:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 4:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 5:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 6:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 7:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 8:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 9:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 10:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 11:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 12:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 13:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 14:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 15:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 16:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 17:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 18:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 19:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 20:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 21:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 22:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 23:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 24:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                            }
                            break;
                        case 6: //Start = Unten, mit dem Uhrzeigersinn, Start = Unten rechts, mit dem Uhrzeigersinn
                            switch (i) {
                                case 1:
                                    await ClickMapDown(browser);
                                    break;
                                case 2:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 3:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 4:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 5:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 6:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 7:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 8:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 9:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 10:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 11:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 12:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 13:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 14:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 15:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 16:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 17:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 18:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 19:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 20:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 21:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 22:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 23:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 24:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                            }
                            break;
                        case 7: //Start = Unten, gegen den Uhrzeigersinn, Start = Unten links, gegen den Uhrzeigersinn
                            switch (i) {
                                case 1:
                                    await ClickMapDown(browser);
                                    break;
                                case 2:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 3:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 4:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 5:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 6:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 7:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 8:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 9:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 10:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 11:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 12:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 13:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 14:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 15:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 16:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 17:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 18:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 19:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 20:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 21:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 22:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 23:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 24:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                            }
                            break;
                        case 8: //Start = Rechtsunten, mit dem Uhrzeigersinn, Start = Rechts, mit dem Uhrzeigersinn
                            switch (i) {
                                case 1:
                                    await ClickMapDown(browser);
                                    await ClickMapRight(browser);
                                    break;
                                case 2:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 3:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 4:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 5:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 6:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 7:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 8:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 9:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 10:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 11:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 12:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 13:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 14:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 15:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 16:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 17:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 18:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 19:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 20:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 21:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 22:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 23:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 24:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                            }
                            break;
                        case 9: //Start = Rechtsunten, gegen den Uhrzeigersinn, Start = Unten, gegen den Uhrzeigersinn
                            switch (i) {
                                case 1:
                                    await ClickMapDown(browser);
                                    await ClickMapRight(browser);
                                    break;
                                case 2:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 3:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 4:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 5:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 6:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 7:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 8:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 9:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 10:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 11:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 12:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 13:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 14:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 15:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 16:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 17:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 18:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 19:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 20:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 21:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 22:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 23:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 24:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                            }
                            break;
                        case 10: //Start = Rechtsoben, mit dem Uhrzeigersinn, Start = oben, mit dem Uhrzeigersinn
                            switch (i) {
                                case 1:
                                    await ClickMapUp(browser);
                                    await ClickMapRight(browser);
                                    break;
                                case 2:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 3:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 4:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 5:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 6:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 7:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 8:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 9:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 10:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 11:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 12:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 13:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 14:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 15:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 16:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 17:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 18:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 19:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 20:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 21:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 22:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 23:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 24:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                            }
                            break;
                        case 11: //Start = Rechtsoben, gegen den Uhrzeigersinn, Start = Rechts gegen den Uhrzeigersinn,
                            switch (i) {
                                case 1:
                                    await ClickMapUp(browser);
                                    await ClickMapRight(browser);
                                    break;
                                case 2:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 3:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 4:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 5:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 6:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 7:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 8:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 9:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 10:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 11:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 12:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 13:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 14:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 15:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 16:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 17:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 18:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 19:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 20:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 21:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 22:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 23:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 24:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                            }
                            break;
                        case 12: //Start = Linksoben, mit dem Uhrzeigersinn, Start = Links, mit dem Uhrzeigersinn
                            switch (i) {
                                case 1:
                                    await ClickMapUp(browser);
                                    await ClickMapLeft(browser);
                                    break;
                                case 2:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 3:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 4:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 5:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 6:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 7:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 8:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 9:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 10:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 11:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 12:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 13:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 14:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 15:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 16:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 17:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 18:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 19:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 20:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 21:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 22:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 23:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 24:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                            }
                            break;
                        case 13: //Start = Linksoben, gegen den Uhrzeigersinn, Start = Oben, gegen den Uhrzeigersinn
                            switch (i) {
                                case 1:
                                    await ClickMapUp(browser);
                                    await ClickMapLeft(browser);
                                    break;
                                case 2:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 3:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 4:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 5:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 6:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 7:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 8:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 9:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 10:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 11:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 12:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 13:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 14:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 15:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 16:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 17:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 18:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 19:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 20:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 21:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 22:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 23:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 24:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                            }
                            break;
                        case 14: //Start = Linksunten, mit dem Uhrzeigersinn, Start = Unten, mit dem Uhrzeigersinn
                            switch (i) {
                                case 1:
                                    await ClickMapDown(browser);
                                    await ClickMapLeft(browser);
                                    break;
                                case 2:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 3:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 4:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 5:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 6:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 7:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 8:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 9:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 10:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 11:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 12:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 13:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 14:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 15:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 16:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 17:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 18:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 19:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 20:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 21:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 22:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 23:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 24:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                            }
                            break;
                        case 15: //Start = Linksunten, gegen den Uhrzeigersinn, Start = Links, gegen den Uhrzeigersinn
                            switch (i) {
                                case 1:
                                    await ClickMapDown(browser);
                                    await ClickMapLeft(browser);
                                    break;
                                case 2:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 3:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 4:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 5:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 6:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 7:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 8:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 9:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 10:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 11:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 12:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 13:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 14:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 15:
                                    await ClickMapRight(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 16:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 17:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 18:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 19:
                                    await ClickMapDown(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 20:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 21:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 22:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 23:
                                    await ClickMapLeft(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                                case 24:
                                    await ClickMapUp(browser);
                                    await Rndm.Sleep(100, 400);
                                    break;
                            }
                            break;
                    }
                } catch {
                    ohnefehler = false;
                }
            } while (!ohnefehler && counter < 2);
        }

        public static async Task ClickMap(Browser browser) {
            browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Allgemein.HEADERMENU_KARTE_SELECTOR_XPATH)).Click();
            await Rndm.Sleep(100, 300);
        }

        public static async Task ClickMapUp(Browser browser) {
            try {
                if (!browser.WebDriver.Url.Contains("map")) {
                    await ClickMap(browser);
                }
                browser.ClickElement(browser.WebDriver.FindElements(By.ClassName("map_navigation")).ToList()[0]);
            } catch (WebDriverException) { }
        }

        public static async Task ClickMapDown(Browser browser) {
            try {
                if (!browser.WebDriver.Url.Contains("map")) {
                    await ClickMap(browser);
                }
                browser.ClickElement(browser.WebDriver.FindElements(By.ClassName("map_navigation")).ToList()[3]);
            } catch (WebDriverException) { }
        }

        public static async Task ClickMapRight(Browser browser) {
            try {
                if (!browser.WebDriver.Url.Contains("map")) {
                    await ClickMap(browser);
                }
                browser.ClickElement(browser.WebDriver.FindElements(By.ClassName("map_navigation")).ToList()[2]);
            } catch (WebDriverException) { }
        }

        public static async Task ClickMapLeft(Browser browser) {
            try {
                if (!browser.WebDriver.Url.Contains("map")) {
                    await ClickMap(browser);
                }
                browser.ClickElement(browser.WebDriver.FindElements(By.ClassName("map_navigation")).ToList()[1]);
            } catch (WebDriverException) { }
        }
    }
}