﻿using System;

namespace Ds_Bot_Reunited_NEWUI.Farmmodul {
    [Serializable]
    public class FarmTemplate {
        public string TruppeEinsName { get; set; }
        public int TruppeEinsMax { get; set; }
        public int TruppeEinsMin { get; set; }
        public string TruppeZweiName { get; set; }
        public int TruppeZweiMax { get; set; }
        public int TruppeZweiMin { get; set; }
    }
}