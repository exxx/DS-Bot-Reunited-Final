﻿using System;
using System.Collections.Concurrent;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Input;
using Ds_Bot_Reunited_NEWUI.AngreifenModul;
using Ds_Bot_Reunited_NEWUI.Annotations;
using Ds_Bot_Reunited_NEWUI.AttackDetectionModul;
using Ds_Bot_Reunited_NEWUI.Attackplaner;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.Multiaccounting;
using Ds_Bot_Reunited_NEWUI.Settings;
using Ds_Bot_Reunited_NEWUI.Spieldaten;

namespace Ds_Bot_Reunited_NEWUI.Accountsettings {
    namespace Ds_Bot_Reunited.Accountsettings {
        [Obfuscation(Exclude = true, Feature = "renaming")]
        public class PremiumDepotRessource : INotifyPropertyChanged {
            private bool _active;
            private int _priority;
            private string _ressourcee;

            public bool Active {
                get => _active;
                set {
                    _active = value;
                    OnPropertyChanged();
                }
            }

            public string Ressource {
                get => _ressourcee;
                set {
                    _ressourcee = value;
                    OnPropertyChanged();
                }
            }

            public int Priority {
                get => _priority;
                set {
                    _priority = value;
                    OnPropertyChanged();
                }
            }

            public event PropertyChangedEventHandler PropertyChanged;

            public PremiumDepotRessource Clone() {
                var premium = new PremiumDepotRessource {Priority = Priority, Ressource = Ressource, Active = Active};
                return premium;
            }

            [NotifyPropertyChangedInvocator]
            protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }


        [Obfuscation(Exclude = true, Feature = "renaming")]
        public class Accountsetting : INotifyPropertyChanged {
            private string _accountname;
            private float _accountPrioritaet;
            private bool _automaticLogoutActive;
            private bool _bauenActive;
            private int _baueninterval;
            private bool _bauenQueued;
            private bool _defendQueued;
            private AsyncObservableCollection<AccountDorfSettings> _doerferSettings;
            private bool _farmenActive;
            private int _farmeninterval;
            private bool _farmenQueued;
            private bool _forschenActive;
            private int _forscheninterval;
            private bool _forschenQueued;
            private AsyncObservableCollection<Truppenbewegung> _geplanteBewegungen;
            private bool _goldCoinsActive;
            private int _goldCoinsinterval;
            private bool _goldCoinsQueued;
            private bool _handelnActive;
            private int _handelninterval;
            private bool _handelQueued;
            private DateTime _lastBauenZeitpunkt;
            private DateTime _lastDorfUebersichtCheck;
            private DateTime _lastFarmenZeitpunkt;
            private DateTime _lastForschenZeitpunkt;
            private DateTime _lastGoldCoinsZeitpunkt;
            private DateTime _lastHandelZeitpunkt;
            private ConcurrentBag<DateTime> _lastLogins;
            private DateTime _lastNachrichtenZeitpunkt;
            private DateTime _lastRaubzugZeitpunkt;
            private DateTime _lastRekrutierenZeitpunkt;
            private DateTime _lastReportReading;
            private DateTime _lastTruppeneinlesen;
            private DateTime _lastUnterstuetzungeneinlesen;
            private bool _loginActive;
            private ICommand _loginCommand;
            private bool _massenAngriffeActive;
            private int _maxHoursToCountBerichteForErfolgsquotient = 5;
            private int _maxLoginsInEinerStunde;
            private MultiaccountingDaten _multiaccountingDaten;
            private bool _nachrichtenAntwortenActive;
            private bool _nachrichtenLesenActive;
            private int _nachrichtenLeseninterval;
            private DateTime _nextAvailableBauschleife;
            private DateTime _nextAvailableTroopAtHomeAt;
            private bool _overallActive;
            private string _passwort;
            private bool _pleaseQueueDefend;
            private bool _premiumDepotActive;
            private string _proxyDrei;
            private string _proxyEins;
            private string _proxyVier;
            private string _proxyZwei;
            private bool _questsActive;
            private bool _raubzugActive;
            private int _raubzuginterval;
            private bool _rausstellenQueued;
            private bool _readForumActive;
            private string _registrierungsemail;
            private string _registrierungsemailpasswort;
            private DateTime _registrierungszeitpunkt;
            private bool _rekrutierenActive;
            private int _rekrutiereninterval;
            private bool _rekrutierenQueued;
            private bool _reloggingActive;
            private int _relogOnSessionLost;
            private bool _sellMode;
            private bool _sendingActive;
            private bool _setBoosterActive;
            private bool _setFlaggeActive;
            private bool _setInventoryItemsActive;
            private ICommand _startWithProxyCommand;
            private bool _useRegressionForErfolgsquotient = true;
            private string _uvname;

            public object DoerferSettingsLock { get; set; } = new object();
            public object GeplanteBewegungenLock { get; set; } = new object();
            public AsyncObservableCollection<Accountmode> Accountmodes { get; set; }

            public bool LoadWorlddata { get; set; } = true;

            public int Playerid { get; set; }

            public string UvZusatzFuerLink => string.IsNullOrEmpty(Uvname) ? "" : "&t=" + App.PlayerlistWholeWorld.FirstOrDefault(x => x.PlayerName.Equals(Uvname))?.PlayerId;

            public AsyncObservableCollection<AttackOnOwnVillage> AngriffeAufAccount { get; set; }

            public object AngriffeAufAccountLock { get; set; } = new object();
            public object UnterstuetzungenLock { get; set; } = new object();
            public object BefehleLock { get; set; } = new object();

            public DateTime LastAdjusted { get; set; }

            public bool AutoPilot { get; set; }

            public AsyncObservableCollection<Unterstuetzung> Unterstuetzungen { get; set; }
            public AsyncObservableCollection<Befehl> Befehle { get; set; }

            public bool PleaseQueueDefend {
                get => _pleaseQueueDefend;
                set {
                    _pleaseQueueDefend = value;
                    OnPropertyChanged();
                }
            }

            public int MaxHoursToCountBerichteForErfolgsquotient {
                get => _maxHoursToCountBerichteForErfolgsquotient;
                set {
                    _maxHoursToCountBerichteForErfolgsquotient = value;
                    OnPropertyChanged();
                }
            }

            public bool UseRegressionForErfolgsquotient {
                get => _useRegressionForErfolgsquotient;
                set {
                    _useRegressionForErfolgsquotient = value;
                    OnPropertyChanged();
                }
            }

            public bool GoldCoinsQueued {
                get => _goldCoinsQueued;
                set {
                    _goldCoinsQueued = value;
                    OnPropertyChanged();
                }
            }

            public bool ForschenQueued {
                get => _forschenQueued;
                set {
                    _forschenQueued = value;
                    OnPropertyChanged();
                }
            }


            public bool LoginActive {
                get => _loginActive;
                set {
                    _loginActive = value;
                    OnPropertyChanged();
                }
            }

            public bool SellMode {
                get => _sellMode;
                set {
                    _sellMode = value;
                    OnPropertyChanged();
                }
            }

            public bool IsSelected { get; set; }

            public bool ReloggingActive {
                get => _reloggingActive;
                set {
                    _reloggingActive = value;
                    OnPropertyChanged();
                }
            }

            public int RelogOnSessionLostAfterMinutes {
                get => _relogOnSessionLost;
                set {
                    _relogOnSessionLost = value;
                    OnPropertyChanged();
                }
            }

            public string Accountname {
                get => _accountname;
                set {
                    _accountname = value;
                    if (LoadWorlddata) {
                        Worlddata.LoadPlayerVillages();
                        if (Playerid == 0) {
                            if (!string.IsNullOrEmpty(value)) {
                                if (App.PlayerlistWholeWorld != null && App.PlayerlistWholeWorld.Any(x => x.PlayerName.Equals(Accountname, StringComparison.CurrentCultureIgnoreCase))) {
                                    var first = App.PlayerlistWholeWorld.FirstOrDefault(x => x.PlayerName.Equals(Accountname, StringComparison.CurrentCultureIgnoreCase));
                                    if (first != null) {
                                        Playerid = first.PlayerId;
                                    }
                                }
                            }
                        }
                    }

                    OnPropertyChanged();
                }
            }

            public string Passwort {
                get => _passwort;
                set {
                    _passwort = value;
                    OnPropertyChanged();
                }
            }

            public string Uvname {
                get => _uvname;
                set {
                    _uvname = value;
                    if (LoadWorlddata) {
                        Worlddata.LoadPlayerVillages();
                        if (!string.IsNullOrEmpty(value)) {
                            var first = App.PlayerlistWholeWorld.FirstOrDefault(x => x.PlayerName.Equals(Uvname));
                            if (first != null) {
                                Playerid = first.PlayerId;
                            }
                        }
                    }

                    OnPropertyChanged();
                }
            }

            public float AccountPrioritaet {
                get => _accountPrioritaet;
                set {
                    _accountPrioritaet = value;
                    OnPropertyChanged();
                }
            }


            public bool FarmenQueued {
                get => _farmenQueued;
                set {
                    _farmenQueued = value;
                    OnPropertyChanged();
                }
            }

            public bool RekrutierenQueued {
                get => _rekrutierenQueued;
                set {
                    _rekrutierenQueued = value;
                    OnPropertyChanged();
                }
            }

            public bool DefendQueued {
                get => _defendQueued;
                set {
                    _defendQueued = value;
                    OnPropertyChanged();
                }
            }

            public bool BauenQueued {
                get => _bauenQueued;
                set {
                    _bauenQueued = value;
                    OnPropertyChanged();
                }
            }

            public bool RausstellenQueued {
                get => _rausstellenQueued;
                set {
                    _rausstellenQueued = value;
                    OnPropertyChanged();
                }
            }

            public int MaxLoginsInEinerStunde {
                get => _maxLoginsInEinerStunde;
                set {
                    _maxLoginsInEinerStunde = value;
                    OnPropertyChanged();
                }
            }

            public DateTime Registrierungszeitpunkt {
                get => _registrierungszeitpunkt;
                set {
                    _registrierungszeitpunkt = value;
                    OnPropertyChanged();
                }
            }

            public string Registrierungsemail {
                get => _registrierungsemail;
                set {
                    _registrierungsemail = value;
                    OnPropertyChanged();
                }
            }

            public string Registrierungsemailpasswort {
                get => _registrierungsemailpasswort;
                set {
                    _registrierungsemailpasswort = value;
                    OnPropertyChanged();
                }
            }

            public bool AutomaticLogoutActive {
                get => _automaticLogoutActive;
                set {
                    _automaticLogoutActive = value;
                    OnPropertyChanged();
                }
            }

            public int Farmeninterval {
                get => _farmeninterval;
                set {
                    _farmeninterval = value;
                    OnPropertyChanged();
                }
            }

            public int Baueninterval {
                get => _baueninterval;
                set {
                    _baueninterval = value;
                    OnPropertyChanged();
                }
            }

            public int Rekrutiereninterval {
                get => _rekrutiereninterval;
                set {
                    _rekrutiereninterval = value;
                    OnPropertyChanged();
                }
            }

            public int GoldCoinsinterval {
                get => _goldCoinsinterval;
                set {
                    _goldCoinsinterval = value;
                    OnPropertyChanged();
                }
            }

            public bool HandelQueued {
                get => _handelQueued;
                set {
                    _handelQueued = value;
                    OnPropertyChanged();
                }
            }

            public DateTime LastDorfUebersichtCheck {
                get => _lastDorfUebersichtCheck;
                set {
                    _lastDorfUebersichtCheck = value;
                    OnPropertyChanged();
                }
            }

            public int Handelninterval {
                get => _handelninterval;
                set {
                    _handelninterval = value;
                    OnPropertyChanged();
                }
            }

            public int NachrichtenLeseninterval {
                get => _nachrichtenLeseninterval;
                set {
                    _nachrichtenLeseninterval = value;
                    OnPropertyChanged();
                }
            }

            public bool OverallActive {
                get => _overallActive;
                set {
                    _overallActive = value;
                    OnPropertyChanged();
                }
            }

            public bool FarmenActive {
                get => _farmenActive;
                set {
                    _farmenActive = value;
                    OnPropertyChanged();
                }
            }

            public bool BauenActive {
                get => _bauenActive;
                set {
                    _bauenActive = value;
                    OnPropertyChanged();
                }
            }

            public bool RekrutierenActive {
                get => _rekrutierenActive;
                set {
                    _rekrutierenActive = value;
                    OnPropertyChanged();
                }
            }

            public bool ForschenActive {
                get => _forschenActive;
                set {
                    _forschenActive = value;
                    OnPropertyChanged();
                }
            }

            public int Forscheninterval {
                get => _forscheninterval;
                set {
                    _forscheninterval = value;
                    OnPropertyChanged();
                }
            }

            public int Raubzuginterval {
                get => _raubzuginterval;
                set {
                    _raubzuginterval = value;
                    OnPropertyChanged();
                }
            }

            public bool PremiumDepotActive {
                get => _premiumDepotActive;
                set {
                    _premiumDepotActive = value;
                    OnPropertyChanged();
                }
            }

            public bool HandelnActive {
                get => _handelnActive;
                set {
                    _handelnActive = value;
                    OnPropertyChanged();
                }
            }

            public bool SendingActive {
                get => _sendingActive;
                set {
                    _sendingActive = value;
                    OnPropertyChanged();
                }
            }

            public bool MassenAngriffeActive {
                get => _massenAngriffeActive;
                set {
                    _massenAngriffeActive = value;
                    OnPropertyChanged();
                }
            }

            public bool NachrichtenLesenActive {
                get => _nachrichtenLesenActive;
                set {
                    _nachrichtenLesenActive = value;
                    OnPropertyChanged();
                }
            }

            public bool NachrichtenAntwortenActive {
                get => _nachrichtenAntwortenActive;
                set {
                    _nachrichtenAntwortenActive = value;
                    OnPropertyChanged();
                }
            }

            public bool QuestsActive {
                get => _questsActive;
                set {
                    _questsActive = value;
                    OnPropertyChanged();
                }
            }

            public bool RaubzugActive {
                get => _raubzugActive;
                set {
                    _raubzugActive = value;
                    OnPropertyChanged();
                }
            }

            public bool ReadForumActive {
                get => _readForumActive;
                set {
                    _readForumActive = value;
                    OnPropertyChanged();
                }
            }

            public DateTime NextAvailableBauschleife {
                get => _nextAvailableBauschleife;
                set {
                    _nextAvailableBauschleife = value;
                    OnPropertyChanged();
                }
            }

            public DateTime LastFarmenZeitpunkt {
                get => _lastFarmenZeitpunkt;
                set {
                    _lastFarmenZeitpunkt = value;
                    OnPropertyChanged();
                }
            }

            public DateTime LastBauenZeitpunkt {
                get => _lastBauenZeitpunkt;
                set {
                    _lastBauenZeitpunkt = value;
                    OnPropertyChanged();
                }
            }

            public DateTime LastRekrutierenZeitpunkt {
                get => _lastRekrutierenZeitpunkt;
                set {
                    _lastRekrutierenZeitpunkt = value;
                    OnPropertyChanged();
                }
            }

            public DateTime LastHandelZeitpunkt {
                get => _lastHandelZeitpunkt;
                set {
                    _lastHandelZeitpunkt = value;
                    OnPropertyChanged();
                }
            }

            public DateTime LastNachrichtenZeitpunkt {
                get => _lastNachrichtenZeitpunkt;
                set {
                    _lastNachrichtenZeitpunkt = value;
                    OnPropertyChanged();
                }
            }

            public DateTime LastRaubzugZeitpunkt {
                get => _lastRaubzugZeitpunkt;
                set {
                    _lastRaubzugZeitpunkt = value;
                    OnPropertyChanged();
                }
            }

            public DateTime NextAvailableTroopAtHomeAt {
                get => _nextAvailableTroopAtHomeAt;
                set {
                    _nextAvailableTroopAtHomeAt = value;
                    OnPropertyChanged();
                }
            }

            public DateTime LastGoldCoinsZeitpunkt {
                get => _lastGoldCoinsZeitpunkt;
                set {
                    _lastGoldCoinsZeitpunkt = value;
                    OnPropertyChanged();
                }
            }

            public ConcurrentBag<DateTime> LastLogins {
                get => _lastLogins;
                set {
                    _lastLogins = value;
                    OnPropertyChanged();
                }
            }

            public DateTime LastReportReading {
                get => _lastReportReading;
                set {
                    _lastReportReading = value;
                    OnPropertyChanged();
                }
            }


            public bool CanLoginAndDoSomething {
                get {
                    if (LastLogins != null && LastLogins.Any()) {
                        if (LastLogins.Count(x => (DateTime.Now - x).TotalMinutes < 60) < MaxLoginsInEinerStunde) {
                            return true;
                        }

                        return false;
                    }

                    return true;
                }
            }

            public DateTime LastTruppeneinlesen {
                get => _lastTruppeneinlesen;
                set {
                    _lastTruppeneinlesen = value;
                    OnPropertyChanged();
                }
            }

            public DateTime LastForschenZeitpunkt {
                get => _lastForschenZeitpunkt;
                set {
                    _lastForschenZeitpunkt = value;
                    OnPropertyChanged();
                }
            }

            public DateTime LastUnterstuetzungeneinlesen {
                get => _lastUnterstuetzungeneinlesen;
                set {
                    _lastUnterstuetzungeneinlesen = value;
                    OnPropertyChanged();
                }
            }

            public AsyncObservableCollection<AccountDorfSettings> DoerferSettings {
                get => _doerferSettings;
                set {
                    _doerferSettings = value;
                    OnPropertyChanged();
                }
            }

            public AsyncObservableCollection<Truppenbewegung> GeplanteBewegungen {
                get => _geplanteBewegungen;
                set {
                    _geplanteBewegungen = value;
                    OnPropertyChanged();
                }
            }

            public string ProxyZwei {
                get => _proxyZwei;
                set {
                    _proxyZwei = value;
                    OnPropertyChanged();
                }
            }

            public string ProxyVier {
                get => _proxyVier;
                set {
                    _proxyVier = value;
                    OnPropertyChanged();
                }
            }

            public string ProxyEins {
                get => _proxyEins;
                set {
                    _proxyEins = value;
                    OnPropertyChanged();
                }
            }

            public string ProxyDrei {
                get => _proxyDrei;
                set {
                    _proxyDrei = value;
                    OnPropertyChanged();
                }
            }

            public MultiaccountingDaten MultiaccountingDaten {
                get => _multiaccountingDaten;
                set {
                    _multiaccountingDaten = value;
                    OnPropertyChanged();
                }
            }

            public bool GoldCoinsActive {
                get => _goldCoinsActive;
                set {
                    _goldCoinsActive = value;
                    OnPropertyChanged();
                }
            }

            public bool SetFlaggeActive {
                get => _setFlaggeActive;
                set {
                    _setFlaggeActive = value;
                    OnPropertyChanged();
                }
            }

            public bool SetBoosterActive {
                get => _setBoosterActive;
                set {
                    _setBoosterActive = value;
                    OnPropertyChanged();
                }
            }

            public bool SetInventoryItemsActive {
                get => _setInventoryItemsActive;
                set {
                    _setInventoryItemsActive = value;
                    OnPropertyChanged();
                }
            }

            public ICommand LoginCommand => _loginCommand ?? (_loginCommand = new AsyncRelayCommand(Login, () => MultiaccountingDaten.Accountmode == Accountmode.Botaccount));

            public ICommand StartWithProxyCommand => _startWithProxyCommand ?? (_startWithProxyCommand = new AsyncRelayCommand(StartWithProxy, () => MultiaccountingDaten.Accountmode == Accountmode.Botaccount));


            public event PropertyChangedEventHandler PropertyChanged;

            public Accountsetting(string accountname, string uvname) {
                LoadWorlddata = false;
                Accountname = accountname;
                Uvname = uvname;
                LoadWorlddata = true;
                Befehle = new AsyncObservableCollection<Befehl>();
                DoerferSettings = new AsyncObservableCollection<AccountDorfSettings>();
                GeplanteBewegungen = new AsyncObservableCollection<Truppenbewegung>();
                Unterstuetzungen = new AsyncObservableCollection<Unterstuetzung>();
                AngriffeAufAccount = new AsyncObservableCollection<AttackOnOwnVillage>();
                BindingOperations.EnableCollectionSynchronization(DoerferSettings, DoerferSettingsLock);
                BindingOperations.EnableCollectionSynchronization(GeplanteBewegungen, GeplanteBewegungenLock);
                BindingOperations.EnableCollectionSynchronization(AngriffeAufAccount, AngriffeAufAccountLock);
                BindingOperations.EnableCollectionSynchronization(Unterstuetzungen, UnterstuetzungenLock);
                BindingOperations.EnableCollectionSynchronization(Befehle, BefehleLock);
            }

            public Accountsetting() {
                Accountmodes = new AsyncObservableCollection<Accountmode> {Accountmode.Botaccount, Accountmode.Mainaccount};
                LoginActive = true;
                DoerferSettings = new AsyncObservableCollection<AccountDorfSettings>();
                GeplanteBewegungen = new AsyncObservableCollection<Truppenbewegung>();
                MaxLoginsInEinerStunde = 10;
                Farmeninterval = 20;
                Baueninterval = 20;
                Rekrutiereninterval = 30;
                Handelninterval = 30;
                NachrichtenLeseninterval = 120;
                GoldCoinsinterval = 30;
                OverallActive = true;
                FarmenActive = true;
                BauenActive = true;
                RekrutierenActive = true;
                PremiumDepotActive = false;
                RaubzugActive = false;
                HandelnActive = false;
                SendingActive = false;
                MassenAngriffeActive = false;
                NachrichtenLesenActive = true;
                NachrichtenAntwortenActive = false;
                QuestsActive = true;
                AutomaticLogoutActive = true;
                ForschenActive = true;
                Forscheninterval = 20;
                ReloggingActive = true;
                LastLogins = new ConcurrentBag<DateTime>();
                MultiaccountingDaten = new MultiaccountingDaten();
                BindingOperations.EnableCollectionSynchronization(DoerferSettings, DoerferSettingsLock);
                BindingOperations.EnableCollectionSynchronization(GeplanteBewegungen, GeplanteBewegungenLock);
                Befehle = new AsyncObservableCollection<Befehl>();
                Unterstuetzungen = new AsyncObservableCollection<Unterstuetzung>();
                AngriffeAufAccount = new AsyncObservableCollection<AttackOnOwnVillage>();
                BindingOperations.EnableCollectionSynchronization(AngriffeAufAccount, AngriffeAufAccountLock);
                BindingOperations.EnableCollectionSynchronization(Unterstuetzungen, UnterstuetzungenLock);
                BindingOperations.EnableCollectionSynchronization(Befehle, BefehleLock);
            }

            public Accountsetting(string accountname, bool mainaccount = false) {
                DoerferSettings = new AsyncObservableCollection<AccountDorfSettings>();
                Accountmodes = new AsyncObservableCollection<Accountmode> {Accountmode.Botaccount, Accountmode.Mainaccount};
                GeplanteBewegungen = new AsyncObservableCollection<Truppenbewegung>();
                Accountname = accountname;
                MaxLoginsInEinerStunde = 10;
                Farmeninterval = 20;
                Baueninterval = 20;
                Rekrutiereninterval = 30;
                Handelninterval = 30;
                NachrichtenLeseninterval = 120;
                GoldCoinsinterval = 30;
                OverallActive = true;
                BauenActive = true;
                RekrutierenActive = true;
                PremiumDepotActive = false;
                HandelnActive = false;
                SendingActive = false;
                MassenAngriffeActive = false;
                NachrichtenLesenActive = true;
                NachrichtenAntwortenActive = false;
                QuestsActive = true;
                AutomaticLogoutActive = true;
                ForschenActive = true;
                Forscheninterval = 20;
                ReloggingActive = true;
                RaubzugActive = false;
                LoginActive = true;
                BindingOperations.EnableCollectionSynchronization(DoerferSettings, DoerferSettingsLock);
                BindingOperations.EnableCollectionSynchronization(GeplanteBewegungen, GeplanteBewegungenLock);
                LastLogins = new ConcurrentBag<DateTime>();
                MultiaccountingDaten = new MultiaccountingDaten();
                if (mainaccount) {
                    MultiaccountingDaten.Accountmode = Accountmode.Mainaccount;
                    FarmenActive = true;
                    AutomaticLogoutActive = false;
                }
                else {
                    AutomaticLogoutActive = true;
                    FarmenActive = false;
                    MultiaccountingDaten.Accountmode = Accountmode.Botaccount;
                }

                Befehle = new AsyncObservableCollection<Befehl>();
                Unterstuetzungen = new AsyncObservableCollection<Unterstuetzung>();
                AngriffeAufAccount = new AsyncObservableCollection<AttackOnOwnVillage>();
                BindingOperations.EnableCollectionSynchronization(AngriffeAufAccount, AngriffeAufAccountLock);
                BindingOperations.EnableCollectionSynchronization(Unterstuetzungen, UnterstuetzungenLock);
                BindingOperations.EnableCollectionSynchronization(Befehle, BefehleLock);
            }


            public void Reset() {
                LoginActive = true;
                DoerferSettings.Clear();
                MaxLoginsInEinerStunde = 10;
                Farmeninterval = 20;
                Baueninterval = 20;
                Rekrutiereninterval = 30;
                Handelninterval = 30;
                NachrichtenLeseninterval = 120;
                GoldCoinsinterval = 30;
                OverallActive = true;
                FarmenActive = true;
                BauenActive = true;
                RekrutierenActive = true;
                PremiumDepotActive = false;
                HandelnActive = false;
                SendingActive = false;
                MassenAngriffeActive = false;
                NachrichtenLesenActive = true;
                NachrichtenAntwortenActive = false;
                QuestsActive = true;
                AutomaticLogoutActive = true;
                ReloggingActive = true;
                MultiaccountingDaten = new MultiaccountingDaten();
                Befehle = new AsyncObservableCollection<Befehl>();
                Unterstuetzungen = new AsyncObservableCollection<Unterstuetzung>();
                AngriffeAufAccount = new AsyncObservableCollection<AttackOnOwnVillage>();
                BindingOperations.EnableCollectionSynchronization(AngriffeAufAccount, AngriffeAufAccountLock);
                SetFlaggeActive = true;
                SetBoosterActive = true;
            }

            private async Task Login() {
                await Task.Run(() => {
                    if (MultiaccountingDaten.Accountmode == Accountmode.Botaccount) {
                        if (MultiaccountingDaten.Accountmode == Accountmode.Botaccount && !string.IsNullOrEmpty(ProxyEins)) {
                            BrowserManager.BrowserManager.GetFreeBrowser(this, ProxyEins, false, true, true, true);
                        }
                    }
                });
            }

            //popup_box_browser_notification_prompt
            private async Task StartWithProxy() {
                await Task.Run(() => {
                    if (MultiaccountingDaten.Accountmode == Accountmode.Botaccount) {
                        if (MultiaccountingDaten.Accountmode == Accountmode.Botaccount && !string.IsNullOrEmpty(ProxyEins)) {
                            BrowserManager.BrowserManager.GetFreeBrowser(this, ProxyEins, false, false, true, true);
                        }
                    }
                });
            }

            public Accountsetting Clone() {
                var newsettings = new Accountsetting(Accountname, Uvname) {
                    Passwort = Passwort,
                    SellMode = SellMode,
                    LastDorfUebersichtCheck = LastDorfUebersichtCheck,
                    Farmeninterval = Farmeninterval,
                    Baueninterval = Baueninterval,
                    Rekrutiereninterval = Rekrutiereninterval,
                    LastLogins = new ConcurrentBag<DateTime>(),
                    FarmenActive = FarmenActive,
                    RekrutierenActive = RekrutierenActive,
                    BauenActive = BauenActive,
                    PremiumDepotActive = PremiumDepotActive,
                    DoerferSettings = new AsyncObservableCollection<AccountDorfSettings>(),
                    MaxLoginsInEinerStunde = MaxLoginsInEinerStunde,
                    Registrierungszeitpunkt = Registrierungszeitpunkt,
                    ProxyEins = ProxyEins,
                    ProxyZwei = ProxyZwei,
                    ProxyDrei = ProxyDrei,
                    ProxyVier = ProxyVier,
                    SendingActive = SendingActive,
                    HandelnActive = HandelnActive,
                    Handelninterval = Handelninterval,
                    MassenAngriffeActive = MassenAngriffeActive,
                    OverallActive = OverallActive,
                    MultiaccountingDaten = MultiaccountingDaten.Clone(),
                    NachrichtenAntwortenActive = NachrichtenAntwortenActive,
                    NachrichtenLesenActive = NachrichtenLesenActive,
                    NachrichtenLeseninterval = NachrichtenLeseninterval,
                    QuestsActive = QuestsActive,
                    ReadForumActive = ReadForumActive,
                    AccountPrioritaet = AccountPrioritaet,
                    AutomaticLogoutActive = AutomaticLogoutActive,
                    LastReportReading = LastReportReading,
                    NextAvailableBauschleife = NextAvailableBauschleife,
                    NextAvailableTroopAtHomeAt = NextAvailableTroopAtHomeAt,
                    LastBauenZeitpunkt = LastBauenZeitpunkt,
                    LastFarmenZeitpunkt = LastFarmenZeitpunkt,
                    LastRekrutierenZeitpunkt = LastRekrutierenZeitpunkt,
                    LoginActive = LoginActive,
                    LastNachrichtenZeitpunkt = LastNachrichtenZeitpunkt,
                    LastHandelZeitpunkt = LastHandelZeitpunkt,
                    ReloggingActive = ReloggingActive,
                    GeplanteBewegungen = new AsyncObservableCollection<Truppenbewegung>(),
                    RelogOnSessionLostAfterMinutes = RelogOnSessionLostAfterMinutes,
                    GoldCoinsActive = GoldCoinsActive,
                    GoldCoinsinterval = GoldCoinsinterval,
                    LastGoldCoinsZeitpunkt = LastGoldCoinsZeitpunkt,
                    Playerid = Playerid,
                    Befehle = new AsyncObservableCollection<Befehl>(),
                    Unterstuetzungen = new AsyncObservableCollection<Unterstuetzung>(),
                    AngriffeAufAccount = new AsyncObservableCollection<AttackOnOwnVillage>(),
                    LastTruppeneinlesen = LastTruppeneinlesen,
                    LastUnterstuetzungeneinlesen = LastUnterstuetzungeneinlesen,
                    SetFlaggeActive = SetFlaggeActive,
                    SetBoosterActive = SetBoosterActive,
                    SetInventoryItemsActive = SetInventoryItemsActive,
                    LastAdjusted = LastAdjusted,
                    AutoPilot = AutoPilot,
                    ForschenActive = ForschenActive,
                    Forscheninterval = Forscheninterval,
                    LastForschenZeitpunkt = LastForschenZeitpunkt,
                    UseRegressionForErfolgsquotient = UseRegressionForErfolgsquotient,
                    MaxHoursToCountBerichteForErfolgsquotient = MaxHoursToCountBerichteForErfolgsquotient,
                    RaubzugActive = RaubzugActive,
                    Raubzuginterval = Raubzuginterval,
                    LastRaubzugZeitpunkt = LastRaubzugZeitpunkt
                };
                foreach (var dorfsetting in DoerferSettings) {
                    newsettings.DoerferSettings.Add(dorfsetting.Clone());
                }

                foreach (var login in LastLogins) {
                    newsettings.LastLogins.Add(login);
                }

                foreach (var bewegung in GeplanteBewegungen) {
                    newsettings.GeplanteBewegungen.Add(bewegung);
                }

                foreach (var angriff in AngriffeAufAccount) {
                    newsettings.AngriffeAufAccount.Add(angriff);
                }

                foreach (var befehl in Befehle) {
                    newsettings.Befehle.Add(befehl);
                }

                foreach (var befehl in Unterstuetzungen) {
                    newsettings.Unterstuetzungen.Add(befehl);
                }

                return newsettings;
            }

            [NotifyPropertyChangedInvocator]
            protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}