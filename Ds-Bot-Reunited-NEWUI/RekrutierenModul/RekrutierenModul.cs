﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ds_Bot_Reunited_NEWUI.Accountsettings.Ds_Bot_Reunited.Accountsettings;
using Ds_Bot_Reunited_NEWUI.Botcaptcha;
using Ds_Bot_Reunited_NEWUI.BrowserManager;
using Ds_Bot_Reunited_NEWUI.Constants;
using Ds_Bot_Reunited_NEWUI.Farmen;
using Ds_Bot_Reunited_NEWUI.ForschenModul;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.Rekrutieren;
using Ds_Bot_Reunited_NEWUI.Settings;
using OpenQA.Selenium;

// ReSharper disable EmptyGeneralCatchClause

namespace Ds_Bot_Reunited_NEWUI.RekrutierenModul {
    public class RekrutierenModul {
		private static TimeSpan GetBauzeit(AccountDorfSettings dorf, Truppe truppe, int count = 1) {
			double baseBuildTime = 0;
			switch (truppe) {
				case Truppe.Speertraeger:
					baseBuildTime = App.Settings.Weltensettings.SpeertraegerBauzeit;
					break;
				case Truppe.Schwertkaempfer:
					baseBuildTime = App.Settings.Weltensettings.SchwertkaempferBauzeit;
					break;
				case Truppe.Axtkaempfer:
					baseBuildTime = App.Settings.Weltensettings.AxtkaempferBauzeit;
					break;
				case Truppe.Bogenschuetzen:
					baseBuildTime = App.Settings.Weltensettings.BogenschuetzenBauzeit;
					break;
				case Truppe.Spaeher:
					baseBuildTime = App.Settings.Weltensettings.SpaeherBauzeit;
					break;
				case Truppe.LeichteKavallerie:
					baseBuildTime = App.Settings.Weltensettings.LeichteKavallerieBauzeit;
					break;
				case Truppe.BeritteneBogenschuetzen:
					baseBuildTime = App.Settings.Weltensettings.BeritteneBogenschuetzenBauzeit;
					break;
				case Truppe.SchwereKavallerie:
					baseBuildTime = App.Settings.Weltensettings.SchwereKavallerieBauzeit;
					break;
				case Truppe.Rammboecke:
					baseBuildTime = App.Settings.Weltensettings.RammboeckeBauzeit;
					break;
				case Truppe.Katapulte:
					baseBuildTime = App.Settings.Weltensettings.KatapulteBauzeit;
					break;
			}

			double buildFactor = 0;
			if (truppe == Truppe.Speertraeger || truppe == Truppe.Schwertkaempfer || truppe == Truppe.Axtkaempfer ||
			    truppe == Truppe.Bogenschuetzen)
				buildFactor = 0.665 * Math.Pow(0.94355, dorf.Dorfinformationen.GebaeudeStufen.Kaserne);
			if (truppe == Truppe.Spaeher || truppe == Truppe.LeichteKavallerie ||
			    truppe == Truppe.BeritteneBogenschuetzen || truppe == Truppe.SchwereKavallerie)
				buildFactor = 0.665 * Math.Pow(0.94355, dorf.Dorfinformationen.GebaeudeStufen.Stall);
			if (truppe == Truppe.Rammboecke || truppe == Truppe.Katapulte)
				buildFactor = 0.665 * Math.Pow(0.94355, dorf.Dorfinformationen.GebaeudeStufen.Werkstatt);

			double resultSec = (baseBuildTime * count) * (1.0 / 1) * buildFactor;
			return TimeSpan.FromSeconds(resultSec);
		}

		private static TimeSpan GetGemeinsameBauzeit(AccountDorfSettings dorf, TruppenTemplate troopstorecruit) {
			TimeSpan gemeinsamerTimespan = new TimeSpan();
			if (troopstorecruit.SpeertraegerAnzahl > 0) {
				gemeinsamerTimespan =
					gemeinsamerTimespan.Add(GetBauzeit(dorf, Truppe.Speertraeger, troopstorecruit.SpeertraegerAnzahl));
			}

			if (troopstorecruit.SchwertkaempferAnzahl > 0) {
				gemeinsamerTimespan =
					gemeinsamerTimespan.Add(GetBauzeit(dorf, Truppe.Schwertkaempfer
					                                   , troopstorecruit.SchwertkaempferAnzahl));
			}

			if (troopstorecruit.AxtkaempferAnzahl > 0) {
				gemeinsamerTimespan =
					gemeinsamerTimespan.Add(GetBauzeit(dorf, Truppe.Axtkaempfer, troopstorecruit.AxtkaempferAnzahl));
			}

			if (troopstorecruit.BogenschuetzenAnzahl > 0) {
				gemeinsamerTimespan =
					gemeinsamerTimespan.Add(GetBauzeit(dorf, Truppe.Bogenschuetzen
					                                   , troopstorecruit.BogenschuetzenAnzahl));
			}

			if (troopstorecruit.SpaeherAnzahl > 0) {
				gemeinsamerTimespan =
					gemeinsamerTimespan.Add(GetBauzeit(dorf, Truppe.Spaeher, troopstorecruit.SpaeherAnzahl));
			}

			if (troopstorecruit.LeichteKavallerieAnzahl > 0) {
				gemeinsamerTimespan =
					gemeinsamerTimespan.Add(GetBauzeit(dorf, Truppe.LeichteKavallerie
					                                   , troopstorecruit.LeichteKavallerieAnzahl));
			}

			if (troopstorecruit.BeritteneBogenschuetzenAnzahl > 0) {
				gemeinsamerTimespan =
					gemeinsamerTimespan.Add(GetBauzeit(dorf, Truppe.BeritteneBogenschuetzen
					                                   , troopstorecruit.BeritteneBogenschuetzenAnzahl));
			}

			if (troopstorecruit.SchwereKavallerieAnzahl > 0) {
				gemeinsamerTimespan =
					gemeinsamerTimespan.Add(GetBauzeit(dorf, Truppe.SchwereKavallerie
					                                   , troopstorecruit.SchwereKavallerieAnzahl));
			}

			if (troopstorecruit.RammboeckeAnzahl > 0) {
				gemeinsamerTimespan =
					gemeinsamerTimespan.Add(GetBauzeit(dorf, Truppe.Rammboecke, troopstorecruit.RammboeckeAnzahl));
			}

			if (troopstorecruit.KatapulteAnzahl > 0) {
				gemeinsamerTimespan =
					gemeinsamerTimespan.Add(GetBauzeit(dorf, Truppe.Katapulte, troopstorecruit.KatapulteAnzahl));
			}

			return gemeinsamerTimespan;
		}

		private static TimeSpan GetBarracksBauzeit(AccountDorfSettings dorf, TruppenTemplate troopstorecruit) {
			TimeSpan gemeinsamerTimespan = new TimeSpan();
			if (troopstorecruit.SpeertraegerAnzahl > 0) {
				gemeinsamerTimespan = gemeinsamerTimespan.Add(GetBauzeit(dorf, Truppe.Speertraeger));
			}

			if (troopstorecruit.SchwertkaempferAnzahl > 0) {
				gemeinsamerTimespan = gemeinsamerTimespan.Add(GetBauzeit(dorf, Truppe.Schwertkaempfer));
			}

			if (troopstorecruit.AxtkaempferAnzahl > 0) {
				gemeinsamerTimespan = gemeinsamerTimespan.Add(GetBauzeit(dorf, Truppe.Axtkaempfer));
			}

			if (troopstorecruit.BogenschuetzenAnzahl > 0) {
				gemeinsamerTimespan = gemeinsamerTimespan.Add(GetBauzeit(dorf, Truppe.Bogenschuetzen));
			}

			return gemeinsamerTimespan;
		}

		private static TimeSpan GetStableBauzeit(AccountDorfSettings dorf, TruppenTemplate troopstorecruit) {
			TimeSpan gemeinsamerTimespan = new TimeSpan();
			if (troopstorecruit.SpaeherAnzahl > 0) {
				gemeinsamerTimespan = gemeinsamerTimespan.Add(GetBauzeit(dorf, Truppe.Spaeher));
			}

			if (troopstorecruit.LeichteKavallerieAnzahl > 0) {
				gemeinsamerTimespan = gemeinsamerTimespan.Add(GetBauzeit(dorf, Truppe.LeichteKavallerie));
			}

			if (troopstorecruit.BeritteneBogenschuetzenAnzahl > 0) {
				gemeinsamerTimespan = gemeinsamerTimespan.Add(GetBauzeit(dorf, Truppe.BeritteneBogenschuetzen));
			}

			if (troopstorecruit.SchwereKavallerieAnzahl > 0) {
				gemeinsamerTimespan = gemeinsamerTimespan.Add(GetBauzeit(dorf, Truppe.SchwereKavallerie));
			}

			return gemeinsamerTimespan;
		}

		private static TimeSpan GetWorkshopBauzeit(AccountDorfSettings dorf, TruppenTemplate troopstorecruit) {
			TimeSpan gemeinsamerTimespan = new TimeSpan();
			if (troopstorecruit.RammboeckeAnzahl > 0) {
				gemeinsamerTimespan = gemeinsamerTimespan.Add(GetBauzeit(dorf, Truppe.Rammboecke));
			}

			if (troopstorecruit.KatapulteAnzahl > 0) {
				gemeinsamerTimespan = gemeinsamerTimespan.Add(GetBauzeit(dorf, Truppe.Katapulte));
			}

			return gemeinsamerTimespan;
		}

		public static async Task StartRekrutBalance(Browser browser, Accountsetting accountsettings) {
			App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
				               ? accountsettings.Uvname
				               : accountsettings.Accountname) + ": " + " Rekruting started");
			foreach (var dorf in
				accountsettings.DoerferSettings.Where(x => !string.IsNullOrEmpty(x.Dorf.Dorf.VillageName) &&
				                                           x.Dorf.PlayerId.Equals(accountsettings.Playerid))) {
				await App.WaitForAttacks();
				if (accountsettings.RekrutierenActive && dorf.RekrutierenActive) {
					App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
						               ? accountsettings.Uvname
						               : accountsettings.Accountname) + ": " + " Rekruting Village: " +
					              dorf.Dorf.VillageId + " Name: " + dorf.Dorf.Name + " started");
					if (dorf.Dorfinformationen.LastRekrutierenZeitpunkt <
					    DateTime.Now.AddMinutes(-dorf.Rekrutiereninterval)) {
						await DorfinformationenModul
						      .DorfinformationenModul.GetDorfinformationenHauptgebaeude(browser, accountsettings, dorf);
						if (dorf.Rekrutieren.SchwertkaempferAnzahl > 0 &&
						    dorf.Dorfinformationen.GebaeudeStufen.Schmiede >= 1 &&
						    !dorf.Dorfinformationen.SchwertkaempferErforscht &&
						    App.Settings.Weltensettings.TenTechLevel) {
							await ForschenModul.ForschenModul.Forge(browser, dorf, Forschen.Schwertkaempfer
							                                        , accountsettings);
						}

						if (dorf.Rekrutieren.AxtkaempferAnzahl > 0 &&
						    dorf.Dorfinformationen.GebaeudeStufen.Schmiede >= 2 &&
						    !dorf.Dorfinformationen.AxtkaempferErforscht) {
							await ForschenModul.ForschenModul.Forge(browser, dorf, Forschen.Axtkaempfer
							                                        , accountsettings);
						}

						if (dorf.Rekrutieren.BogenschuetzenAnzahl > 0 &&
						    !dorf.Dorfinformationen.BogenschuetzenErforscht) {
							await ForschenModul.ForschenModul.Forge(browser, dorf, Forschen.Bogenschuetzen
							                                        , accountsettings);
						}

						if (dorf.Rekrutieren.SpaeherAnzahl > 0 && dorf.Dorfinformationen.GebaeudeStufen.Stall >= 1 &&
						    !dorf.Dorfinformationen.SpaeherErforscht) {
							await ForschenModul.ForschenModul.Forge(browser, dorf, Forschen.Spaeher, accountsettings);
						}

						if (dorf.Rekrutieren.LeichteKavallerieAnzahl > 0 &&
						    dorf.Dorfinformationen.GebaeudeStufen.Stall >= 3 &&
						    !dorf.Dorfinformationen.LeichteKavallerieErforscht) {
							await ForschenModul.ForschenModul.Forge(browser, dorf, Forschen.LeichteKavallerie
							                                        , accountsettings);
						}

						if (dorf.Rekrutieren.BeritteneBogenschuetzenAnzahl > 0 &&
						    dorf.Dorfinformationen.GebaeudeStufen.Stall >= 3 &&
						    !dorf.Dorfinformationen.BeritteneBogenschuetzenErforscht) {
							await ForschenModul.ForschenModul.Forge(browser, dorf, Forschen.BeritteneBogenschuetzen
							                                        , accountsettings);
						}

						if (dorf.Rekrutieren.SchwereKavallerieAnzahl > 0 &&
						    dorf.Dorfinformationen.GebaeudeStufen.Stall >= 3 &&
						    !dorf.Dorfinformationen.SchwereKavallerieErforscht) {
							await ForschenModul.ForschenModul.Forge(browser, dorf, Forschen.SchwereKavallerie
							                                        , accountsettings);
						}

						if (dorf.Rekrutieren.RammboeckeAnzahl > 0 &&
						    dorf.Dorfinformationen.GebaeudeStufen.Werkstatt >= 1 &&
						    !dorf.Dorfinformationen.RammboeckeErforscht) {
							await ForschenModul.ForschenModul.Forge(browser, dorf, Forschen.Rammboecke
							                                        , accountsettings);
						}

						if (dorf.Rekrutieren.KatapulteAnzahl > 0 &&
						    dorf.Dorfinformationen.GebaeudeStufen.Werkstatt >= 5 &&
						    !dorf.Dorfinformationen.KatapulteErforscht) {
							await ForschenModul.ForschenModul.Forge(browser, dorf, Forschen.Katapulte, accountsettings);
						}


						double kaserne = 0;
						double stall = 0;
						double werkstatt = 0;
						if (dorf.Rekrutieren.SpeertraegerAnzahl - dorf.Dorfinformationen.Truppen.SpeertraegerAnzahl >
						    0) {
							kaserne++;
						}

						if (dorf.Rekrutieren.SchwertkaempferAnzahl -
						    dorf.Dorfinformationen.Truppen.SchwertkaempferAnzahl > 0) {
							kaserne++;
						}

						if (dorf.Rekrutieren.AxtkaempferAnzahl - dorf.Dorfinformationen.Truppen.AxtkaempferAnzahl > 0) {
							kaserne++;
						}

						if (dorf.Rekrutieren.BogenschuetzenAnzahl -
						    dorf.Dorfinformationen.Truppen.BogenschuetzenAnzahl > 0) {
							kaserne++;
						}

						if (dorf.Rekrutieren.SpaeherAnzahl - dorf.Dorfinformationen.Truppen.SpaeherAnzahl > 0) {
							stall = stall + 1;
						}

						if (dorf.Rekrutieren.LeichteKavallerieAnzahl -
						    dorf.Dorfinformationen.Truppen.LeichteKavallerieAnzahl > 0) {
							stall = stall + 1.5;
						}

						if (dorf.Rekrutieren.BeritteneBogenschuetzenAnzahl -
						    dorf.Dorfinformationen.Truppen.BeritteneBogenschuetzenAnzahl > 0) {
							stall = stall + 2;
						}

						if (dorf.Rekrutieren.SchwereKavallerieAnzahl -
						    dorf.Dorfinformationen.Truppen.SchwereKavallerieAnzahl > 0) {
							stall = stall + 2.5;
						}

						if (dorf.Rekrutieren.RammboeckeAnzahl - dorf.Dorfinformationen.Truppen.RammboeckeAnzahl > 0 ||
						    dorf.Rekrutieren.KatapulteAnzahl - dorf.Dorfinformationen.Truppen.KatapulteAnzahl > 0) {
							werkstatt = werkstatt + 3;
						}

						if (dorf.Rekrutieren.KatapulteAnzahl - dorf.Dorfinformationen.Truppen.KatapulteAnzahl > 0) {
							werkstatt = werkstatt + 4;
						}

						double gesamt = kaserne + stall + werkstatt;


						dorf.KasernePercent = kaserne > 0 ? kaserne / gesamt * 100 : 0;
						dorf.StallPercent = stall > 0 ? stall / gesamt * 100 : 0;
						dorf.WerkstattPercent = werkstatt > 0 ? werkstatt / gesamt * 100 : 0;


						if (dorf.Rekrutieren.SpeertraegerAnzahl > 0 || dorf.Rekrutieren.SchwertkaempferAnzahl > 0 ||
						    dorf.Rekrutieren.AxtkaempferAnzahl > 0 || dorf.Rekrutieren.BogenschuetzenAnzahl > 0) {
							await RekrutBarracks(dorf.Rekrutieren, browser, dorf, accountsettings
							                     , new TimeSpan(dorf.KaserneMaxHours, 0, 0));
						}

						if ((dorf.Rekrutieren.SpaeherAnzahl > 0 || dorf.Rekrutieren.LeichteKavallerieAnzahl > 0 ||
						     dorf.Rekrutieren.BeritteneBogenschuetzenAnzahl > 0 ||
						     dorf.Rekrutieren.SchwereKavallerieAnzahl > 0)) {
							await RekrutStable(dorf.Rekrutieren, browser, dorf, accountsettings
							                   , new TimeSpan(dorf.StallMaxHours, 0, 0));
						}

						if ((dorf.Rekrutieren.RammboeckeAnzahl > 0 || dorf.Rekrutieren.KatapulteAnzahl > 0)) {
							await RekrutWorkshop(dorf.Rekrutieren, browser, dorf, accountsettings
							                     , new TimeSpan(dorf.WerkstattMaxHours, 0, 0));
						}

						if (dorf.Rekrutieren.PaladinAnzahl > 0 && App.Settings.Weltensettings.PaladinActive) {
							await RekrutPaladin(browser, dorf
							                    , (!string.IsNullOrEmpty(accountsettings.Uvname)
								                       ? accountsettings.Uvname
								                       : accountsettings.Accountname), accountsettings);
						}

						dorf.Dorfinformationen.LastRekrutierenZeitpunkt = DateTime.Now;
					}

					App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
						               ? accountsettings.Uvname
						               : accountsettings.Accountname) + ": " + " Rekruting Village: " +
					              dorf.Dorf.VillageId + " Name: " + dorf.Dorf.Name + " ended");
				}
			}

			App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
				               ? accountsettings.Uvname
				               : accountsettings.Accountname) + ": " + " Recruting ended");
			accountsettings.LastRekrutierenZeitpunkt = DateTime.Now;
		}

		public static async Task<int> RekrutBarracks(TruppenTemplate troopstorecruit
		                                             , Browser browser
		                                             , AccountDorfSettings dorf
		                                             , Accountsetting accountsettings
		                                             , TimeSpan maxBauzeit) {
			if (accountsettings.RekrutierenActive) {
				string link = App.Settings.OperateLink + "village=" + dorf.Dorf.VillageId + "" +
				              accountsettings.UvZusatzFuerLink + "&screen=barracks";
				browser.WebDriver.Navigate().GoToUrl(link);
				browser.WebDriver.WaitForPageload();
				await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings, link);
				dorf.Dorfinformationen.Speicher.Holz =
					int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
					                                                 .Dorfuebersicht.SPEICHER_HOLZ_SELECTOR_XPATH))
					                 .Text);
				dorf.Dorfinformationen.Speicher.Lehm =
					int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
					                                                 .Dorfuebersicht.SPEICHER_LEHM_SELECTOR_XPATH))
					                 .Text);
				dorf.Dorfinformationen.Speicher.Eisen =
					int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
					                                                 .Dorfuebersicht.SPEICHER_EISEN_SELECTOR_XPATH))
					                 .Text);
				TruppenTemplate troopstorecruitt = troopstorecruit.Clone();
				TruppenTemplate inSchleife = new TruppenTemplate();
				try {
					List<IWebElement> barrackqueue = browser
					                                 .WebDriver.FindElement(By.Id("trainqueue_wrap_barracks"))
					                                 .FindElements(By.TagName("td")).ToList();
					foreach (var element in barrackqueue) {
						try {
							int anzahl = int.Parse(element.Text.Split(' ')[0]);
							if (element.Text.Contains(NAMECONSTANTS.GetNameForServer(Names.Speertraeger))) {
								troopstorecruitt.SpeertraegerAnzahl -= anzahl;
								inSchleife.SpeertraegerAnzahl += anzahl;
							} else if (element.Text.Contains(NAMECONSTANTS.GetNameForServer(Names.Schwertkaempfer))) {
								troopstorecruitt.SchwertkaempferAnzahl -= anzahl;
								inSchleife.SchwertkaempferAnzahl += anzahl;
							} else if (element.Text.Contains(NAMECONSTANTS.GetNameForServer(Names.Axtkaempfer))) {
								troopstorecruitt.AxtkaempferAnzahl -= anzahl;
								inSchleife.AxtkaempferAnzahl += anzahl;
							} else if (element.Text.Contains(NAMECONSTANTS.GetNameForServer(Names.Bogenschuetzen))) {
								troopstorecruitt.BogenschuetzenAnzahl -= anzahl;
								inSchleife.BogenschuetzenAnzahl += anzahl;
							}
						} catch { }
					}
				} catch { }

				try {
					List<string> texte = browser
					                     .WebDriver
					                     .FindElements(By.XPath("//form[@id='train_form']/descendant::tr[contains(@class,'row_a')]"))
					                     .Select(x => x.Text).ToList();
					foreach (var element in texte) {
						if (element.Contains(NAMECONSTANTS.GetNameForServer(Names.Speertraeger))) {
							dorf.Dorfinformationen.Truppen.SpeertraegerAnzahl =
								int.Parse(element.Split('/')[1].Split(' ')[0]);
							troopstorecruitt.SpeertraegerAnzahl -= dorf.Dorfinformationen.Truppen.SpeertraegerAnzahl;
						} else if (element.Contains(NAMECONSTANTS.GetNameForServer(Names.Schwertkaempfer))) {
							dorf.Dorfinformationen.Truppen.SchwertkaempferAnzahl =
								int.Parse(element.Split('/')[1].Split(' ')[0]);
							troopstorecruitt.SchwertkaempferAnzahl -=
								dorf.Dorfinformationen.Truppen.SchwertkaempferAnzahl;
						} else if (element.Contains(NAMECONSTANTS.GetNameForServer(Names.Axtkaempfer))) {
							dorf.Dorfinformationen.Truppen.AxtkaempferAnzahl =
								int.Parse(element.Split('/')[1].Split(' ')[0]);
							troopstorecruitt.AxtkaempferAnzahl -= dorf.Dorfinformationen.Truppen.AxtkaempferAnzahl;
						} else if (element.Contains(NAMECONSTANTS.GetNameForServer(Names.Bogenschuetzen))) {
							dorf.Dorfinformationen.Truppen.BogenschuetzenAnzahl =
								int.Parse(element.Split('/')[1].Split(' ')[0]);
							troopstorecruitt.BogenschuetzenAnzahl -=
								dorf.Dorfinformationen.Truppen.BogenschuetzenAnzahl;
						}
					}
				} catch { }

				TruppenTemplate recruit = new TruppenTemplate();
				TimeSpan actualTimespan = GetGemeinsameBauzeit(dorf, inSchleife);
				double totalmilliseconds = actualTimespan.TotalMilliseconds;
				double gewollt = maxBauzeit.TotalMilliseconds;
				int counter = 0;
				int fehlercounter = 0;
				int holz = (int) (dorf.Dorfinformationen.SpeicherTotalHolz * (dorf.KasernePercent / 100));
				int lehm = (int) (dorf.Dorfinformationen.SpeicherTotalLehm * (dorf.KasernePercent / 100));
				int eisen = (int) (dorf.Dorfinformationen.SpeicherTotalEisen * (dorf.KasernePercent / 100));
				holz -= dorf.RekrutierenHolzUebrigLassen;
				lehm -= dorf.RekrutierenLehmUebrigLassen;
				eisen -= dorf.RekrutierenEisenUebrigLassen;
                int farmActual =
					int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
					                                                 .Dorfuebersicht.AKTUELLBH_SELECTOR_XPATH)).Text);
				while (gewollt > totalmilliseconds && fehlercounter < 4 &&
				       (troopstorecruitt.SpeertraegerAnzahl - 1 > 0 || troopstorecruitt.SchwertkaempferAnzahl - 1 > 0 ||
				        troopstorecruitt.AxtkaempferAnzahl - 1 > 0 || troopstorecruitt.BogenschuetzenAnzahl - 1 > 0)) {
					switch (counter) {
						case 0:
							if (troopstorecruitt.SpeertraegerAnzahl - 1 > 0) {
								if (farmActual + 1 < dorf.Dorfinformationen.FarmMax &&
								    holz - TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_SPEERTRAEGER_HOLZ > 0 &&
								    lehm - TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_SPEERTRAEGER_LEHM > 0 &&
								    eisen - TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_SPEERTRAEGER_EISEN > 0 &&
								    gewollt >= totalmilliseconds +
								    GetBauzeit(dorf, Truppe.Speertraeger).TotalMilliseconds) {
									recruit.SpeertraegerAnzahl++;
									holz -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_SPEERTRAEGER_HOLZ;
									lehm -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_SPEERTRAEGER_LEHM;
									eisen -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_SPEERTRAEGER_EISEN;
									totalmilliseconds += GetBauzeit(dorf, Truppe.Speertraeger).TotalMilliseconds;
									troopstorecruitt.SpeertraegerAnzahl--;
									farmActual += 1;
								} else {
									fehlercounter++;
								}
							}

							break;
						case 1:
							if (troopstorecruitt.SchwertkaempferAnzahl - 1 > 0) {
								if (farmActual + 1 < dorf.Dorfinformationen.FarmMax &&
								    holz - TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_SCHWERTKAEMPFER_HOLZ > 0 &&
								    lehm - TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_SCHWERTKAEMPFER_LEHM > 0 &&
								    eisen - TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_SCHWERTKAEMPFER_EISEN > 0 &&
								    gewollt >= totalmilliseconds +
								    GetBauzeit(dorf, Truppe.Schwertkaempfer).TotalMilliseconds) {
									recruit.SchwertkaempferAnzahl++;
									holz -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_SCHWERTKAEMPFER_HOLZ;
									lehm -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_SCHWERTKAEMPFER_LEHM;
									eisen -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_SCHWERTKAEMPFER_EISEN;
									totalmilliseconds += GetBauzeit(dorf, Truppe.Schwertkaempfer).TotalMilliseconds;
									troopstorecruitt.SchwertkaempferAnzahl--;
									farmActual += 1;
								} else {
									fehlercounter++;
								}
							}

							break;
						case 2:
							if (troopstorecruitt.AxtkaempferAnzahl - 1 > 0) {
								if (farmActual + 1 < dorf.Dorfinformationen.FarmMax &&
								    holz - TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_AXTKAEMPFER_HOLZ > 0 &&
								    lehm - TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_AXTKAEMPFER_LEHM > 0 &&
								    eisen - TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_AXTKAEMPFER_EISEN > 0 &&
								    gewollt >= totalmilliseconds +
								    GetBauzeit(dorf, Truppe.Axtkaempfer).TotalMilliseconds) {
									recruit.AxtkaempferAnzahl++;
									holz -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_AXTKAEMPFER_HOLZ;
									lehm -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_AXTKAEMPFER_LEHM;
									eisen -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_AXTKAEMPFER_EISEN;
									totalmilliseconds += GetBauzeit(dorf, Truppe.Axtkaempfer).TotalMilliseconds;
									troopstorecruitt.AxtkaempferAnzahl--;
									farmActual += 1;
								} else {
									fehlercounter++;
								}
							}

							break;
						case 3:
							if (troopstorecruitt.BogenschuetzenAnzahl - 1 > 0) {
								if (farmActual + 1 < dorf.Dorfinformationen.FarmMax &&
								    holz - TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_BOGENSCHUETZEN_HOLZ > 0 &&
								    lehm - TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_BOGENSCHUETZEN_LEHM > 0 &&
								    eisen - TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_BOGENSCHUETZEN_EISEN > 0 &&
								    gewollt >= totalmilliseconds +
								    GetBauzeit(dorf, Truppe.Bogenschuetzen).TotalMilliseconds) {
									recruit.BogenschuetzenAnzahl++;
									holz -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_BOGENSCHUETZEN_HOLZ;
									lehm -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_BOGENSCHUETZEN_LEHM;
									eisen -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_BOGENSCHUETZEN_EISEN;
									totalmilliseconds += GetBauzeit(dorf, Truppe.Bogenschuetzen).TotalMilliseconds;
									troopstorecruitt.BogenschuetzenAnzahl--;
									farmActual += 1;
								} else {
									fehlercounter++;
								}
							}

							break;
					}

					counter++;
					if (counter > 3) counter = 0;
				}

				bool doneRecruit = false;
				if (recruit.SpeertraegerAnzahl > 0 &&
				    browser.WebDriver.ElementExists(By.XPath("//input[@id='spear_0']"))) {
					IWebElement element = browser.WebDriver.FindElement(By.XPath("//input[@id='spear_0']"));
					element.Clear();
					element.SendKeys(recruit.SpeertraegerAnzahl.ToString());
					doneRecruit = true;
					await Rndm.Sleep();
				}

				if (recruit.SchwertkaempferAnzahl > 0 &&
				    browser.WebDriver.ElementExists(By.XPath("//input[@id='sword_0']"))) {
					IWebElement element = browser.WebDriver.FindElement(By.XPath("//input[@id='sword_0']"));
					element.Clear();
					element.SendKeys(recruit.SchwertkaempferAnzahl.ToString());
					doneRecruit = true;
					await Rndm.Sleep();
				}

				if (recruit.AxtkaempferAnzahl > 0 &&
				    browser.WebDriver.ElementExists(By.XPath("//input[@id='axe_0']"))) {
					IWebElement element = browser.WebDriver.FindElement(By.XPath("//input[@id='axe_0']"));
					element.Clear();
					element.SendKeys(recruit.AxtkaempferAnzahl.ToString());
					doneRecruit = true;
					dorf.Dorfinformationen.AxtkaempferErforscht = true;
					await Rndm.Sleep();
				}

				if (recruit.BogenschuetzenAnzahl > 0 &&
				    browser.WebDriver.ElementExists(By.XPath("//input[@id='archer_0']"))) {
					IWebElement element = browser.WebDriver.FindElement(By.XPath("//input[@id='archer_0']"));
					element.Clear();
					element.SendKeys(recruit.BogenschuetzenAnzahl.ToString());
					doneRecruit = true;
					dorf.Dorfinformationen.BogenschuetzenErforscht = true;
					await Rndm.Sleep();
				}

				if (doneRecruit) {
					IWebElement element =
						browser.WebDriver.FindElement(By.XPath("//input[contains(@class,'btn btn-recruit')]"));
					browser.ClickElement(element);
					browser.WebDriver.WaitForPageload();
					await Rndm.Sleep();
				}

				if (recruit.SpeertraegerAnzahl > 0) {
					App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
						               ? accountsettings.Uvname
						               : accountsettings.Accountname) + ": " + " Recruting Village: " +
					              dorf.Dorf.VillageId + " " + recruit.SpeertraegerAnzahl + " spears recruted");
				}

				if (recruit.SchwertkaempferAnzahl > 0) {
					App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
						               ? accountsettings.Uvname
						               : accountsettings.Accountname) + ": " + " Recruting Village: " +
					              dorf.Dorf.VillageId + " " + recruit.SchwertkaempferAnzahl + " swords recruted");
				}

				if (recruit.AxtkaempferAnzahl > 0) {
					App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
						               ? accountsettings.Uvname
						               : accountsettings.Accountname) + ": " + " Recruting Village: " +
					              dorf.Dorf.VillageId + " " + recruit.AxtkaempferAnzahl + " axe recruted");
				}

				if (recruit.BogenschuetzenAnzahl > 0) {
					App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
						               ? accountsettings.Uvname
						               : accountsettings.Accountname) + ": " + " Recruting Village: " +
					              dorf.Dorf.VillageId + " " + recruit.BogenschuetzenAnzahl + " archer recruted");
				}

				return recruit.SpeertraegerAnzahl + recruit.SchwertkaempferAnzahl + recruit.AxtkaempferAnzahl +
				       recruit.BogenschuetzenAnzahl;
			}

			return 0;
		}

		public static async Task<int> RekrutStable(TruppenTemplate troopstorecruit
		                                           , Browser browser
		                                           , AccountDorfSettings dorf
		                                           , Accountsetting accountsettings
		                                           , TimeSpan maxBauzeit) {
			if (accountsettings.RekrutierenActive) {
				string link = App.Settings.OperateLink + "village=" + dorf.Dorf.VillageId + "" +
				              accountsettings.UvZusatzFuerLink + "&screen=stable";
				browser.WebDriver.Navigate().GoToUrl(link);
				browser.WebDriver.WaitForPageload();
				await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings, link);
				dorf.Dorfinformationen.Speicher.Holz =
					int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
					                                                 .Dorfuebersicht.SPEICHER_HOLZ_SELECTOR_XPATH))
					                 .Text);
				dorf.Dorfinformationen.Speicher.Lehm =
					int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
					                                                 .Dorfuebersicht.SPEICHER_LEHM_SELECTOR_XPATH))
					                 .Text);
				dorf.Dorfinformationen.Speicher.Eisen =
					int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
					                                                 .Dorfuebersicht.SPEICHER_EISEN_SELECTOR_XPATH))
					                 .Text);
				TruppenTemplate troopstorecruitt = troopstorecruit.Clone();
				TruppenTemplate inSchleife = new TruppenTemplate();
				try {
					List<IWebElement> barrackqueue = browser
					                                 .WebDriver.FindElement(By.Id("trainqueue_wrap_stable"))
					                                 .FindElements(By.TagName("td")).ToList();
					foreach (var element in barrackqueue) {
						try {
							int anzahl = int.Parse(element.Text.Split(' ')[0]);
							if (element.Text.Contains(NAMECONSTANTS.GetNameForServer(Names.Spaeher))) {
								troopstorecruitt.SpaeherAnzahl -= anzahl;
								inSchleife.SpaeherAnzahl += anzahl;
							} else if (element.Text.Contains(NAMECONSTANTS.GetNameForServer(Names.Leichte))) {
								troopstorecruitt.LeichteKavallerieAnzahl -= anzahl;
								inSchleife.LeichteKavallerieAnzahl += anzahl;
							} else if (element.Text.Contains(NAMECONSTANTS.GetNameForServer(Names.Berittene))) {
								troopstorecruitt.BeritteneBogenschuetzenAnzahl -= anzahl;
								inSchleife.BeritteneBogenschuetzenAnzahl += anzahl;
							} else if (element.Text.Contains(NAMECONSTANTS.GetNameForServer(Names.Schwere))) {
								troopstorecruitt.SchwereKavallerieAnzahl -= anzahl;
								inSchleife.SchwereKavallerieAnzahl += anzahl;
							}
						} catch { }
					}
				} catch { }

				try {
					List<string> texte = browser
					                     .WebDriver
					                     .FindElements(By.XPath("//form[@id='train_form']/descendant::tr[contains(@class,'row_a')]"))
					                     .Select(x => x.Text).ToList();
					foreach (var element in texte) {
						if (element.Contains(NAMECONSTANTS.GetNameForServer(Names.Spaeher))) {
							dorf.Dorfinformationen.Truppen.SpaeherAnzahl =
								int.Parse(element.Split('/')[1].Split(' ')[0]);
							troopstorecruitt.SpaeherAnzahl -= dorf.Dorfinformationen.Truppen.SpaeherAnzahl;
						} else if (element.Contains(NAMECONSTANTS.GetNameForServer(Names.Leichte))) {
							dorf.Dorfinformationen.Truppen.LeichteKavallerieAnzahl =
								int.Parse(element.Split('/')[1].Split(' ')[0]);
							troopstorecruitt.LeichteKavallerieAnzahl -=
								dorf.Dorfinformationen.Truppen.LeichteKavallerieAnzahl;
						} else if (element.Contains(NAMECONSTANTS.GetNameForServer(Names.Berittene))) {
							dorf.Dorfinformationen.Truppen.BeritteneBogenschuetzenAnzahl =
								int.Parse(element.Split('/')[1].Split(' ')[0]);
							troopstorecruitt.BeritteneBogenschuetzenAnzahl -=
								dorf.Dorfinformationen.Truppen.BeritteneBogenschuetzenAnzahl;
						} else if (element.Contains(NAMECONSTANTS.GetNameForServer(Names.Schwere))) {
							dorf.Dorfinformationen.Truppen.SchwereKavallerieAnzahl =
								int.Parse(element.Split('/')[1].Split(' ')[0]);
							troopstorecruitt.SchwereKavallerieAnzahl -=
								dorf.Dorfinformationen.Truppen.SchwereKavallerieAnzahl;
						}
					}
				} catch { }

				TruppenTemplate recruit = new TruppenTemplate();
				TimeSpan actualTimespan = GetGemeinsameBauzeit(dorf, inSchleife);
				double totalmilliseconds = actualTimespan.TotalMilliseconds;
				double gewollt = maxBauzeit.TotalMilliseconds;
				int counter = 0;
				int fehlercounter = 0;
				int holz = (int) (dorf.Dorfinformationen.SpeicherTotalHolz * (dorf.StallPercent / 100));
				int lehm = (int) (dorf.Dorfinformationen.SpeicherTotalLehm * (dorf.StallPercent / 100));
				int eisen = (int) (dorf.Dorfinformationen.SpeicherTotalEisen * (dorf.StallPercent / 100));
				int farmActual =
					int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
					                                                 .Dorfuebersicht.AKTUELLBH_SELECTOR_XPATH)).Text);
				while (gewollt > totalmilliseconds && fehlercounter < 4 && (troopstorecruitt.SpaeherAnzahl - 1 > 0 ||
				                                                            troopstorecruitt.LeichteKavallerieAnzahl -
				                                                            1 > 0 || troopstorecruitt
					                                                            .BeritteneBogenschuetzenAnzahl - 1 >
				                                                            0 || troopstorecruitt
					                                                            .SchwereKavallerieAnzahl - 1 > 0)) {
					switch (counter) {
						case 0:
							if (troopstorecruitt.SpaeherAnzahl - 1 > 0) {
								if (farmActual + TRUPPENCONSTANTS.BAUERNHOFPLAETZE_SPAEHER <
								    dorf.Dorfinformationen.FarmMax &&
								    holz - TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_SPAEHER_HOLZ > 0 &&
								    lehm - TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_SPAEHER_LEHM > 0 &&
								    eisen - TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_SPAEHER_EISEN > 0 && gewollt >=
								    totalmilliseconds + GetBauzeit(dorf, Truppe.Spaeher).TotalMilliseconds) {
									recruit.SpaeherAnzahl++;
									holz -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_SPAEHER_HOLZ;
									lehm -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_SPAEHER_LEHM;
									eisen -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_SPAEHER_EISEN;
									totalmilliseconds += GetBauzeit(dorf, Truppe.Spaeher).TotalMilliseconds;
									troopstorecruitt.SpaeherAnzahl--;
									farmActual += TRUPPENCONSTANTS.BAUERNHOFPLAETZE_SPAEHER;
								} else {
									fehlercounter++;
								}
							}

							break;
						case 1:
							if (troopstorecruitt.LeichteKavallerieAnzahl - 1 > 0) {
								if (farmActual + TRUPPENCONSTANTS.BAUERNHOFPLAETZE_LEICHTEKAVALLERIE <
								    dorf.Dorfinformationen.FarmMax &&
								    holz - TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_LEICHTEKAVALLERIE_HOLZ > 0 &&
								    lehm - TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_LEICHTEKAVALLERIE_LEHM > 0 &&
								    eisen - TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_LEICHTEKAVALLERIE_EISEN > 0 &&
								    gewollt >= totalmilliseconds +
								    GetBauzeit(dorf, Truppe.LeichteKavallerie).TotalMilliseconds) {
									recruit.LeichteKavallerieAnzahl++;
									holz -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_LEICHTEKAVALLERIE_HOLZ;
									lehm -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_LEICHTEKAVALLERIE_LEHM;
									eisen -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_LEICHTEKAVALLERIE_EISEN;
									totalmilliseconds += GetBauzeit(dorf, Truppe.LeichteKavallerie).TotalMilliseconds;
									troopstorecruitt.LeichteKavallerieAnzahl--;
									farmActual += TRUPPENCONSTANTS.BAUERNHOFPLAETZE_LEICHTEKAVALLERIE;
								} else {
									fehlercounter++;
								}
							}

							break;
						case 2:
							if (troopstorecruitt.BeritteneBogenschuetzenAnzahl - 1 > 0) {
								if (farmActual + TRUPPENCONSTANTS.BAUERNHOFPLAETZE_BERITTENEBOGENSCHUETZEN <
								    dorf.Dorfinformationen.FarmMax &&
								    holz - TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_BERITTENEBOGENSCHUETZEN_HOLZ > 0 &&
								    lehm - TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_BERITTENEBOGENSCHUETZEN_LEHM > 0 &&
								    eisen - TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_BERITTENEBOGENSCHUETZEN_EISEN > 0 &&
								    gewollt >= totalmilliseconds + GetBauzeit(dorf, Truppe.BeritteneBogenschuetzen)
									    .TotalMilliseconds) {
									recruit.BeritteneBogenschuetzenAnzahl++;
									holz -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_BERITTENEBOGENSCHUETZEN_HOLZ;
									lehm -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_BERITTENEBOGENSCHUETZEN_LEHM;
									eisen -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_BERITTENEBOGENSCHUETZEN_EISEN;
									totalmilliseconds += GetBauzeit(dorf, Truppe.BeritteneBogenschuetzen)
										.TotalMilliseconds;
									troopstorecruitt.BeritteneBogenschuetzenAnzahl--;
									farmActual += TRUPPENCONSTANTS.BAUERNHOFPLAETZE_BERITTENEBOGENSCHUETZEN;
								} else {
									fehlercounter++;
								}
							}

							break;
						case 3:
							if (troopstorecruitt.SchwereKavallerieAnzahl - 1 > 0) {
								if (farmActual + TRUPPENCONSTANTS.BAUERNHOFPLAETZE_SCHWEREKAVALLERIE <
								    dorf.Dorfinformationen.FarmMax &&
								    holz - TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_SCHWEREKAVALLERIE_HOLZ > 0 &&
								    lehm - TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_SCHWEREKAVALLERIE_LEHM > 0 &&
								    eisen - TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_SCHWEREKAVALLERIE_EISEN > 0 &&
								    gewollt >= totalmilliseconds +
								    GetBauzeit(dorf, Truppe.SchwereKavallerie).TotalMilliseconds) {
									recruit.SchwereKavallerieAnzahl++;
									holz -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_LEICHTEKAVALLERIE_HOLZ;
									lehm -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_LEICHTEKAVALLERIE_LEHM;
									eisen -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_LEICHTEKAVALLERIE_EISEN;
									totalmilliseconds += GetBauzeit(dorf, Truppe.SchwereKavallerie).TotalMilliseconds;
									troopstorecruitt.SchwereKavallerieAnzahl--;
									farmActual += TRUPPENCONSTANTS.BAUERNHOFPLAETZE_SCHWEREKAVALLERIE;
								} else {
									fehlercounter++;
								}
							}

							break;
					}

					counter++;
					if (counter > 3) counter = 0;
				}

				bool doneRecruit = false;
				if (recruit.SpaeherAnzahl > 0 && browser.WebDriver.ElementExists(By.XPath("//input[@id='spy_0']"))) {
					IWebElement element = browser.WebDriver.FindElement(By.XPath("//input[@id='spy_0']"));
					element.Clear();
					element.SendKeys(recruit.SpaeherAnzahl.ToString());
					dorf.Dorfinformationen.SpaeherErforscht = true;
					doneRecruit = true;
					await Rndm.Sleep();
				}

				if (recruit.LeichteKavallerieAnzahl > 0 &&
				    browser.WebDriver.ElementExists(By.XPath("//input[@id='light_0']"))) {
					IWebElement element = browser.WebDriver.FindElement(By.XPath("//input[@id='light_0']"));
					element.Clear();
					element.SendKeys(recruit.LeichteKavallerieAnzahl.ToString());
					dorf.Dorfinformationen.LeichteKavallerieErforscht = true;
					doneRecruit = true;
					await Rndm.Sleep();
				}

				if (recruit.BeritteneBogenschuetzenAnzahl > 0 &&
				    browser.WebDriver.ElementExists(By.XPath("//input[@id='marcher_0']"))) {
					IWebElement element = browser.WebDriver.FindElement(By.XPath("//input[@id='marcher_0']"));
					element.Clear();
					element.SendKeys(recruit.BeritteneBogenschuetzenAnzahl.ToString());
					dorf.Dorfinformationen.BeritteneBogenschuetzenErforscht = true;
					doneRecruit = true;
					await Rndm.Sleep();
				}

				if (recruit.SchwereKavallerieAnzahl > 0 &&
				    browser.WebDriver.ElementExists(By.XPath("//input[@id='heavy_0']"))) {
					IWebElement element = browser.WebDriver.FindElement(By.XPath("//input[@id='heavy_0']"));
					element.Clear();
					element.SendKeys(recruit.SchwereKavallerieAnzahl.ToString());
					dorf.Dorfinformationen.SchwereKavallerieErforscht = true;
					doneRecruit = true;
					await Rndm.Sleep();
				}

				if (doneRecruit) {
					IWebElement element =
						browser.WebDriver.FindElement(By.XPath("//input[contains(@class,'btn btn-recruit')]"));
					browser.ClickElement(element);
					browser.WebDriver.WaitForPageload();
					await Rndm.Sleep();
				}

				if (recruit.SpaeherAnzahl > 0) {
					App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
						               ? accountsettings.Uvname
						               : accountsettings.Accountname) + ": " + " Recruting Village: " +
					              dorf.Dorf.VillageId + " " + recruit.SpaeherAnzahl + " spys recruted");
				}

				if (recruit.LeichteKavallerieAnzahl > 0) {
					App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
						               ? accountsettings.Uvname
						               : accountsettings.Accountname) + ": " + " Recruting Village: " +
					              dorf.Dorf.VillageId + " " + recruit.LeichteKavallerieAnzahl +
					              " light cavallry recruted");
				}

				if (recruit.BeritteneBogenschuetzenAnzahl > 0) {
					App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
						               ? accountsettings.Uvname
						               : accountsettings.Accountname) + ": " + " Recruting Village: " +
					              dorf.Dorf.VillageId + " " + recruit.BeritteneBogenschuetzenAnzahl +
					              " mounted archers recruted");
				}

				if (recruit.SchwereKavallerieAnzahl > 0) {
					App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
						               ? accountsettings.Uvname
						               : accountsettings.Accountname) + ": " + " Recruting Village: " +
					              dorf.Dorf.VillageId + " " + recruit.SchwereKavallerieAnzahl +
					              " heavy cavallry recruted");
				}

				return recruit.SpeertraegerAnzahl + recruit.SchwertkaempferAnzahl + recruit.AxtkaempferAnzahl +
				       recruit.BogenschuetzenAnzahl;
			}

			return 0;
		}

		public static async Task<int> RekrutWorkshop(TruppenTemplate troopstorecruit
		                                             , Browser browser
		                                             , AccountDorfSettings dorf
		                                             , Accountsetting accountsettings
		                                             , TimeSpan maxBauzeit) {
			if (accountsettings.RekrutierenActive) {
				string link = App.Settings.OperateLink + "village=" + dorf.Dorf.VillageId + "" +
				              accountsettings.UvZusatzFuerLink + "&screen=garage";
				browser.WebDriver.Navigate().GoToUrl(link);
				browser.WebDriver.WaitForPageload();
				await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings, link);
				TruppenTemplate troopstorecruitt = troopstorecruit.Clone();
				TruppenTemplate inSchleife = new TruppenTemplate();
				try {
					List<IWebElement> barrackqueue = browser
					                                 .WebDriver.FindElement(By.Id("trainqueue_wrap_garage"))
					                                 .FindElements(By.TagName("td")).ToList();
					foreach (var element in barrackqueue) {
						try {
							int anzahl = int.Parse(element.Text.Split(' ')[0]);
							if (element.Text.Contains(NAMECONSTANTS.GetNameForServer(Names.Ramm))) {
								troopstorecruitt.RammboeckeAnzahl -= anzahl;
								inSchleife.RammboeckeAnzahl += anzahl;
							} else if (element.Text.Contains(NAMECONSTANTS.GetNameForServer(Names.Kata))) {
								troopstorecruitt.KatapulteAnzahl -= anzahl;
								inSchleife.KatapulteAnzahl += anzahl;
							}
						} catch { }
					}
				} catch { }

				try {
					List<string> texte = browser
					                     .WebDriver
					                     .FindElements(By.XPath("//form[@id='train_form']/descendant::tr[contains(@class,'row_a')]"))
					                     .Select(x => x.Text).ToList();
					foreach (var element in texte) {
						int anzahl = int.Parse(element.Split('/')[1].Split(' ')[0]);
						if (element.Contains(NAMECONSTANTS.GetNameForServer(Names.Ramm))) {
							dorf.Dorfinformationen.Truppen.RammboeckeAnzahl = anzahl;
							troopstorecruitt.RammboeckeAnzahl -= dorf.Dorfinformationen.Truppen.RammboeckeAnzahl;
						} else if (element.Contains(NAMECONSTANTS.GetNameForServer(Names.Kata))) {
							dorf.Dorfinformationen.Truppen.KatapulteAnzahl = anzahl;
							troopstorecruitt.KatapulteAnzahl -= dorf.Dorfinformationen.Truppen.KatapulteAnzahl;
						}
					}
				} catch { }

				TruppenTemplate recruit = new TruppenTemplate();
				TimeSpan actualTimespan = GetGemeinsameBauzeit(dorf, inSchleife);
				double totalmilliseconds = actualTimespan.TotalMilliseconds;
				double gewollt = maxBauzeit.TotalMilliseconds;
				int counter = 0;
				int fehlercounter = 0;
				int holz = (int) (dorf.Dorfinformationen.SpeicherTotalHolz * (dorf.WerkstattPercent / 100));
				int lehm = (int) (dorf.Dorfinformationen.SpeicherTotalLehm * (dorf.WerkstattPercent / 100));
				int eisen = (int) (dorf.Dorfinformationen.SpeicherTotalEisen * (dorf.WerkstattPercent / 100));
				int farmActual =
					int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
					                                                 .Dorfuebersicht.AKTUELLBH_SELECTOR_XPATH)).Text);
				while (gewollt > totalmilliseconds && fehlercounter < 4 &&
				       (troopstorecruitt.RammboeckeAnzahl - 1 > 0 || troopstorecruitt.KatapulteAnzahl - 1 > 0)) {
					switch (counter) {
						case 0:
							if (troopstorecruitt.RammboeckeAnzahl - 1 > 0) {
								if (farmActual + TRUPPENCONSTANTS.BAUERNHOFPLAETZE_RAMMBOECKE <
								    dorf.Dorfinformationen.FarmMax &&
								    holz - TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_RAMMBOECKE_HOLZ > 0 &&
								    lehm - TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_RAMMBOECKE_LEHM > 0 &&
								    eisen - TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_RAMMBOECKE_EISEN > 0 &&
								    gewollt >= totalmilliseconds +
								    GetBauzeit(dorf, Truppe.Rammboecke).TotalMilliseconds) {
									recruit.RammboeckeAnzahl++;
									holz -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_RAMMBOECKE_HOLZ;
									lehm -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_RAMMBOECKE_LEHM;
									eisen -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_RAMMBOECKE_EISEN;
									totalmilliseconds += GetBauzeit(dorf, Truppe.Rammboecke).TotalMilliseconds;
									troopstorecruitt.RammboeckeAnzahl--;
									farmActual += TRUPPENCONSTANTS.BAUERNHOFPLAETZE_RAMMBOECKE;
								} else {
									fehlercounter++;
								}
							}

							break;
						case 1:
							if (troopstorecruitt.KatapulteAnzahl - 1 > 0) {
								if (farmActual + TRUPPENCONSTANTS.BAUERNHOFPLAETZE_KATAPULTE <
								    dorf.Dorfinformationen.FarmMax &&
								    holz - TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_KATAPULTE_HOLZ > 0 &&
								    lehm - TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_KATAPULTE_LEHM > 0 &&
								    eisen - TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_KATAPULTE_EISEN > 0 && gewollt >=
								    totalmilliseconds + GetBauzeit(dorf, Truppe.Katapulte).TotalMilliseconds) {
									recruit.KatapulteAnzahl++;
									holz -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_KATAPULTE_HOLZ;
									lehm -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_KATAPULTE_LEHM;
									eisen -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_KATAPULTE_EISEN;
									totalmilliseconds += GetBauzeit(dorf, Truppe.Katapulte).TotalMilliseconds;
									troopstorecruitt.KatapulteAnzahl--;
									farmActual += TRUPPENCONSTANTS.BAUERNHOFPLAETZE_KATAPULTE;
								} else {
									fehlercounter++;
								}
							}

							break;
					}

					counter++;
					if (counter > 3) counter = 0;
				}

				bool doneRecruit = false;
				if (recruit.RammboeckeAnzahl > 0 && browser.WebDriver.ElementExists(By.XPath("//input[@id='ram_0']"))) {
					IWebElement element = browser.WebDriver.FindElement(By.XPath("//input[@id='ram_0']"));
					dorf.Dorfinformationen.RammboeckeErforscht = true;
					element.Clear();
					element.SendKeys(recruit.RammboeckeAnzahl.ToString());
					doneRecruit = true;
					await Rndm.Sleep();
				}

				if (recruit.KatapulteAnzahl > 0 && browser.WebDriver.ElementExists(By.XPath("//input[@id='cat_0']"))) {
					IWebElement element = browser.WebDriver.FindElement(By.XPath("//input[@id='cat_0']"));
					dorf.Dorfinformationen.KatapulteErforscht = true;
					element.Clear();
					element.SendKeys(recruit.KatapulteAnzahl.ToString());
					doneRecruit = true;
					await Rndm.Sleep();
				}

				if (doneRecruit) {
					IWebElement element =
						browser.WebDriver.FindElement(By.XPath("//input[contains(@class,'btn btn-recruit')]"));
					browser.ClickElement(element);
					browser.WebDriver.WaitForPageload();
					await Rndm.Sleep();
				}

				if (recruit.RammboeckeAnzahl > 0) {
					App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
						               ? accountsettings.Uvname
						               : accountsettings.Accountname) + ": " + " Recruting Village: " +
					              dorf.Dorf.VillageId + " " + recruit.RammboeckeAnzahl + " rams recruted");
				}

				if (recruit.KatapulteAnzahl > 0) {
					App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
						               ? accountsettings.Uvname
						               : accountsettings.Accountname) + ": " + " Recruting Village: " +
					              dorf.Dorf.VillageId + " " + recruit.KatapulteAnzahl + " catapults recruted");
				}

				return recruit.SpeertraegerAnzahl + recruit.SchwertkaempferAnzahl;
			}

			return 0;
		}

		//http://de98.twplus.org/calculator/recruit/?garage=0&ram=0&catapult=0&stable=3&heavy=0&spy=0&marcher=0&light=0&barracks=1&spear=0&archer=0&sword=0&axe=0&submit=berechnen
		public static async Task StartRekrut(Browser browser, Accountsetting accountsettings) {
			App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
				               ? accountsettings.Uvname
				               : accountsettings.Accountname) + ": " + " Rekruting started");
			foreach (var dorf in
				accountsettings.DoerferSettings.Where(x => !string.IsNullOrEmpty(x.Dorf.Dorf.VillageName) &&
				                                           x.Dorf.PlayerId.Equals(accountsettings.Playerid))) {
				await App.WaitForAttacks();
				if (accountsettings.RekrutierenActive && dorf.RekrutierenActive) {
					App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
						               ? accountsettings.Uvname
						               : accountsettings.Accountname) + ": " + " Rekruting Village: " +
					              dorf.Dorf.VillageId + " Name: " + dorf.Dorf.Name + " started");
					if (dorf.Dorfinformationen.LastRekrutierenZeitpunkt <
					    DateTime.Now.AddMinutes(-dorf.Rekrutiereninterval)) {
						await DorfinformationenModul
						      .DorfinformationenModul
						      .GetDorfinformationenDorfUebersicht(browser, accountsettings, dorf);
						if (dorf.Rekrutieren.SchwertkaempferAnzahl > 0 &&
						    dorf.Dorfinformationen.GebaeudeStufen.Schmiede >= 1 &&
						    !dorf.Dorfinformationen.SchwertkaempferErforscht &&
						    App.Settings.Weltensettings.TenTechLevel) {
							await ForschenModul.ForschenModul.Forge(browser, dorf, Forschen.Schwertkaempfer
							                                        , accountsettings);
						}

						if (dorf.Rekrutieren.AxtkaempferAnzahl > 0 &&
						    dorf.Dorfinformationen.GebaeudeStufen.Schmiede >= 2 &&
						    !dorf.Dorfinformationen.AxtkaempferErforscht) {
							await ForschenModul.ForschenModul.Forge(browser, dorf, Forschen.Axtkaempfer
							                                        , accountsettings);
						}

						if (dorf.Rekrutieren.BogenschuetzenAnzahl > 0 &&
						    !dorf.Dorfinformationen.BogenschuetzenErforscht) {
							await ForschenModul.ForschenModul.Forge(browser, dorf, Forschen.Bogenschuetzen
							                                        , accountsettings);
						}

						if (dorf.Rekrutieren.SpaeherAnzahl > 0 && dorf.Dorfinformationen.GebaeudeStufen.Stall >= 1 &&
						    !dorf.Dorfinformationen.SpaeherErforscht) {
							await ForschenModul.ForschenModul.Forge(browser, dorf, Forschen.Spaeher, accountsettings);
						}

						if (dorf.Rekrutieren.LeichteKavallerieAnzahl > 0 &&
						    dorf.Dorfinformationen.GebaeudeStufen.Stall >= 3 &&
						    !dorf.Dorfinformationen.LeichteKavallerieErforscht) {
							await ForschenModul.ForschenModul.Forge(browser, dorf, Forschen.LeichteKavallerie
							                                        , accountsettings);
						}

						if (dorf.Rekrutieren.BeritteneBogenschuetzenAnzahl > 0 &&
						    dorf.Dorfinformationen.GebaeudeStufen.Stall >= 3 &&
						    !dorf.Dorfinformationen.BeritteneBogenschuetzenErforscht) {
							await ForschenModul.ForschenModul.Forge(browser, dorf, Forschen.BeritteneBogenschuetzen
							                                        , accountsettings);
						}

						if (dorf.Rekrutieren.SchwereKavallerieAnzahl > 0 &&
						    dorf.Dorfinformationen.GebaeudeStufen.Stall >= 3 &&
						    !dorf.Dorfinformationen.SchwereKavallerieErforscht) {
							await ForschenModul.ForschenModul.Forge(browser, dorf, Forschen.SchwereKavallerie
							                                        , accountsettings);
						}

						if (dorf.Rekrutieren.RammboeckeAnzahl > 0 &&
						    dorf.Dorfinformationen.GebaeudeStufen.Werkstatt >= 1 &&
						    !dorf.Dorfinformationen.RammboeckeErforscht) {
							await ForschenModul.ForschenModul.Forge(browser, dorf, Forschen.Rammboecke
							                                        , accountsettings);
						}

						if (dorf.Rekrutieren.KatapulteAnzahl > 0 &&
						    dorf.Dorfinformationen.GebaeudeStufen.Werkstatt >= 5 &&
						    !dorf.Dorfinformationen.KatapulteErforscht) {
							await ForschenModul.ForschenModul.Forge(browser, dorf, Forschen.Katapulte, accountsettings);
						}

						int holz = 0;
						int lehm = 0;
						int eisen = 0;

						if (dorf.Rekrutieren.SpeertraegerAnzahl > 0) {
							holz += TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_SPEERTRAEGER_HOLZ;
							lehm += TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_SPEERTRAEGER_LEHM;
							eisen += TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_SPEERTRAEGER_EISEN;
						}

						if (dorf.Rekrutieren.SchwertkaempferAnzahl > 0) {
							holz += TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_SCHWERTKAEMPFER_HOLZ;
							lehm += TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_SCHWERTKAEMPFER_LEHM;
							eisen += TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_SCHWERTKAEMPFER_EISEN;
						}

						if (dorf.Rekrutieren.AxtkaempferAnzahl > 0) {
							holz += TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_AXTKAEMPFER_HOLZ;
							lehm += TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_AXTKAEMPFER_LEHM;
							eisen += TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_AXTKAEMPFER_EISEN;
						}

						if (dorf.Rekrutieren.BogenschuetzenAnzahl > 0) {
							holz += TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_BOGENSCHUETZEN_HOLZ;
							lehm += TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_BOGENSCHUETZEN_LEHM;
							eisen += TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_BOGENSCHUETZEN_EISEN;
						}

						if (dorf.Rekrutieren.SpaeherAnzahl > 0) {
							holz += TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_SPAEHER_HOLZ;
							lehm += TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_SPAEHER_LEHM;
							eisen += TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_SPAEHER_EISEN;
						}

						if (dorf.Rekrutieren.LeichteKavallerieAnzahl > 0) {
							holz += TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_LEICHTEKAVALLERIE_HOLZ;
							lehm += TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_LEICHTEKAVALLERIE_LEHM;
							eisen += TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_LEICHTEKAVALLERIE_EISEN;
						}

						if (dorf.Rekrutieren.BeritteneBogenschuetzenAnzahl > 0) {
							holz += TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_BERITTENEBOGENSCHUETZEN_HOLZ;
							lehm += TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_BERITTENEBOGENSCHUETZEN_LEHM;
							eisen += TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_BERITTENEBOGENSCHUETZEN_EISEN;
						}

						if (dorf.Rekrutieren.SchwereKavallerieAnzahl > 0) {
							holz += TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_SCHWEREKAVALLERIE_HOLZ;
							lehm += TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_SCHWEREKAVALLERIE_LEHM;
							eisen += TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_SCHWEREKAVALLERIE_EISEN;
						}

						if (dorf.Rekrutieren.RammboeckeAnzahl > 0) {
							holz += TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_RAMMBOECKE_HOLZ;
							lehm += TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_RAMMBOECKE_LEHM;
							eisen += TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_RAMMBOECKE_EISEN;
						}

						if (dorf.Rekrutieren.KatapulteAnzahl > 0) {
							holz += TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_KATAPULTE_HOLZ;
							lehm += TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_KATAPULTE_LEHM;
							eisen += TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_KATAPULTE_EISEN;
						}

						var gesamt = GetGemeinsameBauzeit(dorf, dorf.Rekrutieren);

						if (dorf.Rekrutieren.SpeertraegerAnzahl > 0 || dorf.Rekrutieren.SchwertkaempferAnzahl > 0 ||
						    dorf.Rekrutieren.AxtkaempferAnzahl > 0 || dorf.Rekrutieren.BogenschuetzenAnzahl > 0) {
							await RekrutBarracks(dorf.Rekrutieren, browser, dorf, holz, lehm, eisen, accountsettings
							                     , gesamt);
						}

						if ((dorf.Rekrutieren.SpaeherAnzahl > 0 || dorf.Rekrutieren.LeichteKavallerieAnzahl > 0 ||
						     dorf.Rekrutieren.BeritteneBogenschuetzenAnzahl > 0 ||
						     dorf.Rekrutieren.SchwereKavallerieAnzahl > 0)) {
							await RekrutStable(dorf.Rekrutieren, browser, dorf, holz, lehm, eisen, accountsettings);
						}

						if ((dorf.Rekrutieren.RammboeckeAnzahl > 0 || dorf.Rekrutieren.KatapulteAnzahl > 0)) {
							await RekrutWerkstatt(dorf.Rekrutieren, browser, dorf, holz, lehm, eisen, accountsettings);
						}

						if (dorf.Rekrutieren.PaladinAnzahl > 0 && App.Settings.Weltensettings.PaladinActive) {
							await RekrutPaladin(browser, dorf
							                    , (!string.IsNullOrEmpty(accountsettings.Uvname)
								                       ? accountsettings.Uvname
								                       : accountsettings.Accountname), accountsettings);
						}

						dorf.Dorfinformationen.LastRekrutierenZeitpunkt = DateTime.Now;
					}

					App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
						               ? accountsettings.Uvname
						               : accountsettings.Accountname) + ": " + " Rekruting Village: " +
					              dorf.Dorf.VillageId + " Name: " + dorf.Dorf.Name + " ended");
				}
			}

			App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
				               ? accountsettings.Uvname
				               : accountsettings.Accountname) + ": " + " Recruting ended");
			accountsettings.LastRekrutierenZeitpunkt = DateTime.Now;
		}

		private static TruppenTemplate GetUnterschied(TruppenTemplate ziel, TruppenTemplate vorhanden) {
			return new TruppenTemplate {
				                           SpeertraegerAnzahl = ziel.SpeertraegerAnzahl - vorhanden.SpeertraegerAnzahl
				                           , SchwertkaempferAnzahl =
					                           ziel.SchwertkaempferAnzahl - vorhanden.SchwertkaempferAnzahl
				                           , AxtkaempferAnzahl = ziel.AxtkaempferAnzahl - vorhanden.AxtkaempferAnzahl
				                           , BogenschuetzenAnzahl =
					                           ziel.BogenschuetzenAnzahl - vorhanden.BogenschuetzenAnzahl
				                           , SpaeherAnzahl = ziel.SpaeherAnzahl - vorhanden.SpaeherAnzahl
				                           , LeichteKavallerieAnzahl =
					                           ziel.LeichteKavallerieAnzahl - vorhanden.LeichteKavallerieAnzahl
				                           , BeritteneBogenschuetzenAnzahl =
					                           ziel.BeritteneBogenschuetzenAnzahl -
					                           vorhanden.BeritteneBogenschuetzenAnzahl
				                           , SchwereKavallerieAnzahl =
					                           ziel.SchwereKavallerieAnzahl - vorhanden.SchwereKavallerieAnzahl
				                           , RammboeckeAnzahl = ziel.RammboeckeAnzahl - vorhanden.RammboeckeAnzahl
				                           , KatapulteAnzahl = ziel.KatapulteAnzahl - vorhanden.KatapulteAnzahl
				                           , PaladinAnzahl = ziel.PaladinAnzahl - vorhanden.PaladinAnzahl
				                           , AdelsgeschlechterAnzahl =
					                           ziel.AdelsgeschlechterAnzahl - vorhanden.AdelsgeschlechterAnzahl
			                           };
		}

		public static async Task RekrutAdelsgeschlecht(Browser browser
		                                               , AccountDorfSettings dorf
		                                               , Accountsetting accountsettings
		                                               , int anzahl) {
			if (dorf.Dorfinformationen.Speicher.Holz - 40000 > dorf.RekrutierenHolzUebrigLassen &&
			    dorf.Dorfinformationen.Speicher.Lehm - 50000 > dorf.RekrutierenLehmUebrigLassen &&
			    dorf.Dorfinformationen.Speicher.Eisen - 50000 > dorf.RekrutierenEisenUebrigLassen) {
				string link = App.Settings.OperateLink + "village=" + dorf.Dorf.VillageId + "" +
				              accountsettings.UvZusatzFuerLink + "&screen=snob";
				browser.WebDriver.Navigate().GoToUrl(link);
				await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings, link);
				for (int i = 0; i < anzahl; i++) {
					if (browser.WebDriver.FindElements(By.XPath("//a[contains(@href,'&screen=snob&action=train&h')]"))
					           .Count > 0) {
						browser.WebDriver.FindElement(By.XPath("//a[contains(@href,'&screen=snob&action=train&h')]"))
						       .Click();
						await Rndm.Sleep(500, 1000);
						App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
							               ? accountsettings.Uvname
							               : accountsettings.Accountname) + ": " + " Recruting Account: " + 1 +
						              " snobs recruted");
						anzahl -= 1;
					} else {
						i = anzahl;
					}
				}

				await Rndm.Sleep(300);
				await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings, link);
			}
		}

		public static async Task RekrutPaladin(Browser browser
		                                       , AccountDorfSettings dorf
		                                       , string paladinname
		                                       , Accountsetting accountsettings) {
			if (dorf.Dorfinformationen.Truppen.PaladinAnzahl == 0 &&
			    dorf.Dorfinformationen.Speicher.Holz - 20 > dorf.RekrutierenHolzUebrigLassen &&
			    dorf.Dorfinformationen.Speicher.Lehm - 20 > dorf.RekrutierenLehmUebrigLassen &&
			    dorf.Dorfinformationen.Speicher.Eisen - 40 > dorf.RekrutierenEisenUebrigLassen) {
				string link = App.Settings.OperateLink + "village=" + dorf.Dorf.VillageId + "" +
				              accountsettings.UvZusatzFuerLink + "&screen=statue";
				browser.WebDriver.Navigate().GoToUrl(link);
				await Rndm.Sleep(300);
				await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings, link);
				try {
					browser.WebDriver
					       .FindElement(By.XPath("//a[contains(@class,'knight_recruit_launch btn btn-recruit')]"))
					       .Click();
					browser.WebDriver.WaitForAjax();
					browser.WebDriver.FindElement(By.XPath("//input[@id='knight_recruit_name']")).SendKeys(paladinname);
					await Rndm.Sleep(200, 400);
					browser.WebDriver.FindElement(By.XPath("//a[contains(@id,'knight_recruit_confirm')]")).Click();
					browser.WebDriver.WaitForAjax();
				} catch {
					try {
						browser.WebDriver.FindElement(By.XPath("//a[contains(@href,'&screen=statue&action=train&')]"))
						       .Click();
						await Rndm.Sleep(200, 400);
					} catch { }
				}
			}
		}

		public static async Task<int> RekrutBarracks(TruppenTemplate troopstorecruit
		                                             , Browser browser
		                                             , AccountDorfSettings dorf
		                                             , int geteiltWood
		                                             , int geteiltStone
		                                             , int geteiltIron
		                                             , Accountsetting accountsettings
		                                             , TimeSpan maxBauzeit) {
			if (accountsettings.RekrutierenActive) {
				string link = App.Settings.OperateLink + "village=" + dorf.Dorf.VillageId + "" +
				              accountsettings.UvZusatzFuerLink + "&screen=barracks";
				browser.WebDriver.Navigate().GoToUrl(link);
				await Rndm.Sleep(300);
				await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings, link);
				dorf.Dorfinformationen.Speicher.Holz =
					int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
					                                                 .Dorfuebersicht.SPEICHER_HOLZ_SELECTOR_XPATH))
					                 .Text);
				dorf.Dorfinformationen.Speicher.Lehm =
					int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
					                                                 .Dorfuebersicht.SPEICHER_LEHM_SELECTOR_XPATH))
					                 .Text);
				dorf.Dorfinformationen.Speicher.Eisen =
					int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
					                                                 .Dorfuebersicht.SPEICHER_EISEN_SELECTOR_XPATH))
					                 .Text);
				TruppenTemplate troopstorecruitt = troopstorecruit.Clone();
				try {
					List<IWebElement> barrackqueue = browser
					                                 .WebDriver.FindElement(By.Id("trainqueue_wrap_barracks"))
					                                 .FindElements(By.TagName("td")).ToList();
					foreach (var element in barrackqueue) {
						if (element.Text.Contains(NAMECONSTANTS.GetNameForServer(Names.Speertraeger))) {
							troopstorecruitt.SpeertraegerAnzahl -= int.Parse(element.Text.Split(' ')[0]);
						} else if (element.Text.Contains(NAMECONSTANTS.GetNameForServer(Names.Schwertkaempfer))) {
							troopstorecruitt.SchwertkaempferAnzahl -= int.Parse(element.Text.Split(' ')[0]);
						} else if (element.Text.Contains(NAMECONSTANTS.GetNameForServer(Names.Axtkaempfer))) {
							troopstorecruitt.AxtkaempferAnzahl -= int.Parse(element.Text.Split(' ')[0]);
						} else if (element.Text.Contains(NAMECONSTANTS.GetNameForServer(Names.Bogenschuetzen))) {
							troopstorecruitt.BogenschuetzenAnzahl -= int.Parse(element.Text.Split(' ')[0]);
						}
					}
				} catch { }

				try {
					List<string> texte = browser
					                     .WebDriver
					                     .FindElements(By.XPath("//form[@id='train_form']/descendant::tr[contains(@class,'row_a')]"))
					                     .Select(x => x.Text).ToList();
					if (troopstorecruitt.SpeertraegerAnzahl != 1) {
						foreach (var element in texte) {
							if (element.Contains(NAMECONSTANTS.GetNameForServer(Names.Speertraeger))) {
								dorf.Dorfinformationen.Truppen.SpeertraegerAnzahl =
									int.Parse(element.Split('/')[1].Split(' ')[0]);
								troopstorecruitt.SpeertraegerAnzahl -=
									dorf.Dorfinformationen.Truppen.SpeertraegerAnzahl;
							} else if (element.Contains(NAMECONSTANTS.GetNameForServer(Names.Schwertkaempfer))) {
								dorf.Dorfinformationen.Truppen.SchwertkaempferAnzahl =
									int.Parse(element.Split('/')[1].Split(' ')[0]);
								troopstorecruitt.SchwertkaempferAnzahl -=
									dorf.Dorfinformationen.Truppen.SchwertkaempferAnzahl;
							} else if (element.Contains(NAMECONSTANTS.GetNameForServer(Names.Axtkaempfer))) {
								dorf.Dorfinformationen.Truppen.AxtkaempferAnzahl =
									int.Parse(element.Split('/')[1].Split(' ')[0]);
								troopstorecruitt.AxtkaempferAnzahl -= dorf.Dorfinformationen.Truppen.AxtkaempferAnzahl;
							} else if (element.Contains(NAMECONSTANTS.GetNameForServer(Names.Bogenschuetzen))) {
								dorf.Dorfinformationen.Truppen.BogenschuetzenAnzahl =
									int.Parse(element.Split('/')[1].Split(' ')[0]);
								troopstorecruitt.BogenschuetzenAnzahl -=
									dorf.Dorfinformationen.Truppen.BogenschuetzenAnzahl;
							}
						}
					}
				} catch { }


				List<string> truppen = new List<string> {"spear", "sword", "axe", "archer"};
				int holz = geteiltWood;
				int lehm = geteiltStone;
				int eisen = geteiltIron;
				if (troopstorecruitt.SpeertraegerAnzahl == 0 && troopstorecruit.SpeertraegerAnzahl > 0) {
					holz -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_SPEERTRAEGER_HOLZ;
					lehm -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_SPEERTRAEGER_LEHM;
					eisen -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_SPEERTRAEGER_EISEN;
				}

				if (troopstorecruitt.SchwertkaempferAnzahl == 0 && troopstorecruit.SchwertkaempferAnzahl > 0) {
					holz -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_SCHWERTKAEMPFER_HOLZ;
					lehm -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_SCHWERTKAEMPFER_LEHM;
					eisen -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_SCHWERTKAEMPFER_EISEN;
				}

				if (troopstorecruitt.AxtkaempferAnzahl == 0 && troopstorecruit.AxtkaempferAnzahl > 0) {
					holz -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_AXTKAEMPFER_HOLZ;
					lehm -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_AXTKAEMPFER_LEHM;
					eisen -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_AXTKAEMPFER_EISEN;
				}

				if (troopstorecruitt.BogenschuetzenAnzahl == 0 && troopstorecruit.BogenschuetzenAnzahl > 0) {
					holz -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_BOGENSCHUETZEN_HOLZ;
					lehm -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_BOGENSCHUETZEN_LEHM;
					eisen -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_BOGENSCHUETZEN_EISEN;
				}

				for (int i = 0; i < 4; i++) {
					TruppenTemplate recruit = new TruppenTemplate();
					if (i == 0) {
						recruit.SpeertraegerAnzahl = troopstorecruitt.SpeertraegerAnzahl;
					} else if (i == 1) {
						recruit.SchwertkaempferAnzahl = troopstorecruitt.SchwertkaempferAnzahl;
					} else if (i == 2) {
						recruit.AxtkaempferAnzahl = troopstorecruitt.AxtkaempferAnzahl;
					} else if (i == 3) {
						recruit.BogenschuetzenAnzahl = troopstorecruitt.BogenschuetzenAnzahl;
					}

					foreach (var truppe in truppen) {
						switch (truppe) {
							case "spear":
								int holzamount =
									(dorf.Dorfinformationen.Speicher.Holz - dorf.RekrutierenHolzUebrigLassen) / holz;
								int lehmamount =
									(dorf.Dorfinformationen.Speicher.Lehm - dorf.RekrutierenLehmUebrigLassen) / lehm;
								int eisenamount =
									(dorf.Dorfinformationen.Speicher.Eisen - dorf.RekrutierenEisenUebrigLassen) / eisen;
								if (holzamount < recruit.SpeertraegerAnzahl) recruit.SpeertraegerAnzahl = holzamount;
								if (lehmamount < recruit.SpeertraegerAnzahl) recruit.SpeertraegerAnzahl = lehmamount;
								if (eisenamount < recruit.SpeertraegerAnzahl) recruit.SpeertraegerAnzahl = eisenamount;
								break;
							case "sword":
								holzamount = (dorf.Dorfinformationen.Speicher.Holz - dorf.RekrutierenHolzUebrigLassen) /
								             holz;
								lehmamount = (dorf.Dorfinformationen.Speicher.Lehm - dorf.RekrutierenLehmUebrigLassen) /
								             lehm;
								eisenamount =
									(dorf.Dorfinformationen.Speicher.Eisen - dorf.RekrutierenEisenUebrigLassen) / eisen;
								if (holzamount < recruit.SchwertkaempferAnzahl)
									recruit.SchwertkaempferAnzahl = holzamount;
								if (lehmamount < recruit.SchwertkaempferAnzahl)
									recruit.SchwertkaempferAnzahl = lehmamount;
								if (eisenamount < recruit.SchwertkaempferAnzahl)
									recruit.SchwertkaempferAnzahl = eisenamount;
								break;
							case "axe":
								holzamount = (dorf.Dorfinformationen.Speicher.Holz - dorf.RekrutierenHolzUebrigLassen) /
								             holz;
								lehmamount = (dorf.Dorfinformationen.Speicher.Lehm - dorf.RekrutierenLehmUebrigLassen) /
								             lehm;
								eisenamount =
									(dorf.Dorfinformationen.Speicher.Eisen - dorf.RekrutierenEisenUebrigLassen) / eisen;
								if (holzamount < recruit.AxtkaempferAnzahl) recruit.AxtkaempferAnzahl = holzamount;
								if (lehmamount < recruit.AxtkaempferAnzahl) recruit.AxtkaempferAnzahl = lehmamount;
								if (eisenamount < recruit.AxtkaempferAnzahl) recruit.AxtkaempferAnzahl = eisenamount;
								break;
							case "archer":
								holzamount = (dorf.Dorfinformationen.Speicher.Holz - dorf.RekrutierenHolzUebrigLassen) /
								             holz;
								lehmamount = (dorf.Dorfinformationen.Speicher.Lehm - dorf.RekrutierenLehmUebrigLassen) /
								             lehm;
								eisenamount =
									(dorf.Dorfinformationen.Speicher.Eisen - dorf.RekrutierenEisenUebrigLassen) / eisen;
								if (holzamount < recruit.BogenschuetzenAnzahl)
									recruit.BogenschuetzenAnzahl = holzamount;
								if (lehmamount < recruit.BogenschuetzenAnzahl)
									recruit.BogenschuetzenAnzahl = lehmamount;
								if (eisenamount < recruit.BogenschuetzenAnzahl)
									recruit.BogenschuetzenAnzahl = eisenamount;
								break;
						}
					}

					int maxspy = 0;
					try {
						string speartorecruit = browser.WebDriver.FindElement(By.Id("spear_0_a")).Text;
						maxspy = int.Parse(speartorecruit.Substring(1, speartorecruit.Length - 2));
					} catch { }

					int maxlight = 0;
					try {
						string swordtorecruit = browser.WebDriver.FindElement(By.Id("sword_0_a")).Text;
						maxlight = int.Parse(swordtorecruit.Substring(1, swordtorecruit.Length - 2));
					} catch { }

					int maxmarcher = 0;
					try {
						string axetorecruit = browser.WebDriver.FindElement(By.Id("axe_0_a")).Text;
						maxmarcher = int.Parse(axetorecruit.Substring(1, axetorecruit.Length - 2));
					} catch { }

					int maxheavy = 0;
					try {
						string archertorecruit = browser.WebDriver.FindElement(By.Id("archer_0_a")).Text;
						maxheavy = int.Parse(archertorecruit.Substring(1, archertorecruit.Length - 2));
					} catch { }

					if (maxspy < recruit.SpeertraegerAnzahl) {
						recruit.SpeertraegerAnzahl = maxspy;
					}

					if (maxlight < recruit.SchwertkaempferAnzahl) {
						recruit.SchwertkaempferAnzahl = maxlight;
					}

					if (maxmarcher < recruit.AxtkaempferAnzahl) {
						recruit.AxtkaempferAnzahl = maxmarcher;
					}

					if (maxheavy < recruit.BogenschuetzenAnzahl) {
						recruit.BogenschuetzenAnzahl = maxheavy;
					}

					((IJavaScriptExecutor) browser.WebDriver).ExecuteScript("$('#chat-wrapper').hide();");
					if (recruit.SpeertraegerAnzahl > 0) {
						IWebElement element = browser.WebDriver.FindElement(By.XPath("//input[@id='spear_0']"));
						element.Clear();
						element.SendKeys(recruit.SpeertraegerAnzahl.ToString());
					}

					if (recruit.SchwertkaempferAnzahl > 0) {
						IWebElement element = browser.WebDriver.FindElement(By.XPath("//input[@id='sword_0']"));
						element.Clear();
						element.SendKeys(recruit.SchwertkaempferAnzahl.ToString());
					}

					if (recruit.AxtkaempferAnzahl > 0) {
						IWebElement element = browser.WebDriver.FindElement(By.XPath("//input[@id='axe_0']"));
						element.Clear();
						element.SendKeys(recruit.AxtkaempferAnzahl.ToString());
					}

					if (recruit.BogenschuetzenAnzahl > 0) {
						IWebElement element = browser.WebDriver.FindElement(By.XPath("//input[@id='archer_0']"));
						element.Clear();
						element.SendKeys(recruit.BogenschuetzenAnzahl.ToString());
					}

					if (recruit.SpeertraegerAnzahl > 0 || recruit.SchwertkaempferAnzahl > 0 ||
					    recruit.AxtkaempferAnzahl > 0 || recruit.BogenschuetzenAnzahl > 0) {
						IWebElement element =
							browser.WebDriver.FindElement(By.XPath("//input[contains(@class,'btn btn-recruit')]"));
						((IJavaScriptExecutor) browser.WebDriver).ExecuteScript("arguments[0].scrollIntoView(true);"
						                                                        , element);
						((IJavaScriptExecutor) browser.WebDriver).ExecuteScript("$('#menu_row').hide();");
						((IJavaScriptExecutor) browser.WebDriver).ExecuteScript("$('.top_bar').hide();");
						element.Click();
						await Rndm.Sleep(200, 400);
						if (recruit.AxtkaempferAnzahl > 0) {
							dorf.Dorfinformationen.AxtkaempferErforscht = true;
						}

						if (recruit.BogenschuetzenAnzahl > 0) {
							dorf.Dorfinformationen.BogenschuetzenErforscht = true;
						}
					}

					if (recruit.SpeertraegerAnzahl > 0) {
						App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
							               ? accountsettings.Uvname
							               : accountsettings.Accountname) + ": " + " Recruting Village: " +
						              dorf.Dorf.VillageId + " " + recruit.SpeertraegerAnzahl + " spears recruted");
					}

					if (recruit.SchwertkaempferAnzahl > 0) {
						App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
							               ? accountsettings.Uvname
							               : accountsettings.Accountname) + ": " + " Recruting Village: " +
						              dorf.Dorf.VillageId + " " + recruit.SchwertkaempferAnzahl + " swords recruted");
					}

					if (recruit.AxtkaempferAnzahl > 0) {
						App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
							               ? accountsettings.Uvname
							               : accountsettings.Accountname) + ": " + " Recruting Village: " +
						              dorf.Dorf.VillageId + " " + recruit.AxtkaempferAnzahl + " axe recruted");
					}

					if (recruit.BogenschuetzenAnzahl > 0) {
						App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
							               ? accountsettings.Uvname
							               : accountsettings.Accountname) + ": " + " Recruting Village: " +
						              dorf.Dorf.VillageId + " " + recruit.BogenschuetzenAnzahl + " archer recruted");
					}
				}

				return troopstorecruitt.SpeertraegerAnzahl + troopstorecruitt.SchwertkaempferAnzahl +
				       troopstorecruitt.AxtkaempferAnzahl + troopstorecruitt.BogenschuetzenAnzahl;
			}

			return 0;
		}

		private static async Task RekrutStable(TruppenTemplate troopstorecruit
		                                       , Browser browser
		                                       , AccountDorfSettings dorf
		                                       , int geteiltWood
		                                       , int geteiltStone
		                                       , int geteiltIron
		                                       , Accountsetting accountsettings) {
			if (accountsettings.RekrutierenActive) {
				string link = App.Settings.OperateLink + "village=" + dorf.Dorf.VillageId + "" +
				              accountsettings.UvZusatzFuerLink + "&screen=stable";
				browser.WebDriver.Navigate().GoToUrl(link);
				await Rndm.Sleep(300);
				await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings, link);
				dorf.Dorfinformationen.Speicher.Holz =
					int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
					                                                 .Dorfuebersicht.SPEICHER_HOLZ_SELECTOR_XPATH))
					                 .Text);
				dorf.Dorfinformationen.Speicher.Lehm =
					int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
					                                                 .Dorfuebersicht.SPEICHER_LEHM_SELECTOR_XPATH))
					                 .Text);
				dorf.Dorfinformationen.Speicher.Eisen =
					int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
					                                                 .Dorfuebersicht.SPEICHER_EISEN_SELECTOR_XPATH))
					                 .Text);
				TruppenTemplate troopstorecruitt = troopstorecruit.Clone();
				try {
					List<IWebElement> barrackqueue = browser
					                                 .WebDriver.FindElement(By.Id("trainqueue_wrap_stable"))
					                                 .FindElements(By.TagName("td")).ToList();
					foreach (var element in barrackqueue) {
						if (element.Text.Contains(NAMECONSTANTS.GetNameForServer(Names.Spaeher))) {
							troopstorecruitt.SpaeherAnzahl -= int.Parse(element.Text.Split(' ')[0]);
						} else if (element.Text.Contains(NAMECONSTANTS.GetNameForServer(Names.Leichte))) {
							troopstorecruitt.LeichteKavallerieAnzahl -= int.Parse(element.Text.Split(' ')[0]);
						} else if (element.Text.Contains(NAMECONSTANTS.GetNameForServer(Names.Berittene))) {
							troopstorecruitt.BeritteneBogenschuetzenAnzahl -= int.Parse(element.Text.Split(' ')[0]);
						} else if (element.Text.Contains(NAMECONSTANTS.GetNameForServer(Names.Schwere))) {
							troopstorecruitt.SchwereKavallerieAnzahl -= int.Parse(element.Text.Split(' ')[0]);
						}
					}
				} catch { }

				try {
					List<string> texte = browser
					                     .WebDriver
					                     .FindElements(By.XPath("//form[@id='train_form']/descendant::tr[contains(@class,'row_a')]"))
					                     .Select(x => x.Text).ToList();
					foreach (var element in texte) {
						if (element.Contains(NAMECONSTANTS.GetNameForServer(Names.Spaeher))) {
							dorf.Dorfinformationen.Truppen.SpaeherAnzahl =
								int.Parse(element.Split('/')[1].Split(' ')[0]);
							troopstorecruitt.SpaeherAnzahl -= dorf.Dorfinformationen.Truppen.SpaeherAnzahl;
						} else if (element.Contains(NAMECONSTANTS.GetNameForServer(Names.Leichte))) {
							dorf.Dorfinformationen.Truppen.LeichteKavallerieAnzahl =
								int.Parse(element.Split('/')[1].Split(' ')[0]);
							troopstorecruitt.LeichteKavallerieAnzahl -=
								dorf.Dorfinformationen.Truppen.LeichteKavallerieAnzahl;
						} else if (element.Contains(NAMECONSTANTS.GetNameForServer(Names.Berittene))) {
							dorf.Dorfinformationen.Truppen.BeritteneBogenschuetzenAnzahl =
								int.Parse(element.Split('/')[1].Split(' ')[0]);
							troopstorecruitt.BeritteneBogenschuetzenAnzahl -=
								dorf.Dorfinformationen.Truppen.BeritteneBogenschuetzenAnzahl;
						} else if (element.Contains(NAMECONSTANTS.GetNameForServer(Names.Schwere))) {
							dorf.Dorfinformationen.Truppen.SchwereKavallerieAnzahl =
								int.Parse(element.Split('/')[1].Split(' ')[0]);
							troopstorecruitt.SchwereKavallerieAnzahl -=
								dorf.Dorfinformationen.Truppen.SchwereKavallerieAnzahl;
						}
					}
				} catch { }

				List<string> truppen = new List<string> {"spear", "sword", "axe", "archer"};
				int holz = geteiltWood;
				int lehm = geteiltStone;
				int eisen = geteiltIron;
				if (troopstorecruitt.SpaeherAnzahl == 0 && troopstorecruit.SpaeherAnzahl > 0) {
					holz -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_SPAEHER_HOLZ;
					lehm -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_SPAEHER_LEHM;
					eisen -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_SPAEHER_EISEN;
				}

				if (troopstorecruitt.LeichteKavallerieAnzahl == 0 && troopstorecruit.LeichteKavallerieAnzahl > 0) {
					holz -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_LEICHTEKAVALLERIE_HOLZ;
					lehm -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_LEICHTEKAVALLERIE_LEHM;
					eisen -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_LEICHTEKAVALLERIE_EISEN;
				}

				if (troopstorecruitt.BeritteneBogenschuetzenAnzahl == 0 &&
				    troopstorecruit.BeritteneBogenschuetzenAnzahl > 0) {
					holz -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_BERITTENEBOGENSCHUETZEN_HOLZ;
					lehm -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_BERITTENEBOGENSCHUETZEN_LEHM;
					eisen -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_BERITTENEBOGENSCHUETZEN_EISEN;
				}

				if (troopstorecruitt.SchwereKavallerieAnzahl == 0 && troopstorecruit.SchwereKavallerieAnzahl > 0) {
					holz -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_SCHWEREKAVALLERIE_HOLZ;
					lehm -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_SCHWEREKAVALLERIE_LEHM;
					eisen -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_SCHWEREKAVALLERIE_EISEN;
				}

				for (int i = 0; i < 4; i++) {
					TruppenTemplate recruit = new TruppenTemplate();
					if (i == 0) {
						recruit.SpaeherAnzahl = troopstorecruitt.SpaeherAnzahl;
					} else if (i == 1) {
						recruit.LeichteKavallerieAnzahl = troopstorecruitt.LeichteKavallerieAnzahl;
					} else if (i == 2) {
						recruit.BeritteneBogenschuetzenAnzahl = troopstorecruitt.BeritteneBogenschuetzenAnzahl;
					} else if (i == 3) {
						recruit.SchwereKavallerieAnzahl = troopstorecruitt.SchwereKavallerieAnzahl;
					}


					foreach (var truppe in truppen) {
						switch (truppe) {
							case "spear":
								int holzamount = (holz != 0)
									                 ? (dorf.Dorfinformationen.Speicher.Holz -
									                    dorf.RekrutierenHolzUebrigLassen) / holz
									                 : 0;
								int lehmamount = (lehm != 0)
									                 ? (dorf.Dorfinformationen.Speicher.Lehm -
									                    dorf.RekrutierenLehmUebrigLassen) / lehm
									                 : 0;
								int eisenamount = (eisen != 0)
									                  ? (dorf.Dorfinformationen.Speicher.Eisen -
									                     dorf.RekrutierenEisenUebrigLassen) / eisen
									                  : 0;
								if (holzamount < recruit.SpaeherAnzahl) recruit.SpaeherAnzahl = holzamount;
								if (lehmamount < recruit.SpaeherAnzahl) recruit.SpaeherAnzahl = lehmamount;
								if (eisenamount < recruit.SpaeherAnzahl) recruit.SpaeherAnzahl = eisenamount;
								break;
							case "sword":
								holzamount = (holz != 0)
									             ? (dorf.Dorfinformationen.Speicher.Holz -
									                dorf.RekrutierenHolzUebrigLassen) / holz
									             : 0;
								lehmamount = (lehm != 0)
									             ? (dorf.Dorfinformationen.Speicher.Lehm -
									                dorf.RekrutierenLehmUebrigLassen) / lehm
									             : 0;
								eisenamount = (eisen != 0)
									              ? (dorf.Dorfinformationen.Speicher.Eisen -
									                 dorf.RekrutierenEisenUebrigLassen) / eisen
									              : 0;
								if (holzamount < recruit.LeichteKavallerieAnzahl)
									recruit.LeichteKavallerieAnzahl = holzamount;
								if (lehmamount < recruit.LeichteKavallerieAnzahl)
									recruit.LeichteKavallerieAnzahl = lehmamount;
								if (eisenamount < recruit.LeichteKavallerieAnzahl)
									recruit.LeichteKavallerieAnzahl = eisenamount;
								break;
							case "axe":
								holzamount = (holz != 0)
									             ? (dorf.Dorfinformationen.Speicher.Holz -
									                dorf.RekrutierenHolzUebrigLassen) / holz
									             : 0;
								lehmamount = (lehm != 0)
									             ? (dorf.Dorfinformationen.Speicher.Lehm -
									                dorf.RekrutierenLehmUebrigLassen) / lehm
									             : 0;
								eisenamount = (eisen != 0)
									              ? (dorf.Dorfinformationen.Speicher.Eisen -
									                 dorf.RekrutierenEisenUebrigLassen) / eisen
									              : 0;
								if (holzamount < recruit.BeritteneBogenschuetzenAnzahl)
									recruit.BeritteneBogenschuetzenAnzahl = holzamount;
								if (lehmamount < recruit.BeritteneBogenschuetzenAnzahl)
									recruit.BeritteneBogenschuetzenAnzahl = lehmamount;
								if (eisenamount < recruit.BeritteneBogenschuetzenAnzahl)
									recruit.BeritteneBogenschuetzenAnzahl = eisenamount;
								break;
							case "archer":
								holzamount = (holz != 0)
									             ? (dorf.Dorfinformationen.Speicher.Holz -
									                dorf.RekrutierenHolzUebrigLassen) / holz
									             : 0;
								lehmamount = (lehm != 0)
									             ? (dorf.Dorfinformationen.Speicher.Lehm -
									                dorf.RekrutierenLehmUebrigLassen) / lehm
									             : 0;
								eisenamount = (eisen != 0)
									              ? (dorf.Dorfinformationen.Speicher.Eisen -
									                 dorf.RekrutierenEisenUebrigLassen) / eisen
									              : 0;
								if (holzamount < recruit.SchwereKavallerieAnzahl)
									recruit.SchwereKavallerieAnzahl = holzamount;
								if (lehmamount < recruit.SchwereKavallerieAnzahl)
									recruit.SchwereKavallerieAnzahl = lehmamount;
								if (eisenamount < recruit.SchwereKavallerieAnzahl)
									recruit.SchwereKavallerieAnzahl = eisenamount;
								break;
						}
					}

					int maxspy = 0;
					try {
						string speartorecruit = browser.WebDriver.FindElement(By.Id("spy_0_a")).Text;
						maxspy = int.Parse(speartorecruit.Substring(1, speartorecruit.Length - 2));
					} catch { }

					int maxlight = 0;
					try {
						string swordtorecruit = browser.WebDriver.FindElement(By.Id("light_0_a")).Text;
						maxlight = int.Parse(swordtorecruit.Substring(1, swordtorecruit.Length - 2));
					} catch { }

					int maxmarcher = 0;
					try {
						string axetorecruit = browser.WebDriver.FindElement(By.Id("marcher_0_a")).Text;
						maxmarcher = int.Parse(axetorecruit.Substring(1, axetorecruit.Length - 2));
					} catch { }

					int maxheavy = 0;
					try {
						string archertorecruit = browser.WebDriver.FindElement(By.Id("heavy_0_a")).Text;
						maxheavy = int.Parse(archertorecruit.Substring(1, archertorecruit.Length - 2));
					} catch { }

					if (maxspy < recruit.SpaeherAnzahl) {
						recruit.SpaeherAnzahl = maxspy;
					}

					((IJavaScriptExecutor) browser.WebDriver).ExecuteScript("$('#chat-wrapper').hide();");
					if (recruit.SpaeherAnzahl > 0) {
						IWebElement element = browser.WebDriver.FindElement(By.XPath("//input[@id='spy_0']"));
						element.Clear();
						element.SendKeys(recruit.SpaeherAnzahl.ToString());
					}

					if (maxlight < recruit.LeichteKavallerieAnzahl) {
						recruit.LeichteKavallerieAnzahl = maxlight;
					}

					if (recruit.LeichteKavallerieAnzahl > 0) {
						IWebElement element = browser.WebDriver.FindElement(By.XPath("//input[@id='light_0']"));
						element.Clear();
						element.SendKeys(recruit.LeichteKavallerieAnzahl.ToString());
					}

					if (maxmarcher < recruit.BeritteneBogenschuetzenAnzahl) {
						recruit.BeritteneBogenschuetzenAnzahl = maxmarcher;
					}

					if (recruit.AxtkaempferAnzahl > 0) {
						IWebElement element = browser.WebDriver.FindElement(By.XPath("//input[@id='marcher_0']"));
						element.Clear();
						element.SendKeys(recruit.BeritteneBogenschuetzenAnzahl.ToString());
					}

					if (maxheavy < recruit.SchwereKavallerieAnzahl) {
						recruit.SchwereKavallerieAnzahl = maxheavy;
					}

					if (recruit.SchwereKavallerieAnzahl > 0) {
						IWebElement element = browser.WebDriver.FindElement(By.XPath("//input[@id='heavy_0']"));
						element.Clear();
						element.SendKeys(recruit.SchwereKavallerieAnzahl.ToString());
					}

					if (recruit.SpaeherAnzahl > 0 || recruit.LeichteKavallerieAnzahl > 0 ||
					    recruit.BeritteneBogenschuetzenAnzahl > 0 || recruit.SchwereKavallerieAnzahl > 0) {
						IWebElement element =
							browser.WebDriver.FindElement(By.XPath("//input[contains(@class,'btn btn-recruit')]"));
						((IJavaScriptExecutor) browser.WebDriver).ExecuteScript("arguments[0].scrollIntoView(true);"
						                                                        , element);
						((IJavaScriptExecutor) browser.WebDriver).ExecuteScript("$('#menu_row').hide();");
						((IJavaScriptExecutor) browser.WebDriver).ExecuteScript("$('.top_bar').hide();");
						element.Click();
						await Rndm.Sleep(200, 400);
						if (recruit.SpaeherAnzahl > 0) {
							dorf.Dorfinformationen.SpaeherErforscht = true;
						}

						if (recruit.LeichteKavallerieAnzahl > 0) {
							dorf.Dorfinformationen.LeichteKavallerieErforscht = true;
						}

						if (recruit.BeritteneBogenschuetzenAnzahl > 0) {
							dorf.Dorfinformationen.BeritteneBogenschuetzenErforscht = true;
						}

						if (recruit.SchwereKavallerieAnzahl > 0) {
							dorf.Dorfinformationen.SchwereKavallerieErforscht = true;
						}
					}

					if (recruit.SpaeherAnzahl > 0) {
						App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
							               ? accountsettings.Uvname
							               : accountsettings.Accountname) + ": " + " Recruting Village: " +
						              dorf.Dorf.VillageId + " " + recruit.SpaeherAnzahl + " spy recruted");
					}

					if (recruit.LeichteKavallerieAnzahl > 0) {
						App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
							               ? accountsettings.Uvname
							               : accountsettings.Accountname) + ": " + " Recruting Village: " +
						              dorf.Dorf.VillageId + " " + recruit.LeichteKavallerieAnzahl + " light recruted");
					}

					if (recruit.BeritteneBogenschuetzenAnzahl > 0) {
						App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
							               ? accountsettings.Uvname
							               : accountsettings.Accountname) + ": " + " Recruting Village: " +
						              dorf.Dorf.VillageId + " " + recruit.BeritteneBogenschuetzenAnzahl +
						              " marcher recruted");
					}

					if (recruit.SchwereKavallerieAnzahl > 0) {
						App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
							               ? accountsettings.Uvname
							               : accountsettings.Accountname) + ": " + " Recruting Village: " +
						              dorf.Dorf.VillageId + " " + recruit.SchwereKavallerieAnzahl + " heavy recruted");
					}
				}
			}
		}

		private static async Task RekrutWerkstatt(TruppenTemplate troopstorecruit
		                                          , Browser browser
		                                          , AccountDorfSettings dorf
		                                          , int geteiltWood
		                                          , int geteiltStone
		                                          , int geteiltIron
		                                          , Accountsetting accountsettings) {
			string link = App.Settings.OperateLink + "village=" + dorf.Dorf.VillageId + "" +
			              accountsettings.UvZusatzFuerLink + "&screen=garage";
			browser.WebDriver.Navigate().GoToUrl(link);
			await Rndm.Sleep(300);
			await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings, link);
			TruppenTemplate troopstorecruitt = troopstorecruit.Clone();
			try {
				List<IWebElement> barrackqueue = browser
				                                 .WebDriver.FindElement(By.Id("trainqueue_wrap_garage"))
				                                 .FindElements(By.TagName("td")).ToList();
				foreach (var element in barrackqueue) {
					if (element.Text.Contains(NAMECONSTANTS.GetNameForServer(Names.Ramm))) {
						troopstorecruit.RammboeckeAnzahl -= int.Parse(element.Text.Split(' ')[0]);
					} else if (element.Text.Contains(NAMECONSTANTS.GetNameForServer(Names.Kata))) {
						troopstorecruit.KatapulteAnzahl -= int.Parse(element.Text.Split(' ')[0]);
					}
				}
			} catch { }

			try {
				List<string> texte = browser
				                     .WebDriver
				                     .FindElements(By.XPath("//form[@id='train_form']/descendant::tr[contains(@class,'row_a')]"))
				                     .Select(x => x.Text).ToList();
				foreach (var element in texte) {
					if (element.Contains(NAMECONSTANTS.GetNameForServer(Names.Ramm))) {
						dorf.Dorfinformationen.Truppen.RammboeckeAnzahl =
							int.Parse(element.Split('/')[1].Split(' ')[0]);
						troopstorecruitt.RammboeckeAnzahl -= dorf.Dorfinformationen.Truppen.RammboeckeAnzahl;
					} else if (element.Contains(NAMECONSTANTS.GetNameForServer(Names.Kata))) {
						dorf.Dorfinformationen.Truppen.KatapulteAnzahl = int.Parse(element.Split('/')[1].Split(' ')[0]);
						troopstorecruitt.KatapulteAnzahl -= dorf.Dorfinformationen.Truppen.KatapulteAnzahl;
					}
				}
			} catch { }

			List<string> truppen = new List<string> {"spear", "sword"};
			int holz = geteiltWood;
			int lehm = geteiltStone;
			int eisen = geteiltIron;
			if (troopstorecruitt.RammboeckeAnzahl == 0 && troopstorecruit.RammboeckeAnzahl > 0) {
				holz -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_RAMMBOECKE_HOLZ;
				lehm -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_RAMMBOECKE_LEHM;
				eisen -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_RAMMBOECKE_EISEN;
			}

			if (troopstorecruitt.KatapulteAnzahl == 0 && troopstorecruit.KatapulteAnzahl > 0) {
				holz -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_KATAPULTE_HOLZ;
				lehm -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_KATAPULTE_LEHM;
				eisen -= TRUPPENCONSTANTS.ROHSTOFFVERBRAUCH_KATAPULTE_EISEN;
			}

			for (int i = 0; i < 2; i++) {
				TruppenTemplate recruit = new TruppenTemplate();
				if (i == 0) {
					recruit.RammboeckeAnzahl = troopstorecruitt.RammboeckeAnzahl;
				} else if (i == 1) {
					recruit.KatapulteAnzahl = troopstorecruitt.KatapulteAnzahl;
				}


				foreach (var truppe in truppen) {
					switch (truppe) {
						case "spear":
							int holzamount = (dorf.Dorfinformationen.Speicher.Holz - dorf.RekrutierenHolzUebrigLassen) /
							                 holz;
							int lehmamount = (dorf.Dorfinformationen.Speicher.Lehm - dorf.RekrutierenLehmUebrigLassen) /
							                 lehm;
							int eisenamount =
								(dorf.Dorfinformationen.Speicher.Eisen - dorf.RekrutierenEisenUebrigLassen) / eisen;
							if (holzamount < recruit.RammboeckeAnzahl) recruit.RammboeckeAnzahl = holzamount;
							if (lehmamount < recruit.RammboeckeAnzahl) recruit.RammboeckeAnzahl = lehmamount;
							if (eisenamount < recruit.RammboeckeAnzahl) recruit.RammboeckeAnzahl = eisenamount;
							break;
						case "sword":
							holzamount = (dorf.Dorfinformationen.Speicher.Holz - dorf.RekrutierenHolzUebrigLassen) /
							             holz;
							lehmamount = (dorf.Dorfinformationen.Speicher.Lehm - dorf.RekrutierenLehmUebrigLassen) /
							             lehm;
							eisenamount = (dorf.Dorfinformationen.Speicher.Eisen - dorf.RekrutierenEisenUebrigLassen) /
							              eisen;
							if (holzamount < recruit.KatapulteAnzahl) recruit.KatapulteAnzahl = holzamount;
							if (lehmamount < recruit.KatapulteAnzahl) recruit.KatapulteAnzahl = lehmamount;
							if (eisenamount < recruit.KatapulteAnzahl) recruit.KatapulteAnzahl = eisenamount;
							break;
					}
				}

				int maxspy = 0;
				try {
					string speartorecruit = browser.WebDriver.FindElement(By.Id("ram_0_a")).Text;
					maxspy = int.Parse(speartorecruit.Substring(1, speartorecruit.Length - 2));
				} catch { }

				int maxlight = 0;
				try {
					string swordtorecruit = browser.WebDriver.FindElement(By.Id("cat_0_a")).Text;
					maxlight = int.Parse(swordtorecruit.Substring(1, swordtorecruit.Length - 2));
				} catch { }

				if (maxspy < recruit.RammboeckeAnzahl) {
					recruit.RammboeckeAnzahl = maxspy;
				}

				((IJavaScriptExecutor) browser.WebDriver).ExecuteScript("$('#chat-wrapper').hide();");
				if (recruit.RammboeckeAnzahl > 0) {
					IWebElement element = browser.WebDriver.FindElement(By.XPath("//input[@id='ram_0']"));
					element.Clear();
					element.SendKeys(recruit.RammboeckeAnzahl.ToString());
				}

				if (maxlight < recruit.KatapulteAnzahl) {
					recruit.KatapulteAnzahl = maxlight;
				}

				if (recruit.KatapulteAnzahl > 0) {
					IWebElement element = browser.WebDriver.FindElement(By.XPath("//input[@id='cat_0']"));
					element.Clear();
					element.SendKeys(recruit.KatapulteAnzahl.ToString());
				}

				if (recruit.RammboeckeAnzahl > 0 || recruit.KatapulteAnzahl > 0) {
					IWebElement element =
						browser.WebDriver.FindElement(By.XPath("//input[contains(@class,'btn btn-recruit')]"));
					((IJavaScriptExecutor) browser.WebDriver).ExecuteScript("arguments[0].scrollIntoView(true);"
					                                                        , element);
					((IJavaScriptExecutor) browser.WebDriver).ExecuteScript("$('#menu_row').hide();");
					((IJavaScriptExecutor) browser.WebDriver).ExecuteScript("$('.top_bar').hide();");
					element.Click();
					await Rndm.Sleep(200, 400);
					if (recruit.RammboeckeAnzahl > 0) {
						dorf.Dorfinformationen.RammboeckeErforscht = true;
					}

					if (recruit.KatapulteAnzahl > 0) {
						dorf.Dorfinformationen.KatapulteErforscht = true;
					}
				}

				if (recruit.RammboeckeAnzahl > 0) {
					App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
						               ? accountsettings.Uvname
						               : accountsettings.Accountname) + ": " + " Recruting Village: " +
					              dorf.Dorf.VillageId + " " + recruit.RammboeckeAnzahl + " ram recruted");
				}

				if (recruit.KatapulteAnzahl > 0) {
					App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
						               ? accountsettings.Uvname
						               : accountsettings.Accountname) + ": " + " Recruting Village: " +
					              dorf.Dorf.VillageId + " " + recruit.KatapulteAnzahl + " cat recruted");
				}
			}
		}
	}
}