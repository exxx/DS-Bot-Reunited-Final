﻿using Ds_Bot_Reunited_NEWUI.Accountsettings.Ds_Bot_Reunited.Accountsettings;

namespace Ds_Bot_Reunited_NEWUI.Angreifensettings {
    public class BewegungAccountsettingsmerged {
        public Accountsetting Accountsetting { get; set; }
        public TruppenbewegungViewModel Bewegung { get; set; }
    }
}