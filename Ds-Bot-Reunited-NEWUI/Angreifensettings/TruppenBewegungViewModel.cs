﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using Ds_Bot_Reunited_NEWUI.Accountsettings;
using Ds_Bot_Reunited_NEWUI.Annotations;
using Ds_Bot_Reunited_NEWUI.BauenModul;
using Ds_Bot_Reunited_NEWUI.Constants;
using Ds_Bot_Reunited_NEWUI.Farmen;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.Properties;
using Ds_Bot_Reunited_NEWUI.SpieldatenViewModel;
using Truppenbewegung = Ds_Bot_Reunited_NEWUI.AngreifenModul.Truppenbewegung;

namespace Ds_Bot_Reunited_NEWUI.Angreifensettings {
    [Obfuscation(Exclude = true, Feature = "renaming")]
    public sealed class TruppenbewegungViewModel : INotifyPropertyChanged {
        private readonly int _playerid;
        private ICommand _agAngriffEntfernenCommand;
        private ICommand _agAngriffHinzufuegenCommand;

        private Color _background;
        private ICollectionView _begleittruppenView;

        private ICollectionView _bewegungsartenView;

        private ICommand _changeStatusCommand;
        private ICommand _getLinkCommand;
        private bool _isSelected;
        private ICollectionView _kattaZieleView;
        private ICommand _openTemplateForAgAngriffViewCommand;
        private ICommand _openTemplateViewCommand;
        public AngreifenSettingsViewModel Vm;

        public TruppenbewegungViewModel(DorfViewModel senderDorf, DorfViewModel empfaengerDorf, int playerid = 0) {
            _playerid = playerid;
            Model = new Truppenbewegung();
            Active = "N";
            Model.SenderDorf = senderDorf;
            Model.EmpfaengerDorf = empfaengerDorf;
            Abschickzeitpunkt = DateTime.Now.AddMinutes(5).ToString("dd.MM.yyyy HH:mm:ss.fff", CultureInfo.InvariantCulture);
            Speertraeger = "0";
            Schwertkaempfer = "0";
            Axtkaempfer = "0";
            Bogenschuetzen = "0";
            Spaeher = "0";
            LeichteKavallerie = "0";
            BeritteneBogenschuetzen = "0";
            SchwereKavallerie = "0";
            Rammboecke = "0";
            Katapulte = "0";
            Paladin = "0";
            Adelsgeschlechter = "0";
            Angriffsinformationen = "";
            var bewegungsartenList = new AsyncObservableCollection<string>(new List<string> {Resources.Support, Resources.Attack});
            Bewegungsarten = CollectionViewSource.GetDefaultView(bewegungsartenList);
            var kattaziele = new AsyncObservableCollection<string>();
            kattaziele.Add(NAMECONSTANTS.GetNameForServer(Names.ReportMain));
            kattaziele.Add(NAMECONSTANTS.GetNameForServer(Names.ReportBarracks));
            kattaziele.Add(NAMECONSTANTS.GetNameForServer(Names.ReportStable));
            kattaziele.Add(NAMECONSTANTS.GetNameForServer(Names.ReportSmith));
            kattaziele.Add(NAMECONSTANTS.GetNameForServer(Names.ReportSnob));
            kattaziele.Add(NAMECONSTANTS.GetNameForServer(Names.ReportMarktplatz));
            kattaziele.Add(NAMECONSTANTS.GetNameForServer(Names.ReportHolz));
            kattaziele.Add(NAMECONSTANTS.GetNameForServer(Names.ReportLehm));
            kattaziele.Add(NAMECONSTANTS.GetNameForServer(Names.ReportEisen));
            kattaziele.Add(NAMECONSTANTS.GetNameForServer(Names.ReportSpeicher));
            kattaziele.Add(NAMECONSTANTS.GetNameForServer(Names.ReportFarm));
            kattaziele.Add(NAMECONSTANTS.GetNameForServer(Names.ReportWall));
            if (App.Settings.Weltensettings.WatchtowerActive)
                kattaziele.Add(NAMECONSTANTS.GetNameForServer(Names.ReportWachturm));
            if (App.Settings.Weltensettings.KircheActive)
                kattaziele.Add(NAMECONSTANTS.GetNameForServer(Names.ReportChurch));
            KattaZiele = CollectionViewSource.GetDefaultView(kattaziele);
            BegleitTruppen = CollectionViewSource.GetDefaultView(Begleittruppen);
            Background = Model.Abschickzeitpunkt < DateTime.Now ? Color.Red : Color.Green;
            Model.Refresh();
        }

        public TruppenbewegungViewModel(DorfViewModel senderDorf, DorfViewModel empfaengerDorf, DateTime abschickzeitpunkt, TruppenTemplateViewModel template, List<TruppenTemplate> begleitTruppen, int playerid = 0) {
            _playerid = playerid;
            Model = new Truppenbewegung {Begleittruppen = new AsyncObservableCollection<TruppenTemplate>(begleitTruppen)};
            Active = "N";
            Model.SenderDorf = senderDorf;
            Model.EmpfaengerDorf = empfaengerDorf;
            Abschickzeitpunkt = abschickzeitpunkt.ToString("dd.MM.yyyy HH:mm:ss.fff", CultureInfo.InvariantCulture);
            Speertraeger = template.SpeertraegerAnzahl.ToString();
            Schwertkaempfer = template.SchwertkaempferAnzahl.ToString();
            Axtkaempfer = template.AxtkaempferAnzahl.ToString();
            Bogenschuetzen = template.BogenschuetzenAnzahl.ToString();
            Spaeher = template.SpaeherAnzahl.ToString();
            LeichteKavallerie = template.LeichteKavallerieAnzahl.ToString();
            BeritteneBogenschuetzen = template.BeritteneBogenschuetzenAnzahl.ToString();
            SchwereKavallerie = template.SchwereKavallerieAnzahl.ToString();
            Rammboecke = template.RammboeckeAnzahl.ToString();
            Katapulte = template.KatapulteAnzahl.ToString();
            Paladin = template.PaladinAnzahl.ToString();
            Adelsgeschlechter = template.AdelsgeschlechterAnzahl.ToString();
            Angriffsinformationen = "";
            var bewegungsartenList = new AsyncObservableCollection<string>(new List<string> {Resources.Support, Resources.Attack});
            Bewegungsarten = CollectionViewSource.GetDefaultView(bewegungsartenList);
            BegleitTruppen = CollectionViewSource.GetDefaultView(Begleittruppen);
            Background = Model.Abschickzeitpunkt < DateTime.Now ? Color.Red : Color.Green;
            var kattaziele = new AsyncObservableCollection<string>();
            foreach (var xx in Enum.GetNames(typeof(Gebaeude)))
                kattaziele.Add(xx);
            KattaZiele = CollectionViewSource.GetDefaultView(kattaziele);
            Model.Refresh();
        }

        public TruppenbewegungViewModel(DorfViewModel senderDorf, DorfViewModel empfaengerDorf, DateTime ankunftszeitpunkt, TruppenTemplateViewModel template, int playerid = 0) {
            _playerid = playerid;
            Model = new Truppenbewegung {Begleittruppen = new AsyncObservableCollection<TruppenTemplate>()};
            Active = "N";
            Model.SenderDorf = senderDorf;
            Model.EmpfaengerDorf = empfaengerDorf;
            Speertraeger = template.SpeertraegerAnzahl.ToString();
            Schwertkaempfer = template.SchwertkaempferAnzahl.ToString();
            Axtkaempfer = template.AxtkaempferAnzahl.ToString();
            Bogenschuetzen = template.BogenschuetzenAnzahl.ToString();
            Spaeher = template.SpaeherAnzahl.ToString();
            LeichteKavallerie = template.LeichteKavallerieAnzahl.ToString();
            BeritteneBogenschuetzen = template.BeritteneBogenschuetzenAnzahl.ToString();
            SchwereKavallerie = template.SchwereKavallerieAnzahl.ToString();
            Rammboecke = template.RammboeckeAnzahl.ToString();
            Katapulte = template.KatapulteAnzahl.ToString();
            Paladin = template.PaladinAnzahl.ToString();
            Adelsgeschlechter = template.AdelsgeschlechterAnzahl.ToString();
            Ankunftszeitpunkt = ankunftszeitpunkt.ToString("dd.MM.yyyy HH:mm:ss.fff", CultureInfo.InvariantCulture);
            Angriffsinformationen = "";
            var bewegungsartenList = new AsyncObservableCollection<string>(new List<string> {Resources.Support, Resources.Attack});
            Bewegungsarten = CollectionViewSource.GetDefaultView(bewegungsartenList);
            BegleitTruppen = CollectionViewSource.GetDefaultView(Begleittruppen);
            Background = Model.Abschickzeitpunkt < DateTime.Now ? Color.Red : Color.Green;
            var kattaziele = new AsyncObservableCollection<string>();
            foreach (var xx in Enum.GetNames(typeof(Gebaeude)))
                kattaziele.Add(xx);
            KattaZiele = CollectionViewSource.GetDefaultView(kattaziele);
            Model.Refresh();
        }

        public TruppenbewegungViewModel(DorfViewModel senderDorf, DorfViewModel empfaengerDorf, DateTime abschickzeitpunkt, int playerid = 0) {
            _playerid = playerid;
            Model = new Truppenbewegung {Begleittruppen = new AsyncObservableCollection<TruppenTemplate>()};
            Active = "N";
            Model.SenderDorf = senderDorf;
            Model.EmpfaengerDorf = empfaengerDorf;
            Speertraeger = "0";
            Schwertkaempfer = "0";
            Axtkaempfer = "0";
            Bogenschuetzen = "0";
            Spaeher = "0";
            LeichteKavallerie = "0";
            BeritteneBogenschuetzen = "0";
            SchwereKavallerie = "0";
            Rammboecke = "0";
            Katapulte = "0";
            Paladin = "0";
            Adelsgeschlechter = "0";
            Abschickzeitpunkt = abschickzeitpunkt.ToString("dd.MM.yyyy HH:mm:ss.fff", CultureInfo.InvariantCulture);
            Angriffsinformationen = "";
            var bewegungsartenList = new AsyncObservableCollection<string>(new List<string> {Resources.Support, Resources.Attack});
            Bewegungsarten = CollectionViewSource.GetDefaultView(bewegungsartenList);
            BegleitTruppen = CollectionViewSource.GetDefaultView(Begleittruppen);
            Background = Model.Abschickzeitpunkt < DateTime.Now ? Color.Red : Color.Green;
            var kattaziele = new AsyncObservableCollection<string>();
            foreach (var xx in Enum.GetNames(typeof(Gebaeude)))
                kattaziele.Add(xx);
            KattaZiele = CollectionViewSource.GetDefaultView(kattaziele);
            Model.Refresh();
        }

        public TruppenbewegungViewModel(Truppenbewegung model, int playerid = 0) {
            _playerid = playerid;
            Model = model;
            if (Model.Queued) {
                Model.Queued = false;
                Model.Active = true;
            }
            var bewegungsartenList = new AsyncObservableCollection<string>(new List<string> {Resources.Support, Resources.Attack});
            Bewegungsarten = CollectionViewSource.GetDefaultView(bewegungsartenList);
            OnPropertyChanged(nameof(BewegungsartSelected));
            BegleitTruppen = CollectionViewSource.GetDefaultView(Begleittruppen);
            Background = Model.Abschickzeitpunkt < DateTime.Now ? Color.Red : Color.Green;
            var kattaziele = new AsyncObservableCollection<string>();
            foreach (var xx in Enum.GetNames(typeof(Gebaeude)))
                kattaziele.Add(xx);
            KattaZiele = CollectionViewSource.GetDefaultView(kattaziele);
            Model.Refresh();
        }

        public TruppenbewegungViewModel(int playerid = 0) {
            _playerid = playerid;
            var bewegungsartenList = new AsyncObservableCollection<string>(new List<string> {Resources.Support, Resources.Attack});
            Bewegungsarten = CollectionViewSource.GetDefaultView(bewegungsartenList);
            OnPropertyChanged(nameof(BewegungsartSelected));
            BegleitTruppen = CollectionViewSource.GetDefaultView(Begleittruppen);
            Background = Model.Abschickzeitpunkt < DateTime.Now ? Color.Red : Color.Green;
            var kattaziele = new AsyncObservableCollection<string>();
            foreach (var xx in Enum.GetNames(typeof(Gebaeude)))
                kattaziele.Add(xx);
            KattaZiele = CollectionViewSource.GetDefaultView(kattaziele);
            Model.Refresh();
        }

        [Obfuscation(Exclude = true, Feature = "renaming")]
        private AsyncObservableCollection<TruppenTemplate> Begleittruppen => Model.Begleittruppen ?? (Model.Begleittruppen = new AsyncObservableCollection<TruppenTemplate>());

        public bool IsSelected {
            get => _isSelected;
            set {
                _isSelected = value;
                OnPropertyChanged();
            }
        }

        public int ProzentAbziehenVonLaufzeit {
            get => Model.ProzentAbziehenVonLaufzeit;
            set {
                Model.ProzentAbziehenVonLaufzeit = value;
                UpdateZeiten("Ankunftszeitpunkt");
                OnPropertyChanged("");
            }
        }

        public string Fehlermeldungen {
            get => Model.Fehlermeldungen;
            set {
                if (string.IsNullOrEmpty(Model.Fehlermeldungen))
                    Model.Fehlermeldungen = "";
                Model.Fehlermeldungen += value + "\n";
                OnPropertyChanged("");
            }
        }

        public string Notizen {
            get => Model.Notizen;
            set {
                if (!Model.Sent) {
                    Model.Notizen = value;
                    OnPropertyChanged();
                }
            }
        }

        public string Angriffsinformationen {
            get => Model.Angriffsinformationen;
            set {
                if (!Model.Sent) {
                    Model.Angriffsinformationen = value;
                    OnPropertyChanged();
                }
            }
        }

        public ICollectionView Bewegungsarten {
            get => _bewegungsartenView;
            set {
                _bewegungsartenView = value;
                OnPropertyChanged();
            }
        }

        public ICollectionView KattaZiele {
            get => _kattaZieleView;
            set {
                _kattaZieleView = value;
                OnPropertyChanged();
            }
        }

        public string SelectedKattaziel {
            get => Model.KattaZiel;
            set {
                Model.KattaZiel = value;
                OnPropertyChanged();
            }
        }

        public bool GenauTimenIsActive {
            get => Model.GenauTimenIsActive;
            set {
                Model.GenauTimenIsActive = value;
                OnPropertyChanged("");
            }
        }

        public bool Rausstellen {
            get => Model.Rausstellen;
            set {
                Model.Rausstellen = value;
                if (!value)
                    Active = "N";
                OnPropertyChanged();
            }
        }

        public bool IsDeff => (Model.SpeertraegerAnzahl > 0 || Model.SchwertkaempferAnzahl > 0 || Model.BogenschuetzenAnzahl > 0 || Model.SpaeherAnzahl > 0 || Model.SchwereKavallerieAnzahl > 0) && !string.IsNullOrEmpty(Model.Angriffsinformationen) && Model.Angriffsinformationen.Contains("Deff") && Model.AdelsgeschlechterAnzahl == 0;

        public bool IsOff => (Model.AxtkaempferAnzahl > 0 || Model.SpaeherAnzahl > 0 || Model.LeichteKavallerieAnzahl > 0 || Model.RammboeckeAnzahl > 0 || Model.BeritteneBogenschuetzenAnzahl > 0 || Model.KatapulteAnzahl > 0 || Model.PaladinAnzahl > 0) && !string.IsNullOrEmpty(Model.Angriffsinformationen) && Model.Angriffsinformationen.Contains("Off") && Model.AdelsgeschlechterAnzahl == 0;

        public bool IsDeffWithAg => (Model.SpeertraegerAnzahl > 0 || Model.SchwertkaempferAnzahl > 0 || Model.BogenschuetzenAnzahl > 0 || Model.SpaeherAnzahl > 0 || Model.SchwereKavallerieAnzahl > 0) && !string.IsNullOrEmpty(Model.Angriffsinformationen) && Model.Angriffsinformationen.Contains("Deff") && Model.AdelsgeschlechterAnzahl > 0;

        public bool IsOffWithAg => (Model.AxtkaempferAnzahl > 0 || Model.SpaeherAnzahl > 0 || Model.LeichteKavallerieAnzahl > 0 || Model.RammboeckeAnzahl > 0 || Model.BeritteneBogenschuetzenAnzahl > 0 || Model.KatapulteAnzahl > 0 || Model.PaladinAnzahl > 0) && !string.IsNullOrEmpty(Model.Angriffsinformationen) && Model.Angriffsinformationen.Contains("Off") && Model.AdelsgeschlechterAnzahl > 0;

        public bool IsFake => !string.IsNullOrEmpty(Model.Angriffsinformationen) && Model.Angriffsinformationen.Contains("Fake");

        public bool IsDeffGenau => (Model.SpeertraegerAnzahl > 0 || Model.SchwertkaempferAnzahl > 0 || Model.BogenschuetzenAnzahl > 0 || Model.SpaeherAnzahl > 0 || Model.SchwereKavallerieAnzahl > 0) && !string.IsNullOrEmpty(Model.Angriffsinformationen) && Model.Angriffsinformationen.Contains("Deff") && Model.AdelsgeschlechterAnzahl == 0 && GenauTimenIsActive;

        public bool IsOffGenau => (Model.AxtkaempferAnzahl > 0 || Model.SpaeherAnzahl > 0 || Model.LeichteKavallerieAnzahl > 0 || Model.RammboeckeAnzahl > 0 || Model.BeritteneBogenschuetzenAnzahl > 0 || Model.KatapulteAnzahl > 0 || Model.PaladinAnzahl > 0) && !string.IsNullOrEmpty(Model.Angriffsinformationen) && Model.Angriffsinformationen.Contains("Off") && Model.AdelsgeschlechterAnzahl == 0 && GenauTimenIsActive;

        public bool IsDeffWithAgGenau => (Model.SpeertraegerAnzahl > 0 || Model.SchwertkaempferAnzahl > 0 || Model.BogenschuetzenAnzahl > 0 || Model.SpaeherAnzahl > 0 || Model.SchwereKavallerieAnzahl > 0) && !string.IsNullOrEmpty(Model.Angriffsinformationen) && Model.Angriffsinformationen.Contains("Deff") && Model.AdelsgeschlechterAnzahl > 0 && GenauTimenIsActive;

        public bool IsOffWithAgGenau => (Model.AxtkaempferAnzahl > 0 || Model.SpaeherAnzahl > 0 || Model.LeichteKavallerieAnzahl > 0 || Model.RammboeckeAnzahl > 0 || Model.BeritteneBogenschuetzenAnzahl > 0 || Model.KatapulteAnzahl > 0 || Model.PaladinAnzahl > 0) && !string.IsNullOrEmpty(Model.Angriffsinformationen) && Model.Angriffsinformationen.Contains("Off") && Model.AdelsgeschlechterAnzahl > 0 && GenauTimenIsActive;

        public bool IsFakeGenau => !string.IsNullOrEmpty(Model.Angriffsinformationen) && Model.Angriffsinformationen.Contains("Fake") && GenauTimenIsActive;


        public Truppenbewegung Model { get; set; }

        public ICollectionView BegleitTruppen {
            get => _begleittruppenView;
            set {
                _begleittruppenView = value;
                OnPropertyChanged();
            }
        }

        public string Active {
            get => Model.Active ? "J" : "N";
            set {
                Model.Active = value == "J";
                OnPropertyChanged("");
            }
        }


        public string Sent {
            get => Model.Sent ? "J" : "N";
            set {
                Model.Sent = value == "J";
                OnPropertyChanged("");
            }
        }


        public string Queued {
            get => Model.Queued ? "J" : "N";
            set {
                Model.Queued = value == "J";
                OnPropertyChanged("");
            }
        }

        public bool GetsQueued {
            get => Model.GetsQueued;
            set {
                Model.GetsQueued = value;
                OnPropertyChanged("");
            }
        }


        public bool Enabled => !Model.Queued;
        public bool BogenschuetzenEnabled => Enabled && App.Settings.Weltensettings.BogenschuetzenActive;
        public bool PaladinEnabled => Enabled && App.Settings.Weltensettings.PaladinActive;
        public bool KatapulteEnthalten => Model.KatapulteAnzahl > 0;

        public string SenderName => Model.SenderDorf.Name;
        public string SenderX => Model.SenderDorf.XCoordinate.ToString();
        public string SenderY => Model.SenderDorf.YCoordinate.ToString();
        public int SenderId => Model.SenderDorf.VillageId;
        public string EmpfaengerName => Model.EmpfaengerDorf.Name;
        public string EmpfaengerX => Model.EmpfaengerDorf.XCoordinate.ToString();
        public string EmpfaengerY => Model.EmpfaengerDorf.YCoordinate.ToString();
        public int EmpfaengerId => Model.EmpfaengerDorf.VillageId;

        public string Abschickzeitpunkt {
            get => Model.Abschickzeitpunkt.ToString("dd.MM.yyyy HH:mm:ss.fff", CultureInfo.InvariantCulture);
            set {
                try {
                    if (!Model.Sent) {
                        var date = value.Split('\n')[0];
                        Model.Abschickzeitpunkt = DateTime.ParseExact(date, "dd.MM.yyyy HH:mm:ss.fff", CultureInfo.InvariantCulture);
                        try {
                            var temp = value.Split('\n')[1];
                        } catch {
                            UpdateZeiten("Abschickzeitpunkt");
                        }
                        Active = "N";
                        LaufZeit = "";
                    }
                    OnPropertyChanged();
                } catch {
                    Abschickzeitpunkt = Model.Abschickzeitpunkt.ToString("dd.MM.yyyy HH:mm:ss.fff", CultureInfo.InvariantCulture);
                    OnPropertyChanged("");
                }
            }
        }

        public Color Background {
            get => _background;
            set {
                _background = value;
                OnPropertyChanged();
            }
        }

        public string Ankunftszeitpunkt {
            get => Model.Ankunftszeitpunkt.ToString("dd.MM.yyyy HH:mm:ss.fff", CultureInfo.InvariantCulture);
            set {
                try {
                    if (!Model.Sent) {
                        var date = value.Split('\n')[0];
                        Model.Ankunftszeitpunkt = DateTime.ParseExact(date, "dd.MM.yyyy HH:mm:ss.fff", CultureInfo.InvariantCulture);
                        try {
                            var temp = value.Split('\n')[1];
                        } catch {
                            UpdateZeiten("Ankunftszeitpunkt");
                        }
                        Active = "N";
                        LaufZeit = "";
                    }
                    OnPropertyChanged();
                } catch {
                    Ankunftszeitpunkt = Model.Ankunftszeitpunkt.ToString("dd.MM.yyyy HH:mm:ss.fff", CultureInfo.InvariantCulture);
                    OnPropertyChanged("");
                }
            }
        }

        public string ActualAnkunftszeitpunkt {
            get => Model.ActualAnkunftszeitpunkt.ToString("dd.MM.yyyy HH:mm:ss.fff", CultureInfo.InvariantCulture);
            set {
                if (Model.Sent) {
                    var date = value.Split('\n')[0];
                    Model.ActualAnkunftszeitpunkt = DateTime.ParseExact(date, "dd.MM.yyyy HH:mm:ss.fff", CultureInfo.InvariantCulture);
                }
                OnPropertyChanged("");
            }
        }

        public string LaufZeit {
            get {
                var ts = Model.Ankunftszeitpunkt - Model.Abschickzeitpunkt;
                return ts.ToString();
            }
            // ReSharper disable once ValueParameterNotUsed
            set => OnPropertyChanged();
        }

        public string Rueckkehrzeitpunkt {
            get => Model.Rueckkehrzeitpunkt.ToString("dd.MM.yyyy HH:mm:ss.fff", CultureInfo.InvariantCulture);
            set {
                try {
                    if (!Model.Sent) {
                        var date = value.Split('\n')[0];
                        Model.Rueckkehrzeitpunkt = DateTime.ParseExact(date, "dd.MM.yyyy HH:mm:ss.fff", CultureInfo.InvariantCulture);
                        var temp = Model.Rueckkehrzeitpunkt.Millisecond;
                        Model.Rueckkehrzeitpunkt = Model.Rueckkehrzeitpunkt.AddMilliseconds(-temp);
                        try {
                            var temp2 = value.Split('\n')[1];
                        } catch {
                            UpdateZeiten("Rueckkehrzeitpunkt");
                        }
                        Active = "N";
                        LaufZeit = "";
                    }
                    OnPropertyChanged();
                } catch {
                    Rueckkehrzeitpunkt = Model.Rueckkehrzeitpunkt.ToString("dd.MM.yyyy HH:mm:ss.fff", CultureInfo.InvariantCulture);
                    OnPropertyChanged();
                }
            }
        }


        public string Speertraeger {
            get => Model.SpeertraegerAnzahl.ToString();
            set {
                if (!Model.Sent) {
                    int.TryParse(value, out var temp);
                    Model.SpeertraegerAnzahl = temp;
                    Active = "N";
                    UpdateZeiten("Abschickzeitpunkt");
                }
                RefreshParent();
                OnPropertyChanged("");
            }
        }

        public string Schwertkaempfer {
            get => Model.SchwertkaempferAnzahl.ToString();
            set {
                if (!Model.Sent) {
                    int.TryParse(value, out var temp);
                    Model.SchwertkaempferAnzahl = temp;
                    Active = "N";
                    UpdateZeiten("Abschickzeitpunkt");
                }
                RefreshParent();
                OnPropertyChanged("");
            }
        }

        public string Axtkaempfer {
            get => Model.AxtkaempferAnzahl.ToString();
            set {
                if (!Model.Sent) {
                    int.TryParse(value, out var temp);
                    Model.AxtkaempferAnzahl = temp;
                    Active = "N";
                    UpdateZeiten("Abschickzeitpunkt");
                }
                RefreshParent();
                OnPropertyChanged("");
            }
        }

        public string Bogenschuetzen {
            get => Model.BogenschuetzenAnzahl.ToString();
            set {
                if (!Model.Sent && App.Settings.Weltensettings.BogenschuetzenActive) {
                    int.TryParse(value, out var temp);
                    Model.BogenschuetzenAnzahl = temp;
                    Active = "N";
                    UpdateZeiten("Abschickzeitpunkt");
                }
                RefreshParent();
                OnPropertyChanged("");
            }
        }

        public string Spaeher {
            get => Model.SpaeherAnzahl.ToString();
            set {
                if (!Model.Sent) {
                    int.TryParse(value, out var temp);
                    Model.SpaeherAnzahl = temp;
                    Active = "N";
                    UpdateZeiten("Abschickzeitpunkt");
                }
                RefreshParent();
                OnPropertyChanged("");
            }
        }

        public string LeichteKavallerie {
            get => Model.LeichteKavallerieAnzahl.ToString();
            set {
                if (!Model.Sent) {
                    int.TryParse(value, out var temp);
                    Model.LeichteKavallerieAnzahl = temp;
                    Active = "N";
                    UpdateZeiten("Abschickzeitpunkt");
                }
                RefreshParent();
                OnPropertyChanged("");
            }
        }

        public string BeritteneBogenschuetzen {
            get => Model.BeritteneBogenschuetzenAnzahl.ToString();
            set {
                if (!Model.Sent && App.Settings.Weltensettings.BogenschuetzenActive) {
                    int.TryParse(value, out var temp);
                    Model.BeritteneBogenschuetzenAnzahl = temp;
                    Active = "N";
                    UpdateZeiten("Abschickzeitpunkt");
                }
                RefreshParent();
                OnPropertyChanged("");
            }
        }

        public string SchwereKavallerie {
            get => Model.SchwereKavallerieAnzahl.ToString();
            set {
                if (!Model.Sent) {
                    int.TryParse(value, out var temp);
                    Model.SchwereKavallerieAnzahl = temp;
                    Active = "N";
                    UpdateZeiten("Abschickzeitpunkt");
                }
                RefreshParent();
                OnPropertyChanged("");
            }
        }

        public string Rammboecke {
            get => Model.RammboeckeAnzahl.ToString();
            set {
                if (!Model.Sent) {
                    int.TryParse(value, out var temp);
                    Model.RammboeckeAnzahl = temp;
                    Active = "N";
                    UpdateZeiten("Abschickzeitpunkt");
                }
                RefreshParent();
                OnPropertyChanged("");
            }
        }

        public string Katapulte {
            get => Model.KatapulteAnzahl.ToString();
            set {
                if (!Model.Sent) {
                    int.TryParse(value, out var temp);
                    Model.KatapulteAnzahl = temp;
                    Active = "N";
                    UpdateZeiten("Abschickzeitpunkt");
                }
                RefreshParent();
                OnPropertyChanged("");
            }
        }

        public string Paladin {
            get => Model.PaladinAnzahl.ToString();
            set {
                if (!Model.Sent && App.Settings.Weltensettings.PaladinActive) {
                    int.TryParse(value, out var temp);
                    Model.PaladinAnzahl = temp;
                    Active = "N";
                    UpdateZeiten("Abschickzeitpunkt");
                }
                RefreshParent();
                OnPropertyChanged("");
            }
        }

        public string Adelsgeschlechter {
            get => Model.AdelsgeschlechterAnzahl.ToString();
            set {
                if (!Model.Sent) {
                    int.TryParse(value, out var temp);
                    Model.AdelsgeschlechterAnzahl = temp;
                    if (temp > 0)
                        Angriffsinformationen = "Ag";
                    Active = "N";
                    UpdateZeiten("Abschickzeitpunkt");
                }
                RefreshParent();
                OnPropertyChanged("");
            }
        }

        public string BewegungsartSelected {
            get => Model.Bewegungsart;
            set {
                if (!Model.Sent) {
                    Model.Bewegungsart = value;
                    UpdateZeiten("Abschickzeitpunkt");
                }
                OnPropertyChanged("");
            }
        }

        public ICommand ChangeStatusCommand => _changeStatusCommand ?? (_changeStatusCommand = new RelayCommand(SetActiveStatus));

        public ICommand OpenTemplateViewCommand => _openTemplateViewCommand ?? (_openTemplateViewCommand = new RelayCommand(OpenTemplateView));

        public ICommand OpenTemplateViewForAgAngriffCommand => _openTemplateForAgAngriffViewCommand ?? (_openTemplateForAgAngriffViewCommand = new RelayCommand(OpenTemplateViewForAgAngriff));

        public ICommand AgAngriffHinzufuegenCommand => _agAngriffHinzufuegenCommand ?? (_agAngriffHinzufuegenCommand = new RelayCommand(AddAgAngriff, () => Model.AdelsgeschlechterAnzahl > 0));

        public ICommand AgAngriffEntfernenCommand => _agAngriffEntfernenCommand ?? (_agAngriffEntfernenCommand = new RelayCommand(DeleteAgAngriff, () => Begleittruppen.Any()));

        public ICommand GetLinkCommand => _getLinkCommand ?? (_getLinkCommand = new RelayCommand(GetLink, () => Model.SenderDorf != null));

        public event PropertyChangedEventHandler PropertyChanged;

        private double GetMaxFeldLaufzeit() {
            if (!string.IsNullOrEmpty(Model.Bewegungsart) && Model.Bewegungsart.Equals(Resources.Support) && Model.PaladinAnzahl > 0)
                return App.Settings.Weltensettings.PaladinLaufzeit;
            if (Model.AdelsgeschlechterAnzahl > 0)
                return App.Settings.Weltensettings.AdelsgeschlechtLaufzeit;
            if (Model.RammboeckeAnzahl > 0 || Model.KatapulteAnzahl > 0)
                return App.Settings.Weltensettings.RammboeckeLaufzeit;
            if (Model.SchwertkaempferAnzahl > 0)
                return App.Settings.Weltensettings.SchwertkaempferLaufzeit;
            if (Model.SpeertraegerAnzahl > 0 || Model.AxtkaempferAnzahl > 0 || Model.BogenschuetzenAnzahl > 0)
                return App.Settings.Weltensettings.SpeertraegerLaufzeit;
            if (Model.SchwereKavallerieAnzahl > 0)
                return App.Settings.Weltensettings.SchwereKavallerieLaufzeit;
            if (Model.LeichteKavallerieAnzahl > 0 || Model.BeritteneBogenschuetzenAnzahl > 0)
                return App.Settings.Weltensettings.LeichteKavallerieLaufzeit;
            if (Model.SpaeherAnzahl > 0 || Model.PaladinAnzahl > 0)
                return App.Settings.Weltensettings.SpaeherLaufzeit;
            return 0;
        }


        private void AddAgAngriff() {
            if (!Model.Sent)
                Begleittruppen.Add(new TruppenTemplate());
        }

        private void DeleteAgAngriff() {
            if (!Model.Sent)
                Begleittruppen.Remove((TruppenTemplate) BegleitTruppen.CurrentItem);
        }


        private void UpdateZeiten(string zeitupdated) {
            var tempx = (Model.SenderDorf.XCoordinate - Model.EmpfaengerDorf.XCoordinate) * (Model.SenderDorf.XCoordinate - Model.EmpfaengerDorf.XCoordinate);
            var tempy = (Model.SenderDorf.YCoordinate - Model.EmpfaengerDorf.YCoordinate) * (Model.SenderDorf.YCoordinate - Model.EmpfaengerDorf.YCoordinate);
            var entfernung = Math.Sqrt(tempx + tempy);
            var temxp = 1 - ((double) ProzentAbziehenVonLaufzeit > 0 ? (double) ProzentAbziehenVonLaufzeit : 100) / 100;
            var temmmmp = temxp > 0 ? temxp : 1;
            var minutes = entfernung * (GetMaxFeldLaufzeit() * temmmmp);
            var hours = (int) minutes / 60;
            minutes = minutes - hours * 60;
            var minutestotal = (int) minutes;
            minutes = minutes - minutestotal;
            var seconds = (int) Math.Round(minutes * 60, 0, MidpointRounding.ToEven);
            // ReSharper disable once PossibleLossOfFraction
            minutes = minutes - seconds / 60;
            var millis = (int) minutes * 1000;
            var ts = new TimeSpan(0, hours, minutestotal, seconds, millis);

            switch (zeitupdated) {
                case "Abschickzeitpunkt":
                    Ankunftszeitpunkt = Model.Abschickzeitpunkt.AddMilliseconds(ts.TotalMilliseconds).ToString("dd.MM.yyyy HH:mm:ss.fff", CultureInfo.InvariantCulture) + "\n1";
                    var temp = Model.Ankunftszeitpunkt.AddMilliseconds(ts.TotalMilliseconds).Millisecond;
                    Rueckkehrzeitpunkt = Model.Ankunftszeitpunkt.AddMilliseconds(ts.TotalMilliseconds - temp).ToString("dd.MM.yyyy HH:mm:ss.fff", CultureInfo.InvariantCulture) + "\n1";
                    break;
                case "Ankunftszeitpunkt":
                    Abschickzeitpunkt = Model.Ankunftszeitpunkt.AddMilliseconds(-ts.TotalMilliseconds).ToString("dd.MM.yyyy HH:mm:ss.fff", CultureInfo.InvariantCulture) + "\n1";
                    var temp2 = Model.Ankunftszeitpunkt.AddMilliseconds(ts.TotalMilliseconds).Millisecond;
                    Rueckkehrzeitpunkt = Model.Ankunftszeitpunkt.AddMilliseconds(ts.TotalMilliseconds).AddMilliseconds(-temp2).ToString("dd.MM.yyyy HH:mm:ss.fff", CultureInfo.InvariantCulture) + "\n1";
                    break;
                case "Rückkehrzeitpunkt":
                    Abschickzeitpunkt = Model.Rueckkehrzeitpunkt.AddMilliseconds(-ts.TotalMilliseconds * 2).ToString("dd.MM.yyyy HH:mm:ss.fff", CultureInfo.InvariantCulture) + "\n1";
                    Ankunftszeitpunkt = Model.Rueckkehrzeitpunkt.AddMilliseconds(-ts.TotalMilliseconds).ToString("dd.MM.yyyy HH:mm:ss.fff", CultureInfo.InvariantCulture) + "\n1";
                    break;
            }
            Background = Model.Abschickzeitpunkt < DateTime.Now ? Color.Red : Color.Green;
        }

        private bool AllesInOrdnung() {
            if (BewegungsartSelected != null && (GetMaxFeldLaufzeit() > 0 || Rausstellen) && Model.Abschickzeitpunkt.AddSeconds(30) > DateTime.Now || !Model.Sent && Model.AdelsgeschlechterAnzahl != 0 && Model.Begleittruppen.Any(x => x.AdelsgeschlechterAnzahl != 0))
                return true;
            return false;
        }

        public void SetActiveStatus() {
            if (AllesInOrdnung()) {
                Active = Active == "J" ? "N" : "J";
                Queued = "N";
            }
            if (Model.Sent) {
                var result = MessageBox.Show(Resources.ReviveAttack, Resources.Achtung, MessageBoxButton.YesNo);
                if (result == MessageBoxResult.Yes) {
                    Sent = "N";
                    Queued = "N";
                    Active = "N";
                }
            }
        }

        public bool SetToFalse(bool yes = false) {
            Active = "N";
            Queued = "N";
            if (Model.Sent) {
                if (yes) {
                    Sent = "N";
                    Queued = "N";
                    return true;
                }
                var result = MessageBox.Show(Resources.ReviveAttack, Resources.Achtung, MessageBoxButton.YesNo);
                if (result == MessageBoxResult.Yes) {
                    Sent = "N";
                    Queued = "N";
                    return true;
                }
            }
            return yes;
        }

        public bool SetToTrue(bool yes = false) {
            Active = "J";
            Queued = "N";
            if (Model.Sent) {
                if (yes) {
                    Sent = "N";
                    Queued = "N";
                    return true;
                }
                var result = MessageBox.Show(Resources.ReviveAttack, Resources.Achtung, MessageBoxButton.YesNo);
                if (result == MessageBoxResult.Yes) {
                    Sent = "N";
                    Queued = "N";
                    return true;
                }
            }
            return yes;
        }

        private void GetLink() {
            if (Model.SenderDorf != null) {
                var spieler = App.Settings.Accountsettingslist.FirstOrDefault(x => x.MultiaccountingDaten.Accountmode.Equals(Accountmode.Mainaccount));
                var url = App.Settings.OperateLink + "village=" + Model.SenderDorf.VillageId + "" + (_playerid != 0 ? _playerid.ToString() : "") + (string.IsNullOrEmpty(spieler?.Uvname) ? "" : "&t=" + App.PlayerlistWholeWorld.FirstOrDefault(x => x.PlayerName.Equals(spieler.Uvname))?.PlayerId) + "&screen=place";
                Clipboard.SetText(url);
            }
        }

        private void OpenTemplateView() {
            //var view = new TemplateView();
            //view.ShowDialog();
            //TemplateViewModel viewmodel = (TemplateViewModel)view.DataContext;
            //if (viewmodel.TemplateToUse != null)
            //{
            //    Speertraeger = viewmodel.TemplateToUse.SpeertraegerAnzahl;
            //    Schwertkaempfer = viewmodel.TemplateToUse.SchwertkaempferAnzahl;
            //    Axtkaempfer = viewmodel.TemplateToUse.AxtkaempferAnzahl;
            //    Bogenschuetzen = viewmodel.TemplateToUse.BogenschuetzenAnzahl;
            //    Spaeher = viewmodel.TemplateToUse.SpaeherAnzahl;
            //    LeichteKavallerie = viewmodel.TemplateToUse.LeichteKavallerieAnzahl;
            //    BeritteneBogenschuetzen = viewmodel.TemplateToUse.BeritteneBogenschuetzenAnzahl;
            //    SchwereKavallerie = viewmodel.TemplateToUse.SchwereKavallerieAnzahl;
            //    Rammboecke = viewmodel.TemplateToUse.RammboeckeAnzahl;
            //    Katapulte = viewmodel.TemplateToUse.KatapulteAnzahl;
            //    Paladin = viewmodel.TemplateToUse.PaladinAnzahl;
            //    Adelsgeschlechter = viewmodel.TemplateToUse.AdelsgeschlechterAnzahl;
            //}
        }

        private void OpenTemplateViewForAgAngriff() {
            //if (BegleitTruppen.CurrentItem != null)
            //{
            //    var view = new TemplateView();
            //    view.ShowDialog();
            //    TemplateViewModel viewmodel = (TemplateViewModel)view.DataContext;
            //    if (viewmodel.TemplateToUse != null)
            //    {
            //        TruppenTemplate tempTemplate = (TruppenTemplate)BegleitTruppen.CurrentItem;
            //        tempTemplate.SpeertraegerAnzahl = viewmodel.TemplateToUse.TruppenTemplate.SpeertraegerAnzahl;
            //        tempTemplate.SchwertkaempferAnzahl = viewmodel.TemplateToUse.TruppenTemplate.SchwertkaempferAnzahl;
            //        tempTemplate.AxtkaempferAnzahl = viewmodel.TemplateToUse.TruppenTemplate.AxtkaempferAnzahl;
            //        tempTemplate.BogenschuetzenAnzahl = viewmodel.TemplateToUse.TruppenTemplate.BogenschuetzenAnzahl;
            //        tempTemplate.SpaeherAnzahl = viewmodel.TemplateToUse.TruppenTemplate.SpaeherAnzahl;
            //        tempTemplate.LeichteKavallerieAnzahl =
            //            viewmodel.TemplateToUse.TruppenTemplate.LeichteKavallerieAnzahl;
            //        tempTemplate.BeritteneBogenschuetzenAnzahl =
            //            viewmodel.TemplateToUse.TruppenTemplate.BeritteneBogenschuetzenAnzahl;
            //        tempTemplate.SchwereKavallerieAnzahl =
            //            viewmodel.TemplateToUse.TruppenTemplate.SchwereKavallerieAnzahl;
            //        tempTemplate.RammboeckeAnzahl = viewmodel.TemplateToUse.TruppenTemplate.RammboeckeAnzahl;
            //        tempTemplate.KatapulteAnzahl = viewmodel.TemplateToUse.TruppenTemplate.KatapulteAnzahl;
            //        tempTemplate.PaladinAnzahl = viewmodel.TemplateToUse.TruppenTemplate.PaladinAnzahl;
            //        tempTemplate.AdelsgeschlechterAnzahl =
            //            viewmodel.TemplateToUse.TruppenTemplate.AdelsgeschlechterAnzahl;
            //        tempTemplate.TemplateName = viewmodel.TemplateToUse.TruppenTemplate.TemplateName;
            //    }
            //}
        }

        public void Refresh() {
            OnPropertyChanged("");
        }

        public void RefreshParent() {
            Vm?.Refresh();
        }

        public TruppenbewegungViewModel Clone(string notiz) {
            var newtruppenbewegung = new TruppenbewegungViewModel(new Truppenbewegung {Abschickzeitpunkt = Model.Abschickzeitpunkt, Active = Model.Active, AdelsgeschlechterAnzahl = Model.AdelsgeschlechterAnzahl, AnkunftszeitAuswirkungenAufList = Model.AnkunftszeitAuswirkungenAufList, Ankunftszeitpunkt = Model.Ankunftszeitpunkt, AxtkaempferAnzahl = Model.AxtkaempferAnzahl, Begleittruppen = Model.Begleittruppen, BeritteneBogenschuetzenAnzahl = Model.BeritteneBogenschuetzenAnzahl, BewegungId = Model.BewegungId + 1, Bewegungsart = Model.Bewegungsart, Queued = Model.Queued, SenderDorf = Model.SenderDorf, SpeertraegerAnzahl = Model.SpeertraegerAnzahl, KatapulteAnzahl = Model.KatapulteAnzahl, LeichteKavallerieAnzahl = Model.LeichteKavallerieAnzahl, SpaeherAnzahl = Model.SpaeherAnzahl, BogenschuetzenAnzahl = Model.BogenschuetzenAnzahl, SchwereKavallerieAnzahl = Model.SchwereKavallerieAnzahl, SchwertkaempferAnzahl = Model.SchwertkaempferAnzahl, PaladinAnzahl = Model.PaladinAnzahl, RammboeckeAnzahl = Model.RammboeckeAnzahl, EmpfaengerDorf = Model.EmpfaengerDorf, Rueckkehrzeitpunkt = Model.Rueckkehrzeitpunkt, Sent = Model.Sent, Angriffsinformationen = Angriffsinformationen, Fehlermeldungen = Fehlermeldungen, GenauTimenIsActive = GenauTimenIsActive, Notizen = Notizen, Rausstellen = Rausstellen}) {Active = "N", Abschickzeitpunkt = Abschickzeitpunkt, Speertraeger = Speertraeger, Schwertkaempfer = Schwertkaempfer, Axtkaempfer = Axtkaempfer, Bogenschuetzen = Bogenschuetzen, Spaeher = Spaeher, LeichteKavallerie = LeichteKavallerie, BeritteneBogenschuetzen = BeritteneBogenschuetzen, SchwereKavallerie = SchwereKavallerie, Rammboecke = Rammboecke, Katapulte = Katapulte, Paladin = Paladin, Adelsgeschlechter = Adelsgeschlechter, Notizen = notiz};
            foreach (var begleittruppe in Begleittruppen) {
                var newtemplate = new TruppenTemplate {SpeertraegerAnzahl = begleittruppe.SpeertraegerAnzahl, SchwertkaempferAnzahl = begleittruppe.SchwertkaempferAnzahl, AxtkaempferAnzahl = begleittruppe.AxtkaempferAnzahl, BogenschuetzenAnzahl = begleittruppe.BogenschuetzenAnzahl, SpaeherAnzahl = begleittruppe.SpaeherAnzahl, LeichteKavallerieAnzahl = begleittruppe.LeichteKavallerieAnzahl, BeritteneBogenschuetzenAnzahl = begleittruppe.BeritteneBogenschuetzenAnzahl, SchwereKavallerieAnzahl = begleittruppe.SchwereKavallerieAnzahl, RammboeckeAnzahl = begleittruppe.RammboeckeAnzahl, KatapulteAnzahl = begleittruppe.KatapulteAnzahl, PaladinAnzahl = begleittruppe.PaladinAnzahl, AdelsgeschlechterAnzahl = begleittruppe.AdelsgeschlechterAnzahl, TemplateName = begleittruppe.TemplateName};
                newtruppenbewegung.Model.Begleittruppen.Add(newtemplate);
            }
            return newtruppenbewegung;
        }

        [NotifyPropertyChangedInvocator]
        private void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
