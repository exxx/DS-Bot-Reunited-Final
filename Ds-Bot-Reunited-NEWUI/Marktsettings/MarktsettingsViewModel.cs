﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Input;
using Ds_Bot_Reunited_NEWUI.Annotations;
using Ds_Bot_Reunited_NEWUI.Constants;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.Properties;
using Ds_Bot_Reunited_NEWUI.Spieldaten;

namespace Ds_Bot_Reunited_NEWUI.Marktsettings {
	public class MarktsettingsViewModel {
		public System.Windows.Window Window { get; set; }
		private ICommand _copyCommand;
		private MarktsettingsDorfViewModel _selectedTemplateVillage;
		private MarktsettingsDorfViewModel _selectedVillage;
		private ICollectionView _villages;
		private GruppeDoerfer _selectedGroup;
		
		public AsyncObservableCollection<MarktsettingsDorfViewModel> SelectedVillages { get; set; }

		private AsyncObservableCollection<MarktsettingsDorfViewModel> Marktsettings { get; }
		public List<GruppeDoerfer> Groups => App.Settings.Gruppen;
		public object MarktsettingsLock { get; set; } = new object();

		public ICollectionView Villages {
			get => _villages;
			set {
				_villages = value;
				OnPropertyChanged();
			}
		}

		public MarktsettingsDorfViewModel SelectedVillage {
			get => _selectedVillage;
			set {
				if (value == null) return;
				_selectedVillage = value;
				OnPropertyChanged();
			}
		}

		public GruppeDoerfer SelectedGroup {
			get => _selectedGroup;
			set {
				_selectedGroup = value;
				Task.Run(async () => { await LoadPlayerVillages(); });
				OnPropertyChanged("");
			}
		}

		public MarktsettingsDorfViewModel SelectedTemplateVillage {
			get => _selectedTemplateVillage;
			set {
				_selectedTemplateVillage = value;
				OnPropertyChanged();
			}
		}

		private async Task LoadPlayerVillages() {
			await Task.Run(() => {
				               try {
					               Marktsettings.Clear();
					               foreach (var liste in
						               App.Settings.Accountsettingslist.Select(x =>
							                                                       x.DoerferSettings.Where(y =>
								                                                                               y
									                                                                               .Dorf
									                                                                               .PlayerId
									                                                                               .Equals(x.Playerid) &&
								                                                                               (SelectedGroup
								                                                                                ?
								                                                                                .DoerferIds
								                                                                                .Any(k =>
									                                                                                     k
										                                                                                     .Equals(y.Dorf
										                                                                                              .VillageId)) ??
								                                                                                true)))
					               ) {
						               foreach (var dorf in liste.OrderBy(x => x.Dorf.Name)) {
							               Marktsettings.Add(new MarktsettingsDorfViewModel(dorf, Window));
						               }
					               }
				               } catch {
					               App.LogString(Resources.FailureLoadingPlayervillages);
				               }

				               if (Marktsettings.Any()) SelectedVillage = Marktsettings.FirstOrDefault();
				               OnPropertyChanged("");
			               });
		}

		public ICommand CopyCommand => _copyCommand ?? (_copyCommand = new RelayCommand(Copy));

		public MarktsettingsViewModel(System.Windows.Window window) {
			Marktsettings = new AsyncObservableCollection<MarktsettingsDorfViewModel>();
			Villages = CollectionViewSource.GetDefaultView(Marktsettings);
			BindingOperations.EnableCollectionSynchronization(Marktsettings, MarktsettingsLock);
			SelectedVillages = new AsyncObservableCollection<MarktsettingsDorfViewModel>();
			SelectedGroup =
				Groups.FirstOrDefault(x => x.Gruppenname.Equals(NAMECONSTANTS.GetNameForServer(Names.Alle)));
		}

		private void Copy() {
			if (SelectedVillages != null && SelectedTemplateVillage != null) {
				foreach (var village in SelectedVillages) {
					village.HandelEmpfangenArt = SelectedTemplateVillage.HandelEmpfangenArt;
					village.Active = SelectedTemplateVillage.Active;
					village.HandelEmpfangenEisenPriority = SelectedTemplateVillage.HandelEmpfangenEisenPriority;
					village.PremiumDepotHandelArt = SelectedTemplateVillage.PremiumDepotHandelArt;
					village.PremiumDepotHandelMaxVerhaeltnis = SelectedTemplateVillage.PremiumDepotHandelMaxVerhaeltnis;
					village.HandelMinRessourcen = SelectedTemplateVillage.HandelMinRessourcen;
					village.HandelEmpfangenArt = SelectedTemplateVillage.HandelEmpfangenArt;
					village.HandelEmpfangenEisenUnter = SelectedTemplateVillage.HandelEmpfangenEisenUnter;
					village.HandelEmpfangenHolzUnter = SelectedTemplateVillage.HandelEmpfangenHolzUnter;
					village.HandelEmpfangenLehmUnter = SelectedTemplateVillage.HandelEmpfangenLehmUnter;
					village.HandelMaxLaufzeit = SelectedTemplateVillage.HandelMaxLaufzeit;
					village.HandelVerschickenArt = SelectedTemplateVillage.HandelVerschickenArt;
					village.HandelVerschickenEisenUeber = SelectedTemplateVillage.HandelVerschickenEisenUeber;
					village.HandelVerschickenHolzUeber = SelectedTemplateVillage.HandelVerschickenHolzUeber;
					village.HandelVerschickenLehmUeber = SelectedTemplateVillage.HandelVerschickenLehmUeber;
					village.HandelHaendlerZurueckHalten = SelectedTemplateVillage.HandelHaendlerZurueckHalten;
					village.HandelEmpfangenHolzPriority = SelectedTemplateVillage.HandelEmpfangenHolzPriority;
					village.HandelEmpfangenLehmPriority = SelectedTemplateVillage.HandelEmpfangenLehmPriority;
					village.HandelSendenEisenPriority = SelectedTemplateVillage.HandelSendenEisenPriority;
					village.HandelSendenHolzPriority = SelectedTemplateVillage.HandelSendenHolzPriority;
					village.HandelSendenLehmPriority = SelectedTemplateVillage.HandelSendenLehmPriority;
					village.Interval = SelectedTemplateVillage.Interval;
					village.IrongettingActive = SelectedTemplateVillage.IrongettingActive;
					village.IrontradingActive = SelectedTemplateVillage.IrontradingActive;
					village.StonegettingActive = SelectedTemplateVillage.StonegettingActive;
					village.StonetradingActive = SelectedTemplateVillage.StonetradingActive;
					village.WoodgettingActive = SelectedTemplateVillage.WoodgettingActive;
					village.WoodtradingActive = SelectedTemplateVillage.WoodtradingActive;
					village.PremiumDepotStayMinutes = SelectedTemplateVillage.PremiumDepotStayMinutes;
					village.HandelRouteCoords = SelectedTemplateVillage.HandelRouteCoords;
				}
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}