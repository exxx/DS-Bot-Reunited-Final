﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using Ds_Bot_Reunited_NEWUI.Annotations;
using Ds_Bot_Reunited_NEWUI.Marktmodul.Ds_Bot_Reunited_NEWUI.Constants;
using Ds_Bot_Reunited_NEWUI.Properties;
using Ds_Bot_Reunited_NEWUI.Settings;

namespace Ds_Bot_Reunited_NEWUI.Marktsettings {
    public class MarktsettingsDorfViewModel : INotifyPropertyChanged {
        public Window Window { get; set; }
        private readonly AccountDorfSettings _accountDorfSettings;
        public string Villagename => _accountDorfSettings.Dorf.Name;
        public bool IsEnabled => App.Permissions.MarketingIsEnabled;
        
        public List<string> Methods { get; set; } = new List<string> {Resources.Marktbalancer/*, Resources.Markt*/, Resources.PremiumDepot, Resources.Handelsroute, Resources.None};
        public List<string> Methods2 { get; set; } = new List<string> {Resources.Marktbalancer, Resources.Markt, Resources.PremiumDepot, Resources.None};

        public bool PremiumDepotActive
            => App.Permissions.PremiumDepotIsEnabled && (HandelVerschickenArt.Equals(Resources.PremiumDepot) || HandelEmpfangenArt.Equals(Resources.PremiumDepot));

        public bool HandelRouteActive
            => HandelVerschickenArt.Equals(Resources.Handelsroute);

        public bool MarktbalancerSendAndGet => HandelVerschickenArt.Equals(Resources.Marktbalancer) && HandelEmpfangenArt.Equals(Resources.Marktbalancer);


        public string HandelVerschickenArt {
            get {
                if (_accountDorfSettings.HandelVerschickenArt == HandelVerschickArt.Marktbalancer)
                    return Resources.Marktbalancer;
                if (_accountDorfSettings.HandelVerschickenArt == HandelVerschickArt.Markt)
                    return Resources.Markt;
                if (_accountDorfSettings.HandelVerschickenArt == HandelVerschickArt.Premiumdepot)
                    return Resources.PremiumDepot;
                if (_accountDorfSettings.HandelVerschickenArt == HandelVerschickArt.Handelsroute)
                    return Resources.Handelsroute;
                return Resources.None;
            }
            set {
                if (value.Equals(Resources.Marktbalancer)) {
                    _accountDorfSettings.HandelVerschickenArt = HandelVerschickArt.Marktbalancer;
                    _accountDorfSettings.HandelMinRessourcen = 1000;
                }
                if (value.Equals(Resources.Markt))
                    _accountDorfSettings.HandelVerschickenArt = HandelVerschickArt.Markt;
                if (value.Equals(Resources.PremiumDepot) && App.Permissions.PremiumDepotIsEnabled)
                    _accountDorfSettings.HandelVerschickenArt = HandelVerschickArt.Premiumdepot;
                if (value.Equals(Resources.Handelsroute))
                    _accountDorfSettings.HandelVerschickenArt = HandelVerschickArt.Handelsroute;
                if (value.Equals(Resources.None))
                    _accountDorfSettings.HandelVerschickenArt = HandelVerschickArt.None;
                OnPropertyChanged("");
            }
        }

        public string HandelEmpfangenArt {
            get {
                if (_accountDorfSettings.HandelEmpfangenArt == HandelEmpfangArt.Marktbalancer)
                    return Resources.Marktbalancer;
                if (_accountDorfSettings.HandelEmpfangenArt == HandelEmpfangArt.Markt)
                    return Resources.Markt;
                if (_accountDorfSettings.HandelEmpfangenArt == HandelEmpfangArt.Premiumdepot)
                    return Resources.PremiumDepot;
                if (_accountDorfSettings.HandelEmpfangenArt == HandelEmpfangArt.Handelsroute)
                    return Resources.Handelsroute;
                return Resources.None;
            }
            set {
                if (value.Equals(Resources.Marktbalancer))
                    _accountDorfSettings.HandelEmpfangenArt = HandelEmpfangArt.Marktbalancer;
                if (value.Equals(Resources.Markt))
                    _accountDorfSettings.HandelEmpfangenArt = HandelEmpfangArt.Markt;
                if (value.Equals(Resources.PremiumDepot))
                    _accountDorfSettings.HandelEmpfangenArt = HandelEmpfangArt.Premiumdepot;
                if (value.Equals(Resources.Handelsroute))
                    _accountDorfSettings.HandelEmpfangenArt = HandelEmpfangArt.Handelsroute;
                if (value.Equals(Resources.None))
                    _accountDorfSettings.HandelEmpfangenArt = HandelEmpfangArt.None;
                OnPropertyChanged("");
            }
        }

        public int HandelVerschickenHolzUeber {
            get => _accountDorfSettings.HandelVerschickenHolzUeber;
            set {
                _accountDorfSettings.HandelVerschickenHolzUeber = value;
                OnPropertyChanged();
            }
        }

        public bool WoodtradingActive {
            get {
                return _accountDorfSettings.Handel.FirstOrDefault(x => x.Ressource.Equals("Wood")).Active;
            }
            set {
                _accountDorfSettings.Handel.FirstOrDefault(x => x.Ressource.Equals("Wood")).Active = value;
                OnPropertyChanged();
            }
        }

        public int HandelSendenHolzPriority {
            get {
                return _accountDorfSettings.Handel.FirstOrDefault(x => x.Ressource.Equals("Wood")).Priority;
            }
            set {
                _accountDorfSettings.Handel.FirstOrDefault(x => x.Ressource.Equals("Wood")).Priority = value;
                OnPropertyChanged();
            }
        }

        public int HandelVerschickenLehmUeber {
            get => _accountDorfSettings.HandelVerschickenLehmUeber;
            set {
                _accountDorfSettings.HandelVerschickenLehmUeber = value;
                OnPropertyChanged();
            }
        }

        public bool StonetradingActive {
            get {
                return _accountDorfSettings.Handel.FirstOrDefault(x => x.Ressource.Equals("Stone")).Active;
            }
            set {
                _accountDorfSettings.Handel.FirstOrDefault(x => x.Ressource.Equals("Stone")).Active = value;
                OnPropertyChanged();
            }
        }

        public int HandelSendenLehmPriority {
            get {
                return _accountDorfSettings.Handel.FirstOrDefault(x => x.Ressource.Equals("Stone")).Priority;
            }
            set {
                _accountDorfSettings.Handel.FirstOrDefault(x => x.Ressource.Equals("Stone")).Priority = value;
                OnPropertyChanged();
            }
        }

        public int HandelVerschickenEisenUeber {
            get => _accountDorfSettings.HandelVerschickenEisenUeber;
            set {
                _accountDorfSettings.HandelVerschickenEisenUeber = value;
                OnPropertyChanged();
            }
        }

        public bool IrontradingActive {
            get {
                return _accountDorfSettings.Handel.FirstOrDefault(x => x.Ressource.Equals("Iron")).Active;
            }
            set {
                _accountDorfSettings.Handel.FirstOrDefault(x => x.Ressource.Equals("Iron")).Active = value;
                OnPropertyChanged();
            }
        }

        public int HandelSendenEisenPriority {
            get {
                return _accountDorfSettings.Handel.FirstOrDefault(x => x.Ressource.Equals("Iron")).Priority;
            }
            set {
                _accountDorfSettings.Handel.FirstOrDefault(x => x.Ressource.Equals("Iron")).Priority = value;
                OnPropertyChanged();
            }
        }

        public int HandelEmpfangenHolzUnter {
            get => _accountDorfSettings.HandelEmpfangenHolzUnter;
            set {
                _accountDorfSettings.HandelEmpfangenHolzUnter = value;
                OnPropertyChanged();
            }
        }

        public bool WoodgettingActive {
            get {
                return _accountDorfSettings.Empfangen.FirstOrDefault(x => x.Ressource.Equals("Wood")).Active;
            }
            set {
                _accountDorfSettings.Empfangen.FirstOrDefault(x => x.Ressource.Equals("Wood")).Active = value;
                OnPropertyChanged();
            }
        }

        public int HandelEmpfangenHolzPriority {
            get {
                return _accountDorfSettings.Empfangen.FirstOrDefault(x => x.Ressource.Equals("Wood")).Priority;
            }
            set {
                _accountDorfSettings.Empfangen.FirstOrDefault(x => x.Ressource.Equals("Wood")).Priority = value;
                OnPropertyChanged();
            }
        }

        public int HandelEmpfangenLehmUnter {
            get => _accountDorfSettings.HandelEmpfangenLehmUnter;
            set {
                _accountDorfSettings.HandelEmpfangenLehmUnter = value;
                OnPropertyChanged();
            }
        }

        public bool StonegettingActive {
            get {
                return _accountDorfSettings.Empfangen.FirstOrDefault(x => x.Ressource.Equals("Stone")).Active;
            }
            set {
                _accountDorfSettings.Empfangen.FirstOrDefault(x => x.Ressource.Equals("Stone")).Active = value;
                OnPropertyChanged();
            }
        }

        public int HandelEmpfangenLehmPriority {
            get {
                return _accountDorfSettings.Empfangen.FirstOrDefault(x => x.Ressource.Equals("Stone")).Priority;
            }
            set {
                _accountDorfSettings.Empfangen.FirstOrDefault(x => x.Ressource.Equals("Stone")).Priority = value;
                OnPropertyChanged();
            }
        }

        public int HandelEmpfangenEisenUnter {
            get => _accountDorfSettings.HandelEmpfangenEisenUnter;
            set {
                _accountDorfSettings.HandelEmpfangenEisenUnter = value;
                OnPropertyChanged();
            }
        }

        public bool IrongettingActive {
            get {
                return _accountDorfSettings.Empfangen.FirstOrDefault(x => x.Ressource.Equals("Iron")).Active;
            }
            set {
                _accountDorfSettings.Empfangen.FirstOrDefault(x => x.Ressource.Equals("Iron")).Active = value;
                OnPropertyChanged();
            }
        }

        public int HandelEmpfangenEisenPriority {
            get {
                return _accountDorfSettings.Empfangen.FirstOrDefault(x => x.Ressource.Equals("Iron")).Priority;
            }
            set {
                _accountDorfSettings.Empfangen.FirstOrDefault(x => x.Ressource.Equals("Iron")).Priority = value;
                OnPropertyChanged();
            }
        }

        public int HandelMaxLaufzeit {
            get => _accountDorfSettings.HandelMaxLaufzeit;
            set {
                _accountDorfSettings.HandelMaxLaufzeit = value;
                OnPropertyChanged();
            }
        }

        public int HandelHaendlerZurueckHalten {
            get => _accountDorfSettings.HandelHaendlerZurueckHalten;
            set {
                _accountDorfSettings.HandelHaendlerZurueckHalten = value;
                OnPropertyChanged();
            }
        }

        public string HandelRouteCoords {
            get => _accountDorfSettings.HandelRouteCoords;
            set {
                try {
                    string[] splitted = value.Split('|');
                    int x = int.Parse(splitted[0]);
                    int y = int.Parse(splitted[1]);
                    _accountDorfSettings.HandelRouteCoords = value;
                } catch {
                    // ignored
                }
                OnPropertyChanged();
            }
        }

        public double HandelMinRessourcen {
            get => _accountDorfSettings.HandelMinRessourcen;
            set {
                _accountDorfSettings.HandelMinRessourcen = value;
                OnPropertyChanged();
            }
        }

        public string PremiumDepotHandelArt {
            get => _accountDorfSettings.PremiumDepotHandelArt;
            set {
                _accountDorfSettings.PremiumDepotHandelArt = value;
                OnPropertyChanged();
            }
        }

        public double PremiumDepotHandelMaxVerhaeltnis {
            get => _accountDorfSettings.PremiumDepotHandelMaxVerhaeltnis;
            set {
                _accountDorfSettings.PremiumDepotHandelMaxVerhaeltnis = value;
                OnPropertyChanged();
            }
        }
        
        public int PremiumDepotStayMinutes {
            get => _accountDorfSettings.PremiumDepotStayMinutes;
            set {
                _accountDorfSettings.PremiumDepotStayMinutes = value;
                OnPropertyChanged();
            }
        }

        public int Interval {
            get => _accountDorfSettings.Handelninterval;
            set {
                _accountDorfSettings.Handelninterval = value;
                OnPropertyChanged();
            }
        }

        public bool Active {
            get => _accountDorfSettings.HandelnActive;
            set {
                _accountDorfSettings.HandelnActive = value;
                OnPropertyChanged();
            }
        }

        public MarktsettingsDorfViewModel(AccountDorfSettings accountDorfSettings, Window window) {
            Window = window;
            _accountDorfSettings = accountDorfSettings;
        }


        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}