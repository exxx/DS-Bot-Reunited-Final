﻿using System.Windows;
using System.Windows.Controls;

namespace Ds_Bot_Reunited_NEWUI.Marktsettings {
    public partial class MarktsettingsView {
        public MarktsettingsView(Window owner, MarktsettingsViewModel datacontext) {
            Owner = owner;
            DataContext = datacontext;
            InitializeComponent();
        }

        private void Save(object sender, RoutedEventArgs e) {
            Close();
        }

        private void MyGrid_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            if (((MarktsettingsViewModel) DataContext)?.SelectedVillages != null) {
                ((MarktsettingsViewModel) DataContext).SelectedVillages.Clear();
                foreach (var item in DataGrid.SelectedItems) ((MarktsettingsViewModel) DataContext).SelectedVillages.Add((MarktsettingsDorfViewModel) item);
            }
        }
    }
}
