﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using Ds_Bot_Reunited_NEWUI.Annotations;
using Ds_Bot_Reunited_NEWUI.BauensettingsDorf;
using Ds_Bot_Reunited_NEWUI.Constants;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.Properties;
using Ds_Bot_Reunited_NEWUI.Spieldaten;

namespace Ds_Bot_Reunited_NEWUI.Bauensettings {
	public class BauensettingsViewModel : INotifyPropertyChanged {
		private ICommand _copyCommand;
		private GruppeDoerfer _selectedGroup;
		private BauensettingsDorfViewModel _selectedTemplateVillage;
		private BauensettingsDorfViewModel _selectedVillage;
		private ICollectionView _villages;

		public BauensettingsViewModel(Window window) {
			BauenList = new AsyncObservableCollection<BauensettingsDorfViewModel>();
			SelectedVillages = new AsyncObservableCollection<BauensettingsDorfViewModel>();
			BindingOperations.EnableCollectionSynchronization(BauenList, BauenLock);
			BindingOperations.EnableCollectionSynchronization(SelectedVillages, SelectedVillagesLock);
			SelectedGroup =
				Groups.FirstOrDefault(x => x.Gruppenname.Equals(NAMECONSTANTS.GetNameForServer(Names.Alle)));
			Villages = CollectionViewSource.GetDefaultView(BauenList);
		}

		private AsyncObservableCollection<BauensettingsDorfViewModel> BauenList { get; }
		public object BauenLock { get; set; } = new object();
		public object SelectedVillagesLock { get; set; } = new object();

		public Window Window { get; set; }
		public AsyncObservableCollection<BauensettingsDorfViewModel> SelectedVillages { get; set; }
		public List<GruppeDoerfer> Groups => App.Settings.Gruppen;

		public ICollectionView Villages {
			get => _villages;
			set {
				_villages = value;
				OnPropertyChanged();
			}
		}

		public GruppeDoerfer SelectedGroup {
			get => _selectedGroup;
			set {
				_selectedGroup = value;
#pragma warning disable 4014
				LoadPlayerVillages();
#pragma warning restore 4014
				OnPropertyChanged("");
			}
		}

		public BauensettingsDorfViewModel SelectedVillage {
			get => _selectedVillage;
			set {
				if (value == null) return;
				_selectedVillage = value;
				OnPropertyChanged();
			}
		}

		public BauensettingsDorfViewModel SelectedTemplateVillage {
			get => _selectedTemplateVillage;
			set {
				_selectedTemplateVillage = value;
				OnPropertyChanged();
			}
		}

		public ICommand CopyCommand => _copyCommand ?? (_copyCommand = new RelayCommand(Copy));

		public event PropertyChangedEventHandler PropertyChanged;

		public async Task LoadPlayerVillages() {
			await Task.Run(() => {
				               try {
					               BauenList.Clear();
					               foreach (var liste in
						               App.Settings.Accountsettingslist.Select(x => x.DoerferSettings.Where(y =>
							                                                                                    //y.Dorf.PlayerId.Equals(x.Playerid) &&
							                                                                                    SelectedGroup
								                                                                                    ?.DoerferIds
								                                                                                    .Any(k =>
									                                                                                         k
										                                                                                         .Equals(y.Dorf
										                                                                                                  .VillageId)) ??
							                                                                                    true))
					               ) {
						               foreach (var dorf in liste.OrderBy(x => x.Dorf.Name))
							               BauenList.Add(new BauensettingsDorfViewModel(dorf, Window));
					               }
				               } catch (Exception e) {
						               App.LogString(Resources.FailureLoadingPlayervillages + " " + e);
				               }

				               if (BauenList.Any()) SelectedVillage = BauenList.FirstOrDefault();
				               OnPropertyChanged("");
			               });
		}

		private void Copy() {
			if (SelectedVillages != null && SelectedTemplateVillage != null)
				foreach (var village in SelectedVillages) {
					if (village != SelectedTemplateVillage) {
						village.Bauschleife.Clear();
						foreach (var bauelement in SelectedTemplateVillage.Bauschleife)
							village.Bauschleife.Add(bauelement);
						village.BuildingActive = SelectedTemplateVillage.BuildingActive;
						village.BauenEisenUebrigLassen = SelectedTemplateVillage.BauenEisenUebrigLassen;
						village.BauenHolzUebrigLassen = SelectedTemplateVillage.BauenHolzUebrigLassen;
						village.BauenLehmUebrigLassen = SelectedTemplateVillage.BauenLehmUebrigLassen;
						village.BauenMinenAusgleichen = SelectedTemplateVillage.BauenMinenAusgleichen;
						village.BhAutomatischBauenActive = SelectedTemplateVillage.BhAutomatischBauenActive;
						village.SpeicherAutomatischBauenActive = SelectedTemplateVillage.SpeicherAutomatischBauenActive;
					}
				}
		}

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}