﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net.NetworkInformation;
using System.Threading;
using System.Threading.Tasks;
using Ds_Bot_Reunited_NEWUI.Accountsettings;
using Ds_Bot_Reunited_NEWUI.Accountsettings.Ds_Bot_Reunited.Accountsettings;
using Ds_Bot_Reunited_NEWUI.Botcaptcha;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.LoginModul;
using Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace Ds_Bot_Reunited_NEWUI.BrowserManager {
    public static class BrowserManager {

        private static DateTime _lastBrowserInitialisation;
        private static readonly ConcurrentDictionary<string, List<Browser>> Browsers;


        static BrowserManager() {
            Browsers = new ConcurrentDictionary<string, List<Browser>>();
        }

        public static bool CanAddNewBrowser() {
            return Browsers.Count < App.Settings.MaxConcurrentWindowsOpen && _lastBrowserInitialisation < DateTime.Now.AddSeconds(-10);
        }

        public static void SynchronizeCookies(Accountsetting settings) {
            var browsers = Browsers[!string.IsNullOrEmpty(settings.Uvname) ? settings.Uvname : settings.Accountname];
            var first = browsers[0];
            browsers.RemoveAt(0);
            foreach (var browser in browsers)
                try {
                    browser.WebDriver.Manage().Cookies.DeleteAllCookies();
                    foreach (var cookie in first.WebDriver.Manage().Cookies.AllCookies)
                        browser.WebDriver.Manage().Cookies.AddCookie(cookie);
                } catch {
                    // ignored
                }
        }

        public static async Task<Browser> GetFreeBrowser(Accountsetting settings, bool hide = false, bool forBewegung = false, bool NOWAIT = false) {
            Browser browser;
            Browser first;
            if (Browsers.ContainsKey(!string.IsNullOrEmpty(settings.Uvname) ? settings.Uvname : settings.Accountname))
                first = Browsers[!string.IsNullOrEmpty(settings.Uvname) ? settings.Uvname : settings.Accountname].First();
            else
                first = await GetFreeBrowser(settings, settings.ProxyEins, !hide, true,false,false, NOWAIT);
            await Botschutzerkennung.CheckIfBotschutz(first, settings);
            if (!hide||forBewegung)
                if (Browsers[!string.IsNullOrEmpty(settings.Uvname) ? settings.Uvname : settings.Accountname].All(x => x.GetsUsed)) {
                    _lastBrowserInitialisation = DateTime.Now;
                    var service = ChromeDriverService.CreateDefaultService();
                    service.HideCommandPromptWindow = true;
                    service.EnableVerboseLogging = false;
                    service.SuppressInitialDiagnosticInformation = true;
                    var chromeOptions = new ChromeOptions {LeaveBrowserRunning = false};
                    if (App.Settings.Botmode == Botmode.Botmode.Performance || forBewegung) {
                        chromeOptions.AddArguments("--headless");
                        chromeOptions.AddArguments("--start-maximized");
                    }
                    chromeOptions.AddArguments("--lang=EN");
                    chromeOptions.AddArguments("--disable-gpu");
                    chromeOptions.AddArguments("--disable-background-timer-throttling");
                    chromeOptions.PageLoadStrategy = PageLoadStrategy.Normal;

                    var webdriver = new ChromeDriver(service, chromeOptions);
                    var list = WindowHider.GetWindowHandles("data");
                    if (App.Settings.Botmode == Botmode.Botmode.Normal && !App.Developer)
                        WindowHider.HideAllNewChromeWindows();
                    webdriver.Navigate().GoToUrl("https://www.die-staemme.de");

                    foreach (var cookie in first.WebDriver.Manage().Cookies.AllCookies)
                        try {
                            webdriver.Manage().Cookies.AddCookie(cookie);
                        } catch {
                            // ignored
                        }
                    browser = new Browser {Accountname = settings.Accountname, GetsUsed = false, WebDriver = webdriver, Service = service};
                    browser.Handles.Add(new Handles {BeingUsed = true, Handle = browser.WebDriver.CurrentWindowHandle, Haupthandle = false});
                    if (list.Any())
                        browser.Handles.Last().Pointer = list.FirstOrDefault();

                    Browsers[!string.IsNullOrEmpty(settings.Uvname) ? settings.Uvname : settings.Accountname].Add(browser);
                } else
                    browser = Browsers[!string.IsNullOrEmpty(settings.Uvname) ? settings.Uvname : settings.Accountname].First(x => !x.GetsUsed);
            else
                browser = Browsers[!string.IsNullOrEmpty(settings.Uvname) ? settings.Uvname : settings.Accountname].First();

            return browser;
        }

        public static void SynchronizeCookiesOverBrowsers(Accountsetting settings) { }

        public static bool HideMainWindows() {
            var returnvalue = false;
            foreach (var browser in Browsers) {
                WindowHider.HideBrowserWindows(browser.Value.First().Handles.Where(x => x.Haupthandle).Select(x => x.Pointer).ToList());
                returnvalue = true;
            }
            return returnvalue;
        }

        public static bool ShowBotWindows() {
            var returnvalue = false;
            foreach (var browser in Browsers) {
                foreach (var brow in browser.Value)
                    WindowHider.ShowBrowserWindows(brow.Handles.Where(x => !x.Haupthandle).Select(x => x.Pointer).ToList());
                returnvalue = true;
            }
            return returnvalue;
        }

        public static bool HideBotWindows() {
            var returnvalue = false;
            foreach (var browser in Browsers) {
                foreach (var brow in browser.Value)
                    WindowHider.HideBrowserWindows(brow.Handles.Where(x => !x.Haupthandle).Select(x => x.Pointer).ToList());
                returnvalue = true;
            }
            return returnvalue;
        }

        public static bool ShowMainWindows() {
            var returnvalue = false;
            foreach (var browser in Browsers) {
                WindowHider.ShowBrowserWindows(browser.Value.FirstOrDefault()?.Handles?.Where(x => x.Haupthandle).Select(x => x.Pointer).ToList());
                returnvalue = true;
            }
            return returnvalue;
        }

        public static async Task<Browser> GetFreeBrowser(Accountsetting settings, string proxy, bool hidewindow = true, bool login = true, bool explicitlogin = false, bool remove = false, bool NOWAIT = false) {
            while (!App.Active && !remove && !NOWAIT)
                await Task.Delay(100);
            Browser browser;
            if (Browsers.ContainsKey(!string.IsNullOrEmpty(settings.Uvname) ? settings.Uvname : settings.Accountname))
                browser = Browsers[!string.IsNullOrEmpty(settings.Uvname) ? settings.Uvname : settings.Accountname].First();
            else {
                browser = AddNewBrowser(settings, proxy, hidewindow, remove);
                if (login) {
                    if (settings.MultiaccountingDaten.Accountmode.Equals(Accountmode.Botaccount) && !explicitlogin)
                        if (hidewindow) {
                            WindowHider.HideAllNewChromeWindows();
                            browser.WebDriver.Manage().Window.Position = new Point(4000, 4000);
                        }
                    await Loginmodul.LoginStart(browser, settings);
                    Thread.Sleep(2000);
                } else
                    Thread.Sleep(100);
            }
            if (remove) {
                RemoveBrowser(settings);
            }
            return browser;
        }

        public static Browser AddNewBrowser(Accountsetting settings, string proxy, bool hide = true, bool leaveWindowOpen = false, string proxyArt = "socks5://", string accountname = "") {
            while(!CanAddNewBrowser()) Thread.Sleep(100);
            _lastBrowserInitialisation = DateTime.Now;
            var service = ChromeDriverService.CreateDefaultService();
            service.HideCommandPromptWindow = true;
            service.EnableVerboseLogging = false;
            service.SuppressInitialDiagnosticInformation = true;
            var chromeOptions = new ChromeOptions {LeaveBrowserRunning = leaveWindowOpen};
            if (!string.IsNullOrEmpty(proxy))
                chromeOptions.AddArguments("--proxy-server=" + proxyArt + proxy);
            if (settings == null || settings.MultiaccountingDaten.Accountmode == Accountmode.Botaccount && hide) {
                chromeOptions.AddArguments("--headless");
                chromeOptions.AddArguments("--start-maximized");
            }
            chromeOptions.AddArguments("--lang=EN");
            chromeOptions.AddArguments("--disable-gpu");
            chromeOptions.AddArguments("--disable-background-timer-throttling");
            chromeOptions.PageLoadStrategy = PageLoadStrategy.Normal;
            var newbrowser = new Browser {
                                             Accountname = settings != null ? (!string.IsNullOrEmpty(settings.Uvname) ? settings.Uvname : settings.Accountname) : accountname, GetsUsed = true, WebDriver = new ChromeDriver(service, chromeOptions),
                                             Service = service
                                         };
            var list = WindowHider.GetWindowHandles("data");
            if (hide)
                WindowHider.HideAllNewChromeWindows();
            newbrowser.Handles.Add(new Handles {BeingUsed = true, Handle = newbrowser.WebDriver.CurrentWindowHandle, Haupthandle = true});
            if (list.Any())
                newbrowser.Handles.Last().Pointer = list.FirstOrDefault();
            bool added;
            do {
                added = Browsers.TryAdd(!string.IsNullOrEmpty(settings.Uvname) ? settings.Uvname : settings.Accountname, new List<Browser> {newbrowser});
            } while (!added);
            return newbrowser;
        }

        public static int GetAvailablePort(int startingPort) {
            var portArray = new List<int>();

            var properties = IPGlobalProperties.GetIPGlobalProperties();

            //getting active connections
            var connections = properties.GetActiveTcpConnections();
            portArray.AddRange(from n in connections where n.LocalEndPoint.Port >= startingPort select n.LocalEndPoint.Port);

            //getting active tcp listners - WCF service listening in tcp
            var endPoints = properties.GetActiveTcpListeners();
            portArray.AddRange(from n in endPoints where n.Port >= startingPort select n.Port);

            //getting active udp listeners
            endPoints = properties.GetActiveUdpListeners();
            portArray.AddRange(from n in endPoints where n.Port >= startingPort select n.Port);

            portArray.Sort();

            for (var i = startingPort; i < ushort.MaxValue; i++)
                if (!portArray.Contains(i))
                    return i;

            return 0;
        }

        public static void RemoveBrowser(Accountsetting settings, bool withoutdestroying = false) {
            var temp = Browsers[!string.IsNullOrEmpty(settings.Uvname) ? settings.Uvname : settings.Accountname];
            if (temp != null && temp.Any()) {
                foreach (var browser in temp)
                    browser.Destroy(withoutdestroying);
                bool done;
                do {
                    done = Browsers.TryRemove(!string.IsNullOrEmpty(settings.Uvname) ? settings.Uvname : settings.Accountname, out temp);
                } while (!done);
            }
        }

        public static void RemoveBrowser(Browser browser) {
            var first = Browsers.FirstOrDefault(x => x.Value.Any(k => k.Equals(browser)));
            browser.Destroy(true);
            first.Value?.Remove(browser);
        }

        public static void DestroyAll() {
            Browsers.ForEach(x => x.Value.ForEach(k => k.Destroy(true)));
            Browsers.Clear();
        }
    }
}
