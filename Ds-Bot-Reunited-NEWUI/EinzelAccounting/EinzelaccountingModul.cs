﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ds_Bot_Reunited_NEWUI.Accountsettings;
using Ds_Bot_Reunited_NEWUI.ActionModul;
using Ds_Bot_Reunited_NEWUI.Angreifensettings;
using Ds_Bot_Reunited_NEWUI.Farmmodul;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.MuenzenModul;
using Ds_Bot_Reunited_NEWUI.Properties;
using Ds_Bot_Reunited_NEWUI.Spieldaten;
using Ds_Bot_Reunited_NEWUI.SynchronizeModul;
using OpenQA.Selenium;

namespace Ds_Bot_Reunited_NEWUI.EinzelAccounting {
	public class EinzelaccountingModul {
		private static TileViewModel _vm;

		public EinzelaccountingModul(TileViewModel vm) { _vm = vm; }

		public void EinzelAccountCycles() {
			if (App.Settings.Accountsettingslist.Any(x =>
				                                         x
					                                         .MultiaccountingDaten.Accountmode
					                                         .Equals(Accountmode.Mainaccount))) {
				BewegungenCycle();
				ActionsCycle();
			}
		}

		private static void BewegungenCycle() {
			var again = false;
			do {
				var task = new Task(async () => {
                    await Task.Delay(10000);
					while (true) {
						if (App.Settings.EinzelaccountingActive)
                        {
                            try {
								if (App.Settings.Accountsettingslist.Any(y =>
									                                            y
										                                            .GeplanteBewegungen
										                                            .Any(x =>
											                                                x
												                                                .Active &&
											                                                !x.Sent &&
											                                                !x
												                                                .Queued &&
											                                                !x
												                                                .GetsQueued &&
											                                                x.Abschickzeitpunkt >
											                                                DateTime
												                                                .Now))
								) {
                                    foreach (var account in
                                        App.Settings.Accountsettingslist.Where(x => x.LoginActive)
                                    )
                                    {
                                        App.Settings.DetectActive = false;     
                                        foreach (var bewegung in account
                                                        .GeplanteBewegungen
                                                        .Where(x => !x.GetsQueued &&
                                                                            x.Active && !x.Sent &&
                                                                            !x.Queued &&
                                                                            (x.Abschickzeitpunkt
                                                                                .AddSeconds(-App
                                                                                            .Settings
                                                                                            .Preparationtime)
                                                                                .AddMilliseconds(App
                                                                                                .Settings
                                                                                                .Difference) <
                                                                                DateTime.Now &&
                                                                                x.GenauTimenIsActive ||
                                                                                x.Abschickzeitpunkt <
                                                                                DateTime.Now &&
                                                                                !x.GenauTimenIsActive
                                                                            ))
                                                        .OrderByDescending(x => x.GenauTimenIsActive)
                                                        .ThenBy(x => x.Abschickzeitpunkt))
                                        {
                                            if (bewegung != null)
                                            {
                                                bewegung.GetsQueued = true;
                                                StringBuilder builder = new StringBuilder();
                                                builder.Append(account.Accountname);
                                                builder.Append(" ");
                                                builder.Append(Resources.Attackpreparation);
                                                builder.Append(" ");
                                                builder.Append(Resources.Started);
                                                builder.Append(", ");
                                                builder.Append(Resources.From);
                                                builder.Append(" ");
                                                builder.Append(bewegung.SenderDorf.Name);
                                                builder.Append(" ");
                                                builder.Append(Resources.To);
                                                builder.Append(" ");
                                                builder.Append(bewegung.EmpfaengerDorf.Name);
                                                builder.Append(" ");
                                                builder.Append(Resources.Departure);
                                                builder.Append(": ");
                                                builder.Append(bewegung
                                                                .Abschickzeitpunkt
                                                                .ToString("dd.MM.yyyy HH:mm:ss.fff"));
                                                builder.Append(" ");
                                                builder.Append(Resources.Arrival);
                                                builder.Append(": ");
                                                builder.Append(bewegung.Ankunftszeitpunkt
                                                                        .ToString("dd.MM.yyyy HH:mm:ss.fff"));
                                                App.LogString(builder.ToString());
                                                _vm.Status = Resources.Attackpreparation;
                                                var xx = new BewegungAccountsettingsmerged
                                                {
                                                    Accountsetting = account, Bewegung = new TruppenbewegungViewModel(bewegung)
                                                };
                                                if (!xx.Bewegung.GenauTimenIsActive) 
                                                    bewegung.Abschickzeitpunkt = bewegung.Abschickzeitpunkt.AddMilliseconds(App.Random.Next(-300, 800));
                                                BewegungenModul.StartBewegungAsync(xx);

                                                _vm.Status = "";
                                            }
                                        }
                                    }
								}
							} catch (Exception e) {
								App.LogString(DateTime.Now.GetString() +
								                " " + Resources.Sendmodul + " " +
								                Resources.Failure + ": " + e);
							}
						}

						await Task.Delay(100);
					}
				}, TaskCreationOptions.LongRunning);

				try {
					task.Start();
				} catch (Exception e) {
					App.LogString(DateTime.Now.GetString() + " Timeloop Error: " +
					              e);
					again = true;
				}
			} while (again);
		}

		private static void ClearData() {
			var dt = DateTime.Now.AddDays(-3);
			if (App.Berichte != null && App.Berichte.Any()) {
				var berichte = App.Berichte.Where(x => dt > x.Kampfzeit).ToList();
				var count = berichte.Count;
				for (var i = 0; i < count; i++) App.Berichte.Remove(berichte[i]);
			}

			foreach (var farm in App.Farmliste) {
				var attackstoDelete = farm.Attacks.Any()
					                      ? farm.Attacks.Where(x => x.LandTime < DateTime.Now).ToList()
					                      : new List<AttacksOnFarmTarget>();
				// ReSharper disable once ForCanBeConvertedToForeach
				for (var i = 0; i < attackstoDelete.Count; i++) farm.Attacks.Remove(attackstoDelete[i]);
			}

			foreach (var account in App.Settings.Accountsettingslist)
				foreach (var transfer in account.DoerferSettings.Select(x => x.Dorfinformationen)) {
					var list = transfer.AnkommendeMarkttransfers.Where(x => x.Datum < DateTime.Now).ToList();
					var counter = list.Count;
					for (var i = 0; i < counter; i++) transfer.AnkommendeMarkttransfers.Remove(list[i]);
				}
		}

		private static void ActionsCycle() {
			var again = false;
			do {
				var task = new Task(async () => {
                    while (true)
                    {
                        if (App.Settings.EinzelaccountingActive)
                        {
                            foreach (var account in
                                App.Settings.Accountsettingslist.Where(x => x.MultiaccountingDaten
                                                                             .Accountmode
                                                                             .Equals(Accountmode
                                                                                         .Mainaccount)))
                                QueueManager.QueueManager.UpdateQueueForAccount(account);
                            await Task.Delay(1000);
                            while (App.PauseEinzelaccounting)
                            {
                                await Task.Delay(1000);
                            }

                            while (!App.Active) await Task.Delay(100);
                            if (App.PriorityQueue.Any() &&
                                App.PriorityQueue.FirstOrDefault(x => x.Ausfuehrungszeitpunkt >
                                                                      DateTime.Now &&
                                                                      x.Ausfuehrungszeitpunkt <
                                                                      DateTime.Now.AddSeconds(30) ||
                                                                      x.Ausfuehrungszeitpunkt <
                                                                      DateTime.Now) != null)
                            {
                                ClearData();
                                if (App.LastWorlddataCheck.AddMinutes(60) < DateTime.Now &&
                                    !App.SaveIsGoingOn)
                                    try
                                    {
                                        await Worlddata.LoadWorldData();
                                        App.LastWorlddataCheck = DateTime.Now;
                                    }
                                    // ReSharper disable once EmptyGeneralCatchClause
                                    catch (Exception) { }

                                var actionModul = QueueManager.QueueManager.Dequeue();
                                if (actionModul != null)
                                {
                                    var modul = actionModul;
                                    var settings =
                                        App.Settings.Accountsettingslist.First(x =>
                                                                                   x
                                                                                       .Accountname
                                                                                       .Equals(modul
                                                                                                   .Accountname) ||
                                                                                   !string
                                                                                       .IsNullOrEmpty(x.Uvname) &&
                                                                                   x.Uvname
                                                                                    .Equals(modul
                                                                                                .Accountname));
                                    if (settings != null)
                                    {
                                        var browser =
                                            await BrowserManager
                                                  .BrowserManager.GetFreeBrowser(settings);
                                        browser.GetsUsed = true;
                                        if (modul != null)
                                        {
                                            while (DateTime.Now < modul.Ausfuehrungszeitpunkt)
                                                await Task.Delay(1000);
                                            try
                                            {
                                                StringBuilder builder = new StringBuilder();
                                                switch (modul.AktionArt)
                                                {
                                                    case ActionArten.Farming:
                                                        builder.Append(Resources.FarmingDequed);
                                                        builder.Append(" ");
                                                        builder
                                                            .Append(!string.IsNullOrEmpty(settings
                                                                                              .Uvname)
                                                                        ? settings.Uvname
                                                                        : settings.Accountname);
                                                        App.LogString(builder.ToString());
                                                        if (settings.FarmenActive)
                                                        {
                                                            _vm.Status = Resources.Farmen;
                                                            await QuestModul
                                                                  .QuestModul
                                                                  .MakeQuests(browser, settings
                                                                              , settings
                                                                                .DoerferSettings
                                                                                .FirstOrDefault());
                                                            await FarmModul.StartFarming(browser
                                                                                         , settings,
                                                                                         App
                                                                                             .PlayerlistWholeWorld
                                                                                             .FirstOrDefault(x =>
                                                                                                                 x
                                                                                                                     .PlayerName
                                                                                                                     .Equals(!string.IsNullOrEmpty(settings.Uvname) ? settings.Uvname : settings.Accountname
                                                                                                                             ,
                                                                                                                             StringComparison
                                                                                                                                 .CurrentCultureIgnoreCase))
                                                                                             .Spieler
                                                                                             .PlayerPoints);
                                                            settings.FarmenQueued = false;
                                                            _vm.Status = "";
                                                        }

                                                        break;
                                                    case ActionArten.Building:
                                                        builder.Append(Resources.BuildingDequed);
                                                        builder.Append(" ");
                                                        builder
                                                            .Append(!string.IsNullOrEmpty(settings
                                                                                              .Uvname)
                                                                        ? settings.Uvname
                                                                        : settings.Accountname);
                                                        App.LogString(builder.ToString());
                                                        if (settings.BauenActive)
                                                        {
                                                            _vm.Status = Resources.Bauen;
                                                            await QuestModul
                                                                  .QuestModul
                                                                  .MakeQuests(browser, settings
                                                                              , settings
                                                                                .DoerferSettings
                                                                                .FirstOrDefault());
                                                            var bauen = new BauenModul.BauenModul();
                                                            await bauen.Bauen(browser, settings);
                                                            settings.BauenQueued = false;
                                                            _vm.Status = "";
                                                        }

                                                        break;
                                                    case ActionArten.Recruiting:
                                                        builder.Append(Resources.RecruitingDequed);
                                                        builder.Append(" ");
                                                        builder
                                                            .Append(!string.IsNullOrEmpty(settings
                                                                                              .Uvname)
                                                                        ? settings.Uvname
                                                                        : settings.Accountname);
                                                        App.LogString(builder.ToString());
                                                        if (settings.RekrutierenActive)
                                                        {
                                                            _vm.Status = Resources.Rekrutieren;
                                                            await QuestModul
                                                                  .QuestModul
                                                                  .MakeQuests(browser, settings
                                                                              , settings
                                                                                .DoerferSettings
                                                                                .FirstOrDefault());
                                                            await RekrutierenModul
                                                                  .RekrutierenModul
                                                                  .StartRekrutBalance(browser
                                                                                      , settings);
                                                            settings.RekrutierenQueued = false;
                                                            _vm.Status = "";
                                                        }

                                                        break;
                                                    case ActionArten.Trading:
                                                        builder.Append(Resources.MarketingDequed);
                                                        builder.Append(" ");
                                                        builder
                                                            .Append(!string.IsNullOrEmpty(settings
                                                                                              .Uvname)
                                                                        ? settings.Uvname
                                                                        : settings.Accountname);
                                                        App.LogString(builder.ToString());
                                                        if (settings.HandelnActive)
                                                        {
                                                            _vm.Status = Resources.Markt;
                                                            await QuestModul
                                                                  .QuestModul
                                                                  .MakeQuests(browser, settings
                                                                              , settings
                                                                                .DoerferSettings
                                                                                .FirstOrDefault());
                                                            await Marktmodul
                                                                  .Marktmodul
                                                                  .StartTrading(browser, settings);
                                                            settings.HandelQueued = false;
                                                            _vm.Status = "";
                                                        }

                                                        break;
                                                    case ActionArten.Defend:
                                                        builder.Append(Resources.DefendDequed);
                                                        builder.Append(" ");
                                                        builder
                                                            .Append(!string.IsNullOrEmpty(settings
                                                                                              .Uvname)
                                                                        ? settings.Uvname
                                                                        : settings.Accountname);
                                                        App.LogString(builder.ToString());
                                                        if (App.Settings.DetectActive)
                                                        {
                                                            _vm.Status = Resources.Defending;
                                                            await DefendModul
                                                                  .DefendModul
                                                                  .Defend(browser, settings);
                                                            settings.DefendQueued = false;
                                                            _vm.Status = "";
                                                        }

                                                        break;
                                                    case ActionArten.Rausstellen:
                                                        builder.Append("Rausstellen");
                                                        builder.Append(" ");
                                                        builder
                                                            .Append(!string.IsNullOrEmpty(settings
                                                                                              .Uvname)
                                                                        ? settings.Uvname
                                                                        : settings.Accountname);
                                                        App.LogString(builder.ToString());
                                                        await QuestModul
                                                              .QuestModul
                                                              .MakeQuests(browser, settings
                                                                          , settings
                                                                            .DoerferSettings
                                                                            .FirstOrDefault());
                                                        //Todo Rausstellen
                                                        settings.RausstellenQueued = false;
                                                        break;
                                                    case ActionArten.Coins:
                                                        builder.Append(Resources.GoldmuenzenDequed);
                                                        builder.Append(" ");
                                                        builder
                                                            .Append(!string.IsNullOrEmpty(settings
                                                                                              .Uvname)
                                                                        ? settings.Uvname
                                                                        : settings.Accountname);
                                                        App.LogString(builder.ToString());
                                                        await QuestModul
                                                              .QuestModul
                                                              .MakeQuests(browser, settings
                                                                          , settings
                                                                            .DoerferSettings
                                                                            .FirstOrDefault());
                                                        await AgPraegenMuenzenModul
                                                            .Start(browser, settings);
                                                        settings.RausstellenQueued = false;
                                                        break;
                                                    case ActionArten.Forge:
                                                        builder.Append(Resources.ForschenDequed);
                                                        builder.Append(" ");
                                                        builder
                                                            .Append(!string.IsNullOrEmpty(settings
                                                                                              .Uvname)
                                                                        ? settings.Uvname
                                                                        : settings.Accountname);
                                                        App.LogString(builder.ToString());
                                                        await QuestModul
                                                              .QuestModul
                                                              .MakeQuests(browser, settings
                                                                          , settings
                                                                            .DoerferSettings
                                                                            .FirstOrDefault());
                                                        await ForschenModul
                                                              .ForschenModul
                                                              .Start(browser, settings);
                                                        settings.RausstellenQueued = false;
                                                        break;
                                                }

                                                if (!QueueManager
                                                     .QueueManager
                                                     .AnyForThisAccountInTheNext10Minutes(modul
                                                                                              .Accountname) &&
                                                    settings.AutomaticLogoutActive)
                                                    BrowserManager
                                                        .BrowserManager.RemoveBrowser(settings);
                                            }
                                            catch (NoSuchWindowException e)
                                            {
                                                App.LogString("General ERROR at Runtime! " +
                                                              e);
                                                BrowserManager
                                                    .BrowserManager.RemoveBrowser(settings);
                                                App.LogString(DateTime
                                                              .Now
                                                              .ToString("dd.MM.yyyy HH:mm:ss.fff"
                                                                        , CultureInfo
                                                                            .InvariantCulture) +
                                                              " Chromedriver restarted! ");
                                                switch (modul.AktionArt)
                                                {
                                                    case ActionArten.Farming:
                                                        settings.FarmenQueued = false;
                                                        break;
                                                    case ActionArten.Building:
                                                        settings.BauenQueued = false;
                                                        break;
                                                    case ActionArten.Recruiting:
                                                        settings.RekrutierenQueued = false;
                                                        break;
                                                    case ActionArten.Trading:
                                                        settings.HandelQueued = false;
                                                        break;
                                                    case ActionArten.Rausstellen:
                                                        settings.RausstellenQueued = false;
                                                        break;
                                                }
                                            }
                                            catch (Exception e)
                                            {
                                                if (e.Message.Contains("WebException") ||
                                                    e.Message.Contains("InvalidOperationException")
                                                )
                                                {
                                                    App.LogString("General ERROR at Runtime! " +
                                                                  e);
                                                    switch (modul.AktionArt)
                                                    {
                                                        case ActionArten.Farming:
                                                            settings.FarmenQueued = false;
                                                            break;
                                                        case ActionArten.Building:
                                                            settings.BauenQueued = false;
                                                            break;
                                                        case ActionArten.Recruiting:
                                                            settings.RekrutierenQueued = false;
                                                            break;
                                                        case ActionArten.Trading:
                                                            settings.HandelQueued = false;
                                                            break;
                                                        case ActionArten.Rausstellen:
                                                            settings.RausstellenQueued = false;
                                                            break;
                                                    }
                                                }
                                                else
                                                {
                                                    App.LogString(DateTime
                                                                  .Now
                                                                  .ToString("dd.MM.yyyy HH:mm:ss.fff"
                                                                            , CultureInfo
                                                                                .InvariantCulture) +
                                                                  " General ERROR at Runtime! " +
                                                                  e);

                                                    switch (modul.AktionArt)
                                                    {
                                                        case ActionArten.Farming:
                                                            settings.FarmenQueued = false;
                                                            break;
                                                        case ActionArten.Building:
                                                            settings.BauenQueued = false;
                                                            break;
                                                        case ActionArten.Recruiting:
                                                            settings.RekrutierenQueued = false;
                                                            break;
                                                        case ActionArten.Trading:
                                                            settings.HandelQueued = false;
                                                            break;
                                                        case ActionArten.Rausstellen:
                                                            settings.RausstellenQueued = false;
                                                            break;
                                                        case ActionArten.Coins:
                                                            settings.GoldCoinsQueued = false;
                                                            break;
                                                    }
                                                }
                                            }
                                        }

                                        browser.GetsUsed = false;
                                    }
									                    await Task.Delay(1000);
								                    }

								                    // ReSharper disable once FunctionNeverReturns
							                    }
						                    }
					                    }
				                    }, TaskCreationOptions.LongRunning);

				try {
					task.Start();
				} catch (Exception e) {
					App.LogString("Timeloop Error: " + e);
					again = true;
				}
			} while (again);
		}
	}
}