﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ds_Bot_Reunited_NEWUI.AngreifenModul;
using Ds_Bot_Reunited_NEWUI.Angreifensettings;
using Ds_Bot_Reunited_NEWUI.Constants;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.Properties;
using Ds_Bot_Reunited_NEWUI.Spieldaten;
using Ds_Bot_Reunited_NEWUI.SpieldatenViewModel;
using OpenQA.Selenium;

namespace Ds_Bot_Reunited_NEWUI.EinzelAccounting {
    public class BewegungenModul {
        public static void StartBewegungAsync(object bewegungaccountsetting) {
            Task.Run(() => { BewegungLogik(bewegungaccountsetting); });
        }

        private static async void BewegungLogik(object bewegungaccountsetting) {
            var bewegungaccountsett = (BewegungAccountsettingsmerged) bewegungaccountsetting;

            var truppenbewegung = bewegungaccountsett.Bewegung;
            var accountsettings = bewegungaccountsett.Accountsetting;
            try {
                var browser = await BrowserManager.BrowserManager.GetFreeBrowser(bewegungaccountsett.Accountsetting, true, true) /*BrowserManager.BrowserManager.GetFreeBrowser(accountsettings)*/;

                browser.GetsUsed = true;
                var logtext = " Angriff von " + truppenbewegung.SenderName + " " + truppenbewegung.SenderX + "|" + truppenbewegung.SenderY + " auf " + truppenbewegung.EmpfaengerName + " " + truppenbewegung.EmpfaengerX + "|" +
                              truppenbewegung.EmpfaengerY + " Ankunft " + truppenbewegung.Ankunftszeitpunkt;


                var uri = App.Settings.OperateLink + "village=" + truppenbewegung.Model.SenderDorf.VillageId + "" + accountsettings.UvZusatzFuerLink + "&screen=place&target=" + truppenbewegung.Model.EmpfaengerDorf.VillageId;
                browser.WebDriver.Navigate().GoToUrl(uri);
                browser.WebDriver.WaitForPageload();
                var dorf = accountsettings.DoerferSettings.First(x => x.Dorf.VillageId.Equals(truppenbewegung.Model.SenderDorf.VillageId));
                if (dorf != null && dorf.LastTruppeneinlesen < DateTime.Now.AddMinutes(30))
                    dorf.Dorfinformationen.Truppen = DorfinformationenModul.DorfinformationenModul.GetAvailableTroops(browser, dorf);
                var accountDorfSettings = accountsettings.DoerferSettings.FirstOrDefault(x => x.Dorf.VillageId.Equals(truppenbewegung.Model.SenderDorf.VillageId));
                if (accountDorfSettings != null) {
                    var troopstosend = new List<TroopTemplate> {
                                                                   new TroopTemplate(truppenbewegung.Rausstellen ? accountDorfSettings.Dorfinformationen.Truppen.SpaeherAnzahl : truppenbewegung.Model.SpaeherAnzahl, "spy"),
                                                                   new TroopTemplate(truppenbewegung.Rausstellen ? accountDorfSettings.Dorfinformationen.Truppen.SpeertraegerAnzahl : truppenbewegung.Model.SpeertraegerAnzahl, "spear"),
                                                                   new TroopTemplate(truppenbewegung.Rausstellen ? accountDorfSettings.Dorfinformationen.Truppen.SchwertkaempferAnzahl : truppenbewegung.Model.SchwertkaempferAnzahl, "sword"),
                                                                   new TroopTemplate(truppenbewegung.Rausstellen ? accountDorfSettings.Dorfinformationen.Truppen.AxtkaempferAnzahl : truppenbewegung.Model.AxtkaempferAnzahl, "axe"),
                                                                   new TroopTemplate(truppenbewegung.Rausstellen ? accountDorfSettings.Dorfinformationen.Truppen.BogenschuetzenAnzahl : truppenbewegung.Model.BogenschuetzenAnzahl, "archer"),
                                                                   new TroopTemplate(truppenbewegung.Rausstellen ? accountDorfSettings.Dorfinformationen.Truppen.LeichteKavallerieAnzahl : truppenbewegung.Model.LeichteKavallerieAnzahl, "light"),
                                                                   new TroopTemplate(truppenbewegung.Rausstellen ? accountDorfSettings.Dorfinformationen.Truppen.SchwereKavallerieAnzahl : truppenbewegung.Model.SchwereKavallerieAnzahl, "heavy"),
                                                                   new TroopTemplate(truppenbewegung.Rausstellen ? accountDorfSettings.Dorfinformationen.Truppen.RammboeckeAnzahl : truppenbewegung.Model.RammboeckeAnzahl, "ram"),
                                                                   new TroopTemplate(truppenbewegung.Rausstellen ? accountDorfSettings.Dorfinformationen.Truppen.KatapulteAnzahl : truppenbewegung.Model.KatapulteAnzahl, "cat"),
                                                                   new TroopTemplate(truppenbewegung.Rausstellen ? accountDorfSettings.Dorfinformationen.Truppen.PaladinAnzahl : truppenbewegung.Model.PaladinAnzahl, "knight"),
                                                                   new TroopTemplate(truppenbewegung.Rausstellen ? accountDorfSettings.Dorfinformationen.Truppen.AdelsgeschlechterAnzahl : truppenbewegung.Model.AdelsgeschlechterAnzahl, "snob"),
                                                                   new TroopTemplate(truppenbewegung.Rausstellen ? accountDorfSettings.Dorfinformationen.Truppen.BeritteneBogenschuetzenAnzahl : truppenbewegung.Model.BeritteneBogenschuetzenAnzahl,
                                                                                     "marcher")
                                                               };
                    var errors = "";
                    if (truppenbewegung.BewegungsartSelected == Resources.Support)
                        errors = await SendModul.SendModul.SendSupportOverPlace(browser, troopstosend, truppenbewegung.Model.SenderDorf, truppenbewegung.Model.Abschickzeitpunkt.AddMilliseconds(App.Settings.Difference),
                                                                                truppenbewegung.GenauTimenIsActive);
                    else if (truppenbewegung.BewegungsartSelected == Resources.Attack) {
                        errors = await SendModul.SendModul.SendAttackOverPlace(browser, troopstosend, App.Settings.Weltensettings.FakeActive ? truppenbewegung.Model.SenderDorf : new DorfViewModel {Dorf = new Dorf()},
                                                                               truppenbewegung.Model.Abschickzeitpunkt.AddMilliseconds(App.Settings.Difference), truppenbewegung.GenauTimenIsActive, truppenbewegung.Model.KattaZiel);
                        if (string.IsNullOrEmpty(errors)) {
                            await Task.Delay(500);
                            if (truppenbewegung.Model.Begleittruppen.Any(x => x.SpeertraegerAnzahl > 0 || x.SchwertkaempferAnzahl > 0 || x.AxtkaempferAnzahl > 0 || x.BogenschuetzenAnzahl > 0 || x.SpaeherAnzahl > 0 || x.LeichteKavallerieAnzahl > 0 ||
                                                                              x.BeritteneBogenschuetzenAnzahl > 0 || x.SchwereKavallerieAnzahl > 0 || x.RammboeckeAnzahl > 0 || x.KatapulteAnzahl > 0 || x.PaladinAnzahl > 0 ||
                                                                              x.AdelsgeschlechterAnzahl > 0)) {
                                var counter = 2;
                                foreach (var begleit in truppenbewegung.Model.Begleittruppen.Where(x => x.SpeertraegerAnzahl > 0 || x.SchwertkaempferAnzahl > 0 || x.AxtkaempferAnzahl > 0 || x.BogenschuetzenAnzahl > 0 || x.SpaeherAnzahl > 0 ||
                                                                                                        x.LeichteKavallerieAnzahl > 0 || x.BeritteneBogenschuetzenAnzahl > 0 || x.SchwereKavallerieAnzahl > 0 || x.RammboeckeAnzahl > 0 ||
                                                                                                        x.KatapulteAnzahl > 0 || x.PaladinAnzahl > 0 || x.AdelsgeschlechterAnzahl > 0))
                                    if (begleit.AdelsgeschlechterAnzahl > 0) {
                                        browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Versammlungsplatz.Befehle.Befehlbestaetigung.NOBELTRAIN_BUTTON_SELECTOR_XPATH)).Click();
                                        browser.WebDriver.WaitForAjax();
                                        browser.WebDriver.FindElement(By.XPath("//input[@name='train[" + counter + "][spear]']")).Clear();
                                        browser.WebDriver.FindElement(By.XPath("//input[@name='train[" + counter + "][spear]']")).SendKeys(begleit.SpeertraegerAnzahl.ToString());
                                        await Rndm.Sleep(100, 200);
                                        browser.WebDriver.FindElement(By.XPath("//input[@name='train[" + counter + "][sword]']")).Clear();
                                        browser.WebDriver.FindElement(By.XPath("//input[@name='train[" + counter + "][sword]']")).SendKeys(begleit.SchwertkaempferAnzahl.ToString());
                                        await Rndm.Sleep(100, 200);
                                        browser.WebDriver.FindElement(By.XPath("//input[@name='train[" + counter + "][axe]']")).Clear();
                                        browser.WebDriver.FindElement(By.XPath("//input[@name='train[" + counter + "][axe]']")).SendKeys(begleit.AxtkaempferAnzahl.ToString());
                                        await Rndm.Sleep(100, 200);
                                        if (App.Settings.Weltensettings.BogenschuetzenActive) {
                                            browser.WebDriver.FindElement(By.XPath("//input[@name='train[" + counter + "][archer]']")).Clear();
                                            browser.WebDriver.FindElement(By.XPath("//input[@name='train[" + counter + "][archer]']")).SendKeys(begleit.BogenschuetzenAnzahl.ToString());
                                            await Rndm.Sleep(100, 200);
                                        }
                                        browser.WebDriver.FindElement(By.XPath("//input[@name='train[" + counter + "][spy]']")).Clear();
                                        browser.WebDriver.FindElement(By.XPath("//input[@name='train[" + counter + "][spy]']")).SendKeys(begleit.SpaeherAnzahl.ToString());
                                        await Rndm.Sleep(100, 200);
                                        browser.WebDriver.FindElement(By.XPath("//input[@name='train[" + counter + "][light]']")).Clear();
                                        browser.WebDriver.FindElement(By.XPath("//input[@name='train[" + counter + "][light]']")).SendKeys(begleit.LeichteKavallerieAnzahl.ToString());
                                        await Rndm.Sleep(100, 200);
                                        if (App.Settings.Weltensettings.BogenschuetzenActive) {
                                            browser.WebDriver.FindElement(By.XPath("//input[@name='train[" + counter + "][marcher]']")).Clear();
                                            browser.WebDriver.FindElement(By.XPath("//input[@name='train[" + counter + "][marcher]']")).SendKeys(begleit.BeritteneBogenschuetzenAnzahl.ToString());
                                            await Rndm.Sleep(100, 200);
                                        }
                                        browser.WebDriver.FindElement(By.XPath("//input[@name='train[" + counter + "][heavy]']")).Clear();
                                        browser.WebDriver.FindElement(By.XPath("//input[@name='train[" + counter + "][heavy]']")).SendKeys(begleit.SchwereKavallerieAnzahl.ToString());
                                        await Rndm.Sleep(100, 200);
                                        browser.WebDriver.FindElement(By.XPath("//input[@name='train[" + counter + "][ram]']")).Clear();
                                        browser.WebDriver.FindElement(By.XPath("//input[@name='train[" + counter + "][ram]']")).SendKeys(begleit.RammboeckeAnzahl.ToString());
                                        await Rndm.Sleep(100, 200);
                                        browser.WebDriver.FindElement(By.XPath("//input[@name='train[" + counter + "][catapult]']")).Clear();
                                        browser.WebDriver.FindElement(By.XPath("//input[@name='train[" + counter++ + "][catapult]']")).SendKeys(begleit.KatapulteAnzahl.ToString());
                                        await Rndm.Sleep(100, 200);
                                    }
                            }
                        }
                    }
                    if (!string.IsNullOrEmpty(errors) && errors.Contains(Resources.BotcaptchaError) || string.IsNullOrEmpty(errors) &&
                        (truppenbewegung.Model.Abschickzeitpunkt.AddSeconds(-1) > DateTime.Now ||
                         !truppenbewegung.GenauTimenIsActive)) {
                        var rnd = new CryptoRandom();
	                    StringBuilder builder = new StringBuilder();
                        if (!truppenbewegung.GenauTimenIsActive)
                            truppenbewegung.Model.Abschickzeitpunkt = truppenbewegung.Model.Abschickzeitpunkt.AddMilliseconds(rnd.Next(-300, 800));

                        if (browser.WebDriver is IJavaScriptExecutor js) {
                            js.ExecuteScript("$('#chat-wrapper').hide();");
                            js.ExecuteScript("var p=window.XMLHttpRequest.prototype; p.open=p.send=p.setRequestHeader=function(){};");
                            js.ExecuteScript("$('#side-notification-container').hide();");

                            var anzahl = 0;
                            DateTime dateTime;
                            do {
                                anzahl++;
                                try {
                                    dateTime = DateTimeHelper.UnixTimeStampToDateTime((double)js.ExecuteScript("return Timing.getCurrentServerTime()"));
                                } catch {
                                    dateTime = DateTime.MinValue;
                                    App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) +
                                                         " timestamp failure");
                                }
                            } while (dateTime == DateTime.MinValue && anzahl < 10);
                            if (dateTime == DateTime.MinValue) {
                                truppenbewegung.Queued = "N";
                                truppenbewegung.Active = "N";
                                truppenbewegung.Model.GetsQueued = false;
                                truppenbewegung.Fehlermeldungen = "ERROR Attacking: timestamp 0";
                                App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) +
                                                     ": " + " NOT SENT, " + errors + " Datetime null, timestamp");
                            } else {
                                try {
	                                builder.Append("var start = performance.now();" + "var xInterval = setInterval(function(){if (performance.now() - start >=");
	                                builder.Append((int)((truppenbewegung.Rausstellen ? truppenbewegung.Model.Abschickzeitpunkt.AddSeconds(-10) : truppenbewegung.Model.Abschickzeitpunkt) - dateTime).TotalMilliseconds + App.Settings.Difference);
	                                builder.Append("){ document.getElementById('troop_confirm_go').click();clearInterval(xInterval);} }, 1);");
                                    js.ExecuteScript( builder.ToString());
									
                                    truppenbewegung.Queued = "J";
                                    truppenbewegung.GetsQueued = false;
                                    //var timespantosend = (truppenbewegung.Rausstellen ? truppenbewegung.Model.Abschickzeitpunkt.AddSeconds(-10) : truppenbewegung.Model.Abschickzeitpunkt) - DateTime.Now;
                                    App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) +
                                                         ": " + Resources.Attackpreparation + " " + Resources.Ended + ", " + Resources.From + " " + truppenbewegung.Model.SenderDorf.Name + " " + Resources.To + " " +
                                                         truppenbewegung.Model.EmpfaengerDorf.Name + " " + Resources.Departure + ": " + truppenbewegung.Abschickzeitpunkt + " " + Resources.Arrival + ": " + truppenbewegung.Ankunftszeitpunkt);
                                    await Task.Delay((int) ((truppenbewegung.Rausstellen ? truppenbewegung.Model.Abschickzeitpunkt.AddSeconds(-10) : truppenbewegung.Model.Abschickzeitpunkt) - dateTime).TotalMilliseconds + /*App.Settings.Difference + */1000);
                                    if (truppenbewegung.Rausstellen)
                                        try {
                                            browser.WebDriver.WaitForAjax();
                                            await Task.Delay(20000);
                                            var elementlist = browser.WebDriver.FindElement(By.XPath("//div[@id='commands_outgoings']")).FindElements(By.CssSelector("tr")).ToList();
                                            var el = elementlist.Last(x => x.Text.Contains(truppenbewegung.EmpfaengerX.ToString() + "|" + truppenbewegung.EmpfaengerY.ToString()) && x.FindElements(By.XPath("//a[@class='command-cancel']")).Count > 0);
                                            browser.WebDriver.Navigate().GoToUrl(el.FindElements(By.CssSelector("a"))[0].GetAttribute("href"));

                                            browser.WebDriver.WaitForPageload();
                                            js.ExecuteScript("$('#chat-wrapper').hide();");
                                            browser.ClickByXpath("//a[contains(@href,'&action=cancel')]");
                                            App.LogString(
                                                                 (!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + logtext + " rescued!");
                                        } catch (Exception e) {
                                            App.LogString(
                                                                 (!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + e);
                                        }
                                    browser.GetsUsed = false;
                                    BrowserManager.BrowserManager.RemoveBrowser(browser);
                                } catch (Exception e) {
                                    if (!string.IsNullOrEmpty(truppenbewegung.Model.Angriffsinformationen) && truppenbewegung.Model.Angriffsinformationen.Contains("Deff") && !truppenbewegung.GenauTimenIsActive && string.IsNullOrEmpty(errors)) {
                                        truppenbewegung.Fehlermeldungen = "ERROR Attacking: setup started too late";
                                        App.LogString(
                                                             (!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + " NOT SENT, ERROR ATTACKING: setup started too late, EXCEPTION: " + e);
                                    }
                                    browser.GetsUsed = false;
                                    BrowserManager.BrowserManager.RemoveBrowser(browser);
                                }
                                truppenbewegung.Sent = "J";
                                if (!truppenbewegung.Rausstellen)
                                    App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) +
                                                         ": " + logtext + " sent!");
                                truppenbewegung.Queued = "N";
                                truppenbewegung.Active = "N";
                            }
                        }
                    } else {
                        truppenbewegung.Queued = "N";
                        truppenbewegung.Active = "N";
                        truppenbewegung.Model.GetsQueued = false;
                        truppenbewegung.Fehlermeldungen = "ERROR Attacking: " + errors;
                        App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " +
                                             " NOT SENT, " + errors + (truppenbewegung.Model.Abschickzeitpunkt.AddSeconds(-1) > DateTime.Now ? " setup started too late" : ""));
                    }
                }
            } catch (Exception e) {
                truppenbewegung.Queued = "N";
                truppenbewegung.Active = "N";
                truppenbewegung.Model.GetsQueued = false;
                App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " +
                                     " NOT SENT, " + e.Message);
            }
        }
    }
}
