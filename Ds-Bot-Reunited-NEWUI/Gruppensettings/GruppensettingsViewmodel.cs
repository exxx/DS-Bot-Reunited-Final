﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using Ds_Bot_Reunited_NEWUI.Accountsettings.Ds_Bot_Reunited.Accountsettings;
using Ds_Bot_Reunited_NEWUI.Annotations;
using Ds_Bot_Reunited_NEWUI.BrowserManager;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.Properties;
using Ds_Bot_Reunited_NEWUI.Spieldaten;
using Framework.UI.Controls;

namespace Ds_Bot_Reunited_NEWUI.Gruppensettings {
    [Serializable]
    public class GruppensettingsViewModel : INotifyPropertyChanged {
        public System.Windows.Window Window { get; set; }
        private ICommand _addCommand;
        private ICommand _removeCommand;
        private ICommand _copyCommand;

        private ICollectionView _gruppen;
        private GruppesettingsViewModel _selectedGruppe;
        private GruppesettingsViewModel _selectedTemplateGruppe;
        private readonly AsyncObservableCollection<GruppesettingsViewModel> _gruppenList;
        private object GruppenLock { get; set; } = new object();
        private ICommand _readGroupsCommand;

        public ICollectionView Gruppen {
            get => _gruppen;
            set {
                _gruppen = value;
                OnPropertyChanged();
            }
        }

        public AsyncObservableCollection<GruppesettingsViewModel> SelectedGruppen { get; set; }


        public GruppesettingsViewModel SelectedGruppe {
            get => _selectedGruppe;
            set {
                if (value == null)
                    return;
                _selectedGruppe = value;
                OnPropertyChanged();
            }
        }

        public GruppesettingsViewModel SelectedTemplateGruppe {
            get => _selectedTemplateGruppe;
            set {
                _selectedTemplateGruppe = value;
                OnPropertyChanged();
            }
        }

        public ICommand AddCommand => _addCommand ?? (_addCommand = new RelayCommand(Add));
        public ICommand RemoveCommand => _removeCommand ?? (_removeCommand = new RelayCommand(Remove));
        public ICommand CopyCommand => _copyCommand ?? (_copyCommand = new RelayCommand(Copy));

        public ICommand ReadGroupsCommand
            => _readGroupsCommand ?? (_readGroupsCommand = new RelayCommand(async () => { await GruppeEinlesen(); }));

        public GruppensettingsViewModel(System.Windows.Window window) {
            _gruppenList = new AsyncObservableCollection<GruppesettingsViewModel>();
            foreach (var gruppe in App.Settings.Gruppen) {
                _gruppenList.Add(new GruppesettingsViewModel(gruppe, window));
            }

            Gruppen = CollectionViewSource.GetDefaultView(_gruppenList);

            if (_gruppenList.Any()) {
                SelectedGruppe = _gruppenList.First();
            }
            SelectedGruppen = new AsyncObservableCollection<GruppesettingsViewModel>();
            BindingOperations.EnableCollectionSynchronization(_gruppenList,
                GruppenLock);
            Window = window;
        }

        private async Task GruppeEinlesen() {
            await Task.Run(async() => {
                try {
	                bool vorher = App.Active;
                    if (App.Active)
                        App.Active = !App.Active;
                    Accountsetting account = App.Settings.Accountsettingslist.FirstOrDefault();
                    if (account != null) {
                        Browser browser = await BrowserManager.BrowserManager.GetFreeBrowser(account, false, false, true);
                        var list = await GruppenDorflisteUpdater.UpdateDoerferGruppen(browser,
                                                                                                      App.Settings.Accountsettingslist.FirstOrDefault());
                        foreach (var gruppe in list) {
                            if (!App.Settings.Gruppen.Any(x => x.Gruppenname.Equals(gruppe.Gruppenname))) {
                                App.Settings.Gruppen.Add(gruppe);
                            }
                            else if (App.Settings.Gruppen.Any(x => x.Gruppenname.Equals(gruppe.Gruppenname))) {
                                var grp =
                                    App.Settings.Gruppen.FirstOrDefault(x => x.Gruppenname.Equals(gruppe.Gruppenname));
	                            if (grp != null) {
		                            grp.DoerferIds.Clear();
		                            foreach (var id in gruppe.DoerferIds) {
			                            if (!grp.DoerferIds.Any(x => x.Equals(id))) {
				                            grp.DoerferIds.Add(id);
			                            }
		                            }
	                            }
                            }
                        }
                        BrowserManager.BrowserManager.RemoveBrowser(browser);
                    }
                    if (vorher)
                        App.Active = true;
                } catch (Exception ex) {
                    App.LogString(ex.Message + "\n");
                }
            });
            await MessageDialog.ShowAsync(Resources.ReopenWIndow, Resources.Achtung, MessageBoxButton.OK,
                MessageDialogType.Accent,
                App.Window);
        }


        private void Add() {
            var model = new GruppeDoerfer();
            App.Settings.Gruppen.Add(model);
            _gruppenList.Add(new GruppesettingsViewModel(model, Window));
        }

        private async void Remove() {
            if (await MessageDialog.ShowAsync("", Resources.ReallyDeleteGroup, MessageBoxButton.YesNo,
                    MessageDialogType.Accent,
                    Window) == MessageBoxResult.No)
                return;

            if (SelectedGruppe != null) {
                App.Settings.Gruppen.Remove(SelectedGruppe.GruppenSettings);
                _gruppenList.Remove(SelectedGruppe);
            }
        }

        private void Copy() {
            if (SelectedGruppen != null && SelectedTemplateGruppe != null) {
                foreach (var gruppe in SelectedGruppen) {
                    if (!gruppe.Name.Equals(SelectedTemplateGruppe.Name)) {
                            gruppe.Doerfer.Clear();
                            gruppe.GruppenSettings.DoerferIds.Clear();
                            foreach (var dorf in SelectedTemplateGruppe.Doerfer) {
                                if (!gruppe.Doerfer.Any(x => x.Dorf.VillageId.Equals(dorf.Dorf.VillageId))) {
                                    gruppe.Doerfer.Add(dorf);
                                    gruppe.GruppenSettings.DoerferIds.Add(dorf.Dorf.VillageId);
                                }
                            }
                    }
                }
                OnPropertyChanged("");
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}