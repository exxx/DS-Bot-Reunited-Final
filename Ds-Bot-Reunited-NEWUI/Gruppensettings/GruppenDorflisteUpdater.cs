﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Threading.Tasks;
using Ds_Bot_Reunited_NEWUI.Accountsettings.Ds_Bot_Reunited.Accountsettings;
using Ds_Bot_Reunited_NEWUI.Botcaptcha;
using Ds_Bot_Reunited_NEWUI.BrowserManager;
using Ds_Bot_Reunited_NEWUI.Constants;
using Ds_Bot_Reunited_NEWUI.Spieldaten;
using OpenQA.Selenium;

namespace Ds_Bot_Reunited_NEWUI.Gruppensettings {
    public class GruppenDorflisteUpdater {
        public static async Task<List<GruppeDoerfer>> UpdateDoerferGruppen(Browser browser, Accountsetting accountsettings) {
            browser.WebDriver.Navigate().Refresh();
            string link = App.Settings.OperateLink + "screen=overview_villages" + accountsettings.UvZusatzFuerLink + "&mode=combined";
            if (!browser.WebDriver.Url.Contains(link)) {
                browser.WebDriver.Navigate().GoToUrl(link);
            }
            try {
                await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings);
            } catch {
                Ping sender = new Ping();
                PingReply result = sender.Send("www.google.de");
                while (result != null && result.Status != IPStatus.Success) {
                    result = sender.Send("www.google.de");
                    App.LogString("Network Timeout");
                    await Task.Delay(2000);
                }
                browser.WebDriver.Navigate().Refresh();
                await Task.Delay(500);
            }
            List<GruppeDoerfer> list = new List<GruppeDoerfer>();
            var element = browser.WebDriver.FindElement(By.ClassName("vis_item"));
            try {
                var allegruppeelement = element.FindElement(By.PartialLinkText(NAMECONSTANTS.GetNameForServer(Names.Alle)));
                if (allegruppeelement != null && allegruppeelement.TagName.Contains("a")) {
                    allegruppeelement.Click();
                    await Task.Delay(200);
                }
            } catch (Exception) {
                App.LogString("Gruppe schon ausgewählt!");
            }
            if (browser.WebDriver.FindElements(By.Id("combined_table")).Any()) {
                var table2 = browser.WebDriver.FindElement(By.Id("combined_table"));
                var tablerows2 = table2.FindElements(By.TagName("tr")).ToList();
                GruppeDoerfer gruppe2 = new GruppeDoerfer {Gruppenname = NAMECONSTANTS.GetNameForServer(Names.Alle), DoerferIds = new List<int>()};
                for (var i = 1; i < tablerows2.Count; i++) {
                    var row = tablerows2[i];
                    var villageid = int.Parse(row.FindElement(By.ClassName("quickedit-vn")).GetAttribute("data-id"));
                    if (!gruppe2.DoerferIds.Any(x => x.Equals(villageid))) {
                        gruppe2.DoerferIds.Add(villageid);
                    }
                }
                list.Add(gruppe2);
                List<string> gruppen = browser.WebDriver.FindElements(By.XPath("//a[@class='group_tooltip group-menu-item']")).Select(x => x.GetAttribute("href")).ToList();
                foreach (var grp in gruppen) {
                    browser.WebDriver.Navigate().GoToUrl(grp + "page=-1&");
                    await Task.Delay(500);

                    GruppeDoerfer gruppe = new GruppeDoerfer {Gruppenname = browser.WebDriver.FindElement(By.TagName("strong"))?.Text.Replace(">", "").Replace("<", "")};
                    await Task.Delay(300);
                    //group=29527&

                    int indexOfGroup = grp.LastIndexOf("group=", StringComparison.CurrentCultureIgnoreCase);

                    int gruppenid;
                    int.TryParse(grp.Substring(indexOfGroup + 6, grp.Length - indexOfGroup - 7), out gruppenid);
                    gruppe.GruppenId = gruppenid;
                    gruppe.DoerferIds = new List<int>();
                    list.Add(gruppe);
                    try {
                        var table = browser.WebDriver.FindElement(By.Id("combined_table"));
                        var tablerows = table.FindElements(By.TagName("tr")).ToList();
                        for (var i = 1; i < tablerows.Count; i++) {
                            var row = tablerows[i];
                            var villageid = int.Parse(row.FindElement(By.ClassName("quickedit-vn")).GetAttribute("data-id"));
                            if (!gruppe.DoerferIds.Any(x => x.Equals(villageid))) {
                                gruppe.DoerferIds.Add(villageid);
                            }
                        }
                    } catch {
                        App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + " Keine Dörfer in Gruppe " + gruppe.Gruppenname + " vorhanden!");
                    }
                }
            }
            return list;
        }
    }
}
