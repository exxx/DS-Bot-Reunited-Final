﻿using System.Windows;
using System.Windows.Controls;

namespace Ds_Bot_Reunited_NEWUI.Gruppensettings {
    public partial class GruppensettingsView {
        public GruppensettingsView(Window owner, GruppensettingsViewModel datacontext) {
            Owner = owner;
            DataContext = datacontext;
            InitializeComponent();
        }

        private void Save(object sender, RoutedEventArgs e) {
            Close();
        }

        private void MyGrid_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            if ((DataContext as GruppensettingsViewModel)?.SelectedGruppen != null) {
                ((GruppensettingsViewModel) DataContext).SelectedGruppen?.Clear();
                foreach (var item in DataGrid.SelectedItems)
                    ((GruppensettingsViewModel) DataContext).SelectedGruppen?.Add(item as GruppesettingsViewModel);
            }
        }
    }
}
