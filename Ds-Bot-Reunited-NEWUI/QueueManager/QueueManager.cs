﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ds_Bot_Reunited_NEWUI.Accountsettings.Ds_Bot_Reunited.Accountsettings;
using Ds_Bot_Reunited_NEWUI.ActionModul;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;

namespace Ds_Bot_Reunited_NEWUI.QueueManager {
	public static class QueueManager {
		public static void UpdateQueueForAccount(Accountsetting account) {
			if (account.LoginActive) {
				List<ActionModulViewModel> actions = new List<ActionModulViewModel>();
				if (App.Settings.DetectActive) {
					var action = DefendAddToQueue(account);
					if (action != null) {
						actions.Add(action);
						account.DefendQueued = true;
						account.PleaseQueueDefend = false;
					}
				}

				if (account.CanLoginAndDoSomething && account.HandelnActive) {
					var action = HandelAddToQueue(account);
					if (action != null) {
						actions.Add(action);
						account.HandelQueued = true;
					}
				}

				if (account.CanLoginAndDoSomething && account.BauenActive) {
					var action = BauenAddToQueue(account);
					if (action != null) {
						actions.Add(action);
						account.BauenQueued = true;
					}
				}

				if (account.CanLoginAndDoSomething && account.RekrutierenActive) {
					var action = RekrutierenAddToQueue(account);
					if (action != null) {
						actions.Add(action);
						account.RekrutierenQueued = true;
					}
				}

				if (account.CanLoginAndDoSomething && account.FarmenActive) {
					var action = FarmenAddToQueue(account);
					if (action != null) {
						actions.Add(action);
						account.FarmenQueued = true;
					}
				}

				if (account.CanLoginAndDoSomething && account.GoldCoinsActive) {
					var action = MuenzenAddToQueue(account);
					if (action != null) {
						actions.Add(action);
						account.GoldCoinsQueued = true;
					}
				}

				if (account.CanLoginAndDoSomething && account.ForschenActive) {
					var action = ForschenAddToQueue(account);
					if (action != null) {
						actions.Add(action);
						account.ForschenQueued = true;
					}
				}

				if (actions.Any()) {
					foreach (var action in actions.OrderBy(x => x.Ausfuehrungszeitpunkt)) {
						App.PriorityQueue.Add(action);
					}
				}
			}
		}

		private static ActionModulViewModel DefendAddToQueue(Accountsetting account) {
			if (!account.DefendQueued &&
			    (account.PleaseQueueDefend || !account.RekrutierenActive && !account.HandelnActive &&
			     !account.BauenActive && !account.FarmenActive && !account.GoldCoinsActive) && account.OverallActive &&
			    CanAddToPriorityQueue(ActionArten.Defend.GetDescription(), account.Accountname, account)) {
				//naechste Gelegenheit wird herausgesucht
				DateTime naechsteGelegenheit = DateTime.MinValue;

				//float priority = GetPrioritaetNachZeit(naechsteGelegenheit);
				return new ActionModulViewModel(new ActionModul.ActionModul(ActionArten.Defend,
				                                                            account.Accountname,
				                                                            NaechsteAusfuehrungBerechnen(App.Settings.LastAttackdetectionZeitpunkt
				                                                                                         ,
				                                                                                         naechsteGelegenheit
				                                                                                         ,
				                                                                                         App.Settings
				                                                                                            .AttackdetectionInterval
				                                                                                         ,
				                                                                                         App.Settings
				                                                                                            .RandomIntervalMax
				                                                                                         ,
				                                                                                         App.Settings
				                                                                                            .RandomIntervalMin)
				                                                            , "0"));
			}

			return null;
		}

		private static ActionModulViewModel ForschenAddToQueue(Accountsetting account) {
			if (!account.RekrutierenQueued && account.OverallActive &&
			    CanAddToPriorityQueue(ActionArten.Forge.GetDescription(), account.Accountname, account)) {
				//naechste Gelegenheit wird herausgesucht
				DateTime naechsteGelegenheit = DateTime.MinValue;

				//float priority = GetPrioritaetNachZeit(naechsteGelegenheit);
				return new ActionModulViewModel(new ActionModul.ActionModul(ActionArten.Forge,
				                                                            account.Accountname,
				                                                            NaechsteAusfuehrungBerechnen(account.LastForschenZeitpunkt
				                                                                                         ,
				                                                                                         naechsteGelegenheit
				                                                                                         ,
				                                                                                         account
					                                                                                         .Forscheninterval
				                                                                                         ,
				                                                                                         App.Settings
				                                                                                            .RandomIntervalMax
				                                                                                         ,
				                                                                                         App.Settings
				                                                                                            .RandomIntervalMin)
				                                                            , "0"));
			}

			return null;
		}

		private static ActionModulViewModel RekrutierenAddToQueue(Accountsetting account) {
			if (!account.RekrutierenQueued && account.OverallActive &&
			    CanAddToPriorityQueue(ActionArten.Recruiting.GetDescription(), account.Accountname, account)) {
				//naechste Gelegenheit wird herausgesucht
				DateTime naechsteGelegenheit = DateTime.MinValue;

				//float priority = GetPrioritaetNachZeit(naechsteGelegenheit);
				return new ActionModulViewModel(new ActionModul.ActionModul(ActionArten.Recruiting,
				                                                            account.Accountname,
				                                                            NaechsteAusfuehrungBerechnen(account.LastRekrutierenZeitpunkt
				                                                                                         ,
				                                                                                         naechsteGelegenheit
				                                                                                         ,
				                                                                                         account
					                                                                                         .Rekrutiereninterval
				                                                                                         ,
				                                                                                         App.Settings
				                                                                                            .RandomIntervalMax
				                                                                                         ,
				                                                                                         App.Settings
				                                                                                            .RandomIntervalMin)
				                                                            , "0"));
			}

			return null;
		}

		private static ActionModulViewModel HandelAddToQueue(Accountsetting account) {
			if (!account.HandelQueued && account.OverallActive &&
			    CanAddToPriorityQueue(ActionArten.Trading.GetDescription(), account.Accountname, account)) {
				//naechste Gelegenheit wird herausgesucht
				DateTime naechsteGelegenheit = DateTime.MinValue;

				//float priority = GetPrioritaetNachZeit(naechsteGelegenheit);
				return new ActionModulViewModel(new ActionModul.ActionModul(ActionArten.Trading,
				                                                            account.Accountname,
				                                                            NaechsteAusfuehrungBerechnen(account.LastHandelZeitpunkt
				                                                                                         ,
				                                                                                         naechsteGelegenheit
				                                                                                         ,
				                                                                                         account
					                                                                                         .Handelninterval
				                                                                                         ,
				                                                                                         App.Settings
				                                                                                            .RandomIntervalMax
				                                                                                         ,
				                                                                                         App.Settings
				                                                                                            .RandomIntervalMin)
				                                                            , "0"));
			}

			return null;
		}

		private static ActionModulViewModel BauenAddToQueue(Accountsetting account) {
			if (!account.BauenQueued && account.OverallActive &&
			    CanAddToPriorityQueue(ActionArten.Building.GetDescription(), account.Accountname, account)) {
				//naechste Gelegenheit wird herausgesucht
				DateTime naechsteGelegenheit = DateTime.MinValue;
				if (account.NextAvailableBauschleife > naechsteGelegenheit)
					naechsteGelegenheit = account.NextAvailableBauschleife;

				//float priority = GetPrioritaetNachZeit(naechsteGelegenheit);
				return new ActionModulViewModel(new ActionModul.ActionModul(ActionArten.Building,
				                                                            account.Accountname,
				                                                            NaechsteAusfuehrungBerechnen(account.LastBauenZeitpunkt
				                                                                                         ,
				                                                                                         naechsteGelegenheit
				                                                                                         ,
				                                                                                         account
					                                                                                         .Baueninterval
				                                                                                         ,
				                                                                                         App.Settings
				                                                                                            .RandomIntervalMax
				                                                                                         ,
				                                                                                         App.Settings
				                                                                                            .RandomIntervalMin)
				                                                            , "0"));
			}

			return null;
		}

		private static ActionModulViewModel FarmenAddToQueue(Accountsetting account) {
			if (!account.FarmenQueued && account.OverallActive &&
			    CanAddToPriorityQueue(ActionArten.Farming.GetDescription(), account.Accountname, account)) {
				//naechste Gelegenheit wird herausgesucht
				DateTime naechsteGelegenheit = DateTime.MinValue;
				if (account.NextAvailableTroopAtHomeAt > naechsteGelegenheit)
					naechsteGelegenheit = account.NextAvailableTroopAtHomeAt;

				//float priority = GetPrioritaetNachZeit(naechsteGelegenheit);
				return new ActionModulViewModel(new ActionModul.ActionModul(ActionArten.Farming,
				                                                            account.Accountname,
				                                                            NaechsteAusfuehrungBerechnen(account.LastFarmenZeitpunkt
				                                                                                         ,
				                                                                                         naechsteGelegenheit
				                                                                                         ,
				                                                                                         account
					                                                                                         .Farmeninterval
				                                                                                         ,
				                                                                                         App.Settings
				                                                                                            .RandomIntervalMax
				                                                                                         ,
				                                                                                         App.Settings
				                                                                                            .RandomIntervalMin)
				                                                            , "0"));
			}

			return null;
		}

		private static ActionModulViewModel MuenzenAddToQueue(Accountsetting account) {
			if (!account.GoldCoinsQueued && account.OverallActive &&
			    CanAddToPriorityQueue(ActionArten.Coins.GetDescription(), account.Accountname, account)) {
				//naechste Gelegenheit wird herausgesucht
				DateTime naechsteGelegenheit = DateTime.MinValue;
				if (account.NextAvailableTroopAtHomeAt > naechsteGelegenheit)
					naechsteGelegenheit = account.NextAvailableTroopAtHomeAt;

				//float priority = GetPrioritaetNachZeit(naechsteGelegenheit);
				return new ActionModulViewModel(new ActionModul.ActionModul(ActionArten.Farming,
				                                                            account.Accountname,
				                                                            NaechsteAusfuehrungBerechnen(account.LastFarmenZeitpunkt
				                                                                                         ,
				                                                                                         naechsteGelegenheit
				                                                                                         ,
				                                                                                         account
					                                                                                         .Farmeninterval
				                                                                                         ,
				                                                                                         App.Settings
				                                                                                            .RandomIntervalMax
				                                                                                         ,
				                                                                                         App.Settings
				                                                                                            .RandomIntervalMin)
				                                                            , "0"));
			}

			return null;
		}

		public static void Enqueue(ActionArten art
		                           , string accountname
		                           , DateTime ausfuehrungszeitpunkt
		                           , string villageid) {
			float priority = GetPrioritaetNachZeit(ausfuehrungszeitpunkt);
			App.PriorityQueue.Add(new ActionModulViewModel(new ActionModul.ActionModul(art, accountname
			                                                                           , ausfuehrungszeitpunkt
			                                                                           , villageid) {
				                                                                                        Priorityy =
					                                                                                        priority
			                                                                                        }));
		}

		public static ActionModul.ActionModul Dequeue() {
			if (App.PriorityQueue.OrderBy(x => x.Ausfuehrungszeitpunkt).FirstOrDefault()?.Ausfuehrungszeitpunkt <
			    DateTime.Now.AddMinutes(1)) {
				if (App.PriorityQueue.OrderBy(x => x.Ausfuehrungszeitpunkt).FirstOrDefault() != null) {
					ActionModulViewModel allob =
						App.PriorityQueue.OrderBy(x => x.Ausfuehrungszeitpunkt).FirstOrDefault();
					if (allob != null) {
						App.PriorityQueue.Remove(allob);
						return allob.ActionModul;
					}
				}
			}

			return null;
		}

		public static bool AnyForThisAccountInTheNext10Minutes(string accountname) {
			return App.PriorityQueue.Any(x => x.Accountname.Equals(accountname) &&
			                                  x.Ausfuehrungszeitpunkt < DateTime.Now.AddMinutes(9));
		}

		public static void EnqueueOldActionModul(ActionModul.ActionModul actionModul) {
			App.PriorityQueue.Add(new ActionModulViewModel(actionModul));
		}

		private static bool CanAddToPriorityQueue(string art, string accountname, Accountsetting account) {
			if (!App.PriorityQueue.Any(x => x.AktionArt.Equals(art) && x.Accountname.Equals(accountname))) {
				return true;
			}

			return false;
		}

		private static DateTime NaechsteAusfuehrungBerechnen(DateTime letzteAusfuehrung
		                                                     , DateTime naechsteGelegenheit
		                                                     , int interval
		                                                     , int randomintervalMax
		                                                     , int randomintervalMin) {
			DateTime now = DateTime.Now;
			TimeSpan ts = now - letzteAusfuehrung;
			if (ts.TotalMinutes < interval) {
				int unterschied = interval - (int) ts.TotalMinutes;
				unterschied += App.Random.Next(randomintervalMin, randomintervalMax);
				if (now.AddMinutes(unterschied) < naechsteGelegenheit) {
					return naechsteGelegenheit;
				}

				return now.AddMinutes(unterschied);
			}

			return DateTime.Now;
		}

		private static bool CanAusfuehren(int interval, DateTime farmenZeitpunkt) {
			DateTime now = DateTime.Now;
			if (farmenZeitpunkt.AddMinutes(interval) < now) {
				return true;
			}

			return false;
		}

		private static float GetPrioritaetNachZeit(DateTime ausfuehrung) {
			float fl = 0;
			if (App.PriorityQueue != null && App.PriorityQueue.Any()) {
				var actionModulViewModel = App.PriorityQueue.LastOrDefault(x => x.Ausfuehrungszeitpunkt > ausfuehrung);
				if (actionModulViewModel != null) fl = (float) (actionModulViewModel.Priority - 0.01);
			}

			if (fl < 0) {
				fl = 0;
			}

			return fl;
		}
	}
}