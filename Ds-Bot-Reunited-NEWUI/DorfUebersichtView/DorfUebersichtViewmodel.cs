﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Data;
using Ds_Bot_Reunited_NEWUI.Annotations;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.Settings;

namespace Ds_Bot_Reunited_NEWUI.DorfUebersichtView {
    public class DorfUebersichtViewModel : INotifyPropertyChanged {
        private AsyncObservableCollection<AccountDorfSettings> _doerfer;
        private ICollectionView _doerferView;
        private bool _progressRingActive;
        private int _seite;

        public DorfUebersichtViewModel() {
            Doerfer = new AsyncObservableCollection<AccountDorfSettings>();
            var xx = App.Settings.Accountsettingslist.Select(x => x.DoerferSettings).ToList();
            if (Seiten == null)
                Seiten = new List<int>();
            foreach (var list in xx) {
                var count = list.Count;
                if (count > 50 && _seite == 0)
                    for (var i = 1; i <= count / 50; i++)
                        Seiten.Add(i);
                else
                    Seiten.Add(1);
            }
            Seite = 1;
            BindingOperations.EnableCollectionSynchronization(Doerfer, DoerferLock);
            DoerferView = CollectionViewSource.GetDefaultView(Doerfer);
        }

        public List<int> Seiten { get; set; }
        public object DoerferLock { get; set; } = new object();

        public int Seite {
            get => _seite;
            set {
                _seite = value;
#pragma warning disable 4014
                FillListe();
#pragma warning restore 4014
                OnPropertyChanged();
            }
        }

        public AsyncObservableCollection<AccountDorfSettings> Doerfer {
            get => _doerfer;
            set {
                _doerfer = value;
                OnPropertyChanged();
            }
        }


        public ICollectionView DoerferView {
            get => _doerferView;
            set {
                _doerferView = value;
                OnPropertyChanged();
            }
        }

        public bool ProgressRingActive {
            get => _progressRingActive;
            set {
                _progressRingActive = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private async Task FillListe() {
            await Task.Run(() => {
                               ProgressRingActive = true;
                               Doerfer.Clear();
                               var xx = App.Settings.Accountsettingslist.Select(x => x.DoerferSettings).ToList();
                               foreach (var list in xx) {
                                   if (_seite == 0)
                                       _seite = 1;
                                   var startindex = 50 * (_seite - 1);
                                   foreach (var dorf in list.OrderBy(k => k.Dorf.Name).Skip(startindex).Take(50))
                                       Doerfer.Add(dorf);
                               }
                               ProgressRingActive = false;
                           });
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
