﻿using System.Windows;

namespace Ds_Bot_Reunited_NEWUI.DorfUebersichtView {
    public partial class DorfUebersichtView {
        public DorfUebersichtView(Window owner, DorfUebersichtViewModel datacontext) {
            Owner = owner;
            DataContext = datacontext;
            InitializeComponent();
        }

        private void Save(object sender, RoutedEventArgs e) {
            Close();
        }
    }
}
