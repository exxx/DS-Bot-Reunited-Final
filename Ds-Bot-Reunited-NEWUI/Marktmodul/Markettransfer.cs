﻿using System;
using Ds_Bot_Reunited_NEWUI.SpieldatenViewModel;

namespace Ds_Bot_Reunited_NEWUI.Marktmodul {
    [Serializable]
    public class Markettransfer {
        public DorfViewModel SenderVillage { get; set; }
        public DorfViewModel EmpfaengerVillage { get; set; }
        public int Holz { get; set; }
        public int Lehm { get; set; }
        public int Eisen { get; set; }
        public DateTime Datum { get; set; }

        public Markettransfer(DorfViewModel sender, DorfViewModel empfaenger, int holz, int lehm, int eisen, DateTime datum) {
            SenderVillage = sender;
            EmpfaengerVillage = empfaenger;
            Holz = holz;
            Lehm = lehm;
            Eisen = eisen;
            Datum = datum;
        }

        public Markettransfer(DorfViewModel sender, DorfViewModel empfaenger, int holz, int lehm, int eisen) {
            SenderVillage = sender;
            EmpfaengerVillage = empfaenger;
            Holz = holz;
            Lehm = lehm;
            Eisen = eisen;
        }

        public Markettransfer(int holz, int lehm, int eisen, DateTime datum) {
            Holz = holz;
            Lehm = lehm;
            Eisen = eisen;
            Datum = datum;
        }

        public Markettransfer Clone() {
            return new Markettransfer(SenderVillage, EmpfaengerVillage, Holz, Lehm, Eisen, Datum);
        }
    }
}