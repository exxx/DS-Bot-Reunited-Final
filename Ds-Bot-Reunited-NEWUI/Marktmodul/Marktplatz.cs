﻿using System.Threading.Tasks;
using Ds_Bot_Reunited_NEWUI.Accountsettings.Ds_Bot_Reunited.Accountsettings;
using Ds_Bot_Reunited_NEWUI.Botcaptcha;
using Ds_Bot_Reunited_NEWUI.BrowserManager;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using OpenQA.Selenium;

namespace Ds_Bot_Reunited_NEWUI.Marktmodul {
    public class Marktplatz {
        public static async Task SendMarketTransfer(Markettransfer transfer, Browser browser, Accountsetting accountsettings) {
            if (transfer != null) {
                string url = App.Settings.OperateLink + "village=" + transfer?.SenderVillage.VillageId + "" + accountsettings.UvZusatzFuerLink + "&screen=market&mode=send";
                if (!browser.WebDriver.Url.Equals(url)) {
                    browser.WebDriver.Navigate().GoToUrl(url);
                    browser.WebDriver.WaitForPageload();
                }
                await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings, url);
                CryptoRandom rnd = new CryptoRandom();
                browser.WebDriver.FindElement(By.XPath("//input[@name='input']")).SendKeys(transfer.EmpfaengerVillage.XCoordinate.ToString() + transfer.EmpfaengerVillage.YCoordinate);
                browser.WebDriver.FindElement(By.XPath("//input[@name='wood']")).SendKeys(transfer.Holz.ToString());
                browser.WebDriver.FindElement(By.XPath("//input[@name='stone']")).SendKeys(transfer.Lehm.ToString());
                browser.WebDriver.FindElement(By.XPath("//input[@name='iron']")).SendKeys(transfer.Eisen.ToString());
                await Task.Delay(rnd.Next(200, 300));
                browser.WebDriver.FindElement(By.XPath("//input[@value='OK']")).Click();
                browser.WebDriver.WaitForPageload();
                browser.WebDriver.FindElement(By.XPath("//input[@value='OK']")).Click();
                browser.WebDriver.WaitForPageload();
            }
        }
    }
}
