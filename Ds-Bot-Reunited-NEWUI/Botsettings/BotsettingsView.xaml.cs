﻿using System.Windows;

namespace Ds_Bot_Reunited_NEWUI.Botsettings {
    public partial class BotsettingsView {
        public BotsettingsView(Window owner, BotsettingsViewModel datacontext) {
            Owner = owner;
            DataContext = datacontext;
            InitializeComponent();
        }

        private void Save(object sender, RoutedEventArgs e) {
            Close();
        }
    }
}
