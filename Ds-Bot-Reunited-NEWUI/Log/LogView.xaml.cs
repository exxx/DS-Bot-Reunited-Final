﻿using System.Windows;

namespace Ds_Bot_Reunited_NEWUI.Log {
    public partial class LogView {
        public LogView(Window owner) {
            Owner = owner;
            DataContext = new LogViewModel();
            InitializeComponent();
        }

        private void Save(object sender, RoutedEventArgs e) {
            this.Close();
        }
    }
}