﻿using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Ds_Bot_Reunited_NEWUI.Annotations;

namespace Ds_Bot_Reunited_NEWUI.Log {
	public class LogViewModel : INotifyPropertyChanged {
		private string _log;

		public string Log {
			get => _log;
			set {
				_log = value;
				OnPropertyChanged();
			}
		}

		public LogViewModel() {
			Task.Run(async () => {
				         StringBuilder builder = new StringBuilder();
				         for (;;) {
					         builder.Clear();
					         foreach (var eintrag in App.Log.OrderByDescending(x => x)) {
						         builder.Append(eintrag);
					         }

					         Log = builder.ToString();
					         await Task.Delay(5000);
				         }
			         });
		}

		public event PropertyChangedEventHandler PropertyChanged;

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}