﻿using System;
using System.Globalization;

namespace Ds_Bot_Reunited_NEWUI.Hilfsklassen {
    public static class DateTimeHelper {
        public static DateTime UnixTimeStampToDateTime(double unixTimeStamp) {
            try {
                // Unix timestamp is seconds past epoch

                System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);

                dtDateTime = dtDateTime.AddMilliseconds(unixTimeStamp).ToLocalTime();

                return dtDateTime;
            } catch {
                return DateTime.MinValue;
            }
        }

        public static double DateTimeToUnixTimeStam(DateTime datetime) {
            return (datetime - new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc).ToLocalTime()).TotalMilliseconds;
        }

	    public static string GetString(this DateTime datetime) { return datetime.ToString("dd.MM.yyyy HH:mm:ss.fff", CultureInfo.InvariantCulture); }
    }
}
