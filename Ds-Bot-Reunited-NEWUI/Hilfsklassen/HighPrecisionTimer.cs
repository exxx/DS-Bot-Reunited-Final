﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Ds_Bot_Reunited_NEWUI.Hilfsklassen {
    public class HighPrecisionTimer : IDisposable {

        public class TickEventArgs : EventArgs {

            public TimeSpan Duration { get; private set; }

            public long TotalTicks { get; private set; }



            public TickEventArgs(TimeSpan totalDuration, long totalTicks) {

                this.Duration = totalDuration;

                this.TotalTicks = totalTicks;

            }

        }



        protected CancellationTokenSource cancelSource;



        public HighPrecisionTimer(DateTime date, Action action) {



            if (date < HighResolutionDateTime.UtcNow)

                throw new ArgumentOutOfRangeException();



            cancelSource = new CancellationTokenSource();





            var watch = System.Diagnostics.Stopwatch.StartNew();



            var task = new Task(() => {

                                    while (!this.cancelSource.IsCancellationRequested) {

                                        if (HighResolutionDateTime.UtcNow >= date) {

                                            action.Invoke();

                                            cancelSource.Cancel();

                                        }

                                        Thread.Sleep(1);

                                    }

                                }, cancelSource.Token, TaskCreationOptions.LongRunning);



            task.Start();

        }



        public void Dispose() {

            this.cancelSource.Cancel();

        }

    }
}
