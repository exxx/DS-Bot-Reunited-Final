﻿using System;
using System.Net;
using System.Net.Sockets;
using static System.Net.Dns;

namespace Ds_Bot_Reunited_NEWUI.Hilfsklassen {
    public enum LeapIndicator {
        NoWarning,		// 0 - No warning
        LastMinute61,	// 1 - Last minute has 61 seconds
        LastMinute59,	// 2 - Last minute has 59 seconds
        Alarm			// 3 - Alarm condition (clock not synchronized)
    }

    //Mode field values
    public enum Mode {
        SymmetricActive,	// 1 - Symmetric active
        SymmetricPassive,	// 2 - Symmetric pasive
        Client,				// 3 - Client
        Server,				// 4 - Server
        Broadcast,			// 5 - Broadcast
        Unknown				// 0, 6, 7 - Reserved
    }

    // Stratum field values
    public enum Stratum {
        Unspecified,			// 0 - unspecified or unavailable
        PrimaryReference,		// 1 - primary reference (e.g. radio-clock)
        SecondaryReference,		// 2-15 - secondary reference (via NTP or SNTP)
        Reserved				// 16-255 - reserved
    }

    /// <summary>
    /// NTPClient is a C# class designed to connect to time servers on the Internet.
    /// The implementation of the protocol is based on the RFC 2030.
    /// 
    /// Public class members:
    ///
    /// LeapIndicator - Warns of an impending leap second to be inserted/deleted in the last
    /// minute of the current day. (See the _LeapIndicator enum)
    /// 
    /// VersionNumber - Version number of the protocol (3 or 4).
    /// 
    /// Mode - Returns mode. (See the _Mode enum)
    /// 
    /// Stratum - Stratum of the clock. (See the _Stratum enum)
    /// 
    /// PollInterval - Maximum interval between successive messages.
    /// 
    /// Precision - Precision of the clock.
    /// 
    /// RootDelay - Round trip time to the primary reference source.
    /// 
    /// RootDispersion - Nominal error relative to the primary reference source.
    /// 
    /// ReferenceID - Reference identifier (either a 4 character string or an IP address).
    /// 
    /// ReferenceTimestamp - The time at which the clock was last set or corrected.
    /// 
    /// OriginateTimestamp - The time at which the request departed the client for the server.
    /// 
    /// ReceiveTimestamp - The time at which the request arrived at the server.
    /// 
    /// Transmit Timestamp - The time at which the reply departed the server for client.
    /// 
    /// RoundTripDelay - The time between the departure of request and arrival of reply.
    /// 
    /// LocalClockOffset - The offset of the local clock relative to the primary reference
    /// source.
    /// 
    /// Initialize - Sets up data structure and prepares for connection.
    /// 
    /// Connect - Connects to the time server and populates the data structure.
    /// 
    /// IsResponseValid - Returns true if received data is valid and if comes from
    /// a NTP-compliant time server.
    /// 
    /// ToString - Returns a string representation of the object.
    /// 
    /// -----------------------------------------------------------------------------
    /// Structure of the standard NTP header (as described in RFC 2030)
    ///                       1                   2                   3
    ///   0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
    ///  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    ///  |LI | VN  |Mode |    Stratum    |     Poll      |   Precision   |
    ///  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    ///  |                          Root Delay                           |
    ///  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    ///  |                       Root Dispersion                         |
    ///  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    ///  |                     Reference Identifier                      |
    ///  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    ///  |                                                               |
    ///  |                   Reference Timestamp (64)                    |
    ///  |                                                               |
    ///  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    ///  |                                                               |
    ///  |                   Originate Timestamp (64)                    |
    ///  |                                                               |
    ///  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    ///  |                                                               |
    ///  |                    Receive Timestamp (64)                     |
    ///  |                                                               |
    ///  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    ///  |                                                               |
    ///  |                    Transmit Timestamp (64)                    |
    ///  |                                                               |
    ///  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    ///  |                 Key Identifier (optional) (32)                |
    ///  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    ///  |                                                               |
    ///  |                                                               |
    ///  |                 Message Digest (optional) (128)               |
    ///  |                                                               |
    ///  |                                                               |
    ///  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    /// 
    /// -----------------------------------------------------------------------------
    /// 
    /// NTP Timestamp Format (as described in RFC 2030)
    ///                         1                   2                   3
    ///     0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
    /// +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    /// |                           Seconds                             |
    /// +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    /// |                  Seconds Fraction (0-padded)                  |
    /// +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    /// 
    /// </summary>

    public class NtpClient {
        // NTP Data Structure Length
        private const byte NTP_DATA_LENGTH = 48;
        // NTP Data Structure (as described in RFC 2030)
        byte[] _ntpData = new byte[NTP_DATA_LENGTH];

        // Offset constants for timestamps in the data structure
        private const byte OFF_REFERENCE_ID = 12;
        private const byte OFF_REFERENCE_TIMESTAMP = 16;
        private const byte OFF_ORIGINATE_TIMESTAMP = 24;
        private const byte OFF_RECEIVE_TIMESTAMP = 32;
        private const byte OFF_TRANSMIT_TIMESTAMP = 40;

        // Leap Indicator
        public LeapIndicator LeapIndicator {
            get {
                // Isolate the two most significant bits
                byte val = (byte)(_ntpData[0] >> 6);
                switch (val) {
                    case 0: return LeapIndicator.NoWarning;
                    case 1: return LeapIndicator.LastMinute61;
                    case 2: return LeapIndicator.LastMinute59;
                    case 3:
                    default:
                        return LeapIndicator.Alarm;
                }
            }
        }

        // Version Number
        public byte VersionNumber {
            get {
                // Isolate bits 3 - 5
                byte val = (byte)((_ntpData[0] & 0x38) >> 3);
                return val;
            }
        }

        // Mode
        public Mode Mode {
            get {
                // Isolate bits 0 - 3
                byte val = (byte)(_ntpData[0] & 0x7);
                switch (val) {
                    case 0:
                    case 6:
                    case 7:
                    default:
                        return Mode.Unknown;
                    case 1:
                        return Mode.SymmetricActive;
                    case 2:
                        return Mode.SymmetricPassive;
                    case 3:
                        return Mode.Client;
                    case 4:
                        return Mode.Server;
                    case 5:
                        return Mode.Broadcast;
                }
            }
        }

        // Stratum
        public Stratum Stratum {
            get {
                byte val = (byte)_ntpData[1];
                if (val == 0) return Stratum.Unspecified;
                else
                    if (val == 1) return Stratum.PrimaryReference;
                else
                        if (val <= 15) return Stratum.SecondaryReference;
                else
                    return Stratum.Reserved;
            }
        }

        // Poll Interval
        public uint PollInterval {
            get {
                return (uint)Math.Round(Math.Pow(2, _ntpData[2]));
            }
        }

        // Precision (in milliseconds)
        public double Precision {
            get {
                return 1000 * Math.Pow(2, _ntpData[3]);
            }
        }

        // Root Delay (in milliseconds)
        public double RootDelay {
            get {
                int temp = 0;
                temp = 256 * (256 * (256 * _ntpData[4] + _ntpData[5]) + _ntpData[6]) + _ntpData[7];
                return 1000 * ((double)temp / 0x10000);
            }
        }

        // Root Dispersion (in milliseconds)
        public double RootDispersion {
            get {
                int temp = 0;
                temp = 256 * (256 * (256 * _ntpData[8] + _ntpData[9]) + _ntpData[10]) + _ntpData[11];
                return 1000 * ((double)temp / 0x10000);
            }
        }

        // Reference Identifier
        public string ReferenceId {
            get {
                string val = "";

                switch (Stratum) {
                    case Stratum.Unspecified:
                    case Stratum.PrimaryReference:

                        val += System.Convert.ToChar(_ntpData[OFF_REFERENCE_ID + 0]);
                        val += System.Convert.ToChar(_ntpData[OFF_REFERENCE_ID + 1]);
                        val += System.Convert.ToChar(_ntpData[OFF_REFERENCE_ID + 2]);
                        val += System.Convert.ToChar(_ntpData[OFF_REFERENCE_ID + 3]);
                        break;
                    case Stratum.SecondaryReference:
                        switch (VersionNumber) {
                            case 3:	// Version 3, Reference ID is an IPv4 address
                                string address = _ntpData[OFF_REFERENCE_ID + 0] + "." +
                                    _ntpData[OFF_REFERENCE_ID + 1] + "." +
                                    _ntpData[OFF_REFERENCE_ID + 2] + "." +
                                    _ntpData[OFF_REFERENCE_ID + 3];
                                try {

                                    IPAddress refAddr = System.Net.IPAddress.Parse(address);
                                    IPHostEntry host = GetHostEntry(refAddr);
                                    val = host.HostName + " (" + address + ")";
                                } catch (Exception) {
                                    val = "N/A";
                                }
                                break;
                            case 4: // Version 4, Reference ID is the timestamp of last update
                                DateTime time = ComputeDate(GetMilliSeconds(OFF_REFERENCE_ID));
                                // Take care of the time zone
                                long offset = TimeZone.CurrentTimeZone.GetUtcOffset(DateTime.Now).Ticks;
                                TimeSpan offspan = TimeSpan.FromTicks(offset);
                                val = (time + offspan).ToString();
                                break;
                            default:
                                val = "N/A";
                                break;
                        }
                        break;
                }

                return val;
            }
        }

        // Reference Timestamp
        public DateTime ReferenceTimestamp {
            get {
                DateTime time = ComputeDate(GetMilliSeconds(OFF_REFERENCE_TIMESTAMP));
                // Take care of the time zone
                long offset = TimeZone.CurrentTimeZone.GetUtcOffset(DateTime.Now).Ticks;
                TimeSpan offspan = TimeSpan.FromTicks(offset);
                return time + offspan;
            }
        }

        // Originate Timestamp
        public DateTime OriginateTimestamp => ComputeDate(GetMilliSeconds(OFF_ORIGINATE_TIMESTAMP));

        // Receive Timestamp
        public DateTime ReceiveTimestamp {
            get {
                DateTime time = ComputeDate(GetMilliSeconds(OFF_RECEIVE_TIMESTAMP));
                // Take care of the time zone
                long offset = TimeZone.CurrentTimeZone.GetUtcOffset(DateTime.Now).Ticks;
                TimeSpan offspan = TimeSpan.FromTicks(offset);
                return time + offspan;
            }
        }

        // Transmit Timestamp
        public DateTime TransmitTimestamp {
            get {
                DateTime time = ComputeDate(GetMilliSeconds(OFF_TRANSMIT_TIMESTAMP));
                // Take care of the time zone
                long offset = TimeZone.CurrentTimeZone.GetUtcOffset(DateTime.Now).Ticks;
                TimeSpan offspan = TimeSpan.FromTicks(offset);
                return time + offspan;
            }
            set => SetDate(OFF_TRANSMIT_TIMESTAMP, value);
        }

        // Reception Timestamp
        public DateTime ReceptionTimestamp;

        // Round trip delay (in milliseconds)
        public int RoundTripDelay {
            get {
                TimeSpan span = ReceiveTimestamp - OriginateTimestamp + (ReceptionTimestamp - TransmitTimestamp);
                return (int)span.TotalMilliseconds;
            }
        }

        // Local clock offset (in milliseconds)
        public int LocalClockOffset {
            get {
                TimeSpan span = ReceiveTimestamp - OriginateTimestamp - (ReceptionTimestamp - TransmitTimestamp);
                return (int)(span.TotalMilliseconds / 2);
            }
        }

        // Compute date, given the number of milliseconds since January 1, 1900
        private DateTime ComputeDate(ulong milliseconds) {
            TimeSpan span = TimeSpan.FromMilliseconds((double)milliseconds);
            DateTime time = new DateTime(1900, 1, 1);
            time += span;
            return time;
        }

        // Compute the number of milliseconds, given the offset of a 8-byte array
        private ulong GetMilliSeconds(byte offset) {
            ulong intpart = 0, fractpart = 0;

            for (int i = 0; i <= 3; i++) {
                intpart = 256 * intpart + _ntpData[offset + i];
            }
            for (int i = 4; i <= 7; i++) {
                fractpart = 256 * fractpart + _ntpData[offset + i];
            }
            ulong milliseconds = intpart * 1000 + fractpart * 1000 / 0x100000000L;
            return milliseconds;
        }

        // Compute the 8-byte array, given the date
        private void SetDate(byte offset, DateTime date) {
            DateTime startOfCentury = new DateTime(1900, 1, 1, 0, 0, 0);	// January 1, 1900 12:00 AM

            ulong milliseconds = (ulong)(date - startOfCentury).TotalMilliseconds;
            var intpart = milliseconds / 1000;
            var fractpart = milliseconds % 1000 * 0x100000000L / 1000;

            ulong temp = intpart;
            for (int i = 3; i >= 0; i--) {
                _ntpData[offset + i] = (byte)(temp % 256);
                temp = temp / 256;
            }

            temp = fractpart;
            for (int i = 7; i >= 4; i--) {
                _ntpData[offset + i] = (byte)(temp % 256);
                temp = temp / 256;
            }
        }

        // Initialize the NTPClient data
        private void Initialize() {
            // Set version number to 4 and Mode to 3 (client)
            _ntpData[0] = 0x1B;
            // Initialize all other fields with 0
            for (int i = 1; i < 48; i++) {
                _ntpData[i] = 0;
            }
            // Initialize the transmit timestamp
            TransmitTimestamp = DateTime.Now;
        }

        public NtpClient(string host) {
            _timeServer = host;
        }

        // Connect to the time server
        public void Connect() {
            IPHostEntry hostadd = GetHostEntry(_timeServer);
            try {
                IPEndPoint ePhost = new IPEndPoint(hostadd.AddressList[0], 123);
                UdpClient timeSocket = new UdpClient();
                timeSocket.Connect(ePhost);
                Initialize();
                timeSocket.Send(_ntpData, _ntpData.Length);
                _ntpData = timeSocket.Receive(ref ePhost);
                if (!IsResponseValid()) {
                    throw new Exception("Invalid response from " + _timeServer);
                }
                ReceptionTimestamp = DateTime.Now;
            } catch (SocketException e) {
                throw new Exception(e.Message);
            }
        }

        // Check if the response from server is valid
        public bool IsResponseValid() {
            if (_ntpData.Length < NTP_DATA_LENGTH || Mode != Mode.Server) {
                return false;
            } else {
                return true;
            }
        }

        // Converts the object to string
        public override string ToString() {
            string str;

            str = "Leap Indicator: ";
            switch (LeapIndicator) {
                case LeapIndicator.NoWarning:
                    str += "No warning";
                    break;
                case LeapIndicator.LastMinute61:
                    str += "Last minute has 61 seconds";
                    break;
                case LeapIndicator.LastMinute59:
                    str += "Last minute has 59 seconds";
                    break;
                case LeapIndicator.Alarm:
                    str += "Alarm Condition (clock not synchronized)";
                    break;
            }
            str += "\r\nVersion number: " + VersionNumber + "\r\n";
            str += "Mode: ";
            switch (Mode) {
                case Mode.Unknown:
                    str += "Unknown";
                    break;
                case Mode.SymmetricActive:
                    str += "Symmetric Active";
                    break;
                case Mode.SymmetricPassive:
                    str += "Symmetric Pasive";
                    break;
                case Mode.Client:
                    str += "Client";
                    break;
                case Mode.Server:
                    str += "Server";
                    break;
                case Mode.Broadcast:
                    str += "Broadcast";
                    break;
            }
            str += "\r\nStratum: ";
            switch (Stratum) {
                case Stratum.Unspecified:
                case Stratum.Reserved:
                    str += "Unspecified";
                    break;
                case Stratum.PrimaryReference:
                    str += "Primary Reference";
                    break;
                case Stratum.SecondaryReference:
                    str += "Secondary Reference";
                    break;
            }
            str += "\r\nLocal time: " + TransmitTimestamp;
            str += "\r\nPrecision: " + Precision + " ms";
            str += "\r\nPoll Interval: " + PollInterval + " s";
            str += "\r\nReference ID: " + ReferenceId;
            str += "\r\nRoot Dispersion: " + RootDispersion + " ms";
            str += "\r\nRound Trip Delay: " + RoundTripDelay + " ms";
            str += "\r\nLocal Clock Offset: " + LocalClockOffset + " ms";
            str += "\r\n";

            return str;
        }

        // The URL of the time server we're connecting to
        private string _timeServer;

        public static NtpClient GetTime(string timeServer) {
            NtpClient client = new NtpClient(timeServer);
            client.Connect();
            return client;
        }
        public static NtpClient GetTime() {
            return GetTime(DefaultTimeServer);
        }
        public static string DefaultTimeServer = "time.nist.gov";


    }
}
