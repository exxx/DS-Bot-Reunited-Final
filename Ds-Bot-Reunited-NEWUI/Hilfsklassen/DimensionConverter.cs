﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Ds_Bot_Reunited_NEWUI.Hilfsklassen {
    public class DimensionConverter : IValueConverter {
        public object Convert(object value, Type targetType,
            object parameter, CultureInfo culture) {
            return (double) value / double.Parse(parameter as string);
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, CultureInfo culture) {
            return null;
        }
    }
}