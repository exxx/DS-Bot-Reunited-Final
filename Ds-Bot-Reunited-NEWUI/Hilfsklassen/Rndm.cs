﻿using System.Threading.Tasks;

namespace Ds_Bot_Reunited_NEWUI.Hilfsklassen {
    public static class Rndm {
        public static async Task Sleep(int min = 100, int max = 500, bool useSettings = true) {
            if (useSettings) {
                if (min < App.Settings.MinActionDelay) {
                    min = App.Settings.MinActionDelay;
                }
                if (max < App.Settings.MaxActionDelay) {
                    max = App.Settings.MaxActionDelay;
                }
            }
            if (min > 0 && max > min && max > 0) {
                await Task.Delay(App.Random.Next(min, max));
            }
        }
    }
}
