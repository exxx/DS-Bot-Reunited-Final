﻿using System;
using System.ComponentModel;
using System.Linq;

namespace Ds_Bot_Reunited_NEWUI.Hilfsklassen {
    public static class EnumExtension {
        public static string GetDescription(this Enum value) {
            var fieldInfo = value.GetType().GetField(value.ToString());
            if (fieldInfo == null) {
                return "";
            }

            var attribute = fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false).FirstOrDefault() as DescriptionAttribute;
            return attribute != null ? attribute.Description : value.ToString();
        }
    }
}