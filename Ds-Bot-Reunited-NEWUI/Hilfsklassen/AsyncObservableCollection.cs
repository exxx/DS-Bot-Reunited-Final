﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Reflection;

namespace Ds_Bot_Reunited_NEWUI.Hilfsklassen {
    [Obfuscation(Exclude = true, Feature = "renaming")]
    public class AsyncObservableCollection<T> : ObservableCollection<T> {
        public AsyncObservableCollection() {
        }

        public AsyncObservableCollection(IEnumerable<T> list) : base(list) {
        }
        
        public void AddRange(IEnumerable<T> collection) {
                if (collection == null) throw new ArgumentNullException("collection");

                foreach (var i in collection) Items.Add(i);
                OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        /// <summary>
        ///     Removes the first occurence of each item in the specified collection from ObservableCollection(Of T).
        /// </summary>
        public void RemoveRange(IEnumerable<T> collection) {
                if (collection == null) throw new ArgumentNullException("collection");

                foreach (var i in collection) Items.Remove(i);
                OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        /// <summary>
        ///     Clears the current collection and replaces it with the specified item.
        /// </summary>
        public void Replace(T item) {
            ReplaceRange(new[] {item});
        }

        /// <summary>
        ///     Clears the current collection and replaces it with the specified collection.
        /// </summary>
        public void ReplaceRange(IEnumerable<T> collection) {
                if (collection == null) throw new ArgumentNullException("collection");

                Items.Clear();
                foreach (var i in collection) Items.Add(i);
                OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
            }
    }
}
