﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Windows;

namespace Ds_Bot_Reunited_NEWUI.Hilfsklassen {
    public partial class ExceptionForm : Window {
        private static string _caption => (Assembly.GetEntryAssembly().GetCustomAttributes(typeof(AssemblyFileVersionAttribute), true).FirstOrDefault() as AssemblyFileVersionAttribute)?.Version;

        public ExceptionForm(System.Exception ex, string username) {
            InitializeComponent();

            if (ex != null) {
                txtExceptionText.Text = ex.Message;
                txtStackTrace.Text = ex.StackTrace;

                if (ex.InnerException != null) {
                    txtInnerExceptionText.Text = ex.InnerException.Message;
                    txtInnerExceptionStackTrace.Text = ex.InnerException.StackTrace;
                }
            }

            MailException(ex, username);
        }

        public static void MailException(System.Exception ex, string username) { }

        private void btnClose_Click(object sender, RoutedEventArgs e) {
            Close();
        }

        private void Window_Closed(object sender, EventArgs e) {
            Process.GetCurrentProcess().Kill();
        }
    }
}