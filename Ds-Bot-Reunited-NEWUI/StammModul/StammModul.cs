﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Ds_Bot_Reunited_NEWUI.Accountsettings.Ds_Bot_Reunited.Accountsettings;
using Ds_Bot_Reunited_NEWUI.Botcaptcha;
using Ds_Bot_Reunited_NEWUI.BrowserManager;
using Ds_Bot_Reunited_NEWUI.Constants;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using OpenQA.Selenium;

namespace Ds_Bot_Reunited_NEWUI.StammModul {
    public static class StammModul {
        public static async Task StammBeitretenFallsNichtVorhanden(Browser browser, Accountsetting accountsettings) {
            var acc = App.PlayerlistWholeWorld.FirstOrDefault(
                    x =>
                        x.PlayerName.Equals(!string.IsNullOrEmpty(accountsettings.Uvname)
                            ? accountsettings.Uvname
                            : accountsettings.Accountname))
                ?.Spieler.AllyId;
            accountsettings.MultiaccountingDaten.HasAlly = acc != 0 && (accountsettings.MultiaccountingDaten.LastAllyCheck < DateTime.Now.AddHours(24));

            if (!accountsettings.MultiaccountingDaten.HasAlly && accountsettings.MultiaccountingDaten.LastAllyCheck < DateTime.Now.AddHours(12)) {
                string link = App.Settings.OperateLink +
                              accountsettings.UvZusatzFuerLink + "&screen=ally";
                browser.WebDriver.Navigate().GoToUrl(link);
                await Rndm.Sleep(200, 400);
                await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings);
                if (browser.WebDriver.FindElements(By.PartialLinkText("Jetzt beitreten!")).Count > 0) {
                    ((IJavaScriptExecutor)browser.WebDriver).ExecuteScript("arguments[0].scrollIntoView(true);", browser.WebDriver.FindElements(By.PartialLinkText("Jetzt beitreten!"))[0]);
                    ((IJavaScriptExecutor)browser.WebDriver).ExecuteScript("$('#menu_row').hide();");
                    ((IJavaScriptExecutor)browser.WebDriver).ExecuteScript("$('.top_bar').hide();");
                    ((IJavaScriptExecutor)browser.WebDriver).ExecuteScript("$('#chat-wrapper').hide();");
                    ((IJavaScriptExecutor)browser.WebDriver).ExecuteScript("$('#footer').hide();");
                    await Rndm.Sleep(100,200);
                    browser.WebDriver.FindElements(By.PartialLinkText("Jetzt beitreten!"))[0].Click();
                    await Rndm.Sleep(300, 500);
                    browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.EigenesProfil.Inventar.ITEM_BESTAETIGEN_BUTTON_SELECTOR_XPATH))
                        .Click();
                    accountsettings.MultiaccountingDaten.HasAlly = true;
                }
                if (browser.WebDriver.FindElements(By.Id("popup_box_feature_share")).Count > 0) {
                    browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Quest.QUESTFENSTER_SCHLIESSEN_BUTTON_SELECTOR_XPATH)).Click();
                    await Rndm.Sleep(300,500);
                }
                accountsettings.MultiaccountingDaten.LastAllyCheck = DateTime.Now;
            }
        }
    }
}