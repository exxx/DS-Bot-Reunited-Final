﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Navigation;
using Ds_Bot_Reunited_NEWUI.Annotations;

namespace Ds_Bot_Reunited_NEWUI {
    public partial class MainWindow : INotifyPropertyChanged {
        private string _text;
        
        public MainWindow() {

            MainWindowViewModel mainWindowViewViewModel = new MainWindowViewModel(this);

            Closing += delegate { mainWindowViewViewModel.CloseAction(); };
            DataContext = mainWindowViewViewModel;
            InitializeComponent();
            App.NotificationIcon = new NotifyIcon {
                Icon = new System.Drawing.Icon("Boticon.ico"),
                Visible = true,
                Text = Text
            };
            App.NotificationIcon.DoubleClick +=
                delegate {
                    Show();
                    WindowState = WindowState.Normal;
                };
            MenuItem mnuItemNew = new MenuItem {Text = Properties.Resources.Start};
            mnuItemNew.Click += delegate {
                mainWindowViewViewModel.TileView.StartCycle();
                if (mainWindowViewViewModel.TileView.Started && App.Active)
                    mnuItemNew.Text = Properties.Resources.Pause;
                else {
                    mnuItemNew.Text = Properties.Resources.Start;
                }
            };
            MenuItem mnuItemNew2 = new MenuItem {Text = Properties.Resources.Exit};
            mnuItemNew2.Click += delegate { Close(); };
            MenuItem[] menuitems = new MenuItem[2];
            menuitems[0] = mnuItemNew;
            menuitems[1] = mnuItemNew2;
            App.NotificationIcon.ContextMenu = new ContextMenu(menuitems);
        }
        
        public string Text {
            get => _text;
            set {
                _text = value;
                OnPropertyChanged();
            }
        }

        protected override void OnStateChanged(EventArgs e) {
            if (App.Settings.MinimizeToTray && WindowState == WindowState.Minimized)
                Hide();

            base.OnStateChanged(e);
        }


        private void Hyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e) {
            Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
            e.Handled = true;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        
        private void AutoUpdaterApplicationExitEvent() {
            Closing += null;
            Environment.Exit(0);
        }
    }
}