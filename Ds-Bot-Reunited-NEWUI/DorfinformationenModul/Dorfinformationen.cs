﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using Ds_Bot_Reunited_NEWUI.Annotations;
using Ds_Bot_Reunited_NEWUI.Dorfinformationen;
using Ds_Bot_Reunited_NEWUI.Farmen;
using Ds_Bot_Reunited_NEWUI.Marktmodul;

namespace Ds_Bot_Reunited_NEWUI.DorfinformationenModul {
    [Serializable]
    public class Dorfinformationen : INotifyPropertyChanged {
        private List<Markettransfer> _ankommendeMarkttransfers;
        private int _availableTraders;
        private bool _axtkaempferErforscht;
        private int _axtkaempferStufe;
        private bool _beritteneBogenschuetzenErforscht;
        private int _beritteneBogenschuetzenStufe;
        private bool _bogenschuetzenErforscht;
        private int _bogenschuetzenStufe;
        private int _farmActual;
        private int _farmMax;
        private Gebaeude _gebaeudeStufen;
        private bool _katapulteErforscht;
        private int _katapulteStufe;
        private DateTime _lastBauenZeitpunkt;
        private DateTime _lastFarmenZeitpunkt;
        private DateTime _lastForschenZeitpunkt;
        private DateTime _lastRaubzugZeitpunkt;
        private DateTime _lastRekrutierenZeitpunkt;
        private bool _leichteKavallerieErforscht;
        private int _leichteKavallerieStufe;
        private DateTime _noobschutzaktivBis;
        private bool _rammboeckeErforscht;
        private int _rammboeckeStufe;
        private bool _schwereKavallerieErforscht;
        private int _schwereKavallerieStufe;
        private bool _schwertkaempferErforscht;
        private int _schwertkaempferStufe;
        private bool _spaeherErforscht;
        private int _spaeherStufe;
        private int _speertraegerStufe = 1;
        private Beute _speicher;
        private int _storageMax;
        private TruppenTemplate _truppen;

        public DateTime NoobschutzaktivBis {
            get => _noobschutzaktivBis;
            set {
                _noobschutzaktivBis = value;
                OnPropertyChanged();
            }
        }

        public Beute Speicher {
            get => _speicher;
            set {
                _speicher = value;
                OnPropertyChanged();
            }
        }

        public int SpeicherTotalHolz {
            get {
                int holz = 0;
                holz += Speicher.Holz;
                if (AnkommendeMarkttransfers.Any(x => x.Datum > DateTime.Now)) {
                    holz -= AnkommendeMarkttransfers.Where(x => x.Datum > DateTime.Now).Sum(x => x.Holz);
                }

                return holz;
            }
        }

        public int SpeicherTotalLehm {
            get {
                int lehm = 0;
                lehm += Speicher.Lehm;
                if (AnkommendeMarkttransfers.Any(x => x.Datum > DateTime.Now)) {
                    lehm -= AnkommendeMarkttransfers.Where(x => x.Datum > DateTime.Now).Sum(x => x.Lehm);
                }

                return lehm;
            }
        }

        public int SpeicherTotalEisen {
            get {
                int eisen = 0;
                eisen += Speicher.Eisen;
                if (AnkommendeMarkttransfers.Any(x => x.Datum > DateTime.Now)) {
                    eisen -= AnkommendeMarkttransfers.Where(x => x.Datum > DateTime.Now).Sum(x => x.Eisen);
                }

                return eisen;
            }
        }

        public DateTime LastForschenZeitpunkt {
            get => _lastForschenZeitpunkt;
            set {
                _lastForschenZeitpunkt = value;
                OnPropertyChanged();
            }
        }

        public DateTime LastRaubzugZeitpunkt {
            get => _lastRaubzugZeitpunkt;
            set {
                _lastRaubzugZeitpunkt = value;
                OnPropertyChanged();
            }
        }

        public Gebaeude GebaeudeStufen {
            get => _gebaeudeStufen;
            set {
                _gebaeudeStufen = value;
                OnPropertyChanged();
            }
        }

        public TruppenTemplate Truppen {
            get => _truppen;
            set {
                _truppen = value;
                OnPropertyChanged();
            }
        }

        public int FarmActual {
            get => _farmActual;
            set {
                _farmActual = value;
                OnPropertyChanged();
            }
        }

        public int FarmMax {
            get => _farmMax;
            set {
                _farmMax = value;
                OnPropertyChanged();
            }
        }

        public int StorageMax {
            get => _storageMax;
            set {
                _storageMax = value;
                OnPropertyChanged();
            }
        }

        public DateTime LastFarmenZeitpunkt {
            get => _lastFarmenZeitpunkt;
            set {
                _lastFarmenZeitpunkt = value;
                OnPropertyChanged();
            }
        }

        public DateTime LastBauenZeitpunkt {
            get => _lastBauenZeitpunkt;
            set {
                _lastBauenZeitpunkt = value;
                OnPropertyChanged();
            }
        }

        public DateTime LastRekrutierenZeitpunkt {
            get => _lastRekrutierenZeitpunkt;
            set {
                _lastRekrutierenZeitpunkt = value;
                OnPropertyChanged();
            }
        }

        public int SpeertraegerStufe {
            get => _speertraegerStufe;
            set {
                _speertraegerStufe = value;
                OnPropertyChanged();
            }
        }

        public int SchwertkaempferStufe {
            get => _schwertkaempferStufe;
            set {
                _schwertkaempferStufe = value;
                OnPropertyChanged();
            }
        }

        public int AxtkaempferStufe {
            get => _axtkaempferStufe;
            set {
                _axtkaempferStufe = value;
                OnPropertyChanged();
            }
        }

        public int BogenschuetzenStufe {
            get => _bogenschuetzenStufe;
            set {
                _bogenschuetzenStufe = value;
                OnPropertyChanged();
            }
        }

        public int SpaeherStufe {
            get => _spaeherStufe;
            set {
                _spaeherStufe = value;
                OnPropertyChanged();
            }
        }

        public int LeichteKavallerieStufe {
            get => _leichteKavallerieStufe;
            set {
                _leichteKavallerieStufe = value;
                OnPropertyChanged();
            }
        }

        public int BeritteneBogenschuetzenStufe {
            get => _beritteneBogenschuetzenStufe;
            set {
                _beritteneBogenschuetzenStufe = value;
                OnPropertyChanged();
            }
        }

        public int SchwereKavallerieStufe {
            get => _schwereKavallerieStufe;
            set {
                _schwereKavallerieStufe = value;
                OnPropertyChanged();
            }
        }

        public int RammboeckeStufe {
            get => _rammboeckeStufe;
            set {
                _rammboeckeStufe = value;
                OnPropertyChanged();
            }
        }

        public int KatapulteStufe {
            get => _katapulteStufe;
            set {
                _katapulteStufe = value;
                OnPropertyChanged();
            }
        }


        public bool SchwertkaempferErforscht {
            get => _schwertkaempferErforscht;
            set {
                _schwertkaempferErforscht = value;
                OnPropertyChanged();
            }
        }

        public bool AxtkaempferErforscht {
            get => _axtkaempferErforscht;
            set {
                _axtkaempferErforscht = value;
                OnPropertyChanged();
            }
        }

        public bool BogenschuetzenErforscht {
            get => _bogenschuetzenErforscht;
            set {
                _bogenschuetzenErforscht = value;
                OnPropertyChanged();
            }
        }

        public bool SpaeherErforscht {
            get => _spaeherErforscht;
            set {
                _spaeherErforscht = value;
                OnPropertyChanged();
            }
        }

        public bool LeichteKavallerieErforscht {
            get => _leichteKavallerieErforscht;
            set {
                _leichteKavallerieErforscht = value;
                OnPropertyChanged();
            }
        }

        public bool BeritteneBogenschuetzenErforscht {
            get => _beritteneBogenschuetzenErforscht;
            set {
                _beritteneBogenschuetzenErforscht = value;
                OnPropertyChanged();
            }
        }

        public bool SchwereKavallerieErforscht {
            get => _schwereKavallerieErforscht;
            set {
                _schwereKavallerieErforscht = value;
                OnPropertyChanged();
            }
        }

        public bool RammboeckeErforscht {
            get => _rammboeckeErforscht;
            set {
                _rammboeckeErforscht = value;
                OnPropertyChanged();
            }
        }

        public bool KatapulteErforscht {
            get => _katapulteErforscht;
            set {
                _katapulteErforscht = value;
                OnPropertyChanged();
            }
        }

        public int AvailableTraders {
            get => _availableTraders;
            set {
                _availableTraders = value;
                OnPropertyChanged();
            }
        }

        public List<Markettransfer> AnkommendeMarkttransfers {
            get => _ankommendeMarkttransfers;
            set {
                _ankommendeMarkttransfers = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;


        public Dorfinformationen() {
            GebaeudeStufen = new Gebaeude();
            Speicher = new Beute();
            Truppen = new TruppenTemplate();
            AnkommendeMarkttransfers = new List<Markettransfer>();
        }

        public int GetGebaeudeStufe(BauenModul.Gebaeude gebaeude) {
            switch (gebaeude) {
                case BauenModul.Gebaeude.Hauptgebaeude:
                    return GebaeudeStufen.Hauptgebaeude;
                case BauenModul.Gebaeude.Kaserne:
                    return GebaeudeStufen.Kaserne;
                case BauenModul.Gebaeude.Stall:
                    return GebaeudeStufen.Stall;
                case BauenModul.Gebaeude.Werkstatt:
                    return GebaeudeStufen.Werkstatt;
                case BauenModul.Gebaeude.Adelshof:
                    return GebaeudeStufen.Adelshof;
                case BauenModul.Gebaeude.Versammlungsplatz:
                    return GebaeudeStufen.Versammlungsplatz;
                case BauenModul.Gebaeude.Statue:
                    return GebaeudeStufen.Statue;
                case BauenModul.Gebaeude.Marktplatz:
                    return GebaeudeStufen.Marktplatz;
                case BauenModul.Gebaeude.Holzfaeller:
                    return GebaeudeStufen.Holzfaeller;
                case BauenModul.Gebaeude.Lehmgrube:
                    return GebaeudeStufen.Lehmgrube;
                case BauenModul.Gebaeude.Eisenmine:
                    return GebaeudeStufen.Eisenmine;
                case BauenModul.Gebaeude.Speicher:
                    return GebaeudeStufen.Speicher;
                case BauenModul.Gebaeude.Bauernhof:
                    return GebaeudeStufen.Bauernhof;
                case BauenModul.Gebaeude.Versteck:
                    return GebaeudeStufen.Versteck;
                case BauenModul.Gebaeude.Wall:
                    return GebaeudeStufen.Wall;
                default:
                    return 0;
            }
        }

        public Dorfinformationen Clone() {
            Dorfinformationen newDorfinformationen = new Dorfinformationen {
                NoobschutzaktivBis = NoobschutzaktivBis,
                Speicher = Speicher,
                GebaeudeStufen = GebaeudeStufen.Clone(),
                Truppen = Truppen.Clone(),
                StorageMax = StorageMax,
                FarmActual = FarmActual,
                FarmMax = FarmMax,
                LastFarmenZeitpunkt = LastFarmenZeitpunkt,
                LastBauenZeitpunkt = LastBauenZeitpunkt,
                LastRekrutierenZeitpunkt = LastRekrutierenZeitpunkt,
                AxtkaempferErforscht = AxtkaempferErforscht,
                BogenschuetzenErforscht = BogenschuetzenErforscht,
                BeritteneBogenschuetzenErforscht = BeritteneBogenschuetzenErforscht,
                KatapulteErforscht = KatapulteErforscht,
                RammboeckeErforscht = RammboeckeErforscht,
                SchwereKavallerieErforscht = SchwereKavallerieErforscht,
                SpaeherErforscht = SpaeherErforscht,
                LeichteKavallerieErforscht = LeichteKavallerieErforscht,
                AvailableTraders = AvailableTraders,
                AnkommendeMarkttransfers = new List<Markettransfer>(),
                SchwertkaempferErforscht = SchwertkaempferErforscht,
                SpeertraegerStufe = SpeertraegerStufe,
                SchwertkaempferStufe = SchwertkaempferStufe,
                AxtkaempferStufe = AxtkaempferStufe,
                BogenschuetzenStufe = BogenschuetzenStufe,
                LeichteKavallerieStufe = LeichteKavallerieStufe,
                BeritteneBogenschuetzenStufe = BeritteneBogenschuetzenStufe,
                SchwereKavallerieStufe = SchwereKavallerieStufe,
                RammboeckeStufe = RammboeckeStufe,
                KatapulteStufe = KatapulteStufe,
                SpaeherStufe = SpaeherStufe,
                LastForschenZeitpunkt = LastForschenZeitpunkt,
                LastRaubzugZeitpunkt = LastRaubzugZeitpunkt
            };
            foreach (var markttransfer in AnkommendeMarkttransfers) {
                newDorfinformationen.AnkommendeMarkttransfers.Add(markttransfer);
            }

            return newDorfinformationen;
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}