﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ds_Bot_Reunited_NEWUI.Accountsettings;
using Ds_Bot_Reunited_NEWUI.Accountsettings.Ds_Bot_Reunited.Accountsettings;
using Ds_Bot_Reunited_NEWUI.AttackDetectionModul;
using Ds_Bot_Reunited_NEWUI.Attackplaner;
using Ds_Bot_Reunited_NEWUI.Botcaptcha;
using Ds_Bot_Reunited_NEWUI.BrowserManager;
using Ds_Bot_Reunited_NEWUI.Constants;
using Ds_Bot_Reunited_NEWUI.Dorfinformationen;
using Ds_Bot_Reunited_NEWUI.Farmen;
using Ds_Bot_Reunited_NEWUI.Farmmodul;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.Properties;
using Ds_Bot_Reunited_NEWUI.Settings;
using OpenQA.Selenium;

namespace Ds_Bot_Reunited_NEWUI.DorfinformationenModul {
    public class SynchronizeVillage {
        public int VillageId { get; set; }
        public int Truppe { get; set; }
    }

    public class DorfinformationenModul {
        public static bool TruppeneinlesenInDoing;

        public static async Task UpdateVillagesOverProductionView(Browser browser, Accountsetting accountsettings, List<AccountDorfSettings> doerfer) {
            var productionUrl = App.Settings.OperateLink + "screen=overview_villages" + accountsettings.UvZusatzFuerLink + "&mode=prod";
            browser.WebDriver.Navigate().GoToUrl(productionUrl);
            await Rndm.Sleep(100, 300);
            if (browser.WebDriver.FindElements(By.XPath("//a[contains(@href,'&order=trader_available')]")).Any()) {
                var production = browser.WebDriver.FindElement(By.Id("production_table"));
                if (production != null) {
                    var rows = production.FindElements(By.TagName("tr")).ToList();


                    foreach (var row in rows) {
                        var tableCells = row.FindElements(By.TagName("td")).ToList();
                        var maxstorage = 0;
                        var verfuegbarehaendler = 0;
                        if (tableCells.Any()) {
                            var villagestring = tableCells[1].Text;
                            var empfaengerDorfX = villagestring.Substring(villagestring.IndexOf("(", StringComparison.Ordinal) + 1, 3);
                            var empfaengerDorfY = villagestring.Substring(villagestring.IndexOf("(", StringComparison.Ordinal) + 5, 3);
                            
                            int.TryParse(empfaengerDorfX, out int x);
                            int.TryParse(empfaengerDorfY, out int y);
                            var dorf = doerfer.FirstOrDefault(k => k.Dorf.XCoordinate.Equals(x) && k.Dorf.YCoordinate.Equals(y));
                            if (dorf != null) {
                                var ressources = tableCells[3].Text.Trim().Replace(".", "").Split(' ');
                                int.TryParse(ressources[0], out int wood);
                                int.TryParse(ressources[1], out int stone);
                                int.TryParse(ressources[2], out int iron);
                                if (wood > 1000 || stone > 1000 || iron > 1000) {
                                    int.TryParse(tableCells[4].Text.Trim().Replace(".", ""), out maxstorage);
                                    int.TryParse(tableCells[5].Text.Trim().Split('/')[0], out verfuegbarehaendler);
                                }

                                dorf.Dorfinformationen.Speicher.Holz = wood;
                                dorf.Dorfinformationen.Speicher.Lehm = stone;
                                dorf.Dorfinformationen.Speicher.Eisen = iron;
                                dorf.Dorfinformationen.StorageMax = maxstorage;
                                dorf.Dorfinformationen.AvailableTraders = verfuegbarehaendler;
                            }
                        }
                    }
                }
            }
            else {
                foreach (var dorf in doerfer) {
                    UpdateVillageOverMarketplaceSending(browser, dorf, accountsettings);
                }
            }
        }


        public static void UpdateVillageOverMarketplaceSending(Browser browser, AccountDorfSettings dorf, Accountsetting accountsettings) {
            //
            var url = App.Settings.OperateLink + "village=" + dorf.Dorf.VillageId + "" + accountsettings.UvZusatzFuerLink + "&screen=market&mode=send";
            browser.WebDriver.Navigate().GoToUrl(url);
            browser.WebDriver.WaitForPageload();
            dorf.Dorfinformationen.Speicher = new Beute {Holz = int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Dorfuebersicht.SPEICHER_HOLZ_SELECTOR_XPATH)).Text), Lehm = int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Dorfuebersicht.SPEICHER_LEHM_SELECTOR_XPATH)).Text), Eisen = int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Dorfuebersicht.SPEICHER_EISEN_SELECTOR_XPATH)).Text)};
            dorf.Dorfinformationen.StorageMax = int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Dorfuebersicht.SPEICHER_SELECTOR_XPATH)).Text);
            dorf.Dorfinformationen.FarmActual = int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Dorfuebersicht.AKTUELLBH_SELECTOR_XPATH)).Text);
            dorf.Dorfinformationen.FarmMax = int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Dorfuebersicht.MAXBH_SELECTOR_XPATH)).Text);
            dorf.Dorfinformationen.AvailableTraders = int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Uebersicht.VERFUEGBAREHAENDLER_SELECTOR_XPATH)).Text);
        }

        public static TruppenTemplate GetTroopsOfVillage(int villageid) {
            var truppentemplate = new TruppenTemplate();
            var account = App.Settings.Accountsettingslist.FirstOrDefault(x => x.DoerferSettings.Any(k => k.Dorf.VillageId.Equals(villageid)));
            var dorf = account?.DoerferSettings.FirstOrDefault(x => x.Dorf.VillageId.Equals(villageid));
            if (dorf != null) {
                truppentemplate = dorf.Dorfinformationen.Truppen.Clone();
            }

            if (account != null && account.GeplanteBewegungen.Any()) {
                foreach (var bewegung in account.GeplanteBewegungen.Where(x => x.Abschickzeitpunkt > DateTime.Now && x.SenderDorf.VillageId.Equals(villageid))) {
                    truppentemplate.SpeertraegerAnzahl -= bewegung.SpeertraegerAnzahl;
                    truppentemplate.SchwertkaempferAnzahl -= bewegung.SchwertkaempferAnzahl;
                    truppentemplate.AxtkaempferAnzahl -= bewegung.AxtkaempferAnzahl;
                    truppentemplate.BogenschuetzenAnzahl -= bewegung.BogenschuetzenAnzahl;
                    truppentemplate.SpaeherAnzahl -= bewegung.SpaeherAnzahl;
                    truppentemplate.LeichteKavallerieAnzahl -= bewegung.LeichteKavallerieAnzahl;
                    truppentemplate.BeritteneBogenschuetzenAnzahl -= bewegung.BeritteneBogenschuetzenAnzahl;
                    truppentemplate.SchwereKavallerieAnzahl -= bewegung.SchwereKavallerieAnzahl;
                    truppentemplate.RammboeckeAnzahl -= bewegung.RammboeckeAnzahl;
                    truppentemplate.KatapulteAnzahl -= bewegung.KatapulteAnzahl;
                    truppentemplate.PaladinAnzahl -= bewegung.PaladinAnzahl;
                    truppentemplate.AdelsgeschlechterAnzahl -= bewegung.AdelsgeschlechterAnzahl;
                }
            }

            return truppentemplate;
        }

        public static void ImportMissingAttacks(Browser browser, AccountDorfSettings dorf) {
            try {
                var commands = browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Dorfuebersicht.ANGRIFFE_SELECTOR_XPATH)).Where(x => x.Text.Contains(NAMECONSTANTS.GetNameForServer(Names.Angriff))).ToList();
                if (commands.Any() && commands.Count < 50) {
                    for (var i = 0; i < commands.Count; i++) {
                        var command = commands[i];
                        try {
                            var attack = new AttacksOnFarmTarget {AttackerVillageId = dorf.Dorf.VillageId};

                            var info = command.FindElements(By.TagName("td"))[0].Text;
                            var indexLastKlammerauf = info.LastIndexOf("(", StringComparison.CurrentCulture);
                            var indexLastKlammerzu = info.LastIndexOf("K", StringComparison.CurrentCulture);
                            var koordinaten = info.Substring(indexLastKlammerauf + 1, info.Length - indexLastKlammerzu + 4);
                            var indexOfSchreagstrich = koordinaten.IndexOf("|", StringComparison.CurrentCulture);
                            var x = int.Parse(koordinaten.Substring(0, indexOfSchreagstrich));
                            var y = int.Parse(koordinaten.Substring(indexOfSchreagstrich + 1, koordinaten.Length - indexOfSchreagstrich - 1));
                            try {
                                var first = App.Farmliste.First(k => k.Dorf.XCoordinate.Equals(x) && k.Dorf.YCoordinate.Equals(y));
                                if (first != null) {
                                    var temppp = command.FindElements(By.TagName("td"))[1].Text;
                                    DateTime fff;
                                    if (temppp.Contains(NAMECONSTANTS.GetNameForServer(Names.Heute))) {
                                        var temp2 = temppp.Split(' ');
                                        fff = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, int.Parse(temp2[2].Substring(0, 2)), int.Parse(temp2[2].Substring(3, 2)), int.Parse(temp2[2].Substring(6, 2)));
                                        if (App.Settings.Weltensettings.MillisecondsActive) {
                                            fff = fff.AddMilliseconds(int.Parse(temp2[2].Substring(9, 3)));
                                        }
                                    }
                                    else if (temppp.Contains(NAMECONSTANTS.GetNameForServer(Names.Morgen))) {
                                        var temp2 = temppp.Split(' ');
                                        fff = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day + 1, int.Parse(temp2[2].Substring(0, 2)), int.Parse(temp2[2].Substring(3, 2)), int.Parse(temp2[2].Substring(6, 2)));
                                        if (App.Settings.Weltensettings.MillisecondsActive) {
                                            fff = fff.AddMilliseconds(int.Parse(temp2[2].Substring(9, 3)));
                                        }
                                    }
                                    else {
                                        var temp2 = temppp.Split(' ');
                                        fff = new DateTime(DateTime.Now.Year, int.Parse(temp2[1].Substring(3, 2)), int.Parse(temp2[1].Substring(0, 2)), int.Parse(temp2[3].Substring(0, 2)), int.Parse(temp2[3].Substring(3, 2)), int.Parse(temp2[3].Substring(6, 2)));
                                        if (App.Settings.Weltensettings.MillisecondsActive) {
                                            fff = fff.AddMilliseconds(int.Parse(temp2[3].Substring(9, 3)));
                                        }
                                    }

                                    attack.LandTime = fff;
                                    if (!first.Attacks.Any(k => k.LandTime.AddSeconds(-5) < attack.LandTime && k.LandTime.AddSeconds(+5) > attack.LandTime)) {
                                        first.Attacks.Add(attack);
                                    }
                                }
                            } catch {
                                // ignored
                            }
                        } catch {
                            commands = browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Dorfuebersicht.ANGRIFFE_SELECTOR_XPATH)).Where(x => x.Text.Contains(NAMECONSTANTS.GetNameForServer(Names.Angriff))).ToList();
                            i -= 1;
                            if (i < 0) {
                                i = 0;
                            }
                        }
                    }
                }
            } catch {
                // ignored
            }
        }

        public static async void ImportTruppenzahlUndUnterstuetzungenPremiumAsync(Browser browser, Accountsetting accountsettings) {
            await Task.Run(async () => {
                if (!TruppeneinlesenInDoing && App.Settings.PreventActive) {
                    browser.GetsUsed = true;
                    TruppeneinlesenInDoing = true;
                    try {
                        if (accountsettings.LastTruppeneinlesen.AddMinutes(App.Settings.UpdateUnitsinterval) < DateTime.Now) {
                            App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + Resources.TruppenEinlesen);
                            var url = App.Settings.OperateLink + "screen=overview_villages" + accountsettings.UvZusatzFuerLink + "&mode=combined";
                            if (!browser.WebDriver.Url.Contains(url)) {
                                browser.WebDriver.Navigate().GoToUrl(url);
                            }

                            await Task.Delay(500);
                            await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings);
                            var element = browser.WebDriver.FindElement(By.ClassName("vis_item"));
                            try {
                                var allegruppeelement = element.FindElement(By.PartialLinkText("alle"));
                                if (allegruppeelement != null && allegruppeelement.TagName.Contains("a")) {
                                    allegruppeelement.Click();
                                    browser.WebDriver.WaitForPageload();
                                    browser.WebDriver.WaitForAjax();
                                }
                            } catch (Exception) {
                                App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + Resources.GruppeSchonEingelesen + "!");
                            }

                            var table = browser.WebDriver.FindElement(By.Id("combined_table"));
                            if (table != null) {
                                var tablerows = table.FindElements(By.TagName("tr")).ToList();
                                for (var i = 1; i < tablerows.Count; i++) {
                                    App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + Resources.TruppeEinlesenDorf + " " + i + " " + Resources.Von + " " + (tablerows.Count - 2));
                                    var row = tablerows[i];
                                    var villageid = int.Parse(row.FindElement(By.ClassName("quickedit-vn")).GetAttribute("data-id"));
                                    //index 8
                                    var troopcells = row.FindElements(By.TagName("td")).ToList();
                                    var index = 8;
                                    var speertraegerAnzahl = int.Parse(troopcells[index++].Text);
                                    var schwertkaempferAnzahl = int.Parse(troopcells[index++].Text);
                                    var axtkaempferAnzahl = int.Parse(troopcells[index++].Text);
                                    var bogenschuetzenAnzahl = App.Settings.Weltensettings.BogenschuetzenActive ? int.Parse(troopcells[index++].Text) : 0;
                                    var spaeherAnzahl = int.Parse(troopcells[index++].Text);
                                    var leichteKavallerieAnzahl = int.Parse(troopcells[index++].Text);
                                    var beritteneBogenschuetzenAnzahl = App.Settings.Weltensettings.BogenschuetzenActive ? int.Parse(troopcells[index++].Text) : 0;
                                    var schwereKavallerieAnzahl = int.Parse(troopcells[index++].Text);
                                    var rammboeckeAnzahl = int.Parse(troopcells[index++].Text);
                                    var katapultAnzahl = int.Parse(troopcells[index++].Text);
                                    var paladinAnzahl = App.Settings.Weltensettings.PaladinActive ? int.Parse(troopcells[index++].Text) : 0;
                                    var adelsgeschlechtAnzahl = int.Parse(troopcells[index].Text);

                                    var dorf = accountsettings.DoerferSettings.FirstOrDefault(x => x.Dorf.VillageId.Equals(villageid));
                                    if (dorf != null) {
                                        dorf.Dorfinformationen.Truppen.SpeertraegerAnzahl = speertraegerAnzahl;
                                        dorf.Dorfinformationen.Truppen.SchwertkaempferAnzahl = schwertkaempferAnzahl;
                                        dorf.Dorfinformationen.Truppen.AxtkaempferAnzahl = axtkaempferAnzahl;
                                        dorf.Dorfinformationen.Truppen.BogenschuetzenAnzahl = bogenschuetzenAnzahl;
                                        dorf.Dorfinformationen.Truppen.SpaeherAnzahl = spaeherAnzahl;
                                        dorf.Dorfinformationen.Truppen.LeichteKavallerieAnzahl = leichteKavallerieAnzahl;
                                        dorf.Dorfinformationen.Truppen.BeritteneBogenschuetzenAnzahl = beritteneBogenschuetzenAnzahl;
                                        dorf.Dorfinformationen.Truppen.SchwereKavallerieAnzahl = schwereKavallerieAnzahl;
                                        dorf.Dorfinformationen.Truppen.RammboeckeAnzahl = rammboeckeAnzahl;
                                        dorf.Dorfinformationen.Truppen.KatapulteAnzahl = katapultAnzahl;
                                        dorf.Dorfinformationen.Truppen.PaladinAnzahl = paladinAnzahl;
                                        dorf.Dorfinformationen.Truppen.AdelsgeschlechterAnzahl = adelsgeschlechtAnzahl;
                                    }

                                    if (dorf != null) {
                                        dorf.LastTruppeneinlesen = DateTime.Now;
                                    }
                                }
                            }

                            accountsettings.LastTruppeneinlesen = DateTime.Now;
                        }

                        if (accountsettings.LastUnterstuetzungeneinlesen.AddMinutes(App.Settings.UpdateUnitsinterval) < DateTime.Now) {
                            App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + Resources.UnterstuetzungeinlesenGestartet);

                            var url = App.Settings.OperateLink + "screen=overview_villages" + accountsettings.UvZusatzFuerLink + "&mode=units&type=away_detail";
                            if (!browser.WebDriver.Url.Contains(url)) {
                                browser.WebDriver.Navigate().GoToUrl(url);
                            }

                            browser.WebDriver.WaitForPageload();
                            try {
                                await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings);
                                var element2 = browser.WebDriver.FindElement(By.ClassName("vis_item"));
                                try {
                                    var allegruppeelement = element2.FindElement(By.PartialLinkText("alle"));
                                    if (allegruppeelement != null && allegruppeelement.TagName.Contains("a")) {
                                        allegruppeelement.Click();
                                        await Task.Delay(200);
                                    }
                                } catch (Exception) {
                                    App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + Resources.GruppeSchonEingelesen + "!");
                                }

                                accountsettings.Unterstuetzungen.Clear();
                                foreach (var seite in SeitenModul.GetSeiten(browser.WebDriver, true)) {
                                    SeitenModul.SetPage(browser, seite);
                                    var tableunit = browser.WebDriver.FindElement(By.Id("units_table"));
                                    if (tableunit != null) {
                                        var tablerows2 = tableunit.FindElements(By.TagName("tr")).ToList();
                                        var vorherigeVillageId = 0;
                                        for (var i = 1; i < tablerows2.Count; i++) {
                                            App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + Resources.UnterstuetzungenEinlesen + " " + i + " " + Resources.Von + " " + (tablerows2.Count - 2));
                                            var row = tablerows2[i];
                                            try {
                                                vorherigeVillageId = int.Parse(row.FindElement(By.ClassName("quickedit-vn")).GetAttribute("data-id"));
                                            } catch {
                                                // ignored
                                            }

                                            if (row.GetAttribute("class").Equals("row_a") || row.GetAttribute("class").Equals("row_b")) {
                                                var elements = row.FindElements(By.TagName("a")).ToList();
                                                var empfaengerdorf = elements[0].Text;
                                                var empfaengerDorfX = int.Parse(empfaengerdorf.Substring(empfaengerdorf.IndexOf("(", StringComparison.Ordinal) + 1, 3));
                                                var empfaengerDorfY = int.Parse(empfaengerdorf.Substring(empfaengerdorf.IndexOf("(", StringComparison.Ordinal) + 5, 3));
                                                var searchtuple = new VillageTuple<int, int>(empfaengerDorfX, empfaengerDorfY);
                                                if (App.VillagelistWholeWorld.ContainsKey(searchtuple)) {
                                                    var empfaengerid = App.VillagelistWholeWorld[searchtuple].VillageId;
                                                    var counterr = 1;
                                                    var tableCells = row.FindElements(By.TagName("td")).ToList();
                                                    var speertraegerAnzahl = tableCells[counterr++].Text;
                                                    var schwertkaempferAnzahl = tableCells[counterr++].Text;
                                                    var axtkaempferAnzahl = tableCells[counterr++].Text;
                                                    var bogenschuetzenAnzahl = "0";
                                                    if (App.Settings != null && App.Settings.Weltensettings.BogenschuetzenActive) {
                                                        bogenschuetzenAnzahl = tableCells[counterr++].Text;
                                                    }

                                                    var spaeherAnzahl = tableCells[counterr++].Text;
                                                    var leichtekavallerieAnzahl = tableCells[counterr++].Text;
                                                    var berittenebogenschuetzenAnzahl = "0";
                                                    if (App.Settings != null && App.Settings.Weltensettings.BogenschuetzenActive) {
                                                        berittenebogenschuetzenAnzahl = tableCells[counterr++].Text;
                                                    }

                                                    var schwerekavallerieAnzahl = tableCells[counterr++].Text;
                                                    var rammboeckeAnzahl = tableCells[counterr++].Text;
                                                    var katapulteAnzahl = tableCells[counterr++].Text;
                                                    var paladinAnzahl = "0";
                                                    if (App.Settings.Weltensettings.PaladinActive) {
                                                        paladinAnzahl = tableCells[counterr++].Text;
                                                    }

                                                    var adelsgeschlechtAnzahl = tableCells[counterr].Text;
                                                    var id = vorherigeVillageId;
                                                    if (accountsettings.Unterstuetzungen.Any(x => x.EmpfaengerVillageId.Equals(empfaengerid) && x.SenderVillageId.Equals(id))) {
                                                        var unterstuetzung = new Unterstuetzung {EmpfaengerVillageId = empfaengerid, SenderVillageId = vorherigeVillageId, SpeertraegerAnzahl = int.Parse(speertraegerAnzahl), SchwertkaempferAnzahl = int.Parse(schwertkaempferAnzahl), AxtkaempferAnzahl = int.Parse(axtkaempferAnzahl), BogenschuetzenAnzahl = int.Parse(bogenschuetzenAnzahl), SpaeherAnzahl = int.Parse(spaeherAnzahl), LeichteKavallerieAnzahl = int.Parse(leichtekavallerieAnzahl), BeritteneBogenschuetzenAnzahl = int.Parse(berittenebogenschuetzenAnzahl), SchwereKavallerieAnzahl = int.Parse(schwerekavallerieAnzahl), RammboeckeAnzahl = int.Parse(rammboeckeAnzahl), KatapulteAnzahl = int.Parse(katapulteAnzahl), PaladinAnzahl = int.Parse(paladinAnzahl), AdelsgeschlechtAnzahl = int.Parse(adelsgeschlechtAnzahl)};
                                                        accountsettings.Unterstuetzungen.Add(unterstuetzung);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            } catch (Exception ex) {
                                App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + ex.Message + "\n");
                            }

                            App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + Resources.TruppenEinlesen + " " + Resources.Ended);
                            accountsettings.LastUnterstuetzungeneinlesen = DateTime.Now;
                        }
                    } catch (Exception ex) {
                        App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + ex.Message + "\n");
                    }

                    browser.GetsUsed = false;
                    TruppeneinlesenInDoing = false;
                }
            });
        }

        public static TruppenTemplate GetAvailableTroops(Browser browser, AccountDorfSettings dorf) {
            var template = new TruppenTemplate();
            var truppen = browser.WebDriver.FindElements(By.XPath("//a[contains(@id, 'units_entry_all')]")).ToList();
            for (var i = 0; i < truppen.Count; i++) {
                var index = 0;
                if (i == index++) {
                    template.SpeertraegerAnzahl = int.Parse(truppen[i].Text.Replace("(", "").Replace(")", ""));
                }

                if (i == index++) {
                    template.SchwertkaempferAnzahl = int.Parse(truppen[i].Text.Replace("(", "").Replace(")", ""));
                }

                if (i == index++) {
                    template.AxtkaempferAnzahl = int.Parse(truppen[i].Text.Replace("(", "").Replace(")", ""));
                }

                if (App.Settings.Weltensettings.BogenschuetzenActive && i == index++) {
                    template.BogenschuetzenAnzahl = int.Parse(truppen[i].Text.Replace("(", "").Replace(")", ""));
                }

                if (i == index++) {
                    template.SpaeherAnzahl = int.Parse(truppen[i].Text.Replace("(", "").Replace(")", ""));
                }

                if (i == index++) {
                    template.LeichteKavallerieAnzahl = int.Parse(truppen[i].Text.Replace("(", "").Replace(")", ""));
                }

                if (App.Settings.Weltensettings.BogenschuetzenActive && i == index++) {
                    template.BeritteneBogenschuetzenAnzahl = int.Parse(truppen[i].Text.Replace("(", "").Replace(")", ""));
                }

                if (i == index++) {
                    template.SchwereKavallerieAnzahl = int.Parse(truppen[i].Text.Replace("(", "").Replace(")", ""));
                }

                if (i == index++) {
                    template.RammboeckeAnzahl = int.Parse(truppen[i].Text.Replace("(", "").Replace(")", ""));
                }

                if (i == index++) {
                    template.KatapulteAnzahl = int.Parse(truppen[i].Text.Replace("(", "").Replace(")", ""));
                }

                if (App.Settings.Weltensettings.PaladinActive && i == index++) {
                    template.PaladinAnzahl = int.Parse(truppen[i].Text.Replace("(", "").Replace(")", ""));
                }

                if (i == index) {
                    template.AdelsgeschlechterAnzahl = int.Parse(truppen[i].Text.Replace("(", "").Replace(")", ""));
                }
            }

            return template;
        }

        public static async Task UpdateVillageTroopsOfAccountOverRallyPoint(Browser browser, Accountsetting accountsettings) {
            if ((DateTime.Now - accountsettings.LastTruppeneinlesen).TotalMinutes > 30) {
                foreach (var dorf in accountsettings.DoerferSettings.Where(x => !string.IsNullOrEmpty(x.Dorf.Dorf.VillageName) && x.Dorf.PlayerId.Equals(accountsettings.Playerid))) {
                    if (dorf.LastTruppeneinlesen < DateTime.Now.AddMinutes(30)) {
                        var link = App.Settings.OperateLink + "village=" + dorf.Dorf.VillageId + "" + accountsettings.UvZusatzFuerLink + "&screen=place";

                        if (!browser.WebDriver.Url.Equals(link)) {
                            browser.WebDriver.Navigate().GoToUrl(link);
                            await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings, link);
                        }

                        dorf.Dorfinformationen.Truppen = GetAvailableTroops(browser, dorf);
                    }
                }

                accountsettings.LastTruppeneinlesen = DateTime.Now;
            }
        }

        public static async Task<SynchronizeVillage> GetVillageWithEnoughTroopsForSync(Browser browser, Accountsetting accountsettings) {
            foreach (var dorf in accountsettings.DoerferSettings.Where(x => !string.IsNullOrEmpty(x.Dorf.Dorf.VillageName) && x.Dorf.PlayerId.Equals(accountsettings.Playerid))) {
                var link = App.Settings.OperateLink + "village=" + dorf.Dorf.VillageId + "" + accountsettings.UvZusatzFuerLink + "&screen=place";
                browser.WebDriver.Navigate().GoToUrl(link);
                await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings, link);
                var truppentemplate = GetAvailableTroops(browser, dorf);
                try {
                    var commands = browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Dorfuebersicht.ANGRIFFE_SELECTOR_XPATH)).Where(x => x.Text.Contains(NAMECONSTANTS.GetNameForServer(Names.Angriff))).ToList();

                    if (!commands.Any() || commands.Count < App.Settings.SynchronizeMaxBefehleCount) {
                        var templates = new List<TruppenTemplate>();


                        //switch (App.Settings.UseTroopForSynchronize) {
                        //    case "spear":
                        templates.Add(new TruppenTemplate {SpeertraegerAnzahl = App.Settings.SynchronizeTroopCount});
                        //counter = 0;
                        //    break;
                        //case "sword":
                        templates.Add(new TruppenTemplate {SchwertkaempferAnzahl = App.Settings.SynchronizeTroopCount});
                        // counter =1;
                        //    break;
                        //case "axe":
                        templates.Add(new TruppenTemplate {AxtkaempferAnzahl = App.Settings.SynchronizeTroopCount});
                        // counter = 2;
                        //    break;
                        //case "archer":
                        templates.Add(new TruppenTemplate {BogenschuetzenAnzahl = App.Settings.SynchronizeTroopCount});
                        // counter =3;
                        //    break;
                        //case "spy":
                        templates.Add(new TruppenTemplate {SpaeherAnzahl = App.Settings.SynchronizeTroopCount});
                        //counter = 4;
                        //    break;
                        //case "light":
                        templates.Add(new TruppenTemplate {LeichteKavallerieAnzahl = App.Settings.SynchronizeTroopCount});
                        //counter = 5;
                        //    break;
                        //case "marcher":
                        templates.Add(new TruppenTemplate {BeritteneBogenschuetzenAnzahl = App.Settings.SynchronizeTroopCount});
                        //counter = 6;
                        //    break;
                        //case "heavy":
                        templates.Add(new TruppenTemplate {SchwereKavallerieAnzahl = App.Settings.SynchronizeTroopCount});
                        //counter = 7;
                        //    break;
                        //case "ram":
                        templates.Add(new TruppenTemplate {RammboeckeAnzahl = App.Settings.SynchronizeTroopCount});
                        // counter = 8;
                        //    break;
                        //case "cat":
                        templates.Add(new TruppenTemplate {KatapulteAnzahl = App.Settings.SynchronizeTroopCount});
                        //counter = 9;
                        // break;
                        //}

                        int counter = 0;
                        foreach (var template in templates) {
                            if (FarmModul.HasEnoughTroopsExact(truppentemplate, template)) {
                                return new SynchronizeVillage {Truppe = counter, VillageId = dorf.Dorf.VillageId};
                            }

                            counter++;
                        }
                    }
                } catch (Exception) {
                    // ignored
                }
            }

            return new SynchronizeVillage {Truppe = 0, VillageId = 0};
        }

        public static async Task GetDorfinformationenDorfUebersicht(Browser browser, Accountsetting accountsettings, AccountDorfSettings dorf) {
            if (accountsettings.LastDorfUebersichtCheck < DateTime.Now.AddHours(-3)) {
                var link = App.Settings.OperateLink + "village=" + dorf.Dorf.VillageId + "" + accountsettings.UvZusatzFuerLink + "&screen=overview";
                browser.WebDriver.Navigate().GoToUrl(link);
                await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings);
                var dt = DateTime.MinValue;
                if (dorf.Dorfinformationen.NoobschutzaktivBis == DateTime.MinValue) {
                    try {
                        if (browser.WebDriver.FindElement(By.ClassName("vis_item")).Displayed) {
                            var temp = browser.WebDriver.FindElement(By.XPath("//div[@class='vis_item']")).Text;
                            var temparray = temp.Split(' ');
                            if (!temp.Contains(Resources.ZeitMorgen)) {
                                dt = new DateTime(DateTime.Now.Year, int.Parse(temparray[4].Split('.')[1]), int.Parse(temparray[4].Split('.')[0]), int.Parse(temparray[6].Split(':')[0]), int.Parse(temparray[6].Split(':')[1]), int.Parse(temparray[6].Split(':')[2]));
                            }
                            else {
                                dt = new DateTime(DateTime.Now.Year, DateTime.Now.AddDays(1).Month, DateTime.Now.AddDays(1).Day, int.Parse(temparray[5].Split(':')[0]), int.Parse(temparray[5].Split(':')[1]), int.Parse(temparray[5].Split(':')[2]));
                            }
                        }
                    } catch (Exception) {
                        // ignored
                    }

                    dorf.Dorfinformationen.NoobschutzaktivBis = dt;
                }

                dorf.Dorfinformationen.Speicher = new Beute {Holz = int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Dorfuebersicht.SPEICHER_HOLZ_SELECTOR_XPATH)).Text), Lehm = int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Dorfuebersicht.SPEICHER_LEHM_SELECTOR_XPATH)).Text), Eisen = int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Dorfuebersicht.SPEICHER_EISEN_SELECTOR_XPATH)).Text)};
                dorf.Dorfinformationen.StorageMax = int.Parse(browser.WebDriver.FindElement(By.XPath("//span[@id='storage']")).Text);
                dorf.Dorfinformationen.FarmActual = int.Parse(browser.WebDriver.FindElement(By.XPath("//span[@id='pop_current_label']")).Text);
                dorf.Dorfinformationen.FarmMax = int.Parse(browser.WebDriver.FindElement(By.XPath("//span[@id='pop_max_label']")).Text);
                int.TryParse(browser.WebDriver.FindElement(By.XPath("//span[@id='premium_points']")).Text.Trim(), out int premium);
                accountsettings.MultiaccountingDaten.PremiumPunkte = premium;
                var text = browser.WebDriver.FindElement(By.Id("show_units")).Text.ToUpper();
                if (text.Contains(NAMECONSTANTS.GetNameForServer(Names.Alle).ToUpper()))
                {
                    browser.ClickByCss("a[class=units-widget-next]"); 
                    text = browser.WebDriver.FindElement(By.Id("show_units")).Text.ToUpper();
                } else if (text.Contains(NAMECONSTANTS.GetNameForServer(Names.Alle).ToUpper()))
                {
                    browser.ClickByCss("a[class=units-widget-prev]"); 
                    text = browser.WebDriver.FindElement(By.Id("show_units")).Text.ToUpper();
                }
                var temp2 = text.Split('\n');
                dorf.Dorfinformationen.Truppen = new TruppenTemplate();
                for (var i = 2; i < temp2.Length - 2; i++) {
                    var tempp = temp2[i].Split(' ');
                    string tempppp;
                    if (tempp.Length > 1) {
                        tempppp = tempp[1].Trim();
                    }
                    else {
                        tempppp = tempp[0].Trim();
                    }

                    var amount = int.Parse(tempp[0]);
                    //TODO Anderst machen! über bilder!

                    if (tempppp.Contains(NAMECONSTANTS.GetNameForServer(Names.Speertraeger))) {
                        dorf.Dorfinformationen.Truppen.SpeertraegerAnzahl = amount;
                    }

                    if (tempppp.Contains(NAMECONSTANTS.GetNameForServer(Names.Schwertkaempfer))) {
                        dorf.Dorfinformationen.Truppen.SchwertkaempferAnzahl = amount;
                    }

                    if (tempppp.Contains(NAMECONSTANTS.GetNameForServer(Names.Axtkaempfer))) {
                        dorf.Dorfinformationen.Truppen.AxtkaempferAnzahl = amount;
                    }

                    if (tempppp.Contains(NAMECONSTANTS.GetNameForServer(Names.Bogenschuetzen))) {
                        dorf.Dorfinformationen.Truppen.BogenschuetzenAnzahl = amount;
                    }

                    if (tempppp.Contains(NAMECONSTANTS.GetNameForServer(Names.Spaeher))) {
                        dorf.Dorfinformationen.Truppen.SpaeherAnzahl = amount;
                    }

                    if (tempppp.Contains(NAMECONSTANTS.GetNameForServer(Names.LeichteKavallerie))) {
                        dorf.Dorfinformationen.Truppen.LeichteKavallerieAnzahl = amount;
                    }

                    if (tempppp.Contains(NAMECONSTANTS.GetNameForServer(Names.BeritteneBogenschuetzen))) {
                        dorf.Dorfinformationen.Truppen.BeritteneBogenschuetzenAnzahl = amount;
                    }

                    if (tempppp.Contains(NAMECONSTANTS.GetNameForServer(Names.SchwereKavallerie))) {
                        dorf.Dorfinformationen.Truppen.SchwereKavallerieAnzahl = amount;
                    }

                    if (tempppp.Contains(NAMECONSTANTS.GetNameForServer(Names.Rammboecke))) {
                        dorf.Dorfinformationen.Truppen.RammboeckeAnzahl = amount;
                    }

                    if (tempppp.Contains(NAMECONSTANTS.GetNameForServer(Names.Katapulte))) {
                        dorf.Dorfinformationen.Truppen.KatapulteAnzahl = amount;
                    }

                    if (tempppp.Contains(NAMECONSTANTS.GetNameForServer(Names.Paladin)) || tempppp.Equals(accountsettings.Accountname) && accountsettings.MultiaccountingDaten.Accountmode == Accountmode.Botaccount) {
                        dorf.Dorfinformationen.Truppen.PaladinAnzahl = amount;
                    }

                    if (tempppp.Contains(NAMECONSTANTS.GetNameForServer(Names.Adelsgeschlecht))) {
                        dorf.Dorfinformationen.Truppen.AdelsgeschlechterAnzahl = amount;
                    }
                }

                accountsettings.LastDorfUebersichtCheck = DateTime.Now;
            }
        }

        public static async Task GetDorfinformationenHauptgebaeude(Browser browser, Accountsetting accountsettings, AccountDorfSettings dorf) {
            string link = App.Settings.OperateLink +
                          // ReSharper disable once ConditionIsAlwaysTrueOrFalse
                          (dorf != null ? "village=" + dorf.Dorf.VillageId : "") + accountsettings.UvZusatzFuerLink + "&screen=main";
            if (!browser.WebDriver.Url.Equals(link)) {
                browser.WebDriver.Navigate().GoToUrl(link);
                browser.WebDriver.WaitForPageload();
            }

            await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings);
            browser.WebDriver.WaitForPageload();
            if (dorf != null) {
                dorf.Dorfinformationen.Speicher = new Beute {Holz = int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Dorfuebersicht.SPEICHER_HOLZ_SELECTOR_XPATH)).Text), Lehm = int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Dorfuebersicht.SPEICHER_LEHM_SELECTOR_XPATH)).Text), Eisen = int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Dorfuebersicht.SPEICHER_EISEN_SELECTOR_XPATH)).Text)};
                dorf.Dorfinformationen.StorageMax = int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Dorfuebersicht.SPEICHER_SELECTOR_XPATH)).Text);
                dorf.Dorfinformationen.FarmActual = int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Dorfuebersicht.AKTUELLBH_SELECTOR_XPATH)).Text);
                dorf.Dorfinformationen.FarmMax = int.Parse(browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Dorfuebersicht.MAXBH_SELECTOR_XPATH)).Text);
                if (dorf.Dorfinformationen.GebaeudeStufen == null) {
                    dorf.Dorfinformationen.GebaeudeStufen = new Gebaeude();
                }

                try {
                    dorf.Dorfinformationen.GebaeudeStufen.Hauptgebaeude = int.Parse(browser.WebDriver.FindElement(By.XPath("//tr[@id='main_buildrow_main']/descendant::span[contains(text(),'" + NAMECONSTANTS.GetNameForServer(Names.Stufe) + "')]")).Text.Replace("" + NAMECONSTANTS.GetNameForServer(Names.Stufe) + " ", ""));
                } catch {
                    dorf.Dorfinformationen.GebaeudeStufen.Hauptgebaeude = 0;
                }

                try {
                    dorf.Dorfinformationen.GebaeudeStufen.Kaserne = int.Parse(browser.WebDriver.FindElement(By.XPath("//tr[@id='main_buildrow_barracks']/descendant::span[contains(text(),'" + NAMECONSTANTS.GetNameForServer(Names.Stufe) + "')]")).Text.Replace("" + NAMECONSTANTS.GetNameForServer(Names.Stufe) + " ", ""));
                } catch (Exception) {
                    dorf.Dorfinformationen.GebaeudeStufen.Kaserne = 0;
                }

                try {
                    dorf.Dorfinformationen.GebaeudeStufen.Stall = int.Parse(browser.WebDriver.FindElement(By.XPath("//tr[@id='main_buildrow_stable']/descendant::span[contains(text(),'" + NAMECONSTANTS.GetNameForServer(Names.Stufe) + "')]")).Text.Replace("" + NAMECONSTANTS.GetNameForServer(Names.Stufe) + " ", ""));
                } catch (Exception) {
                    dorf.Dorfinformationen.GebaeudeStufen.Stall = 0;
                }

                try {
                    dorf.Dorfinformationen.GebaeudeStufen.Werkstatt = int.Parse(browser.WebDriver.FindElement(By.XPath("//tr[@id='main_buildrow_garage']/descendant::span[contains(text(),'" + NAMECONSTANTS.GetNameForServer(Names.Stufe) + "')]")).Text.Replace("" + NAMECONSTANTS.GetNameForServer(Names.Stufe) + " ", ""));
                } catch (Exception) {
                    dorf.Dorfinformationen.GebaeudeStufen.Werkstatt = 0;
                }

                try {
                    dorf.Dorfinformationen.GebaeudeStufen.ErsteKirche = int.Parse(browser.WebDriver.FindElement(By.XPath("//tr[@id='main_buildrow_church_f']/descendant::span[contains(text(),'" + NAMECONSTANTS.GetNameForServer(Names.Stufe) + "')]")).Text.Replace("" + NAMECONSTANTS.GetNameForServer(Names.Stufe) + " ", ""));
                } catch (Exception) {
                    dorf.Dorfinformationen.GebaeudeStufen.ErsteKirche = 0;
                }

                try {
                    dorf.Dorfinformationen.GebaeudeStufen.Kirche = int.Parse(browser.WebDriver.FindElement(By.XPath("//tr[@id='main_buildrow_church']/descendant::span[contains(text(),'" + NAMECONSTANTS.GetNameForServer(Names.Stufe) + "')]")).Text.Replace("" + NAMECONSTANTS.GetNameForServer(Names.Stufe) + " ", ""));
                } catch (Exception) {
                    dorf.Dorfinformationen.GebaeudeStufen.Kirche = 0;
                }

                try {
                    dorf.Dorfinformationen.GebaeudeStufen.Schmiede = int.Parse(browser.WebDriver.FindElement(By.XPath("//tr[@id='main_buildrow_smith']/descendant::span[contains(text(),'" + NAMECONSTANTS.GetNameForServer(Names.Stufe) + "')]")).Text.Replace("" + NAMECONSTANTS.GetNameForServer(Names.Stufe) + " ", ""));
                } catch (Exception) {
                    dorf.Dorfinformationen.GebaeudeStufen.Schmiede = 0;
                }

                try {
                    dorf.Dorfinformationen.GebaeudeStufen.Versammlungsplatz = int.Parse(browser.WebDriver.FindElement(By.XPath("//tr[@id='main_buildrow_place']/descendant::span[contains(text(),'" + NAMECONSTANTS.GetNameForServer(Names.Stufe) + "')]")).Text.Replace("" + NAMECONSTANTS.GetNameForServer(Names.Stufe) + " ", ""));
                } catch (Exception) {
                    dorf.Dorfinformationen.GebaeudeStufen.Versammlungsplatz = 0;
                }

                try {
                    dorf.Dorfinformationen.GebaeudeStufen.Statue = int.Parse(browser.WebDriver.FindElement(By.XPath("//tr[@id='main_buildrow_statue']/descendant::span[contains(text(),'" + NAMECONSTANTS.GetNameForServer(Names.Stufe) + "')]")).Text.Replace("" + NAMECONSTANTS.GetNameForServer(Names.Stufe) + " ", ""));
                } catch (Exception) {
                    dorf.Dorfinformationen.GebaeudeStufen.Statue = 0;
                }

                try {
                    dorf.Dorfinformationen.GebaeudeStufen.Marktplatz = int.Parse(browser.WebDriver.FindElement(By.XPath("//tr[@id='main_buildrow_market']/descendant::span[contains(text(),'" + NAMECONSTANTS.GetNameForServer(Names.Stufe) + "')]")).Text.Replace("" + NAMECONSTANTS.GetNameForServer(Names.Stufe) + " ", ""));
                } catch (Exception) {
                    dorf.Dorfinformationen.GebaeudeStufen.Marktplatz = 0;
                }

                try {
                    dorf.Dorfinformationen.GebaeudeStufen.Holzfaeller = int.Parse(browser.WebDriver.FindElement(By.XPath("//tr[@id='main_buildrow_wood']/descendant::span[contains(text(),'" + NAMECONSTANTS.GetNameForServer(Names.Stufe) + "')]")).Text.Replace("" + NAMECONSTANTS.GetNameForServer(Names.Stufe) + " ", ""));
                } catch (Exception) {
                    dorf.Dorfinformationen.GebaeudeStufen.Holzfaeller = 0;
                }

                try {
                    dorf.Dorfinformationen.GebaeudeStufen.Lehmgrube = int.Parse(browser.WebDriver.FindElement(By.XPath("//tr[@id='main_buildrow_stone']/descendant::span[contains(text(),'" + NAMECONSTANTS.GetNameForServer(Names.Stufe) + "')]")).Text.Replace("" + NAMECONSTANTS.GetNameForServer(Names.Stufe) + " ", ""));
                } catch (Exception) {
                    dorf.Dorfinformationen.GebaeudeStufen.Lehmgrube = 0;
                }

                try {
                    dorf.Dorfinformationen.GebaeudeStufen.Eisenmine = int.Parse(browser.WebDriver.FindElement(By.XPath("//tr[@id='main_buildrow_iron']/descendant::span[contains(text(),'" + NAMECONSTANTS.GetNameForServer(Names.Stufe) + "')]")).Text.Replace("" + NAMECONSTANTS.GetNameForServer(Names.Stufe) + " ", ""));
                } catch (Exception) {
                    dorf.Dorfinformationen.GebaeudeStufen.Eisenmine = 0;
                }

                try {
                    dorf.Dorfinformationen.GebaeudeStufen.Bauernhof = int.Parse(browser.WebDriver.FindElement(By.XPath("//tr[@id='main_buildrow_farm']/descendant::span[contains(text(),'" + NAMECONSTANTS.GetNameForServer(Names.Stufe) + "')]")).Text.Replace("" + NAMECONSTANTS.GetNameForServer(Names.Stufe) + " ", ""));
                } catch (Exception) {
                    dorf.Dorfinformationen.GebaeudeStufen.Bauernhof = 0;
                }

                try {
                    dorf.Dorfinformationen.GebaeudeStufen.Speicher = int.Parse(browser.WebDriver.FindElement(By.XPath("//tr[@id='main_buildrow_storage']/descendant::span[contains(text(),'" + NAMECONSTANTS.GetNameForServer(Names.Stufe) + "')]")).Text.Replace("" + NAMECONSTANTS.GetNameForServer(Names.Stufe) + " ", ""));
                } catch (Exception) {
                    dorf.Dorfinformationen.GebaeudeStufen.Speicher = 0;
                }

                try {
                    dorf.Dorfinformationen.GebaeudeStufen.Versteck = int.Parse(browser.WebDriver.FindElement(By.XPath("//tr[@id='main_buildrow_hide']/descendant::span[contains(text(),'" + NAMECONSTANTS.GetNameForServer(Names.Stufe) + "')]")).Text.Replace("" + NAMECONSTANTS.GetNameForServer(Names.Stufe) + " ", ""));
                } catch (Exception) {
                    dorf.Dorfinformationen.GebaeudeStufen.Versteck = 0;
                }

                try {
                    dorf.Dorfinformationen.GebaeudeStufen.Wall = int.Parse(browser.WebDriver.FindElement(By.XPath("//tr[@id='main_buildrow_wall']/descendant::span[contains(text(),'" + NAMECONSTANTS.GetNameForServer(Names.Stufe) + "')]")).Text.Replace("" + NAMECONSTANTS.GetNameForServer(Names.Stufe) + " ", ""));
                } catch (Exception) {
                    dorf.Dorfinformationen.GebaeudeStufen.Wall = 0;
                }
            }
        }
    }
}