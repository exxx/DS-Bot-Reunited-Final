﻿using System;
using System.ComponentModel;

namespace Ds_Bot_Reunited_NEWUI.Constants {
    [Serializable]
    public enum SERVERCONSTANTS {
        [Description("die-staemme.de")] Diestaemme,
        [Description("tribalwars.net")] Tribalwarsnet,
        [Description("tribalwars.co.uk")] Tribalwarscouk,
        [Description("staemme.ch")] Staemmech,
        [Description("tribalwars.us")] tribalwarsus
    }
}