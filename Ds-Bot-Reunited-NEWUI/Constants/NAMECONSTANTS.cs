﻿namespace Ds_Bot_Reunited_NEWUI.Constants {
    public enum Names {
        Alle,
        Andere,
        ReportAnzahl,
        ReportAttacker,
        ReportBarracks,
        ReportBetreff,
        ReportBeute,
        ReportChurch,
        ReportEffekte,
        ReportEinheitenAußerhalb,
        ReportEisen,
        ReportErspaehte,
        ReportErspaehteRohstoffe,
        ReportFarm,
        ReportFarmassistent,
        ReportFirstChurch,
        ReportFlagge,
        ReportGebaeudeStufe,
        ReportGlauben,
        ReportHerkunft,
        ReportHolz,
        ReportKaempfer,
        ReportKampfzeit,
        ReportLehm,
        ReportList,
        ReportMain,
        ReportMarktplatz,
        ReportMoegliche,
        ReportMoeglicheRohstoffe,
        ReportMoral,
        ReportNachtbonus,
        ReportNeueBerichte,
        ReportPaladin,
        ReportSmith,
        ReportSnob,
        ReportSpeicher,
        ReportStable,
        ReportStatue,
        ReportVerluste,
        ReportVersammlungsplatz,
        ReportVersteck,
        ReportVerteidiger,
        ReportWachturm,
        ReportWall,
        ReportWorkshop,
        Stufe,
        Speertraeger,
        Schwertkaempfer,
        Axtkaempfer,
        Bogenschuetzen,
        Spaeher,
        LeichteKavallerie,
        BeritteneBogenschuetzen,
        SchwereKavallerie,
        Rammboecke,
        Katapulte,
        Paladin,
        Adelsgeschlecht,
        Heute,
        Morgen,
        Rueckkehr,
        Angriff,
        Farmlimit,
        Ramm,
        Kata,
        Leichte,
        Schwere,
        Berittene,
        DerAngriffstrupp,
        Unterstuetzung,
        Genug
    }

    public static class NAMECONSTANTS {
        public static string GetNameForServer(Names name) {
            switch (App.Settings.Server) {
                case SERVERCONSTANTS.Diestaemme:
                    switch (name) {
                        case Names.Alle:
                            return "alle";
                        case Names.Andere:
                            return "andere";
                        case Names.ReportAnzahl:
                            return "Anzahl";
                        case Names.ReportAttacker:
                            return "Angreifer";
                        case Names.ReportBarracks: return "Kaserne";
                        case Names.ReportBetreff: return "Betreff";
                        case Names.ReportBeute: return "Beute";
                        case Names.ReportChurch: return "Kirche";
                        case Names.ReportEffekte: return "Effekte";
                        case Names.ReportEinheitenAußerhalb: return "Einheiten außerhalb";
                        case Names.ReportEisen: return "Eisenmine";
                        case Names.ReportErspaehte: return "Erspähte";
                        case Names.ReportErspaehteRohstoffe: return "Erspähte Rohstoffe";
                        case Names.ReportFarm: return "Bauernhof";
                        case Names.ReportFarmassistent: return "Farm-Assistent";
                        case Names.ReportFirstChurch: return"Erste Kirche";
                        case Names.ReportFlagge: return "Flagge";
                        case Names.ReportGebaeudeStufe: return"Gebäude Stufe";
                        case Names.ReportGlauben: return "Glauben";
                        case Names.ReportHerkunft: return"Herkunft";
                        case Names.ReportHolz: return"Holzfällerlager";
                        case Names.ReportKaempfer: return"Kämpfer";
                        case Names.ReportKampfzeit: return"Kampfzeit";
                        case Names.ReportLehm: return"Lehmgrube";
                        case Names.ReportList: return"Reportlist";
                        case Names.ReportMain: return"Hauptgebäude";
                        case Names.ReportMarktplatz: return"Marktplatz";
                        case Names.ReportMoegliche: return"Mögliche";
                        case Names.ReportMoeglicheRohstoffe: return"Mögliche Rohstoffe";
                        case Names.ReportMoral: return "Moral";
                        case Names.ReportNachtbonus: return"Nachtbonus";
                        case Names.ReportNeueBerichte: return"Neue Berichte";
                        case Names.ReportPaladin: return "Paladin";
                        case Names.ReportSmith: return"Schmiede";
                        case Names.ReportSnob: return"Adelshof";
                        case Names.ReportSpeicher: return"Speicher";
                        case Names.ReportStable: return"Stall";
                        case Names.ReportStatue: return"Statue";
                        case Names.ReportVerluste: return"Verluste";
                        case Names.ReportVersammlungsplatz: return"Versammlungsplatz";
                        case Names.ReportVersteck: return "Versteck";
                        case Names.ReportVerteidiger: return"Verteidiger";
                        case Names.ReportWachturm: return"Wachturm";
                        case Names.ReportWall: return"Wall";
                        case Names.ReportWorkshop: return"Werkstatt";
                        case Names.Stufe:
                            return "Stufe";
                        case Names.Speertraeger:
                            return "Speerträger";
                        case Names.Schwertkaempfer:
                            return "Schwertkämpfer";
                        case Names.Axtkaempfer:
                            return "Axtkämpfer";
                        case Names.Bogenschuetzen:
                            return "Bogenschützen";
                        case Names.Spaeher:
                            return "Späher";
                        case Names.LeichteKavallerie:
                            return "Leichte";
                        case Names.BeritteneBogenschuetzen:
                            return "Berittene";
                        case Names.SchwereKavallerie:
                            return "Schwere";
                        case Names.Rammboecke:
                            return "Rammböcke";
                        case Names.Katapulte:
                            return "Katapulte";
                        case Names.Paladin:
                            return "Paladin";
                        case Names.Adelsgeschlecht:
                            return "Adelsgeschlechter";
                        case Names.Heute:
                            return "heute";
                        case Names.Morgen:
                            return "morgen";
                        case Names.Rueckkehr:
                            return "Rückkehr";
                        case Names.Angriff:
                            return "Angriff";
                        case Names.Farmlimit:
                            return "Du konntest nicht alle Rohstoffe erbeuten, weil du bereits das Beutelimit erreicht hast.";
                        case Names.Ramm:
                            return "Ramm";
                        case Names.Kata:
                            return "Kata";
                        case Names.Leichte:
                            return "Leichte";
                        case Names.Schwere:
                            return "Schwere";
                        case Names.Berittene:
                            return "Berittene";
                        case Names.DerAngriffstrupp:
                            return "Der Angriffstrupp";
                        case Names.Unterstuetzung:
                            return "Unterstützung";
                        case Names.Genug:
                            return "Genug";
                    }
                    return "";
                case SERVERCONSTANTS.Tribalwarsnet:
                    switch (name) {
                        case Names.Alle:
                            return "all";
                        case Names.Andere:
                            return "other";
                        case Names.ReportAnzahl:
                            return "Quantity";
                        case Names.ReportAttacker:
                            return "Attacker";
                        case Names.ReportBarracks:
                            return "Barracks";
                        case Names.ReportBetreff:
                            return "Subject";
                        case Names.ReportBeute:
                            return "Haul";
                        case Names.ReportChurch:
                            return "Church";
                        case Names.ReportEffekte:
                            return "Effekte";
                        case Names.ReportEinheitenAußerhalb:
                            return "Einheiten außerhalb";
                        case Names.ReportEisen:
                            return "Iron mine";
                        case Names.ReportErspaehte:
                            return "Erspähte";
                        case Names.ReportErspaehteRohstoffe:
                            return "Erspähte Rohstoffe";
                        case Names.ReportFarm:
                            return "Farm";
                        case Names.ReportFarmassistent:
                            return "Loot-Assist";
                        case Names.ReportFirstChurch:
                            return "First Church";
                        case Names.ReportFlagge:
                            return "Flagge";
                        case Names.ReportGebaeudeStufe:
                            return "Gebäude Stufe";
                        case Names.ReportGlauben:
                            return "Glauben";
                        case Names.ReportHerkunft:
                            return "Origin";
                        case Names.ReportHolz:
                            return "Timber camp";
                        case Names.ReportKaempfer:
                            return "Attacker";
                        case Names.ReportKampfzeit:
                            return "Battle time";
                        case Names.ReportLehm:
                            return "Clay pit";
                        case Names.ReportList:
                            return "Reportlist";
                        case Names.ReportMain:
                            return "Village Headquarters";
                        case Names.ReportMarktplatz:
                            return "Market";
                        case Names.ReportMoegliche:
                            return "Mögliche";
                        case Names.ReportMoeglicheRohstoffe:
                            return "Mögliche Rohstoffe";
                        case Names.ReportMoral:
                            return "Morale";
                        case Names.ReportNachtbonus:
                            return "Nachtbonus";
                        case Names.ReportNeueBerichte:
                            return "Neue Berichte";
                        case Names.ReportPaladin:
                            return "Paladin";
                        case Names.ReportSmith:
                            return "Smithy";
                        case Names.ReportSnob:
                            return "Academy";
                        case Names.ReportSpeicher:
                            return "Warehouse";
                        case Names.ReportStable:
                            return "Stable";
                        case Names.ReportStatue:
                            return "Statue";
                        case Names.ReportVerluste:
                            return "Losses";
                        case Names.ReportVersammlungsplatz:
                            return "Rally point";
                        case Names.ReportVersteck:
                            return "Hiding place";
                        case Names.ReportVerteidiger:
                            return "Defender";
                        case Names.ReportWachturm:
                            return "Watchtower";
                        case Names.ReportWall:
                            return "Wall";
                        case Names.ReportWorkshop:
                            return "Workshop";
                        case Names.Stufe:
                            return "Level";
                        case Names.Speertraeger:
                            return "Spear";
                        case Names.Schwertkaempfer:
                            return "Swordsman";
                        case Names.Axtkaempfer:
                            return "Axeman";
                        case Names.Bogenschuetzen:
                            return "Archer";
                        case Names.Spaeher:
                            return "Scout";
                        case Names.LeichteKavallerie:
                            return "Light";
                        case Names.BeritteneBogenschuetzen:
                            return "Mounted";
                        case Names.SchwereKavallerie:
                            return "Heavy";
                        case Names.Rammboecke:
                            return "Ram";
                        case Names.Katapulte:
                            return "Catapult";
                        case Names.Paladin:
                            return "Paladin";
                        case Names.Adelsgeschlecht:
                            return "Nobleman";
                        case Names.Heute:
                            return "today";
                        case Names.Morgen:
                            return "tomorrow";
                        case Names.Rueckkehr:
                            return "return";
                        case Names.Angriff:
                            return "Attack";
                        case Names.Farmlimit:
                            return "";
                        case Names.Ramm:
                            return "Ram";
                        case Names.Kata:
                            return "Cat";
                        case Names.Leichte:
                            return "Light";
                        case Names.Schwere:
                            return "Heavy";
                        case Names.Berittene:
                            return "Mounted";
                        case Names.DerAngriffstrupp:
                            return "Der Angriffstrupp";
                        case Names.Unterstuetzung:
                            return "Support";
                        case Names.Genug:
                            return "enough";
                    }
                    return "";
                case SERVERCONSTANTS.Tribalwarscouk:
                    switch (name) {
                        case Names.Alle:
                            return "all";
                        case Names.Andere:
                            return "other";
                        case Names.ReportAnzahl:
                            return "Quantity";
                        case Names.ReportAttacker:
                            return "Attacker";
                        case Names.ReportBarracks:
                            return "Barracks";
                        case Names.ReportBetreff:
                            return "Subject";
                        case Names.ReportBeute:
                            return "Haul";
                        case Names.ReportChurch:
                            return "Church";
                        case Names.ReportEffekte:
                            return "Effekte";
                        case Names.ReportEinheitenAußerhalb:
                            return "Einheiten außerhalb";
                        case Names.ReportEisen:
                            return "Iron mine";
                        case Names.ReportErspaehte:
                            return "Erspähte";
                        case Names.ReportErspaehteRohstoffe:
                            return "Erspähte Rohstoffe";
                        case Names.ReportFarm:
                            return "Farm";
                        case Names.ReportFarmassistent:
                            return "Loot-Assist";
                        case Names.ReportFirstChurch:
                            return "First Church";
                        case Names.ReportFlagge:
                            return "Flagge";
                        case Names.ReportGebaeudeStufe:
                            return "Gebäude Stufe";
                        case Names.ReportGlauben:
                            return "Glauben";
                        case Names.ReportHerkunft:
                            return "Origin";
                        case Names.ReportHolz:
                            return "Timber camp";
                        case Names.ReportKaempfer:
                            return "Attacker";
                        case Names.ReportKampfzeit:
                            return "Battle time";
                        case Names.ReportLehm:
                            return "Clay pit";
                        case Names.ReportList:
                            return "Reportlist";
                        case Names.ReportMain:
                            return "Village Headquarters";
                        case Names.ReportMarktplatz:
                            return "Market";
                        case Names.ReportMoegliche:
                            return "Mögliche";
                        case Names.ReportMoeglicheRohstoffe:
                            return "Mögliche Rohstoffe";
                        case Names.ReportMoral:
                            return "Morale";
                        case Names.ReportNachtbonus:
                            return "Nachtbonus";
                        case Names.ReportNeueBerichte:
                            return "Neue Berichte";
                        case Names.ReportPaladin:
                            return "Paladin";
                        case Names.ReportSmith:
                            return "Smithy";
                        case Names.ReportSnob:
                            return "Academy";
                        case Names.ReportSpeicher:
                            return "Warehouse";
                        case Names.ReportStable:
                            return "Stable";
                        case Names.ReportStatue:
                            return "Statue";
                        case Names.ReportVerluste:
                            return "Losses";
                        case Names.ReportVersammlungsplatz:
                            return "Rally point";
                        case Names.ReportVersteck:
                            return "Hiding place";
                        case Names.ReportVerteidiger:
                            return "Defender";
                        case Names.ReportWachturm:
                            return "Watchtower";
                        case Names.ReportWall:
                            return "Wall";
                        case Names.ReportWorkshop:
                            return "Workshop";
                        case Names.Stufe:
                            return "Level";
                        case Names.Speertraeger:
                            return "Spear";
                        case Names.Schwertkaempfer:
                            return "Swordsman";
                        case Names.Axtkaempfer:
                            return "Axeman";
                        case Names.Bogenschuetzen:
                            return "Archer";
                        case Names.Spaeher:
                            return "Scout";
                        case Names.LeichteKavallerie:
                            return "Light";
                        case Names.BeritteneBogenschuetzen:
                            return "Mounted";
                        case Names.SchwereKavallerie:
                            return "Heavy";
                        case Names.Rammboecke:
                            return "Ram";
                        case Names.Katapulte:
                            return "Catapult";
                        case Names.Paladin:
                            return "Paladin";
                        case Names.Adelsgeschlecht:
                            return "Nobleman";
                        case Names.Heute:
                            return "today";
                        case Names.Morgen:
                            return "tomorrow";
                        case Names.Rueckkehr:
                            return "return";
                        case Names.Angriff:
                            return "Attack";
                        case Names.Farmlimit:
                            return "";
                        case Names.Ramm:
                            return "Ram";
                        case Names.Kata:
                            return "Cat";
                        case Names.Leichte:
                            return "Light";
                        case Names.Schwere:
                            return "Heavy";
                        case Names.Berittene:
                            return "Mounted";
                        case Names.DerAngriffstrupp:
                            return "Der Angriffstrupp";
                        case Names.Unterstuetzung:
                            return "Support";
                        case Names.Genug:
                            return "enough";
                    }
                    return "";
            }
            return "";
        }
    }
}
