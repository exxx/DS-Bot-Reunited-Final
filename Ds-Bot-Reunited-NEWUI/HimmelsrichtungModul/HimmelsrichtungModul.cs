﻿using System.ComponentModel;
using System.Threading.Tasks;
using Ds_Bot_Reunited_NEWUI.Accountsettings.Ds_Bot_Reunited.Accountsettings;
using Ds_Bot_Reunited_NEWUI.Botcaptcha;
using Ds_Bot_Reunited_NEWUI.BrowserManager;
using Ds_Bot_Reunited_NEWUI.Constants;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using OpenQA.Selenium;

namespace Ds_Bot_Reunited_NEWUI.HimmelsrichtungModul {
    public class HimmelsrichtungModul {
        public static async Task SetHimmelsrichtung(string himmelsrichtung, Browser browser, Accountsetting accountsettings) {
            if (!accountsettings.MultiaccountingDaten.RestartedOverItem) {
                string link =App.Settings.OperateLink +
                              accountsettings.UvZusatzFuerLink + "&screen=inventory";
                browser.WebDriver.Navigate()
                    .GoToUrl(link);
                await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings, link);
                browser.WebDriver.FindElement(By.XPath("//img[contains(@src,'/relocate.png')]")).Click();
                await Rndm.Sleep(200, 400);
                browser.WebDriver.FindElement(By.XPath("//a[contains(@href,'choose_location')]")).Click();
                await Rndm.Sleep(200, 400);
                browser.WebDriver.FindElement(By.XPath("//input[contains(@value,'" + himmelsrichtung + "')]")).Click();
                await Rndm.Sleep(200, 400);
                browser.WebDriver.FindElement(By.XPath("//a[contains(@onclick,'Relocation.beginConsummation')]")).Click();
                await Rndm.Sleep(200, 400);
                browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.EigenesProfil.Inventar.ITEM_BESTAETIGEN_BUTTON_SELECTOR_XPATH)).Click();
                await Rndm.Sleep(200, 400);
                accountsettings.MultiaccountingDaten.RestartedOverItem = true;
            }
        }

        public enum Himmelsrichtungen {
            [Description("se")] Suedost,
            [Description("sw")] Suedwest,
            [Description("ne")] Nordost,
            [Description("nw")] Nordwest,
        }
    }
}