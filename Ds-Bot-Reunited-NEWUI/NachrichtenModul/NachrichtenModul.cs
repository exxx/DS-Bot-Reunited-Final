﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ds_Bot_Reunited_NEWUI.Accountsettings.Ds_Bot_Reunited.Accountsettings;
using Ds_Bot_Reunited_NEWUI.Botcaptcha;
using Ds_Bot_Reunited_NEWUI.BrowserManager;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.Nachrichten;
using OpenQA.Selenium;

namespace Ds_Bot_Reunited_NEWUI.NachrichtenModul {
    public class NachrichtenModul {
        public static async Task MakeNachrichten(Browser browser, Accountsetting accountsettings) {
            if (accountsettings.NachrichtenLesenActive && accountsettings.LastNachrichtenZeitpunkt.AddMinutes(accountsettings.NachrichtenLeseninterval) < DateTime.Now) {
                string link = App.Settings.OperateLink +
                              accountsettings.UvZusatzFuerLink + "&screen=mail";
                browser.WebDriver.Navigate().GoToUrl(link);
                await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings, link);
                int maxcount = CountUngeleseneNachrichten(browser);
                try {
                    for (int i = 0; i < maxcount; i++) {
                        List<IWebElement> elements = browser.WebDriver.FindElements(By.XPath("//a[contains(@href,'=view&view=')]/descendant::img[contains(@src,'new mail.png')]")).ToList();
                        if (elements.Any()) {
                            elements.FirstOrDefault().Click();
                        } else if (browser.WebDriver.FindElements(By.XPath("//img[contains(@src,'list-down.png')]")).Any()) {
                            browser.ClickByXpath("//img[contains(@src,'list-down.png')]");
                        }
                        Nachricht nachricht = new Nachricht { NachrichtPartner = browser.WebDriver.FindElements(By.XPath("//div[@class='post ']"))?[0]?.Text };
                        int index = browser.WebDriver.Url.IndexOf("view&view", StringComparison.CurrentCulture);
                        nachricht.Id = browser.WebDriver.Url.Substring(index);
                        nachricht.Accountname = (!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname);
                        accountsettings.MultiaccountingDaten.Nachrichten.Add(nachricht);
                    }
                    //TODO FALSCHER PLATZ
                    //if (accountsettings.NachrichtenAntwortenActive) {
                    //    await NachrichtenBeantworten(browser, accountsettings);
                    //}
                } catch {
                    // ignored
                }
                accountsettings.LastNachrichtenZeitpunkt = DateTime.Now;
            }
        }

        private static int CountUngeleseneNachrichten(Browser browser) {
            List<IWebElement> nachrichten = browser.WebDriver.FindElements(By.XPath("//img[contains(@src,'new_mail.png')]")).ToList();
            return nachrichten.Count;
        }

        private static async Task NachrichtenBeantworten(Browser browser, Accountsetting accountsettings) {
            if (accountsettings.MultiaccountingDaten.Nachrichten.Any(x => !string.IsNullOrEmpty(x.Antwort))) {
                foreach (var nachricht in accountsettings.MultiaccountingDaten.Nachrichten.Where(x => !string.IsNullOrEmpty(x.Antwort))) {
                    string link = App.Settings.OperateLink +
                                  accountsettings.UvZusatzFuerLink + "&screen=mail&mode=view&view=" + nachricht;
                    browser.WebDriver.Navigate()
                        .GoToUrl(link);
                    await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings, link);
                    await Rndm.Sleep(200, 500);
                    browser.WebDriver.FindElement(By.XPath("//a[contains(@href,'beginReply()')]")).Click();
                    await Rndm.Sleep(200, 500);
                    browser.WebDriver.FindElement(By.XPath("//textarea[@id='message']")).SendKeys(nachricht.Antwort);
                    await Rndm.Sleep(200, 500);
                    browser.WebDriver.FindElement(By.XPath("//input[@name='answer']")).Click();
                    await Rndm.Sleep(200, 500);
                }
            }
        }
    }
}