﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ds_Bot_Reunited_NEWUI.Accountsettings.Ds_Bot_Reunited.Accountsettings;
using Ds_Bot_Reunited_NEWUI.AngreifenModul;
using Ds_Bot_Reunited_NEWUI.Angreifensettings;
using Ds_Bot_Reunited_NEWUI.BrowserManager;
using Ds_Bot_Reunited_NEWUI.Constants;
using Ds_Bot_Reunited_NEWUI.DorfinformationenModul;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.SpieldatenViewModel;
using OpenQA.Selenium;

namespace Ds_Bot_Reunited_NEWUI.SynchronizeModul {
	public static class SyncronizeModul {
		private static bool _sendfinished;

		private static List<int> _differences;

		public static async Task StartSynchronize(Browser browser, string welt, Accountsetting accountsettings) {
			try {
				if (
					App.Settings.LastTimeSynchronisation <
					DateTime.Now
					        .AddMinutes(-20) && !accountsettings.GeplanteBewegungen.Any(x => x.Abschickzeitpunkt.AddMilliseconds(App.Settings.Difference) > DateTime.Now.AddMinutes(-5) && x.Abschickzeitpunkt.AddMilliseconds(App.Settings.Difference) < DateTime.Now.AddMinutes(5) && x.Active && x.GenauTimenIsActive)
				) {
					App.PauseEinzelaccounting = true;
					App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
						               ? accountsettings.Uvname
						               : accountsettings.Accountname) + ": " + " Synchronizing Time started!");
					CryptoRandom rnd = new CryptoRandom();
					_differences = new List<int>();
					SynchronizeVillage village =
						await DorfinformationenModul
						      .DorfinformationenModul.GetVillageWithEnoughTroopsForSync(browser, accountsettings);
					if (village.VillageId > 0) {
						var villllage =
							accountsettings.DoerferSettings
							               .FirstOrDefault(x => x.Dorf.VillageId.Equals(village.VillageId));
						if (villllage != null) {
							DorfViewModel empfaenger = null;
							var list = accountsettings
							           .DoerferSettings.Where(x => !x.Dorf.VillageId.Equals(villllage.Dorf.VillageId))
							           .ToList();
							if (list.Any()) {
								empfaenger = list.First().Dorf;
							} else {
								empfaenger = App.VillagelistWholeWorld.Last(x => x.Value.PlayerId.Equals(0)).Value;
							}

							TruppenbewegungViewModel truppenbewegung =
								new TruppenbewegungViewModel(new Truppenbewegung {
									                                                 SenderDorf = villllage.Dorf
									                                                 , EmpfaengerDorf = empfaenger
								                                                 });
							switch (village.Truppe) {
								case 0:
									truppenbewegung.Model.SpeertraegerAnzahl = 1;
									break;
								case 1:
									truppenbewegung.Model.SchwertkaempferAnzahl = 1;
									break;
								case 2:
									truppenbewegung.Model.AxtkaempferAnzahl = 1;
									break;
								case 3:
									truppenbewegung.Model.BogenschuetzenAnzahl = 1;
									break;
								case 4:
									truppenbewegung.Model.SpaeherAnzahl = 1;
									break;
								case 5:
									truppenbewegung.Model.LeichteKavallerieAnzahl = 1;
									break;
								case 6:
									truppenbewegung.Model.BeritteneBogenschuetzenAnzahl = 1;
									break;
								case 7:
									truppenbewegung.Model.SchwereKavallerieAnzahl = 1;
									break;
								case 8:
									truppenbewegung.Model.RammboeckeAnzahl = 1;
									break;
								case 9:
									truppenbewegung.Model.KatapulteAnzahl = 1;
									break;
								case 10:
									truppenbewegung.Model.KatapulteAnzahl = 1;
									break;
							}

							PreciseDatetime dt = new PreciseDatetime();
							for (int i = 0; i < rnd.Next(4, 6); i++) {
								TruppenbewegungViewModel bewegung =
									new TruppenbewegungViewModel(truppenbewegung.Model.SenderDorf
									                             , truppenbewegung.Model.EmpfaengerDorf) {
										                                                                     BewegungsartSelected
											                                                                     =
											                                                                     "Attack"
										                                                                     , Speertraeger
											                                                                     =
											                                                                     truppenbewegung
												                                                                     .Speertraeger
										                                                                     , Schwertkaempfer
											                                                                     =
											                                                                     truppenbewegung
												                                                                     .Schwertkaempfer
										                                                                     , Axtkaempfer
											                                                                     =
											                                                                     truppenbewegung
												                                                                     .Axtkaempfer
										                                                                     , Bogenschuetzen
											                                                                     =
											                                                                     truppenbewegung
												                                                                     .Bogenschuetzen
										                                                                     , Spaeher =
											                                                                     truppenbewegung
												                                                                     .Spaeher
										                                                                     , LeichteKavallerie
											                                                                     =
											                                                                     truppenbewegung
												                                                                     .LeichteKavallerie
										                                                                     , BeritteneBogenschuetzen
											                                                                     =
											                                                                     truppenbewegung
												                                                                     .BeritteneBogenschuetzen
										                                                                     , SchwereKavallerie
											                                                                     =
											                                                                     truppenbewegung
												                                                                     .SchwereKavallerie
										                                                                     , Rammboecke
											                                                                     = truppenbewegung
												                                                                     .Rammboecke
										                                                                     , Katapulte
											                                                                     = truppenbewegung
												                                                                     .Katapulte
										                                                                     , Paladin =
											                                                                     truppenbewegung
												                                                                     .Paladin
										                                                                     , Adelsgeschlechter
											                                                                     = truppenbewegung
												                                                                     .Adelsgeschlechter
										                                                                     , GenauTimenIsActive
											                                                                     = true
										                                                                     , Abschickzeitpunkt
											                                                                     =
											                                                                     DateTime
												                                                                     .Now
												                                                                     .AddSeconds(rnd
													                                                                                 .Next(10
													                                                                                       , 15))
												                                                                     .ToString("dd.MM.yyyy HH:mm:ss.fff"
												                                                                               , CultureInfo
													                                                                               .InvariantCulture)
									                                                                     };
								if (!App.Settings.Weltensettings.MillisecondsActive) {
									bewegung.Model.Abschickzeitpunkt =
										bewegung.Model.Abschickzeitpunkt.AddMilliseconds(1000 - bewegung
										                                                        .Model.Abschickzeitpunkt
										                                                        .Millisecond);
								}

								_sendfinished = false;
								await StartBewegung(welt, browser, bewegung, GetAvailableTroops(truppenbewegung)
								                    , accountsettings, dt);
								while (!_sendfinished) {
									await Task.Delay(200);
								}

								await Task.Delay(rnd.Next(6000, 12000));
							}

							int durchschnitt = _differences.Aggregate(0, (current, difference) => current + difference);
							if (App.Settings.Weltensettings.MillisecondsActive)
								for (int i = 0; i < _differences.Count; i++) {
									if (_differences.Count(x => x - 20 < _differences[i] && x + 20 > _differences[i]) <
									    3) {
										_differences.Remove(_differences[i]);
										i--;
									}
								}

							if (_differences.Any())
								durchschnitt = _differences.Aggregate(0, (current, difference) => current + difference);
							var alt = App.Settings.Difference;
							App.Settings.Difference = durchschnitt / _differences.Count;
							if (alt != 0 && App.Settings.LastTimeSynchronisation != DateTime.MinValue) {
								var unterschied = (App.Settings.Difference - alt) /
								                  (DateTime.Now - App.Settings.LastTimeSynchronisation).TotalMinutes;
								App.Settings.TimeDifferenceByMachinePerMinute = unterschied;
							}
						}

						App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
							               ? accountsettings.Uvname
							               : accountsettings.Accountname) + ": " +
						              " Synchronizing Time ended! Current Delay: " + App.Settings.Difference);
					} else {
						App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
							               ? accountsettings.Uvname
							               : accountsettings.Accountname) + ": " +
						              " Synchronizing ERROR No troops available");
					}

					App.Settings.LastTimeSynchronisation = DateTime.Now;
					App.PauseEinzelaccounting = false;
				}
			} catch (Exception) {
				// ignored
			}

			browser.GetsUsed = false;
		}

		private static List<TroopTemplate> GetAvailableTroops(TruppenbewegungViewModel bewegung) {
			List<TroopTemplate> list = new List<TroopTemplate>();
			if (bewegung.Model.SpeertraegerAnzahl > 0) {
				list.Add(new TroopTemplate(1, "spear"));
			} else if (bewegung.Model.SchwertkaempferAnzahl > 0) {
				list.Add(new TroopTemplate(1, "sword"));
			} else if (bewegung.Model.AxtkaempferAnzahl > 0) {
				list.Add(new TroopTemplate(1, "axe"));
			} else if (bewegung.Model.BogenschuetzenAnzahl > 0) {
				list.Add(new TroopTemplate(1, "archer"));
			} else if (bewegung.Model.SpaeherAnzahl > 0) {
				list.Add(new TroopTemplate(1, "spy"));
			} else if (bewegung.Model.LeichteKavallerieAnzahl > 0) {
				list.Add(new TroopTemplate(1, "light"));
			} else if (bewegung.Model.BeritteneBogenschuetzenAnzahl > 0) {
				list.Add(new TroopTemplate(1, "marcher"));
			} else if (bewegung.Model.SchwereKavallerieAnzahl > 0) {
				list.Add(new TroopTemplate(1, "heavy"));
			} else if (bewegung.Model.RammboeckeAnzahl > 0) {
				list.Add(new TroopTemplate(1, "ram"));
			} else if (bewegung.Model.KatapulteAnzahl > 0) {
				list.Add(new TroopTemplate(1, "cat"));
			} else if (bewegung.Model.PaladinAnzahl > 0) {
				list.Add(new TroopTemplate(1, "knight"));
			}

			return list;
		}

		private static async Task StartBewegung(string welt
		                                        , Browser browser
		                                        , TruppenbewegungViewModel truppenbewegung
		                                        , List<TroopTemplate> troopstosend /*, Handles handle*/
		                                        , Accountsetting accountsettings
		                                        , PreciseDatetime dt) {
			try {
				string uri = App.Settings.OperateLink + "village=" + truppenbewegung.Model.SenderDorf.VillageId + "" +
				             accountsettings.UvZusatzFuerLink + "&screen=place&target=" +
				             truppenbewegung.Model.EmpfaengerDorf.VillageId;
				//if (!webDriver.CurrentWindowHandle.Equals(handle.Handle))
				//    webDriver.SwitchTo().Window(handle.Handle);
				browser.WebDriver.Navigate().GoToUrl(uri);
				browser.WebDriver.WaitForPageload();
				await Botcaptcha.Botschutzerkennung.CheckIfBotschutz(browser, accountsettings);
				switch (truppenbewegung.BewegungsartSelected) {
					case "Support":
						await SendModul.SendModul.SendSupportOverPlace(browser, troopstosend
						                                               , truppenbewegung.Model.SenderDorf
						                                               , truppenbewegung.Model.Abschickzeitpunkt, true);
						break;
					case "Attack":
						await SendModul.SendModul.SendAttackOverPlace(browser, troopstosend
						                                              , truppenbewegung.Model.SenderDorf
						                                              , truppenbewegung.Model.Abschickzeitpunkt, true);
						break;
				}
				

				var js2 = (IJavaScriptExecutor) browser.WebDriver;
				if (js2 != null) {
					js2.ExecuteScript("$('#chat-wrapper').hide();");
					js2.ExecuteScript("$('#side-notification-container').hide();");
					js2.ExecuteScript("var p=window.XMLHttpRequest.prototype; p.open=p.send=p.setRequestHeader=function(){};");
                    truppenbewegung.Sent = "J";
					StringBuilder builder = new StringBuilder();
                    DateTime dateTime;
					int anzahl = 0;
					do {
						anzahl++;
						try {
							dateTime = DateTimeHelper.UnixTimeStampToDateTime((double)js2.ExecuteScript("return Timing.getCurrentServerTime()"));
						} catch {
							dateTime = DateTime.MinValue;
							App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) +
							              " timestamp failure");
						}
					} while (dateTime == DateTime.MinValue && anzahl < 10);
					builder.Append("var start = performance.now();" + "var xInterval = setInterval(function(){if (performance.now() - start >=");
					builder.Append((int)(truppenbewegung.Model.Abschickzeitpunkt - dateTime).TotalMilliseconds);
					builder.Append("){ document.getElementById('troop_confirm_go').click();clearInterval(xInterval);} }, 1);");
					js2.ExecuteScript(builder.ToString());

                    await Task.Delay((int)(truppenbewegung.Model.Abschickzeitpunkt - dateTime).TotalMilliseconds + 1000);
					browser.WebDriver.WaitForPageload();
					IWebElement el = null;
					string temppp = "";
					bool fehler;
					do {
						fehler = false;
						try {
							el = browser.WebDriver.FindElement(By.XPath("//div[@id='commands_outgoings']"))
							            .FindElements(By.CssSelector("tr")).ToList()
							            .Last(x => x.Text.Contains(truppenbewegung.EmpfaengerX.ToString() + "|" +
							                                       truppenbewegung.EmpfaengerY.ToString()) &&
							                       x.Text.Contains(truppenbewegung
							                                       .Model.Ankunftszeitpunkt.Minute.ToString()));
							temppp = el.FindElements(By.CssSelector("td"))[1].Text;
							var url = el.FindElements(By.CssSelector("a"))[0].GetAttribute("href");
							await Rndm.Sleep(100, 200);
							browser.WebDriver.Navigate().GoToUrl(url);
						} catch (Exception) {
							fehler = true;
						}
					} while (fehler);

					await Rndm.Sleep(400, 600);
					((IJavaScriptExecutor) browser.WebDriver).ExecuteScript("$('#chat-wrapper').hide();");
					browser.WebDriver.FindElement(By.XPath("//a[contains(@href,'&action=cancel')]")).Click();
					await Rndm.Sleep(500, 800);
					DateTime fff;
					if (temppp.Contains(NAMECONSTANTS.GetNameForServer(Names.Heute))) {
						string[] temp2 = temppp.Split(' ');
						fff = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day
						                   , int.Parse(temp2[2].Substring(0, 2)), int.Parse(temp2[2].Substring(3, 2))
						                   , int.Parse(temp2[2].Substring(6, 2)));
						if (App.Settings.Weltensettings.MillisecondsActive)
							fff = fff.AddMilliseconds(int.Parse(temp2[2].Substring(9, 3)));
					} else if (temppp.Contains(NAMECONSTANTS.GetNameForServer(Names.Morgen))) {
						string[] temp2 = temppp.Split(' ');
						fff = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day + 1
						                   , int.Parse(temp2[2].Substring(0, 2)), int.Parse(temp2[2].Substring(3, 2))
						                   , int.Parse(temp2[2].Substring(6, 2)));
						if (App.Settings.Weltensettings.MillisecondsActive)
							fff = fff.AddMilliseconds(int.Parse(temp2[2].Substring(9, 3)));
					} else {
						string[] temp2 = temppp.Split(' ');
						fff = new DateTime(DateTime.Now.Year, int.Parse(temp2[1].Substring(3, 2))
						                   , int.Parse(temp2[1].Substring(0, 2)), int.Parse(temp2[3].Substring(0, 2))
						                   , int.Parse(temp2[3].Substring(3, 2)), int.Parse(temp2[3].Substring(6, 2)));
						if (App.Settings.Weltensettings.MillisecondsActive)
							fff = fff.AddMilliseconds(int.Parse(temp2[3].Substring(9, 3)));
					}

					var t = truppenbewegung.Model.Ankunftszeitpunkt - fff;

					try {
						IJavaScriptExecutor js = (IJavaScriptExecutor) browser.WebDriver;
						js.ExecuteScript("$('#side-notification-container').hide();$('#chat-wrapper').hide();$('#menu_row').hide();$('.top_bar').hide();");
						await Task.Delay(300);
						js.ExecuteScript("arguments[0].scrollIntoView(true);"
						                 , el.FindElements(By.XPath("//a[@class='command-cancel']"))[0]);
						await Task.Delay(200);
						el.FindElements(By.XPath("//a[@class='command-cancel']"))[0].Click();
					} catch (Exception) { }

					int xxx = t.Seconds * 1000 + t.Milliseconds;
					_differences.Add(xxx);
					App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
						               ? accountsettings.Uvname
						               : accountsettings.Accountname) + ": " +
					              " Synchronizing Time current delay in Milliseconds: " + xxx);
					_sendfinished = true;
				}
			} catch (Exception e) {
				App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
					               ? accountsettings.Uvname
					               : accountsettings.Accountname) + ": " + " Synchronizing Failure: " + e);
				_sendfinished = true;
			}
		}
	}
}