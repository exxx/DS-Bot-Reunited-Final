﻿using System.Windows;

namespace Ds_Bot_Reunited_NEWUI.LoginView {
    public partial class LoginView {
        public LoginView() {
            LoginViewModel vm = new LoginViewModel(this);
            DataContext = vm;
            InitializeComponent();
        }

        private void Save(object sender, RoutedEventArgs e) {
            this.Close();
        }
    }
}