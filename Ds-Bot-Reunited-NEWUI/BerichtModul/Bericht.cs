﻿using System;
using Ds_Bot_Reunited_NEWUI.Farmen;

namespace Ds_Bot_Reunited_NEWUI.BerichtModul {
    [Serializable]
    public class Bericht {
        public string Berichtid { get; set; }
        public string Betreff { get; set; }
        public string Ergebnis { get; set; }
        public bool IsSelected { get; set; }
        public DateTime Kampfzeit { get; set; }
        public double Angreiferglueck { get; set; }
        public int Moral { get; set; }
        public int AngreiferPlayerId { get; set; }
        public int AngreiferVillageId { get; set; }
        public TruppenTemplate AngreiferTruppenAnzahl { get; set; }
        public TruppenTemplate AngreiferTruppenVerluste { get; set; }

        public bool HasLosses {
            get {
                if (AngreiferTruppenVerluste.SpeertraegerAnzahl > 0
                    || AngreiferTruppenVerluste.SchwertkaempferAnzahl > 0
                    || AngreiferTruppenVerluste.AxtkaempferAnzahl > 0
                    || AngreiferTruppenVerluste.BogenschuetzenAnzahl > 0
                    || AngreiferTruppenVerluste.SpaeherAnzahl > 0
                    || AngreiferTruppenVerluste.LeichteKavallerieAnzahl > 0
                    || AngreiferTruppenVerluste.BeritteneBogenschuetzenAnzahl > 0
                    || AngreiferTruppenVerluste.SchwereKavallerieAnzahl > 0
                    || AngreiferTruppenVerluste.RammboeckeAnzahl > 0
                    || AngreiferTruppenVerluste.KatapulteAnzahl > 0
                    || AngreiferTruppenVerluste.PaladinAnzahl > 0) {
                    return true;
                }
                return false;
            }
        }

        public string Effekt { get; set; }

        public int VerteidigerPlayerId { get; set; }
        public int VerteidigerVillageId { get; set; }
        public TruppenTemplate VerteidigerTruppenAnzahl { get; set; }
        public TruppenTemplate VerteidigerTruppenVerluste { get; set; }

        public Beute Beute { get; set; }
        public int Erwartet { get; set; }

        public Beute NochDrin { get; set; }

        public int Erfolgsquotient { get; set; }

        public Bericht Clone() {
            Bericht newBericht = new Bericht {
                AngreiferTruppenVerluste = AngreiferTruppenVerluste?.Clone(),
                Beute = Beute?.Clone(),
                VerteidigerVillageId = VerteidigerVillageId,
                Erwartet = Erwartet,
                Effekt = Effekt,
                Kampfzeit = Kampfzeit,
                AngreiferVillageId = AngreiferVillageId,
                AngreiferTruppenAnzahl = AngreiferTruppenAnzahl?.Clone(),
                VerteidigerPlayerId = VerteidigerPlayerId,
                Ergebnis = Ergebnis,
                Betreff = Betreff,
                VerteidigerTruppenVerluste = VerteidigerTruppenVerluste?.Clone(),
                VerteidigerTruppenAnzahl = VerteidigerTruppenAnzahl?.Clone(),
                Angreiferglueck = Angreiferglueck,
                Moral = Moral,
                AngreiferPlayerId = AngreiferPlayerId,
                Berichtid = Berichtid,
                NochDrin = NochDrin?.Clone(),
                Erfolgsquotient = Erfolgsquotient
            };
            return newBericht;
        }
    }
}