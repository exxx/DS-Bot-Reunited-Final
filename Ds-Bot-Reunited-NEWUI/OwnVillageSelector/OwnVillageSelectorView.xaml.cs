﻿using System.Windows;
using System.Windows.Controls;
using Ds_Bot_Reunited_NEWUI.SpieldatenViewModel;
using Framework.UI.Controls;
using Window = System.Windows.Window;

namespace Ds_Bot_Reunited_NEWUI.OwnVillageSelector {
    public partial class OwnVillageSelectorView : OverlayWindow {
        public OwnVillageSelectorView(Window owner, OwnVillageSelectorViewModel vm) {
            Owner = owner;
            DataContext = vm;
            InitializeComponent();
        }

        private void Save(object sender, RoutedEventArgs e) {
            Close();
        }

        void MyGrid_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            if (((OwnVillageSelectorViewModel) DataContext)?.SelectedVillages != null) {
                ((OwnVillageSelectorViewModel) DataContext).SelectedVillages.Clear();
                foreach (var item in DataGrid.SelectedItems) {
                    ((OwnVillageSelectorViewModel) DataContext).SelectedVillages.Add((DorfViewModel) item);
                }
            }
        }
    }
}