﻿using System;

namespace Ds_Bot_Reunited_NEWUI.TimeModul {
    public class TimeModulMarkt {
        public static TimeSpan GetLaufzeit(int xcoord1, int ycoord1, int xcoord2, int ycoord2) {
            var tempx = (xcoord1 - xcoord2) *
                        (xcoord1 - xcoord2);
            var tempy = (ycoord1 - ycoord2) *
                        (ycoord1 - ycoord2);
            var entfernung = Math.Sqrt(tempx + tempy);
            var minutes = entfernung * 10;
            var hours = (int) minutes / 60;
            minutes = minutes - hours * 60;
            var minutestotal = (int) minutes;
            minutes = minutes - minutestotal;
            var seconds = (int) Math.Round(minutes * 60, 0, MidpointRounding.ToEven);
            // ReSharper disable once PossibleLossOfFraction
            minutes = minutes - seconds / 60;
            var millis = (int) minutes * 1000;
            var ts = new TimeSpan(0, hours, minutestotal, seconds, millis);
            return ts;
        }
    }
}