﻿using System;

namespace Ds_Bot_Reunited_NEWUI.DefendModul {

    [Serializable]
    public enum DefendArt {
        Aggressive,
        Defensive,
        Neutral
    }
}