﻿using System;
using System.Threading.Tasks;
using Ds_Bot_Reunited_NEWUI.Accountsettings.Ds_Bot_Reunited.Accountsettings;
using Ds_Bot_Reunited_NEWUI.BrowserManager;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;

namespace Ds_Bot_Reunited_NEWUI.DefendModul {
    [Serializable]
    public class DefendModul {
        public static async Task Defend(Browser browser, Accountsetting accountsettings) {
            if (App.Settings.LastAttackdetectionZeitpunkt.AddMinutes(App.Settings.AttackdetectionInterval) < DateTime.Now) {
                string link1 = App.Settings.OperateLink + "screen=overview_villages" + accountsettings.UvZusatzFuerLink + "&mode=incomings&type=all&subtype=attacks&page=-1&group=0";
                if (!browser.WebDriver.Url.Equals(link1)) {
                    browser.WebDriver.Navigate().GoToUrl(link1);
                    browser.WebDriver.WaitForPageload();
                }
                if (App.Settings.DetectActive) {
                    await AttackDetectionModul.AttackDetectionModul.GetAttacks(browser, accountsettings);
                }
                if (App.Settings.PreventActive) {
                    AttackPreventionModul.AttackPreventionModul.PreventAttacks(accountsettings);
                }
                App.Settings.LastAttackdetectionZeitpunkt = DateTime.Now;
            }
        }
    }
}
