﻿using System;
using System.Threading.Tasks;
using Ds_Bot_Reunited_NEWUI.Accountsettings.Ds_Bot_Reunited.Accountsettings;
using Ds_Bot_Reunited_NEWUI.Botcaptcha;
using Ds_Bot_Reunited_NEWUI.BrowserManager;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace Ds_Bot_Reunited_NEWUI.PremiumFeaturesModul {
    public static class PremiumFeaturesModul {
        public static async Task SetBooster(Browser browser, Accountsetting accountsettings) {
            accountsettings.MultiaccountingDaten.PremiumPunkte = int.Parse(browser.WebDriver.FindElement(By.XPath("//span[@id='premium_points']")).Text.Trim());
            if (accountsettings.MultiaccountingDaten.PremiumPunkte >= 40 && ((DateTime.Now > accountsettings.MultiaccountingDaten.WoodBoostEndtime) || (DateTime.Now > accountsettings.MultiaccountingDaten.StoneBoostEndtime) || (DateTime.Now > accountsettings.MultiaccountingDaten.IronBoostEndtime))) {
                string premiumLink = App.Settings.OperateLink + accountsettings.UvZusatzFuerLink + "&screen=premium&mode=use";
                browser.WebDriver.Navigate().GoToUrl(premiumLink);
                await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings);

                //premium-box
                try {
                    if (browser.WebDriver.PageSource.Contains("Gratis Premium-Account")) {
                        browser.WebDriver.FindElement(By.XPath("//*[@id='content_value']/div[1]/div/form/div[2]/div[2]/button")).Click();
                        await Rndm.Sleep(500, 1000);
                    }

                    if ((DateTime.Now > accountsettings.MultiaccountingDaten.WoodBoostEndtime) && accountsettings.MultiaccountingDaten.PremiumPunkte >= 40) {
                        var zeitfenster = browser.WebDriver.FindElement(By.XPath("//*[@id='content_value']/div[2]/div[1]/form/div[2]/div[1]/div[1]/select"));
                        ((IJavaScriptExecutor) browser.WebDriver).ExecuteScript("arguments[0].scrollIntoView(true);", zeitfenster);
                        await Rndm.Sleep(300, 500);
                        SelectElement select = new SelectElement(zeitfenster);
                        try {
                            select.DeselectAll();
                        } catch { }
                        select.SelectByText(zeitfenster.FindElement(By.XPath("//option[@value='12']")).Text);
                        await Rndm.Sleep(100, 500);
                        var element = browser.WebDriver.FindElement(By.XPath("//*[@id='content_value']/div[2]/div[1]/form/div[2]/div[2]/button[1]"));
                        ((IJavaScriptExecutor) browser.WebDriver).ExecuteScript("arguments[0].scrollIntoView(true);", zeitfenster);
                        await Rndm.Sleep(300, 500);
                        element.Click();
                        await Rndm.Sleep(1000, 1500);
                        browser.WebDriver.FindElement(By.XPath("//button[@class='btn evt-confirm-btn btn-confirm-yes']")).Click();
                        accountsettings.MultiaccountingDaten.WoodBoostEndtime = DateTime.Now.AddDays(7);
                        accountsettings.MultiaccountingDaten.PremiumPunkte -= 40;
                    }
                    if (DateTime.Now > accountsettings.MultiaccountingDaten.StoneBoostEndtime && accountsettings.MultiaccountingDaten.PremiumPunkte >= 40) {
                        var zeitfenster = browser.WebDriver.FindElement(By.XPath("//*[@id='content_value']/div[2]/div[2]/form/div[2]/div[1]/div[1]/select"));
                        ((IJavaScriptExecutor) browser.WebDriver).ExecuteScript("arguments[0].scrollIntoView(true);", zeitfenster);
                        await Rndm.Sleep(300, 500);
                        SelectElement select = new SelectElement(zeitfenster);
                        try {
                            select.DeselectAll();
                        } catch { }
                        select.SelectByText(zeitfenster.FindElement(By.XPath("//option[@value='13']")).Text);
                        await Rndm.Sleep(100, 500);
                        var element = browser.WebDriver.FindElement(By.XPath("//*[@id='content_value']/div[2]/div[2]/form/div[2]/div[2]/button[1]"));
                        ((IJavaScriptExecutor) browser.WebDriver).ExecuteScript("arguments[0].scrollIntoView(true);", zeitfenster);
                        await Rndm.Sleep(300, 500);
                        element.Click();
                        await Rndm.Sleep(1000, 1500);
                        browser.WebDriver.FindElement(By.XPath("//button[@class='btn evt-confirm-btn btn-confirm-yes']")).Click();
                        accountsettings.MultiaccountingDaten.StoneBoostEndtime = DateTime.Now.AddDays(7);
                        accountsettings.MultiaccountingDaten.PremiumPunkte -= 40;
                    }
                    if ((DateTime.Now > accountsettings.MultiaccountingDaten.IronBoostEndtime) && accountsettings.MultiaccountingDaten.PremiumPunkte >= 40) {
                        var zeitfenster = browser.WebDriver.FindElement(By.XPath("//*[@id='content_value']/div[2]/div[3]/form/div[2]/div[1]/div[1]/select"));
                        ((IJavaScriptExecutor) browser.WebDriver).ExecuteScript("arguments[0].scrollIntoView(true);", zeitfenster);
                        await Rndm.Sleep(300, 500);
                        SelectElement select = new SelectElement(zeitfenster);
                        try {
                            select.DeselectAll();
                        } catch { }
                        select.SelectByText(zeitfenster.FindElement(By.XPath("//option[@value='14']")).Text);
                        await Rndm.Sleep(100, 500);
                        var element = browser.WebDriver.FindElement(By.XPath("//*[@id='content_value']/div[2]/div[3]/form/div[2]/div[2]/button[1]"));
                        ((IJavaScriptExecutor) browser.WebDriver).ExecuteScript("arguments[0].scrollIntoView(true);", zeitfenster);
                        await Rndm.Sleep(300, 500);
                        element.Click();
                        await Rndm.Sleep(1000, 1500);
                        browser.WebDriver.FindElement(By.XPath("//button[@class='btn evt-confirm-btn btn-confirm-yes']")).Click();
                        accountsettings.MultiaccountingDaten.IronBoostEndtime = DateTime.Now.AddDays(7);
                        accountsettings.MultiaccountingDaten.PremiumPunkte -= 40;
                    }
                } catch (Exception e) {
                    App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + " Failure Premiumfeatures Account " + e);
                }
            }
        }
    }
}
