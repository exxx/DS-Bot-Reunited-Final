﻿using System.Windows;
using System.Windows.Controls;
using Ds_Bot_Reunited_NEWUI.Farmmodul;

namespace Ds_Bot_Reunited_NEWUI.Farmliste {
    public partial class FarmlisteView {
        public FarmlisteView(Window owner, FarmlisteViewModel datacontext) {
            Owner = owner;
            DataContext = datacontext;
            InitializeComponent();
        }

        private void Save(object sender, RoutedEventArgs e) {
            Close();
        }

        private void MyGrid_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            if (((FarmlisteViewModel) DataContext)?.SelectedVillages != null) {
                ((FarmlisteViewModel) DataContext).SelectedVillages.Clear();
                foreach (var item in DataGrid.SelectedItems) ((FarmlisteViewModel) DataContext).SelectedVillages.Add((FarmtargetViewModel) item);
            }
        }
    }
}
