﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using Ds_Bot_Reunited_NEWUI.Annotations;
using Ds_Bot_Reunited_NEWUI.Dorfinformationen;
using Ds_Bot_Reunited_NEWUI.Farmen;
using Ds_Bot_Reunited_NEWUI.Farmmodul;
using Ds_Bot_Reunited_NEWUI.Farmsettings;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.Properties;
using Ds_Bot_Reunited_NEWUI.Spieldaten;
using Ds_Bot_Reunited_NEWUI.SpieldatenViewModel;
using Framework.UI.Controls;
using Window = System.Windows.Window;

namespace Ds_Bot_Reunited_NEWUI.Farmliste {
	[Serializable]
	public class FarmlisteViewModel : INotifyPropertyChanged {
		public Window Window { get; set; }
		private ICommand _removeFarmtargetCommand;
		private ICommand _addFarmtargetCommand;
		private ICommand _deactivateFarmtargetCommand;
		private ICommand _activateFarmtargetCommand;
		private ICollectionView _farmlisteView;
		private AsyncObservableCollection<FarmtargetViewModel> _farmliste;
		private double _umkreis;
		private ICommand _addNewFarmtargetsCommand;
		private ICommand _resetErfolgsquotientCommand;
		private ICommand _showFarmsettingsCommand;
		private readonly AsyncObservableCollection<DorfViewModel> _villagelistActualAccount;
		private ICollectionView _villagelistView;
		private GruppeDoerfer _selectedSenderGruppe;
		private string _senderCoords;
		private DorfViewModel _selectedSenderVillage;
		private ICommand _setErfolgsquotientCommand;
		private ICommand _doActivateFixStatusCommand;
		private ICommand _doDeactivateFixStatusCommand;
		public AsyncObservableCollection<FarmtargetViewModel> SelectedVillages { get; set; }

		public ICollectionView Villagelist {
			get => _villagelistView;
			set {
				_villagelistView = value;
				OnPropertyChanged();
			}
		}

		public GruppeDoerfer SelectedSenderGruppe {
			get => _selectedSenderGruppe;
			set {
				_selectedSenderGruppe = value;
				LoadPlayerVillages();
				OnPropertyChanged("");
			}
		}

		public DorfViewModel SelectedSenderVillage {
			get => _selectedSenderVillage;
			set {
				_selectedSenderVillage = value;
				if (_selectedSenderVillage != null)
					SenderCoords = _selectedSenderVillage.XCoordinate + "|" + _selectedSenderVillage.YCoordinate;
				else
					SenderCoords = "";
				OnPropertyChanged();
			}
		}

		public string SenderCoords {
			get => _senderCoords;
			set {
				_senderCoords = value;
				try {
					string[] splitted = value.Split('|');

					var searchTuple = new VillageTuple<int, int>(int.Parse(splitted[0]), int.Parse(splitted[1]));
					var village = App.VillagelistWholeWorld[searchTuple];
					if (village != null && (SelectedSenderVillage == null ||
					                        !SelectedSenderVillage.VillageId.Equals(village.VillageId))) {
						SelectedSenderVillage = village;
					}
				} catch {
					// ignored
				}

				OnPropertyChanged();
			}
		}

		public AsyncObservableCollection<FarmtargetViewModel> Farmliste {
			get => _farmliste;
			set {
				_farmliste = value;
				OnPropertyChanged();
			}
		}

		public ICollectionView FarmlisteView {
			get => _farmlisteView;
			set {
				_farmlisteView = value;
				OnPropertyChanged();
			}
		}

		public int FarmCount => _farmliste.Count;

		public ICommand AddFarmtargetCommand =>
			_addFarmtargetCommand ?? (_addFarmtargetCommand = new RelayCommand(DoAddFarmtarget));

		public string ZielCoordinate { get; set; }

		public FarmtargetViewModel SelectedFarm { get; set; }

		public double Umkreis {
			get => _umkreis;
			set {
				_umkreis = value;
				OnPropertyChanged();
			}
		}

		private void LoadPlayerVillages() {
			try {
				_villagelistActualAccount.Clear();
				foreach (var acccountsettings in App.Settings.Accountsettingslist) {
					List<DorfViewModel> list = (!string.IsNullOrEmpty(SelectedSenderGruppe?.Gruppenname)
						                            ? acccountsettings
						                              .DoerferSettings.Select(y => y.Dorf)
						                              .Where(x1 => App.Settings.Gruppen.Any(k => {
							                                                                    return
								                                                                    SelectedSenderGruppe !=
								                                                                    null &&
								                                                                    (!string
									                                                                     .IsNullOrEmpty(SelectedSenderGruppe
										                                                                                    ?
										                                                                                    .Gruppenname) ||
								                                                                     k.Gruppenname ==
								                                                                     SelectedSenderGruppe
									                                                                     .Gruppenname &&
								                                                                     k.DoerferIds
								                                                                      .Any(l => l == x1
									                                                                                .VillageId)
								                                                                    );
						                                                                    }))
						                            : acccountsettings.DoerferSettings.Select(y => y.Dorf)).ToList();
					foreach (var xx in list) {
						_villagelistActualAccount.Add(xx);
					}
				}
			} catch {
				App.LogString(Resources.FailureLoadingPlayervillages);
			}
		}

		public int GesamtHolz => _farmliste.Any() ? _farmliste.Sum(x => x.ErbeutetGesamt.Holz) : 0;
		public int GesamtLehm => _farmliste.Any() ? _farmliste.Sum(x => x.ErbeutetGesamt.Lehm) : 0;
		public int GesamtEisen => _farmliste.Any() ? _farmliste.Sum(x => x.ErbeutetGesamt.Eisen) : 0;
		public int Gesamt => _farmliste.Any() ? _farmliste.Sum(x => x.ErbeutetGesamt.Gesamt) : 0;

		public int GesamtHolzLast24 =>
			App.Berichte.Any(x => x.Kampfzeit < DateTime.Now && x.Kampfzeit > DateTime.Now.AddHours(-24) &&
			                      x.Beute != null)
				? App.Berichte.Where(x => x.Kampfzeit < DateTime.Now && x.Kampfzeit > DateTime.Now.AddHours(-24) &&
				                          x.Beute != null).Sum(x => x.Beute.Holz)
				: 0;

		public int GesamtLehmLast24 =>
			App.Berichte.Any(x => x.Kampfzeit < DateTime.Now && x.Kampfzeit > DateTime.Now.AddHours(-24) &&
			                      x.Beute != null)
				? App.Berichte.Where(x => x.Kampfzeit < DateTime.Now && x.Kampfzeit > DateTime.Now.AddHours(-24) &&
				                          x.Beute != null).Sum(x => x.Beute.Lehm)
				: 0;

		public int GesamtEisenLast24 =>
			App.Berichte.Any(x => x.Kampfzeit < DateTime.Now && x.Kampfzeit > DateTime.Now.AddHours(-24) &&
			                      x.Beute != null)
				? App.Berichte.Where(x => x.Kampfzeit < DateTime.Now && x.Kampfzeit > DateTime.Now.AddHours(-24) &&
				                          x.Beute != null).Sum(x => x.Beute.Eisen)
				: 0;

		public int GesamtLast24 =>
			App.Berichte.Any(x => x.Kampfzeit < DateTime.Now && x.Kampfzeit > DateTime.Now.AddHours(-24) &&
			                      x.Beute != null)
				? App.Berichte.Where(x => x.Kampfzeit < DateTime.Now && x.Kampfzeit > DateTime.Now.AddHours(-24) &&
				                          x.Beute != null).Sum(x => x.Beute.Gesamt)
				: 0;

		public int TotalAttacksSentLast24 =>
			_farmliste.Any() ? _farmliste.Sum(x => x.Attacks.Count(y => y.LandTime > DateTime.Now.AddHours(-24))) : 0;

		public int TotalAttacksSent => _farmliste.Any() ? _farmliste.Sum(x => x.Attacks.Count) : 0;
		public int GesamtErwartet => _farmliste.Any() ? _farmliste.Sum(x => x.Attacks.Sum(y => y.Erwartet)) : 0;

		public string DurchschnittErwartet =>
			_farmliste.Any(x => x.Attacks.Any())
				? _farmliste.Where(x => x.Attacks.Any()).Average(x => x.Attacks.Average(y => y.Erwartet))
				            .ToString("0.0")
				: "";

		public string DurchschnittBekommen =>
			App.Berichte.Any(x => x.Beute != null)
				? App.Berichte.Where(x => x.Beute != null).Average(x => x.Beute.Gesamt).ToString("0.0")
				: "";

		public ICommand RemoveFarmtargetCommand =>
			_removeFarmtargetCommand ?? (_removeFarmtargetCommand = new AsyncRelayCommand(DoRemoveFarmtarget));

		public ICommand DeactivateFarmtargetCommand =>
			_deactivateFarmtargetCommand ?? (_deactivateFarmtargetCommand = new RelayCommand(DoDeactivateFarmtarget));

		public ICommand ActivateFarmtargetCommand =>
			_activateFarmtargetCommand ?? (_activateFarmtargetCommand = new RelayCommand(DoActivateFarmtarget));

		public ICommand AddNewFarmtargetsCommand =>
			_addNewFarmtargetsCommand ?? (_addNewFarmtargetsCommand = new RelayCommand(AddNewFarmtargets));

		public ICommand ResetErfolgsquotientCommand =>
			_resetErfolgsquotientCommand ??
			(_resetErfolgsquotientCommand = new AsyncRelayCommand(ResetErfolgsquotient));

		public ICommand SetErfolgsquotientCommand =>
			_setErfolgsquotientCommand ?? (_setErfolgsquotientCommand = new RelayCommand(SetErfolgsquotient));

		public ICommand ShowFarmsettingsCommand =>
			_showFarmsettingsCommand ?? (_showFarmsettingsCommand = new RelayCommand(ShowFarmsettings));

		public ICommand DoActivateFixStatusCommand =>
			_doActivateFixStatusCommand ?? (_doActivateFixStatusCommand = new AsyncRelayCommand(DoActivateFixStatus));

		public ICommand DoDeactivateFixStatusCommand =>
			_doDeactivateFixStatusCommand ??
			(_doDeactivateFixStatusCommand = new AsyncRelayCommand(DoDeactivateFixStatus));

		private void ShowFarmsettings() {
			if (SelectedFarm != null) {
				FarmtargetSettingsViewModel vm = new FarmtargetSettingsViewModel(Window, SelectedFarm);
				FarmtargetSettingsView window = new FarmtargetSettingsView(Window, vm);
				window.ShowDialog();
			}
		}

		public FarmlisteViewModel(Window window) {
			Window = window;
			Farmliste = App.Farmliste;
			FarmlisteView = CollectionViewSource.GetDefaultView(Farmliste);
			_villagelistActualAccount = new AsyncObservableCollection<DorfViewModel>();
			foreach (var accountsetting in App.Settings.Accountsettingslist) {
				var spielerViewModel =
					App.PlayerlistWholeWorld.FirstOrDefault(x =>
						                                        x
							                                        .PlayerName
							                                        .Equals((!string.IsNullOrEmpty(accountsetting
								                                                                       .Uvname)
								                                                 ? accountsetting.Uvname
								                                                 : accountsetting.Accountname)));
				if (spielerViewModel != null) {
					int playeridx = spielerViewModel.PlayerId;
					_villagelistActualAccount.AddRange(accountsetting
					                                   .DoerferSettings
					                                   .Where(x => !string.IsNullOrEmpty(x.Dorf.Dorf.VillageName) &&
					                                               x.Dorf.PlayerId.Equals(playeridx))
					                                   .Select(x => x.Dorf));
				}
			}

			Villagelist = CollectionViewSource.GetDefaultView(_villagelistActualAccount);
			SelectedVillages = new AsyncObservableCollection<FarmtargetViewModel>();
		}

		private async void AddNewFarmtargets() {
			if (_umkreis > 0 && SelectedSenderVillage != null) {
				await Task.Run(() => {
					               List<DorfViewModel> neuefarmDoerfer =
						               FarmlistenVerwaltung.GetFarmableVillagesList(_umkreis
						                                                            , SelectedSenderVillage.XCoordinate
						                                                            , SelectedSenderVillage
							                                                            .YCoordinate);
					               foreach (var dorf in neuefarmDoerfer) {
						               FarmlistenVerwaltung.AddToFarmliste(0, 0, new Gebaeude(), new TruppenTemplate()
						                                                   , new TruppenTemplate(), dorf);
					               }
				               });
			}
		}

		private async Task DoRemoveFarmtarget() {
			if (await MessageDialog.ShowAsync(Resources.YouReallyWantToDeleteFarmingTargets, Resources.AreYouSure
			                                  , MessageBoxButton.YesNo, MessageDialogType.Accent, App.Window) ==
			    MessageBoxResult.No)
				return;
			if (SelectedVillages == null || !SelectedVillages.Any()) return;
			for (int i = 0; i < SelectedVillages.Count; i++) {
				FarmlistenVerwaltung.RemoveFromFarmliste(SelectedVillages[i]);
			}
		}

		private async Task ResetErfolgsquotient() {
			if (await MessageDialog.ShowAsync(Resources.YouReallyWantToResetTheEQ, Resources.AreYouSure
			                                  , MessageBoxButton.YesNo, MessageDialogType.Accent, App.Window) ==
			    MessageBoxResult.No)
				return;
			if (SelectedVillages == null || !SelectedVillages.Any()) return;
			foreach (var farmtarget in SelectedVillages) {
				farmtarget.Erfolgsquotient = 100;
				try {
					foreach (var bericht in
						App.Berichte.Where(x => x.VerteidigerVillageId.Equals(farmtarget.Dorf.VillageId))) {
						bericht.Erfolgsquotient = 100;
					}
				} catch {
					// ignored
				}
			}
		}

		private async Task DoActivateFixStatus() {
			if (await MessageDialog.ShowAsync(Resources.ReallyFixTheStatus, Resources.AreYouSure, MessageBoxButton.YesNo
			                                  , MessageDialogType.Accent, App.Window) == MessageBoxResult.No)
				return;
			if (SelectedVillages == null || !SelectedVillages.Any()) return;
			foreach (var farmtarget in SelectedVillages) {
				farmtarget.FixStatus = true;
			}
		}

		private async Task DoDeactivateFixStatus() {
			if (await MessageDialog.ShowAsync(Resources.ReallyFixTheStatus, Resources.AreYouSure, MessageBoxButton.YesNo
			                                  , MessageDialogType.Accent, App.Window) == MessageBoxResult.No)
				return;
			if (SelectedVillages == null || !SelectedVillages.Any()) return;
			foreach (var farmtarget in SelectedVillages) {
				farmtarget.FixStatus = false;
			}
		}

		private void SetErfolgsquotient() {
			if (SelectedVillages == null || !SelectedVillages.Any()) return;
			var window = new InputWindow("EQ:");
			window.ShowDialog();
			var result = window.Answer;
			int.TryParse(result, out var eq);
			if (eq > 0) {
				foreach (var farmtarget in SelectedVillages) {
					farmtarget.Erfolgsquotient = eq;
					try {
						foreach (var bericht in
							App.Berichte.Where(x => x.VerteidigerVillageId.Equals(farmtarget.Dorf.VillageId))) {
							bericht.Erfolgsquotient = 100;
						}
					} catch {
						// ignored
					}
				}
			}
		}

		private void DoDeactivateFarmtarget() {
			if (SelectedVillages == null || !SelectedVillages.Any()) return;
			foreach (var farmtarget in SelectedVillages) {
				farmtarget.Active = false;
			}
		}

		private void DoActivateFarmtarget() {
			if (SelectedVillages == null || !SelectedVillages.Any()) return;
			foreach (var farmtarget in SelectedVillages) {
				farmtarget.Active = true;
			}
		}

		private void DoAddFarmtarget() {
			if (!string.IsNullOrEmpty(ZielCoordinate)) {
				try {
					string[] splitted = ZielCoordinate.Split('|');
					int.TryParse(splitted[0], out var x);
					int.TryParse(splitted[1], out var y);

					if (x > 0 && y > 0) {
						FarmlistenVerwaltung.AddToFarmliste(x, y, null, null, null);
					}
				} catch {
					MessageBox.Show(Resources.InvalidCoordinates, Resources.Invalid, MessageBoxButton.OK
					                , MessageBoxImage.Exclamation);
				}
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}