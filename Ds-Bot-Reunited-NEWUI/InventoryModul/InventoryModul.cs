﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Ds_Bot_Reunited_NEWUI.Accountsettings.Ds_Bot_Reunited.Accountsettings;
using Ds_Bot_Reunited_NEWUI.Botcaptcha;
using Ds_Bot_Reunited_NEWUI.BrowserManager;
using Ds_Bot_Reunited_NEWUI.Constants;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using OpenQA.Selenium;

namespace Ds_Bot_Reunited_NEWUI.InventoryModul {
    public static class InventoryModul {
        public static async Task UseInventoryRohstoffe(Browser browser, Accountsetting accountsettings) {
            if (accountsettings.SetInventoryItemsActive && accountsettings.MultiaccountingDaten.InventoryLastUsed < DateTime.Now.AddHours(-24)) {
                string link = App.Settings.OperateLink + accountsettings.UvZusatzFuerLink + "&screen=inventory";
                browser.WebDriver.Navigate().GoToUrl(link);
                browser.WebDriver.WaitForPageload();
                await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings);
                int counter = 0;
                while (counter < 10 && browser.WebDriver.FindElements(By.XPath("//img[contains(@src,'items/resources_percent.png')]")).Count > 0) {
                    counter++;
                    try {
                        browser.WebDriver.FindElements(By.XPath("//img[contains(@src,'items/resources_percent.png')]"))[0].Click();
                        await Rndm.Sleep(300, 500);
                        browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.EigenesProfil.Inventar.ITEM_AKTIVIEREN_BUTTON_SELECTOR_XPATH)).Click();
                        await Rndm.Sleep(300, 500);
                        browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.EigenesProfil.Inventar.ITEM_BESTAETIGEN_BUTTON_SELECTOR_XPATH)).Click();
                        await Rndm.Sleep(300, 500);

                        browser.WebDriver.FindElement(By.XPath("//button[@class='popup_closer btn btn-confirm-yes btn-centered']")).Click();
                        await Rndm.Sleep(300, 500);
                    } catch { }
                }
                accountsettings.MultiaccountingDaten.InventoryLastUsed = DateTime.Now;
            }
        }

        public static async Task UseInventorySkills(Browser browser, Accountsetting accountsettings) {
            if (accountsettings.MultiaccountingDaten.PaladinLastSkillUsed < DateTime.Now.AddHours(-1)) {
                try {
                    string link = App.Settings.OperateLink + accountsettings.UvZusatzFuerLink + "&screen=inventory";
                    browser.WebDriver.Navigate().GoToUrl(link);
                    await Rndm.Sleep(500, 800);
                    await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings);
                    if (browser.WebDriver.FindElements(By.XPath("//img[contains(@src,'items/knight_skill')]")).Any()) {
                        var elements = browser.WebDriver.FindElements(By.XPath("//img[contains(@src,'items/knight_skill')]")).OrderByDescending(x => x.GetAttribute("src")).ToList();
                        int count = 0;
                        int index = 0;
                        while (elements.Any() && elements.Count > index && count < 6) {
                            elements[index].Click();
                            await Rndm.Sleep(500, 800);
                            browser.WebDriver.FindElement(By.XPath("//a[contains(@href,'javascript:Inventory.openItemDialog')]")).Click();
                            await Rndm.Sleep(500, 800);
                            try {
                                if (browser.WebDriver.FindElement(By.XPath("//a[@class='btn btn-default']")).Displayed)
                                    browser.WebDriver.FindElement(By.XPath("//a[@class='btn btn-default']")).Click();
                                else {
                                    index++;
                                    browser.WebDriver.FindElement(By.XPath("//a[@class='popup_box_close tooltip-delayed']")).Click();
                                }
                            } catch {
                                App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + " Skill zu hoch für Paladin");
                                index++;
                                browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Quest.QUESTFENSTER_SCHLIESSEN_BUTTON_SELECTOR_XPATH)).Click();
                            }
                            await Rndm.Sleep(500, 800);
                            await QuestModul.QuestModul.CheckIfQuestOpenAndCloseIt(browser);
                            await Rndm.Sleep(1000, 2000);
                            elements = browser.WebDriver.FindElements(By.XPath("//img[contains(@src,'items/knight_skill')]")).OrderByDescending(x => x.GetAttribute("src")).ToList();
                            count++;
                        }
                    }
                    accountsettings.MultiaccountingDaten.PaladinLastSkillUsed = DateTime.Now;
                } catch (Exception e) {
                    App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + " Failure InventoryModul " + e);
                }
            }
        }
    }
}
