﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;
using Ds_Bot_Reunited_NEWUI.Annotations;
using Ds_Bot_Reunited_NEWUI.BauenModul;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.Properties;
using Ds_Bot_Reunited_NEWUI.Settings;
using Framework.UI.Controls;
using Window = System.Windows.Window;

namespace Ds_Bot_Reunited_NEWUI.BauensettingsDorf {
    public class BauensettingsDorfViewModel : INotifyPropertyChanged {
        private readonly AccountDorfSettings _dorfSettings;
        private ICollectionView _bauschleifeView;
        private ICommand _minusCommand;
        private ICommand _moveDownCommand;
        private ICommand _moveUpCommand;
        private ICommand _plusCommand;
        private ICommand _removeCommand;
        private AsyncObservableCollection<Bauelement> _selectedBauschleife;
        private ICommand _setTemplateCommand;
        private ICommand _removeListCommand;


        public BauensettingsDorfViewModel(AccountDorfSettings dorfSettings, Window window) {
            Window = window;
            _dorfSettings = dorfSettings;
            BindingOperations.EnableCollectionSynchronization(Bauschleife, BauschleifeLock);
            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() => {
                                                                                                     BauschleifeView = CollectionViewSource.GetDefaultView(Bauschleife);
                                                                                                     BauschleifeView.SortDescriptions.Add(new SortDescription(nameof(Bauelement.Priority), ListSortDirection.Ascending));
                                                                                                 }));
            SelectedBauschleife = new AsyncObservableCollection<Bauelement>();
        }

        public Window Window { get; set; }
        public string Villagename => _dorfSettings.Dorf.Name;
        public bool IsEnabled => App.Permissions.BuildingIsEnabled;

        public bool BuildingActive {
            get => _dorfSettings.BauenActive;
            set {
                _dorfSettings.BauenActive = value;
                OnPropertyChanged();
            }
        }

        public int Baueninterval {
            get => _dorfSettings.Baueninterval;
            set {
                _dorfSettings.Baueninterval = value;
                OnPropertyChanged();
            }
        }

        public ICollectionView BauschleifeView {
            get => _bauschleifeView;
            set {
                _bauschleifeView = value;
                OnPropertyChanged();
            }
        }

        [Obfuscation(Exclude = true, Feature = "renaming")]
        public AsyncObservableCollection<Bauelement> Bauschleife {
            get => _dorfSettings.Bauschleife;
            set {
                _dorfSettings.Bauschleife = value;
                OnPropertyChanged();
            }
        }

        [Obfuscation(Exclude = true, Feature = "renaming")]
        public AsyncObservableCollection<Bauelement> SelectedBauschleife {
            get => _selectedBauschleife;
            set {
                _selectedBauschleife = value;
                OnPropertyChanged();
            }
        }

        public Visibility KircheVisible => App.Settings.Weltensettings.KircheActive ? Visibility.Visible : Visibility.Collapsed;

        public Visibility WatchtowerVisible => App.Settings.Weltensettings.WatchtowerActive ? Visibility.Visible : Visibility.Collapsed;

        public int ActualLevelMain => _dorfSettings.Dorfinformationen.GebaeudeStufen.Hauptgebaeude;

        public int WantedLevelMain => _dorfSettings.Dorfinformationen.GebaeudeStufen.Hauptgebaeude + (Bauschleife != null && Bauschleife.Any(x => x.Gebaeude.Equals(Gebaeude.Hauptgebaeude))
                                                                                                          ? Bauschleife.Last(x => x.Gebaeude.Equals(Gebaeude.Hauptgebaeude)).TargetLevel - _dorfSettings.Dorfinformationen.GebaeudeStufen.Hauptgebaeude
                                                                                                          : 0);

        public int ActualLevelBarracks => _dorfSettings.Dorfinformationen.GebaeudeStufen.Kaserne;

        public int WantedLevelBarracks => _dorfSettings.Dorfinformationen.GebaeudeStufen.Kaserne + (Bauschleife != null && Bauschleife.Any(x => x.Gebaeude.Equals(Gebaeude.Kaserne))
                                                                                                        ? Bauschleife.Last(x => x.Gebaeude.Equals(Gebaeude.Kaserne)).TargetLevel - _dorfSettings.Dorfinformationen.GebaeudeStufen.Kaserne : 0);

        public int ActualLevelStable => _dorfSettings.Dorfinformationen.GebaeudeStufen.Stall;

        public int WantedLevelStable => _dorfSettings.Dorfinformationen.GebaeudeStufen.Stall + (Bauschleife != null && Bauschleife.Any(x => x.Gebaeude.Equals(Gebaeude.Stall))
                                                                                                    ? Bauschleife.Last(x => x.Gebaeude.Equals(Gebaeude.Stall)).TargetLevel - _dorfSettings.Dorfinformationen.GebaeudeStufen.Stall : 0);

        public int ActualLevelGarage => _dorfSettings.Dorfinformationen.GebaeudeStufen.Werkstatt;

        public int WantedLevelGarage => _dorfSettings.Dorfinformationen.GebaeudeStufen.Werkstatt + (Bauschleife != null && Bauschleife.Any(x => x.Gebaeude.Equals(Gebaeude.Werkstatt))
                                                                                                        ? Bauschleife.Last(x => x.Gebaeude.Equals(Gebaeude.Werkstatt)).TargetLevel - _dorfSettings.Dorfinformationen.GebaeudeStufen.Werkstatt : 0);

        public int ActualLevelChurch => _dorfSettings.Dorfinformationen.GebaeudeStufen.Kirche;

        public int WantedLevelChurch => _dorfSettings.Dorfinformationen.GebaeudeStufen.Kirche + (Bauschleife != null && Bauschleife.Any(x => x.Gebaeude.Equals(Gebaeude.Kirche))
                                                                                                     ? Bauschleife.Last(x => x.Gebaeude.Equals(Gebaeude.Kirche)).TargetLevel - _dorfSettings.Dorfinformationen.GebaeudeStufen.Kirche : 0);

        public int ActualLevelFirstChurch => _dorfSettings.Dorfinformationen.GebaeudeStufen.ErsteKirche;

        public int WantedLevelFirstChurch => _dorfSettings.Dorfinformationen.GebaeudeStufen.ErsteKirche + (Bauschleife != null && Bauschleife.Any(x => x.Gebaeude.Equals(Gebaeude.Erstekirche))
                                                                                                               ? Bauschleife.Last(x => x.Gebaeude.Equals(Gebaeude.Erstekirche)).TargetLevel - _dorfSettings.Dorfinformationen.GebaeudeStufen.ErsteKirche
                                                                                                               : 0);

        public int ActualLevelSnob => _dorfSettings.Dorfinformationen.GebaeudeStufen.Adelshof;

        public int WantedLevelSnob => _dorfSettings.Dorfinformationen.GebaeudeStufen.Adelshof + (Bauschleife != null && Bauschleife.Any(x => x.Gebaeude.Equals(Gebaeude.Adelshof))
                                                                                                     ? Bauschleife.Last(x => x.Gebaeude.Equals(Gebaeude.Adelshof)).TargetLevel - _dorfSettings.Dorfinformationen.GebaeudeStufen.Adelshof : 0);

        public int ActualLevelSmith => _dorfSettings.Dorfinformationen.GebaeudeStufen.Schmiede;

        public int WantedLevelSmith => _dorfSettings.Dorfinformationen.GebaeudeStufen.Schmiede + (Bauschleife != null && Bauschleife.Any(x => x.Gebaeude.Equals(Gebaeude.Schmiede))
                                                                                                      ? Bauschleife.Last(x => x.Gebaeude.Equals(Gebaeude.Schmiede)).TargetLevel - _dorfSettings.Dorfinformationen.GebaeudeStufen.Schmiede : 0);

        public int ActualLevelPlace => _dorfSettings.Dorfinformationen.GebaeudeStufen.Versammlungsplatz;

        public int WantedLevelPlace => _dorfSettings.Dorfinformationen.GebaeudeStufen.Versammlungsplatz + (Bauschleife != null && Bauschleife.Any(x => x.Gebaeude.Equals(Gebaeude.Versammlungsplatz))
                                                                                                               ? Bauschleife.Last(x => x.Gebaeude.Equals(Gebaeude.Versammlungsplatz)).TargetLevel -
                                                                                                                 _dorfSettings.Dorfinformationen.GebaeudeStufen.Versammlungsplatz : 0);

        public int ActualLevelStatue => _dorfSettings.Dorfinformationen.GebaeudeStufen.Statue;

        public int WantedLevelStatue => _dorfSettings.Dorfinformationen.GebaeudeStufen.Statue + (Bauschleife != null && Bauschleife.Any(x => x.Gebaeude.Equals(Gebaeude.Statue))
                                                                                                     ? Bauschleife.Last(x => x.Gebaeude.Equals(Gebaeude.Statue)).TargetLevel - _dorfSettings.Dorfinformationen.GebaeudeStufen.Statue : 0);

        public int ActualLevelMarket => _dorfSettings.Dorfinformationen.GebaeudeStufen.Marktplatz;

        public int WantedLevelMarket => _dorfSettings.Dorfinformationen.GebaeudeStufen.Marktplatz + (Bauschleife != null && Bauschleife.Any(x => x.Gebaeude.Equals(Gebaeude.Marktplatz))
                                                                                                         ? Bauschleife.Last(x => x.Gebaeude.Equals(Gebaeude.Marktplatz)).TargetLevel - _dorfSettings.Dorfinformationen.GebaeudeStufen.Marktplatz : 0);

        public int ActualLevelWood => _dorfSettings.Dorfinformationen.GebaeudeStufen.Holzfaeller;

        public int WantedLevelWood => _dorfSettings.Dorfinformationen.GebaeudeStufen.Holzfaeller + (Bauschleife != null && Bauschleife.Any(x => x.Gebaeude.Equals(Gebaeude.Holzfaeller))
                                                                                                        ? Bauschleife.Last(x => x.Gebaeude.Equals(Gebaeude.Holzfaeller)).TargetLevel - _dorfSettings.Dorfinformationen.GebaeudeStufen.Holzfaeller : 0);

        public int ActualLevelStone => _dorfSettings.Dorfinformationen.GebaeudeStufen.Lehmgrube;

        public int WantedLevelStone => _dorfSettings.Dorfinformationen.GebaeudeStufen.Lehmgrube + (Bauschleife != null && Bauschleife.Any(x => x.Gebaeude.Equals(Gebaeude.Lehmgrube))
                                                                                                       ? Bauschleife.Last(x => x.Gebaeude.Equals(Gebaeude.Lehmgrube)).TargetLevel - _dorfSettings.Dorfinformationen.GebaeudeStufen.Lehmgrube : 0);

        public int ActualLevelIron => _dorfSettings.Dorfinformationen.GebaeudeStufen.Eisenmine;

        public int WantedLevelIron => _dorfSettings.Dorfinformationen.GebaeudeStufen.Eisenmine + (Bauschleife != null && Bauschleife.Any(x => x.Gebaeude.Equals(Gebaeude.Eisenmine))
                                                                                                      ? Bauschleife.Last(x => x.Gebaeude.Equals(Gebaeude.Eisenmine)).TargetLevel - _dorfSettings.Dorfinformationen.GebaeudeStufen.Eisenmine : 0);

        public int ActualLevelStorage => _dorfSettings.Dorfinformationen.GebaeudeStufen.Speicher;

        public int WantedLevelStorage => _dorfSettings.Dorfinformationen.GebaeudeStufen.Speicher + (Bauschleife != null && Bauschleife.Any(x => x.Gebaeude.Equals(Gebaeude.Speicher))
                                                                                                        ? Bauschleife.Last(x => x.Gebaeude.Equals(Gebaeude.Speicher)).TargetLevel - _dorfSettings.Dorfinformationen.GebaeudeStufen.Speicher : 0);

        public int ActualLevelFarm => _dorfSettings.Dorfinformationen.GebaeudeStufen.Bauernhof;

        public int WantedLevelFarm => _dorfSettings.Dorfinformationen.GebaeudeStufen.Bauernhof + (Bauschleife != null && Bauschleife.Any(x => x.Gebaeude.Equals(Gebaeude.Bauernhof))
                                                                                                      ? Bauschleife.Last(x => x.Gebaeude.Equals(Gebaeude.Bauernhof)).TargetLevel - _dorfSettings.Dorfinformationen.GebaeudeStufen.Bauernhof : 0);

        public int ActualLevelHide => _dorfSettings.Dorfinformationen.GebaeudeStufen.Versteck;

        public int WantedLevelHide => _dorfSettings.Dorfinformationen.GebaeudeStufen.Versteck + (Bauschleife != null && Bauschleife.Any(x => x.Gebaeude.Equals(Gebaeude.Versteck))
                                                                                                     ? Bauschleife.Last(x => x.Gebaeude.Equals(Gebaeude.Versteck)).TargetLevel - _dorfSettings.Dorfinformationen.GebaeudeStufen.Versteck : 0);

        public int ActualLevelWall => _dorfSettings.Dorfinformationen.GebaeudeStufen.Wall;

        public int WantedLevelWall => _dorfSettings.Dorfinformationen.GebaeudeStufen.Wall + (Bauschleife != null && Bauschleife.Any(x => x.Gebaeude.Equals(Gebaeude.Wall))
                                                                                                 ? Bauschleife.Last(x => x.Gebaeude.Equals(Gebaeude.Wall)).TargetLevel - _dorfSettings.Dorfinformationen.GebaeudeStufen.Wall : 0);

        public int ActualLevelWatchTower => _dorfSettings.Dorfinformationen.GebaeudeStufen.Watchtower;

        public int WantedLevelWatchTower => _dorfSettings.Dorfinformationen.GebaeudeStufen.Watchtower + (Bauschleife != null && Bauschleife.Any(x => x.Gebaeude.Equals(Gebaeude.WatchTower))
                                                                                                             ? Bauschleife.Last(x => x.Gebaeude.Equals(Gebaeude.WatchTower)).TargetLevel - _dorfSettings.Dorfinformationen.GebaeudeStufen.Watchtower : 0);

        public string LastBuilding => " " + _dorfSettings.Dorfinformationen.LastBauenZeitpunkt.ToString("dd.MM.yy hh:mm:ss", CultureInfo.InvariantCulture);

        public string NextBuilding => " " + _dorfSettings.NextAvailableBauschleife.ToString("dd.MM.yy hh:mm:ss", CultureInfo.InvariantCulture);

        public int BauenHolzUebrigLassen {
            get => _dorfSettings.BauenHolzUebrigLassen;
            set {
                _dorfSettings.BauenHolzUebrigLassen = value;
                OnPropertyChanged();
            }
        }

        public int BauenLehmUebrigLassen {
            get => _dorfSettings.BauenLehmUebrigLassen;
            set {
                _dorfSettings.BauenLehmUebrigLassen = value;
                OnPropertyChanged();
            }
        }

        public int BauenEisenUebrigLassen {
            get => _dorfSettings.BauenEisenUebrigLassen;
            set {
                _dorfSettings.BauenEisenUebrigLassen = value;
                OnPropertyChanged();
            }
        }

        public bool BauenMinenAusgleichen {
            get => _dorfSettings.BauenMinenAusgleichen;
            set {
                _dorfSettings.BauenMinenAusgleichen = value;
                OnPropertyChanged();
            }
        }

        public bool BhAutomatischBauenActive {
            get => _dorfSettings.BhAutomatischBauenActive;
            set {
                _dorfSettings.BhAutomatischBauenActive = value;
                OnPropertyChanged();
            }
        }

        public bool SpeicherAutomatischBauenActive {
            get => _dorfSettings.SpeicherAutomatischBauenActive;
            set {
                _dorfSettings.SpeicherAutomatischBauenActive = value;
                OnPropertyChanged();
            }
        }


        public ICommand MoveUpCommand => _moveUpCommand ?? (_moveUpCommand = new RelayCommand<int>(MoveUp));
        public ICommand MoveDownCommand => _moveDownCommand ?? (_moveDownCommand = new RelayCommand<int>(MoveDown));
        public ICommand RemoveCommand => _removeCommand ?? (_removeCommand = new RelayCommand<int>(Remove));
        public ICommand PlusCommand => _plusCommand ?? (_plusCommand = new RelayCommand<Gebaeude>(Plus));
        public ICommand MinusCommand => _minusCommand ?? (_minusCommand = new RelayCommand<Gebaeude>(Minus));
        public ICommand SetTemplateCommand => _setTemplateCommand ?? (_setTemplateCommand = new AsyncRelayCommand(SetTemplate));
        public ICommand RemoveListCommand => _removeListCommand ?? (_removeListCommand = new RelayCommand(RemoveFromList));

        public object BauschleifeLock => _dorfSettings.BauschleifeLock;

        public event PropertyChangedEventHandler PropertyChanged;

        public async Task InitBauschleife() {
            await Task.Run(() => {
                               foreach (var bauelement in _dorfSettings.Bauschleife)
                                   Bauschleife.Add(bauelement);
                           });
        }

        private async void Plus(Gebaeude gebaeude) {
            if (Bauschleife != null) {
                var last = Bauschleife.OrderBy(x => x.Priority).LastOrDefault();
                var newBauelement = new Bauelement();
                if (last != null && last.Priority.Equals(Bauschleife.Max(x => x.Priority)) && last.Gebaeude.Equals(gebaeude)) {
                    newBauelement = last;
                    if (MaxStufe.GetMaxStufeOfGebaeude(gebaeude) < newBauelement.TargetLevel + 1) {
                        await MessageDialog.ShowAsync("", Resources.BuildingAlreadyOnMaxLevel, MessageBoxButton.OK, MessageDialogType.Accent, Window);
                        return;
                    }
                    newBauelement.TargetLevel = newBauelement.TargetLevel + 1;
                } else {
                    newBauelement.ImageSource = GebaeudeLinks.GetImageSourceOfGebaeude(gebaeude);
                    newBauelement.TargetLevel = _dorfSettings.Dorfinformationen.GebaeudeStufen.Stufe(gebaeude) +
                                                (Bauschleife.Any(x => x.Gebaeude.Equals(gebaeude)) ? Bauschleife.Last(x => x.Gebaeude.Equals(gebaeude)).TargetLevel - _dorfSettings.Dorfinformationen.GebaeudeStufen.Stufe(gebaeude) : 0) + 1;
                    newBauelement.Priority = (last?.Priority ?? 0) + 1;
                    newBauelement.Gebaeude = gebaeude;
                    if (MaxStufe.GetMaxStufeOfGebaeude(gebaeude) < newBauelement.TargetLevel) {
                        await MessageDialog.ShowAsync("", Resources.BuildingAlreadyOnMaxLevel, MessageBoxButton.OK, MessageDialogType.Accent, Window);
                        return;
                    }
                    Bauschleife.Add(newBauelement);
                }
            }
            BauschleifeView.Refresh();
            OnPropertyChanged("");
        }

        private async void Minus(Gebaeude gebaeude) {
            var last = Bauschleife?.OrderBy(x => x.Priority).LastOrDefault(x => x.Gebaeude.Equals(gebaeude) && x.TargetLevel != _dorfSettings.Dorfinformationen.GebaeudeStufen.Stufe(gebaeude));
            if (last != null)
                if (last.TargetLevel == 1 || Bauschleife.Any(x => x.Gebaeude.Equals(gebaeude) && x.TargetLevel.Equals(last.TargetLevel - 1))) {
                    foreach (var gebaeud in Bauschleife.Where(x => x.Priority > last.Priority))
                        gebaeud.Priority -= 1;
                    Bauschleife.Remove(last);
                } else
                    last.TargetLevel -= 1;
            else {
                await MessageDialog.ShowAsync("", Resources.BuildingIsAlreadyAtLowest, MessageBoxButton.OK, MessageDialogType.Accent, Window);
                return;
            }
            BauschleifeView.Refresh();
            OnPropertyChanged("");
        }

        private void MoveUp(int priority) {
            if (Bauschleife != null && Bauschleife.Any()) {
                var bauelement = Bauschleife.FirstOrDefault(x => x.Priority.Equals(priority));
                if (bauelement != null) {
                    var gebaeudeVorher = Bauschleife.OrderBy(x => x.Priority).LastOrDefault(x => x.Priority < bauelement.Priority);
                    if (gebaeudeVorher != null) {
                        var prioritaet = gebaeudeVorher.Priority;
                        gebaeudeVorher.Priority = bauelement.Priority;
                        bauelement.Priority = prioritaet;
                        if (bauelement.Gebaeude == gebaeudeVorher.Gebaeude) {
                            var stufe = gebaeudeVorher.TargetLevel;
                            gebaeudeVorher.TargetLevel = bauelement.TargetLevel;
                            bauelement.TargetLevel = stufe;
                        }
                    }
                }
            }
            BauschleifeView.Refresh();
        }

        private void MoveDown(int priority) {
            if (Bauschleife != null && Bauschleife.Any()) {
                var bauelement = Bauschleife.FirstOrDefault(x => x.Priority.Equals(priority));
                if (bauelement != null) {
                    var gebaeudeVorher = Bauschleife.OrderBy(x => x.Priority).FirstOrDefault(x => x.Priority > bauelement.Priority);
                    if (gebaeudeVorher != null) {
                        var prioritaet = gebaeudeVorher.Priority;
                        gebaeudeVorher.Priority = bauelement.Priority;
                        bauelement.Priority = prioritaet;
                        if (bauelement.Gebaeude == gebaeudeVorher.Gebaeude) {
                            var stufe = gebaeudeVorher.TargetLevel;
                            gebaeudeVorher.TargetLevel = bauelement.TargetLevel;
                            bauelement.TargetLevel = stufe;
                        }
                    }
                }
            }
            BauschleifeView.Refresh();
        }

        private void Remove(int priority) {
            if (Bauschleife != null && Bauschleife.Any()) {
                var bauelement = Bauschleife.FirstOrDefault(x => x.Priority.Equals(priority));
                if (bauelement != null)
                    Bauschleife.Remove(bauelement);
                BauschleifeView.Refresh();
            }
        }

        private void RemoveFromList() {
            var list = SelectedBauschleife.ToList();
            foreach (var eintrag in list) {
                Bauschleife.Remove(eintrag);
            }
        }

        private async Task SetTemplate() {
            int prioritaet = 0;

            if (await MessageDialog.ShowAsync("You want to set the Template Buildingqueue? Your actual Buildingqueue gets deleted!", Resources.Achtung, MessageBoxButton.YesNo, MessageDialogType.Accent, App.Window) == MessageBoxResult.No) {
                return;
            }


            Bauschleife.Clear();
            
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Holzfaeller, TargetLevel = 1, Priority = prioritaet++, ImageSource = GebaeudeLinks.GetImageSourceOfGebaeude(Gebaeude.Holzfaeller) });
            
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Lehmgrube, TargetLevel = 1, Priority = prioritaet++, ImageSource = GebaeudeLinks.GetImageSourceOfGebaeude(Gebaeude.Lehmgrube) });
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Eisenmine, TargetLevel = 1, Priority = prioritaet++, ImageSource = GebaeudeLinks.GetImageSourceOfGebaeude(Gebaeude.Eisenmine) });
            
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Holzfaeller, TargetLevel = 2, Priority = prioritaet++, ImageSource = GebaeudeLinks.GetImageSourceOfGebaeude(Gebaeude.Holzfaeller) });
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Lehmgrube, TargetLevel = 2, Priority = prioritaet++, ImageSource = GebaeudeLinks.GetImageSourceOfGebaeude(Gebaeude.Lehmgrube) });
            
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Hauptgebaeude, TargetLevel = 2, Priority = prioritaet++ });
            
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Hauptgebaeude, TargetLevel = 3, Priority = prioritaet++ });
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Kaserne, TargetLevel = 1, Priority = prioritaet++ });
            
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Holzfaeller, TargetLevel = 3, Priority = prioritaet++ });
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Lehmgrube, TargetLevel = 3, Priority = prioritaet++ });
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Kaserne, TargetLevel = 2, Priority = prioritaet++ });
            
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Speicher, TargetLevel = 2, Priority = prioritaet++ });
            
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Eisenmine, TargetLevel = 2, Priority = prioritaet++ });
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Speicher, TargetLevel = 3, Priority = prioritaet++ });
            
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Kaserne, TargetLevel = 3, Priority = prioritaet++ });
            
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Statue, TargetLevel = 1, Priority = prioritaet++ });
            
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Eisenmine, TargetLevel = 3, Priority = prioritaet++ });
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Bauernhof, TargetLevel = 2, Priority = prioritaet++ });
            
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Hauptgebaeude, TargetLevel = 5, Priority = prioritaet++ });
            
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Wall, TargetLevel = 1, Priority = prioritaet++ });
            
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Marktplatz, TargetLevel = 1, Priority = prioritaet++ });
            
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Holzfaeller, TargetLevel = 4, Priority = prioritaet++ });
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Lehmgrube, TargetLevel = 4, Priority = prioritaet++ });
            
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Versteck, TargetLevel = 3, Priority = prioritaet++ });
            
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Holzfaeller, TargetLevel = 5, Priority = prioritaet++ });
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Lehmgrube, TargetLevel = 5, Priority = prioritaet++ });

            
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Schmiede, TargetLevel = 1, Priority = prioritaet++ });

            
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Holzfaeller, TargetLevel = 6, Priority = prioritaet++ });
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Lehmgrube, TargetLevel = 6, Priority = prioritaet++ });
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Eisenmine, TargetLevel = 5, Priority = prioritaet++ });

            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Holzfaeller, TargetLevel = 7, Priority = prioritaet++ });
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Lehmgrube, TargetLevel = 7, Priority = prioritaet++ });
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Eisenmine, TargetLevel = 6, Priority = prioritaet++ });
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Holzfaeller, TargetLevel = 8, Priority = prioritaet++ });
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Lehmgrube, TargetLevel = 8, Priority = prioritaet++ });
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Eisenmine, TargetLevel = 7, Priority = prioritaet++ });
            

            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Speicher, TargetLevel = 6, Priority = prioritaet++ });
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Bauernhof, TargetLevel = 4, Priority = prioritaet++ });


            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Holzfaeller, TargetLevel = 9, Priority = prioritaet++ });
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Lehmgrube, TargetLevel = 9, Priority = prioritaet++ });
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Eisenmine, TargetLevel = 8, Priority = prioritaet++ });
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Holzfaeller, TargetLevel = 10, Priority = prioritaet++ });
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Lehmgrube, TargetLevel = 10, Priority = prioritaet++ });
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Eisenmine, TargetLevel = 9, Priority = prioritaet++ });
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Holzfaeller, TargetLevel = 11, Priority = prioritaet++ });
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Lehmgrube, TargetLevel = 11, Priority = prioritaet++ });
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Eisenmine, TargetLevel = 12, Priority = prioritaet++ });

            
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Hauptgebaeude, TargetLevel = 7, Priority = prioritaet++ });
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Kaserne, TargetLevel = 5, Priority = prioritaet++ });
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Bauernhof, TargetLevel = 5, Priority = prioritaet++ });

            

            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Hauptgebaeude, TargetLevel = 10, Priority = prioritaet++ });
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Schmiede, TargetLevel = 5, Priority = prioritaet++ });

            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Stall, TargetLevel = 1, Priority = prioritaet++ });
            
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Holzfaeller, TargetLevel = 12, Priority = prioritaet++ });
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Lehmgrube, TargetLevel = 12, Priority = prioritaet++ });
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Eisenmine, TargetLevel = 14, Priority = prioritaet++ });
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Wall, TargetLevel = 5, Priority = prioritaet++ });
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Holzfaeller, TargetLevel = 13, Priority = prioritaet++ });
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Lehmgrube, TargetLevel = 13, Priority = prioritaet++ });
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Eisenmine, TargetLevel = 15, Priority = prioritaet++ });
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Holzfaeller, TargetLevel = 14, Priority = prioritaet++ });
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Lehmgrube, TargetLevel = 14, Priority = prioritaet++ });
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Holzfaeller, TargetLevel = 15, Priority = prioritaet++ });
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Lehmgrube, TargetLevel = 15, Priority = prioritaet++ });
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Eisenmine, TargetLevel = 17, Priority = prioritaet++ });
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Holzfaeller, TargetLevel = 20, Priority = prioritaet++ });
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Lehmgrube, TargetLevel = 20, Priority = prioritaet++ });
            Bauschleife.Add(new Bauelement { Gebaeude = Gebaeude.Eisenmine, TargetLevel = 20, Priority = prioritaet });
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
