﻿using System.ComponentModel;
using Ds_Bot_Reunited_NEWUI.Farmen;

namespace Ds_Bot_Reunited_NEWUI.Rekrutieren {
    public class Rekrutelement {
        public bool Active { get; set; }
        public TruppenTemplate ZielTruppen { get; set; }
        public int PunkteUntergrenze { get; set; }
        public int PunkteObergrenze { get; set; }
        public int ProzentSatzRessourcenUebrigLassenFuerBauen { get; set; }
        public bool IsSelected { get; set; }
        public int Reihenfolge { get; set; }

        public Rekrutelement() {
            ZielTruppen = new TruppenTemplate();
        }

        public Rekrutelement Clone() {
            Rekrutelement newRekrutelement = new Rekrutelement {
                ProzentSatzRessourcenUebrigLassenFuerBauen = ProzentSatzRessourcenUebrigLassenFuerBauen,
                PunkteUntergrenze = PunkteUntergrenze,
                ZielTruppen = ZielTruppen.Clone(),
                PunkteObergrenze = PunkteObergrenze,
                Active = Active,
                Reihenfolge = Reihenfolge
            };
            return newRekrutelement;
        }
    }

    public enum Truppe {
        [Description("spear")] Speertraeger,
        [Description("sword")] Schwertkaempfer,
        [Description("axe")] Axtkaempfer,
        [Description("archer")] Bogenschuetzen,
        [Description("spy")] Spaeher,
        [Description("light")] LeichteKavallerie,
        [Description("marcher")] BeritteneBogenschuetzen,
        [Description("heavy")] SchwereKavallerie,
        [Description("ram")] Rammboecke,
        [Description("cat")] Katapulte,
        [Description("knight")] Paladin,
        [Description("snob")] Adelsgeschlecht
    }

    public static class Truppeb {
        public const string Speertraeger = "spear";
        public const string Schwertkaempfer = "sword";
        public const string Axtkaempfer = "axe";
        public const string Bogenschuetzen = "archer";
        public const string Spaeher = "spy";
        public const string LeichteKavallerie = "light";
        public const string BeritteneBogenschuetzen = "marcher";
        public const string SchwereKavallerie = "heavy";
        public const string Rammboecke = "ram";
        public const string Katapulte = "cat";
        public const string Miliz = "miliz";
        public const string Paladin = "knight";
        public const string Adelsgeschlecht = "snob";
    }
}