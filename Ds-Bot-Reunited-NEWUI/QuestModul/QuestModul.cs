﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ds_Bot_Reunited_NEWUI.Accountsettings.Ds_Bot_Reunited.Accountsettings;
using Ds_Bot_Reunited_NEWUI.BauenModul;
using Ds_Bot_Reunited_NEWUI.Botcaptcha;
using Ds_Bot_Reunited_NEWUI.BrowserManager;
using Ds_Bot_Reunited_NEWUI.Constants;
using Ds_Bot_Reunited_NEWUI.Farmen;
using Ds_Bot_Reunited_NEWUI.ForschenModul;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.Settings;
using OpenQA.Selenium;

namespace Ds_Bot_Reunited_NEWUI.QuestModul {
    public class QuestModul {
        public static async Task<int> CheckIfQuestOpenAndCloseIt(Browser browser, int i = 0) {
            if (browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Quest.DAILYPRICE_SELECTOR_XPATH)).Count > 0) {
                if (browser.WebDriver.FindElements(By.XPath("//div[@class='chest unlocked']")).Count > 0) {
                    browser.ClickByXpath("//a[@class='btn btn-default' and @href='#']");
                }
            }
            if (browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Allgemein.QUESTFENSTER_SELECTOR_XPATH)).Count > 0) {
                try {
                    if (browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Quest.WARNUNG_SELECTOR_XPATH)).Count == 0 && browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Quest.ANNEHMEN_BUTTON_SELECTOR_XPATH)).Count > 0) {
                        browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Quest.ANNEHMEN_BUTTON_SELECTOR_XPATH)).Click();
                        browser.WebDriver.WaitForAjax();
                        await Rndm.Sleep(700, 900);
                        i -= 1;
                    } else if (browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Quest.UEBERSPRINGEN_BUTTON_SELECTOR_XPATH)).Count > 0) {
                        browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Quest.UEBERSPRINGEN_BUTTON_SELECTOR_XPATH)).Click();
                        browser.WebDriver.WaitForAjax();
                    }
                    if (browser.WebDriver.FindElements(By.XPath("//div[contains(@id,'popup_box_daily')]")).Count > 0) {
                        browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Quest.QUESTFENSTER_SCHLIESSEN_BUTTON_SELECTOR_XPATH)).Click();
                        browser.WebDriver.WaitForAjax();
                    }
                    await Rndm.Sleep(200, 400);
                } catch {
                    // ignored
                }
            }
            if (browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Quest.DAILYPRICE_SELECTOR_XPATH)).Count > 0) {
                browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Quest.QUESTFENSTER_SCHLIESSEN_BUTTON_SELECTOR_XPATH)).Click();
                browser.WebDriver.WaitForAjax();
                await Rndm.Sleep(200, 400);
            }
            return i;
        }

        public static async Task MakeQuests(Browser browser, Accountsetting accountsettings, AccountDorfSettings dorf) {
            if (accountsettings.QuestsActive) {
                if (browser.WebDriver.FindElements(By.XPath("//div[contains(@class,'finished')]")).Count > 0) {
                    foreach (var element in browser.WebDriver.FindElements(By.XPath("//div[contains(@class,'finished')]"))) {
                        try {
                            browser.ClickElement(element);
                            browser.WebDriver.WaitForAjax();
                            await CheckIfQuestOpenAndCloseIt(browser);
                            await Rndm.Sleep(200, 400);
                            await CheckIfQuestOpenAndCloseIt(browser);
                        } catch {
                            // ignored
                        }
                    }
                }

                if (browser.WebDriver.FindElements(By.XPath("//div[contains(@class,'quest')]")).Count > 0) {
                    List<IWebElement> elements = browser.WebDriver.FindElements(By.XPath("//div[contains(@id,'quest_')]")).ToList();

                    bool again = true;
                    int counter = 0;

                    while (again && counter < 3) {
                        counter++;
                        again = false;
                        for (int i = 0; i < elements.Count; i++) {
                            try {
                                string idstring = elements[i].GetAttribute("id").Replace("quest_", "");
                                int id = int.Parse(idstring);
                                AccountDorfSettings dorff = dorf.Clone();
                                dorff.FarmTruppenTemplate = new FarmenTruppenTemplate {SpeertraegerActive = true, SchwertkaempferActive = true, AxtkaempferActive = false, LeichteKavallerieActive = false, SpeertraegerAnzahlMitSchwertkaempfer = 10, SchwertkaempferAnzahlMitSpeertraeger = 10, SpeertraegerMaxLaufzeit = 300, SchwertkaempferMaxLaufzeit = 300};
                                switch (id) {
                                    case 1010:
                                        if (dorf.Dorfinformationen.GebaeudeStufen.Holzfaeller < 1) {
                                            BauenModul.BauenModul temp = new BauenModul.BauenModul();
                                            await temp.BuildVillage(browser, accountsettings, dorf, new Bauelement {Gebaeude = Gebaeude.Holzfaeller, TargetLevel = 1, Priority = 0});
                                        }
                                        break;
                                    case 1020:
                                        if (dorf.Dorfinformationen.GebaeudeStufen.Lehmgrube < 1) {
                                            BauenModul.BauenModul temp = new BauenModul.BauenModul();
                                            await temp.BuildVillage(browser, accountsettings, dorf, new Bauelement {Gebaeude = Gebaeude.Lehmgrube, TargetLevel = 1, Priority = 0});
                                        }
                                        if (dorf.Dorfinformationen.GebaeudeStufen.Eisenmine < 1) {
                                            BauenModul.BauenModul temp = new BauenModul.BauenModul();
                                            await temp.BuildVillage(browser, accountsettings, dorf, new Bauelement {Gebaeude = Gebaeude.Eisenmine, TargetLevel = 1, Priority = 0});
                                        }
                                        break;
                                    case 1030:
                                        if (dorf.Dorfinformationen.GebaeudeStufen.Holzfaeller < 2) {
                                            BauenModul.BauenModul temp = new BauenModul.BauenModul();
                                            await temp.BuildVillage(browser, accountsettings, dorf, new Bauelement {Gebaeude = Gebaeude.Holzfaeller, TargetLevel = 2, Priority = 0});
                                        }
                                        if (dorf.Dorfinformationen.GebaeudeStufen.Lehmgrube < 2) {
                                            BauenModul.BauenModul temp = new BauenModul.BauenModul();
                                            await temp.BuildVillage(browser, accountsettings, dorf, new Bauelement {Gebaeude = Gebaeude.Lehmgrube, TargetLevel = 2, Priority = 0});
                                        }
                                        break;
                                    case 1040:
                                        if (dorf.Dorfinformationen.GebaeudeStufen.Hauptgebaeude < 2) {
                                            BauenModul.BauenModul temp = new BauenModul.BauenModul();
                                            await temp.BuildVillage(browser, accountsettings, dorf, new Bauelement {Gebaeude = Gebaeude.Hauptgebaeude, TargetLevel = 2, Priority = 0});
                                        }
                                        break;
                                    case 1050:
                                        if (dorf.Dorfinformationen.GebaeudeStufen.Hauptgebaeude < 3) {
                                            BauenModul.BauenModul temp = new BauenModul.BauenModul();
                                            await temp.BuildVillage(browser, accountsettings, dorf, new Bauelement {Gebaeude = Gebaeude.Hauptgebaeude, TargetLevel = 3, Priority = 0});
                                        }
                                        if (dorf.Dorfinformationen.GebaeudeStufen.Kaserne < 1) {
                                            BauenModul.BauenModul temp = new BauenModul.BauenModul();
                                            await temp.BuildVillage(browser, accountsettings, dorf, new Bauelement {Gebaeude = Gebaeude.Kaserne, TargetLevel = 1, Priority = 0});
                                        }
                                        break;
                                    case 1060:
                                        if (dorf.Dorfinformationen.GebaeudeStufen.Holzfaeller < 3) {
                                            BauenModul.BauenModul temp = new BauenModul.BauenModul();
                                            await temp.BuildVillage(browser, accountsettings, dorf, new Bauelement {Gebaeude = Gebaeude.Holzfaeller, TargetLevel = 3, Priority = 0});
                                        }
                                        if (dorf.Dorfinformationen.GebaeudeStufen.Lehmgrube < 3) {
                                            BauenModul.BauenModul temp = new BauenModul.BauenModul();
                                            await temp.BuildVillage(browser, accountsettings, dorf, new Bauelement {Gebaeude = Gebaeude.Lehmgrube, TargetLevel = 3, Priority = 0});
                                        }
                                        if (dorf.Dorfinformationen.GebaeudeStufen.Kaserne < 3) {
                                            BauenModul.BauenModul temp = new BauenModul.BauenModul();
                                            await temp.BuildVillage(browser, accountsettings, dorf, new Bauelement {Gebaeude = Gebaeude.Kaserne, TargetLevel = 2, Priority = 0});
                                        }
                                        break;
                                    case 1070:
                                        if (dorf.Dorfinformationen.GebaeudeStufen.Speicher < 2) {
                                            BauenModul.BauenModul temp = new BauenModul.BauenModul();
                                            await temp.BuildVillage(browser, accountsettings, dorf, new Bauelement {Gebaeude = Gebaeude.Speicher, TargetLevel = 2, Priority = 0});
                                        }
                                        break;
                                    case 1090:
                                        if (dorf.Dorfinformationen.GebaeudeStufen.Eisenmine < 2) {
                                            BauenModul.BauenModul temp = new BauenModul.BauenModul();
                                            await temp.BuildVillage(browser, accountsettings, dorf, new Bauelement {Gebaeude = Gebaeude.Eisenmine, TargetLevel = 2, Priority = 0});
                                        }
                                        if (dorf.Dorfinformationen.GebaeudeStufen.Speicher < 3) {
                                            BauenModul.BauenModul temp = new BauenModul.BauenModul();
                                            await temp.BuildVillage(browser, accountsettings, dorf, new Bauelement {Gebaeude = Gebaeude.Speicher, TargetLevel = 3, Priority = 0});
                                        }
                                        break;
                                    case 1160:
                                        if (dorf.Dorfinformationen.GebaeudeStufen.Bauernhof < 2) {
                                            BauenModul.BauenModul temp = new BauenModul.BauenModul();
                                            await temp.BuildVillage(browser, accountsettings, dorf, new Bauelement {Gebaeude = Gebaeude.Bauernhof, TargetLevel = 2, Priority = 0});
                                        }
                                        if (dorf.Dorfinformationen.GebaeudeStufen.Eisenmine < 3) {
                                            BauenModul.BauenModul temp = new BauenModul.BauenModul();
                                            await temp.BuildVillage(browser, accountsettings, dorf, new Bauelement {Gebaeude = Gebaeude.Eisenmine, TargetLevel = 3, Priority = 0});
                                        }
                                        break;
                                    case 1110:
                                        if (dorf.Dorfinformationen.GebaeudeStufen.Hauptgebaeude < 5) {
                                            BauenModul.BauenModul temp = new BauenModul.BauenModul();
                                            await temp.BuildVillage(browser, accountsettings, dorf, new Bauelement {Gebaeude = Gebaeude.Hauptgebaeude, TargetLevel = 5, Priority = 0});
                                        }
                                        if (dorf.Dorfinformationen.GebaeudeStufen.Schmiede < 1) {
                                            BauenModul.BauenModul temp = new BauenModul.BauenModul();
                                            await temp.BuildVillage(browser, accountsettings, dorf, new Bauelement {Gebaeude = Gebaeude.Schmiede, TargetLevel = 1, Priority = 0});
                                        }
                                        break;
                                    case 1091:
                                        if (dorf.Dorfinformationen.GebaeudeStufen.Holzfaeller < 4) {
                                            BauenModul.BauenModul temp = new BauenModul.BauenModul();
                                            await temp.BuildVillage(browser, accountsettings, dorf, new Bauelement {Gebaeude = Gebaeude.Holzfaeller, TargetLevel = 4, Priority = 0});
                                        }
                                        if (dorf.Dorfinformationen.GebaeudeStufen.Lehmgrube < 4) {
                                            BauenModul.BauenModul temp = new BauenModul.BauenModul();
                                            await temp.BuildVillage(browser, accountsettings, dorf, new Bauelement {Gebaeude = Gebaeude.Lehmgrube, TargetLevel = 4, Priority = 0});
                                        }
                                        break;
                                    case 1240:
                                        if (dorf.Dorfinformationen.GebaeudeStufen.Wall < 1) {
                                            BauenModul.BauenModul temp = new BauenModul.BauenModul();
                                            await temp.BuildVillage(browser, accountsettings, dorf, new Bauelement {Gebaeude = Gebaeude.Wall, TargetLevel = 1, Priority = 0});
                                        }
                                        break;
                                    case 1880:
                                        if (dorf.Dorfinformationen.GebaeudeStufen.Versteck < 3) {
                                            BauenModul.BauenModul temp = new BauenModul.BauenModul();
                                            await temp.BuildVillage(browser, accountsettings, dorf, new Bauelement {Gebaeude = Gebaeude.Versteck, TargetLevel = 3, Priority = 0});
                                        }
                                        break;
                                    case 1250:
                                        if (dorf.Dorfinformationen.GebaeudeStufen.Holzfaeller < 5) {
                                            BauenModul.BauenModul temp = new BauenModul.BauenModul();
                                            await temp.BuildVillage(browser, accountsettings, dorf, new Bauelement {Gebaeude = Gebaeude.Holzfaeller, TargetLevel = 5, Priority = 0});
                                        }
                                        if (dorf.Dorfinformationen.GebaeudeStufen.Lehmgrube < 5) {
                                            BauenModul.BauenModul temp = new BauenModul.BauenModul();
                                            await temp.BuildVillage(browser, accountsettings, dorf, new Bauelement {Gebaeude = Gebaeude.Lehmgrube, TargetLevel = 5, Priority = 0});
                                        }
                                        break;
                                    case 1130:
                                        if (dorf.Dorfinformationen.GebaeudeStufen.Marktplatz < 1) {
                                            BauenModul.BauenModul temp = new BauenModul.BauenModul();
                                            await temp.BuildVillage(browser, accountsettings, dorf, new Bauelement {Gebaeude = Gebaeude.Marktplatz, TargetLevel = 1, Priority = 0});
                                        }
                                        break;


                                    case 1840:
                                        await FlaggenModul.FlaggenModul.SetRohstoffFlagge(browser, accountsettings, dorf);
                                        break;
                                    case 1820:
                                        //try {
                                        //    if (!accountsettings.MultiaccountingDaten.Quest1820Done) {
                                        //        await FarmModul.AttackNewVillagesOverMap(browser, accountsettings, dorff, 1, false, false, 100, true);
                                        //        string link = App.Settings.OperateLink + "village=" + dorf.Dorf.VillageId + "" + accountsettings.UvZusatzFuerLink + "&screen=place";
                                        //        browser.WebDriver.Navigate().GoToUrl(link);
                                        //        await Rndm.Sleep(500, 1000);
                                        //        await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings, link);
                                        //        i = await CheckIfQuestOpenAndCloseIt(browser);
                                        //        browser.WebDriver.WaitForAjax();
                                        //        browser.ClickByXpath("//img[contains(@src,'graphic/delete.png')]");
                                        //        await Rndm.Sleep();
                                        //        again = true;
                                        //        accountsettings.MultiaccountingDaten.Quest1820Done = true;
                                        //        //
                                        //    }
                                        //    //
                                        //} catch (Exception e) {
                                        //    App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + " Failure Quest 1820 " + e);
                                        //}
                                        //Truppen farmen und abbrechen
                                        break;
                                    case 1821:
                                        //try {
                                        //    if (!accountsettings.MultiaccountingDaten.Quest1821Done) {
                                        //        await FarmModul.AttackNewVillagesOverMap(browser, accountsettings, dorff, 1, false, false, 100, true);
                                        //        string link = App.Settings.OperateLink + "village=" + dorf.Dorf.VillageId + "" + accountsettings.UvZusatzFuerLink + "&screen=place";
                                        //        browser.WebDriver.Navigate().GoToUrl(link);
                                        //        await Rndm.Sleep(500, 1000);
                                        //        await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings, link);
                                        //        i = await CheckIfQuestOpenAndCloseIt(browser);
                                        //        browser.WebDriver.WaitForAjax();
                                        //        browser.ClickByXpath("//img[contains(@src,'graphic/delete.png')]");
                                        //        await Rndm.Sleep();
                                        //        again = true;
                                        //        accountsettings.MultiaccountingDaten.Quest1821Done = true;
                                        //    }
                                        //} catch {
                                        //    App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + " Failure Quest 1821 ");
                                        //}
                                        break;
                                    case 1822:
                                        //try {
                                        //    if (!accountsettings.MultiaccountingDaten.Quest1822Done) {
                                        //        await FarmModul.AttackNewVillagesOverMap(browser, accountsettings, dorff, 2, false, false, 100, true);
                                        //        await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings);
                                        //        await Rndm.Sleep(300);
                                        //        accountsettings.MultiaccountingDaten.Quest1822Done = true;
                                        //    }
                                        //} catch {
                                        //    App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + " Failure Quest 1822 ");
                                        //}
                                        break;
                                    case 1140:
                                        await RekrutierenModul.RekrutierenModul.RekrutBarracks(new TruppenTemplate {SpeertraegerAnzahl = 21}, browser, dorf, 50, 30, 10, accountsettings, new TimeSpan());
                                        again = true;
                                        break;
                                    case 1150:
                                        if (dorf.Dorfinformationen.GebaeudeStufen.Kaserne < 3) {
                                            BauenModul.BauenModul temp = new BauenModul.BauenModul();
                                            await temp.BuildVillage(browser, accountsettings, dorf, new Bauelement {Gebaeude = Gebaeude.Kaserne, TargetLevel = 3, Priority = 0});
                                        }
                                        await RekrutierenModul.RekrutierenModul.RekrutBarracks(new TruppenTemplate {SpeertraegerAnzahl = 22}, browser, dorf, 50, 30, 10, accountsettings, new TimeSpan());
                                        again = true;
                                        break;
                                    case 1920:
                                        if (!accountsettings.MultiaccountingDaten.Quest1920Done) {
                                            await RekrutierenModul.RekrutierenModul.RekrutPaladin(browser, dorf, (!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname), accountsettings);
                                            accountsettings.MultiaccountingDaten.Quest1920Done = true;
                                        }
                                        break;
                                    case 1120:
                                        if (elements.Count(x => int.Parse(x.GetAttribute("id").Replace("quest_", "")).Equals(1091)) == 0) {
                                            if (accountsettings.MultiaccountingDaten.Quest1120Done < 50 && dorf.Dorfinformationen.GebaeudeStufen.Holzfaeller > 10) {
                                                accountsettings.MultiaccountingDaten.Quest1120Done = dorf.Dorfinformationen.Truppen.SpeertraegerAnzahl + await RekrutierenModul.RekrutierenModul.RekrutBarracks(new TruppenTemplate {SpeertraegerAnzahl = 40}, browser, dorf, 50, 30, 10, accountsettings, new TimeSpan());
                                            }
                                        }
                                        break;
                                    case 2010:
                                        if (accountsettings.MultiaccountingDaten.Quest2010Done < 50 && dorf.Dorf.VillagePoints > 400) {
                                            accountsettings.MultiaccountingDaten.Quest2010Done = dorf.Dorfinformationen.Truppen.SchwertkaempferAnzahl + await RekrutierenModul.RekrutierenModul.RekrutBarracks(new TruppenTemplate {SchwertkaempferAnzahl = 50}, browser, dorf, 50, 30, 10, accountsettings, new TimeSpan());
                                        }
                                        break;
                                    case 1900:
                                        //try {
                                        //    if (!accountsettings.MultiaccountingDaten.Quest1900Done) {
                                        //        string link = App.Settings.OperateLink + "village=" + dorf.Dorf.VillageId + "" + accountsettings.UvZusatzFuerLink + "&screen=am_farm";
                                        //        browser.WebDriver.Navigate().GoToUrl(link);
                                        //        await Rndm.Sleep(300);
                                        //        await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings, link);
                                        //        await FarmassistentModul.SetTemplate(browser, "a", new TruppenTemplate {SpeertraegerAnzahl = 2, SchwertkaempferAnzahl = 2});
                                        //        await Rndm.Sleep(300);
                                        //        await FarmModul.AttackNewVillagesOverMap(browser, accountsettings, dorff, 1, false, true, 100, true);
                                        //        accountsettings.MultiaccountingDaten.Quest1900Done = true;
                                        //    }
                                        //} catch {
                                        //    App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + " Failure Quest 1900 ");
                                        //}
                                        break;
                                    case 1910:
                                        //try {
                                        //    if (accountsettings.MultiaccountingDaten.Quest1910Done < 5) {
                                        //        accountsettings.MultiaccountingDaten.Quest1910Done += await FarmModul.AttackNewVillagesOverMap(browser, accountsettings, dorff, 5 - accountsettings.MultiaccountingDaten.Quest1910Done, false, true, 100, true);
                                        //    }
                                        //} catch {
                                        //    App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + " Failure Quest 1910 ");
                                        //}
                                        break;
                                    case 4010:
                                        if (dorf.Dorf.VillagePoints > 400)
                                            await RekrutierenModul.RekrutierenModul.RekrutBarracks(new TruppenTemplate {SpeertraegerAnzahl = 50}, browser, dorf, 50, 30, 10, accountsettings, new TimeSpan());
                                        break;
                                    case 1660:
                                        try {
                                            string link = App.Settings.OperateLink + "village=" + dorf.Dorf.VillageId + "" + accountsettings.UvZusatzFuerLink + "&screen=forum";
                                            browser.WebDriver.Navigate().GoToUrl(link);
                                            await Rndm.Sleep(300);
                                            await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings, link);
                                            again = true;
                                        } catch {
                                            App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + " Failure Quest 1660 ");
                                        }
                                        break;
                                    case 48:
                                        try {
                                            string link = App.Settings.OperateLink + "village=" + dorf.Dorf.VillageId + "" + accountsettings.UvZusatzFuerLink + "&screen=mentor";
                                            browser.WebDriver.Navigate().GoToUrl(link);
                                            await Rndm.Sleep(300);
                                            await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings, link);
                                            again = true;
                                        } catch {
                                            App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + " Failure Quest 48 ");
                                        }
                                        break;
                                    case 47:
                                        try {
                                            string link = App.Settings.OperateLink + "village=" + dorf.Dorf.VillageId + "" + accountsettings.UvZusatzFuerLink + "&screen=mentor";
                                            browser.WebDriver.Navigate().GoToUrl(link);
                                            await Rndm.Sleep(300);
                                            await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings, link);
                                            again = true;
                                        } catch {
                                            App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + " Failure Quest 47 ");
                                        }
                                        break;
                                    case 1610:
                                        try {
                                            string link = App.Settings.OperateLink + "village=" + dorf.Dorf.VillageId + "" + accountsettings.UvZusatzFuerLink + "&screen=main";
                                            browser.WebDriver.Navigate().GoToUrl(link);
                                            await Rndm.Sleep(50, 100);
                                            browser.ClickByXpath(SpielOberflaeche.Hauptgebaeude.DORFUMBENENNENBUTTON_SELECTOR_XPATH);
                                            await Rndm.Sleep(300);
                                            await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings);
                                            again = true;
                                        } catch {
                                            App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + " Failure Quest 1610 ");
                                        }
                                        break;
                                    case 1620:
                                        try {
                                            browser.WebDriver.Navigate().GoToUrl(App.Settings.OperateLink + "village=" + dorf.Dorf.VillageId + "" + accountsettings.UvZusatzFuerLink + "&screen=info_player");
                                            await Rndm.Sleep(200);
                                            browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.EigenesProfil.PROFILBEARBEITEN_BUTTON_SELECTOR_XPATH)).Click();
                                            await Rndm.Sleep(800, 1000);
                                            //browser.WebDriver.FindElements(
                                            //    By.XPath(SpielOberflaeche.EigenesProfil.AVATARE_SELECTOR_XPATH))[
                                            //    App.Random.Next(0, 8)].Click();
                                            //await Rndm.Sleep(800, 1000);

                                            browser.ClickByXpath(SpielOberflaeche.EigenesProfil.PROFILTEXT_BESTAETIGEN_SELECTOR_XPATH);
                                            await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings);
                                            browser.WebDriver.Navigate().GoToUrl(App.Settings.OperateLink + "village=" + dorf.Dorf.VillageId + "" + accountsettings.UvZusatzFuerLink + "&screen=info_player&mode=awards");
                                            await Rndm.Sleep(200);
                                            if (browser.WebDriver.FindElements(By.XPath("//tr[@class='individual_awards']"))[0].Displayed) {
                                                await CheckIfQuestOpenAndCloseIt(browser, i);
                                                browser.WebDriver.FindElement(By.XPath("//input[@name='show_awards']")).Click();
                                                await Rndm.Sleep(200);
                                                browser.WebDriver.FindElement(By.XPath("//input[@value='Speichern']")).Click();
                                                await Rndm.Sleep(200);
                                            }
                                            again = true;
                                        } catch {
                                            App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + " Failure Quest 1620 ");
                                        }
                                        break;
                                    case 1630:
                                        try {
                                            browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Allgemein.RANGLISTE_BUTTON_SELECTOR_XPATH)).Click();
                                            await Rndm.Sleep(200, 400);
                                            //player_ranking_table
                                            browser.WebDriver.FindElements(By.XPath("//img[contains(@src,'coa_unknown.png')]"))[0].Click();
                                            await Rndm.Sleep(200, 400);
                                            browser.WebDriver.FindElements(By.XPath("//a[contains(@href,'screen=mail&mode=new')]"))[1].Click();
                                            await Rndm.Sleep(200, 400);
                                            browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Nachrichten.Nachrichtschreiben.BETREFF_TEXTBOX_SELECTOR_XPATH)).SendKeys(WordFinder.Find(App.Random.Next(3, 20)));
                                            browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Nachrichten.Nachrichtschreiben.NACHRICHT_TEXTBOX_SELECTOR_XPATH)).SendKeys(WordFinder.Find(App.Random.Next(3, 120)));
                                            browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Nachrichten.Nachrichtschreiben.ABSENDEN_BUTTON_SELECTOR_XPATH)).Click();
                                            again = true;
                                        } catch {
                                            App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + " Failure Quest 1630 ");
                                        }
                                        break;
                                    case 1850:
                                        //try {
                                        //    if (!accountsettings.MultiaccountingDaten.Quest1850Done) {
                                        //        await FarmModul.AttackNewVillagesOverMap(browser, accountsettings, dorff, 0, true);
                                        //        await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings);
                                        //        await Rndm.Sleep(300);
                                        //        accountsettings.MultiaccountingDaten.Quest1850Done = true;
                                        //    }
                                        //} catch (Exception e) {
                                        //    App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + " Failure Quest 1850 " + e.Message);
                                        //}
                                        break;
                                    case 1220:
                                        try {
                                            if (!accountsettings.MultiaccountingDaten.Quest1220Done) {
                                                string link = App.Settings.OperateLink + "village=" + dorf.Dorf.VillageId + "" + accountsettings.UvZusatzFuerLink + "&screen=market&mode=own_offer";
                                                browser.WebDriver.Navigate().GoToUrl(link);
                                                await Rndm.Sleep(200);
                                                await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings, link);
                                                browser.WebDriver.FindElement(By.XPath("//input[@class='btn']")).Click();
                                                accountsettings.MultiaccountingDaten.Quest1220Done = true;
                                                await Rndm.Sleep(200);
                                            }
                                            again = true;
                                        } catch {
                                            App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + " Failure Quest 1850 ");
                                        }
                                        break;
                                    case 1860:
                                        try {
                                            await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings);
                                            browser.WebDriver.Navigate().GoToUrl(App.Settings.OperateLink + "village=" + dorf.Dorf.VillageId + "" + accountsettings.UvZusatzFuerLink + "&screen=farm");
                                            await Rndm.Sleep(200);
                                            ((IJavaScriptExecutor) browser.WebDriver).ExecuteScript("$('#chat-wrapper').hide();");
                                            browser.WebDriver.FindElement(By.XPath("//a[contains(@href,'action=establish_militia')]")).Click();
                                            await Rndm.Sleep(200);
                                            again = true;
                                        } catch {
                                            App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + " Failure Quest 1860 ");
                                        }
                                        break;
                                    case 2030:
                                        //try
                                        //{
                                        //    if (dorf.Dorfinformationen.GebaeudeStufen.Hauptgebaeude < 7)
                                        //    {
                                        //        BauenModul.BauenModul temp = new BauenModul.BauenModul();
                                        //        await temp.BuildVillage(browser, accountsettings, dorf, new Bauelement
                                        //        {
                                        //            Gebaeude = Gebaeude.Hauptgebaeude,
                                        //            TargetLevel = 7,
                                        //            Priority = 0
                                        //        });
                                        //    }
                                        //    if (dorf.Dorfinformationen.GebaeudeStufen.Kaserne < 5)
                                        //    {
                                        //        BauenModul.BauenModul temp = new BauenModul.BauenModul();
                                        //        await temp.BuildVillage(browser, accountsettings, dorf, new Bauelement
                                        //        {
                                        //            Gebaeude = Gebaeude.Kaserne,
                                        //            TargetLevel =5,
                                        //            Priority = 0
                                        //        });
                                        //    }
                                        //}
                                        //catch (Exception e) {
                                        //    App.LogQueue.Enqueue(
                                        //        DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss.fff",
                                        //            CultureInfo.InvariantCulture) +
                                        //        " Failure Quest 2030 ");
                                        //}
                                        break;
                                    case 4030:
                                        try {
                                            await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings);
                                            if (dorf.Dorfinformationen.GebaeudeStufen.Hauptgebaeude < 10) {
                                                BauenModul.BauenModul temp = new BauenModul.BauenModul();
                                                await temp.BuildVillage(browser, accountsettings, dorf, new Bauelement {Gebaeude = Gebaeude.Hauptgebaeude, TargetLevel = 10, Priority = 0});
                                            }
                                            if (dorf.Dorfinformationen.GebaeudeStufen.Schmiede < 5) {
                                                BauenModul.BauenModul temp = new BauenModul.BauenModul();
                                                await temp.BuildVillage(browser, accountsettings, dorf, new Bauelement {Gebaeude = Gebaeude.Schmiede, TargetLevel = 5, Priority = 0});
                                            }
                                            if (dorf.Dorfinformationen.GebaeudeStufen.Hauptgebaeude >= 10 && dorf.Dorfinformationen.GebaeudeStufen.Schmiede >= 5 && dorf.Dorfinformationen.GebaeudeStufen.Stall < 1) {
                                                BauenModul.BauenModul temp = new BauenModul.BauenModul();
                                                await temp.BuildVillage(browser, accountsettings, dorf, new Bauelement {Gebaeude = Gebaeude.Stall, TargetLevel = 1, Priority = 0});
                                            }
                                            if (dorf.Dorfinformationen.GebaeudeStufen.Hauptgebaeude >= 10 && dorf.Dorfinformationen.GebaeudeStufen.Kaserne >= 5 && dorf.Dorfinformationen.GebaeudeStufen.Schmiede >= 5 && dorf.Dorfinformationen.GebaeudeStufen.Stall >= 1 && dorf.Dorfinformationen.Speicher.Holz >= 2200 && dorf.Dorfinformationen.Speicher.Lehm >= 2400 && dorf.Dorfinformationen.Speicher.Eisen >= 2000) {
                                                await ForschenModul.ForschenModul.Forge(browser, dorff, Forschen.Spaeher, accountsettings);
                                            }
                                            again = true;
                                        } catch {
                                            App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + " Failure Quest 4030 ");
                                        }
                                        break;
                                }
                            } catch {
                                // ignored
                            }
                            await Rndm.Sleep(500, 1000);
                            i = await CheckIfQuestOpenAndCloseIt(browser, i);

                            if (await CheckIfQuestOpenAndCloseIt(browser, i) < i) {
                                browser.WebDriver.Navigate().Refresh();
                                await Rndm.Sleep(500, 1000);
                            }
                            elements = browser.WebDriver.FindElements(By.XPath("//div[contains(@id,'quest_')]")).ToList();
                        }
                    }
                }
                //TODO Andere Quests machen!
            }
        }
    }
}
