﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Ds_Bot_Reunited_NEWUI.Accountsettings.Ds_Bot_Reunited.Accountsettings;
using Ds_Bot_Reunited_NEWUI.BrowserManager;
using Ds_Bot_Reunited_NEWUI.Constants;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.Rekrutieren;
using OpenQA.Selenium;

namespace Ds_Bot_Reunited_NEWUI.RaubzugModul {
    public class RaubzugModul {
        public static async Task StartRaubzug(Browser browser, Accountsetting accountsettings, int playerpoints) {
            await App.WaitForAttacks();
            App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + "Raubzug started");
            foreach (var dorf in accountsettings.DoerferSettings.Where(x => !string.IsNullOrEmpty(x.Dorf.Dorf.VillageName) && x.Dorf.PlayerId.Equals(accountsettings.Playerid))) {
                var raubzuginterval = dorf.RaubzugInterval;
                var now = DateTime.Now;
                if (dorf.Dorfinformationen.LastRaubzugZeitpunkt < now.AddMinutes(-raubzuginterval)) {
                    await App.WaitForAttacks();
                    var link = App.Settings.OperateLink + "village=" + dorf.Dorf.VillageId + "" + accountsettings.UvZusatzFuerLink + "&screen=place&mode=scavenge";
                    browser.WebDriver.Navigate().GoToUrl(link);
                    browser.WebDriver.WaitForPageload();

                    if (!browser.WebDriver.ElementExists(By.XPath(SpielOberflaeche.Raubzug.STARTBUTTON_SELECTOR_XPATH))) {
                        browser.ClickByXpath(SpielOberflaeche.Raubzug.FREISCHALTENBUTTON_SELECTOR_XPATH);
                        browser.WebDriver.WaitForPageload();
                    }

                    int.TryParse(GetMaxTruppen(browser, Truppe.Speertraeger.GetDescription()), out int speertraeger);
                    int.TryParse(GetMaxTruppen(browser, Truppe.Schwertkaempfer.GetDescription()), out int schwertkaempfer);
                    int.TryParse(GetMaxTruppen(browser, Truppe.Axtkaempfer.GetDescription()), out int axtkaempfer);
                    int.TryParse(GetMaxTruppen(browser, Truppe.Bogenschuetzen.GetDescription()), out int bogenschuetzen);
                    int.TryParse(GetMaxTruppen(browser, Truppe.LeichteKavallerie.GetDescription()), out int leichtekavallerie);
                    int.TryParse(GetMaxTruppen(browser, Truppe.BeritteneBogenschuetzen.GetDescription()), out int berittenebogenschuetzen);
                    int.TryParse(GetMaxTruppen(browser, Truppe.SchwereKavallerie.GetDescription()), out int schwerekavallerie);
                    int.TryParse(GetMaxTruppen(browser, Truppe.Paladin.GetDescription()), out int paladin);

                    bool schicken = false;

                    if (speertraeger > 0) {
                        schicken = true;
                        browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Raubzug.SPEERTRAEGER_SELECTOR_XPATH)).SendKeys(speertraeger.ToString());
                    }
                    if (schwertkaempfer > 0) {
                        schicken = true;
                        browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Raubzug.SCHWERTKAEMPFER_SELECTOR_XPATH)).SendKeys(schwertkaempfer.ToString());
                    }
                    if (axtkaempfer > 0) {
                        schicken = true;
                        browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Raubzug.AXTKAEMPFER_SELECTOR_XPATH)).SendKeys(axtkaempfer.ToString());
                    }
                    if (bogenschuetzen > 0) {
                        schicken = true;
                        browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Raubzug.BOGENSCHUETZEN_SELECTOR_XPATH)).SendKeys(bogenschuetzen.ToString());
                    }
                    if (leichtekavallerie > 0) {
                        schicken = true;
                        browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Raubzug.LEICHTEKAVALLERIE_SELECTOR_XPATH)).SendKeys(leichtekavallerie.ToString());
                    }
                    if (berittenebogenschuetzen > 0) {
                        schicken = true;
                        browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Raubzug.BERITTENEBOGENSCHUETZEN_SELECTOR_XPATH)).SendKeys(berittenebogenschuetzen.ToString());
                    }
                    if (schwerekavallerie > 0) {
                        schicken = true;
                        browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Raubzug.SCHWEREKAVALLERIE_SELECTOR_XPATH)).SendKeys(schwerekavallerie.ToString());
                    }
                    if (paladin > 0) {
                        schicken = true;
                        browser.WebDriver.FindElement(By.XPath(SpielOberflaeche.Raubzug.PALADIN_SELECTOR_XPATH)).SendKeys(paladin.ToString());
                    }

                    if (schicken) {
                        browser.ClickByXpath(SpielOberflaeche.Raubzug.STARTBUTTON_SELECTOR_XPATH);
                        dorf.Dorfinformationen.LastRaubzugZeitpunkt = DateTime.Now;
                    }
                }
            }
            accountsettings.LastRaubzugZeitpunkt = DateTime.Now;
        }

        private static string GetMaxTruppen(Browser browser, string truppe) {
            return browser.WebDriver.FindElement(By.XPath("//a[@data-unit='" + truppe + "']"))?.Text.Replace("(","").Replace(")","");
        }
    }
}