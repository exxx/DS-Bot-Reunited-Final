﻿using System;
using System.ComponentModel;

namespace Ds_Bot_Reunited_NEWUI.ActionModul {
    [Serializable]
    public enum ActionArten {
        [Description("Farming")] Farming,
        [Description("Building")] Building,
        [Description("Recruiting")] Recruiting,
        [Description("Rausstellen")] Rausstellen,
        [Description("Trading")] Trading,
        [Description("Defend")] Defend,
        [Description("Coins")]Coins,
        [Description("Forge")] Forge
    }
}