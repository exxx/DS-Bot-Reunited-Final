﻿using System;

namespace Ds_Bot_Reunited_NEWUI.ActionModul {
    [Serializable]
    public class ActionModul {
        public string ActionModulNr { get; set; }
        public string Accountname { get; set; }
        public string VillageId { get; set; }
        public ActionArten AktionArt { get; set; }
        public DateTime Ausfuehrungszeitpunkt { get; set; }
        public float Priorityy { get; set; }

        public ActionModul() { }

        public ActionModul(ActionArten aktionArt, string accountname, DateTime ausfuehrungszeitpunkt, string villageid) {
            ActionModulNr = Guid.NewGuid().ToString();
            Accountname = accountname;
            AktionArt = aktionArt;
            Ausfuehrungszeitpunkt = ausfuehrungszeitpunkt;
            VillageId = villageid;
        }

        public ActionModul Clone() {
            ActionModul newActionModul = new ActionModul {
                Accountname = Accountname,
                ActionModulNr = ActionModulNr,
                AktionArt = AktionArt,
                Ausfuehrungszeitpunkt = Ausfuehrungszeitpunkt,
                Priorityy = Priorityy,
                VillageId = VillageId
            };
            return newActionModul;
        }
    }
}