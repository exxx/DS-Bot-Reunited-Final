﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.CompilerServices;
using Ds_Bot_Reunited_NEWUI.Annotations;
using Ds_Bot_Reunited_NEWUI.Properties;

namespace Ds_Bot_Reunited_NEWUI.ActionModul {
    [Serializable]
    public class ActionModulViewModel : INotifyPropertyChanged {
        private float _priority;
        public ActionModul ActionModul { get; }

        public string Accountname => ActionModul.Accountname;

        public string AktionArt {
            get {
                switch (ActionModul.AktionArt) {
                    case ActionArten.Farming:
                        return Resources.Farmen;
                    case ActionArten.Building:
                        return Resources.Bauen;
                    case ActionArten.Trading:
                        return Resources.PremiumDepot;
                    case ActionArten.Rausstellen:
                        return Resources.Rausstellen;
                    case ActionArten.Recruiting:
                        return Resources.Rekrutieren;
                    case ActionArten.Defend:
                        return Resources.Defending;
                    case ActionArten.Forge:
                        return Resources.Forschen;
                    default:
                        return "";
                }
            }
        }

        public string VillageId => ActionModul.VillageId;

        public float Priority {
            get => _priority;
            set {
                _priority = value;
                OnPropertyChanged();
            }
        }

        public DateTime Ausfuehrungszeitpunkt {
            get => ActionModul.Ausfuehrungszeitpunkt;
            set {
                ActionModul.Ausfuehrungszeitpunkt = value;
                OnPropertyChanged();
            }
        }

        public string Zeitpunkt => ActionModul.Ausfuehrungszeitpunkt.ToString("dd.MM.yyyy HH:mm:ss.fff",
            CultureInfo.InvariantCulture);

        public ActionModulViewModel(ActionModul actionModul) {
            ActionModul = actionModul;
        }


        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}