﻿using System.Windows;
using System.Windows.Controls;

namespace Ds_Bot_Reunited_NEWUI.Botmessages
{
    /// <summary>
    /// Interaktionslogik für BotmessagesView.xaml
    /// </summary>
    public partial class BotmessagesView
    {
        public BotmessagesView(Window owner)
        {
            Owner = owner;
            DataContext = BotmessagesViewModel.Create(Owner);
            InitializeComponent();
        }
        private void Save(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        void MyGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (((BotmessagesViewModel)DataContext)?.SelectedBotmessages != null)
            {
                ((BotmessagesViewModel)DataContext).SelectedBotmessages.Clear();
                foreach (var item in DataGrid.SelectedItems)
                {
                    ((BotmessagesViewModel)DataContext).SelectedBotmessages.Add((Botmessage)item);
                }
            }
        }
    }
}
