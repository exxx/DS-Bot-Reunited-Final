﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Input;
using Ds_Bot_Reunited_NEWUI.Annotations;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;

namespace Ds_Bot_Reunited_NEWUI.Botmessages {
    public class BotmessagesViewModel : INotifyPropertyChanged {
        private ICommand _removeCommand;
        private Botmessage _selectedMessage;
        private static BotmessagesViewModel StaticViewModel { get; set; }
        public System.Windows.Window Window { get; set; }

        public Botmessage SelectedMessage {
            get => _selectedMessage;
            set {
                _selectedMessage = value;
                OnPropertyChanged();
            }
        }

        public AsyncObservableCollection<Botmessage> Botmessages => App.Botmessages;
        public AsyncObservableCollection<Botmessage> SelectedBotmessages { get; set; }

        public ICommand RemoveCommand
            => _removeCommand ?? (_removeCommand = new RelayCommand(async () => { await Remove(); }));

        private BotmessagesViewModel(System.Windows.Window window) {
            Window = window;
            SelectedBotmessages = new AsyncObservableCollection<Botmessage>();
        }

        private async Task Remove() {
            await Task.Run(() => {
                    foreach (var botmessage in SelectedBotmessages) {
                        App.Botmessages.Remove(botmessage);
                    }
            });
        }


        public static BotmessagesViewModel Create(System.Windows.Window window) {
            if (StaticViewModel == null)
                StaticViewModel = new BotmessagesViewModel(window);
            return StaticViewModel;
        }


        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}