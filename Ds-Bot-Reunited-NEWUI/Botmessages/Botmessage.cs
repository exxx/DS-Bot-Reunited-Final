﻿using System;

namespace Ds_Bot_Reunited_NEWUI.Botmessages {
    [Serializable]
    public class Botmessage {
        public string Text { get; set; }
        public BotmessageArt Art { get; set; }
        public string Subject { get; set; }
        public string AktionenDanach { get; set; }
        public string Notiz { get; set; }
        public DateTime Zeit { get; set; }
        public bool Unread { get; set; } = true;

        public Botmessage Clone() {
            Botmessage newBotmessage = new Botmessage {
                Text = Text,
                Art = Art,
                Subject = Subject,
                AktionenDanach = AktionenDanach,
                Notiz = Notiz,
                Zeit = Zeit,
                Unread = Unread
            };
            return newBotmessage;
        }
    }
}