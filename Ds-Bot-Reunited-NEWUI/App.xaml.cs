﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using System.Windows.Threading;
using Ds_Bot_Reunited_NEWUI.ActionModul;
using Ds_Bot_Reunited_NEWUI.BerichtModul;
using Ds_Bot_Reunited_NEWUI.Botmessages;
using Ds_Bot_Reunited_NEWUI.Farmmodul;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.Settings;
using Ds_Bot_Reunited_NEWUI.SpieldatenViewModel;
using Binding = System.Windows.Data.Binding;
using ButtonBase = System.Windows.Controls.Primitives.ButtonBase;
using ListView = System.Windows.Controls.ListView;

namespace Ds_Bot_Reunited_NEWUI {
	[Obfuscation(Exclude = true, Feature = "renaming")]
	public partial class App {
		public static DependencyProperty GridViewSortPropertyNameProperty =
			DependencyProperty.RegisterAttached("GridViewSortPropertyName", typeof(string), typeof(App)
			                                    , new UIPropertyMetadata(null));

		public static DependencyProperty CurrentSortColumnProperty =
			DependencyProperty.RegisterAttached("CurrentSortColumn", typeof(GridViewColumn), typeof(App)
			                                    , new UIPropertyMetadata(null, CurrentSortColumnChanged));

		public static DependencyProperty EnableGridViewSortProperty =
			DependencyProperty.RegisterAttached("EnableGridViewSort", typeof(bool), typeof(App)
			                                    , new UIPropertyMetadata(false, EnableGridViewSortChanged));

		public App() {
			Random = new CryptoRandom();
			Permissions = new Permissions();
			DispatcherUnhandledException += OnDispatcherUnhandledException;
		}

		private static DateTime _lastWrite = DateTime.Now;
		private static readonly StringBuilder _builder = new StringBuilder();

		private static DateTime _lastFailureLog = DateTime.MinValue;

		public static void LogString(string log) {
			_builder.Clear();
			_builder.Append("\n");
			_builder.Append(DateTime.Now.GetString());
			_builder.Append(" ");
            _builder.Append(log);
			Log.Add(_builder.ToString());
			if (_lastWrite.AddMinutes(20) < DateTime.Now || Log.Count > 3000) {
				File.AppendAllLines("log.txt", Log);
				_lastWrite = DateTime.Now;
				Log.Clear();
			}

			if (log.Contains("Failure") || log.Contains("ERROR") && _lastFailureLog < DateTime.Now.AddMinutes(-1)) {
				_builder.Clear();
				foreach (var eintrag in Log.OrderByDescending(x => x)) {
					_builder.Append(eintrag);
				}
                File.AppendAllLines("failurelog.txt", new List<string> { _builder .ToString()});
				_lastFailureLog = DateTime.Now;
			}
		}

		public static bool Developer { get; set; }
		public static object PlayerlistLock { get; set; } = new object();
		public static object FarmlisteLock { get; set; } = new object();
		public static object BerichteLock { get; set; } = new object();
		public static object BotmessagesLock { get; set; } = new object();
		public static AsyncObservableCollection<SpielerViewModel> PlayerlistWholeWorld { get; set; }
		public static ConcurrentDictionary<VillageTuple<int, int>, DorfViewModel> VillagelistWholeWorld { get; set; }
		public static Settings.Settings Settings { get; set; }
		public static AsyncObservableCollection<Bericht> Berichte { get; set; }
		public static AsyncObservableCollection<FarmtargetViewModel> Farmliste { get; set; } = new AsyncObservableCollection<FarmtargetViewModel>();
		public static AsyncObservableCollection<ActionModulViewModel> PriorityQueue { get; set; } = new AsyncObservableCollection<ActionModulViewModel>();
		public static CryptoRandom Random { get; set; }
		public static bool Active { get; set; } = false;
		public static List<string> Log { get; set; } = new List<string>();
		public static bool SaveIsGoingOn { get; set; }
		public static Window Window { get; set; }
		public static NotifyIcon NotificationIcon { get; set; }
		public static AsyncObservableCollection<Botmessage> Botmessages { get; set; }
		public static DateTime LastWorlddataCheck { get; set; }
		public static Permissions Permissions { get; set; }
		public static bool PauseEinzelaccounting { get; set; }

		private void OnDispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e) {
			var window = new ExceptionForm(e.Exception, "xx");
			window.ShowDialog();
			e.Handled = true;
			Shutdown();
		}

		public static async Task WaitForAttacks() {
			while (!Active || PauseEinzelaccounting || Settings
			                                           .Accountsettingslist.First().GeplanteBewegungen
			                                           .Any(y => y.Abschickzeitpunkt.AddSeconds(2)
			                                                      .AddMilliseconds(Settings.Difference) <
			                                                     DateTime.Now &&
			                                                     y.Abschickzeitpunkt.AddSeconds(-2)
			                                                      .AddMilliseconds(Settings.Difference) >
			                                                     DateTime.Now && y.GenauTimenIsActive && !y.Sent &&
			                                                     y.Active && !y.Sent &&
			                                                     y.Queued && !y.GetsQueued))
				await Task.Delay(1000);
		}

		public static string GetGridViewSortPropertyName(GridViewColumn gvc) {
			return (string) gvc.GetValue(GridViewSortPropertyNameProperty);
		}

		public static void SetGridViewSortPropertyName(GridViewColumn gvc, string n) {
			gvc.SetValue(GridViewSortPropertyNameProperty, n);
		}

		public static GridViewColumn GetCurrentSortColumn(GridView gv) {
			return (GridViewColumn) gv.GetValue(CurrentSortColumnProperty);
		}

		public static void SetCurrentSortColumn(GridView gv, GridViewColumn value) {
			gv.SetValue(CurrentSortColumnProperty, value);
		}

		public static void CurrentSortColumnChanged(object sender, DependencyPropertyChangedEventArgs e) { }

		public static bool GetEnableGridViewSort(ListView lv) { return (bool) lv.GetValue(EnableGridViewSortProperty); }

		public static void SetEnableGridViewSort(ListView lv, bool value) {
			lv.SetValue(EnableGridViewSortProperty, value);
		}

		public static void EnableGridViewSortChanged(object sender, DependencyPropertyChangedEventArgs e) {
			var lv = sender as ListView;
			if (lv == null) return;

			if (!(e.NewValue is bool)) return;
			var enableGridViewSort = (bool) e.NewValue;

			if (enableGridViewSort) {
				lv.AddHandler(ButtonBase.ClickEvent, new RoutedEventHandler(EnableGridViewSortGvhClicked));
				if (lv.View == null)
					lv.Loaded += EnableGridViewSortLvLoaded;
				else
					EnableGridViewSortLvInitialize(lv);
			} else
				lv.RemoveHandler(ButtonBase.ClickEvent, new RoutedEventHandler(EnableGridViewSortGvhClicked));
		}

		public static void EnableGridViewSortLvLoaded(object sender, RoutedEventArgs e) {
			var lv = e.Source as ListView;
			EnableGridViewSortLvInitialize(lv);
			if (lv != null) lv.Loaded -= EnableGridViewSortLvLoaded;
		}

		public static void EnableGridViewSortLvInitialize(ListView lv) {
			var gv = lv.View as GridView;
			if (gv == null) return;

			var first = true;
			foreach (var gvc in gv.Columns)
				if (first) {
					EnableGridViewSortApplySort(lv, gv, gvc);
					first = false;
				}
		}

		public static void EnableGridViewSortGvhClicked(object sender, RoutedEventArgs e) {
			var gvch = e.OriginalSource as GridViewColumnHeader;
			var gvc = gvch?.Column;
			if (gvc == null) return;
			var lv = VisualUpwardSearch<ListView>(gvch);
			var gv = lv?.View as GridView;
			if (gv == null) return;

			EnableGridViewSortApplySort(lv, gv, gvc);
		}

		public static void EnableGridViewSortApplySort(ListView lv, GridView gv, GridViewColumn gvc) {
			var isEnabled = GetEnableGridViewSort(lv);
			if (!isEnabled) return;

			var propertyName = GetGridViewSortPropertyName(gvc);
			if (string.IsNullOrEmpty(propertyName)) {
				var b = gvc.DisplayMemberBinding as Binding;
				if (b?.Path != null) propertyName = b.Path.Path;

				if (string.IsNullOrEmpty(propertyName)) return;
			}

			ApplySort(lv.Items, propertyName);
			SetCurrentSortColumn(gv, gvc);
		}

		public static void ApplySort(ICollectionView view, string propertyName) {
			if (string.IsNullOrEmpty(propertyName)) return;

			var lsd = ListSortDirection.Ascending;
			if (view.SortDescriptions.Count > 0) {
				var sd = view.SortDescriptions[0];
				if (sd.PropertyName.Equals(propertyName))
					lsd = sd.Direction == ListSortDirection.Ascending
						      ? ListSortDirection.Descending
						      : ListSortDirection.Ascending;
				view.SortDescriptions.Clear();
			}

			view.SortDescriptions.Add(new SortDescription(propertyName, lsd));
		}

		public static T VisualUpwardSearch<T>(DependencyObject source) where T : DependencyObject {
			return VisualUpwardSearch(source, x => x is T) as T;
		}

		public static DependencyObject VisualUpwardSearch(DependencyObject source, Predicate<DependencyObject> match) {
			var returnVal = source;

			while (returnVal != null && !match(returnVal)) {
				DependencyObject tempReturnVal = null;
				if (returnVal is Visual || returnVal is Visual3D) tempReturnVal = VisualTreeHelper.GetParent(returnVal);
				returnVal = tempReturnVal ?? LogicalTreeHelper.GetParent(returnVal);
			}

			return returnVal;
		}

		private void App_OnStartup(object sender, StartupEventArgs e) {
			if (e.Args.Length > 0) {
				if (e.Args[0].Equals("dev")) {
					Developer = true;
				}
			}

			MainWindow mainWindow = new MainWindow();
			mainWindow.Show();
		}
	}
}