﻿using System.Collections.Generic;
using Ds_Bot_Reunited_NEWUI.Farmen;

namespace Ds_Bot_Reunited_NEWUI.AttackPreventionModul {
    public class AttackPreventionVillage {
        public int VillageId { get; set; }
        public string LangsamsteEinheit { get; set; }
        public double Felder { get; set; }
        public int TemplatesPassenrein { get; set; }
        public List<TruppenTemplate> Templates { get; set; }
    }
}