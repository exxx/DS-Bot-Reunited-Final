﻿using System.Windows;
using System.Windows.Controls;

namespace Ds_Bot_Reunited_NEWUI.Farmsettings {
    public partial class FarmsettingsView {
        public FarmsettingsView(Window owner, FarmsettingsViewModel datacontext) {
            Owner = owner;
            DataContext = datacontext;
            InitializeComponent();
        }

        private void Save(object sender, RoutedEventArgs e) {
            Close();
        }

        private void MyGrid_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            if (((FarmsettingsViewModel) DataContext)?.SelectedVillages != null) {
                ((FarmsettingsViewModel) DataContext).SelectedVillages.Clear();
                foreach (var item in DataGrid.SelectedItems) ((FarmsettingsViewModel) DataContext).SelectedVillages.Add((FarmensettingsDorfViewModel) item);
            }
        }
    }
}
