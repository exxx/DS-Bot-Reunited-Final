﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using Ds_Bot_Reunited_NEWUI.Annotations;
using Ds_Bot_Reunited_NEWUI.Farmmodul;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.OwnVillageSelector;
using Ds_Bot_Reunited_NEWUI.SpieldatenViewModel;

namespace Ds_Bot_Reunited_NEWUI.Farmsettings {
    public class FarmtargetSettingsViewModel {
        private readonly FarmtargetViewModel _farm;
        private ICommand _addCommand;
        private ICommand _removeCommand;
        private ICollectionView _villages;

        public FarmtargetSettingsViewModel(Window window, FarmtargetViewModel farm) {
            Window = window;
            _farm = farm;
            Villagess = farm.OnlyGetFarmedByObs;
            Villages = CollectionViewSource.GetDefaultView(Villagess);
            SelectedVillages = new AsyncObservableCollection<DorfViewModel>();
            BindingOperations.EnableCollectionSynchronization(Villagess, VillagessLock);
        }

        public Window Window { get; set; }

        public AsyncObservableCollection<DorfViewModel> SelectedVillages { get; set; }

        public AsyncObservableCollection<DorfViewModel> Villagess { get; set; }
        public object VillagessLock { get; set; } = new object();

        public ICollectionView Villages {
            get => _villages;
            set {
                _villages = value;
                OnPropertyChanged();
            }
        }

        public string SetInactiveAfter {
            get => _farm.SetInactiveAfter.ToString("dd.MM.yyyy HH:mm:ss.fff", CultureInfo.InvariantCulture);
            set {
                try {
                    var date = value.Split('\n')[0];
                    _farm.SetInactiveAfter = DateTime.ParseExact(date, "dd.MM.yyyy HH:mm:ss.fff", CultureInfo.InvariantCulture);
                    OnPropertyChanged();
                } catch {
                    OnPropertyChanged("");
                }
            }
        }

        public string SetActiveAfter {
            get => _farm.SetActiveAfter.ToString("dd.MM.yyyy HH:mm:ss.fff", CultureInfo.InvariantCulture);
            set {
                try {
                    var date = value.Split('\n')[0];
                    _farm.SetActiveAfter = DateTime.ParseExact(date, "dd.MM.yyyy HH:mm:ss.fff", CultureInfo.InvariantCulture);
                    OnPropertyChanged();
                } catch {
                    OnPropertyChanged("");
                }
            }
        }

        public ICommand RemoveCommand => _removeCommand ?? (_removeCommand = new RelayCommand(async () => { await Remove(); }));
        public ICommand AddCommand => _addCommand ?? (_addCommand = new RelayCommand(Add));

        private async Task Remove() {
            await Task.Run(() => {
                               if (SelectedVillages.Any()) {
                                   var count = SelectedVillages.Count;
                                   for (var i = 0; i < count; i++)
                                       Villagess.Remove(SelectedVillages[i]);
                               }
                           });
        }

        private void Add() {
            var vm = new OwnVillageSelectorViewModel(Window);
            var selector = new OwnVillageSelectorView(Window, vm);
            selector.ShowDialog();
            if (vm.SelectedVillages.Any())
                foreach (var dorf in vm.SelectedVillages)
                    if (!Villagess.Any(x => x.VillageId.Equals(dorf.VillageId)))
                        Villagess.Add(dorf);
        }


        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
