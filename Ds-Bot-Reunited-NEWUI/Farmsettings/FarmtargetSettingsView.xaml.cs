﻿using System.Windows;
using System.Windows.Controls;
using Ds_Bot_Reunited_NEWUI.SpieldatenViewModel;
using Window = System.Windows.Window;

namespace Ds_Bot_Reunited_NEWUI.Farmsettings {
    /// <summary>
    /// Interaktionslogik für FarmtargetSettingsView.xaml
    /// </summary>
    public partial class FarmtargetSettingsView {
        public FarmtargetSettingsView(Window owner, FarmtargetSettingsViewModel datacontext) {
            Owner = owner;
            DataContext = datacontext;
            InitializeComponent();
        }

        private void Save(object sender, RoutedEventArgs e) {
            Close();
        }

        void MyGrid_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            if (((FarmtargetSettingsViewModel) DataContext)?.SelectedVillages != null) {
                ((FarmtargetSettingsViewModel) DataContext).SelectedVillages.Clear();
                foreach (var item in DataGrid.SelectedItems) {
                    ((FarmtargetSettingsViewModel) DataContext).SelectedVillages.Add((DorfViewModel) item);
                }
            }
        }
    }
}
