﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using Ds_Bot_Reunited_NEWUI.Annotations;
using Ds_Bot_Reunited_NEWUI.Constants;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.Properties;
using Ds_Bot_Reunited_NEWUI.Spieldaten;

namespace Ds_Bot_Reunited_NEWUI.Farmsettings {
    public class FarmsettingsViewModel : INotifyPropertyChanged {
        private ICommand _copyCommand;
        private GruppeDoerfer _selectedGroup;
        private FarmensettingsDorfViewModel _selectedTemplateVillage;
        private FarmensettingsDorfViewModel _selectedVillage;
        private ICollectionView _villages;

        public FarmsettingsViewModel(Window window) {
            Farmen = new AsyncObservableCollection<FarmensettingsDorfViewModel>();
            Villages = CollectionViewSource.GetDefaultView(Farmen);
            BindingOperations.EnableCollectionSynchronization(Farmen, FarmenLock);
            SelectedVillages = new AsyncObservableCollection<FarmensettingsDorfViewModel>();
            SelectedGroup = Groups.FirstOrDefault(x => x.Gruppenname.Equals(NAMECONSTANTS.GetNameForServer(Names.Alle)));
        }

        public Window Window { get; set; }
        public AsyncObservableCollection<FarmensettingsDorfViewModel> SelectedVillages { get; set; }

        private AsyncObservableCollection<FarmensettingsDorfViewModel> Farmen { get; }
        public List<GruppeDoerfer> Groups => App.Settings.Gruppen;
        public object FarmenLock { get; set; } = new object();

        public ICollectionView Villages {
            get => _villages;
            set {
                _villages = value;
                OnPropertyChanged();
            }
        }

        public FarmensettingsDorfViewModel SelectedVillage {
            get => _selectedVillage;
            set {
                _selectedVillage = value;
                OnPropertyChanged();
            }
        }

        public GruppeDoerfer SelectedGroup {
            get => _selectedGroup;
            set {
                _selectedGroup = value;
                Task.Run(async () => { 
                    await LoadPlayerVillages();
                    OnPropertyChanged("");
                });
            }
        }

        public FarmensettingsDorfViewModel SelectedTemplateVillage {
            get => _selectedTemplateVillage;
            set {
                _selectedTemplateVillage = value;
                OnPropertyChanged();
            }
        }

        public ICommand CopyCommand => _copyCommand ?? (_copyCommand = new RelayCommand(Copy));


        public event PropertyChangedEventHandler PropertyChanged;

        private async Task LoadPlayerVillages() {
            await Task.Run(() => {
                               try {
                                   Farmen.Clear();
                                   foreach (var liste in App.Settings.Accountsettingslist.Select(x => x.DoerferSettings.Where(y => y.Dorf.PlayerId.Equals(x.Playerid) && (SelectedGroup?.DoerferIds.Any(k => k.Equals(y.Dorf.VillageId)) ?? true)))) {
                                       foreach (var dorf in liste.OrderBy(x => x.Dorf.Name))
                                           Farmen.Add(new FarmensettingsDorfViewModel(dorf, Window));
                                   }
                               } catch {
                                       App.LogString(Resources.FailureLoadingPlayervillages);
                               }
                               if (Farmen.Any())
                                   SelectedVillage = Farmen.FirstOrDefault();
                               OnPropertyChanged("");
                           });
        }

        private void Copy() {
            if (SelectedVillages != null && SelectedTemplateVillage != null)
                foreach (var village in SelectedVillages) {
                    village.Template = SelectedTemplateVillage.Template.Clone();
                    village.FarmenActive = SelectedTemplateVillage.FarmenActive;
                    village.FarmenInterval = SelectedTemplateVillage.FarmenInterval;
                    village.MinDorfFarmeninterval = SelectedTemplateVillage.MinDorfFarmeninterval;
                    village.FarmMode = SelectedTemplateVillage.FarmMode;
                    village.MinFarmassistDelay = SelectedTemplateVillage.MinFarmassistDelay;
                    village.MaxFarmassistDelay = SelectedTemplateVillage.MaxFarmassistDelay;
                    village.FarmenInactiveFrom = SelectedTemplateVillage.FarmenInactiveFrom;
                    village.FarmenInactiveTo = SelectedTemplateVillage.FarmenInactiveTo;
                    village.FarmenTroopsMustBeHomeFrom = SelectedTemplateVillage.FarmenTroopsMustBeHomeFrom;
                    village.FarmenTroopsMustBeHomeTo = SelectedTemplateVillage.FarmenTroopsMustBeHomeTo;
                    village.AddVillagesAutomatically = SelectedTemplateVillage.AddVillagesAutomatically;
                    village.AddVillagesPointsMin = SelectedTemplateVillage.AddVillagesPointsMin;
                    village.AddVillagesPointsMax = SelectedTemplateVillage.AddVillagesPointsMax;
                    village.AddVillagesRadiusMin = SelectedTemplateVillage.AddVillagesRadiusMin;
                    village.AddVillagesRadiusMax = SelectedTemplateVillage.AddVillagesRadiusMax;
                    village.AddPlayersAutomatically = SelectedTemplateVillage.AddPlayersAutomatically;
                    village.AddPlayersPointsMin = SelectedTemplateVillage.AddPlayersPointsMin;
                    village.AddPlayersPointsMax = SelectedTemplateVillage.AddPlayersPointsMax;
                    village.UseATemplateFarmassist = SelectedTemplateVillage.UseATemplateFarmassist;
                    village.UseBTemplateFarmassist = SelectedTemplateVillage.UseBTemplateFarmassist;
                    village.UseCTemplateFarmassist = SelectedTemplateVillage.UseCTemplateFarmassist;
                    village.TemplateA = SelectedTemplateVillage.TemplateA.Clone();
                    village.TemplateB = SelectedTemplateVillage.TemplateB.Clone();
                    village.AddFarmsToFarmassistlistAutomatically = SelectedTemplateVillage.AddFarmsToFarmassistlistAutomatically;
                    village.UseMinDorfFarmeninterval = SelectedTemplateVillage.UseMinDorfFarmeninterval;
                }
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
