﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using Ds_Bot_Reunited_NEWUI.Annotations;
using Ds_Bot_Reunited_NEWUI.Constants;
using Ds_Bot_Reunited_NEWUI.Farmen;
using Ds_Bot_Reunited_NEWUI.Properties;
using Ds_Bot_Reunited_NEWUI.Settings;

namespace Ds_Bot_Reunited_NEWUI.Farmsettings {
    public class FarmensettingsDorfViewModel : INotifyPropertyChanged {
        public Window Window { get; set; }
        private readonly AccountDorfSettings _accountDorfSettings;
        public string Villagename => _accountDorfSettings.Dorf.Name;

        public List<string> Farmsystems { get; } = new List<string> {Resources.Intelligent, Resources.Farmassist};

        public bool Farmassist => _accountDorfSettings.FarmMode.Equals(Resources.Farmassist) || _accountDorfSettings.FarmMode.Equals(Resources.Normal);
        public bool Intelligent => _accountDorfSettings.FarmMode.Equals(Resources.Intelligent);
        public bool IsEnabled => App.Permissions.FarmingIsEnabled;

        public int MaxVillages {
            get {
                var alle = App.Settings.Gruppen.FirstOrDefault(x => x.Gruppenname.Equals(NAMECONSTANTS.GetNameForServer(Names.Alle)));
                if (alle != null)
                    return alle.DoerferIds.Count;
                return 0;
            }
        }

        #region FarmenAllgemeinSettingsViewmodel
        public bool AddFarmsToFarmassistlistAutomatically {
            get => _accountDorfSettings.AddFarmsToFarmassistlistAutomatically;
            set {
                _accountDorfSettings.AddFarmsToFarmassistlistAutomatically = value;
                OnPropertyChanged();
            }
        }

        public int FarmOrderNumber {
            get => _accountDorfSettings.FarmOrderNumber;
            set {
                _accountDorfSettings.FarmOrderNumber = value;
                OnPropertyChanged();
            }
        }

        public int AddPlayersMinInactiveDays {
            get => _accountDorfSettings.AddPlayersMinInactiveDays;
            set {
                _accountDorfSettings.AddPlayersMinInactiveDays = value;
                OnPropertyChanged();
            }
        }

        public bool ReserveUnits {
            get => _accountDorfSettings.ReserveUnits;
            set {
                _accountDorfSettings.ReserveUnits = value;
                OnPropertyChanged();
            }
        }

        public TruppenTemplate ReservedUnits {
            get => _accountDorfSettings.ReservedUnits;
            set {
                _accountDorfSettings.ReservedUnits = value;
                OnPropertyChanged();
            }
        }

        public bool FarmenActive {
            get => _accountDorfSettings.FarmenActive;
            set {
                _accountDorfSettings.FarmenActive = value;
                OnPropertyChanged();
            }
        }

        public bool KatternActive {
            get => _accountDorfSettings.KatternActive;
            set {
                _accountDorfSettings.KatternActive = value;
                OnPropertyChanged();
            }
        }

        public int FarmenInterval {
            get => _accountDorfSettings.Farmeninterval;
            set {
                _accountDorfSettings.Farmeninterval = value;
                OnPropertyChanged();
            }
        }

        public int MinDorfFarmeninterval {
            get => _accountDorfSettings.MinDorfFarmeninterval;
            set {
                _accountDorfSettings.MinDorfFarmeninterval = value;
                OnPropertyChanged();
            }
        }

        public string FarmMode {
            get => _accountDorfSettings.FarmMode;
            set {
                _accountDorfSettings.FarmMode = value;
                OnPropertyChanged("");
            }
        }

        public int MinFarmassistDelay {
            get => _accountDorfSettings.MinFarmassistDelay;
            set {
                _accountDorfSettings.MinFarmassistDelay = value;
                OnPropertyChanged();
            }
        }

        public int MaxFarmassistDelay {
            get => _accountDorfSettings.MaxFarmassistDelay;
            set {
                _accountDorfSettings.MaxFarmassistDelay = value;
                OnPropertyChanged();
            }
        }

        public string FarmenInactiveFrom {
            get => _accountDorfSettings.FarmenInactiveFrom.ToString("dd.MM.yyyy HH:mm:ss.fff",
                                                                    CultureInfo.InvariantCulture);
            set {
                try {
                    string date = value.Split('\n')[0];
                    _accountDorfSettings.FarmenInactiveFrom = DateTime.ParseExact(date, "dd.MM.yyyy HH:mm:ss.fff",
                        CultureInfo.InvariantCulture);
                    if (_accountDorfSettings.FarmenInactiveTo < _accountDorfSettings.FarmenInactiveFrom) {
                        FarmenInactiveTo = _accountDorfSettings.FarmenInactiveFrom.ToString("dd.MM.yyyy HH:mm:ss.fff",
                            CultureInfo.InvariantCulture);
                    }
                    OnPropertyChanged();
                } catch {
                    FarmenInactiveFrom = _accountDorfSettings.FarmenInactiveFrom.ToString("dd.MM.yyyy HH:mm:ss.fff",
                        CultureInfo.InvariantCulture);
                    OnPropertyChanged("");
                }
            }
        }

        public string FarmenInactiveTo {
            get => _accountDorfSettings.FarmenInactiveTo.ToString("dd.MM.yyyy HH:mm:ss.fff",
                                                                  CultureInfo.InvariantCulture);
            set {
                try {
                    string date = value.Split('\n')[0];
                    _accountDorfSettings.FarmenInactiveTo = DateTime.ParseExact(date, "dd.MM.yyyy HH:mm:ss.fff",
                        CultureInfo.InvariantCulture);
                    if (_accountDorfSettings.FarmenInactiveTo < _accountDorfSettings.FarmenInactiveFrom) {
                        FarmenInactiveFrom = _accountDorfSettings.FarmenInactiveTo.ToString("dd.MM.yyyy HH:mm:ss.fff",
                            CultureInfo.InvariantCulture);
                    }
                    OnPropertyChanged();
                } catch {
                    FarmenInactiveTo = _accountDorfSettings.FarmenInactiveTo.ToString("dd.MM.yyyy HH:mm:ss.fff",
                        CultureInfo.InvariantCulture);
                    OnPropertyChanged("");
                }
            }
        }

        public string FarmenTroopsMustBeHomeFrom {
            get => _accountDorfSettings.FarmenTroopsMustBeHomeFrom.ToString("dd.MM.yyyy HH:mm:ss.fff",
                                                                            CultureInfo.InvariantCulture);
            set {
                try {
                    string date = value.Split('\n')[0];
                    _accountDorfSettings.FarmenTroopsMustBeHomeFrom = DateTime.ParseExact(date, "dd.MM.yyyy HH:mm:ss.fff",
                        CultureInfo.InvariantCulture);
                    if (_accountDorfSettings.FarmenTroopsMustBeHomeTo < _accountDorfSettings.FarmenTroopsMustBeHomeFrom) {
                        FarmenTroopsMustBeHomeTo = _accountDorfSettings.FarmenTroopsMustBeHomeFrom.ToString("dd.MM.yyyy HH:mm:ss.fff",
                            CultureInfo.InvariantCulture);
                    }
                    OnPropertyChanged();
                } catch {
                    FarmenTroopsMustBeHomeFrom =
                        _accountDorfSettings.FarmenTroopsMustBeHomeFrom.ToString("dd.MM.yyyy HH:mm:ss.fff",
                            CultureInfo.InvariantCulture);
                    OnPropertyChanged("");
                }
            }
        }

        public string FarmenTroopsMustBeHomeTo {
            get => _accountDorfSettings.FarmenTroopsMustBeHomeTo.ToString("dd.MM.yyyy HH:mm:ss.fff",
                                                                          CultureInfo.InvariantCulture);
            set {
                try {
                    string date = value.Split('\n')[0];
                    _accountDorfSettings.FarmenTroopsMustBeHomeTo = DateTime.ParseExact(date, "dd.MM.yyyy HH:mm:ss.fff",
                        CultureInfo.InvariantCulture);
                    if (_accountDorfSettings.FarmenTroopsMustBeHomeTo < _accountDorfSettings.FarmenTroopsMustBeHomeFrom) {
                        FarmenTroopsMustBeHomeFrom = _accountDorfSettings.FarmenTroopsMustBeHomeTo.ToString("dd.MM.yyyy HH:mm:ss.fff",
                            CultureInfo.InvariantCulture);
                    }
                    OnPropertyChanged();
                } catch {
                    FarmenTroopsMustBeHomeTo =
                        _accountDorfSettings.FarmenTroopsMustBeHomeTo.ToString("dd.MM.yyyy HH:mm:ss.fff",
                            CultureInfo.InvariantCulture);
                    OnPropertyChanged("");
                }
            }
        }

        public bool AddVillagesAutomatically {
            get => _accountDorfSettings.AddVillagesAutomatically;
            set {
                _accountDorfSettings.AddVillagesAutomatically = value;
                OnPropertyChanged();
            }
        }

        public int AddVillagesPointsMin {
            get => _accountDorfSettings.AddVillagesPointsMin;
            set {
                _accountDorfSettings.AddVillagesPointsMin = value;
                OnPropertyChanged();
            }
        }

        public int AddVillagesPointsMax {
            get => _accountDorfSettings.AddVillagesPointsMax;
            set {
                _accountDorfSettings.AddVillagesPointsMax = value;
                OnPropertyChanged();
            }
        }

        public double AddVillagesRadiusMin {
            get => _accountDorfSettings.AddVillagesRadiusMin;
            set {
                _accountDorfSettings.AddVillagesRadiusMin = value;
                OnPropertyChanged();
            }
        }

        public double AddVillagesRadiusMax {
            get => _accountDorfSettings.AddVillagesRadiusMax;
            set {
                _accountDorfSettings.AddVillagesRadiusMax = value;
                OnPropertyChanged();
            }
        }

        public bool AddPlayersAutomatically {
            get => _accountDorfSettings.AddPlayersAutomatically;
            set {
                _accountDorfSettings.AddPlayersAutomatically = value;
                OnPropertyChanged();
            }
        }

        public int AddPlayersPointsMin {
            get => _accountDorfSettings.AddPlayersPointsMin;
            set {
                _accountDorfSettings.AddPlayersPointsMin = value;
                OnPropertyChanged();
            }
        }

        public int AddPlayersPointsMax {
            get => _accountDorfSettings.AddPlayersPointsMax;
            set {
                _accountDorfSettings.AddPlayersPointsMax = value;
                OnPropertyChanged();
            }
        }

        public bool UseATemplateFarmassist {
            get => _accountDorfSettings.UseATemplateFarmassist;
            set {
                _accountDorfSettings.UseATemplateFarmassist = value;
                OnPropertyChanged();
            }
        }

        public bool UseBTemplateFarmassist {
            get => _accountDorfSettings.UseBTemplateFarmassist;
            set {
                _accountDorfSettings.UseBTemplateFarmassist = value;
                OnPropertyChanged();
            }
        }

        public bool UseCTemplateFarmassist {
            get => _accountDorfSettings.UseCTemplateFarmassist;
            set {
                _accountDorfSettings.UseCTemplateFarmassist = value;
                OnPropertyChanged();
            }
        }

        public bool UseMinDorfFarmeninterval {
            get => _accountDorfSettings.UseMinDorfFarmeninterval;
            set {
                _accountDorfSettings.UseMinDorfFarmeninterval = value;
                OnPropertyChanged();
            }
        }

        public TruppenTemplate TemplateA {
            get => _accountDorfSettings.TemplateA;
            set {
                _accountDorfSettings.TemplateA = value;
                OnPropertyChanged();
            }
        }

        public TruppenTemplate TemplateB {
            get => _accountDorfSettings.TemplateB;
            set {
                _accountDorfSettings.TemplateB = value;
                OnPropertyChanged();
            }
        }

        #endregion


        #region FarmenTruppenSettingsViewmodel

        public int SpeertraegerMaxLaufzeit {
            get => _accountDorfSettings.FarmTruppenTemplate.SpeertraegerMaxLaufzeit;
            set {
                _accountDorfSettings.FarmTruppenTemplate.SpeertraegerMaxLaufzeit = value;
                OnPropertyChanged();
            }
        }

        public int SchwertkaempferMaxLaufzeit {
            get => _accountDorfSettings.FarmTruppenTemplate.SchwertkaempferMaxLaufzeit;
            set {
                _accountDorfSettings.FarmTruppenTemplate.SchwertkaempferMaxLaufzeit = value;
                OnPropertyChanged();
            }
        }

        public int AxtkaempferMaxLaufzeit {
            get => _accountDorfSettings.FarmTruppenTemplate.AxtkaempferMaxLaufzeit;
            set {
                _accountDorfSettings.FarmTruppenTemplate.AxtkaempferMaxLaufzeit = value;
                OnPropertyChanged();
            }
        }

        public int BogenschuetzenMaxLaufzeit {
            get => _accountDorfSettings.FarmTruppenTemplate.BogenschuetzenMaxLaufzeit;
            set {
                _accountDorfSettings.FarmTruppenTemplate.BogenschuetzenMaxLaufzeit = value;
                OnPropertyChanged();
            }
        }

        public int SpaeherMaxLaufzeit {
            get => _accountDorfSettings.FarmTruppenTemplate.SpaeherMaxLaufzeit;
            set {
                _accountDorfSettings.FarmTruppenTemplate.SpaeherMaxLaufzeit = value;
                OnPropertyChanged();
            }
        }

        public int LeichteKavallerieMaxLaufzeit {
            get => _accountDorfSettings.FarmTruppenTemplate.LeichteKavallerieMaxLaufzeit;
            set {
                _accountDorfSettings.FarmTruppenTemplate.LeichteKavallerieMaxLaufzeit = value;
                OnPropertyChanged();
            }
        }

        public int BeritteneBogenschuetzenMaxLaufzeit {
            get => _accountDorfSettings.FarmTruppenTemplate.BeritteneBogenschuetzenMaxLaufzeit;
            set {
                _accountDorfSettings.FarmTruppenTemplate.BeritteneBogenschuetzenMaxLaufzeit = value;
                OnPropertyChanged();
            }
        }

        public int SchwereKavallerieMaxLaufzeit {
            get => _accountDorfSettings.FarmTruppenTemplate.SchwereKavallerieMaxLaufzeit;
            set {
                _accountDorfSettings.FarmTruppenTemplate.SchwereKavallerieMaxLaufzeit = value;
                OnPropertyChanged();
            }
        }

        public int RammboeckeMaxLaufzeit {
            get => _accountDorfSettings.FarmTruppenTemplate.RammboeckeMaxLaufzeit;
            set {
                _accountDorfSettings.FarmTruppenTemplate.RammboeckeMaxLaufzeit = value;
                OnPropertyChanged();
            }
        }

        public int KatapulteMaxLaufzeit {
            get => _accountDorfSettings.FarmTruppenTemplate.KatapulteMaxLaufzeit;
            set {
                _accountDorfSettings.FarmTruppenTemplate.KatapulteMaxLaufzeit = value;
                OnPropertyChanged();
            }
        }

        public int PaladinMaxLaufzeit {
            get => _accountDorfSettings.FarmTruppenTemplate.PaladinMaxLaufzeit;
            set {
                _accountDorfSettings.FarmTruppenTemplate.PaladinMaxLaufzeit = value;
                OnPropertyChanged();
            }
        }

        public bool SpeertraegerActive {
            get => _accountDorfSettings.FarmTruppenTemplate.SpeertraegerActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.SpeertraegerActive = value;
                OnPropertyChanged();
            }
        }

        public bool SchwertkaempferActive {
            get => _accountDorfSettings.FarmTruppenTemplate.SchwertkaempferActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.SchwertkaempferActive = value;
                OnPropertyChanged();
            }
        }

        public bool AxtkaempferActive {
            get => _accountDorfSettings.FarmTruppenTemplate.AxtkaempferActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.AxtkaempferActive = value;
                OnPropertyChanged();
            }
        }

        public bool BogenschuetzenActive {
            get => _accountDorfSettings.FarmTruppenTemplate.BogenschuetzenActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.BogenschuetzenActive = value;
                OnPropertyChanged();
            }
        }

        public bool SpaeherActive {
            get => _accountDorfSettings.FarmTruppenTemplate.SpaeherActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.SpaeherActive = value;
                OnPropertyChanged();
            }
        }

        public bool LeichteKavallerieActive {
            get => _accountDorfSettings.FarmTruppenTemplate.LeichteKavallerieActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.LeichteKavallerieActive = value;
                OnPropertyChanged();
            }
        }

        public bool BeritteneBogenschuetzenActive {
            get => _accountDorfSettings.FarmTruppenTemplate.BeritteneBogenschuetzenActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.BeritteneBogenschuetzenActive = value;
                OnPropertyChanged();
            }
        }

        public bool SchwereKavallerieActive {
            get => _accountDorfSettings.FarmTruppenTemplate.SchwereKavallerieActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.SchwereKavallerieActive = value;
                OnPropertyChanged();
            }
        }

        public bool RammboeckeActive {
            get => _accountDorfSettings.FarmTruppenTemplate.RammboeckeActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.RammboeckeActive = value;
                OnPropertyChanged();
            }
        }

        public bool KatapulteActive {
            get => _accountDorfSettings.FarmTruppenTemplate.KatapulteActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.KatapulteActive = value;
                OnPropertyChanged();
            }
        }

        public bool PaladinActive {
            get => _accountDorfSettings.FarmTruppenTemplate.PaladinActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.PaladinActive = value;
                OnPropertyChanged();
            }
        }

        public bool SpeertraegerAgainstWallActive {
            get => _accountDorfSettings.FarmTruppenTemplate.SpeertraegerAgainstWallActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.SpeertraegerAgainstWallActive = value;
                OnPropertyChanged();
            }
        }

        public bool SpeertraegerAgainstBarbsActive {
            get => _accountDorfSettings.FarmTruppenTemplate.SpeertraegerAgainstBarbsActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.SpeertraegerAgainstBarbsActive = value;
                OnPropertyChanged();
            }
        }

        public bool SpeertraegerAgainstPlayerActive {
            get => _accountDorfSettings.FarmTruppenTemplate.SpeertraegerAgainstPlayerActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.SpeertraegerAgainstPlayerActive = value;
                OnPropertyChanged();
            }
        }

        public bool SpeertraegerWithRamActive {
            get => _accountDorfSettings.FarmTruppenTemplate.SpeertraegerWithRamActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.SpeertraegerWithRamActive = value;
                OnPropertyChanged();
            }
        }

        public bool SpeertraegerWithCatActive {
            get => _accountDorfSettings.FarmTruppenTemplate.SpeertraegerWithCatActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.SpeertraegerWithCatActive = value;
                OnPropertyChanged();
            }
        }

        public bool SchwertkaempferAgainstWallActive {
            get => _accountDorfSettings.FarmTruppenTemplate.SchwertkaempferAgainstWallActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.SchwertkaempferAgainstWallActive = value;
                OnPropertyChanged();
            }
        }

        public bool SchwertkaempferAgainstBarbsActive {
            get => _accountDorfSettings.FarmTruppenTemplate.SchwertkaempferAgainstBarbsActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.SchwertkaempferAgainstBarbsActive = value;
                OnPropertyChanged();
            }
        }

        public bool SchwertkaempferAgainstPlayerActive {
            get => _accountDorfSettings.FarmTruppenTemplate.SchwertkaempferAgainstPlayerActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.SchwertkaempferAgainstPlayerActive = value;
                OnPropertyChanged();
            }
        }

        public bool SchwertkaempferWithRamActive {
            get => _accountDorfSettings.FarmTruppenTemplate.SchwertkaempferWithRamActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.SchwertkaempferWithRamActive = value;
                OnPropertyChanged();
            }
        }

        public bool SchwertkaempferWithCatActive {
            get => _accountDorfSettings.FarmTruppenTemplate.SchwertkaempferWithCatActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.SchwertkaempferWithCatActive = value;
                OnPropertyChanged();
            }
        }

        public bool AxtkaempferAgainstWallActive {
            get => _accountDorfSettings.FarmTruppenTemplate.AxtkaempferAgainstWallActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.AxtkaempferAgainstWallActive = value;
                OnPropertyChanged();
            }
        }

        public bool AxtkaempferAgainstBarbsActive {
            get => _accountDorfSettings.FarmTruppenTemplate.AxtkaempferAgainstBarbsActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.AxtkaempferAgainstBarbsActive = value;
                OnPropertyChanged();
            }
        }

        public bool AxtkaempferAgainstPlayerActive {
            get => _accountDorfSettings.FarmTruppenTemplate.AxtkaempferAgainstPlayerActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.AxtkaempferAgainstPlayerActive = value;
                OnPropertyChanged();
            }
        }

        public bool AxtkaempferWithRamActive {
            get => _accountDorfSettings.FarmTruppenTemplate.AxtkaempferWithRamActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.AxtkaempferWithRamActive = value;
                OnPropertyChanged();
            }
        }

        public bool AxtkaempferWithCatActive {
            get => _accountDorfSettings.FarmTruppenTemplate.AxtkaempferWithCatActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.AxtkaempferWithCatActive = value;
                OnPropertyChanged();
            }
        }

        public bool BogenschuetzenAgainstWallActive {
            get => _accountDorfSettings.FarmTruppenTemplate.BogenschuetzenAgainstWallActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.BogenschuetzenAgainstWallActive = value;
                OnPropertyChanged();
            }
        }

        public bool BogenschuetzenAgainstBarbsActive {
            get => _accountDorfSettings.FarmTruppenTemplate.BogenschuetzenAgainstBarbsActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.BogenschuetzenAgainstBarbsActive = value;
                OnPropertyChanged();
            }
        }

        public bool BogenschuetzenAgainstPlayerActive {
            get => _accountDorfSettings.FarmTruppenTemplate.BogenschuetzenAgainstPlayerActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.BogenschuetzenAgainstPlayerActive = value;
                OnPropertyChanged();
            }
        }

        public bool BogenschuetzenWithRamActive {
            get => _accountDorfSettings.FarmTruppenTemplate.BogenschuetzenWithRamActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.BogenschuetzenWithRamActive = value;
                OnPropertyChanged();
            }
        }

        public bool BogenschuetzenWithCatActive {
            get => _accountDorfSettings.FarmTruppenTemplate.BogenschuetzenWithCatActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.BogenschuetzenWithCatActive = value;
                OnPropertyChanged();
            }
        }

        public bool SpaeherAgainstWallActive {
            get => _accountDorfSettings.FarmTruppenTemplate.SpaeherAgainstWallActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.SpaeherAgainstWallActive = value;
                OnPropertyChanged();
            }
        }

        public bool SpaeherAgainstBarbsActive {
            get => _accountDorfSettings.FarmTruppenTemplate.SpaeherAgainstBarbsActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.SpaeherAgainstBarbsActive = value;
                OnPropertyChanged();
            }
        }

        public bool SpaeherAgainstPlayerActive {
            get => _accountDorfSettings.FarmTruppenTemplate.SpaeherAgainstPlayerActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.SpaeherAgainstPlayerActive = value;
                OnPropertyChanged();
            }
        }

        public bool LeichteKavallerieAgainstWallActive {
            get => _accountDorfSettings.FarmTruppenTemplate.LeichteKavallerieAgainstWallActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.LeichteKavallerieAgainstWallActive = value;
                OnPropertyChanged();
            }
        }

        public bool LeichteKavallerieAgainstBarbsActive {
            get => _accountDorfSettings.FarmTruppenTemplate.LeichteKavallerieAgainstBarbsActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.LeichteKavallerieAgainstBarbsActive = value;
                OnPropertyChanged();
            }
        }

        public bool LeichteKavallerieAgainstPlayerActive {
            get => _accountDorfSettings.FarmTruppenTemplate.LeichteKavallerieAgainstPlayerActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.LeichteKavallerieAgainstPlayerActive = value;
                OnPropertyChanged();
            }
        }

        public bool LeichteKavallerieWithRamActive {
            get => _accountDorfSettings.FarmTruppenTemplate.LeichteKavallerieWithRamActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.LeichteKavallerieWithRamActive = value;
                OnPropertyChanged();
            }
        }

        public bool LeichteKavallerieWithCatActive {
            get => _accountDorfSettings.FarmTruppenTemplate.LeichteKavallerieWithCatActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.LeichteKavallerieWithCatActive = value;
                OnPropertyChanged();
            }
        }

        public bool BeritteneBogenschuetzenAgainstWallActive {
            get => _accountDorfSettings.FarmTruppenTemplate.BeritteneBogenschuetzenAgainstWallActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.BeritteneBogenschuetzenAgainstWallActive = value;
                OnPropertyChanged();
            }
        }

        public bool BeritteneBogenschuetzenAgainstBarbsActive {
            get => _accountDorfSettings.FarmTruppenTemplate.BeritteneBogenschuetzenAgainstBarbsActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.BeritteneBogenschuetzenAgainstBarbsActive = value;
                OnPropertyChanged();
            }
        }

        public bool BeritteneBogenschuetzenAgainstPlayerActive {
            get => _accountDorfSettings.FarmTruppenTemplate.BeritteneBogenschuetzenAgainstPlayerActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.BeritteneBogenschuetzenAgainstPlayerActive = value;
                OnPropertyChanged();
            }
        }

        public bool BeritteneBogenschuetzenWithRamActive {
            get => _accountDorfSettings.FarmTruppenTemplate.BeritteneBogenschuetzenWithRamActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.BeritteneBogenschuetzenWithRamActive = value;
                OnPropertyChanged();
            }
        }

        public bool BeritteneBogenschuetzenWithCatActive {
            get => _accountDorfSettings.FarmTruppenTemplate.BeritteneBogenschuetzenWithCatActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.BeritteneBogenschuetzenWithCatActive = value;
                OnPropertyChanged();
            }
        }

        public bool SchwereKavallerieAgainstWallActive {
            get => _accountDorfSettings.FarmTruppenTemplate.SchwereKavallerieAgainstWallActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.SchwereKavallerieAgainstWallActive = value;
                OnPropertyChanged();
            }
        }

        public bool SchwereKavallerieAgainstBarbsActive {
            get => _accountDorfSettings.FarmTruppenTemplate.SchwereKavallerieAgainstBarbsActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.SchwereKavallerieAgainstBarbsActive = value;
                OnPropertyChanged();
            }
        }

        public bool SchwereKavallerieAgainstPlayerActive {
            get => _accountDorfSettings.FarmTruppenTemplate.SchwereKavallerieAgainstPlayerActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.SchwereKavallerieAgainstPlayerActive = value;
                OnPropertyChanged();
            }
        }

        public bool SchwereKavallerieWithRamActive {
            get => _accountDorfSettings.FarmTruppenTemplate.SchwereKavallerieWithRamActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.SchwereKavallerieWithRamActive = value;
                OnPropertyChanged();
            }
        }

        public bool SchwereKavallerieWithCatActive {
            get => _accountDorfSettings.FarmTruppenTemplate.SchwereKavallerieWithCatActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.SchwereKavallerieWithCatActive = value;
                OnPropertyChanged();
            }
        }

        public bool PaladinAgainstWallActive {
            get => _accountDorfSettings.FarmTruppenTemplate.PaladinAgainstWallActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.PaladinAgainstWallActive = value;
                OnPropertyChanged();
            }
        }

        public bool PaladinAgainstBarbsActive {
            get => _accountDorfSettings.FarmTruppenTemplate.PaladinAgainstBarbsActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.PaladinAgainstBarbsActive = value;
                OnPropertyChanged();
            }
        }

        public bool PaladinAgainstPlayerActive {
            get => _accountDorfSettings.FarmTruppenTemplate.PaladinAgainstPlayerActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.PaladinAgainstPlayerActive = value;
                OnPropertyChanged();
            }
        }

        public bool RammboeckeAgainstWallActive {
            get => _accountDorfSettings.FarmTruppenTemplate.RammboeckeAgainstWallActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.RammboeckeAgainstWallActive = value;
                OnPropertyChanged();
            }
        }

        public bool RammboeckeAgainstBarbsActive {
            get => _accountDorfSettings.FarmTruppenTemplate.RammboeckeAgainstBarbsActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.RammboeckeAgainstBarbsActive = value;
                OnPropertyChanged();
            }
        }

        public bool RammboeckeAgainstPlayerActive {
            get => _accountDorfSettings.FarmTruppenTemplate.RammboeckeAgainstPlayerActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.RammboeckeAgainstPlayerActive = value;
                OnPropertyChanged();
            }
        }

        public bool KatapulteAgainstWallActive {
            get => _accountDorfSettings.FarmTruppenTemplate.KatapulteAgainstWallActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.KatapulteAgainstWallActive = value;
                OnPropertyChanged();
            }
        }

        public bool KatapulteAgainstBarbsActive {
            get => _accountDorfSettings.FarmTruppenTemplate.KatapulteAgainstBarbsActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.KatapulteAgainstBarbsActive = value;
                OnPropertyChanged();
            }
        }

        public bool KatapulteAgainstPlayerActive {
            get => _accountDorfSettings.FarmTruppenTemplate.KatapulteAgainstPlayerActive;
            set {
                _accountDorfSettings.FarmTruppenTemplate.KatapulteAgainstPlayerActive = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region FarmenPaareSettingsViewmodel

        public FarmenTruppenTemplate Template {
            get => _accountDorfSettings.FarmTruppenTemplate;
            set {
                _accountDorfSettings.FarmTruppenTemplate = value;
                OnPropertyChanged();
            }
        }

        #endregion


        public bool PaladinOverallActive => App.Settings.Weltensettings.PaladinActive;
        public bool BogenschuetzenOverallActive => App.Settings.Weltensettings.BogenschuetzenActive;

        public FarmensettingsDorfViewModel(AccountDorfSettings accountDorfSettings, Window window) {
            Window = window;
            _accountDorfSettings = accountDorfSettings;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}