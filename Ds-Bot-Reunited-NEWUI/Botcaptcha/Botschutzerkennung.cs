﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Ds_Bot_Reunited_NEWUI.Accountsettings;
using Ds_Bot_Reunited_NEWUI.Accountsettings.Ds_Bot_Reunited.Accountsettings;
using Ds_Bot_Reunited_NEWUI.BrowserManager;
using Ds_Bot_Reunited_NEWUI.Constants;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.LoginModul;
using Ds_Bot_Reunited_NEWUI.Properties;
using Ds_Bot_Reunited_NEWUI.TwoCaptcha_API;
using Framework.UI.Controls;
using OpenQA.Selenium;

namespace Ds_Bot_Reunited_NEWUI.Botcaptcha {
    public class Botschutzerkennung {
        public static async Task<bool> CheckIfBotschutz(Browser browser, Accountsetting accountsettings, string wantedurl = null) {
            try {
                await SessionLost(browser, accountsettings, wantedurl);
                if (accountsettings.MultiaccountingDaten.Accountmode == Accountmode.Mainaccount && browser.WebDriver.FindElements(By.XPath(SpielOberflaeche.Uebersicht.Angriffe.ANGRIFFECOUNTER_SELECTOR_XPATH)).Count > 0) {
                    accountsettings.PleaseQueueDefend = true;
                }

                await QuestModul.QuestModul.CheckIfQuestOpenAndCloseIt(browser);
                if (browser.WebDriver.FindElements(By.XPath("//iframe[contains(@src,'https://www.google.com/recaptcha/api2')]")).Any()) {
                    int counter = 0;

                    var frame =
	                    browser.WebDriver
	                           .FindElement(By.XPath("//iframe[contains(@src,'https://www.google.com/recaptcha/api2')]"));
                    browser.WebDriver.SwitchTo().Frame(frame);
                    try {
	                    if (browser.WebDriver.FindElements(By.ClassName("recaptcha-checkbox-checkmark")).Any()) {
		                    var button = browser.WebDriver.FindElement(By.ClassName("recaptcha-checkbox-checkmark"));
		                    browser.ScrollToElement(button);
		                    button.Click();
	                    }
                    } catch {
	                    // ignored
                    }

                    browser.WebDriver.SwitchTo().DefaultContent();

                    while (browser.WebDriver.FindElements(By.XPath("//iframe[contains(@src,'https://www.google.com/recaptcha/api2')]")).Any() &&
                           counter < 5) {
                        browser.Active = false; App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + Resources.BotcaptchaSolvingVersuch + " " + (counter + 1));
                        string url = browser.WebDriver.Url;
                        TwoCaptchaClient client = new TwoCaptchaClient(App.Settings.TwoCaptchaApiKey);
                        frame =
                            browser.WebDriver.FindElement(By.XPath("//iframe[contains(@src,'https://www.google.com/recaptcha/api2')]"));
                        string kanfang = frame.GetAttribute("src");
                        string[] splitted = kanfang.Split('&');
                        var keySplitted = splitted[1].Split('=');

                        bool succeeded = client.SolveRecaptchaV2(keySplitted[1], url, out var result);

                        if (succeeded) {
                            IJavaScriptExecutor executor = (IJavaScriptExecutor) browser.WebDriver;
                            executor.ExecuteScript(
                                "document.getElementById('g-recaptcha-response').style.display = 'block';");
                            await Task.Delay(500);
                            IWebElement element =
                                browser.WebDriver.FindElement(By.XPath("//textarea[@class='g-recaptcha-response']"));

                            ((IJavaScriptExecutor) browser.WebDriver).ExecuteScript(
                                "arguments[0].scrollIntoView(true);",
                                element);
                            element.SendKeys(result);
                            await Task.Delay(500);
                            
                            executor.ExecuteScript("BotProtect.check($('#g-recaptcha-response').val());");
                            browser.WebDriver.WaitForPageload();
                            browser.WebDriver.WaitForAjax();
                            browser.WebDriver.Navigate().GoToUrl(url);
                            browser.WebDriver.WaitForPageload();
                            if (counter > 2) {
                                await Task.Delay(60000);
                            }
                            browser.WebDriver.Navigate().Refresh();
                            browser.WebDriver.WaitForPageload();
                        } else {
	                        App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": BOTCAPTCHA NOT SOLVED");
                        }
                        counter++;
                    }
                    if (counter > 5) {
                        MessageBox.Show(Resources.BotcaptchaNotSolvable, Resources.ERROR, MessageBoxButton.OK,
                            MessageBoxImage.Error);
                    }
                    else {
                        App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + Resources.BotcaptchaSolved);
                        browser.Active = true;
                        await Task.Delay(500);
                        return true;
                    }
                }
            } catch (Exception e) {
                App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + Resources.BotcaptchaError + " " + e);
            }
            return false;
        }

        private static async Task SessionLost(Browser browser, Accountsetting accountsettings, string wantedUrl = null) {
            try {
                if (accountsettings.MultiaccountingDaten.Accountmode == Accountmode.Mainaccount && !browser.WebDriver.Url.Contains(App.Settings.Welt)) {
                    App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + Resources.LostSession);
                    DateTime now = DateTime.Now;
                    if (accountsettings.RelogOnSessionLostAfterMinutes > 0) {
                        App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + Resources.RelogIn + " " + accountsettings.RelogOnSessionLostAfterMinutes + " " + Resources.Minutes);
                        while (now > DateTime.Now.AddMinutes(accountsettings.RelogOnSessionLostAfterMinutes)) {
                            App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + Resources.WaitingForRelog);
                            await Task.Delay(1000);
                        }
                    }
                    while (!accountsettings.CanLoginAndDoSomething)
                        await Task.Delay(100);
                    if (accountsettings.ReloggingActive) {
                        await Loginmodul.LoginStart(browser, accountsettings, true);
                        BrowserManager.BrowserManager.SynchronizeCookies(accountsettings);
                        if (!string.IsNullOrEmpty(wantedUrl)) {
                            browser.WebDriver.Navigate().GoToUrl(wantedUrl);
                            await Task.Delay(3000);
                        }
                    }
                    if (!accountsettings.ReloggingActive) {
                        await MessageDialog.ShowAsync(Resources.PleaseRestartBot, Resources.RestartTheBot, MessageBoxButton.OK, MessageDialogType.Accent,
                                                      App.Window);
                    }
                }
            } catch (Exception e) {
                App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + e);
            }
        }
    }
}