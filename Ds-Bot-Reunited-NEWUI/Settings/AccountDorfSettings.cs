﻿using System;
using System.Collections.Concurrent;
using System.ComponentModel;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Windows.Data;
using Ds_Bot_Reunited_NEWUI.Accountsettings.Ds_Bot_Reunited.Accountsettings;
using Ds_Bot_Reunited_NEWUI.Annotations;
using Ds_Bot_Reunited_NEWUI.BauenModul;
using Ds_Bot_Reunited_NEWUI.Farmen;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.Marktmodul.Ds_Bot_Reunited_NEWUI.Constants;
using Ds_Bot_Reunited_NEWUI.Properties;
using Ds_Bot_Reunited_NEWUI.Rekrutieren;
using Ds_Bot_Reunited_NEWUI.SpieldatenViewModel;

namespace Ds_Bot_Reunited_NEWUI.Settings {
    [Obfuscation(Exclude = true, Feature = "renaming")]
    public class AccountDorfSettings : INotifyPropertyChanged {
        private bool _addPlayersAutomatically = true;
        private int _addPlayersMinInactiveDays;
        private int _addPlayersPointsMax = 90;
        private int _addPlayersPointsMin = 23;
        private bool _addVillagesAutomatically = true;
        private int _addVillagesPointsMax = 90;
        private int _addVillagesPointsMin = 23;
        private double _addVillagesRadiusMax = 15;
        private double _addVillagesRadiusMin;
        private bool _angreifenActive;
        private int _angreifeninterval;
        private bool _attackdetectionActive = true;
        private int _attackdetectioninterval = 30;
        private bool _attackDodgingActive;
        private int _attackDodginginterval = 30;
        private bool _attackPreventionActive;
        private int _attackPreventioninterval = 30;
        private bool _bauenActive;
        private int _bauenEisenUebrigLassen;
        private int _bauenHolzUebrigLassen;
        private int _baueninterval = 10;
        private int _bauenLehmUebrigLassen;
        private bool _bauenMinenAusgleichen;
        private bool _bhAutomatischBauenActive;
        private DorfViewModel _dorf;
        private DorfinformationenModul.Dorfinformationen _dorfinformationen;
        private ConcurrentBag<PremiumDepotRessource> _empfangen;
        private bool _farmenActive;
        private DateTime _farmenInactiveFrom;
        private DateTime _farmenInactiveTo;
        private int _farmeninterval;
        private DateTime _farmenTroopsMustBeHomeFrom;
        private DateTime _farmenTroopsMustBeHomeTo;
        private string _farmMode = Resources.Intelligent;
        private ConcurrentBag<PremiumDepotRessource> _handel;
        private HandelEmpfangArt _handelEmpfangenArt;
        private int _handelEmpfangenEisenUnter;
        private int _handelEmpfangenHolzUnter;
        private int _handelEmpfangenLehmUnter;
        private int _handelHaendlerZurueckHalten;
        private int _handelMaxLaufzeit;
        private double _handelMinRessourcen;
        private bool _handelnActive;
        private int _handelninterval = 30;
        private string _handelRouteCoords;
        private HandelVerschickArt _handelVerschickenArt;
        private int _handelVerschickenEisenUeber;
        private int _handelVerschickenHolzUeber;
        private int _handelVerschickenLehmUeber;
        private bool _katternActive;
        private DateTime _lastTruppeneinlesen;
        private DateTime _lastUnterstuetzungeneinlesen;
        private bool _massenAngriffeActive;
        private int _massenAngriffeinterval;
        private int _maxFarmassistDelay = 40;
        private int _minDorfFarmeninterval = 30;
        private int _minFarmassistDelay = 10;
        private int _muenzenPraegen;
        private DateTime _nextAvailableBauschleife;
        private DateTime _nextAvailableTroopAtHomeAt;
        private int _notUnderPremiumPunkte;
        private bool _overallActive = true;
        private string _premiumDepotHandelArt;
        private double _premiumDepotHandelMaxVerhaeltnis;
        private double _premiumDepotHandelMinVerhaeltnis;
        private int _premiumDepotinterval;
        private bool _premiumDepotQueued;
        private int _premiumDepotSellPercentage;
        private int _premiumDepotStayMinutes;
        private TruppenTemplate _rekrutieren;
        private bool _rekrutierenActive;
        private int _rekrutierenEisenUebrigLassen;
        private int _rekrutierenHolzUebrigLassen;
        private int _rekrutiereninterval = 10;
        private int _rekrutierenLehmUebrigLassen;
        private TruppenTemplate _reservedUnits;
        private bool _reserveUnits;
        private bool _speicherAutomatischBauenActive;
        private TruppenTemplate _templateA;
        private TruppenTemplate _templateB;
        private bool _useATemplateFarmassist;
        private bool _useBTemplateFarmassist;
        private bool _useCTemplateFarmassist;
        private bool _useOverallSettings;
        private string _zugehoerigerAccountname;
        private int _farmOrderNumber;
        private bool _forschenActive = true;
        private int _forscheninterval = 20;
        private bool _balanceForschenActive;
        private bool _addFarmsToFarmassistlistAutomatically;
        private double _kasernePercent = 33.3;
        private double _stallPercent = 33.3;
        private double _werkstattPercent = 33.3;
        private int _kaserneMaxHours = 2;
        private int _stallMaxHours = 2;
        private int _werkstattMaxHours = 2;
        private bool _useMinDorfFarmeninterval = true;
        private bool _raubzugActive;
        private int _raubzugInterval;

        public AccountDorfSettings() {
            BauenMinenAusgleichen = true;
            OverallActive = true;
            UseOverallSettings = false;
            FarmenActive = true;
            ForschenActive = true;
            BauenActive = true;
            RekrutierenActive = true;
            FarmTruppenTemplate = new FarmenTruppenTemplate();
            Dorfinformationen = new DorfinformationenModul.Dorfinformationen();
            Bauschleife = new AsyncObservableCollection<Bauelement>();
            BindingOperations.EnableCollectionSynchronization(Bauschleife, BauschleifeLock);
            Rekrutieren = new TruppenTemplate();
            ReservedUnits = new TruppenTemplate();
            BalanceForschenActive = true;
            Handel = new ConcurrentBag<PremiumDepotRessource> {new PremiumDepotRessource {Ressource = "Wood", Priority = 0}, new PremiumDepotRessource {Ressource = "Stone", Priority = 0}, new PremiumDepotRessource {Ressource = "Iron", Priority = 0}};
            Empfangen = new ConcurrentBag<PremiumDepotRessource> {new PremiumDepotRessource {Ressource = "Wood", Priority = 0}, new PremiumDepotRessource {Ressource = "Stone", Priority = 0}, new PremiumDepotRessource {Ressource = "Iron", Priority = 0}};
            Forschenschleife = new AsyncObservableCollection<PremiumDepotRessource> {
                                                                                        new PremiumDepotRessource { Ressource = Truppeb.Speertraeger, Priority = 0 },
                                                                                        new PremiumDepotRessource { Ressource = Truppeb.Schwertkaempfer, Priority = 1 },
                                                                                        new PremiumDepotRessource { Ressource = Truppeb.Axtkaempfer, Priority = 2 },
                                                                                        new PremiumDepotRessource { Ressource = Truppeb.Bogenschuetzen, Priority = 3},
                                                                                        new PremiumDepotRessource { Ressource = Truppeb.Spaeher, Priority = 4},
                                                                                        new PremiumDepotRessource { Ressource = Truppeb.LeichteKavallerie, Priority = 5 },
                                                                                        new PremiumDepotRessource { Ressource = Truppeb.BeritteneBogenschuetzen, Priority = 6 },
                                                                                        new PremiumDepotRessource { Ressource = Truppeb.SchwereKavallerie, Priority = 7 },
                                                                                        new PremiumDepotRessource { Ressource = Truppeb.Rammboecke, Priority = 8 },
                                                                                        new PremiumDepotRessource { Ressource = Truppeb.Katapulte, Priority = 9 }
                                                                                    };
        }

        public AccountDorfSettings(DorfViewModel dorf) {
            BauenMinenAusgleichen = true;
            OverallActive = true;
            UseOverallSettings = false;
            Dorf = dorf;
            FarmenActive = true;
            ForschenActive = true;
            BauenActive = true;
            RekrutierenActive = true;
            FarmTruppenTemplate = new FarmenTruppenTemplate();
            Dorfinformationen = new DorfinformationenModul.Dorfinformationen();
            Bauschleife = new AsyncObservableCollection<Bauelement>();
            BindingOperations.EnableCollectionSynchronization(Bauschleife, BauschleifeLock);
            Rekrutieren = new TruppenTemplate();
            ReservedUnits = new TruppenTemplate();
            TemplateA = new TruppenTemplate {LeichteKavallerieAnzahl = 10};
            TemplateB = new TruppenTemplate {SchwereKavallerieAnzahl = 10};
            BalanceForschenActive = true;
            Handel = new ConcurrentBag<PremiumDepotRessource> {new PremiumDepotRessource {Ressource = "Wood", Priority = 0}, new PremiumDepotRessource {Ressource = "Stone", Priority = 0}, new PremiumDepotRessource {Ressource = "Iron", Priority = 0}};
            Empfangen = new ConcurrentBag<PremiumDepotRessource> {new PremiumDepotRessource {Ressource = "Wood", Priority = 0}, new PremiumDepotRessource {Ressource = "Stone", Priority = 0}, new PremiumDepotRessource {Ressource = "Iron", Priority = 0}};
            Forschenschleife = new AsyncObservableCollection<PremiumDepotRessource> {
                                                                                                                                                                                                                                                                                                                                          new PremiumDepotRessource { Ressource = Truppeb.Speertraeger, Priority = 0 },
                                                                                                                                                                                                                                                                                                                                          new PremiumDepotRessource { Ressource = Truppeb.Schwertkaempfer, Priority = 1 },
                                                                                                                                                                                                                                                                                                                                          new PremiumDepotRessource { Ressource = Truppeb.Axtkaempfer, Priority = 2 },
                                                                                                                                                                                                                                                                                                                                          new PremiumDepotRessource { Ressource = Truppeb.Bogenschuetzen, Priority = 3},
                                                                                                                                                                                                                                                                                                                                          new PremiumDepotRessource { Ressource = Truppeb.Spaeher, Priority = 4},
                                                                                                                                                                                                                                                                                                                                          new PremiumDepotRessource { Ressource = Truppeb.LeichteKavallerie, Priority = 5 },
                                                                                                                                                                                                                                                                                                                                          new PremiumDepotRessource { Ressource = Truppeb.BeritteneBogenschuetzen, Priority = 6 },
                                                                                                                                                                                                                                                                                                                                          new PremiumDepotRessource { Ressource = Truppeb.SchwereKavallerie, Priority = 7 },
                                                                                                                                                                                                                                                                                                                                          new PremiumDepotRessource { Ressource = Truppeb.Rammboecke, Priority = 8 },
                                                                                                                                                                                                                                                                                                                                          new PremiumDepotRessource { Ressource = Truppeb.Katapulte, Priority = 9 }
                                                                                                                                                                                                                                                                                                                                      };
        }

        public int BogenColumnWidth => App.Settings.Weltensettings != null ? (App.Settings.Weltensettings.BogenschuetzenActive ? 30 : 0) : 30;

        public int PaladinColumnWidth => App.Settings.Weltensettings != null ? (App.Settings.Weltensettings.PaladinActive ? 30 : 0) : 30;

        public bool BauenMinenAusgleichen {
            get => _bauenMinenAusgleichen;
            set {
                _bauenMinenAusgleichen = value;
                OnPropertyChanged();
            }
        }

        public int Farmeninterval {
            get => _farmeninterval;
            set {
                _farmeninterval = value;
                OnPropertyChanged();
            }
        }

        public int MinDorfFarmeninterval {
            get => _minDorfFarmeninterval;
            set {
                _minDorfFarmeninterval = value;
                OnPropertyChanged();
            }
        }

        public int FarmOrderNumber {
            get => _farmOrderNumber;
            set {
                _farmOrderNumber = value;
                OnPropertyChanged();
            }
        }

        public string FarmMode {
            get => _farmMode;
            set {
                _farmMode = value;
                OnPropertyChanged();
            }
        }

        public bool UseATemplateFarmassist {
            get => _useATemplateFarmassist;
            set {
                _useATemplateFarmassist = value;
                OnPropertyChanged();
            }
        }

        public bool UseMinDorfFarmeninterval {
            get => _useMinDorfFarmeninterval;
            set {
                _useMinDorfFarmeninterval = value;
                OnPropertyChanged();
            }
        }

        public bool RaubzugActive {
            get => _raubzugActive;
            set {
                _raubzugActive = value;
                OnPropertyChanged();
            }
        }

        public int RaubzugInterval {
            get => _raubzugInterval;
            set {
                _raubzugInterval = value;
                OnPropertyChanged();
            }
        }

        public bool AddFarmsToFarmassistlistAutomatically {
            get => _addFarmsToFarmassistlistAutomatically;
            set {
                _addFarmsToFarmassistlistAutomatically = value;
                OnPropertyChanged();
            }
        }

        public TruppenTemplate TemplateA {
            get => _templateA;
            set {
                _templateA = value;
                OnPropertyChanged();
            }
        }

        public bool UseBTemplateFarmassist {
            get => _useBTemplateFarmassist;
            set {
                _useBTemplateFarmassist = value;
                OnPropertyChanged();
            }
        }

        public TruppenTemplate TemplateB {
            get => _templateB;
            set {
                _templateB = value;
                OnPropertyChanged();
            }
        }

        public bool UseCTemplateFarmassist {
            get => _useCTemplateFarmassist;
            set {
                _useCTemplateFarmassist = value;
                OnPropertyChanged();
            }
        }

        public bool AddVillagesAutomatically {
            get => _addVillagesAutomatically;
            set {
                _addVillagesAutomatically = value;
                OnPropertyChanged();
            }
        }

        public int AddVillagesPointsMin {
            get => _addVillagesPointsMin;
            set {
                _addVillagesPointsMin = value;
                OnPropertyChanged();
            }
        }

        public int AddVillagesPointsMax {
            get => _addVillagesPointsMax;
            set {
                _addVillagesPointsMax = value;
                OnPropertyChanged();
            }
        }

        public double AddVillagesRadiusMin {
            get => _addVillagesRadiusMin;
            set {
                _addVillagesRadiusMin = value;
                OnPropertyChanged();
            }
        }

        public double AddVillagesRadiusMax {
            get => _addVillagesRadiusMax;
            set {
                _addVillagesRadiusMax = value;
                OnPropertyChanged();
            }
        }


        public bool AddPlayersAutomatically {
            get => _addPlayersAutomatically;
            set {
                _addPlayersAutomatically = value;
                OnPropertyChanged();
            }
        }

        public int AddPlayersPointsMin {
            get => _addPlayersPointsMin;
            set {
                _addPlayersPointsMin = value;
                OnPropertyChanged();
            }
        }

        public int AddPlayersPointsMax {
            get => _addPlayersPointsMax;
            set {
                _addPlayersPointsMax = value;
                OnPropertyChanged();
            }
        }

        public int AddPlayersMinInactiveDays {
            get => _addPlayersMinInactiveDays;
            set {
                _addPlayersMinInactiveDays = value;
                OnPropertyChanged();
            }
        }

        public int Baueninterval {
            get => _baueninterval;
            set {
                _baueninterval = value;
                OnPropertyChanged();
            }
        }

        public int Rekrutiereninterval {
            get => _rekrutiereninterval;
            set {
                _rekrutiereninterval = value;
                OnPropertyChanged();
            }
        }

        public int Attackdetectioninterval {
            get => _attackdetectioninterval;
            set {
                _attackdetectioninterval = value;
                OnPropertyChanged();
            }
        }

        public int AttackDodginginterval {
            get => _attackDodginginterval;
            set {
                _attackDodginginterval = value;
                OnPropertyChanged();
            }
        }

        public int AttackPreventioninterval {
            get => _attackPreventioninterval;
            set {
                _attackPreventioninterval = value;
                OnPropertyChanged();
            }
        }

        public int Handelninterval {
            get => _handelninterval;
            set {
                _handelninterval = value;
                OnPropertyChanged();
            }
        }

        public int Angreifeninterval {
            get => _angreifeninterval;
            set {
                _angreifeninterval = value;
                OnPropertyChanged();
            }
        }

        public int MassenAngriffeinterval {
            get => _massenAngriffeinterval;
            set {
                _massenAngriffeinterval = value;
                OnPropertyChanged();
            }
        }

        public int MinFarmassistDelay {
            get => _minFarmassistDelay;
            set {
                _minFarmassistDelay = value;
                OnPropertyChanged();
            }
        }

        public int MaxFarmassistDelay {
            get => _maxFarmassistDelay;
            set {
                _maxFarmassistDelay = value;
                OnPropertyChanged();
            }
        }

        public bool UseOverallSettings {
            get => _useOverallSettings;
            set {
                _useOverallSettings = value;
                OnPropertyChanged();
            }
        }

        public bool OverallActive {
            get => _overallActive;
            set {
                _overallActive = value;
                OnPropertyChanged();
            }
        }

        public bool FarmenActive {
            get => _farmenActive;
            set {
                _farmenActive = value;
                OnPropertyChanged();
            }
        }

        public bool ForschenActive {
            get => _forschenActive;
            set {
                _forschenActive = value;
                OnPropertyChanged();
            }
        }

        public int Forscheninterval {
            get => _forscheninterval;
            set {
                _forscheninterval = value;
                OnPropertyChanged();
            }
        }

        public bool KatternActive {
            get => _katternActive;
            set {
                _katternActive = value;
                OnPropertyChanged();
            }
        }

        public bool BalanceForschenActive {
            get => _balanceForschenActive;
            set {
                _balanceForschenActive = value;
                OnPropertyChanged();
            }
        }

        public bool BauenActive {
            get => _bauenActive;
            set {
                _bauenActive = value;
                OnPropertyChanged();
            }
        }

        public bool RekrutierenActive {
            get => _rekrutierenActive;
            set {
                _rekrutierenActive = value;
                OnPropertyChanged();
            }
        }

        public bool AttackdetectionActive {
            get => _attackdetectionActive;
            set {
                _attackdetectionActive = value;
                OnPropertyChanged();
            }
        }

        public bool AttackDodgingActive {
            get => _attackDodgingActive;
            set {
                _attackDodgingActive = value;
                OnPropertyChanged();
            }
        }

        public bool AttackPreventionActive {
            get => _attackPreventionActive;
            set {
                _attackPreventionActive = value;
                OnPropertyChanged();
            }
        }

        public bool HandelnActive {
            get => _handelnActive;
            set {
                _handelnActive = value;
                OnPropertyChanged();
            }
        }

        public bool AngreifenActive {
            get => _angreifenActive;
            set {
                _angreifenActive = value;
                OnPropertyChanged();
            }
        }

        public bool MassenAngriffeActive {
            get => _massenAngriffeActive;
            set {
                _massenAngriffeActive = value;
                OnPropertyChanged();
            }
        }

        public DateTime FarmenInactiveFrom {
            get => _farmenInactiveFrom;
            set {
                _farmenInactiveFrom = value;
                OnPropertyChanged();
            }
        }

        public DateTime FarmenInactiveTo {
            get => _farmenInactiveTo;
            set {
                _farmenInactiveTo = value;
                OnPropertyChanged();
            }
        }

        public DateTime FarmenTroopsMustBeHomeFrom {
            get => _farmenTroopsMustBeHomeFrom;
            set {
                _farmenTroopsMustBeHomeFrom = value;
                OnPropertyChanged();
            }
        }

        public DateTime FarmenTroopsMustBeHomeTo {
            get => _farmenTroopsMustBeHomeTo;
            set {
                _farmenTroopsMustBeHomeTo = value;
                OnPropertyChanged();
            }
        }

        public bool ReserveUnits {
            get => _reserveUnits;
            set {
                _reserveUnits = value;
                OnPropertyChanged();
            }
        }

        public TruppenTemplate ReservedUnits {
            get => _reservedUnits;
            set {
                _reservedUnits = value;
                OnPropertyChanged();
            }
        }

        public AsyncObservableCollection<Bauelement> Bauschleife { get; set; }
        public AsyncObservableCollection<PremiumDepotRessource> Forschenschleife { get; set; }
        public FarmenTruppenTemplate FarmTruppenTemplate { get; set; }
        public object BauschleifeLock { get; set; } = new object();
        public object AngriffeLock { get; set; } = new object();

        public TruppenTemplate Rekrutieren {
            get => _rekrutieren;
            set {
                _rekrutieren = value;
                OnPropertyChanged();
            }
        }

        public DorfViewModel Dorf {
            get => _dorf;
            set {
                _dorf = value;
                OnPropertyChanged();
            }
        }

        public string ZugehoerigerAccountname {
            get => _zugehoerigerAccountname;
            set {
                _zugehoerigerAccountname = value;
                OnPropertyChanged();
            }
        }

        public DorfinformationenModul.Dorfinformationen Dorfinformationen {
            get => _dorfinformationen;
            set {
                _dorfinformationen = value;
                OnPropertyChanged();
            }
        }

        public DateTime NextAvailableTroopAtHomeAt {
            get => _nextAvailableTroopAtHomeAt;
            set {
                _nextAvailableTroopAtHomeAt = value;
                OnPropertyChanged();
            }
        }

        public DateTime NextAvailableBauschleife {
            get => _nextAvailableBauschleife;
            set {
                _nextAvailableBauschleife = value;
                OnPropertyChanged();
            }
        }

        public int MuenzenPraegen {
            get => _muenzenPraegen;
            set {
                _muenzenPraegen = value;
                OnPropertyChanged();
            }
        }

        public int PremiumDepotinterval {
            get => _premiumDepotinterval;
            set {
                _premiumDepotinterval = value;
                OnPropertyChanged();
            }
        }

        public string PremiumDepotHandelArt {
            get => _premiumDepotHandelArt;
            set {
                _premiumDepotHandelArt = value;
                OnPropertyChanged();
            }
        }

        public double PremiumDepotHandelMaxVerhaeltnis {
            get => _premiumDepotHandelMaxVerhaeltnis;
            set {
                _premiumDepotHandelMaxVerhaeltnis = value;
                OnPropertyChanged();
            }
        }

        public double PremiumDepotHandelMinVerhaeltnis {
            get => _premiumDepotHandelMinVerhaeltnis;
            set {
                _premiumDepotHandelMinVerhaeltnis = value;
                OnPropertyChanged();
            }
        }

        public double HandelMinRessourcen {
            get => _handelMinRessourcen;
            set {
                _handelMinRessourcen = value;
                OnPropertyChanged();
            }
        }

        public ConcurrentBag<PremiumDepotRessource> Handel {
            get => _handel;
            set {
                _handel = value;
                OnPropertyChanged();
            }
        }

        public ConcurrentBag<PremiumDepotRessource> Empfangen {
            get => _empfangen;
            set {
                _empfangen = value;
                OnPropertyChanged();
            }
        }

        public bool PremiumDepotQueued {
            get => _premiumDepotQueued;
            set {
                _premiumDepotQueued = value;
                OnPropertyChanged();
            }
        }

        public int PremiumDepotSellPercentage {
            get => _premiumDepotSellPercentage;
            set {
                _premiumDepotSellPercentage = value;
                OnPropertyChanged();
            }
        }

        public int NotUnderPremiumPunkte {
            get => _notUnderPremiumPunkte;
            set {
                _notUnderPremiumPunkte = value;
                OnPropertyChanged();
            }
        }

        public int BauenHolzUebrigLassen {
            get => _bauenHolzUebrigLassen;
            set {
                _bauenHolzUebrigLassen = value;
                OnPropertyChanged();
            }
        }

        public int BauenLehmUebrigLassen {
            get => _bauenLehmUebrigLassen;
            set {
                _bauenLehmUebrigLassen = value;
                OnPropertyChanged();
            }
        }

        public int BauenEisenUebrigLassen {
            get => _bauenEisenUebrigLassen;
            set {
                _bauenEisenUebrigLassen = value;
                OnPropertyChanged();
            }
        }

        public int RekrutierenHolzUebrigLassen {
            get => _rekrutierenHolzUebrigLassen;
            set {
                _rekrutierenHolzUebrigLassen = value;
                OnPropertyChanged();
            }
        }

        public int RekrutierenLehmUebrigLassen {
            get => _rekrutierenLehmUebrigLassen;
            set {
                _rekrutierenLehmUebrigLassen = value;
                OnPropertyChanged();
            }
        }

        public int RekrutierenEisenUebrigLassen {
            get => _rekrutierenEisenUebrigLassen;
            set {
                _rekrutierenEisenUebrigLassen = value;
                OnPropertyChanged();
            }
        }

        public HandelVerschickArt HandelVerschickenArt {
            get => _handelVerschickenArt;
            set {
                _handelVerschickenArt = value;
                OnPropertyChanged();
            }
        }

        public HandelEmpfangArt HandelEmpfangenArt {
            get => _handelEmpfangenArt;
            set {
                _handelEmpfangenArt = value;
                OnPropertyChanged();
            }
        }

        public int HandelVerschickenHolzUeber {
            get => _handelVerschickenHolzUeber;
            set {
                _handelVerschickenHolzUeber = value;
                OnPropertyChanged();
            }
        }

        public int HandelVerschickenLehmUeber {
            get => _handelVerschickenLehmUeber;
            set {
                _handelVerschickenLehmUeber = value;
                OnPropertyChanged();
            }
        }

        public int HandelVerschickenEisenUeber {
            get => _handelVerschickenEisenUeber;
            set {
                _handelVerschickenEisenUeber = value;
                OnPropertyChanged();
            }
        }

        public int HandelEmpfangenHolzUnter {
            get => _handelEmpfangenHolzUnter;
            set {
                _handelEmpfangenHolzUnter = value;
                OnPropertyChanged();
            }
        }

        public int HandelEmpfangenLehmUnter {
            get => _handelEmpfangenLehmUnter;
            set {
                _handelEmpfangenLehmUnter = value;
                OnPropertyChanged();
            }
        }

        public int HandelEmpfangenEisenUnter {
            get => _handelEmpfangenEisenUnter;
            set {
                _handelEmpfangenEisenUnter = value;
                OnPropertyChanged();
            }
        }

        public int HandelMaxLaufzeit {
            get => _handelMaxLaufzeit;
            set {
                _handelMaxLaufzeit = value;
                OnPropertyChanged();
            }
        }

        public int HandelHaendlerZurueckHalten {
            get => _handelHaendlerZurueckHalten;
            set {
                _handelHaendlerZurueckHalten = value;
                OnPropertyChanged();
            }
        }

        public DateTime LastTruppeneinlesen {
            get => _lastTruppeneinlesen;
            set {
                _lastTruppeneinlesen = value;
                OnPropertyChanged();
            }
        }

        public DateTime LastUnterstuetzungeneinlesen {
            get => _lastUnterstuetzungeneinlesen;
            set {
                _lastUnterstuetzungeneinlesen = value;
                OnPropertyChanged();
            }
        }

        public int PremiumDepotStayMinutes {
            get => _premiumDepotStayMinutes;
            set {
                _premiumDepotStayMinutes = value;
                OnPropertyChanged();
            }
        }
        public double KasernePercent {
            get => _kasernePercent;
            set {
                _kasernePercent = value;
                OnPropertyChanged();
            }
        }
        public double StallPercent {
            get => _stallPercent;
            set {
                _stallPercent = value;
                OnPropertyChanged();
            }
        }
        public double WerkstattPercent {
            get => _werkstattPercent;
            set {
                _werkstattPercent = value;
                OnPropertyChanged();
            }
        }

        public int KaserneMaxHours {
            get => _kaserneMaxHours;
            set {
                _kaserneMaxHours = value;
                OnPropertyChanged();
            }
        }
        public int StallMaxHours {
            get => _stallMaxHours;
            set {
                _stallMaxHours = value;
                OnPropertyChanged();
            }
        }
        public int WerkstattMaxHours {
            get => _werkstattMaxHours;
            set {
                _werkstattMaxHours = value;
                OnPropertyChanged();
            }
        }

        public string HandelRouteCoords {
            get => _handelRouteCoords;
            set {
                _handelRouteCoords = value;
                OnPropertyChanged();
            }
        }

        public bool BhAutomatischBauenActive {
            get => _bhAutomatischBauenActive;
            set {
                _bhAutomatischBauenActive = value;
                OnPropertyChanged();
            }
        }

        public bool SpeicherAutomatischBauenActive {
            get => _speicherAutomatischBauenActive;
            set {
                _speicherAutomatischBauenActive = value;
                OnPropertyChanged();
            }
        }
        
        public event PropertyChangedEventHandler PropertyChanged;

        public AccountDorfSettings Clone() {
            var newAccountDorfSettings = new AccountDorfSettings {UseMinDorfFarmeninterval = UseMinDorfFarmeninterval,BauenMinenAusgleichen = BauenMinenAusgleichen, AngreifenActive = AngreifenActive, MassenAngriffeinterval = MassenAngriffeinterval, MassenAngriffeActive = MassenAngriffeActive, Angreifeninterval = Angreifeninterval, AttackDodgingActive = AttackDodgingActive, AttackDodginginterval = AttackDodginginterval, AttackPreventioninterval = AttackPreventioninterval, AttackPreventionActive = AttackPreventionActive, Attackdetectioninterval = Attackdetectioninterval, AttackdetectionActive = AttackdetectionActive, Baueninterval = Baueninterval, BauenActive = BauenActive, FarmenActive = FarmenActive, Farmeninterval = Farmeninterval, Handelninterval = Handelninterval, HandelnActive = HandelnActive, NextAvailableBauschleife = NextAvailableBauschleife, NextAvailableTroopAtHomeAt = NextAvailableTroopAtHomeAt, OverallActive = OverallActive, UseOverallSettings = UseOverallSettings, Rekrutiereninterval = Rekrutiereninterval, RekrutierenActive = RekrutierenActive, Bauschleife = new AsyncObservableCollection<Bauelement>(), Dorf = Dorf.Clone(), ZugehoerigerAccountname = ZugehoerigerAccountname, FarmTruppenTemplate = FarmTruppenTemplate.Clone(), Rekrutieren = Rekrutieren.Clone(), Dorfinformationen = Dorfinformationen?.Clone(), FarmenInactiveFrom = FarmenInactiveFrom, FarmenInactiveTo = FarmenInactiveTo, FarmenTroopsMustBeHomeFrom = FarmenTroopsMustBeHomeFrom, FarmenTroopsMustBeHomeTo = FarmenTroopsMustBeHomeTo, MinDorfFarmeninterval = MinDorfFarmeninterval, KatternActive = KatternActive, FarmMode = FarmMode, UseATemplateFarmassist = UseATemplateFarmassist, UseBTemplateFarmassist = UseBTemplateFarmassist, UseCTemplateFarmassist = UseCTemplateFarmassist, TemplateA = TemplateA, TemplateB = TemplateB, MuenzenPraegen = MuenzenPraegen, MinFarmassistDelay = MinFarmassistDelay, MaxFarmassistDelay = MaxFarmassistDelay, ReservedUnits = ReservedUnits.Clone(), ReserveUnits = ReserveUnits, RekrutierenLehmUebrigLassen = RekrutierenLehmUebrigLassen, RekrutierenEisenUebrigLassen = RekrutierenEisenUebrigLassen, RekrutierenHolzUebrigLassen = RekrutierenHolzUebrigLassen, AddPlayersAutomatically = AddPlayersAutomatically, AddVillagesRadiusMin = AddVillagesRadiusMin, AddVillagesPointsMin = AddVillagesPointsMin, AddPlayersPointsMax = AddPlayersPointsMax, AddVillagesRadiusMax = AddVillagesRadiusMax, AddVillagesAutomatically = AddVillagesAutomatically, AddPlayersPointsMin = AddPlayersPointsMin, AddVillagesPointsMax = AddVillagesPointsMax, BauenEisenUebrigLassen = BauenEisenUebrigLassen, BauenHolzUebrigLassen = BauenHolzUebrigLassen, BauenLehmUebrigLassen = BauenLehmUebrigLassen, NotUnderPremiumPunkte = NotUnderPremiumPunkte, Handel = new ConcurrentBag<PremiumDepotRessource>(), PremiumDepotHandelArt = PremiumDepotHandelArt, PremiumDepotHandelMaxVerhaeltnis = PremiumDepotHandelMaxVerhaeltnis, HandelMinRessourcen = HandelMinRessourcen, PremiumDepotHandelMinVerhaeltnis = PremiumDepotHandelMinVerhaeltnis, PremiumDepotSellPercentage = PremiumDepotSellPercentage, PremiumDepotinterval = PremiumDepotinterval, AddPlayersMinInactiveDays = AddPlayersMinInactiveDays, HandelEmpfangenArt = HandelEmpfangenArt, HandelEmpfangenEisenUnter = HandelEmpfangenEisenUnter, HandelEmpfangenHolzUnter = HandelEmpfangenHolzUnter, HandelEmpfangenLehmUnter = HandelEmpfangenLehmUnter, HandelMaxLaufzeit = HandelMaxLaufzeit, HandelVerschickenArt = HandelVerschickenArt, HandelVerschickenEisenUeber = HandelVerschickenEisenUeber, HandelVerschickenHolzUeber = HandelVerschickenHolzUeber, HandelVerschickenLehmUeber = HandelVerschickenLehmUeber, HandelHaendlerZurueckHalten = HandelHaendlerZurueckHalten, Empfangen = new ConcurrentBag<PremiumDepotRessource>(), HandelRouteCoords = HandelRouteCoords, PremiumDepotStayMinutes = PremiumDepotStayMinutes, LastTruppeneinlesen = LastTruppeneinlesen, LastUnterstuetzungeneinlesen = LastUnterstuetzungeneinlesen, BhAutomatischBauenActive = BhAutomatischBauenActive, SpeicherAutomatischBauenActive = SpeicherAutomatischBauenActive, FarmOrderNumber = FarmOrderNumber, ForschenActive = ForschenActive, Forscheninterval = Forscheninterval, Forschenschleife = new AsyncObservableCollection<PremiumDepotRessource>(), BalanceForschenActive = BalanceForschenActive, KasernePercent = KasernePercent, StallPercent = StallPercent, WerkstattPercent = WerkstattPercent, AddFarmsToFarmassistlistAutomatically = AddFarmsToFarmassistlistAutomatically, KaserneMaxHours = KaserneMaxHours, StallMaxHours = StallMaxHours, WerkstattMaxHours = WerkstattMaxHours};
            if (Handel != null)
                foreach (var handel in Handel)
                    newAccountDorfSettings.Handel.Add(handel.Clone());
            if (Empfangen != null)
                foreach (var handel in Empfangen)
                    newAccountDorfSettings.Empfangen.Add(handel.Clone());
            foreach (var bauelement in Bauschleife)
                newAccountDorfSettings.Bauschleife.Add(bauelement.Clone());
            foreach (var forschen in Forschenschleife)
                newAccountDorfSettings.Forschenschleife.Add(forschen.Clone());
            return newAccountDorfSettings;
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
