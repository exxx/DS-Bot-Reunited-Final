﻿using System;

namespace Ds_Bot_Reunited_NEWUI.Settings {
    [Serializable]
    public class WeltenSettings {
        public bool BogenschuetzenActive { get; set; }
        public bool PaladinActive { get; set; }
        public bool MilizActive { get; set; }
        public bool MillisecondsActive { get; set; }
        public bool VerkuerzenActive { get; set; }
        public bool FakeActive { get; set; }
        public int NachtbonusVon { get; set; }
        public int NachtbonusBis { get; set; }
        public int Angriffsschutztage { get; set; }
        public double Spielgeschwindigkeit { get; set; }
        public double RessourcenProductionFaktor { get; set; }
        public double SpeertraegerLaufzeit { get; set; }
        public double SchwertkaempferLaufzeit { get; set; }
        public double AxtkaempferLaufzeit { get; set; }
        public double BogenschuetzenLaufzeit { get; set; }
        public double SpaeherLaufzeit { get; set; }
        public double LeichteKavallerieLaufzeit { get; set; }
        public double BeritteneBogenschuetzenLaufzeit { get; set; }
        public double SchwereKavallerieLaufzeit { get; set; }
        public double RammboeckeLaufzeit { get; set; }
        public double KatapulteLaufzeit { get; set; }
        public double PaladinLaufzeit { get; set; }
        public double AdelsgeschlechtLaufzeit { get; set; }
        public int SpeertraegerBauzeit { get; set; }
        public int SchwertkaempferBauzeit { get; set; }
        public int AxtkaempferBauzeit { get; set; }
        public int BogenschuetzenBauzeit { get; set; }
        public int SpaeherBauzeit { get; set; }
        public int LeichteKavallerieBauzeit { get; set; }
        public int BeritteneBogenschuetzenBauzeit { get; set; }
        public int SchwereKavallerieBauzeit { get; set; }
        public int RammboeckeBauzeit { get; set; }
        public int KatapulteBauzeit { get; set; }
        public int PaladinBauzeit { get; set; }
        public int AdelsgeschlechtBauzeit { get; set; }
        public bool AhTillLevel3 { get; set; }
        public bool KircheActive { get; set; }
        public bool WatchtowerActive { get; set; }
        public bool TenTechLevel { get; set; }

        public WeltenSettings() { }

        public WeltenSettings Clone() {
            WeltenSettings newWeltenSettings = new WeltenSettings {AdelsgeschlechtLaufzeit = AdelsgeschlechtLaufzeit, AxtkaempferLaufzeit = AxtkaempferLaufzeit, BeritteneBogenschuetzenLaufzeit = BeritteneBogenschuetzenLaufzeit, BogenschuetzenActive = BogenschuetzenActive, BogenschuetzenLaufzeit = BogenschuetzenLaufzeit, KatapulteLaufzeit = KatapulteLaufzeit, LeichteKavallerieLaufzeit = LeichteKavallerieLaufzeit, MilizActive = MilizActive, NachtbonusBis = NachtbonusBis, NachtbonusVon = NachtbonusVon, PaladinActive = PaladinActive, PaladinLaufzeit = PaladinLaufzeit, RammboeckeLaufzeit = RammboeckeLaufzeit, SchwereKavallerieLaufzeit = SchwereKavallerieLaufzeit, SchwertkaempferLaufzeit = SchwertkaempferLaufzeit, SpaeherLaufzeit = SpaeherLaufzeit, SpeertraegerLaufzeit = SpeertraegerLaufzeit, Spielgeschwindigkeit = Spielgeschwindigkeit, RessourcenProductionFaktor = RessourcenProductionFaktor, VerkuerzenActive = VerkuerzenActive, MillisecondsActive = MillisecondsActive, FakeActive = FakeActive, AhTillLevel3 = AhTillLevel3, KircheActive = KircheActive, WatchtowerActive = WatchtowerActive, TenTechLevel = TenTechLevel, Angriffsschutztage = Angriffsschutztage, SpeertraegerBauzeit = SpeertraegerBauzeit, SchwertkaempferBauzeit = SchwertkaempferBauzeit, AxtkaempferBauzeit = AxtkaempferBauzeit, BogenschuetzenBauzeit = BogenschuetzenBauzeit, SpaeherBauzeit = SpaeherBauzeit, LeichteKavallerieBauzeit = LeichteKavallerieBauzeit, BeritteneBogenschuetzenBauzeit = BeritteneBogenschuetzenBauzeit, SchwereKavallerieBauzeit = SchwereKavallerieBauzeit, RammboeckeBauzeit = RammboeckeBauzeit, KatapulteBauzeit = KatapulteBauzeit, PaladinBauzeit = PaladinBauzeit, AdelsgeschlechtBauzeit = AdelsgeschlechtBauzeit};
            return newWeltenSettings;
        }
    }
}
