﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Windows.Data;
using Ds_Bot_Reunited_NEWUI.Accountsettings.Ds_Bot_Reunited.Accountsettings;
using Ds_Bot_Reunited_NEWUI.Annotations;
using Ds_Bot_Reunited_NEWUI.Constants;
using Ds_Bot_Reunited_NEWUI.DefendModul;
using Ds_Bot_Reunited_NEWUI.Farmen;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.MultiBewegungen;
using Ds_Bot_Reunited_NEWUI.Properties;
using Ds_Bot_Reunited_NEWUI.Rekrutieren;
using Ds_Bot_Reunited_NEWUI.Spieldaten;

namespace Ds_Bot_Reunited_NEWUI.Settings {
    [Obfuscation(Exclude = true, Feature = "renaming")]
    public class Settings : INotifyPropertyChanged {
        private int _difference;
        private DateTime _lastTimeSynchronisation;
        private string _lizenzGueltig;
        private int _maxActionDelay;
        private int _minActionDelay;
        private int _randomIntervalMax;
        private int _randomIntervalMin;
        private int _synchronizeMaxBefehleCount;
        private int _synchronizeTroopCount;
        private double _timeDifferenceByMachinePerMinute;
        private string _useTroopForSynchronize;
        private int _preparationtime;
        private string _twoCaptchaApiKey;

        public Settings() {
            Weltensettings = new WeltenSettings();
            Accountsettingslist = new AsyncObservableCollection<Accountsetting> {new Accountsetting()};
            ViewSettings = new ViewSettings();
            Ziele = new AsyncObservableCollection<MultiBewegungsziel>();
            BindingOperations.EnableCollectionSynchronization(Accountsettingslist, AccountsettingslistLock);
            BindingOperations.EnableCollectionSynchronization(Ziele, ZieleLock);
            Botmode = Ds_Bot_Reunited_NEWUI.Botmode.Botmode.Performance;
            Sprachversion = Sprache.Sprache.English;
            MinActionDelay = 50;
            MaxActionDelay = 100;
            SynchronizeTroopCount = 1;
            UseTroopForSynchronize = Truppeb.Speertraeger;
            MaxBuildingqueue = 2;
            Gruppen = new List<GruppeDoerfer>();
            TabbingTemplates = new AsyncObservableCollection<TruppenTemplate> {new TruppenTemplate {TemplateName = Resources.Tabbing, SpeertraegerAnzahl = 50, SchwertkaempferAnzahl = 50}};
            DefendingTemplates = new AsyncObservableCollection<TruppenTemplate> {
                                                                                    new TruppenTemplate {TemplateName = Resources.Defending, SpeertraegerAnzahl = 4000, SchwertkaempferAnzahl = 4000},
                                                                                    new TruppenTemplate {TemplateName = Resources.DefendMinTemplate, SpeertraegerAnzahl = 50, SchwertkaempferAnzahl = 50},
                                                                                    new TruppenTemplate {TemplateName = Resources.DefendMaxTemplate, SpeertraegerAnzahl = 500, SchwertkaempferAnzahl = 500}
                                                                                };
            RetimeTemplates = new AsyncObservableCollection<TruppenTemplate> {new TruppenTemplate {TemplateName = Resources.Retiming, AxtkaempferAnzahl = 5000, LeichteKavallerieAnzahl = 2500, RammboeckeAnzahl = 300}};
            HoldBackTemplates = new AsyncObservableCollection<TruppenTemplate> {new TruppenTemplate {TemplateName = Resources.HoldBackTroops}};
            BindingOperations.EnableCollectionSynchronization(HoldBackTemplates, HoldBackTemplatesLock);
            BindingOperations.EnableCollectionSynchronization(TabbingTemplates, TabbingTemplatesLock);
            BindingOperations.EnableCollectionSynchronization(DefendingTemplates, DefendingTemplatesLock);
            BindingOperations.EnableCollectionSynchronization(RetimeTemplates, RetimeTemplatesLock);
            DefendArt = DefendArt.Defensive;
            DetectActive = false;
            RenameActive = true;
            PreventActive = false;
            SendouttroopsActive = true;
            NeighbourMaxRadius = 15;
            RetimeActive = false;
            RetimeMaxLaufzeit = 0;
            UpdateUnitsinterval = 120;
            EmptyRessourcesActive = true;
            RecruitToEmptyRessourcesActive = false;
            BuildToEmptyRessourcesActive = true;
            MaxConcurrentWindowsOpen = 5;
            AttackdetectionInterval = 20;
            MultiaccountingActive = true;
            EinzelaccountingActive = true;
            SynchronizeMaxBefehleCount = 50;
            Preparationtime = 30;
        }

        public List<GruppeDoerfer> Gruppen { get; set; }

        public string OperateLink => "https://" + App.Settings.Welt + "." + App.Settings.Server.GetDescription() + "/game.php?";

        public int Difference {
            get => _difference;
            set {
                _difference = value;
                OnPropertyChanged();
            }
        }

        public string LizenzGueltig {
            get => _lizenzGueltig;
            set {
                _lizenzGueltig = value;
                OnPropertyChanged();
            }
        }

        public string TwoCaptchaApiKey {
	        get => _twoCaptchaApiKey;
	        set {
		        _twoCaptchaApiKey = value;
		        OnPropertyChanged();
	        }
        }

        public int MinActionDelay {
            get => _minActionDelay;
            set {
                _minActionDelay = value;
                OnPropertyChanged();
            }
        }

        public int MaxActionDelay {
            get => _maxActionDelay;
            set {
                _maxActionDelay = value;
                OnPropertyChanged();
            }
        }

        public int RandomIntervalMax {
            get => _randomIntervalMax;
            set {
                _randomIntervalMax = value;
                OnPropertyChanged();
            }
        }

        public int RandomIntervalMin {
            get => _randomIntervalMin;
            set {
                _randomIntervalMin = value;
                OnPropertyChanged();
            }
        }

        public string UseTroopForSynchronize {
            get => _useTroopForSynchronize;
            set {
                _useTroopForSynchronize = value;
                OnPropertyChanged();
            }
        }

        public int SynchronizeTroopCount {
            get => _synchronizeTroopCount;
            set {
                _synchronizeTroopCount = value;
                OnPropertyChanged();
            }
        }

        public int SynchronizeMaxBefehleCount {
            get => _synchronizeMaxBefehleCount;
            set {
                _synchronizeMaxBefehleCount = value;
                OnPropertyChanged();
            }
        }

        public double TimeDifferenceByMachinePerMinute {
            get => _timeDifferenceByMachinePerMinute;
            set {
                _timeDifferenceByMachinePerMinute = value;
                OnPropertyChanged();
            }
        }

        public int Preparationtime {
            get => _preparationtime;
            set {
                _preparationtime = value;
                OnPropertyChanged();
            }
        }
        
        public double GetMsToAddSinceLastSynchronize => LastTimeSynchronisation != DateTime.MinValue ? (DateTime.Now - LastTimeSynchronisation).TotalMinutes * TimeDifferenceByMachinePerMinute : 0;

        public DateTime LastTimeSynchronisation {
            get => _lastTimeSynchronisation;
            set {
                _lastTimeSynchronisation = value;
                OnPropertyChanged();
            }
        }

        public string Accountname { get; set; }
        public string Passwort { get; set; }
        public bool MinimizeToTray { get; set; } = true;
        public SERVERCONSTANTS Server { get; set; }
        public string Welt { get; set; }
        public Sprache.Sprache Sprachversion { get; set; }
        public Botmode.Botmode Botmode { get; set; }
        public int MaxConcurrentWindowsOpen { get; set; }

        public WeltenSettings Weltensettings { get; set; }
        public AsyncObservableCollection<MultiBewegungsziel> Ziele { get; set; }

        public DateTime LastWorldDataCheck { get; set; }

        public AsyncObservableCollection<Accountsetting> Accountsettingslist { get; set; }

        public Accountsetting Selectedaccount { get; set; }
        public ViewSettings ViewSettings { get; set; }
        public int MaxBuildingqueue { get; set; }

        public object AccountsettingslistLock { get; set; } = new object();
        public object ZieleLock { get; set; } = new object();

        public DefendArt DefendArt { get; set; }
        public bool DetectActive { get; set; }
        public bool RenameActive { get; set; }
        public bool PreventActive { get; set; }
        public bool SendouttroopsActive { get; set; }
        public int NeighbourMaxRadius { get; set; }
        public bool RetimeActive { get; set; }
        public int RetimeMaxLaufzeit { get; set; }
        public int UpdateUnitsinterval { get; set; }
        public bool EmptyRessourcesActive { get; set; }
        public bool RecruitToEmptyRessourcesActive { get; set; }
        public bool BuildToEmptyRessourcesActive { get; set; }
        public AsyncObservableCollection<TruppenTemplate> TabbingTemplates { get; set; }
        public AsyncObservableCollection<TruppenTemplate> DefendingTemplates { get; set; }
        public AsyncObservableCollection<TruppenTemplate> RetimeTemplates { get; set; }
        public AsyncObservableCollection<TruppenTemplate> HoldBackTemplates { get; set; }
        public object TabbingTemplatesLock { get; set; } = new object();
        public object DefendingTemplatesLock { get; set; } = new object();
        public object RetimeTemplatesLock { get; set; } = new object();
        public object HoldBackTemplatesLock { get; set; } = new object();
        public DateTime LastAttackdetectionZeitpunkt { get; set; }
        public int AttackdetectionInterval { get; set; }
        public bool MultiaccountingActive { get; set; }
        public bool EinzelaccountingActive { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public Settings Clone() {
            var fehler = true;
            while (fehler) {
                fehler = false;
                try {
                    var newsetting = new Settings {
                                                      Welt = Welt,
                                                      EinzelaccountingActive = EinzelaccountingActive,
                                                      MultiaccountingActive = MultiaccountingActive,
                                                      Weltensettings = Weltensettings?.Clone(),
                                                      LastWorldDataCheck = LastWorldDataCheck,
                                                      Accountsettingslist = new AsyncObservableCollection<Accountsetting>(),
                                                      Selectedaccount = Selectedaccount?.Clone(),
                                                      ViewSettings = ViewSettings?.Clone(),
                                                      Ziele = new AsyncObservableCollection<MultiBewegungsziel>(),
                                                      Sprachversion = Sprachversion,
                                                      Botmode = Botmode,
                                                      MinimizeToTray = MinimizeToTray,
                                                      Passwort = Passwort,
                                                      Server = Server,
                                                      Accountname = Accountname,
                                                      Difference = Difference,
                                                      RandomIntervalMax = RandomIntervalMax,
                                                      RandomIntervalMin = RandomIntervalMin,
                                                      MinActionDelay = MinActionDelay,
                                                      MaxActionDelay = MaxActionDelay,
                                                      SynchronizeTroopCount = SynchronizeTroopCount,
                                                      UseTroopForSynchronize = UseTroopForSynchronize,
                                                      LastTimeSynchronisation = LastTimeSynchronisation,
                                                      MaxBuildingqueue = MaxBuildingqueue,
                                                      Gruppen = new List<GruppeDoerfer>(),
                                                      DefendingTemplates = new AsyncObservableCollection<TruppenTemplate>(),
                                                      RetimeTemplates = new AsyncObservableCollection<TruppenTemplate>(),
                                                      TabbingTemplates = new AsyncObservableCollection<TruppenTemplate>(),
                                                      BuildToEmptyRessourcesActive = BuildToEmptyRessourcesActive,
                                                      DefendArt = DefendArt,
                                                      DetectActive = DetectActive,
                                                      EmptyRessourcesActive = EmptyRessourcesActive,
                                                      NeighbourMaxRadius = NeighbourMaxRadius,
                                                      PreventActive = PreventActive,
                                                      RecruitToEmptyRessourcesActive = RecruitToEmptyRessourcesActive,
                                                      RenameActive = RenameActive,
                                                      RetimeActive = RetimeActive,
                                                      RetimeMaxLaufzeit = RetimeMaxLaufzeit,
                                                      SendouttroopsActive = SendouttroopsActive,
                                                      UpdateUnitsinterval = UpdateUnitsinterval,
                                                      LastAttackdetectionZeitpunkt = LastAttackdetectionZeitpunkt,
                                                      HoldBackTemplates = new AsyncObservableCollection<TruppenTemplate>(),
                                                      MaxConcurrentWindowsOpen = MaxConcurrentWindowsOpen,
                                                      AttackdetectionInterval = AttackdetectionInterval,
                                                      SynchronizeMaxBefehleCount = SynchronizeMaxBefehleCount,
                                                      TimeDifferenceByMachinePerMinute = TimeDifferenceByMachinePerMinute,
                                                      Preparationtime = Preparationtime
                    };
                    for (var i = 0; i < Accountsettingslist.Count; i++)
                        newsetting.Accountsettingslist.Add(Accountsettingslist[i].Clone());
                    for (var i = 0; i < Ziele.Count; i++)
                        newsetting.Ziele.Add(Ziele[i].Clone());

                    foreach (var gruppe in Gruppen)
                        newsetting.Gruppen.Add(gruppe.Clone());

                    foreach (var tab in TabbingTemplates)
                        newsetting.TabbingTemplates.Add(tab.Clone());

                    foreach (var tab in DefendingTemplates)
                        newsetting.DefendingTemplates.Add(tab.Clone());
                    foreach (var tab in RetimeTemplates)
                        newsetting.RetimeTemplates.Add(tab.Clone());
                    foreach (var tab in HoldBackTemplates)
                        newsetting.HoldBackTemplates.Add(tab.Clone());
                    return newsetting;
                } catch {
                    fehler = true;
                }
            }
            return null;
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
