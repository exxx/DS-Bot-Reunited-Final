﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Ds_Bot_Reunited_NEWUI.Accountsettings.Ds_Bot_Reunited.Accountsettings;
using Ds_Bot_Reunited_NEWUI.Angreifensettings;
using Ds_Bot_Reunited_NEWUI.Botmessages;
using Ds_Bot_Reunited_NEWUI.BrowserManager;
using Ds_Bot_Reunited_NEWUI.Constants;
using Ds_Bot_Reunited_NEWUI.Farmen;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.Properties;
using Ds_Bot_Reunited_NEWUI.Settings;
using Ds_Bot_Reunited_NEWUI.SpieldatenViewModel;
using OpenQA.Selenium;

namespace Ds_Bot_Reunited_NEWUI.AttackDetectionModul {
	public class AttackDetectionModul {
		public static async Task GetAttacks(Browser browser, Accountsetting accountsettings) {
			App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
				               ? accountsettings.Uvname
				               : accountsettings.Accountname) + ": " + " Attack Detection Account started");
			await Rndm.Sleep();
			if (browser.WebDriver.ElementExists(By.XPath(SpielOberflaeche
			                                             .Uebersicht.Angriffe.ANGRIFFECOUNTER_SELECTOR_XPATH)) ||
			    browser.WebDriver.ElementExists(By.XPath(SpielOberflaeche
			                                             .Uebersicht.Angriffe.ANGRIFFZEICHEN_SELECTOR_XPATH))) {
				string link1 = App.Settings.OperateLink + "screen=overview_villages" +
				               accountsettings.UvZusatzFuerLink +
				               "&mode=incomings&type=all&subtype=attacks&page=-1&group=0";
				if (!browser.WebDriver.Url.Equals(link1)) {
					browser.WebDriver.Navigate().GoToUrl(link1);
					await Rndm.Sleep(500, 800);
				}

				if (!browser.WebDriver.ElementExists(By.XPath("//form[@id='incomings_form']"))) {
					browser.WebDriver.Navigate().GoToUrl(App.Settings.OperateLink + accountsettings.UvZusatzFuerLink +
					                                     "&screen=overview_villages");

					List<AccountDorfSettings> doerferMitAngriffen = new List<AccountDorfSettings>();

					var list = SeitenModul.GetSeiten(browser.WebDriver);
					if (list.Any()) {
						foreach (var seite in list) {
							try {
								SeitenModul.SetPage(browser, seite);
								List<IWebElement> elements2 = browser
								                              .WebDriver
								                              .FindElements(By.XPath("//a/descendant::img[contains(@src,'command/attack.png')]/parent::*"))
								                              .ToList();
								foreach (var element in elements2) {
									string link = element.GetAttribute("href");
									int indexOfGleich = link.IndexOf("=", StringComparison.Ordinal);
									int indexOfUnd = link.IndexOf("&", StringComparison.Ordinal);
									int id =
										int.Parse(link.Substring(indexOfGleich + 1, indexOfUnd - indexOfGleich - 1));
									var dorf =
										accountsettings
											.DoerferSettings.FirstOrDefault(x => x.Dorf.VillageId.Equals(id));
									if (dorf != null) doerferMitAngriffen.Add(dorf);
								}
							} catch (Exception) {
								// ignored
							}
						}
					} else {
						doerferMitAngriffen.Add(accountsettings.DoerferSettings.First());
					}

					foreach (var dorf in doerferMitAngriffen) {
						await App.WaitForAttacks();
						App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
							               ? accountsettings.Uvname
							               : accountsettings.Accountname) + ": Attack Detection: " +
						              dorf.Dorf.VillageId + " Name: " + dorf.Dorf.Name + " started");
						browser.WebDriver.Navigate()
						       .GoToUrl(App.Settings.OperateLink + "village=" + dorf.Dorf.VillageId +
						                accountsettings.UvZusatzFuerLink + "&screen=overview");
						await Rndm.Sleep(100, 400);
						List<IWebElement> angriffe = browser
						                             .WebDriver
						                             .FindElements(By.XPath(SpielOberflaeche
						                                                    .Uebersicht.Angriffe
						                                                    .ANGRIFF_SELECTOR_XPATH)).ToList();

						List<string> attackLinks = new List<string>();
						// ReSharper disable once ForCanBeConvertedToForeach
						for (int i = 0; i < angriffe.Count; i++) {
							await App.WaitForAttacks();
							IWebElement element =
								angriffe[i].FindElement(By.XPath("//a[contains(@href,'info_command&id=')]"));
							string link = element?.GetAttribute("href");
							if (!string.IsNullOrEmpty(link)) {
								int lastIndexOfGleichInurl =
									link.LastIndexOf("info_command&id=", StringComparison.Ordinal);
								int lastUndZeichen = link.LastIndexOf("&", StringComparison.Ordinal);
								string attackid = link.Substring(lastIndexOfGleichInurl + 16
								                                 , lastUndZeichen - lastIndexOfGleichInurl - 16);
								if (!accountsettings.AngriffeAufAccount.Any(x => x.AttackId.Equals(attackid))) {
									attackLinks.Add(link);
								}
							}
						}

						foreach (var attack in attackLinks) {
							await App.WaitForAttacks();
							browser.WebDriver.Navigate().GoToUrl(attack);
							await Rndm.Sleep(200, 400);

							int lastIndexOfGleichInurl =
								browser.WebDriver.Url.LastIndexOf("info_command&id=", StringComparison.Ordinal);
							int lastUndZeichen = browser.WebDriver.Url.LastIndexOf("&", StringComparison.Ordinal);
							AttackOnOwnVillage attackOnOwnVillage =
								new AttackOnOwnVillage {
									                       AttackId =
										                       browser.WebDriver.Url
										                              .Substring(lastIndexOfGleichInurl + 16
										                                         , lastUndZeichen -
										                                           lastIndexOfGleichInurl - 16)
								                       };

							if (!string.IsNullOrEmpty(attackOnOwnVillage.AttackId) &&
							    !accountsettings.AngriffeAufAccount.Any(x =>
								                                            x.AttackId.Contains(attackOnOwnVillage
									                                                                .AttackId) &&
								                                            !x.AlreadyRenamed && !x.AlreadyPrevented)) {
								IWebElement angreiferElement =
									browser.WebDriver.FindElement(By.XPath(SpielOberflaeche
									                                       .Uebersicht.Angriffe
									                                       .INC_ANGREIFERPART_SELECTOR_XPATH));
								string angreiferstring = angreiferElement.GetAttribute("href");
								int indexOfLastGleich = angreiferstring.LastIndexOf("=", StringComparison.Ordinal);
								int angreiferDorfId =
									int.Parse(angreiferstring.Substring(indexOfLastGleich + 1
									                                    , angreiferstring.Length - indexOfLastGleich -
									                                      1));
								DorfViewModel angreiferDorf =
									App.VillagelistWholeWorld
									   .FirstOrDefault(x => x.Value.VillageId.Equals(angreiferDorfId)).Value;

								string ankunft = browser
								                 .WebDriver
								                 .FindElement(By.XPath(SpielOberflaeche
								                                       .Uebersicht.Angriffe.ANKUNFT_SELECTOR_XPATH))
								                 .Text;
								DateTime ankunftszeitpunkt =
									TimeModul.TimeModulAttackDetection.GetTimeOfString(ankunft);
								attackOnOwnVillage.AttackerVillage = angreiferDorf;
								attackOnOwnVillage.DefenderVillage = dorf.Dorf;
								attackOnOwnVillage.SlowestUnit =
									TimeModul.TimeModulAttackDetection.GetSlowestUnitOfAttack(angreiferDorf, dorf.Dorf
									                                                          , ankunftszeitpunkt);
								attackOnOwnVillage.Laufzeit =
									TimeModul.Laufzeit.GetTravelTime(angreiferDorf.Dorf, dorf.Dorf.Dorf
									                                 , GetTemplateForSlowestUnit(attackOnOwnVillage
										                                                             .SlowestUnit)
										                                 .TruppenTemplate);

								attackOnOwnVillage.Ankunftszeitpunkt = ankunftszeitpunkt;

								if (!accountsettings.AngriffeAufAccount.Any(x =>
									                                            x.AttackId.Equals(attackOnOwnVillage
										                                                              .AttackId) &&
									                                            x.DefenderVillage.VillageId
									                                             .Equals(attackOnOwnVillage
									                                                     .DefenderVillage.VillageId) &&
									                                            x.Ankunftszeitpunkt
									                                             .Equals(attackOnOwnVillage
										                                                     .Ankunftszeitpunkt))) {
									accountsettings.AngriffeAufAccount.Add(attackOnOwnVillage);
									Botmessage message = new Botmessage {
										                                    Subject = "Attack"
										                                    , Art = BotmessageArt.Attackdetection
										                                    , AktionenDanach = "nothing"
										                                    , Text = "Attack on " +
										                                             attackOnOwnVillage
											                                             .DefenderVillage.Name +
										                                             " from " +
										                                             attackOnOwnVillage
											                                             .AttackerVillage.Name +
										                                             " TIME: " +
										                                             attackOnOwnVillage
											                                             .Ankunftszeitpunkt
											                                             .ToString("dd.MM.yyyy HH:mm:ss.fff"
											                                                       , CultureInfo
												                                                       .InvariantCulture) +
										                                             " with: " + attackOnOwnVillage
											                                             .SlowestUnit
									                                    };
									App.Botmessages.Add(message);
								}
							}
						}

						App.Settings.LastAttackdetectionZeitpunkt = DateTime.Now;

						App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
							               ? accountsettings.Uvname
							               : accountsettings.Accountname) + ": " + Resources.Attackdetection +
						              ": " + dorf.Dorf.VillageId + " " + Resources.Name + ": " + dorf.Dorf.Name +
						              " " + Resources.Ended);
					}
				} else {
					var elements3 = browser.WebDriver.FindElement(By.ClassName("vis_item"));
					try {
						var allegruppeelement =
							elements3.FindElement(By.PartialLinkText(NAMECONSTANTS.GetNameForServer(Names.Alle)));
						if (allegruppeelement != null && allegruppeelement.TagName.Contains("a")) {
							allegruppeelement.Click();
							Thread.Sleep(200);
						}
					} catch (Exception) {
						App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
							               ? accountsettings.Uvname
							               : accountsettings.Accountname) + ": " + " Group already selected!");
					}

					foreach (var seite in SeitenModul.GetSeiten(browser.WebDriver)) {
						try {
							SeitenModul.SetPage(browser, seite);
							IWebElement element = browser.WebDriver.FindElement(By.Id("incomings_table"));

							List<IWebElement> angriffeRows = element.FindElements(By.TagName("tr")).ToList();
							if (angriffeRows.Count > 2) {
								int counter = 0;
								foreach (var angriffeRow in angriffeRows) {
									await App.WaitForAttacks();
									if (counter != 0)
										try {
											List<IWebElement> tableCells =
												angriffeRow.FindElements(By.TagName("td")).ToList();
											if (tableCells.Any()) {
												string defenderstring = tableCells[1].Text;
												string attackerstring = tableCells[2].Text;

												string attackid = tableCells[0]
												                  .FindElements(By.TagName("input")).FirstOrDefault()
												                  ?.GetAttribute("name");
												if (!string.IsNullOrEmpty(attackid) &&
												    !accountsettings.AngriffeAufAccount.Any(x =>
													                                            x
														                                            .AttackId
														                                            .Contains(attackid) &&
													                                            !x.AlreadyRenamed &&
													                                            !x.AlreadyPrevented)) {
													var defenderVillageX =
														defenderstring
															.Substring(defenderstring.IndexOf("(", StringComparison.Ordinal) + 1
															           , 3);
													var defenderVillageY =
														defenderstring
															.Substring(defenderstring.IndexOf("(", StringComparison.Ordinal) + 5
															           , 3);
													var attackerVillageX =
														attackerstring
															.Substring(attackerstring.IndexOf("(", StringComparison.Ordinal) + 1
															           , 3);
													var attackerVillageY =
														attackerstring
															.Substring(attackerstring.IndexOf("(", StringComparison.Ordinal) + 5
															           , 3);

													int.TryParse(defenderVillageX, out var defenderx);
													int.TryParse(defenderVillageY, out var defendery);
													int.TryParse(attackerVillageX, out var attackerx);
													int.TryParse(attackerVillageY, out var attackery);

													if (attackerx > 0 && attackery > 0 && defenderx > 0 &&
													    defendery > 0) {
														DorfViewModel attackerVillage =
															App.VillagelistWholeWorld
															   .FirstOrDefault(x => x.Value.XCoordinate == attackerx &&
															                        x.Value.YCoordinate == attackery)
															   .Value;
														DorfViewModel defenderVillage =
															App.VillagelistWholeWorld
															   .FirstOrDefault(x => x.Value.XCoordinate == defenderx &&
															                        x.Value.YCoordinate == defendery)
															   .Value;
														if (attackerVillage != null && defenderVillage != null) {
															string ankunft = tableCells[5].Text;
															DateTime ankunftszeitpunkt =
																TimeModul.TimeModulAttackDetection
																         .GetTimeOfString(ankunft);

															//Hier startet die Ermittlung der langsamsten Einheit
															string slowest =
																TimeModul.TimeModulAttackDetection
																         .GetSlowestUnitOfAttack(attackerVillage
																                                 , defenderVillage
																                                 , ankunftszeitpunkt);
															string alreadyslowest =
																GetTroopOutOfName(tableCells[0].Text);
															if (!string.IsNullOrEmpty(alreadyslowest)) {
																slowest = alreadyslowest;
															}

															AttackOnOwnVillage attack =
																new AttackOnOwnVillage {
																	                       AttackId = attackid
																	                       , AttackerVillage =
																		                       attackerVillage
																	                       , DefenderVillage =
																		                       defenderVillage
																	                       , Laufzeit =
																		                       TimeModul
																			                       .Laufzeit
																			                       .GetTravelTime(attackerVillage.Dorf
																			                                      , defenderVillage
																				                                      .Dorf
																			                                      , GetTemplateForSlowestUnit(slowest)
																				                                      .TruppenTemplate)
																	                       , SlowestUnit = slowest
																	                       , Ankunftszeitpunkt =
																		                       ankunftszeitpunkt
																                       };
															if (!accountsettings
															     .AngriffeAufAccount
															     .Any(x => x.AttackId.Equals(attack.AttackId))) {
																accountsettings.AngriffeAufAccount.Add(attack);
																Botmessage message =
																	new Botmessage {
																		               Subject = "Attack"
																		               , Art =
																			               BotmessageArt.Attackdetection
																		               , AktionenDanach = "nothing"
																		               , Text = "Attack on " +
																		                        attack
																			                        .DefenderVillage
																			                        .Name + " from " +
																		                        attack.AttackerVillage
																		                              .Name +
																		                        " TIME: " +
																		                        attack.Ankunftszeitpunkt
																		                              .ToString("dd.MM.yyyy HH:mm:ss.fff"
																		                                        , CultureInfo
																			                                        .InvariantCulture) +
																		                        " with: " + attack
																			                        .SlowestUnit
																	               };
																App.Botmessages.Add(message);
																if (string.IsNullOrEmpty(alreadyslowest) &&
																    App.Settings.RenameActive) {
																	try {
																		IWebElement click =
																			browser
																				.WebDriver
																				.FindElements(By.XPath(SpielOberflaeche
																				                       .Uebersicht
																				                       .RENAMEICON_SELECTOR_XPATH))
																				[counter];
																		browser.ClickElement(click);
																		Thread.Sleep(500);
																		string angriffstext =
																			slowest + " " + Resources.Traveltime +
																			": " + attack.Laufzeit + " " +
																			Resources.Returntime + ": " +
																			attack.Ankunftszeitpunkt
																			      .Add(attack.Laufzeit)
																			      .ToLongTimeString();
																		IWebElement span = tableCells[0]
																			.FindElement(By.XPath("//span[@class='quickedit-edit']"));
																		span.FindElement(By.TagName("input")).Clear();
																		Thread.Sleep(200);
																		span.FindElement(By.TagName("input"))
																		    .SendKeys(angriffstext + "\n");
																		Thread.Sleep(300);
																	} catch {
																		// ignored
																	}
																}
															}
														}
													}
												}
											}
										} catch (Exception ex) {
											App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
												               ? accountsettings.Uvname
												               : accountsettings.Accountname) + ": " +
											              " " + Resources.Failure + " " + ex + " " + counter);
										}

									counter++;
									App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
										               ? accountsettings.Uvname
										               : accountsettings.Accountname) + ": " + counter + " " +
									              Resources.AngriffeEingelesenUndUmbenannt);
								}
							}
						} catch {
							App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
								               ? accountsettings.Uvname
								               : accountsettings.Accountname) + ": " +
							              Resources.KeineAngriffeErkannt);
						}
					}
				}
			}


			App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname)
				               ? accountsettings.Uvname
				               : accountsettings.Accountname) + ": " + Resources.AttackDetectionAccount + " " +
			              Resources.Ended);
			//incomings_amount
		}

		public static double GetMaxFeldLaufzeit(string truppe) {
			if (truppe.Contains(Resources.Snob)) return App.Settings.Weltensettings.AdelsgeschlechtLaufzeit;
			if (truppe.Contains(Resources.Ram)) return App.Settings.Weltensettings.RammboeckeLaufzeit;
			if (truppe.Contains(Resources.Sword)) return App.Settings.Weltensettings.SchwertkaempferLaufzeit;
			if (truppe.Contains(Resources.Spear)) return App.Settings.Weltensettings.SpeertraegerLaufzeit;
			if (truppe.Contains(Resources.Heavy)) return App.Settings.Weltensettings.SchwereKavallerieLaufzeit;
			if (truppe.Contains(Resources.Marcher)) return App.Settings.Weltensettings.BeritteneBogenschuetzenLaufzeit;
			if (truppe.Contains(Resources.Spy)) return App.Settings.Weltensettings.SpaeherLaufzeit;
			return 0;
		}

		private static string GetTroopOutOfName(string name) {
			if (name.Contains(Resources.Ram) || name.Contains("Ramme") || name.Contains("Rammböcke") ||
			    name.Contains(NAMECONSTANTS.GetNameForServer(Names.Rammboecke)))
				return Resources.Ram;
			if (name.Contains(Resources.Snob) || name.Contains("Adelsgeschlecht") || name.Contains("Ag") ||
			    name.Contains(NAMECONSTANTS.GetNameForServer(Names.Adelsgeschlecht)))
				return Resources.Snob;
			if (name.Contains(Resources.Sword) || name.Contains("Schwert") || name.Contains("Schw") ||
			    name.Contains(NAMECONSTANTS.GetNameForServer(Names.Schwertkaempfer)))
				return Resources.Sword;
			if (name.Contains(Resources.Spear) || name.Contains("Speerträger") || name.Contains("Speer") ||
			    name.Contains(NAMECONSTANTS.GetNameForServer(Names.Speertraeger)))
				return Resources.Spear;
			if (name.Contains(Resources.Heavy) || name.Contains("Schwere Kavallerie") || name.Contains("Schwere") ||
			    name.Contains(NAMECONSTANTS.GetNameForServer(Names.SchwereKavallerie)))
				return Resources.Heavy;
			if (name.Contains(Resources.Marcher) || name.Contains("Berittene Bogenschützen") ||
			    name.Contains("Berittene") ||
			    name.Contains(NAMECONSTANTS.GetNameForServer(Names.BeritteneBogenschuetzen)))
				return Resources.Marcher;
			if (name.Contains(Resources.Spy) || name.Contains("Späher") ||
			    name.Contains(NAMECONSTANTS.GetNameForServer(Names.Spaeher)))
				return Resources.Spy;
			return "";
		}

		public static TruppenTemplateViewModel GetTemplateForSlowestUnit(string truppe) {
			if (truppe.Contains(Resources.Snob))
				return new TruppenTemplateViewModel(new TruppenTemplate {AdelsgeschlechterAnzahl = 1});
			if (truppe.Contains(Resources.Ram))
				return new TruppenTemplateViewModel(new TruppenTemplate {RammboeckeAnzahl = 1});
			if (truppe.Contains(Resources.Sword))
				return new TruppenTemplateViewModel(new TruppenTemplate {SchwertkaempferAnzahl = 1});
			if (truppe.Contains(Resources.Spear))
				return new TruppenTemplateViewModel(new TruppenTemplate {SpeertraegerAnzahl = 1});
			if (truppe.Contains(Resources.Heavy))
				return new TruppenTemplateViewModel(new TruppenTemplate {SchwereKavallerieAnzahl = 1});
			if (truppe.Contains(Resources.Marcher))
				return new TruppenTemplateViewModel(new TruppenTemplate {BeritteneBogenschuetzenAnzahl = 1});
			if (truppe.Contains(Resources.Spy))
				return new TruppenTemplateViewModel(new TruppenTemplate {SpaeherAnzahl = 1});
			return new TruppenTemplateViewModel(new TruppenTemplate());
		}
	}
}