﻿using System;
using System.Collections.Generic;
using Ds_Bot_Reunited_NEWUI.Farmen;
using Ds_Bot_Reunited_NEWUI.Properties;
using Ds_Bot_Reunited_NEWUI.SpieldatenViewModel;

namespace Ds_Bot_Reunited_NEWUI.MultiBewegungen {
    [Serializable]
    public class MultiBewegungsziel {
        public DorfViewModel EmpfaengerDorf { get; set; }
        public DateTime SpaetesterAnkunftszeitpunkt { get; set; }
        public DateTime GenauerAnkunftszeitpunkt { get; set; }
        public TruppenTemplate EmpfangendeTruppen { get; set; }
        public TruppenTemplate MinimumTruppenproAngriff { get; set; }
        public int AngriffeCount { get; set; }
        public string Bewegungsart { get; set; }

        public List<string> Bewegungsarten => new List<string> {Resources.Attack, Resources.Support};

        public MultiBewegungsziel() {
            EmpfangendeTruppen = new TruppenTemplate();
            MinimumTruppenproAngriff = new TruppenTemplate();
        }

        public MultiBewegungsziel Clone() {
            MultiBewegungsziel newGemeinsamesZielFuerBewegung = new MultiBewegungsziel {
                Bewegungsart = Bewegungsart,
                SpaetesterAnkunftszeitpunkt = SpaetesterAnkunftszeitpunkt,
                GenauerAnkunftszeitpunkt = GenauerAnkunftszeitpunkt,
                EmpfaengerDorf = EmpfaengerDorf,
                EmpfangendeTruppen = EmpfangendeTruppen.Clone(),
                MinimumTruppenproAngriff = MinimumTruppenproAngriff.Clone(),
                AngriffeCount = AngriffeCount
            };
            return newGemeinsamesZielFuerBewegung;
        }
    }
}