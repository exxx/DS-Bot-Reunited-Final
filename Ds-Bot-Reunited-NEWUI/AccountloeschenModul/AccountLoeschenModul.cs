﻿using System;
using System.Threading.Tasks;
using Ds_Bot_Reunited_NEWUI.Accountsettings.Ds_Bot_Reunited.Accountsettings;
using Ds_Bot_Reunited_NEWUI.Botcaptcha;
using Ds_Bot_Reunited_NEWUI.BrowserManager;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using OpenQA.Selenium;

namespace Ds_Bot_Reunited_NEWUI.AccountloeschenModul {
    public class AccountLoeschenModul {
        public static async Task AccountLoeschen(Browser browser, Accountsetting accountsettings) {
            if (!accountsettings.MultiaccountingDaten.Deleted)
                try {
                    var link = App.Settings.OperateLink + accountsettings.UvZusatzFuerLink + "&screen=settings&mode=account";
                    browser.WebDriver.Navigate().GoToUrl(link);
                    await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings, link);
                    var element = browser.WebDriver.FindElements(By.XPath("//input[@name='password']"))[1];
                    ((IJavaScriptExecutor) browser.WebDriver).ExecuteScript("arguments[0].scrollIntoView(true);", element);
                    element.SendKeys(accountsettings.Passwort);
                    browser.WebDriver.FindElement(By.XPath("//input[@value='OK']")).Click();
                    accountsettings.MultiaccountingDaten.Deleted = true;
                    accountsettings.LoginActive = false;
                } catch (Exception) {
                    App.LogString(DateTime.Now.GetString()+" "+ (!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname)+": " + " Failure deleting.");
                }
        }
    }
}
