﻿using System;
using System.Linq;
using Ds_Bot_Reunited_NEWUI.Accountsettings.Ds_Bot_Reunited.Accountsettings;
using Ds_Bot_Reunited_NEWUI.BauenModul;
using Ds_Bot_Reunited_NEWUI.Marktmodul.Ds_Bot_Reunited_NEWUI.Constants;
using Ds_Bot_Reunited_NEWUI.Settings;

namespace Ds_Bot_Reunited_NEWUI.Multiaccounting {
    public class MultiaccountingSettingsAdjustModul {
        public static void AdjustSettingsForMultiaccount(Accountsetting accountsettings) {
            if (accountsettings.AutoPilot)
                if (accountsettings.LastAdjusted < DateTime.Now.AddMinutes(-20)) {
                    SetSellModeIfPossible(accountsettings);
                    if (accountsettings.DoerferSettings.Any())
                        foreach (var dorf in accountsettings.DoerferSettings) {
                            SetBauschleifeForDorf(dorf, accountsettings);
                            SetTroopsToRecruit(dorf);
                            SetMarketSettings(dorf);
                        }
                    accountsettings.LastAdjusted = DateTime.Now;
                }
        }

        private static void SetMarketSettings(AccountDorfSettings dorf) {
            if (dorf.Dorfinformationen.NoobschutzaktivBis != DateTime.MinValue)
                if (DateTime.Now > dorf.Dorfinformationen.NoobschutzaktivBis && dorf.Dorfinformationen.StorageMax > 6000) {
                    dorf.Handel.First(x => x.Ressource.Equals("Wood")).Active = true;
                    dorf.Handel.First(x => x.Ressource.Equals("Stone")).Active = true;
                    dorf.Handel.First(x => x.Ressource.Equals("Iron")).Active = true;
                    dorf.Empfangen.First(x => x.Ressource.Equals("Wood")).Active = false;
                    dorf.Empfangen.First(x => x.Ressource.Equals("Stone")).Active = false;
                    dorf.Empfangen.First(x => x.Ressource.Equals("Iron")).Active = false;
                    dorf.HandelMaxLaufzeit = 1000;
                    dorf.HandelMinRessourcen = 300;
                    dorf.HandelVerschickenArt = HandelVerschickArt.Premiumdepot;
                    dorf.HandelnActive = true;
                    var verschickenUeber = (int) (dorf.Dorfinformationen.StorageMax * 0.8);
                    dorf.HandelVerschickenHolzUeber = verschickenUeber;
                    dorf.HandelVerschickenLehmUeber = verschickenUeber;
                    dorf.HandelVerschickenEisenUeber = verschickenUeber;
                } else {
                    dorf.Handel.First(x => x.Ressource.Equals("Wood")).Active = true;
                    dorf.Handel.First(x => x.Ressource.Equals("Stone")).Active = true;
                    dorf.Handel.First(x => x.Ressource.Equals("Iron")).Active = true;
                    dorf.Empfangen.First(x => x.Ressource.Equals("Wood")).Active = false;
                    dorf.Empfangen.First(x => x.Ressource.Equals("Stone")).Active = false;
                    dorf.Empfangen.First(x => x.Ressource.Equals("Iron")).Active = false;
                    dorf.HandelMaxLaufzeit = 1000;
                    dorf.HandelMinRessourcen = 300;
                    dorf.HandelVerschickenArt = HandelVerschickArt.Premiumdepot;
                    dorf.HandelnActive = true;
                    var verschickenUeber = 500;
                    dorf.HandelVerschickenHolzUeber = verschickenUeber;
                    dorf.HandelVerschickenLehmUeber = verschickenUeber;
                    dorf.HandelVerschickenEisenUeber = verschickenUeber;
                }
        }

        private static void SetTroopsToRecruit(AccountDorfSettings dorfSettings) {
            if (dorfSettings.Dorfinformationen.NoobschutzaktivBis != DateTime.MinValue) {
                double hoursNoobschutz = -(App.Settings.Weltensettings.Angriffsschutztage * 24);
                if (DateTime.Now.AddHours(hoursNoobschutz * 0.2) < dorfSettings.Dorfinformationen.NoobschutzaktivBis)
                    dorfSettings.Rekrutieren.SpaeherAnzahl = 100;
            }
        }

        private static void SetSellModeIfPossible(Accountsetting accountsettings) {
            if (accountsettings.DoerferSettings.Any()) {
                var dorf = accountsettings.DoerferSettings.First();
                if (dorf.Dorfinformationen.NoobschutzaktivBis != DateTime.MinValue)
                    if (dorf.Dorfinformationen.GebaeudeStufen.Holzfaeller > 19 || dorf.Dorfinformationen.GebaeudeStufen.Lehmgrube > 19 || dorf.Dorfinformationen.GebaeudeStufen.Eisenmine > 19 ||
                        (DateTime.Now - dorf.Dorfinformationen.NoobschutzaktivBis).TotalHours > 100 || (DateTime.Now - dorf.Dorfinformationen.NoobschutzaktivBis).TotalHours > 0 &&
                        (dorf.Dorfinformationen.GebaeudeStufen.Holzfaeller < 13 || dorf.Dorfinformationen.GebaeudeStufen.Lehmgrube < 13 || dorf.Dorfinformationen.GebaeudeStufen.Lehmgrube < 13))
                        accountsettings.SellMode = true;
            }
        }

        public static void SetBauschleifeForDorf(AccountDorfSettings dorfSettings, Accountsetting accountsettings) {
            var prioritaet = 0;
            if (accountsettings.SellMode) {
                dorfSettings.Bauschleife.Clear();
                dorfSettings.Bauschleife.Add(new Bauelement(prioritaet++, 3, Gebaeude.Hauptgebaeude));
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Speicher, TargetLevel = 2, Priority = prioritaet++});
                dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Marktplatz, TargetLevel = 5, Priority = prioritaet});
                if (dorfSettings.Dorfinformationen.Truppen.SpaeherAnzahl > 0)
                    switch (App.Random.Next(0, 11)) {
                        case 0:
                            dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Hauptgebaeude, TargetLevel = dorfSettings.Dorfinformationen.GetGebaeudeStufe(Gebaeude.Hauptgebaeude) + 1, Priority = prioritaet});
                            break;
                        case 1:
                            dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Kaserne, TargetLevel = dorfSettings.Dorfinformationen.GetGebaeudeStufe(Gebaeude.Kaserne) + 1, Priority = prioritaet});
                            break;
                        case 2:
                            dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Stall, TargetLevel = dorfSettings.Dorfinformationen.GetGebaeudeStufe(Gebaeude.Stall) + 1, Priority = prioritaet});
                            break;
                        case 3:
                            dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Werkstatt, TargetLevel = dorfSettings.Dorfinformationen.GetGebaeudeStufe(Gebaeude.Werkstatt) + 1, Priority = prioritaet});
                            break;
                        case 4:
                            dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Schmiede, TargetLevel = dorfSettings.Dorfinformationen.GetGebaeudeStufe(Gebaeude.Schmiede) + 1, Priority = prioritaet});
                            break;
                        case 5:
                            dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Marktplatz, TargetLevel = dorfSettings.Dorfinformationen.GetGebaeudeStufe(Gebaeude.Marktplatz) + 1, Priority = prioritaet});
                            break;
                        case 6:
                            dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Holzfaeller, TargetLevel = dorfSettings.Dorfinformationen.GetGebaeudeStufe(Gebaeude.Holzfaeller) + 1, Priority = prioritaet});
                            break;
                        case 7:
                            dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Lehmgrube, TargetLevel = dorfSettings.Dorfinformationen.GetGebaeudeStufe(Gebaeude.Lehmgrube) + 1, Priority = prioritaet});
                            break;
                        case 8:
                            dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Eisenmine, TargetLevel = dorfSettings.Dorfinformationen.GetGebaeudeStufe(Gebaeude.Eisenmine) + 1, Priority = prioritaet});
                            break;
                        case 9:
                            dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Speicher, TargetLevel = dorfSettings.Dorfinformationen.GetGebaeudeStufe(Gebaeude.Speicher) + 1, Priority = prioritaet});
                            break;
                        case 10:
                            dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Bauernhof, TargetLevel = dorfSettings.Dorfinformationen.GetGebaeudeStufe(Gebaeude.Bauernhof) + 1, Priority = prioritaet});
                            break;
                        case 11:
                            dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Wall, TargetLevel = dorfSettings.Dorfinformationen.GetGebaeudeStufe(Gebaeude.Wall) + 1, Priority = prioritaet});
                            break;
                    }
            } else {
                if (dorfSettings.Bauschleife.Count < 10) {
                    //Quest 1010
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Holzfaeller, TargetLevel = 1, Priority = prioritaet++, ImageSource = GebaeudeLinks.GetImageSourceOfGebaeude(Gebaeude.Holzfaeller)});
                    //Quest 1020
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Lehmgrube, TargetLevel = 1, Priority = prioritaet++, ImageSource = GebaeudeLinks.GetImageSourceOfGebaeude(Gebaeude.Lehmgrube)});
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Eisenmine, TargetLevel = 1, Priority = prioritaet++, ImageSource = GebaeudeLinks.GetImageSourceOfGebaeude(Gebaeude.Eisenmine)});
                    //Quest 1030
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Holzfaeller, TargetLevel = 2, Priority = prioritaet++, ImageSource = GebaeudeLinks.GetImageSourceOfGebaeude(Gebaeude.Holzfaeller)});
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Lehmgrube, TargetLevel = 2, Priority = prioritaet++, ImageSource = GebaeudeLinks.GetImageSourceOfGebaeude(Gebaeude.Lehmgrube)});
                    //Quest 1040
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Hauptgebaeude, TargetLevel = 2, Priority = prioritaet++});
                    //Quest 1050
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Hauptgebaeude, TargetLevel = 3, Priority = prioritaet++});
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Kaserne, TargetLevel = 1, Priority = prioritaet++});
                    //Quest 1060
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Holzfaeller, TargetLevel = 3, Priority = prioritaet++});
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Lehmgrube, TargetLevel = 3, Priority = prioritaet++});
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Kaserne, TargetLevel = 2, Priority = prioritaet++});
                    //Quest 1070
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Speicher, TargetLevel = 2, Priority = prioritaet++});
                    //Quest 1090
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Eisenmine, TargetLevel = 2, Priority = prioritaet++});
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Speicher, TargetLevel = 3, Priority = prioritaet++});
                    //Quest 1150
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Kaserne, TargetLevel = 3, Priority = prioritaet++});
                    //Quest 1810
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Statue, TargetLevel = 1, Priority = prioritaet++});
                    //Quest 1160
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Eisenmine, TargetLevel = 3, Priority = prioritaet++});
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Bauernhof, TargetLevel = 2, Priority = prioritaet++});
                    //Quest 1110
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Hauptgebaeude, TargetLevel = 5, Priority = prioritaet++});
                    //VERSETZT NACH UNTEN!
                    //Quest 1240
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Wall, TargetLevel = 1, Priority = prioritaet++});
                    //Quest 1130
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Marktplatz, TargetLevel = 1, Priority = prioritaet++});
                    //Quest 1091
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Holzfaeller, TargetLevel = 4, Priority = prioritaet++});
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Lehmgrube, TargetLevel = 4, Priority = prioritaet++});
                    //Quest 1880
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Versteck, TargetLevel = 3, Priority = prioritaet++});
                    //Quest 1250
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Holzfaeller, TargetLevel = 5, Priority = prioritaet++});
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Lehmgrube, TargetLevel = 5, Priority = prioritaet++});

                    //Questteil
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Schmiede, TargetLevel = 1, Priority = prioritaet++});


                    //Normale minen ausbauen
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Holzfaeller, TargetLevel = 6, Priority = prioritaet++});
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Lehmgrube, TargetLevel = 6, Priority = prioritaet++});
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Eisenmine, TargetLevel = 5, Priority = prioritaet++});

                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Holzfaeller, TargetLevel = 7, Priority = prioritaet++});
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Lehmgrube, TargetLevel = 7, Priority = prioritaet++});
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Eisenmine, TargetLevel = 6, Priority = prioritaet++});
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Holzfaeller, TargetLevel = 8, Priority = prioritaet++});
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Lehmgrube, TargetLevel = 8, Priority = prioritaet++});
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Eisenmine, TargetLevel = 7, Priority = prioritaet++});

                    //Quest ohne nummer

                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Speicher, TargetLevel = 6, Priority = prioritaet++});
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Bauernhof, TargetLevel = 4, Priority = prioritaet++});


                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Holzfaeller, TargetLevel = 9, Priority = prioritaet++});
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Lehmgrube, TargetLevel = 9, Priority = prioritaet++});
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Eisenmine, TargetLevel = 8, Priority = prioritaet++});
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Holzfaeller, TargetLevel = 10, Priority = prioritaet++});
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Lehmgrube, TargetLevel = 10, Priority = prioritaet++});
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Eisenmine, TargetLevel = 9, Priority = prioritaet++});
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Holzfaeller, TargetLevel = 11, Priority = prioritaet++});
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Lehmgrube, TargetLevel = 11, Priority = prioritaet++});
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Eisenmine, TargetLevel = 12, Priority = prioritaet++});

                    //Quest 2030

                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Marktplatz, TargetLevel = 5, Priority = prioritaet++});


                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Hauptgebaeude, TargetLevel = 7, Priority = prioritaet++});
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Kaserne, TargetLevel = 5, Priority = prioritaet++});
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Bauernhof, TargetLevel = 5, Priority = prioritaet++});

                    //Quest 4030

                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Hauptgebaeude, TargetLevel = 10, Priority = prioritaet++});
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Schmiede, TargetLevel = 5, Priority = prioritaet++});

                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Stall, TargetLevel = 1, Priority = prioritaet++});

                    //Normal Building
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Holzfaeller, TargetLevel = 12, Priority = prioritaet++});
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Lehmgrube, TargetLevel = 12, Priority = prioritaet++});
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Eisenmine, TargetLevel = 14, Priority = prioritaet++});
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Wall, TargetLevel = 5, Priority = prioritaet++});
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Holzfaeller, TargetLevel = 13, Priority = prioritaet++});
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Lehmgrube, TargetLevel = 13, Priority = prioritaet++});
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Eisenmine, TargetLevel = 15, Priority = prioritaet++});
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Holzfaeller, TargetLevel = 14, Priority = prioritaet++});
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Lehmgrube, TargetLevel = 14, Priority = prioritaet++});
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Holzfaeller, TargetLevel = 15, Priority = prioritaet++});
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Lehmgrube, TargetLevel = 15, Priority = prioritaet++});
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Eisenmine, TargetLevel = 17, Priority = prioritaet++});
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Versteck, TargetLevel = 10, Priority = prioritaet++});
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Wall, TargetLevel = 10, Priority = prioritaet++});
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Marktplatz, TargetLevel = 10, Priority = prioritaet++});
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Holzfaeller, TargetLevel = 20, Priority = prioritaet++});
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Lehmgrube, TargetLevel = 20, Priority = prioritaet++});
                    dorfSettings.Bauschleife.Add(new Bauelement {Gebaeude = Gebaeude.Eisenmine, TargetLevel = 20, Priority = prioritaet});
                }
            }
        }
    }
}
