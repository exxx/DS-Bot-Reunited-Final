﻿namespace Ds_Bot_Reunited_NEWUI.Botmode {
    [global::System.Reflection.Obfuscation(Exclude = true, Feature = "renaming")]
    public enum Botmode {
        Performance,
        Normal
    }
}