﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Data;
using Ds_Bot_Reunited_NEWUI.Annotations;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.SpieldatenViewModel;

namespace Ds_Bot_Reunited_NEWUI.Attackplaner {
    [Serializable]
    public class BewegungenPlan {
        private AsyncObservableCollection<DorfViewModel> _empfangsDoerfer;
        private AsyncObservableCollection<ZeitspanneViewModel> _zeitspannen;
        private AsyncObservableCollection<AttackplanerAttackTemplateViewModel> _vorlagen;
        private AsyncObservableCollection<DorfViewModel> _absendeDoerfer;
        public int SupportCount { get; set; }
        public int AttackCount { get; set; }
        public int FakeCount { get; set; }
        public int NoblemenCount { get; set; }
        public int AttacksPerVillageCount { get; set; } = 1;
        public bool NoblemenOutOfOneVillageActive { get; set; }
        public bool GenauIsChecked { get; set; }

        public AsyncObservableCollection<ZeitspanneViewModel> ZeitSpannen {
            get => _zeitspannen;
            set {
                _zeitspannen = value;
                OnPropertyChanged();
            }
        }
        public object ZeitSpannenLock { get; set; } = new object();

        public AsyncObservableCollection<AttackplanerAttackTemplateViewModel> Vorlagen {
            get => _vorlagen;
            set {
                _vorlagen = value;
                OnPropertyChanged();
            }
        }
        public object VorlagenLock { get; set; } = new object();

        public AsyncObservableCollection<DorfViewModel> AbsendeDoerfer {
            get => _absendeDoerfer;
            set {
                _absendeDoerfer = value;
                OnPropertyChanged();
            }
        }
        public object AbsendeDoerferLock { get; set; } = new object();

        public AsyncObservableCollection<DorfViewModel> EmpfangsDoerfer {
            get => _empfangsDoerfer;
            set {
                _empfangsDoerfer = value;
                OnPropertyChanged();
            }
        }
        public object EmpfangsDoerferLock { get; set; } = new object();

        public string FileName { get; set; }

        // ReSharper disable once EmptyConstructor
        public BewegungenPlan() {
            ZeitSpannen = new AsyncObservableCollection<ZeitspanneViewModel>();
            Vorlagen = new AsyncObservableCollection<AttackplanerAttackTemplateViewModel>();
            AbsendeDoerfer = new AsyncObservableCollection<DorfViewModel>();
            EmpfangsDoerfer = new AsyncObservableCollection<DorfViewModel>();
            BindingOperations.EnableCollectionSynchronization(ZeitSpannen,ZeitSpannenLock);
            BindingOperations.EnableCollectionSynchronization(Vorlagen, VorlagenLock);
            BindingOperations.EnableCollectionSynchronization(AbsendeDoerfer, AbsendeDoerferLock);
            BindingOperations.EnableCollectionSynchronization(EmpfangsDoerfer, EmpfangsDoerferLock);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}