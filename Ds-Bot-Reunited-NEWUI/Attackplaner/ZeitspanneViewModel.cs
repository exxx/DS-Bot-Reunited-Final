﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Ds_Bot_Reunited_NEWUI.Annotations;

namespace Ds_Bot_Reunited_NEWUI.Attackplaner {
    public class ZeitspanneViewModel {
        private readonly Zeitspanne _model;
        private int _id;

        public bool IsSelected { get; set; }

        public string Zeitart {
            get => _model.Zeitart;
            set {
                _model.Zeitart = value;
                OnPropertyChanged();
            }
        }

        public DateTime Von {
            get => _model.Von;
            set {
                _model.Von = value;
                OnPropertyChanged();
            }
        }

        public DateTime Bis {
            get => _model.Bis;
            set {
                _model.Bis = value;
                OnPropertyChanged();
            }
        }

        public string Notiz {
            get => _model.Notiz;
            set {
                _model.Notiz = value;
                OnPropertyChanged();
            }
        }

        public int Id {
            get => _id;
            set {
                _id = value;
                OnPropertyChanged();
            }
        }


        public ZeitspanneViewModel(string zeitart, DateTime von, DateTime bis) {
            _model = new Zeitspanne(zeitart, von, bis);
        }

        public ZeitspanneViewModel() {
            if (_model == null) {
                _model = new Zeitspanne();
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}