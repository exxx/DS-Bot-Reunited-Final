﻿using System;
using System.Collections.Generic;
using Ds_Bot_Reunited_NEWUI.Farmen;

namespace Ds_Bot_Reunited_NEWUI.Attackplaner {
    public class PassendeZeitspanne {
        public int ZeitspanneidAbsenden { get; set; }
        public int ZeitspanneidEmpfangen { get; set; }
        public TimeSpan AddableTimeSpan { get; set; }
        public TimeSpan MustAddTimeSpanToStartTime { get; set; }
        public TimeSpan TravelTime { get; set; }
        public int SenderVillageId { get; set; }
        public Queue<TruppenTemplate> Templates { get; set; }
        public int TemplateCount { get; set; }
    }
}