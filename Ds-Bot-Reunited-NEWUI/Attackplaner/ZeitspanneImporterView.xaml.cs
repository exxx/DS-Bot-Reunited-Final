﻿using System.Windows;

namespace Ds_Bot_Reunited_NEWUI.Attackplaner {
    public partial class ZeitspanneImporterView {
        public ZeitspanneImporterView(Window owner) {
            Owner = owner;
            var vm = new ZeitspanneImporterViewModel {CloseAction = Close};
            DataContext = vm;
            InitializeComponent();
        }
    }
}