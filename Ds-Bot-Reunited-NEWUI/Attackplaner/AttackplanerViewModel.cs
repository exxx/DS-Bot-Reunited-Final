﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using Ds_Bot_Reunited_NEWUI.Accountsettings;
using Ds_Bot_Reunited_NEWUI.Accountsettings.Ds_Bot_Reunited.Accountsettings;
using Ds_Bot_Reunited_NEWUI.Angreifensettings;
using Ds_Bot_Reunited_NEWUI.Annotations;
using Ds_Bot_Reunited_NEWUI.Farmen;
using Ds_Bot_Reunited_NEWUI.Farmmodul;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.OwnVillageSelector;
using Ds_Bot_Reunited_NEWUI.Properties;
using Ds_Bot_Reunited_NEWUI.Save;
using Ds_Bot_Reunited_NEWUI.Spieldaten;
using Ds_Bot_Reunited_NEWUI.SpieldatenViewModel;
using Ds_Bot_Reunited_NEWUI.TimeModul;

namespace Ds_Bot_Reunited_NEWUI.Attackplaner {
    public class AttackplanerViewModel : INotifyPropertyChanged {
        private readonly BewegungenPlan _model;
        private ICollectionView _absendeDoerferView;
        private ICommand _absendeDorfHinzufuegenCommand;
        private ICommand _addBewegungCommand;
        private ICommand _berechnenCommand;
        private ICommand _cloneCommand;
        private ICommand _deleteAbsenderDoerferCommand;
        private ICommand _deleteEmpfaengerDoerferCommand;
        private ICommand _deleteZeitspanneCommand;
        private ICommand _empfaengerDorfHinzufuegenCommand;
        private ICollectionView _empfangsDoerferView;
        private ICommand _importierenCommand;
        private string _importInformation;
        private ICommand _loadCommand;
        private bool _progressRingActive;
        private string _progressText;
        private ICommand _removeCommand;
        private ICommand _saveCommand;
        private AttackplanerAttackTemplateViewModel _selectedVorlage;
        private ICollectionView _vorlagen2View;
        private ICollectionView _vorlagenView;
        private ICommand _zeitspanneHinzufuegenCommand;
        private ICollectionView _zeitspannenView;

        public AttackplanerViewModel(Window window) {
            Window = window;
            _model = new BewegungenPlan();
            ZeitspannenView = CollectionViewSource.GetDefaultView(_model.ZeitSpannen);
            VorlagenView = CollectionViewSource.GetDefaultView(_model.Vorlagen);
            AbsendeDoerferView = CollectionViewSource.GetDefaultView(_model.AbsendeDoerfer);
            EmpfangsDoerferView = CollectionViewSource.GetDefaultView(_model.EmpfangsDoerfer);
            BewegungenToImport = new AsyncObservableCollection<TruppenbewegungViewModel>();
            SelectedEmpfangsDoerfer = new AsyncObservableCollection<DorfViewModel>();
            SelectedAbsendeDoerfer = new AsyncObservableCollection<DorfViewModel>();
            SelectedZeitSpannen = new AsyncObservableCollection<ZeitspanneViewModel>();

            BindingOperations.EnableCollectionSynchronization(SelectedEmpfangsDoerfer, SelectedEmpfangsDoerferLock);
            BindingOperations.EnableCollectionSynchronization(SelectedAbsendeDoerfer, SelectedAbsendeDoerferLock);
            BindingOperations.EnableCollectionSynchronization(SelectedZeitSpannen, SelectedZeitSpannenLock);
        }

        public object SelectedEmpfangsDoerferLock { get; set; } = new object();
        public object SelectedAbsendeDoerferLock { get; set; } = new object();
        public object SelectedZeitSpannenLock { get; set; } = new object();

        public Window Window { get; set; }

        public bool ProgressRingActive {
            get => _progressRingActive;
            set {
                _progressRingActive = value;
                OnPropertyChanged();
            }
        }

        public string ProgressText {
            get => _progressText;
            set {
                _progressText = value;
                OnPropertyChanged();
            }
        }

        public bool IsEnabled => App.Permissions.AttackplanerIsEnabled;

        public Accountsetting Account => App.Settings.Accountsettingslist.FirstOrDefault(x => x.MultiaccountingDaten.Accountmode.Equals(Accountmode.Mainaccount));

        public Action CloseAction { get; set; }

        public bool ShouldImport { get; set; }

        public object VorlagenLock { get; set; } = new object();


        public AsyncObservableCollection<ZeitspanneViewModel> SelectedZeitSpannen { get; }
        public AsyncObservableCollection<DorfViewModel> SelectedAbsendeDoerfer { get; }
        public AsyncObservableCollection<DorfViewModel> SelectedEmpfangsDoerfer { get; }

        private List<ZeitspanneAmountProDorf> ZeitspannenProDorf { get; set; }

        public AsyncObservableCollection<TruppenbewegungViewModel> BewegungenToImport { get; set; }

        public ICollectionView AbsendeDoerferView {
            get => _absendeDoerferView;
            set {
                _absendeDoerferView = value;
                OnPropertyChanged();
            }
        }


        public ICollectionView EmpfangsDoerferView {
            get => _empfangsDoerferView;
            set {
                _empfangsDoerferView = value;
                OnPropertyChanged();
            }
        }

        public string ImportInformation {
            get => _importInformation;
            set {
                _importInformation = value;
                OnPropertyChanged();
            }
        }

        public int AnzahlVorlagen => _model.Vorlagen.Count;

        public int AttacksPerVillageCount {
            get => _model.AttacksPerVillageCount;
            set {
                _model.AttacksPerVillageCount = value;
                OnPropertyChanged();
            }
        }


        public bool GenauIsChecked {
            get => _model.NoblemenOutOfOneVillageActive;
            set {
                _model.NoblemenOutOfOneVillageActive = value;
                OnPropertyChanged();
            }
        }

        public bool NoblemenOutOfOneVillageActive {
            get => _model.NoblemenOutOfOneVillageActive;
            set {
                _model.NoblemenOutOfOneVillageActive = value;
                OnPropertyChanged();
            }
        }

        public ICollectionView VorlagenView {
            get => _vorlagenView;
            set {
                _vorlagenView = value;
                OnPropertyChanged();
            }
        }

        public ICollectionView Vorlagen2View {
            get => _vorlagen2View;
            set {
                _vorlagen2View = value;
                OnPropertyChanged();
            }
        }

        public AttackplanerAttackTemplateViewModel SelectedVorlage {
            get => _selectedVorlage;
            set {
                _selectedVorlage = value;
                OnPropertyChanged();
            }
        }

        public ICollectionView ZeitspannenView {
            get => _zeitspannenView;
            set {
                _zeitspannenView = value;
                OnPropertyChanged();
            }
        }

        public ICommand AbsendeDorfHinzufuegenCommand => _absendeDorfHinzufuegenCommand ?? (_absendeDorfHinzufuegenCommand = new RelayCommand(AbsendeDorfHinzufuegen));

        public ICommand EmpfaengerDorfHinzufuegenCommand => _empfaengerDorfHinzufuegenCommand ?? (_empfaengerDorfHinzufuegenCommand = new RelayCommand(EmpfaengerDorfHinzufuegen));

        public ICommand ZeitspanneHinzufuegenCommand => _zeitspanneHinzufuegenCommand ?? (_zeitspanneHinzufuegenCommand = new RelayCommand(ZeitspanneHinzufuegen));

        public ICommand BerechnenCommand => _berechnenCommand ?? (_berechnenCommand = new RelayCommand(BerechnenNeu,
                                                                                                       () => _model.AbsendeDoerfer.Any() && _model.EmpfangsDoerfer.Any() && _model.ZeitSpannen.Count(x => x.Zeitart.Contains("Absenden")) > 0 &&
                                                                                                             _model.ZeitSpannen.Count(x => x.Zeitart.Contains("Empfangen")) > 0 &&
                                                                                                             !_model.Vorlagen.Any(x => x.Template.TruppenTemplate.SpeertraegerAnzahl == 0 && x.Template.TruppenTemplate.SchwertkaempferAnzahl == 0 &&
                                                                                                                                       x.Template.TruppenTemplate.AxtkaempferAnzahl == 0 && x.Template.TruppenTemplate.BogenschuetzenAnzahl == 0 &&
                                                                                                                                       x.Template.TruppenTemplate.SpaeherAnzahl == 0 && x.Template.TruppenTemplate.LeichteKavallerieAnzahl == 0 &&
                                                                                                                                       x.Template.TruppenTemplate.BeritteneBogenschuetzenAnzahl == 0 &&
                                                                                                                                       x.Template.TruppenTemplate.SchwereKavallerieAnzahl == 0 && x.Template.TruppenTemplate.RammboeckeAnzahl == 0 &&
                                                                                                                                       x.Template.TruppenTemplate.KatapulteAnzahl == 0 && x.Template.TruppenTemplate.PaladinAnzahl == 0 &&
                                                                                                                                       x.Template.TruppenTemplate.AdelsgeschlechterAnzahl == 0)));

        public ICommand ImportierenCommand => _importierenCommand ?? (_importierenCommand = new RelayCommand(Importieren, () => BewegungenToImport.Any()));

        public ICommand DeleteAbsenderDoerferCommand => _deleteAbsenderDoerferCommand ?? (_deleteAbsenderDoerferCommand = new RelayCommand(DeleteAbsenderDoerfer));

        public ICommand DeleteEmpfaengerDoerferCommand => _deleteEmpfaengerDoerferCommand ?? (_deleteEmpfaengerDoerferCommand = new RelayCommand(DeleteEmpfaengerDoerfer));

        public ICommand DeleteZeitspanneCommand => _deleteZeitspanneCommand ?? (_deleteZeitspanneCommand = new RelayCommand(DeleteZeitspanne));

        public ICommand SaveCommand => _saveCommand ?? (_saveCommand = new RelayCommand(Save));

        public ICommand LoadCommand => _loadCommand ?? (_loadCommand = new RelayCommand(Load));

        public ICommand AddBewegungCommand => _addBewegungCommand ?? (_addBewegungCommand = new RelayCommand(AddBewegung));

        public ICommand CloneCommand => _cloneCommand ?? (_cloneCommand = new RelayCommand(Clone));

        public ICommand RemoveCommand => _removeCommand ?? (_removeCommand = new RelayCommand(Remove));


        public event PropertyChangedEventHandler PropertyChanged;

        private void AddBewegung() {
            var millisecond = 0;
            if (_model.Vorlagen.Any())
                millisecond = _model.Vorlagen.Max(x => x.MillisecondsDelay) + 1;
            _model.Vorlagen.Add(new AttackplanerAttackTemplateViewModel {MillisecondsDelay = millisecond, Template = new TruppenTemplateViewModel(new TruppenTemplate())});
            OnPropertyChanged("");
        }

        private void Clone() {
            if (SelectedVorlage != null) {
                var model = SelectedVorlage.Clone();
                var last = _model.Vorlagen.OrderBy(x => x.MillisecondsDelay).LastOrDefault();
                if (last != null)
                    model.MillisecondsDelay = last.MillisecondsDelay + 1;
                _model.Vorlagen.Add(model);
            }
            OnPropertyChanged("");
        }

        private void Remove() {
            if (SelectedVorlage != null)
                _model.Vorlagen.Remove(SelectedVorlage);
            OnPropertyChanged("");
        }

        //private void UpdateTroopTemplates() {
        //    var names = new List<string>();
        //    if (AttackCount > 0)
        //        names.Add(Resources.AttackTemplate);
        //    if (FakeCount > 0)
        //        names.Add(Resources.FakeTemplate);
        //    if (SupportCount > 0) {
        //        names.Add(Resources.SupportEndCount);
        //        names.Add(Resources.SupportMinTemplate);
        //        names.Add(Resources.SupportMaxTemplate);
        //    }
        //    if (NoblemenCount > 0)
        //        names.Add(Resources.NoblemenAttacks);

        //    foreach (var nam in names)
        //        if (!_model.Vorlagen.Any(x => x.Template.TemplateName.Equals(nam))) {
        //            var template = new TruppenTemplate {TemplateName = nam};
        //            var vm = new AttackplanerAttackTemplateViewModel {Template = new TruppenTemplateViewModel(template)};
        //            _model.Vorlagen.Add(vm);
        //        }
        //    for (var i = 0; i < _model.Vorlagen.Count; i++)
        //        if (!names.Any(x => x.Equals(_model.Vorlagen[i].Template.TemplateName))) {
        //            lock (_model.VorlagenLock) {
        //                _model.Vorlagen.Remove(_model.Vorlagen[i]);
        //            }
        //            i--;
        //        }
        //    UpdateGesamteTruppenverwendet();
        //}

        private void Save() {
            if (_model != null)
                XmlVerwaltung.XmlSpeichernPlan(_model);
        }

        private async void Load() {
            try {
                var model = await XmlVerwaltung.XmlLadenPlan();
                if (model != null) {
                    _model.ZeitSpannen.Clear();
                    foreach (var zeit in model.ZeitSpannen.OrderBy(x => x.Zeitart))
                        _model.ZeitSpannen.Add(zeit);
                    _model.Vorlagen.Clear();
                    foreach (var vorlage in model.Vorlagen)
                        _model.Vorlagen.Add(vorlage);
                    BewegungenToImport = new AsyncObservableCollection<TruppenbewegungViewModel>();
                    _model.AbsendeDoerfer.Clear();
                    foreach (var dorf in model.AbsendeDoerfer)
                        _model.AbsendeDoerfer.Add(dorf);
                    _model.EmpfangsDoerfer.Clear();
                    foreach (var dorf in model.EmpfangsDoerfer)
                        _model.EmpfangsDoerfer.Add(dorf);
                }
            } catch {
                MessageBox.Show("Falsche Datei ausgewählt", "Fehler", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            OnPropertyChanged("");
        }

        private void UpdateGesamteTruppenverwendet() {
            var template = _model.Vorlagen.FirstOrDefault(x => x.Template.TemplateName.Equals("Gesamt"));
            if (template != null)
                foreach (var bewegung in BewegungenToImport) {
                    template.Template.SpeertraegerAnzahl += bewegung.Model.SpeertraegerAnzahl;
                    template.Template.SchwertkaempferAnzahl += bewegung.Model.SchwertkaempferAnzahl;
                    template.Template.AxtkaempferAnzahl += bewegung.Model.AxtkaempferAnzahl;
                    template.Template.BogenschuetzenAnzahl += bewegung.Model.BogenschuetzenAnzahl;
                    template.Template.SpaeherAnzahl += bewegung.Model.SpaeherAnzahl;
                    template.Template.LeichteKavallerieAnzahl += bewegung.Model.LeichteKavallerieAnzahl;
                    template.Template.BeritteneBogenschuetzenAnzahl += bewegung.Model.BeritteneBogenschuetzenAnzahl;
                    template.Template.SchwereKavallerieAnzahl += bewegung.Model.SchwereKavallerieAnzahl;
                    template.Template.RammboeckeAnzahl += bewegung.Model.RammboeckeAnzahl;
                    template.Template.KatapulteAnzahl += bewegung.Model.KatapulteAnzahl;
                    template.Template.PaladinAnzahl += bewegung.Model.PaladinAnzahl;
                    template.Template.AdelsgeschlechterAnzahl += bewegung.Model.AdelsgeschlechterAnzahl;
                }
            OnPropertyChanged("");
        }

        private void DeleteAbsenderDoerfer() {
            var count = SelectedAbsendeDoerfer.Count;
            for (var i = 0; i < count; i++)
                _model.AbsendeDoerfer.Remove(SelectedAbsendeDoerfer[i]);
        }

        private void DeleteEmpfaengerDoerfer() {
            var count = SelectedEmpfangsDoerfer.Count;
            for (var i = 0; i < count; i++)
                _model.EmpfangsDoerfer.Remove(SelectedEmpfangsDoerfer[i]);
        }

        private void DeleteZeitspanne() {
            var count = SelectedZeitSpannen.Count;
            for (var i = 0; i < count; i++)
                _model.ZeitSpannen.Remove(SelectedZeitSpannen[0]);
        }

        private void AbsendeDorfHinzufuegen() {
            var vm = new OwnVillageSelectorViewModel(Window);
            var window = new OwnVillageSelectorView(Window, vm);
            window.ShowDialog();
            if (vm.SelectedVillages.Any())
                foreach (var village in vm.SelectedVillages)
                    _model.AbsendeDoerfer.Add(village);
        }

        private void EmpfaengerDorfHinzufuegen() {
            var view = new DorfKoordinatenImporterView(Window);
            view.ShowDialog();
            var viewmodel = (DorfKoordinatenImporterViewModel) view.DataContext;
            if (viewmodel.DoerferToImport.Any())
                foreach (var dorf in viewmodel.DoerferToImport)
                    _model.EmpfangsDoerfer.Add(dorf);
        }

        private void ZeitspanneHinzufuegen() {
            var view = new ZeitspanneImporterView(Window);
            view.ShowDialog();
            var viewmodel = (ZeitspanneImporterViewModel) view.DataContext;
            if (viewmodel.ZeitSpanneToImport != null) {
                var zeitspanneViewModel = _model.ZeitSpannen.FirstOrDefault();
                if (zeitspanneViewModel != null)
                    viewmodel.ZeitSpanneToImport.Id = zeitspanneViewModel.Id + 1;
                _model.ZeitSpannen.Add(viewmodel.ZeitSpanneToImport);
            }
        }

        private async void BerechnenNeu() {
            if ((DateTime.Now - Account.LastTruppeneinlesen).TotalMinutes > 30)
                await Task.Run(async () => {
                                   var browser = await BrowserManager.BrowserManager.GetFreeBrowser(Account, false, true, true);
                                   await DorfinformationenModul.DorfinformationenModul.UpdateVillageTroopsOfAccountOverRallyPoint(browser, Account);
                                   BrowserManager.BrowserManager.RemoveBrowser(browser);
                               });
            BewegungenToImport.Clear();
            await Task.Run(() => {
                               foreach (var vorlage in _model.Vorlagen) {
                                   var senderDoerferMitGenugTruppen = GetAllVillagesWithEnoughtTroopsForTemplate(_model.AbsendeDoerfer, vorlage.Template);
                                   if (senderDoerferMitGenugTruppen.Any()) {
                                       var berechneteDoerfer = new List<VersandDoerfer>();
                                       foreach (var empfangsdorf in _model.EmpfangsDoerfer) {
                                           var versandDorf = new VersandDoerfer {EmpfaengerVillageId = empfangsdorf.VillageId, Zeitspannen = new List<PassendeZeitspanne>()};
                                           ZeitspannenProDorf = new List<ZeitspanneAmountProDorf>();
                                           foreach (var dorf in senderDoerferMitGenugTruppen) {
                                               var ts = Laufzeit.GetTravelTime(dorf, empfangsdorf.Dorf, vorlage.Template.TruppenTemplate);
                                               versandDorf.Zeitspannen.AddRange(IsPassend(ts, dorf.VillageId));
                                           }
                                           if (versandDorf.Zeitspannen.Any())
                                               berechneteDoerfer.Add(versandDorf);
                                       }

                                       if (berechneteDoerfer.Any())
                                           foreach (var berechnetesDorf in berechneteDoerfer) {
                                               berechnetesDorf.Zeitspannen = GetZeitspannenOrdered(berechnetesDorf.Zeitspannen);
                                               int villageidNotUsed;
                                               var empfaengerid = berechnetesDorf.EmpfaengerVillageId;
                                               DorfViewModel senderDorf = null;
                                               var empfaengerDorf = App.VillagelistWholeWorld.FirstOrDefault(x => x.Value.VillageId == empfaengerid);
                                               PassendeZeitspanne firstOrDefault = null;
                                               var abschickzeitpunkt = DateTime.MinValue;
                                               for (var k = 0; k < berechnetesDorf.Zeitspannen.Count; k++)
                                                   if (vorlage.Bewegungstyp == Bewegungstyp.Fake &&
                                                       BewegungenToImport.Count(x => x.Model.SenderDorf.VillageId == berechnetesDorf.Zeitspannen[k].SenderVillageId && x.Model.Angriffsinformationen.Equals(vorlage.Bewegungstyp.GetDescription())) < 1 &&
                                                       BewegungenToImport.Count(x => x.Model.SenderDorf.VillageId == berechnetesDorf.Zeitspannen[k].SenderVillageId && x.Model.Angriffsinformationen.Equals(vorlage.Bewegungstyp.GetDescription())) +
                                                       Account.GeplanteBewegungen.Count(x => x.SenderDorf.VillageId == berechnetesDorf.Zeitspannen[k].SenderVillageId && x.Abschickzeitpunkt > DateTime.Now &&
                                                                                             x.Angriffsinformationen.Equals(vorlage.Bewegungstyp.GetDescription())) < AttacksPerVillageCount ||
                                                       vorlage.Bewegungstyp == Bewegungstyp.Off &&
                                                       BewegungenToImport.Count(x => x.Model.SenderDorf.VillageId == berechnetesDorf.Zeitspannen[k].SenderVillageId && x.Model.Angriffsinformationen.Equals(vorlage.Bewegungstyp.GetDescription())) +
                                                       Account.GeplanteBewegungen.Count(x => x.SenderDorf.VillageId == berechnetesDorf.Zeitspannen[k].SenderVillageId && x.Abschickzeitpunkt > DateTime.Now &&
                                                                                             x.Angriffsinformationen.Equals(vorlage.Bewegungstyp.GetDescription())) < 1 || vorlage.Bewegungstyp == Bewegungstyp.Deff &&
                                                       BewegungenToImport.Count(x => x.Model.SenderDorf.VillageId == berechnetesDorf.Zeitspannen[k].SenderVillageId && x.Model.EmpfaengerDorf.VillageId == berechnetesDorf.EmpfaengerVillageId &&
                                                                                     x.Model.Angriffsinformationen.Equals(vorlage.Bewegungstyp.GetDescription())) +
                                                       Account.GeplanteBewegungen.Count(x => x.SenderDorf.VillageId == berechnetesDorf.Zeitspannen[k].SenderVillageId && x.EmpfaengerDorf.VillageId == berechnetesDorf.EmpfaengerVillageId &&
                                                                                             x.Abschickzeitpunkt > DateTime.Now && x.Angriffsinformationen.Equals(vorlage.Bewegungstyp.GetDescription())) < AttacksPerVillageCount) {
                                                       var rnd = new CryptoRandom();
                                                       firstOrDefault = berechnetesDorf.Zeitspannen[k];
                                                       villageidNotUsed = firstOrDefault.SenderVillageId;
                                                       senderDorf = App.VillagelistWholeWorld.FirstOrDefault(x => x.Value.VillageId == villageidNotUsed).Value;
                                                       var zeitspanne = _model.ZeitSpannen.First(x => x.Id.Equals(firstOrDefault.ZeitspanneidAbsenden) && x.Zeitart.Contains("Absenden"));
                                                       var maxmillisecondsadd = (int) firstOrDefault.AddableTimeSpan.TotalMilliseconds;
                                                       var addablemilliseconds = rnd.Next(0, maxmillisecondsadd);
                                                       if (zeitspanne.Von.AddMilliseconds(firstOrDefault.MustAddTimeSpanToStartTime.TotalMilliseconds).AddMilliseconds(addablemilliseconds).Hour < App.Settings.Weltensettings.NachtbonusVon &&
                                                           zeitspanne.Von.AddMilliseconds(firstOrDefault.MustAddTimeSpanToStartTime.TotalMilliseconds).AddMilliseconds(addablemilliseconds).Hour > App.Settings.Weltensettings.NachtbonusBis - 1)
                                                           addablemilliseconds = 0;
                                                       abschickzeitpunkt = zeitspanne.Von.AddMilliseconds(firstOrDefault.MustAddTimeSpanToStartTime.TotalMilliseconds);
                                                       if (!GenauIsChecked)
                                                           abschickzeitpunkt.AddMilliseconds(addablemilliseconds);
                                                       k = berechnetesDorf.Zeitspannen.Count + 1;
                                                   }
                                               if (firstOrDefault != null)
                                                   if (senderDorf != null && empfaengerDorf.Value != null && abschickzeitpunkt != DateTime.MinValue) {
                                                       var template = vorlage.Template.TruppenTemplate.Clone();

                                                       var begleittruppen = GetBegleittruppenIfNeeded(template, vorlage.Bewegungstyp);

                                                       var bewegung =
                                                           new TruppenbewegungViewModel(senderDorf, empfaengerDorf.Value, abschickzeitpunkt.AddMilliseconds(vorlage.MillisecondsDelay), new TruppenTemplateViewModel(template), begleittruppen) {
                                                                                                                                                                                                                                                    BewegungsartSelected
                                                                                                                                                                                                                                                        = vorlage
                                                                                                                                                                                                                                                              .Bewegungstyp
                                                                                                                                                                                                                                                              .Equals(Bewegungstyp
                                                                                                                                                                                                                                                                          .Deff)
                                                                                                                                                                                                                                                              ? Resources
                                                                                                                                                                                                                                                                  .Support
                                                                                                                                                                                                                                                              : Resources
                                                                                                                                                                                                                                                                  .Attack,
                                                                                                                                                                                                                                                    Angriffsinformationen
                                                                                                                                                                                                                                                        = vorlage
                                                                                                                                                                                                                                                            .Bewegungstyp
                                                                                                                                                                                                                                                            .GetDescription(),
                                                                                                                                                                                                                                                    GenauTimenIsActive
                                                                                                                                                                                                                                                        = GenauIsChecked
                                                                                                                                                                                                                                                };
                                                       bewegung.Active = "J";
                                                       BewegungenToImport.Add(bewegung);
                                                   }
                                           }
                                   }
                               }
                               foreach (var dorf in _model.EmpfangsDoerfer)
                                   dorf.BewegungenCount = 0;
                               foreach (var dorf in _model.AbsendeDoerfer)
                                   dorf.BewegungenCount = 0;
                               foreach (var bewegung in BewegungenToImport) {
                                   var firstOrDefault = _model.EmpfangsDoerfer.FirstOrDefault(x => x.VillageId == bewegung.Model.EmpfaengerDorf.VillageId);
                                   if (firstOrDefault != null) {
                                       firstOrDefault.IsUsed = Color.Green;
                                       firstOrDefault.BewegungenCount++;
                                   }
                                   var dorfViewModel = _model.AbsendeDoerfer.FirstOrDefault(x => x.VillageId == bewegung.Model.SenderDorf.VillageId);
                                   if (dorfViewModel != null) {
                                       dorfViewModel.IsUsed = Color.Green;
                                       dorfViewModel.BewegungenCount++;
                                   }
                               }
                           });
        }

        private List<TruppenTemplate> GetBegleittruppenIfNeeded(TruppenTemplate template, Bewegungstyp bewegungstyp) {
            var begleittruppen = new List<TruppenTemplate>();
            if (bewegungstyp == Bewegungstyp.Noble && NoblemenOutOfOneVillageActive) {
                var begleittruppeTemplate = new TruppenTemplate();
                if (template.LeichteKavallerieAnzahl > 45)
                    begleittruppeTemplate.LeichteKavallerieAnzahl = 15;
                else if (template.LeichteKavallerieAnzahl > 75)
                    begleittruppeTemplate.LeichteKavallerieAnzahl = 25;
                else if (template.LeichteKavallerieAnzahl > 150)
                    begleittruppeTemplate.LeichteKavallerieAnzahl = 50;
                else if (template.AxtkaempferAnzahl > 100)
                    begleittruppeTemplate.AxtkaempferAnzahl = 33;
                else if (template.AxtkaempferAnzahl > 150)
                    begleittruppeTemplate.AxtkaempferAnzahl = 50;
                else if (template.AxtkaempferAnzahl > 210)
                    begleittruppeTemplate.AxtkaempferAnzahl = 70;
                else if (template.AxtkaempferAnzahl > 300)
                    begleittruppeTemplate.AxtkaempferAnzahl = 100;
                else if (template.SchwereKavallerieAnzahl > 100)
                    begleittruppeTemplate.SchwereKavallerieAnzahl = 33;
                else if (template.SchwereKavallerieAnzahl > 150)
                    begleittruppeTemplate.SchwereKavallerieAnzahl = 50;
                else if (template.SchwereKavallerieAnzahl > 210)
                    begleittruppeTemplate.SchwereKavallerieAnzahl = 70;
                else if (template.SchwertkaempferAnzahl > 100)
                    begleittruppeTemplate.SchwertkaempferAnzahl = 33;
                else if (template.SchwertkaempferAnzahl > 150)
                    begleittruppeTemplate.SchwertkaempferAnzahl = 50;
                else if (template.SchwertkaempferAnzahl > 210)
                    begleittruppeTemplate.SchwertkaempferAnzahl = 70;
                begleittruppeTemplate.AdelsgeschlechterAnzahl = 1;
                template = FarmModul.SubstractTemplateFromTemplate(template, begleittruppeTemplate);
            }
            return begleittruppen;
        }

        //ALT
        //private async void Berechnen() {
        //    if ((DateTime.Now - Account.LastTroopUpdate).TotalMinutes > 30) {
        //        App.AttacksToSend += 1;
        //        Browser browser = BrowserManager.BrowserManager.GetFreeBrowser(Account,
        //            Account.ProxyEins);
        //        await Task.Run(() => {
        //            DorfinformationenModul.DorfinformationenModul.UpdateVillageTroopsOfAccountOverRallyPoint(browser,
        //                Account);
        //        });
        //        App.AttacksToSend -= 1;
        //        Account.LastTroopUpdate = DateTime.Now;
        //    }
        //    if (ZeitSpannen.Count(x => x.Zeitart.Contains("Absenden")) > 0 &&
        //        ZeitSpannen.Count(x => x.Zeitart.Contains("Empfangen")) > 0 &&
        //        !Vorlagen.Any(x => x.SpeertraegerAnzahl == 0 && x.SchwertkaempferAnzahl == 0
        //                           && x.AxtkaempferAnzahl == 0 && x.BogenschuetzenAnzahl == 0 && x.SpaeherAnzahl == 0 &&
        //                           x.LeichteKavallerieAnzahl == 0 && x.BeritteneBogenschuetzenAnzahl == 0 &&
        //                           x.SchwereKavallerieAnzahl == 0 && x.RammboeckeAnzahl == 0 && x.KatapulteAnzahl == 0 &&
        //                           x.PaladinAnzahl == 0 && x.AdelsgeschlechterAnzahl == 0) &&
        //        Vorlagen.Any()) {
        //        BewegungenToImport.Clear();
        //        List<TruppenbewegungViewModel> bewegungen = null;
        //        bewegungen = new List<TruppenbewegungViewModel>();
        //        await Task.Run(() => {
        //            ProgressRingActive = true;
        //            lock (BewegungenToImportLock) {
        //                ZeitspannenProDorf = new List<ZeitspanneAmountProDorf>();
        //                if (AttackCount > 0 || FakeCount > 0) {
        //                    var angriffstemplate = AttackCount > 0 ? Resources.AttackTemplate : Resources.FakeTemplate;
        //                    var senderDoerferMitGenugTruppen = GetAllVillagesWithEnoughTroops(AbsendeDoerfer, Vorlagen);

        //                    var berechneteDoerfer = new List<VersandDoerfer>();
        //                    var durchgaenge = AttackCount;
        //                    if (senderDoerferMitGenugTruppen.Any())
        //                        foreach (var berechnung in EmpfangsDoerfer) {
        //                            var versandDorf = new VersandDoerfer {
        //                                EmpfaengerVillageId = berechnung.VillageId,
        //                                Zeitspannen = new List<PassendeZeitspanne>()
        //                            };
        //                            ZeitspannenProDorf = new List<ZeitspanneAmountProDorf>();
        //                            foreach (var dorf in senderDoerferMitGenugTruppen) {
        //                                var template =
        //                                    Vorlagen.FirstOrDefault(x => x.TemplateName.Equals(angriffstemplate));
        //                                var ts = TimeModul.Laufzeit.GetTravelTime(dorf.Dorf.Dorf,
        //                                    berechnung.Dorf, template.TruppenTemplate);
        //                                versandDorf.Zeitspannen.AddRange(IsPassend(ts,
        //                                    dorf.Dorf.VillageId));
        //                            }
        //                            if (versandDorf.Zeitspannen.Any()) {
        //                                berechneteDoerfer.Add(versandDorf);
        //                            }
        //                        }
        //                    if (berechneteDoerfer.Any()) {
        //                        int maxsendeanzahl = AttacksPerVillageCount;
        //                        var counter = 1;
        //                        var minmillisecondsadd = FakeCount > 0 ? 100 : 20;
        //                        foreach (var berechnetesDorf in berechneteDoerfer) {
        //                            berechnetesDorf.Zeitspannen = GetZeitspannenOrdered(berechnetesDorf.Zeitspannen);
        //                            for (var l = 0; l < durchgaenge; l++) {
        //                                int villageidNotUsed;
        //                                var empfaengerid = berechnetesDorf.EmpfaengerVillageId;
        //                                DorfViewModel senderDorf = null;
        //                                var empfaengerDorf =
        //                                    App.VillagelistWholeWorld.FirstOrDefault(
        //                                        x => x.Value.VillageId == empfaengerid);
        //                                ZeitspanneViewModel zeitspanne = null;
        //                                var truppenTemplate =
        //                                    Vorlagen.FirstOrDefault(x => x.TemplateName == angriffstemplate);
        //                                PassendeZeitspanne firstOrDefault = null;
        //                                DateTime abschickzeitpunkt = DateTime.MinValue;
        //                                for (var k = 0; k < berechnetesDorf.Zeitspannen.Count; k++) {
        //                                    var model =
        //                                        ZeitSpannen.FirstOrDefault(
        //                                            x =>
        //                                                x.Id.Equals(berechnetesDorf.Zeitspannen[k].ZeitspanneidAbsenden));
        //                                    var landtime =
        //                                        model.Von.Add(berechnetesDorf.Zeitspannen[k].MustAddTimeSpanToStartTime)
        //                                            .Add(berechnetesDorf.Zeitspannen[k].TravelTime);
        //                                    if (landtime.Hour > App.Settings.Weltensettings.NachtbonusVon - 1 &&
        //                                        landtime.Hour < App.Settings.Weltensettings.NachtbonusBis - 1)
        //                                        if (
        //                                            bewegungen.Count(
        //                                                x =>
        //                                                    x.Model.SenderDorf.VillageId ==
        //                                                    berechnetesDorf.Zeitspannen[k].SenderVillageId) +
        //                                            Account.GeplanteBewegungen.Count(
        //                                                x =>
        //                                                    x.SenderDorf.VillageId ==
        //                                                    berechnetesDorf.Zeitspannen[k].SenderVillageId) <
        //                                            senderDoerferMitGenugTruppen.FirstOrDefault(
        //                                                    x =>
        //                                                        x.Dorf.VillageId ==
        //                                                        berechnetesDorf.Zeitspannen[k].SenderVillageId)
        //                                                .TemplatePassReinAmount
        //                                            &&
        //                                            bewegungen.Count(
        //                                                x =>
        //                                                    x.Model.SenderDorf.VillageId ==
        //                                                    berechnetesDorf.Zeitspannen[k].SenderVillageId)
        //                                            +
        //                                            Account.GeplanteBewegungen.Count(
        //                                                x =>
        //                                                    x.SenderDorf.VillageId ==
        //                                                    berechnetesDorf.Zeitspannen[k].SenderVillageId) < maxsendeanzahl
        //                                            &&
        //                                            bewegungen.Count(
        //                                                x =>
        //                                                    x.Model.SenderDorf.VillageId ==
        //                                                    berechnetesDorf.Zeitspannen[k].SenderVillageId &&
        //                                                    x.Model.EmpfaengerDorf.VillageId ==
        //                                                    berechnetesDorf.EmpfaengerVillageId &&
        //                                                    x.Angriffsinformationen.Contains(FakeCount > 0
        //                                                        ? Resources.AttackTemplate
        //                                                        : Resources.FakeTemplate))
        //                                            +
        //                                            Account.GeplanteBewegungen.Count(
        //                                                x =>
        //                                                    x.SenderDorf.VillageId ==
        //                                                    berechnetesDorf.Zeitspannen[k].SenderVillageId &&
        //                                                    x.EmpfaengerDorf.VillageId ==
        //                                                    berechnetesDorf.EmpfaengerVillageId &&
        //                                                    x.Angriffsinformationen.Contains(FakeCount > 0
        //                                                        ? Resources.FakeTemplate
        //                                                        : Resources.AttackTemplate)) == 0) {
        //                                            var rnd = new CryptoRandom();
        //                                            firstOrDefault = berechnetesDorf.Zeitspannen[k];
        //                                            villageidNotUsed = firstOrDefault.SenderVillageId;
        //                                            senderDorf =
        //                                                App.VillagelistWholeWorld.FirstOrDefault(
        //                                                        x => x.Value.VillageId == villageidNotUsed)
        //                                                    .Value;
        //                                            zeitspanne =
        //                                                ZeitSpannen.FirstOrDefault(
        //                                                    x =>
        //                                                        x.Id.Equals(firstOrDefault.ZeitspanneidAbsenden) &&
        //                                                        x.Zeitart.Contains("Absenden"));
        //                                            var maxmillisecondsadd =
        //                                                (int) firstOrDefault.AddableTimeSpan.TotalMilliseconds;
        //                                            if (maxmillisecondsadd < minmillisecondsadd)
        //                                                maxmillisecondsadd = minmillisecondsadd;
        //                                            var addablemilliseconds = rnd.Next(minmillisecondsadd,
        //                                                maxmillisecondsadd);
        //                                            if (zeitspanne.Von.AddMilliseconds(
        //                                                        firstOrDefault.MustAddTimeSpanToStartTime
        //                                                            .TotalMilliseconds)
        //                                                    .AddMilliseconds(addablemilliseconds)
        //                                                    .Hour <
        //                                                App.Settings.Weltensettings.NachtbonusVon &&
        //                                                zeitspanne.Von.AddMilliseconds(
        //                                                        firstOrDefault.MustAddTimeSpanToStartTime
        //                                                            .TotalMilliseconds)
        //                                                    .AddMilliseconds(addablemilliseconds)
        //                                                    .Hour >
        //                                                App.Settings.Weltensettings.NachtbonusBis - 1)
        //                                                addablemilliseconds = 0;
        //                                            abschickzeitpunkt = zeitspanne.Von.AddMilliseconds(
        //                                                    firstOrDefault.MustAddTimeSpanToStartTime.TotalMilliseconds)
        //                                                .AddMilliseconds(addablemilliseconds);
        //                                            if (bewegungen.Count(
        //                                                    x =>
        //                                                        x.Model.Abschickzeitpunkt.AddMilliseconds(-200) <
        //                                                        abschickzeitpunkt &&
        //                                                        x.Model.Abschickzeitpunkt.AddMilliseconds(200) >
        //                                                        abschickzeitpunkt) +
        //                                                Account.GeplanteBewegungen.Count(
        //                                                    x =>
        //                                                        x.Abschickzeitpunkt.AddMilliseconds(-200) <
        //                                                        abschickzeitpunkt &&
        //                                                        x.Abschickzeitpunkt.AddMilliseconds(200) >
        //                                                        abschickzeitpunkt) == 0 && GenauIsChecked) {
        //                                                k = berechnetesDorf.Zeitspannen.Count + 1;
        //                                            }
        //                                            else if (!GenauIsChecked) {
        //                                                k = berechnetesDorf.Zeitspannen.Count + 1;
        //                                            }
        //                                        }
        //                                }
        //                                if (firstOrDefault != null) {
        //                                    if (senderDorf != null && empfaengerDorf.Value != null && zeitspanne != null &&
        //                                        truppenTemplate != null) {
        //                                        var bewegung = new TruppenbewegungViewModel(senderDorf,
        //                                            empfaengerDorf.Value, abschickzeitpunkt, truppenTemplate,
        //                                            new List<TruppenTemplate>()) {
        //                                            BewegungsartSelected = Resources.Attack,
        //                                            Angriffsinformationen = FakeCount > 0 ? Resources.FakeTemplate : Resources.AttackTemplate,
        //                                            GenauTimenIsActive = GenauIsChecked
        //                                        };
        //                                        bewegungen.Add(bewegung);
        //                                        minmillisecondsadd += FakeCount > 0 ? 1000 : 20;
        //                                    }
        //                                }
        //                            }
        //                            minmillisecondsadd = FakeCount > 0 ? 100 : 20;
        //                        }
        //                    }
        //                }
        //                else if (NoblemenCount > 0) {
        //                    var angriffstemplate = Resources.NobleTemplate;
        //                    var senderDoerferMitGenugTruppen =
        //                        GetAllVillagesWithEnoughTroops(AbsendeDoerfer,
        //                            Vorlagen);
        //                    var berechneteDoerfer = new List<VersandDoerfer>();
        //                    // ReSharper disable once RedundantAssignment
        //                    var durchgaenge = 1;
        //                    if (senderDoerferMitGenugTruppen.Any())
        //                        foreach (var berechnung in EmpfangsDoerfer) {
        //                            var versandDorf = new VersandDoerfer {
        //                                EmpfaengerVillageId = berechnung.VillageId,
        //                                Zeitspannen = new List<PassendeZeitspanne>()
        //                            };
        //                            foreach (var dorf in senderDoerferMitGenugTruppen) {
        //                                var template =
        //                                    Vorlagen.FirstOrDefault(x => x.TemplateName.Equals(angriffstemplate));
        //                                var ts = TimeModul.Laufzeit.GetTravelTime(dorf.Dorf.Dorf,
        //                                    berechnung.Dorf, template.TruppenTemplate);
        //                                versandDorf.Zeitspannen.AddRange(IsPassend(ts,
        //                                    dorf.Dorf.VillageId));
        //                            }
        //                            if (versandDorf.Zeitspannen.Count > 0) {
        //                                berechneteDoerfer.Add(versandDorf);
        //                            }
        //                        }
        //                    if (berechneteDoerfer.Any()) {
        //                        var counter = 1;
        //                        foreach (var berechnetesDorf in berechneteDoerfer) {
        //                            var minmillisecondsadd = 150;
        //                            for (var l = 0; l < (NoblemenOutOfOneVillageActive ? 1 : 4); l++) {
        //                                PassendeZeitspanne firstOrDefault = null;
        //                                for (var k = 0; k < berechnetesDorf.Zeitspannen.Count; k++) {
        //                                    var model =
        //                                        ZeitSpannen.FirstOrDefault(
        //                                            x =>
        //                                                x.Id.Equals(berechnetesDorf.Zeitspannen[k].ZeitspanneidAbsenden));
        //                                    var landtime =
        //                                        model.Von.Add(berechnetesDorf.Zeitspannen[k].MustAddTimeSpanToStartTime)
        //                                            .Add(berechnetesDorf.Zeitspannen[k].TravelTime);
        //                                    if (landtime.Hour > App.Settings.Weltensettings.NachtbonusVon - 1 &&
        //                                        landtime.Hour < App.Settings.Weltensettings.NachtbonusBis - 1)
        //                                        if (
        //                                            bewegungen.Count(
        //                                                x =>
        //                                                    x.Model.SenderDorf.VillageId ==
        //                                                    berechnetesDorf.Zeitspannen[k].SenderVillageId) +
        //                                            Account.GeplanteBewegungen.Count(
        //                                                x =>
        //                                                    x.SenderDorf.VillageId ==
        //                                                    berechnetesDorf.Zeitspannen[k].SenderVillageId)
        //                                            <
        //                                            (NoblemenOutOfOneVillageActive
        //                                                ? 1
        //                                                : senderDoerferMitGenugTruppen.FirstOrDefault(
        //                                                        x =>
        //                                                            x.Dorf.VillageId ==
        //                                                            berechnetesDorf.Zeitspannen[k].SenderVillageId)
        //                                                    .TemplatePassReinAmount)
        //                                        ) {
        //                                            firstOrDefault = berechnetesDorf.Zeitspannen[k];
        //                                            k = berechnetesDorf.Zeitspannen.Count + 1;
        //                                        }
        //                                }

        //                                if (firstOrDefault != null) {
        //                                    var villageidNotUsed = firstOrDefault.SenderVillageId;
        //                                    var empfaengerid = berechnetesDorf.EmpfaengerVillageId;
        //                                    var senderDorf =
        //                                        App.VillagelistWholeWorld.FirstOrDefault(
        //                                            x => x.Value.VillageId == villageidNotUsed);
        //                                    var empfaengerDorf =
        //                                        App.VillagelistWholeWorld.FirstOrDefault(
        //                                            x => x.Value.VillageId == empfaengerid);
        //                                    var zeitspanne =
        //                                        ZeitSpannen.FirstOrDefault(
        //                                            x => x.Id.Equals(firstOrDefault.ZeitspanneidAbsenden));
        //                                    var truppenTemplate =
        //                                        Vorlagen.FirstOrDefault(x => x.TemplateName == angriffstemplate);
        //                                    if (senderDorf.Value != null && empfaengerDorf.Value != null &&
        //                                        zeitspanne != null &&
        //                                        truppenTemplate != null) {
        //                                        var rnd = new CryptoRandom();
        //                                        var maxmillisecondsadd =
        //                                            (int) firstOrDefault.AddableTimeSpan.TotalMilliseconds;
        //                                        if (maxmillisecondsadd < minmillisecondsadd)
        //                                            maxmillisecondsadd = minmillisecondsadd;
        //                                        var addablemilliseconds = rnd.Next(minmillisecondsadd,
        //                                            maxmillisecondsadd);
        //                                        if (zeitspanne.Von.AddMilliseconds(
        //                                                    firstOrDefault.MustAddTimeSpanToStartTime.TotalMilliseconds)
        //                                                .AddMilliseconds(addablemilliseconds)
        //                                                .Hour <
        //                                            App.Settings.Weltensettings.NachtbonusVon &&
        //                                            zeitspanne.Von.AddMilliseconds(
        //                                                    firstOrDefault.MustAddTimeSpanToStartTime.TotalMilliseconds)
        //                                                .AddMilliseconds(addablemilliseconds)
        //                                                .Hour >
        //                                            App.Settings.Weltensettings.NachtbonusBis - 1)
        //                                            addablemilliseconds = 0;
        //                                        List<TruppenTemplate> begleittruppen = new List<TruppenTemplate>();
        //                                        if (NoblemenOutOfOneVillageActive) {
        //                                            for (int u = 0; u < 3; u++) {
        //                                                begleittruppen.Add(truppenTemplate.TruppenTemplate);
        //                                            }
        //                                        }
        //                                        var bewegung = new TruppenbewegungViewModel(senderDorf.Value, empfaengerDorf.Value,
        //                                            zeitspanne.Von.AddMilliseconds(
        //                                                    firstOrDefault.MustAddTimeSpanToStartTime.TotalMilliseconds)
        //                                                .AddMilliseconds(addablemilliseconds), truppenTemplate,
        //                                            begleittruppen) {
        //                                            BewegungsartSelected = Resources.AttackTemplate,
        //                                            Angriffsinformationen = Resources.NobleTemplate,
        //                                            GenauTimenIsActive = GenauIsChecked
        //                                        };
        //                                        bewegungen.Add(bewegung);
        //                                        minmillisecondsadd += 10;
        //                                    }
        //                                }
        //                            }
        //                        }
        //                    }


        //                    // Off
        //                    angriffstemplate = Resources.AttackTemplate;
        //                    senderDoerferMitGenugTruppen = GetAllVillagesWithEnoughTroops(AbsendeDoerfer,
        //                        Vorlagen);
        //                    berechneteDoerfer = new List<VersandDoerfer>();
        //                    durchgaenge = AttackCount;
        //                    int maxsendeanzahl = AttacksPerVillageCount;
        //                    if (senderDoerferMitGenugTruppen.Any())
        //                        foreach (var berechnung in EmpfangsDoerfer) {
        //                            var versandDorf = new VersandDoerfer {
        //                                EmpfaengerVillageId = berechnung.VillageId,
        //                                Zeitspannen = new List<PassendeZeitspanne>()
        //                            };
        //                            ZeitspannenProDorf = new List<ZeitspanneAmountProDorf>();
        //                            foreach (var dorf in senderDoerferMitGenugTruppen) {
        //                                var template =
        //                                    Vorlagen.FirstOrDefault(x => x.TemplateName.Equals(angriffstemplate));
        //                                var ts = TimeModul.Laufzeit.GetTravelTime(dorf.Dorf.Dorf,
        //                                    berechnung.Dorf, template.TruppenTemplate);
        //                                versandDorf.Zeitspannen.AddRange(IsPassend(ts,
        //                                    dorf.Dorf.VillageId));
        //                            }
        //                            if (versandDorf.Zeitspannen.Count > 0) {
        //                                berechneteDoerfer.Add(versandDorf);
        //                            }
        //                        }
        //                    if (berechneteDoerfer.Any()) {
        //                        var counter = 0;
        //                        foreach (var berechnetesDorf in berechneteDoerfer) {
        //                            berechnetesDorf.Zeitspannen = GetZeitspannenOrdered(berechnetesDorf.Zeitspannen);
        //                            var minmillisecondsadd = 40;
        //                            for (var l = 0; l < durchgaenge; l++) {
        //                                int villageidNotUsed;
        //                                var empfaengerid = berechnetesDorf.EmpfaengerVillageId;
        //                                DorfViewModel senderDorf = null;
        //                                var empfaengerDorf =
        //                                    App.VillagelistWholeWorld.FirstOrDefault(
        //                                        x => x.Value.VillageId == empfaengerid);
        //                                ZeitspanneViewModel zeitspanne = null;
        //                                var truppenTemplate =
        //                                    Vorlagen.FirstOrDefault(x => x.TemplateName == angriffstemplate);
        //                                PassendeZeitspanne firstOrDefault = null;
        //                                DateTime abschickzeitpunkt = DateTime.MinValue;
        //                                for (var k = 0; k < berechnetesDorf.Zeitspannen.Count; k++) {
        //                                    var model =
        //                                        ZeitSpannen.FirstOrDefault(
        //                                            x =>
        //                                                x.Id.Equals(berechnetesDorf.Zeitspannen[k].ZeitspanneidAbsenden));
        //                                    var landtime =
        //                                        model.Von.Add(berechnetesDorf.Zeitspannen[k].MustAddTimeSpanToStartTime)
        //                                            .Add(berechnetesDorf.Zeitspannen[k].TravelTime);
        //                                    if (landtime.Hour > App.Settings.Weltensettings.NachtbonusVon - 1 &&
        //                                        landtime.Hour < App.Settings.Weltensettings.NachtbonusBis - 1)
        //                                        if (
        //                                            bewegungen.Count(
        //                                                x =>
        //                                                    x.Model.SenderDorf.VillageId ==
        //                                                    berechnetesDorf.Zeitspannen[k].SenderVillageId) +
        //                                            Account.GeplanteBewegungen.Count(
        //                                                x =>
        //                                                    x.SenderDorf.VillageId ==
        //                                                    berechnetesDorf.Zeitspannen[k].SenderVillageId) <
        //                                            senderDoerferMitGenugTruppen.FirstOrDefault(
        //                                                    x =>
        //                                                        x.Dorf.VillageId ==
        //                                                        berechnetesDorf.Zeitspannen[k].SenderVillageId)
        //                                                .TemplatePassReinAmount
        //                                            &&
        //                                            bewegungen.Count(
        //                                                x =>
        //                                                    x.Model.SenderDorf.VillageId ==
        //                                                    berechnetesDorf.Zeitspannen[k].SenderVillageId)
        //                                            +
        //                                            Account.GeplanteBewegungen.Count(
        //                                                x =>
        //                                                    x.SenderDorf.VillageId ==
        //                                                    berechnetesDorf.Zeitspannen[k].SenderVillageId) < maxsendeanzahl
        //                                            &&
        //                                            bewegungen.Count(
        //                                                x =>
        //                                                    x.Model.SenderDorf.VillageId ==
        //                                                    berechnetesDorf.Zeitspannen[k].SenderVillageId &&
        //                                                    x.Model.EmpfaengerDorf.VillageId ==
        //                                                    berechnetesDorf.EmpfaengerVillageId &&
        //                                                    x.Angriffsinformationen.Contains(Resources.AttackTemplate))
        //                                            +
        //                                            Account.GeplanteBewegungen.Count(
        //                                                x =>
        //                                                    x.SenderDorf.VillageId ==
        //                                                    berechnetesDorf.Zeitspannen[k].SenderVillageId &&
        //                                                    x.EmpfaengerDorf.VillageId ==
        //                                                    berechnetesDorf.EmpfaengerVillageId &&
        //                                                    x.Angriffsinformationen.Contains(Resources.AttackTemplate)) == 0) {
        //                                            var rnd = new CryptoRandom();
        //                                            firstOrDefault = berechnetesDorf.Zeitspannen[k];
        //                                            villageidNotUsed = firstOrDefault.SenderVillageId;
        //                                            senderDorf =
        //                                                App.VillagelistWholeWorld.FirstOrDefault(
        //                                                        x => x.Value.VillageId == villageidNotUsed)
        //                                                    .Value;
        //                                            zeitspanne =
        //                                                ZeitSpannen.FirstOrDefault(
        //                                                    x =>
        //                                                        x.Id.Equals(firstOrDefault.ZeitspanneidAbsenden) &&
        //                                                        x.Zeitart.Contains("Absenden"));
        //                                            var maxmillisecondsadd =
        //                                                (int) firstOrDefault.AddableTimeSpan.TotalMilliseconds;
        //                                            if (maxmillisecondsadd < minmillisecondsadd)
        //                                                maxmillisecondsadd = minmillisecondsadd;
        //                                            var addablemilliseconds = rnd.Next(minmillisecondsadd,
        //                                                maxmillisecondsadd);
        //                                            if (zeitspanne.Von.AddMilliseconds(
        //                                                        firstOrDefault.MustAddTimeSpanToStartTime
        //                                                            .TotalMilliseconds)
        //                                                    .AddMilliseconds(addablemilliseconds)
        //                                                    .Hour <
        //                                                App.Settings.Weltensettings.NachtbonusVon &&
        //                                                zeitspanne.Von.AddMilliseconds(
        //                                                        firstOrDefault.MustAddTimeSpanToStartTime
        //                                                            .TotalMilliseconds)
        //                                                    .AddMilliseconds(addablemilliseconds)
        //                                                    .Hour >
        //                                                App.Settings.Weltensettings.NachtbonusBis - 1)
        //                                                addablemilliseconds = 0;
        //                                            abschickzeitpunkt = zeitspanne.Von.AddMilliseconds(
        //                                                    firstOrDefault.MustAddTimeSpanToStartTime.TotalMilliseconds)
        //                                                .AddMilliseconds(addablemilliseconds);
        //                                            if (Account.GeplanteBewegungen.Count(
        //                                                    x =>
        //                                                        x.Abschickzeitpunkt.AddMilliseconds(-200) <
        //                                                        abschickzeitpunkt &&
        //                                                        x.Abschickzeitpunkt.AddMilliseconds(200) >
        //                                                        abschickzeitpunkt) +
        //                                                bewegungen.Count(
        //                                                    x =>
        //                                                        x.Model.Abschickzeitpunkt.AddMilliseconds(-200) <
        //                                                        abschickzeitpunkt &&
        //                                                        x.Model.Abschickzeitpunkt.AddMilliseconds(200) >
        //                                                        abschickzeitpunkt) == 0) {
        //                                                k = berechnetesDorf.Zeitspannen.Count + 1;
        //                                            }
        //                                        }
        //                                }
        //                                if (firstOrDefault != null) {
        //                                    if (senderDorf != null && empfaengerDorf.Value != null && zeitspanne != null &&
        //                                        truppenTemplate != null) {
        //                                        var bewegung = new TruppenbewegungViewModel(senderDorf, empfaengerDorf.Value, abschickzeitpunkt, truppenTemplate,
        //                                            new List<TruppenTemplate>()) {
        //                                            BewegungsartSelected = Resources.Attack,
        //                                            Angriffsinformationen = Resources.AttackTemplate,
        //                                            GenauTimenIsActive = GenauIsChecked
        //                                        };
        //                                        bewegungen.Add(bewegung);
        //                                        minmillisecondsadd += 10;
        //                                    }
        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //                //Deff verschicken
        //                if (SupportCount > 0) {
        //                    var angriffstemplate = Resources.SupportEndCount;
        //                    var senderDoerferMitGenugTruppen = GetAllVillagesWithEnoughTroopsForDeff(AbsendeDoerfer,
        //                        Vorlagen);
        //                    var berechneteDoerfer = new List<VersandDoerfer>();
        //                    if (senderDoerferMitGenugTruppen.Any())
        //                        foreach (var berechnung in EmpfangsDoerfer) {
        //                            var versandDorf = new VersandDoerfer {
        //                                EmpfaengerVillageId = berechnung.VillageId,
        //                                Zeitspannen = new List<PassendeZeitspanne>()
        //                            };
        //                            ZeitspannenProDorf = new List<ZeitspanneAmountProDorf>();
        //                            foreach (var dorf in senderDoerferMitGenugTruppen) {
        //                                var template =
        //                                    Vorlagen.FirstOrDefault(x => x.TemplateName.Equals(angriffstemplate));
        //                                var ts = TimeModul.Laufzeit.GetTravelTime(dorf.Dorf.Dorf,
        //                                    berechnung.Dorf, template.TruppenTemplate);
        //                                versandDorf.Zeitspannen.AddRange(IsPassend(ts,
        //                                    dorf.Dorf.VillageId, dorf.Templates));
        //                            }
        //                            if (versandDorf.Zeitspannen.Count > 0) {
        //                                berechneteDoerfer.Add(versandDorf);
        //                            }
        //                        }
        //                    if (berechneteDoerfer.Any()) {
        //                        foreach (var berechnetesDorf in berechneteDoerfer) {
        //                            var counter = 0;
        //                            TruppenTemplate actualAnkommendeTruppen = new TruppenTemplate();
        //                            AttackPreventionModul.AttackPreventionModul.AddOnTemplate(actualAnkommendeTruppen,
        //                                GetAllTruppenFromBefehle(berechnetesDorf.EmpfaengerVillageId));
        //                            var minmillisecondsadd = 100;
        //                            var empfaengerid = berechnetesDorf.EmpfaengerVillageId;
        //                            DorfViewModel senderDorf = null;
        //                            var empfaengerDorf =
        //                                App.VillagelistWholeWorld.FirstOrDefault(
        //                                        x => x.Value.VillageId == empfaengerid)
        //                                    .Value;
        //                            berechnetesDorf.Zeitspannen = GetZeitspannenOrdered(berechnetesDorf.Zeitspannen);
        //                            while (
        //                                !AttackPreventionModul.AttackPreventionModul.CompareTemplates(
        //                                    actualAnkommendeTruppen,
        //                                    Vorlagen.FirstOrDefault(x => x.TemplateName.Equals(angriffstemplate))
        //                                        .TruppenTemplate) && counter < berechnetesDorf.Zeitspannen.Count) {
        //                                int villageidNotUsed;

        //                                TruppenTemplate fehlendeTruppen =
        //                                    AttackPreventionModul.AttackPreventionModul.UnterschiedTemplates(
        //                                        actualAnkommendeTruppen,
        //                                        Vorlagen.FirstOrDefault(x => x.TemplateName.Equals(angriffstemplate))
        //                                            .TruppenTemplate);

        //                                ZeitspanneViewModel zeitspanne = null;
        //                                PassendeZeitspanne firstOrDefault = null;
        //                                DateTime abschickzeitpunkt = DateTime.MinValue;
        //                                for (var k = 0; k < berechnetesDorf.Zeitspannen.Count; k++) {
        //                                    // ReSharper disable once UnusedVariable
        //                                    var model =
        //                                        ZeitSpannen.FirstOrDefault(
        //                                            x =>
        //                                                x.Id.Equals(berechnetesDorf.Zeitspannen[k].ZeitspanneidAbsenden));
        //                                    if (
        //                                        berechnetesDorf.Zeitspannen[k].Templates.Count > 0
        //                                        &&
        //                                        bewegungen.Count(
        //                                            x =>
        //                                                x.Model.SenderDorf.VillageId ==
        //                                                berechnetesDorf.Zeitspannen[k].SenderVillageId
        //                                                &&
        //                                                x.Model.EmpfaengerDorf.VillageId ==
        //                                                berechnetesDorf.EmpfaengerVillageId &&
        //                                                x.Angriffsinformationen.Contains(Resources.Support)) == 0 &&
        //                                        Account.GeplanteBewegungen.Count(
        //                                            x =>
        //                                                x.SenderDorf.VillageId ==
        //                                                berechnetesDorf.Zeitspannen[k].SenderVillageId
        //                                                &&
        //                                                x.EmpfaengerDorf.VillageId ==
        //                                                berechnetesDorf.EmpfaengerVillageId &&
        //                                                x.Angriffsinformationen.Contains(Resources.Support)) == 0) {
        //                                        var rnd = new CryptoRandom();
        //                                        firstOrDefault = berechnetesDorf.Zeitspannen[k];
        //                                        villageidNotUsed = firstOrDefault.SenderVillageId;
        //                                        senderDorf =
        //                                            App.VillagelistWholeWorld.FirstOrDefault(
        //                                                    x => x.Value.VillageId == villageidNotUsed)
        //                                                .Value;
        //                                        zeitspanne =
        //                                            ZeitSpannen.FirstOrDefault(
        //                                                x =>
        //                                                    x.Id.Equals(firstOrDefault.ZeitspanneidAbsenden) &&
        //                                                    x.Zeitart.Contains("Absenden"));
        //                                        var maxmillisecondsadd =
        //                                            (int) firstOrDefault.AddableTimeSpan.TotalMilliseconds;
        //                                        if (maxmillisecondsadd < 0) maxmillisecondsadd = minmillisecondsadd;
        //                                        var addablemilliseconds = rnd.Next(minmillisecondsadd,
        //                                            maxmillisecondsadd);
        //                                        if (zeitspanne.Von.AddMilliseconds(
        //                                                    firstOrDefault.MustAddTimeSpanToStartTime.TotalMilliseconds)
        //                                                .AddMilliseconds(addablemilliseconds)
        //                                                .Hour <
        //                                            App.Settings.Weltensettings.NachtbonusVon &&
        //                                            zeitspanne.Von.AddMilliseconds(
        //                                                    firstOrDefault.MustAddTimeSpanToStartTime.TotalMilliseconds)
        //                                                .AddMilliseconds(addablemilliseconds)
        //                                                .Hour >
        //                                            App.Settings.Weltensettings.NachtbonusBis - 1)
        //                                            addablemilliseconds = 0;
        //                                        abschickzeitpunkt = zeitspanne.Von.AddMilliseconds(
        //                                                firstOrDefault.MustAddTimeSpanToStartTime.TotalMilliseconds)
        //                                            .AddMilliseconds(addablemilliseconds);
        //                                        if (Account.GeplanteBewegungen.Count(
        //                                                x =>
        //                                                    x.Abschickzeitpunkt.AddMilliseconds(-200) <
        //                                                    abschickzeitpunkt &&
        //                                                    x.Abschickzeitpunkt.AddMilliseconds(200) > abschickzeitpunkt) +
        //                                            bewegungen.Count(
        //                                                x =>
        //                                                    x.Model.Abschickzeitpunkt.AddMilliseconds(-200) <
        //                                                    abschickzeitpunkt &&
        //                                                    x.Model.Abschickzeitpunkt.AddMilliseconds(200) >
        //                                                    abschickzeitpunkt) == 0 && GenauIsChecked) {
        //                                            k = berechnetesDorf.Zeitspannen.Count + 1;
        //                                        }
        //                                        else if (!GenauIsChecked) {
        //                                            k = berechnetesDorf.Zeitspannen.Count + 1;
        //                                        }
        //                                    }
        //                                }
        //                                if (firstOrDefault != null) {
        //                                    var truppenTemplate = UseableTemplate(firstOrDefault.Templates.Dequeue(),
        //                                        fehlendeTruppen);
        //                                    if (truppenTemplate.TruppenTemplate.SpeertraegerAnzahl > 0 ||
        //                                        truppenTemplate.TruppenTemplate.SchwertkaempferAnzahl > 0 ||
        //                                        truppenTemplate.TruppenTemplate.BogenschuetzenAnzahl > 0 ||
        //                                        truppenTemplate.TruppenTemplate.SpaeherAnzahl > 0 ||
        //                                        truppenTemplate.TruppenTemplate.SchwereKavallerieAnzahl > 0)
        //                                        if (senderDorf != null && empfaengerDorf != null && zeitspanne != null &&
        //                                            truppenTemplate != null) {
        //                                            var bewegung = new TruppenbewegungViewModel(senderDorf,
        //                                                empfaengerDorf, abschickzeitpunkt, truppenTemplate,
        //                                                new List<TruppenTemplate>()) {
        //                                                BewegungsartSelected = Resources.Support,
        //                                                Angriffsinformationen = Resources.Support,
        //                                                GenauTimenIsActive = GenauIsChecked
        //                                            };
        //                                            bewegungen.Add(bewegung);
        //                                            AttackPreventionModul.AttackPreventionModul.AddOnTemplate(actualAnkommendeTruppen, truppenTemplate.TruppenTemplate);
        //                                            minmillisecondsadd += 20;
        //                                        }
        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //            ProgressRingActive = false;
        //        });


        //        foreach (var dorf in AbsendeDoerfer) {
        //            dorf.BewegungenCount = 0;
        //        }
        //        foreach (var dorf in EmpfangsDoerfer) {
        //            dorf.BewegungenCount = 0;
        //        }


        //        foreach (var bewegung in BewegungenToImport) {
        //            var firstOrDefault =
        //                EmpfangsDoerfer.FirstOrDefault(x => x.VillageId == bewegung.Model.EmpfaengerDorf.VillageId);
        //            if (firstOrDefault != null) {
        //                firstOrDefault.IsUsed =
        //                    Color.Green;
        //                firstOrDefault.BewegungenCount++;
        //            }
        //            var dorfViewModel =
        //                AbsendeDoerfer.FirstOrDefault(x => x.VillageId == bewegung.Model.SenderDorf.VillageId);
        //            if (dorfViewModel != null) {
        //                dorfViewModel.IsUsed =
        //                    Color.Green;
        //                dorfViewModel.BewegungenCount++;
        //            }
        //        }
        //        UpdateGesamteTruppenverwendet();
        //        OnPropertyChanged("");
        //    }
        //}

        private void Importieren() {
            App.Settings.Accountsettingslist.First().GeplanteBewegungen.AddRange(BewegungenToImport.Select(x => x.Model));
            CloseAction();
        }

        private TruppenTemplateViewModel UseableTemplate(TruppenTemplate hat, TruppenTemplate soll) {
            var template = new TruppenTemplateViewModel {
                                                            TruppenTemplate = new TruppenTemplate {
                                                                                                      SpeertraegerAnzahl = soll.SpeertraegerAnzahl < hat.SpeertraegerAnzahl ? soll.SpeertraegerAnzahl : hat.SpeertraegerAnzahl,
                                                                                                      SchwertkaempferAnzahl = soll.SchwertkaempferAnzahl < hat.SchwertkaempferAnzahl ? soll.SchwertkaempferAnzahl : hat.SchwertkaempferAnzahl,
                                                                                                      BogenschuetzenAnzahl = soll.BogenschuetzenAnzahl < hat.BogenschuetzenAnzahl ? soll.BogenschuetzenAnzahl : hat.BogenschuetzenAnzahl,
                                                                                                      SpaeherAnzahl = soll.SpaeherAnzahl < hat.SpaeherAnzahl ? soll.SpaeherAnzahl : hat.SpaeherAnzahl,
                                                                                                      SchwereKavallerieAnzahl = soll.SchwereKavallerieAnzahl < hat.SchwereKavallerieAnzahl ? soll.SchwereKavallerieAnzahl : hat.SchwereKavallerieAnzahl,
                                                                                                      PaladinAnzahl = soll.PaladinAnzahl < hat.PaladinAnzahl ? soll.PaladinAnzahl : hat.PaladinAnzahl
                                                                                                  }
                                                        };
            if (template.TruppenTemplate.SpeertraegerAnzahl < 0)
                template.TruppenTemplate.SpeertraegerAnzahl = 0;
            if (template.TruppenTemplate.SchwertkaempferAnzahl < 0)
                template.TruppenTemplate.SchwertkaempferAnzahl = 0;
            if (template.TruppenTemplate.BogenschuetzenAnzahl < 0)
                template.TruppenTemplate.BogenschuetzenAnzahl = 0;
            if (template.TruppenTemplate.SpaeherAnzahl < 0)
                template.TruppenTemplate.SpaeherAnzahl = 0;
            if (template.TruppenTemplate.SchwereKavallerieAnzahl < 0)
                template.TruppenTemplate.SchwereKavallerieAnzahl = 0;
            return template;
        }

        private TruppenTemplate GetAllTruppenFromBefehle(int empfaengerid) {
            var template = new TruppenTemplate();
            foreach (var befehl in Account.Befehle.Where(x => x.Befehlsart == "support" && x.EmpfaengerVillageId.Equals(empfaengerid))) {
                template.SpeertraegerAnzahl += befehl.SpeertraegerAnzahl;
                template.SchwertkaempferAnzahl += befehl.SchwertkaempferAnzahl;
                template.BogenschuetzenAnzahl += befehl.BogenschuetzenAnzahl;
                template.SpaeherAnzahl += befehl.SpaeherAnzahl;
                template.SchwertkaempferAnzahl += befehl.SchwereKavallerieAnzahl;
                template.PaladinAnzahl += befehl.PaladinAnzahl;
            }
            foreach (var befehl in Account.Unterstuetzungen.Where(x => x.EmpfaengerVillageId.Equals(empfaengerid))) {
                template.SpeertraegerAnzahl += befehl.SpeertraegerAnzahl;
                template.SchwertkaempferAnzahl += befehl.SchwertkaempferAnzahl;
                template.BogenschuetzenAnzahl += befehl.BogenschuetzenAnzahl;
                template.SpaeherAnzahl += befehl.SpaeherAnzahl;
                template.SchwertkaempferAnzahl += befehl.SchwereKavallerieAnzahl;
                template.PaladinAnzahl += befehl.PaladinAnzahl;
            }
            foreach (var befehl in Account.GeplanteBewegungen.Where(x => x.EmpfaengerDorf.VillageId.Equals(empfaengerid))) {
                template.SpeertraegerAnzahl += befehl.SpeertraegerAnzahl;
                template.SchwertkaempferAnzahl += befehl.SchwertkaempferAnzahl;
                template.BogenschuetzenAnzahl += befehl.BogenschuetzenAnzahl;
                template.SpaeherAnzahl += befehl.SpaeherAnzahl;
                template.SchwertkaempferAnzahl += befehl.SchwereKavallerieAnzahl;
                template.PaladinAnzahl += befehl.PaladinAnzahl;
            }
            return template;
        }

        private List<PassendeZeitspanne> GetZeitspannenOrdered(List<PassendeZeitspanne> zeitspannen) {
            var templist = new List<PassendeZeitspanne>();
            foreach (var zeitspanne in ZeitspannenProDorf.OrderBy(x => x.Amount))
                if (zeitspannen.Any(x => x.SenderVillageId == zeitspanne.VillageId))
                    templist.Add(zeitspannen.FirstOrDefault(x => x.SenderVillageId == zeitspanne.VillageId));
            return templist.OrderByDescending(x => x.TravelTime).ToList();
        }

        private List<PassendeZeitspanne> IsPassend(TimeSpan ts, int villageId, Queue<TruppenTemplate> templates = null) {
            var versendenZeitspannen = _model.ZeitSpannen.Where(x => x.Zeitart.Equals("Absenden")).OrderBy(x => x.Von).ToList();
            var ankommenZeitspannen = _model.ZeitSpannen.Where(x => x.Zeitart.Equals("Empfangen")).OrderBy(x => x.Von).ToList();

            var passendeZeitspannen = new List<PassendeZeitspanne>();

            foreach (var zeitspanne in versendenZeitspannen)
                foreach (var zeitspanneempfangen in ankommenZeitspannen)
                    if (zeitspanne.Von.Add(ts) >= zeitspanneempfangen.Von && zeitspanne.Von.Add(ts) <= zeitspanneempfangen.Bis && zeitspanne.Von.Add(ts).Hour > App.Settings.Weltensettings.NachtbonusBis - 1 &&
                        zeitspanne.Von.Add(ts).Hour < App.Settings.Weltensettings.NachtbonusVon + 1 ||
                        zeitspanne.Bis.Add(ts) >= zeitspanneempfangen.Von && zeitspanne.Bis.Add(ts) <= zeitspanneempfangen.Bis && zeitspanne.Bis.Add(ts).Hour > App.Settings.Weltensettings.NachtbonusBis - 1 &&
                        zeitspanne.Bis.Add(ts).Hour < App.Settings.Weltensettings.NachtbonusVon + 1 ||
                        zeitspanneempfangen.Von.Add(-ts) >= zeitspanne.Von && zeitspanneempfangen.Von.Add(-ts) <= zeitspanne.Bis && zeitspanneempfangen.Von.Add(-ts).Hour > App.Settings.Weltensettings.NachtbonusBis - 1 &&
                        zeitspanneempfangen.Von.Add(-ts).Hour < App.Settings.Weltensettings.NachtbonusVon + 1 || zeitspanneempfangen.Bis.Add(-ts) >= zeitspanne.Von && zeitspanneempfangen.Bis.Add(-ts) <= zeitspanne.Bis &&
                        zeitspanneempfangen.Bis.Add(-ts).Hour > App.Settings.Weltensettings.NachtbonusBis - 1 && zeitspanneempfangen.Bis.Add(-ts).Hour < App.Settings.Weltensettings.NachtbonusVon + 1) {
                        var zeitspannepassend = new PassendeZeitspanne();

                        var mustaddTimeSpan = new TimeSpan();

                        if (zeitspanne.Von.Add(ts) < zeitspanneempfangen.Von)
                            mustaddTimeSpan = zeitspanneempfangen.Von - zeitspanne.Von.Add(ts);
                        TimeSpan variableTs;
                        if (zeitspanne.Bis - zeitspanne.Von < zeitspanneempfangen.Bis - zeitspanneempfangen.Von)
                            variableTs = zeitspanne.Bis - zeitspanne.Von - mustaddTimeSpan;
                        else
                            variableTs = zeitspanneempfangen.Bis - zeitspanneempfangen.Von;
                        zeitspannepassend.MustAddTimeSpanToStartTime = mustaddTimeSpan;
                        zeitspannepassend.AddableTimeSpan = variableTs;
                        zeitspannepassend.ZeitspanneidAbsenden = zeitspanne.Id;
                        zeitspannepassend.ZeitspanneidEmpfangen = zeitspanneempfangen.Id;
                        zeitspannepassend.SenderVillageId = villageId;
                        zeitspannepassend.TravelTime = ts;
                        zeitspannepassend.Templates = templates;
                        zeitspannepassend.TemplateCount = templates?.Count ?? 0;

                        passendeZeitspannen.Add(zeitspannepassend);
                        AddZeitspanneProDorfToList(zeitspannepassend.SenderVillageId);
                    }
            return passendeZeitspannen;
        }

        public static List<DorfZuTemplate> GetAllVillagesWithEnoughTroopsForDeff(AsyncObservableCollection<DorfViewModel> doerfer, AsyncObservableCollection<TruppenTemplateViewModel> templates) {
            var dorfliste = new List<DorfZuTemplate>();
            foreach (var dorf in doerfer) {
                var templatequeue = new Queue<TruppenTemplate>();
                var template = templates.FirstOrDefault(x => x.TemplateName.Contains(Resources.DefendMinTemplate));
                var templateMax = templates.FirstOrDefault(x => x.TemplateName.Contains(Resources.DefendMaxTemplate));
                var truppenverfuegbar = DorfinformationenModul.DorfinformationenModul.GetTroopsOfVillage(dorf.VillageId);


                bool hasenoughtroops;
                do {
                    hasenoughtroops = truppenverfuegbar.SpeertraegerAnzahl > template?.TruppenTemplate.SpeertraegerAnzahl && truppenverfuegbar.SpeertraegerAnzahl > 0 && template.TruppenTemplate.SpeertraegerAnzahl != 0 ||
                                      truppenverfuegbar.SchwertkaempferAnzahl > template?.TruppenTemplate.SchwertkaempferAnzahl && truppenverfuegbar.SchwertkaempferAnzahl > 0 && template.TruppenTemplate.SchwertkaempferAnzahl != 0 ||
                                      truppenverfuegbar.BogenschuetzenAnzahl > template?.TruppenTemplate.BogenschuetzenAnzahl && truppenverfuegbar.BogenschuetzenAnzahl > 0 && template.TruppenTemplate.BogenschuetzenAnzahl != 0 ||
                                      truppenverfuegbar.SpaeherAnzahl > template?.TruppenTemplate.SpaeherAnzahl && truppenverfuegbar.SpaeherAnzahl > 0 && template.TruppenTemplate.SpaeherAnzahl != 0 ||
                                      truppenverfuegbar.SchwereKavallerieAnzahl > template?.TruppenTemplate.SchwereKavallerieAnzahl && truppenverfuegbar.SchwereKavallerieAnzahl > 0 && template.TruppenTemplate.SchwereKavallerieAnzahl != 0 ||
                                      truppenverfuegbar.PaladinAnzahl > template?.TruppenTemplate.PaladinAnzahl && truppenverfuegbar.PaladinAnzahl > 0 && template.TruppenTemplate.PaladinAnzahl != 0;
                    if (hasenoughtroops) {
                        var template2 = new TruppenTemplate {
                                                                SpeertraegerAnzahl =
                                                                    templateMax != null && truppenverfuegbar.SpeertraegerAnzahl > templateMax.TruppenTemplate.SpeertraegerAnzahl ? templateMax.TruppenTemplate.SpeertraegerAnzahl
                                                                        : truppenverfuegbar.SpeertraegerAnzahl,
                                                                SchwertkaempferAnzahl =
                                                                    templateMax != null && truppenverfuegbar.SchwertkaempferAnzahl > templateMax.TruppenTemplate.SchwertkaempferAnzahl ? templateMax.TruppenTemplate.SchwertkaempferAnzahl
                                                                        : truppenverfuegbar.SchwertkaempferAnzahl,
                                                                BogenschuetzenAnzahl =
                                                                    templateMax != null && truppenverfuegbar.BogenschuetzenAnzahl > templateMax.TruppenTemplate.BogenschuetzenAnzahl ? templateMax.TruppenTemplate.BogenschuetzenAnzahl
                                                                        : truppenverfuegbar.BogenschuetzenAnzahl,
                                                                SpaeherAnzahl =
                                                                    templateMax != null && truppenverfuegbar.SpaeherAnzahl > templateMax.TruppenTemplate.SpaeherAnzahl ? templateMax.TruppenTemplate.SpaeherAnzahl : truppenverfuegbar.SpaeherAnzahl,
                                                                SchwereKavallerieAnzahl =
                                                                    templateMax != null && truppenverfuegbar.SchwereKavallerieAnzahl > templateMax.TruppenTemplate.SchwereKavallerieAnzahl ? templateMax.TruppenTemplate.SchwereKavallerieAnzahl
                                                                        : truppenverfuegbar.SchwereKavallerieAnzahl,
                                                                PaladinAnzahl = templateMax != null && truppenverfuegbar.PaladinAnzahl > templateMax.TruppenTemplate.PaladinAnzahl ? templateMax.TruppenTemplate.PaladinAnzahl
                                                                                    : truppenverfuegbar.PaladinAnzahl
                                                            };

                        templatequeue.Enqueue(template2);
                        truppenverfuegbar.SpeertraegerAnzahl -= template2.SpeertraegerAnzahl;
                        truppenverfuegbar.SchwertkaempferAnzahl -= template2.SchwertkaempferAnzahl;
                        truppenverfuegbar.BogenschuetzenAnzahl -= template2.BogenschuetzenAnzahl;
                        truppenverfuegbar.SpaeherAnzahl -= template2.SpaeherAnzahl;
                        truppenverfuegbar.SchwereKavallerieAnzahl -= template2.SchwereKavallerieAnzahl;
                        truppenverfuegbar.PaladinAnzahl -= template2.PaladinAnzahl;
                    }
                } while (hasenoughtroops);

                dorfliste.Add(new DorfZuTemplate {Dorf = dorf, TemplatePassReinAmount = templatequeue.Count, Templates = templatequeue});
            }
            return dorfliste;
        }

        private List<Dorf> GetAllVillagesWithEnoughtTroopsForTemplate(AsyncObservableCollection<DorfViewModel> doerfer, TruppenTemplateViewModel template) {
            var doerferPassend = new List<Dorf>();
            foreach (var dorf in doerfer) {
                var truppentemplate = DorfinformationenModul.DorfinformationenModul.GetTroopsOfVillage(dorf.VillageId);
                if (FarmassistentModul.TruppenVorhanden(template.TruppenTemplate, truppentemplate))
                    doerferPassend.Add(dorf.Dorf);
            }
            return doerferPassend;
        }

        private List<DorfZuTemplate> GetAllVillagesWithEnoughTroops(AsyncObservableCollection<DorfViewModel> doerfer, AsyncObservableCollection<TruppenTemplateViewModel> templates) {
            var dorfliste = new List<DorfZuTemplate>();
            foreach (var dorf in doerfer) {
                var addit = true;
                var count = 0;
                var truppentemplate = DorfinformationenModul.DorfinformationenModul.GetTroopsOfVillage(dorf.VillageId);
                foreach (var template in templates.Where(x => !x.TemplateName.Contains("Gesamt"))) {
                    if (truppentemplate.SpeertraegerAnzahl < template.TruppenTemplate.SpeertraegerAnzahl)
                        addit = false;
                    if (truppentemplate.SchwertkaempferAnzahl < template.TruppenTemplate.SchwertkaempferAnzahl)
                        addit = false;
                    if (truppentemplate.AxtkaempferAnzahl < template.TruppenTemplate.AxtkaempferAnzahl)
                        addit = false;
                    if (truppentemplate.BogenschuetzenAnzahl < template.TruppenTemplate.BogenschuetzenAnzahl)
                        addit = false;
                    if (truppentemplate.SpaeherAnzahl < template.TruppenTemplate.SpaeherAnzahl)
                        addit = false;
                    if (truppentemplate.LeichteKavallerieAnzahl < template.TruppenTemplate.LeichteKavallerieAnzahl)
                        addit = false;
                    if (truppentemplate.BeritteneBogenschuetzenAnzahl < template.TruppenTemplate.BeritteneBogenschuetzenAnzahl)
                        addit = false;
                    if (truppentemplate.SchwereKavallerieAnzahl < template.TruppenTemplate.SchwereKavallerieAnzahl)
                        addit = false;
                    if (truppentemplate.RammboeckeAnzahl < template.TruppenTemplate.RammboeckeAnzahl)
                        addit = false;
                    if (truppentemplate.KatapulteAnzahl < template.TruppenTemplate.KatapulteAnzahl)
                        addit = false;
                    if (truppentemplate.PaladinAnzahl < template.TruppenTemplate.PaladinAnzahl)
                        addit = false;
                    if (truppentemplate.AdelsgeschlechterAnzahl < template.TruppenTemplate.AdelsgeschlechterAnzahl)
                        addit = false;
                    if (addit) {
                        var countlist = new List<int>();
                        if (template.TruppenTemplate.SpeertraegerAnzahl > 0) {
                            var temp = (double) truppentemplate.SpeertraegerAnzahl / template.TruppenTemplate.SpeertraegerAnzahl;
                            countlist.Add((int) temp);
                        }
                        if (template.TruppenTemplate.SchwertkaempferAnzahl > 0) {
                            var temp = (double) truppentemplate.SchwertkaempferAnzahl / template.TruppenTemplate.SchwertkaempferAnzahl;
                            countlist.Add((int) temp);
                        }
                        if (template.TruppenTemplate.AxtkaempferAnzahl > 0) {
                            var temp = (double) truppentemplate.AxtkaempferAnzahl / template.TruppenTemplate.AxtkaempferAnzahl;
                            countlist.Add((int) temp);
                        }
                        if (template.TruppenTemplate.BogenschuetzenAnzahl > 0) {
                            var temp = (double) truppentemplate.BogenschuetzenAnzahl / template.TruppenTemplate.BogenschuetzenAnzahl;
                            countlist.Add((int) temp);
                        }
                        if (template.TruppenTemplate.SpaeherAnzahl > 0) {
                            var temp = (double) truppentemplate.SpaeherAnzahl / template.TruppenTemplate.SpaeherAnzahl;
                            countlist.Add((int) temp);
                        }
                        if (template.TruppenTemplate.LeichteKavallerieAnzahl > 0) {
                            var temp = (double) truppentemplate.LeichteKavallerieAnzahl / template.TruppenTemplate.LeichteKavallerieAnzahl;
                            countlist.Add((int) temp);
                        }
                        if (template.TruppenTemplate.BeritteneBogenschuetzenAnzahl > 0) {
                            var temp = (double) truppentemplate.BeritteneBogenschuetzenAnzahl / template.TruppenTemplate.BeritteneBogenschuetzenAnzahl;
                            countlist.Add((int) temp);
                        }
                        if (template.TruppenTemplate.SchwereKavallerieAnzahl > 0) {
                            var temp = (double) truppentemplate.SchwereKavallerieAnzahl / template.TruppenTemplate.SchwereKavallerieAnzahl;
                            countlist.Add((int) temp);
                        }
                        if (template.TruppenTemplate.RammboeckeAnzahl > 0) {
                            var temp = (double) truppentemplate.RammboeckeAnzahl / template.TruppenTemplate.RammboeckeAnzahl;
                            countlist.Add((int) temp);
                        }
                        if (template.TruppenTemplate.KatapulteAnzahl > 0) {
                            var temp = (double) truppentemplate.KatapulteAnzahl / template.TruppenTemplate.KatapulteAnzahl;
                            countlist.Add((int) temp);
                        }
                        if (template.TruppenTemplate.PaladinAnzahl > 0) {
                            var temp = (double) truppentemplate.PaladinAnzahl / template.TruppenTemplate.PaladinAnzahl;
                            countlist.Add((int) temp);
                        }
                        if (template.TruppenTemplate.AdelsgeschlechterAnzahl > 0) {
                            var temp = (double) truppentemplate.AdelsgeschlechterAnzahl / template.TruppenTemplate.AdelsgeschlechterAnzahl;
                            countlist.Add((int) temp);
                        }
                        countlist.RemoveAll(x => x.Equals(0));

                        count = countlist.FirstOrDefault();
                        count = countlist.Concat(new[] {
                                                           count
                                                       }).Min();
                    }
                }
                if (addit)
                    dorfliste.Add(new DorfZuTemplate {Dorf = dorf, TemplatePassReinAmount = count});
            }
            return dorfliste;
        }


        private void AddZeitspanneProDorfToList(int villageid) {
            if (ZeitspannenProDorf == null)
                ZeitspannenProDorf = new List<ZeitspanneAmountProDorf>();
            var zeitspanne = new ZeitspanneAmountProDorf();
            if (ZeitspannenProDorf.Any(x => x.VillageId == villageid))
                zeitspanne = ZeitspannenProDorf.FirstOrDefault(x => x.VillageId == villageid);
            else {
                zeitspanne.VillageId = villageid;
                ZeitspannenProDorf.Add(zeitspanne);
            }
            zeitspanne.Amount++;
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
