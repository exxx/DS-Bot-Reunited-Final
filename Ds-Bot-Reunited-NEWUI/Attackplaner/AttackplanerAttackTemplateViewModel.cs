﻿using System;
using Ds_Bot_Reunited_NEWUI.Angreifensettings;

namespace Ds_Bot_Reunited_NEWUI.Attackplaner {
	[Serializable]
    public class AttackplanerAttackTemplateViewModel {
        public Bewegungstyp Bewegungstyp { get; set; }
        public TruppenTemplateViewModel Template { get; set; }
        public int MillisecondsDelay { get; set; }

        public AttackplanerAttackTemplateViewModel Clone() {
            AttackplanerAttackTemplateViewModel model = new AttackplanerAttackTemplateViewModel();
            model.Bewegungstyp = Bewegungstyp;
            model.Template = Template;
            model.MillisecondsDelay = MillisecondsDelay;
            return model;
        }
    }
}