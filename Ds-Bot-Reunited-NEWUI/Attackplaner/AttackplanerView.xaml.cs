﻿using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Ds_Bot_Reunited_NEWUI.SpieldatenViewModel;

namespace Ds_Bot_Reunited_NEWUI.Attackplaner {
    public partial class AttackplanerView {
        public AttackplanerView(Window owner, AttackplanerViewModel datacontext) {
            Owner = owner;
            DataContext = datacontext;
            datacontext.CloseAction += Close;
            InitializeComponent();
        }

        private void Save(object sender, RoutedEventArgs e) {
            Close();
        }

        private void AbsendeDoerfer_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            if (((AttackplanerViewModel) DataContext)?.SelectedEmpfangsDoerfer != null) {
                ((AttackplanerViewModel) DataContext).SelectedAbsendeDoerfer.Clear();
                ((AttackplanerViewModel) DataContext).SelectedAbsendeDoerfer.AddRange(AbsendeDoerfer.SelectedItems.Cast<DorfViewModel>());
            }
        }

        private void Empfangsdoerfer_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            if (((AttackplanerViewModel) DataContext)?.SelectedEmpfangsDoerfer != null) {
                ((AttackplanerViewModel) DataContext).SelectedEmpfangsDoerfer.Clear();
                ((AttackplanerViewModel) DataContext).SelectedEmpfangsDoerfer.AddRange(Empfangsdoerfer.SelectedItems.Cast<DorfViewModel>());
            }
        }

        private void Zeitspannen_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            if (((AttackplanerViewModel) DataContext)?.SelectedZeitSpannen != null) {
                ((AttackplanerViewModel) DataContext).SelectedZeitSpannen.Clear();
                ((AttackplanerViewModel) DataContext).SelectedZeitSpannen.AddRange(Zeitspannen.SelectedItems.Cast<ZeitspanneViewModel>());
            }
        }
    }
}
