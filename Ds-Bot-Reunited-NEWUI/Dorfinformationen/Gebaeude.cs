﻿using System;

namespace Ds_Bot_Reunited_NEWUI.Dorfinformationen {
    [Serializable]
    public class Gebaeude {
        public int Hauptgebaeude { get; set; }
        public int Kaserne { get; set; }
        public int Stall { get; set; }
        public int Werkstatt { get; set; }
        public int Kirche { get; set; }
        public int ErsteKirche { get; set; }
        public int Schmiede { get; set; }
        public int Adelshof { get; set; }
        public int Watchtower { get; set; }
        public int Versammlungsplatz { get; set; }
        public int Statue { get; set; }
        public int Marktplatz { get; set; }
        public int Holzfaeller { get; set; }
        public int Lehmgrube { get; set; }
        public int Eisenmine { get; set; }
        public int Bauernhof { get; set; }
        public int Speicher { get; set; }
        public int Versteck { get; set; }
        public int Wall { get; set; }

        public int Stufe(BauenModul.Gebaeude Gebaeude) {
            switch (Gebaeude) {
                case BauenModul.Gebaeude.Hauptgebaeude:
                    return Hauptgebaeude;
                case BauenModul.Gebaeude.Kaserne:
                    return Kaserne;
                case BauenModul.Gebaeude.Stall:
                    return Stall;
                case BauenModul.Gebaeude.Werkstatt:
                    return Werkstatt;
                case BauenModul.Gebaeude.Kirche:
                    return Kirche;
                case BauenModul.Gebaeude.Erstekirche:
                    return ErsteKirche;
                case BauenModul.Gebaeude.Adelshof:
                    return Adelshof;
                case BauenModul.Gebaeude.Schmiede:
                    return Schmiede;
                case BauenModul.Gebaeude.Versammlungsplatz:
                    return Versammlungsplatz;
                case BauenModul.Gebaeude.Statue:
                    return Statue;
                case BauenModul.Gebaeude.Marktplatz:
                    return Marktplatz;
                case BauenModul.Gebaeude.Holzfaeller:
                    return Holzfaeller;
                case BauenModul.Gebaeude.Lehmgrube:
                    return Lehmgrube;
                case BauenModul.Gebaeude.Eisenmine:
                    return Eisenmine;
                case BauenModul.Gebaeude.Speicher:
                    return Speicher;
                case BauenModul.Gebaeude.Bauernhof:
                    return Bauernhof;
                case BauenModul.Gebaeude.Versteck:
                    return Versteck;
                case BauenModul.Gebaeude.Wall:
                    return Wall;
                case BauenModul.Gebaeude.WatchTower:
                    return Watchtower;
                default:
                    return 0;
            }
        }

        public Gebaeude() { }

        public Gebaeude Clone() {
            Gebaeude newGebaeude = new Gebaeude {
                Adelshof = Adelshof,
                Bauernhof = Bauernhof,
                Eisenmine = Eisenmine,
                ErsteKirche = ErsteKirche,
                Hauptgebaeude = Hauptgebaeude,
                Holzfaeller = Holzfaeller,
                Kaserne = Kaserne,
                Kirche = Kirche,
                Lehmgrube = Lehmgrube,
                Marktplatz = Marktplatz,
                Schmiede = Schmiede,
                Speicher = Speicher,
                Stall = Stall,
                Statue = Statue,
                Versammlungsplatz = Versammlungsplatz,
                Versteck = Versteck,
                Wall = Wall,
                Watchtower = Watchtower,
                Werkstatt = Werkstatt
            };
            return newGebaeude;
        }
    }
}