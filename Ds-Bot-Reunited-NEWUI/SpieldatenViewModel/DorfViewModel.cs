﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.CompilerServices;
using Ds_Bot_Reunited_NEWUI.Annotations;
using Ds_Bot_Reunited_NEWUI.Spieldaten;

namespace Ds_Bot_Reunited_NEWUI.SpieldatenViewModel {
    [Serializable]
    public class DorfViewModel : INotifyPropertyChanged {
        private bool _isSelected;
        private Color _isUsed;
        private int _bewegungenCount;

        public Dorf Dorf { get; set; }

        public bool IsSelected {
            get => _isSelected;
            set {
                _isSelected = value;
                OnPropertyChanged();
            }
        }

        public Color IsUsed {
            get => _isUsed;
            set {
                _isUsed = value;
                OnPropertyChanged();
            }
        }

        public int BewegungenCount {
            get => _bewegungenCount;
            set {
                _bewegungenCount = value;
                OnPropertyChanged();
            }
        }

        public string Name {
            get {
                if (Dorf != null) {
                    return Dorf.VillageName + (!string.IsNullOrEmpty(Dorf.VillageName) ? " || (" + Dorf.XCoordinate + "|" + Dorf.YCoordinate + ") || " + Dorf.VillagePoints + " || " + Dorf.PlayerName : "");
                }
                return "";
            }
        }

        public int PlayerId {
            get {
                if (Dorf != null) {
                    return Dorf.PlayerId;
                }
                return 0;
            }
        }

        public int XCoordinate {
            get {
                if (Dorf != null) {
                    return Dorf.XCoordinate;
                }
                return 0;
            }
        }

        public int YCoordinate {
            get {
                if (Dorf != null) {
                    return Dorf.YCoordinate;
                }
                return 0;
            }
        }

        public int VillageId {
            get {
                if (Dorf != null) {
                    return Dorf.VillageId;
                }
                return 0;
            }
        }

        public int VillagePoints => Dorf.VillagePoints;
        public string Notiz { get; set; }

        public DorfViewModel(Dorf dorf) {
            Dorf = dorf;
            IsUsed = Color.Red;
        }

        public DorfViewModel() {
            IsUsed = Color.Red;
        }

        public DorfViewModel Clone() {
            DorfViewModel newDorfViewModel = new DorfViewModel {BewegungenCount = BewegungenCount, Notiz = "", Dorf = Dorf.Clone()};
            return newDorfViewModel;
        }


        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
