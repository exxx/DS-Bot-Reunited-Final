﻿using System;
using Ds_Bot_Reunited_NEWUI.Spieldaten;

namespace Ds_Bot_Reunited_NEWUI.SpieldatenViewModel {
    [Serializable]
    public class SpielerViewModel {
        public Spieler Spieler { get; set; }
        public string PlayerName => Spieler.PlayerName;
        public int PlayerId => Spieler.PlayerId;

        public SpielerViewModel(Spieler spieler) {
            Spieler = spieler;
        }

        public SpielerViewModel() { }
    }
}