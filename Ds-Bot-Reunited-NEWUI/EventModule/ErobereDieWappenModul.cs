﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ds_Bot_Reunited_NEWUI.Accountsettings.Ds_Bot_Reunited.Accountsettings;
using Ds_Bot_Reunited_NEWUI.BrowserManager;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using OpenQA.Selenium;

namespace Ds_Bot_Reunited_NEWUI.EventModule {
    public class ErobereDieWappenModul {
        public static async Task Start(Browser browser, Accountsetting accountsettings) {
            try {
                App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " +
                    " Erobere die Wappen started");
                browser.WebDriver.Navigate()
                    .GoToUrl(App.Settings.OperateLink +
                             accountsettings.UvZusatzFuerLink + "&screen=event_crest");
                await Rndm.Sleep(100, 200);


                for (int i = 0; i < 10; i++) {
                    if (!browser.WebDriver.FindElement(By.XPath("//*[@id='content_value']/table[1]/tbody/tr[3]/td[2]")).Text.Contains("0")) {
                        List<IWebElement> seitenbuttons =
                            browser.WebDriver.FindElements(By.XPath("//a[@class='paged-nav-v2']")).ToList();
                        if (i > 0) {
                            try {
                                seitenbuttons[i].Click();
                            } catch { }
                            await Rndm.Sleep(300, 500);
                        }
                        IWebElement table = browser.WebDriver.FindElement(By.XPath("//table[@id='challenge_table']"));
                        List<IWebElement> buttons =
                            table.FindElements(By.XPath("//a[contains(@href, '&screen=event_crest&') and contains(@href, 'player_id')]")).ToList();
                        int count = 0;
                        while (count < 8 && buttons.Any() && !browser.WebDriver.FindElement(By.XPath("//*[@id='content_value']/table[1]/tbody/tr[3]/td[2]")).Text.Contains("0")) {
                            try {
                                ((IJavaScriptExecutor) browser.WebDriver).ExecuteScript(
                                    "$('#chat-wrapper').hide();");
                                buttons[0].Click();
                                await Rndm.Sleep(400, 500);
                                table = browser.WebDriver.FindElement(By.XPath("//table[@id='challenge_table']"));
                                buttons =
                                    table.FindElements(By.XPath("//a[contains(@href, '&screen=event_crest&') and contains(@href, 'player_id')]")).ToList();
                            } catch {
                                buttons = new List<IWebElement>();
                            }
                            count++;
                        }
                    }
                }
                App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " +
                    " Erobere die Wappen ended");
            } catch (Exception e) {
                App.LogString("Erobere die Wappen Failure: " + e);
            }
        }
    }
}