﻿using System.Linq;
using System.Threading.Tasks;
using Ds_Bot_Reunited_NEWUI.Accountsettings.Ds_Bot_Reunited.Accountsettings;
using Ds_Bot_Reunited_NEWUI.BrowserManager;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.Settings;
using OpenQA.Selenium;

namespace Ds_Bot_Reunited_NEWUI.EventModule {
    public class JahrmarktDesAdelsModul {
        public static async Task Start(Browser browser, Accountsetting accountsettings) {
            App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " +
" Jahrmarkt started");
            AccountDorfSettings dorf =
                accountsettings.DoerferSettings.FirstOrDefault(x => !string.IsNullOrEmpty(x.Dorf.Dorf.VillageName));
            browser.WebDriver.Navigate()
                .GoToUrl(App.Settings.OperateLink +
                         (dorf != null
                             ? "village=" +
                               dorf.Dorf.VillageId
                             : "") + "" +
                         accountsettings.UvZusatzFuerLink + "&screen=event_wheel");
            await Rndm.Sleep(100, 200);
            try {
                ((IJavaScriptExecutor) browser.WebDriver).ExecuteScript(
                    "$('#chat-wrapper').hide();");
                if (browser.WebDriver.FindElements(By.XPath("//a[@id='wheel_spin_button']")).Count > 0) {
                    try {
                        browser.WebDriver.FindElement(By.XPath("//a[@id='wheel_spin_button']")).Click();
                    } catch { }
                    await Rndm.Sleep(1000, 1500);
                }
                if (browser.WebDriver.FindElements(By.XPath("//a[@id='wheel_spin_button']")).Count > 0) {
                    try {
                        browser.WebDriver.FindElement(By.XPath("//a[@id='wheel_spin_button']")).Click();
                    } catch { }
                    await Rndm.Sleep(1000, 1500);
                }
                if (
                    browser.WebDriver.FindElements(By.XPath("//a[contains(@href,'&screen=event_wheel&action=activity')]"))
                        .Count > 0) {
                    ((IJavaScriptExecutor) browser.WebDriver).ExecuteScript(
                        "arguments[0].scrollIntoView(true);",
                        browser.WebDriver.FindElements(
                                By.XPath("//a[contains(@href,'&screen=event_wheel&action=activity')]"))
                            [0]);
                    browser.WebDriver.FindElements(By.XPath("//a[contains(@href,'&screen=event_wheel&action=activity')]"))
                        [0]
                        .Click();
                    await Rndm.Sleep(200, 400);
                    ((IJavaScriptExecutor) browser.WebDriver).ExecuteScript(
                        "$('#chat-wrapper').hide();");
                }
                if (browser.WebDriver.FindElements(By.XPath("//a[@id='wheel_spin_button']")).Count > 0) {
                    try {
                        browser.WebDriver.FindElement(By.XPath("//a[@id='wheel_spin_button']")).Click();
                    } catch { }
                    await Rndm.Sleep(1000, 1500);
                }
            } catch {
                // ignored
            }
            App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " +
" Jahrmarkt Account: ended");
        }
    }
}