﻿using System;
using System.Threading.Tasks;
using Ds_Bot_Reunited_NEWUI.Accountsettings.Ds_Bot_Reunited.Accountsettings;
using Ds_Bot_Reunited_NEWUI.AngreifenModul;
using Ds_Bot_Reunited_NEWUI.BrowserManager;
using Ds_Bot_Reunited_NEWUI.Farmen;
using Ds_Bot_Reunited_NEWUI.Farmmodul;
using Ds_Bot_Reunited_NEWUI.Settings;
using Ds_Bot_Reunited_NEWUI.TimeModul;

namespace Ds_Bot_Reunited_NEWUI.MultiBewegungenModul {
    public class MultiBewegungenModul {
        public static async Task SendBewegungen(Browser browser, AccountDorfSettings dorf, Accountsetting accountsettings) {
            foreach (var ziel in App.Settings.Ziele) {
                TimeSpan ts = Laufzeit.GetTravelTime(dorf.Dorf.Dorf, ziel.EmpfaengerDorf.Dorf, ziel.MinimumTruppenproAngriff);
                TruppenTemplate truppen = DorfinformationenModul.DorfinformationenModul.GetAvailableTroops(browser, dorf);
                if (DateTime.Now.Add(ts) < ziel.SpaetesterAnkunftszeitpunkt && FarmassistentModul.TruppenVorhanden(ziel.MinimumTruppenproAngriff, truppen)) {
                    await Versammlungsplatzmodul.SendBewegungOverVersammlungsplatz(browser, ziel.EmpfaengerDorf.XCoordinate, ziel.EmpfaengerDorf.YCoordinate, truppen, dorf, ziel.Bewegungsart, accountsettings);
                    ziel.AngriffeCount++;
                    //TODO EmpfangendeTruppen hinzufügen
                }
            }
        }
    }
}