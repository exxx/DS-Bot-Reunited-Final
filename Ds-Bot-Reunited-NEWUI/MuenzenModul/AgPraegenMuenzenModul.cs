﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Ds_Bot_Reunited_NEWUI.Accountsettings.Ds_Bot_Reunited.Accountsettings;
using Ds_Bot_Reunited_NEWUI.BrowserManager;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.Settings;
using OpenQA.Selenium;

namespace Ds_Bot_Reunited_NEWUI.MuenzenModul {
    public class AgPraegenMuenzenModul {
        //<table class="vis">
        //<tbody><tr>
        //<td colspan = "2" >
        //< select name="factor">
        //<option value = "0" > -nichts -</ option >
        //< option value="1" selected="selected">1x(28000, 30000, 25000)</option>

        //</select>
        //<input class="btn" type="submit" value="Einlagern">
        //</td>
        //</tr>
        //</tbody></table>
        public static async Task Start(Browser browser, Accountsetting accountsettings) {
            App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + " Minting Gold Coins started");
            foreach (var dorf in accountsettings.DoerferSettings.Where(x => !string.IsNullOrEmpty(x.Dorf.Dorf.VillageName) && x.Dorf.Name.Contains(!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname))) {
                await App.WaitForAttacks();
                if (App.Settings.Weltensettings.AhTillLevel3) {
                    await Einlagern(browser, accountsettings, dorf);
                } else {
                    await PraegeMuenzen(browser, accountsettings, dorf);
                }
            }
            App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + " Minting Gold Coins Account: " + (!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + " ended");
        }

        private static async Task Einlagern(Browser browser, Accountsetting accountsettings, AccountDorfSettings dorf) {
            try {
                if (dorf.MuenzenPraegen > 0) {
                    App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + " Minting Gold Coins Village: " + dorf.Dorf.VillageId + " Name: " + dorf.Dorf.Name + " started");
                    browser.WebDriver.Navigate().GoToUrl(App.Settings.OperateLink + "village=" + dorf.Dorf.VillageId + "" + accountsettings.UvZusatzFuerLink + "&screen=snob");
                    browser.WebDriver.WaitForPageload();
                    int maxcoins = 0;
                    if (browser.WebDriver.FindElements(By.XPath("//a[@id='coin_mint_fill_max']")).Count > 0) {
                        string max = browser.WebDriver.FindElement(By.XPath("//a[@id='coin_mint_fill_max']")).Text;
                        int.TryParse(max.Replace("(", "").Replace(")", ""), out maxcoins);
                    }
                    if (maxcoins > dorf.MuenzenPraegen) {
                        maxcoins = dorf.MuenzenPraegen;
                    }
                    if (maxcoins > 0) {
                        var element = browser.WebDriver.FindElement(By.XPath("//input[@id='coin_mint_count']"));
                        element.Clear();
                        await Rndm.Sleep(100, 200);
                        element.SendKeys(maxcoins.ToString());
                        await Rndm.Sleep(100, 200);
                        browser.WebDriver.FindElement(By.XPath("//input[@class='btn btn-default']")).Click();
                        await Rndm.Sleep(100, 200);
                    }
                    dorf.MuenzenPraegen -= maxcoins;
                    App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + " Minting Gold Coins Village: " + dorf.Dorf.VillageId + " Name: " + dorf.Dorf.Name + " ended");
                }
            } catch (Exception e) {
                App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + " Minting Gold Coins ERROR! " + e);
            }
        }

        private static async Task PraegeMuenzen(Browser browser, Accountsetting accountsettings, AccountDorfSettings dorf) {
            try {
                if (dorf.MuenzenPraegen > 0) {
                    App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + " Minting Gold Coins Village: " + dorf.Dorf.VillageId + " Name: " + dorf.Dorf.Name + " started");
                    browser.WebDriver.Navigate().GoToUrl(App.Settings.OperateLink + "village=" + dorf.Dorf.VillageId + "" + accountsettings.UvZusatzFuerLink + "&screen=snob");
                    browser.WebDriver.WaitForPageload();
                    int maxcoins = 0;
                    if (browser.WebDriver.FindElements(By.XPath("//a[@id='coin_mint_fill_max']")).Count > 0) {
                        string max = browser.WebDriver.FindElement(By.XPath("//a[@id='coin_mint_fill_max']")).Text;
                        int.TryParse(max.Replace("(", "").Replace(")", ""), out maxcoins);
                    }
                    if (maxcoins > dorf.MuenzenPraegen) {
                        maxcoins = dorf.MuenzenPraegen;
                    }
                    if (maxcoins > 0) {
                        var element = browser.WebDriver.FindElement(By.XPath("//input[@id='coin_mint_count']"));
                        element.Clear();
                        await Rndm.Sleep(100, 200);
                        element.SendKeys(maxcoins.ToString());
                        await Rndm.Sleep(100, 200);
                        browser.WebDriver.FindElement(By.XPath("//input[@class='btn btn-default']")).Click();
                        await Rndm.Sleep(100, 200);
                    }
                    dorf.MuenzenPraegen -= maxcoins;
                    App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + " Minting Gold Coins Village: " + dorf.Dorf.VillageId + " Name: " + dorf.Dorf.Name + " ended");
                }
            } catch (Exception e) {
                App.LogString((!string.IsNullOrEmpty(accountsettings.Uvname) ? accountsettings.Uvname : accountsettings.Accountname) + ": " + " Minting Gold Coins ERROR! " + e);
            }
        }
    }
}
