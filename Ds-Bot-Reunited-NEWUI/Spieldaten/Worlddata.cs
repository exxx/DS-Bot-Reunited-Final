﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Data;
using Ds_Bot_Reunited_NEWUI.Constants;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.Properties;
using Ds_Bot_Reunited_NEWUI.Save;
using Ds_Bot_Reunited_NEWUI.Settings;
using Ds_Bot_Reunited_NEWUI.SpieldatenViewModel;

namespace Ds_Bot_Reunited_NEWUI.Spieldaten {
	public static class Worlddata {
		private static bool _worlddataIsLoading;

		public static async Task LoadWorldData() {
			if (!_worlddataIsLoading) {
				_worlddataIsLoading = true;
				if (!string.IsNullOrEmpty(App.Settings.Welt)) {
					var fehlerfrei = false;
					var fehlercounter = 0;
					while (!fehlerfrei && fehlercounter < 3) {
						fehlerfrei = true;
						try {
							App.LogString(Resources.LoadingWorlddata);
							if (App.VillagelistWholeWorld == null)
								App.VillagelistWholeWorld =
									new ConcurrentDictionary<VillageTuple<int, int>, DorfViewModel>();
							foreach (var dorf in XmlVerwaltung.XmlLadenWeltdaten()) {
								var searchTuple = new VillageTuple<int, int>(dorf.XCoordinate, dorf.YCoordinate);
								App.VillagelistWholeWorld.TryAdd(searchTuple, dorf);
							}

							var villagelist = DataHandler.GetVillagesFromWorld(App.Settings.Welt).GetAwaiter().GetResult();
							if (villagelist == null || !villagelist.Any())
								throw new Exception("Failure Loading Village.txt");
							foreach (var dorf in villagelist)
								try {
									var searchTuple = new VillageTuple<int, int>(dorf.XCoordinate, dorf.YCoordinate);
									if (App.VillagelistWholeWorld.ContainsKey(searchTuple)) {
										var xx = App.VillagelistWholeWorld[searchTuple].Dorf;
										xx.PlayerId = dorf.Dorf.PlayerId;
										xx.VillageName = dorf.Dorf.VillageName;
										xx.VillagePoints = dorf.VillagePoints;
										var unterschiedzuVorher = 0;
										if (!xx.VillageId.Equals(0)) {
											if (xx.History.Any())
												unterschiedzuVorher =
													xx.VillagePoints - xx.History.LastOrDefault().NeuePunkte;
											var list = xx
											           .History.Where(x => x.Zeitpunkt < DateTime.Now.AddDays(-4))
											           .ToList();
											for (var i = 0; i < list.Count; i++) xx.History.Remove(list[i]);
											if (xx.History.Any() &&
											    xx.History.OrderBy(x => x.Zeitpunkt).LastOrDefault().Zeitpunkt <
											    DateTime.Now.AddHours(-1) && unterschiedzuVorher > 0)
												xx.History.Add(new PunkteHistorie {
													                                  NeuePunkte = xx.VillagePoints
													                                  , UnterschiedZuVorherigemPunktestand
														                                  = unterschiedzuVorher
													                                  , Zeitpunkt = DateTime.Now
												                                  });
											if (!xx.History.Any())
												xx.History.Add(new PunkteHistorie {
													                                  NeuePunkte = xx.VillagePoints
													                                  , UnterschiedZuVorherigemPunktestand
														                                  = 0
													                                  , Zeitpunkt = DateTime.Now
												                                  });
										}
									} else
										App.VillagelistWholeWorld.TryAdd(searchTuple, dorf);

									if (App.Farmliste.Any(x => x.VillageId.Equals(dorf.VillageId))) {
										var farm =
											App.Farmliste.FirstOrDefault(x => x.VillageId.Equals(dorf.VillageId));
										if (farm.Dorf.PlayerId.Equals(0) && !dorf.Dorf.PlayerId.Equals(0))
											farm.Active = false;
										if (!farm.Dorf.PlayerId.Equals(0) && dorf.Dorf.PlayerId.Equals(0))
											farm.Active = true;
										farm.Dorf = dorf.Dorf;
									}

									if (App.Settings.Accountsettingslist.Any(x =>
										                                         x
											                                         .DoerferSettings
											                                         .Any(k =>
												                                              k
													                                              .Dorf.VillageId
													                                              .Equals(dorf
														                                                      .VillageId) &&
												                                              !k.Dorf.PlayerId
												                                                .Equals(dorf
													                                                        .PlayerId)))
									)
										foreach (var account in App.Settings.Accountsettingslist) {
											var first =
												account.DoerferSettings.FirstOrDefault(k =>
													                                       k
														                                       .Dorf.VillageId
														                                       .Equals(dorf
															                                               .VillageId) &&
													                                       !k.Dorf.PlayerId
													                                         .Equals(dorf.PlayerId));
											if (first != null) account.DoerferSettings.Remove(first);
										}
								} catch {
									// ignored
								}

							if (App.PlayerlistWholeWorld == null) {
								App.PlayerlistWholeWorld = new AsyncObservableCollection<SpielerViewModel>();
								BindingOperations.EnableCollectionSynchronization(App.PlayerlistWholeWorld
								                                                  , App.PlayerlistLock);
							}

							var playerlist = await DataHandler.GetSpielerFromWorld(App.Settings.Welt);
							if (playerlist == null || !playerlist.Any())
								throw new Exception("Failure Loading Player.txt");
							foreach (var player in playerlist)
								try {
									if (App.PlayerlistWholeWorld.Any(x => x.PlayerId.Equals(player?.PlayerId))) {
										var spieler =
											App.PlayerlistWholeWorld
											   .FirstOrDefault(x => x.PlayerId.Equals(player?.PlayerId));
										var xx = spieler?.Spieler;
										if (xx != null) {
											xx.AllyId = player.Spieler.AllyId;
											xx.NumberVillages = player.Spieler.NumberVillages;
											xx.PlayerPoints = player.Spieler.PlayerPoints;
										}
									} else
										App.PlayerlistWholeWorld.Add(player);
								} catch {
									// ignored
								}

							foreach (var dorf in App.VillagelistWholeWorld)
								if (dorf.Value.PlayerId != 0)
									dorf.Value.Dorf.PlayerName =
										App.PlayerlistWholeWorld.FirstOrDefault(x => x.PlayerId == dorf.Value.PlayerId)
										   ?.PlayerName;
						} catch (Exception e) {
							App.LogString(Resources.ErrorLoadingWorlddata + " " + e);
							await Task.Delay(2000);
							fehlerfrei = false;
						}

						fehlercounter++;
					}

					LoadPlayerVillages();
					App.Settings.LastWorldDataCheck = DateTime.Now; 
                    App.LastWorlddataCheck = DateTime.Now;
                }
			}

			_worlddataIsLoading = false;
		}

		public static void LoadPlayerVillages() {
			try {
				if (App.PlayerlistWholeWorld != null) {
					App.LogString(Resources.LoadingPlayers);
					if (App.Settings.Accountsettingslist != null)
						foreach (var accountsetting in App.Settings.Accountsettingslist) {
							var account = !string.IsNullOrEmpty(accountsetting.Uvname)
								              ? accountsetting.Uvname
								              : accountsetting.Accountname;
							var spielerViewModel =
								App.PlayerlistWholeWorld.FirstOrDefault(x => x.PlayerName.Equals(account
								                                                                 , StringComparison
									                                                                 .InvariantCultureIgnoreCase));
							if (spielerViewModel != null) {
								var playerid = spielerViewModel.PlayerId;
								accountsetting.Playerid = playerid;
								var doerferOfAccount =
									new List<DorfViewModel>(App.VillagelistWholeWorld
									                           .Where(x => x.Value.PlayerId.Equals(playerid))
									                           .Select(x => x.Value));
								foreach (var dorf in doerferOfAccount) {
									var newsettings = new AccountDorfSettings(dorf);
									if (accountsetting.DoerferSettings.Any()) {
										var old = accountsetting.DoerferSettings.FirstOrDefault();
										if (old != null) {
											newsettings.FarmTruppenTemplate = old.FarmTruppenTemplate.Clone();
											newsettings.FarmenActive = old.FarmenActive;
											newsettings.Farmeninterval = old.Farmeninterval;
											newsettings.MinDorfFarmeninterval = old.MinDorfFarmeninterval;
											newsettings.FarmMode = old.FarmMode;
											newsettings.MinFarmassistDelay = old.MinFarmassistDelay;
											newsettings.MaxFarmassistDelay = old.MaxFarmassistDelay;
											newsettings.FarmenInactiveFrom = old.FarmenInactiveFrom;
											newsettings.FarmenInactiveTo = old.FarmenInactiveTo;
											newsettings.FarmenTroopsMustBeHomeFrom = old.FarmenTroopsMustBeHomeFrom;
											newsettings.FarmenTroopsMustBeHomeTo = old.FarmenTroopsMustBeHomeTo;
											newsettings.AddVillagesAutomatically = old.AddVillagesAutomatically;
											newsettings.AddVillagesPointsMin = old.AddVillagesPointsMin;
											newsettings.AddVillagesPointsMax = old.AddVillagesPointsMax;
											newsettings.AddVillagesRadiusMin = old.AddVillagesRadiusMin;
											newsettings.AddVillagesRadiusMax = old.AddVillagesRadiusMax;
											newsettings.AddPlayersAutomatically = old.AddPlayersAutomatically;
											newsettings.AddPlayersPointsMin = old.AddPlayersPointsMin;
											newsettings.AddPlayersPointsMax = old.AddPlayersPointsMax;
											newsettings.UseATemplateFarmassist = old.UseATemplateFarmassist;
											newsettings.UseBTemplateFarmassist = old.UseBTemplateFarmassist;
											newsettings.UseCTemplateFarmassist = old.UseCTemplateFarmassist;
											newsettings.TemplateA = old.TemplateA.Clone();
											newsettings.TemplateB = old.TemplateB.Clone();
											newsettings.AddFarmsToFarmassistlistAutomatically =
												old.AddFarmsToFarmassistlistAutomatically;
										}
									}

									if (!accountsetting
									     .DoerferSettings.Any(x => x.Dorf.VillageId.Equals(dorf.VillageId)))
										accountsetting.DoerferSettings.Add(newsettings);
									else {
										var dorfsettings =
											accountsetting
												.DoerferSettings
												.FirstOrDefault(x => x.Dorf.VillageId.Equals(dorf.VillageId));
										if (dorfsettings != null) dorfsettings.Dorf = dorf;
									}

									if (!App.Settings.Gruppen.Any(x => x.Gruppenname.Equals(NAMECONSTANTS
										                                                        .GetNameForServer(Names
											                                                                          .Alle)))
									)
										App.Settings.Gruppen.Add(new GruppeDoerfer {
											                                           Gruppenname =
												                                           NAMECONSTANTS
													                                           .GetNameForServer(Names
														                                                             .Alle)
										                                           });
									var group =
										App.Settings.Gruppen.FirstOrDefault(x => x.Gruppenname.Equals(NAMECONSTANTS
											                                                              .GetNameForServer(Names
												                                                                                .Alle)));
									if (group != null && !group.DoerferIds.Any(x => x.Equals(dorf.VillageId)))
										group.DoerferIds.Add(dorf.VillageId);
								}
							}
						}
				}
			} catch (Exception e) {
				App.LogString(Resources.ErrorLoadingPlayers + ", " + e);
			}
		}

		public static int GetVillageIdForBericht(string koords) {
			koords = koords.Replace("" + Resources.ReportHerkunft + ": ", "");
			var xindex = koords.IndexOf("(", StringComparison.CurrentCulture);
			var yindex = koords.IndexOf(")", StringComparison.CurrentCulture);
			var coords = koords.Substring(xindex + 1, yindex - xindex - 1);
			var xangreifer = int.Parse(coords.Split('|')[0]);
			var yangreifer = int.Parse(coords.Split('|')[1]);
			var searchTuple = new VillageTuple<int, int>(xangreifer, yangreifer);
			var village = App.VillagelistWholeWorld[searchTuple];
			if (village != null) return village.VillageId;
			return -1;
		}

		public static int GetPlayerIdForBericht(string playername) {
			var player =
				App.PlayerlistWholeWorld.FirstOrDefault(x =>
					                                        x
						                                        .PlayerName
						                                        .Equals(playername
							                                                .Replace("" + NAMECONSTANTS.GetNameForServer(Names.ReportAttacker) + ": "
							                                                         , "")));
			if (player != null) return player.PlayerId;
			return 0;
		}
	}
}