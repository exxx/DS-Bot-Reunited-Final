﻿using System;
using System.Collections.Generic;

namespace Ds_Bot_Reunited_NEWUI.Spieldaten {
	[Serializable]
    public class Spieler {
        public int PlayerId { get; set; }
        public int PlayerPoints { get; set; }
        public List<PunkteHistorie> History { get; set; }
        public string PlayerName { get; set; }
        public int AllyId { get; set; }
        public int NumberVillages { get; set; }

        // ReSharper disable once EmptyConstructor        
        public Spieler() {
            History = new List<PunkteHistorie>();
        }

        public Spieler Clone() {
            Spieler newSpieler = new Spieler {
                AllyId = AllyId,
                NumberVillages = NumberVillages,
                PlayerId = PlayerId,
                PlayerName = PlayerName,
                PlayerPoints = PlayerPoints,
                History = new List<PunkteHistorie>()
            };
            foreach (var innn in History) {
                newSpieler.History.Add(innn.Clone());
            }
            return newSpieler;
        }
    }
}