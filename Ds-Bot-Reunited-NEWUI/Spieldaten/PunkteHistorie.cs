﻿using System;

namespace Ds_Bot_Reunited_NEWUI.Spieldaten {
    [Serializable]
    public class PunkteHistorie {
        public int NeuePunkte { get; set; }
        public int UnterschiedZuVorherigemPunktestand { get; set; }
        public DateTime Zeitpunkt { get; set; }

        public PunkteHistorie() { }

        public PunkteHistorie Clone() {
            PunkteHistorie newPunkteHistorie = new PunkteHistorie {
                NeuePunkte = NeuePunkte,
                UnterschiedZuVorherigemPunktestand = UnterschiedZuVorherigemPunktestand,
                Zeitpunkt = Zeitpunkt
            };
            return newPunkteHistorie;
        }
    }
}