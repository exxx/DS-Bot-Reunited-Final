﻿using System;
using System.Collections.Generic;

namespace Ds_Bot_Reunited_NEWUI.Spieldaten {
    [Serializable]
    public class GruppeDoerfer {
        public List<int> DoerferIds { get; set; }
        public string Gruppenname { get; set; }
        public int GruppenId { get; set; }

        public GruppeDoerfer() {
            DoerferIds = new List<int>();
        }

        public GruppeDoerfer Clone() {
            GruppeDoerfer gruppeDoerfer = new GruppeDoerfer {
                Gruppenname = Gruppenname,
                DoerferIds = new List<int>(),
                GruppenId = GruppenId
            };
            foreach (var id in DoerferIds) {
                gruppeDoerfer.DoerferIds.Add(id);
            }
            return gruppeDoerfer;
        }
    }
}