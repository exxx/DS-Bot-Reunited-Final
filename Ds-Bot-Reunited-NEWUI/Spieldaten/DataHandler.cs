﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Threading.Tasks;
using System.Web;
using System.Xml;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.Settings;
using Ds_Bot_Reunited_NEWUI.SpieldatenViewModel;

namespace Ds_Bot_Reunited_NEWUI.Spieldaten {
	public static class DataHandler {
		private static System.Net.WebClient _wc = new System.Net.WebClient();

        public static async Task<List<DorfViewModel>> GetVillagesFromWorld(string welt) {
			try {
				string uri = "https://" + welt + @"." + App.Settings.Server.GetDescription() + "/map/village.txt";
				byte[] raw = _wc.DownloadData(uri);
				string text = System.Text.Encoding.UTF8.GetString(raw);
				string[] lines = text.Split('\n'); /*File.ReadAllLines("doerferdaten.txt");*/
				return (from line in lines
				        where line.Length > 2
				        select line.Split(',')
				        into splitted
				        where splitted.Length > 1
				        select new DorfViewModel(new Dorf {
					                                          VillageName = HttpUtility.UrlDecode(splitted[1].Trim())
					                                          , VillagePoints = int.Parse(splitted[5].Trim())
					                                          , PlayerId = int.Parse(splitted[4].Trim())
					                                          , VillageId = int.Parse(splitted[0].Trim())
					                                          , XCoordinate = int.Parse(splitted[2].Trim())
					                                          , YCoordinate = int.Parse(splitted[3].Trim())
				                                          })).OrderBy(x => x.Name).ToList();
			} catch {
				Ping sender = new Ping();
				PingReply result = sender.Send("www.google.de");
				while (result != null && result.Status != IPStatus.Success) {
					result = sender.Send("www.google.de");
					App.LogString("Network Timeout");
					await Task.Delay(2000);
				}

				System.Net.WebClient _wc = new System.Net.WebClient();
				string uri = "https://" + welt + @"." + App.Settings.Server.GetDescription() + "/map/village.txt";
				byte[] raw = _wc.DownloadData(uri);
				string text = System.Text.Encoding.UTF8.GetString(raw);
				string[] lines = text.Split('\n');
				return (from line in lines
				        where line.Length > 2
				        select line.Split(',')
				        into splitted
				        where splitted.Length > 1
				        select new DorfViewModel(new Dorf {
					                                          VillageName = HttpUtility.UrlDecode(splitted[1].Trim())
					                                          , VillagePoints = int.Parse(splitted[5].Trim())
					                                          , PlayerId = int.Parse(splitted[4].Trim())
					                                          , VillageId = int.Parse(splitted[0].Trim())
					                                          , XCoordinate = int.Parse(splitted[2].Trim())
					                                          , YCoordinate = int.Parse(splitted[3].Trim())
				                                          })).OrderBy(x => x.Name).ToList();
			}
		}

		public static async Task<List<SpielerViewModel>> GetSpielerFromWorld(string welt) {
			try {
				string uri2 = "https://" + welt + @"." + App.Settings.Server.GetDescription() + "/map/player.txt";
				byte[] raw2 = _wc.DownloadData(uri2);
				string text2 = System.Text.Encoding.UTF8.GetString(raw2);
				string[] lines2 = text2.Split('\n');
				return (from line in lines2
				        where line.Length > 2
				        select line.Split(',')
				        into splitted
				        where splitted.Length > 1
				        select new SpielerViewModel(new Spieler {
					                                                AllyId = int.Parse(splitted[2].Trim())
					                                                , NumberVillages = int.Parse(splitted[3].Trim())
					                                                , PlayerId = int.Parse(splitted[0].Trim())
					                                                , PlayerName =
						                                                HttpUtility.UrlDecode(splitted[1].Trim())
					                                                , PlayerPoints = int.Parse(splitted[4].Trim())
				                                                })).OrderBy(x => x.PlayerName).ToList();
			} catch {
				Ping sender = new Ping();
				PingReply result = sender.Send("www.google.de");
				while (result != null && result.Status != IPStatus.Success) {
					result = sender.Send("www.google.de");
					App.LogString("Network Timeout");
					await Task.Delay(2000);
				}
				
				string uri2 = "https://" + welt + @"." + App.Settings.Server.GetDescription() + "/map/player.txt";
				byte[] raw2 = _wc.DownloadData(uri2);
				string text2 = System.Text.Encoding.UTF8.GetString(raw2);
				string[] lines2 = text2.Split('\n');
				return (from line in lines2
				        where line.Length > 2
				        select line.Split(',')
				        into splitted
				        where splitted.Length > 1
				        select new SpielerViewModel(new Spieler {
					                                                AllyId = int.Parse(splitted[2].Trim())
					                                                , NumberVillages = int.Parse(splitted[3].Trim())
					                                                , PlayerId = int.Parse(splitted[0].Trim())
					                                                , PlayerName =
						                                                HttpUtility.UrlDecode(splitted[1].Trim())
					                                                , PlayerPoints = int.Parse(splitted[4].Trim())
				                                                })).OrderBy(x => x.PlayerName).ToList();
			}
		}

		public static async Task GetWorldSettings(WeltenSettings weltenSettings) {
			if (!string.IsNullOrEmpty(App.Settings.Welt)) {
				bool fehler;
				do {
					fehler = false;
					try {
						string uri = "https://" + App.Settings.Welt + @"." + App.Settings.Server.GetDescription() +
						             "/interface.php?func=get_config";
						byte[] raw = _wc.DownloadData(uri);
						string text = System.Text.Encoding.UTF8.GetString(raw);
						XmlDocument doc = new XmlDocument();
						doc.LoadXml(text);


						weltenSettings.MillisecondsActive = GetBoolOfElement(doc, "millis_arrival");

						weltenSettings.BogenschuetzenActive = GetBoolOfElement(doc, "archer");
						weltenSettings.PaladinActive = GetBoolOfElement(doc, "knight");
						weltenSettings.FakeActive = GetBoolOfElement(doc, "fake_limit");
						weltenSettings.AhTillLevel3 = !GetBoolOfElement(doc, "gold");

						weltenSettings.KircheActive = GetBoolOfElement(doc, "church");
						weltenSettings.WatchtowerActive = GetBoolOfElement(doc, "watchtower");
						weltenSettings.TenTechLevel = GetIntOfElement(doc, "tech") == 0;

						weltenSettings.NachtbonusVon = GetIntOfElement(doc, "start_hour");
						weltenSettings.NachtbonusBis = GetIntOfElement(doc, "end_hour");
						weltenSettings.Angriffsschutztage = GetIntOfElement(doc, "days");
						weltenSettings.Spielgeschwindigkeit = GetIntOfElement(doc, "speed");

						var baseproduction = GetIntOfElement(doc, "base_production");
						weltenSettings.RessourcenProductionFaktor = (double) baseproduction / 30;

						_wc = new System.Net.WebClient();
						uri = "https://" + App.Settings.Welt + @"." + App.Settings.Server.GetDescription() +
						      "/interface.php?func=get_unit_info";
						byte[] raw2 = _wc.DownloadData(uri);
						string text2 = System.Text.Encoding.UTF8.GetString(raw2);
						doc.LoadXml(text2);

						var spear = doc.GetElementsByTagName("spear");
						weltenSettings.SpeertraegerLaufzeit = GetDoubleOfElement(spear);
						weltenSettings.SpeertraegerBauzeit = (int) Math.Round(GetDoubleOfElement(spear, true));
						var sword = doc.GetElementsByTagName("sword");
						weltenSettings.SchwertkaempferLaufzeit = GetDoubleOfElement(sword);
						weltenSettings.SchwertkaempferBauzeit = (int) Math.Round(GetDoubleOfElement(sword, true));
						var axe = doc.GetElementsByTagName("axe");
						weltenSettings.AxtkaempferLaufzeit = GetDoubleOfElement(axe);
						weltenSettings.AxtkaempferBauzeit = (int) Math.Round(GetDoubleOfElement(axe, true));
						var archer = doc.GetElementsByTagName("archer");
						weltenSettings.BogenschuetzenLaufzeit = GetDoubleOfElement(archer);
						weltenSettings.BogenschuetzenBauzeit = (int) Math.Round(GetDoubleOfElement(archer, true));
						var spy = doc.GetElementsByTagName("spy");
						weltenSettings.SpaeherLaufzeit = GetDoubleOfElement(spy);
						weltenSettings.SpaeherBauzeit = (int) Math.Round(GetDoubleOfElement(spy, true));
						var light = doc.GetElementsByTagName("light");
						weltenSettings.LeichteKavallerieLaufzeit = GetDoubleOfElement(light);
						weltenSettings.LeichteKavallerieBauzeit = (int) Math.Round(GetDoubleOfElement(light, true));
						var marcher = doc.GetElementsByTagName("marcher");
						weltenSettings.BeritteneBogenschuetzenLaufzeit = GetDoubleOfElement(marcher);
						weltenSettings.BeritteneBogenschuetzenBauzeit =
							(int) Math.Round(GetDoubleOfElement(marcher, true));
						var heavy = doc.GetElementsByTagName("heavy");
						weltenSettings.SchwereKavallerieLaufzeit = GetDoubleOfElement(heavy);
						weltenSettings.SchwereKavallerieBauzeit = (int) Math.Round(GetDoubleOfElement(heavy, true));
						var ram = doc.GetElementsByTagName("ram");
						weltenSettings.RammboeckeLaufzeit = GetDoubleOfElement(ram);
						weltenSettings.RammboeckeBauzeit = (int) Math.Round(GetDoubleOfElement(ram, true));
						var cat = doc.GetElementsByTagName("catapult");
						weltenSettings.KatapulteLaufzeit = GetDoubleOfElement(cat);
						weltenSettings.KatapulteBauzeit = (int) Math.Round(GetDoubleOfElement(cat, true));
						var knight = doc.GetElementsByTagName("knight");
						weltenSettings.PaladinLaufzeit = GetDoubleOfElement(knight);
						weltenSettings.PaladinBauzeit = (int) Math.Round(GetDoubleOfElement(knight, true));
						var snob = doc.GetElementsByTagName("snob");
						weltenSettings.AdelsgeschlechtLaufzeit = GetDoubleOfElement(snob);
						weltenSettings.AdelsgeschlechtBauzeit = (int) Math.Round(GetDoubleOfElement(snob, true));
					} catch (Exception e) {
						Ping sender = new Ping();
						PingReply result = sender.Send("www.google.de");
						while (result != null && result.Status != IPStatus.Success) {
							result = sender.Send("www.google.de");
							App.LogString("Network Timeout");
							await Task.Delay(2000);
						}

						fehler = true;
					}
				} while (fehler);
			}
		}

		private static bool GetBoolOfElement(XmlDocument doc, string elementtagname) {
			var element = doc.GetElementsByTagName(elementtagname);
			var text = element[0]?.InnerText;
			if (!string.IsNullOrEmpty(text)) {
				return text == "1";
			}

			return false;
		}

		private static int GetIntOfElement(XmlDocument doc, string elementtagname) {
			var element = doc.GetElementsByTagName(elementtagname);
			var text = element[0]?.InnerText;
			if (!string.IsNullOrEmpty(text)) {
				int.TryParse(text, out int temp);
				return temp;
			}

			return 0;
		}

		private static double GetDoubleOfElement(XmlNodeList doc, bool bauzeit = false) {
			foreach (XmlElement element in doc) {
				var xx = element.GetElementsByTagName(!bauzeit ? "speed" : "build_time");
				var text = ((XmlElement) xx[0])?.InnerText;
				if (!string.IsNullOrEmpty(text)) {
					if (text.Contains(".")) text = text.Replace(".", ",");
					double.TryParse(text, out double temp);
					return temp;
				}
			}

			return 0;
		}
	}
}