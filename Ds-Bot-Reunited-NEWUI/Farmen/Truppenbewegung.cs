﻿using System;
using Ds_Bot_Reunited_NEWUI.Spieldaten;

namespace Ds_Bot_Reunited_NEWUI.Farmen {
    [Serializable]
    public class Truppenbewegung {
        public int SenderVillageId { get; set; }
        public string BewegungsArt { get; set; }
        public Dorf SenderDorf { get; set; }
        public int EmpfaengerVillageId { get; set; }
        public Dorf EmpfaengerDorf { get; set; }
        public DateTime Absendezeitpunkt { get; set; }
        public DateTime Ankunftszeitpunkt { get; set; }
        public DateTime Rueckkehrzeitpunkt { get; set; }
        public TimeSpan Laufzeit { get; set; }
        public TruppenTemplate TruppenSent { get; set; }
        public TruppenTemplate TruppenUeberlebt { get; set; }
        public Beute Erwartet { get; set; }
        public Beute Erbeutet { get; set; }
        public bool IsSelected { get; set; }

        //Wenn Bogen auf Welt deaktiviert dann wird die Column unterdrückt!

        public Truppenbewegung() {
            Erbeutet = new Beute();
            TruppenUeberlebt = new TruppenTemplate();
        }

        public Truppenbewegung Clone() {
            Truppenbewegung newTruppenbewegung = new Truppenbewegung {
                Absendezeitpunkt = Absendezeitpunkt,
                Ankunftszeitpunkt = Ankunftszeitpunkt,
                BewegungsArt = BewegungsArt,
                IsSelected = IsSelected,
                SenderVillageId = SenderVillageId,
                EmpfaengerDorf = EmpfaengerDorf.Clone(),
                EmpfaengerVillageId = EmpfaengerVillageId,
                Erbeutet = Erbeutet.Clone(),
                Erwartet = Erwartet.Clone(),
                Laufzeit = Laufzeit,
                Rueckkehrzeitpunkt = Rueckkehrzeitpunkt,
                SenderDorf = SenderDorf.Clone(),
                TruppenSent = TruppenSent.Clone(),
                TruppenUeberlebt = TruppenUeberlebt.Clone()
            };
            return newTruppenbewegung;
        }
    }
}