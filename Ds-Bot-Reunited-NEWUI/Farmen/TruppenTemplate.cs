﻿using System;

namespace Ds_Bot_Reunited_NEWUI.Farmen {
    [Serializable]
    public class TruppenTemplate {
        public string TemplateName { get; set; }
        public int SpeertraegerAnzahl { get; set; }
        public int SchwertkaempferAnzahl { get; set; }
        public int AxtkaempferAnzahl { get; set; }
        public int BogenschuetzenAnzahl { get; set; }
        public int SpaeherAnzahl { get; set; }
        public int LeichteKavallerieAnzahl { get; set; }
        public int BeritteneBogenschuetzenAnzahl { get; set; }
        public int SchwereKavallerieAnzahl { get; set; }
        public int RammboeckeAnzahl { get; set; }
        public int KatapulteAnzahl { get; set; }
        public int PaladinAnzahl { get; set; }
        public int AdelsgeschlechterAnzahl { get; set; }

        // ReSharper disable once EmptyConstructor         
        public TruppenTemplate() { }

        public TruppenTemplate Clone() {
            TruppenTemplate newTruppenTemplate = new TruppenTemplate {
                AdelsgeschlechterAnzahl = AdelsgeschlechterAnzahl,
                AxtkaempferAnzahl = AxtkaempferAnzahl,
                BeritteneBogenschuetzenAnzahl = BeritteneBogenschuetzenAnzahl,
                BogenschuetzenAnzahl = BogenschuetzenAnzahl,
                KatapulteAnzahl = KatapulteAnzahl,
                LeichteKavallerieAnzahl = LeichteKavallerieAnzahl,
                PaladinAnzahl = PaladinAnzahl,
                RammboeckeAnzahl = RammboeckeAnzahl,
                SchwereKavallerieAnzahl = SchwereKavallerieAnzahl,
                SchwertkaempferAnzahl = SchwertkaempferAnzahl,
                SpaeherAnzahl = SpaeherAnzahl,
                SpeertraegerAnzahl = SpeertraegerAnzahl,
                TemplateName = TemplateName
            };
            return newTruppenTemplate;
        }
    }
}