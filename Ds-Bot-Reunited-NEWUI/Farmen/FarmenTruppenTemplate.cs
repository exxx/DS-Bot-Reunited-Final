﻿using System;
using System.Windows;

namespace Ds_Bot_Reunited_NEWUI.Farmen {
    [Serializable]
    public class FarmenTruppenTemplate {
        public string TemplateName { get; set; }
        
        public Visibility BogenschuetzenActiv => App.Settings.Weltensettings != null ? (App.Settings.Weltensettings.BogenschuetzenActive ? Visibility.Visible : Visibility.Collapsed) : Visibility.Visible;

        public Visibility PaladinActiv => App.Settings.Weltensettings != null ? (App.Settings.Weltensettings.PaladinActive ? Visibility.Visible : Visibility.Collapsed) : Visibility.Visible;


        public bool SpeertraegerAgainstBarbsActive { get; set; } = true;
        public bool SpeertraegerAgainstPlayerActive { get; set; }
        public bool SchwertkaempferAgainstBarbsActive { get; set; } = true;
        public bool SchwertkaempferAgainstPlayerActive { get; set; }
        public bool AxtkaempferAgainstBarbsActive { get; set; } = true;
        public bool AxtkaempferAgainstPlayerActive { get; set; } = true;
        public bool BogenschuetzenAgainstBarbsActive { get; set; } = true;
        public bool BogenschuetzenAgainstPlayerActive { get; set; }
        public bool SpaeherAgainstBarbsActive { get; set; } = true;
        public bool SpaeherAgainstPlayerActive { get; set; } = true;
        public bool LeichteKavallerieAgainstBarbsActive { get; set; } = true;
        public bool LeichteKavallerieAgainstPlayerActive { get; set; } = true;
        public bool BeritteneBogenschuetzenAgainstBarbsActive { get; set; } = true;
        public bool BeritteneBogenschuetzenAgainstPlayerActive { get; set; } = true;
        public bool SchwereKavallerieAgainstBarbsActive { get; set; } = true;
        public bool SchwereKavallerieAgainstPlayerActive { get; set; } = true;
        public bool RammboeckeAgainstBarbsActive { get; set; } = true;
        public bool RammboeckeAgainstPlayerActive { get; set; } = true;
        public bool KatapulteAgainstBarbsActive { get; set; } = true;
        public bool KatapulteAgainstPlayerActive { get; set; } = true;
        public bool PaladinAgainstBarbsActive { get; set; } = true;
        public bool PaladinAgainstPlayerActive { get; set; } = true;

        public bool SpeertraegerWithRamActive { get; set; }
        public bool SpeertraegerWithCatActive { get; set; }
        public bool SchwertkaempferWithRamActive { get; set; }
        public bool SchwertkaempferWithCatActive { get; set; }
        public bool AxtkaempferWithRamActive { get; set; } = true;
        public bool AxtkaempferWithCatActive { get; set; } = true;
        public bool BogenschuetzenWithRamActive { get; set; }
        public bool BogenschuetzenWithCatActive { get; set; }
        public bool LeichteKavallerieWithRamActive { get; set; } = true;
        public bool LeichteKavallerieWithCatActive { get; set; } = true;
        public bool BeritteneBogenschuetzenWithRamActive { get; set; } = true;
        public bool BeritteneBogenschuetzenWithCatActive { get; set; } = true;
        public bool SchwereKavallerieWithRamActive { get; set; } = true;
        public bool SchwereKavallerieWithCatActive { get; set; } = true;


        public bool SpeertraegerActive { get; set; } = true;
        public bool SpeertraegerAgainstWallActive { get; set; }
        public int SpeertraegerAnzahl { get; set; }
        public int SpeertraegerAnzahlMitSchwertkaempfer { get; set; } = 2;
        public int SpeertraegerAnzahlMitAxtkaempfer { get; set; } = 1;
        public int SpeertraegerAnzahlMitBogenschuetzen { get; set; } = 5;
        public int SpeertraegerAnzahlMitSpaeher { get; set; }
        public int SpeertraegerAnzahlMitLeichteKavallerie { get; set; } = 1;
        public int SpeertraegerAnzahlMitBeritteneBogenschuetzen { get; set; } = 1;
        public int SpeertraegerAnzahlMitSchwereKavallerie { get; set; } = 1;
        public int SpeertraegerAnzahlMitRammboecke { get; set; }
        public int SpeertraegerAnzahlMitKatapulte { get; set; }
        public int SpeertraegerAnzahlMitPaladin { get; set; } = 10;
        public int SpeertraegerMaxLaufzeit { get; set; } = 120;

        public bool SchwertkaempferActive { get; set; } = true;
        public bool SchwertkaempferAgainstWallActive { get; set; }
        public int SchwertkaempferAnzahl { get; set; }
        public int SchwertkaempferAnzahlMitSpeertraeger { get; set; } = 2;
        public int SchwertkaempferAnzahlMitAxtkaempfer { get; set; } = 1;
        public int SchwertkaempferAnzahlMitBogenschuetzen { get; set; } = 2;
        public int SchwertkaempferAnzahlMitSpaeher { get; set; }
        public int SchwertkaempferAnzahlMitLeichteKavallerie { get; set; }
        public int SchwertkaempferAnzahlMitBeritteneBogenschuetzen { get; set; } = 1;
        public int SchwertkaempferAnzahlMitSchwereKavallerie { get; set; } = 1;
        public int SchwertkaempferAnzahlMitRammboecke { get; set; }
        public int SchwertkaempferAnzahlMitKatapulte { get; set; }
        public int SchwertkaempferAnzahlMitPaladin { get; set; } = 10;
        public int SchwertkaempferMaxLaufzeit { get; set; } = 120;

        public bool AxtkaempferActive { get; set; } = true;
        public bool AxtkaempferAgainstWallActive { get; set; }
        public int AxtkaempferAnzahl { get; set; } = 20;
        public int AxtkaempferAnzahlMitSpeertraeger { get; set; } = 1;
        public int AxtkaempferAnzahlMitSchwertkaempfer { get; set; } = 1;
        public int AxtkaempferAnzahlMitBogenschuetzen { get; set; } = 1;
        public int AxtkaempferAnzahlMitSpaeher { get; set; }
        public int AxtkaempferAnzahlMitLeichteKavallerie { get; set; } = 1;
        public int AxtkaempferAnzahlMitBeritteneBogenschuetzen { get; set; } = 1;
        public int AxtkaempferAnzahlMitSchwereKavallerie { get; set; } = 1;
        public int AxtkaempferAnzahlMitRammboecke { get; set; } = 10;
        public int AxtkaempferAnzahlMitKatapulte { get; set; } = 10;
        public int AxtkaempferAnzahlMitPaladin { get; set; } = 10;
        public int AxtkaempferMaxLaufzeit { get; set; } = 120;

        public bool BogenschuetzenActive { get; set; } = true;
        public bool BogenschuetzenAgainstWallActive { get; set; }
        public int BogenschuetzenAnzahl { get; set; }
        public int BogenschuetzenAnzahlMitAxtkaempfer { get; set; } = 1;
        public int BogenschuetzenAnzahlMitSchwertkaempfer { get; set; } = 1;
        public int BogenschuetzenAnzahlMitSpeertraeger { get; set; } = 5;
        public int BogenschuetzenAnzahlMitSpaeher { get; set; }
        public int BogenschuetzenAnzahlMitLeichteKavallerie { get; set; } = 1;
        public int BogenschuetzenAnzahlMitBeritteneBogenschuetzen { get; set; } = 1;
        public int BogenschuetzenAnzahlMitSchwereKavallerie { get; set; } = 1;
        public int BogenschuetzenAnzahlMitRammboecke { get; set; }
        public int BogenschuetzenAnzahlMitKatapulte { get; set; }
        public int BogenschuetzenAnzahlMitPaladin { get; set; } = 5;
        public int BogenschuetzenMaxLaufzeit { get; set; } = 120;

        public bool SpaeherActive { get; set; }
        public bool SpaeherAgainstWallActive { get; set; } = true;
        public int SpaeherAnzahl { get; set; } = 1;
        public int SpaeherAnzahlMitSpeertraeger { get; set; } = 1;
        public int SpaeherAnzahlMitSchwertkaempfer { get; set; } = 1;
        public int SpaeherAnzahlMitAxtkaempfer { get; set; } = 1;
        public int SpaeherAnzahlMitBogenschuetzen { get; set; } = 1;
        public int SpaeherAnzahlMitLeichteKavallerie { get; set; } = 1;
        public int SpaeherAnzahlMitBeritteneBogenschuetzen { get; set; } = 1;
        public int SpaeherAnzahlMitSchwereKavallerie { get; set; } = 1;
        public int SpaeherAnzahlMitRammboecke { get; set; } = 1;
        public int SpaeherAnzahlMitKatapulte { get; set; } = 1;
        public int SpaeherAnzahlMitPaladin { get; set; } = 1;
        public int SpaeherMaxLaufzeit { get; set; } = 120;

        public bool LeichteKavallerieActive { get; set; } = true;
        public bool LeichteKavallerieAgainstWallActive { get; set; }
        public int LeichteKavallerieAnzahl { get; set; } = 1;
        public int LeichteKavallerieAnzahlMitSpeertraeger { get; set; } = 1;
        public int LeichteKavallerieAnzahlMitSchwertkaempfer { get; set; } = 1;
        public int LeichteKavallerieAnzahlMitAxtkaempfer { get; set; } = 1;
        public int LeichteKavallerieAnzahlMitBogenschuetzen { get; set; } = 1;
        public int LeichteKavallerieAnzahlMitSpaeher { get; set; }
        public int LeichteKavallerieAnzahlMitBeritteneBogenschuetzen { get; set; } = 1;
        public int LeichteKavallerieAnzahlMitSchwereKavallerie { get; set; } = 1;
        public int LeichteKavallerieAnzahlMitRammboecke { get; set; } = 1;
        public int LeichteKavallerieAnzahlMitKatapulte { get; set; } = 1;
        public int LeichteKavallerieAnzahlMitPaladin { get; set; } = 1;
        public int LeichteKavallerieMaxLaufzeit { get; set; } = 120;

        public bool BeritteneBogenschuetzenActive { get; set; } = true;
        public bool BeritteneBogenschuetzenAgainstWallActive { get; set; }
        public int BeritteneBogenschuetzenAnzahl { get; set; } = 1;
        public int BeritteneBogenschuetzenAnzahlMitSpeertraeger { get; set; } = 1;
        public int BeritteneBogenschuetzenAnzahlMitSchwertkaempfer { get; set; } = 1;
        public int BeritteneBogenschuetzenAnzahlMitAxtkaempfer { get; set; } = 1;
        public int BeritteneBogenschuetzenAnzahlMitBogenschuetzen { get; set; } = 1;
        public int BeritteneBogenschuetzenAnzahlMitSpaeher { get; set; }
        public int BeritteneBogenschuetzenAnzahlMitLeichteKavallerie { get; set; } = 1;
        public int BeritteneBogenschuetzenAnzahlMitSchwereKavallerie { get; set; } = 1;
        public int BeritteneBogenschuetzenAnzahlMitRammboecke { get; set; } = 1;
        public int BeritteneBogenschuetzenAnzahlMitKatapulte { get; set; } = 1;
        public int BeritteneBogenschuetzenAnzahlMitPaladin { get; set; } = 1;
        public int BeritteneBogenschuetzenMaxLaufzeit { get; set; } = 120;

        public bool SchwereKavallerieActive { get; set; } = true;
        public bool SchwereKavallerieAgainstWallActive { get; set; }
        public int SchwereKavallerieAnzahl { get; set; } = 1;
        public int SchwereKavallerieAnzahlMitSpeertraeger { get; set; } = 1;
        public int SchwereKavallerieAnzahlMitSchwertkaempfer { get; set; } = 1;
        public int SchwereKavallerieAnzahlMitAxtkaempfer { get; set; } = 1;
        public int SchwereKavallerieAnzahlMitBogenschuetzen { get; set; } = 1;
        public int SchwereKavallerieAnzahlMitSpaeher { get; set; }
        public int SchwereKavallerieAnzahlMitLeichteKavallerie { get; set; } = 1;
        public int SchwereKavallerieAnzahlMitBeritteneBogenschuetzen { get; set; } = 1;
        public int SchwereKavallerieAnzahlMitRammboecke { get; set; } = 1;
        public int SchwereKavallerieAnzahlMitKatapulte { get; set; } = 1;
        public int SchwereKavallerieAnzahlMitPaladin { get; set; } = 1;
        public int SchwereKavallerieMaxLaufzeit { get; set; } = 120;

        public bool RammboeckeActive { get; set; }
        public bool RammboeckeAgainstWallActive { get; set; } = true;
        public int RammboeckeAnzahl { get; set; }
        public int RammboeckeAnzahlMitSpeertraeger { get; set; }
        public int RammboeckeAnzahlMitSchwertkaempfer { get; set; }
        public int RammboeckeAnzahlMitAxtkaempfer { get; set; }
        public int RammboeckeAnzahlMitBogenschuetzen { get; set; }
        public int RammboeckeAnzahlMitSpaeher { get; set; }
        public int RammboeckeAnzahlMitLeichteKavallerie { get; set; }
        public int RammboeckeAnzahlMitSchwereKavallerie { get; set; }
        public int RammboeckeAnzahlMitBeritteneBogenschuetzen { get; set; }
        public int RammboeckeAnzahlMitKatapulte { get; set; }
        public int RammboeckeAnzahlMitPaladin { get; set; }
        public int RammboeckeMaxLaufzeit { get; set; }

        public bool KatapulteActive { get; set; }
        public bool KatapulteAgainstWallActive { get; set; } = true;
        public int KatapulteAnzahl { get; set; }
        public int KatapulteAnzahlMitSpeertraeger { get; set; }
        public int KatapulteAnzahlMitSchwertkaempfer { get; set; }
        public int KatapulteAnzahlMitAxtkaempfer { get; set; }
        public int KatapulteAnzahlMitBogenschuetzen { get; set; }
        public int KatapulteAnzahlMitSpaeher { get; set; }
        public int KatapulteAnzahlMitLeichteKavallerie { get; set; }
        public int KatapulteAnzahlMitSchwereKavallerie { get; set; }
        public int KatapulteAnzahlMitRammboecke { get; set; }
        public int KatapulteAnzahlMitBeritteneBogenschuetzen { get; set; }
        public int KatapulteAnzahlMitPaladin { get; set; }
        public int KatapulteMaxLaufzeit { get; set; }

        public bool PaladinActive { get; set; } = true;
        public bool PaladinAgainstWallActive { get; set; } = false;
        public int PaladinAnzahl { get; set; } = 1;
        public int PaladinAnzahlMitSpeertraeger { get; set; } = 1;
        public int PaladinAnzahlMitSchwertkaempfer { get; set; } = 1;
        public int PaladinAnzahlMitAxtkaempfer { get; set; } = 1;
        public int PaladinAnzahlMitBogenschuetzen { get; set; } = 1;
        public int PaladinAnzahlMitSpaeher { get; set; }
        public int PaladinAnzahlMitLeichteKavallerie { get; set; } = 1;
        public int PaladinAnzahlMitSchwereKavallerie { get; set; } = 1;
        public int PaladinAnzahlMitRammboecke { get; set; } = 1;
        public int PaladinAnzahlMitKatapulte { get; set; } = 1;
        public int PaladinAnzahlMitBeritteneBogenschuetzen { get; set; } = 1;
        public int PaladinMaxLaufzeit { get; set; } = 120;

        // ReSharper disable once EmptyConstructor         
        public FarmenTruppenTemplate() { }

        public FarmenTruppenTemplate Clone() {
            FarmenTruppenTemplate newFarmenTruppenTemplate = new FarmenTruppenTemplate {
                AxtkaempferAnzahl = AxtkaempferAnzahl,
                PaladinAnzahl = PaladinAnzahl,
                BeritteneBogenschuetzenAnzahl = BeritteneBogenschuetzenAnzahl,
                LeichteKavallerieAnzahl = LeichteKavallerieAnzahl,
                RammboeckeAnzahl = RammboeckeAnzahl,
                SpeertraegerAnzahl = SpeertraegerAnzahl,
                SpaeherAnzahl = SpaeherAnzahl,
                BogenschuetzenAnzahl = BogenschuetzenAnzahl,
                KatapulteAnzahl = KatapulteAnzahl,
                SchwereKavallerieAnzahl = SchwereKavallerieAnzahl,
                SchwertkaempferAnzahl = SchwertkaempferAnzahl,
                TemplateName = TemplateName,
                AxtkaempferAnzahlMitBeritteneBogenschuetzen = AxtkaempferAnzahlMitBeritteneBogenschuetzen,
                AxtkaempferAnzahlMitBogenschuetzen = AxtkaempferAnzahlMitBogenschuetzen,
                AxtkaempferAnzahlMitKatapulte = AxtkaempferAnzahlMitKatapulte,
                AxtkaempferAnzahlMitLeichteKavallerie = AxtkaempferAnzahlMitLeichteKavallerie,
                AxtkaempferAnzahlMitPaladin = AxtkaempferAnzahlMitPaladin,
                AxtkaempferAnzahlMitRammboecke = AxtkaempferAnzahlMitRammboecke,
                AxtkaempferAnzahlMitSchwereKavallerie = AxtkaempferAnzahlMitSchwereKavallerie,
                AxtkaempferAnzahlMitSchwertkaempfer = AxtkaempferAnzahlMitSchwertkaempfer,
                AxtkaempferAnzahlMitSpaeher = AxtkaempferAnzahlMitSpaeher,
                AxtkaempferAnzahlMitSpeertraeger = AxtkaempferAnzahlMitSpeertraeger,
                AxtkaempferMaxLaufzeit = AxtkaempferMaxLaufzeit,
                BeritteneBogenschuetzenAnzahlMitAxtkaempfer = BeritteneBogenschuetzenAnzahlMitAxtkaempfer,
                BeritteneBogenschuetzenAnzahlMitBogenschuetzen = BeritteneBogenschuetzenAnzahlMitBogenschuetzen,
                BeritteneBogenschuetzenAnzahlMitKatapulte = BeritteneBogenschuetzenAnzahlMitKatapulte,
                BeritteneBogenschuetzenAnzahlMitLeichteKavallerie = BeritteneBogenschuetzenAnzahlMitLeichteKavallerie,
                BeritteneBogenschuetzenAnzahlMitPaladin = BeritteneBogenschuetzenAnzahlMitPaladin,
                BeritteneBogenschuetzenAnzahlMitRammboecke = BeritteneBogenschuetzenAnzahlMitRammboecke,
                BeritteneBogenschuetzenAnzahlMitSchwereKavallerie = BeritteneBogenschuetzenAnzahlMitSchwereKavallerie,
                BeritteneBogenschuetzenAnzahlMitSchwertkaempfer = BeritteneBogenschuetzenAnzahlMitSchwertkaempfer,
                BeritteneBogenschuetzenAnzahlMitSpaeher = BeritteneBogenschuetzenAnzahlMitSpaeher,
                BeritteneBogenschuetzenAnzahlMitSpeertraeger = BeritteneBogenschuetzenAnzahlMitSpeertraeger,
                BeritteneBogenschuetzenMaxLaufzeit = BeritteneBogenschuetzenMaxLaufzeit,
                BogenschuetzenAnzahlMitAxtkaempfer = BogenschuetzenAnzahlMitAxtkaempfer,
                BogenschuetzenAnzahlMitBeritteneBogenschuetzen = BogenschuetzenAnzahlMitBeritteneBogenschuetzen,
                BogenschuetzenAnzahlMitKatapulte = BogenschuetzenAnzahlMitKatapulte,
                BogenschuetzenAnzahlMitLeichteKavallerie = BogenschuetzenAnzahlMitLeichteKavallerie,
                BogenschuetzenAnzahlMitPaladin = BogenschuetzenAnzahlMitPaladin,
                BogenschuetzenAnzahlMitRammboecke = BogenschuetzenAnzahlMitRammboecke,
                BogenschuetzenAnzahlMitSchwereKavallerie = BogenschuetzenAnzahlMitSchwereKavallerie,
                BogenschuetzenAnzahlMitSchwertkaempfer = BogenschuetzenAnzahlMitSchwertkaempfer,
                BogenschuetzenAnzahlMitSpaeher = BogenschuetzenAnzahlMitSpaeher,
                BogenschuetzenAnzahlMitSpeertraeger = BogenschuetzenAnzahlMitSpeertraeger,
                BogenschuetzenMaxLaufzeit = BogenschuetzenMaxLaufzeit,
                KatapulteAnzahlMitAxtkaempfer = KatapulteAnzahlMitAxtkaempfer,
                KatapulteAnzahlMitBeritteneBogenschuetzen = KatapulteAnzahlMitBeritteneBogenschuetzen,
                KatapulteAnzahlMitBogenschuetzen = KatapulteAnzahlMitBogenschuetzen,
                KatapulteAnzahlMitLeichteKavallerie = KatapulteAnzahlMitLeichteKavallerie,
                KatapulteAnzahlMitPaladin = KatapulteAnzahlMitPaladin,
                KatapulteAnzahlMitRammboecke = KatapulteAnzahlMitRammboecke,
                KatapulteAnzahlMitSchwereKavallerie = KatapulteAnzahlMitSchwereKavallerie,
                KatapulteAnzahlMitSchwertkaempfer = KatapulteAnzahlMitSchwertkaempfer,
                KatapulteAnzahlMitSpaeher = KatapulteAnzahlMitSpaeher,
                KatapulteAnzahlMitSpeertraeger = KatapulteAnzahlMitSpeertraeger,
                KatapulteMaxLaufzeit = KatapulteMaxLaufzeit,
                LeichteKavallerieAnzahlMitAxtkaempfer = LeichteKavallerieAnzahlMitAxtkaempfer,
                LeichteKavallerieAnzahlMitBeritteneBogenschuetzen = LeichteKavallerieAnzahlMitBeritteneBogenschuetzen,
                LeichteKavallerieAnzahlMitBogenschuetzen = LeichteKavallerieAnzahlMitBogenschuetzen,
                LeichteKavallerieAnzahlMitKatapulte = LeichteKavallerieAnzahlMitKatapulte,
                LeichteKavallerieAnzahlMitPaladin = LeichteKavallerieAnzahlMitPaladin,
                LeichteKavallerieAnzahlMitRammboecke = LeichteKavallerieAnzahlMitRammboecke,
                LeichteKavallerieAnzahlMitSchwereKavallerie = LeichteKavallerieAnzahlMitSchwereKavallerie,
                LeichteKavallerieAnzahlMitSchwertkaempfer = LeichteKavallerieAnzahlMitSchwertkaempfer,
                LeichteKavallerieAnzahlMitSpaeher = LeichteKavallerieAnzahlMitSpaeher,
                LeichteKavallerieAnzahlMitSpeertraeger = LeichteKavallerieAnzahlMitSpeertraeger,
                LeichteKavallerieMaxLaufzeit = LeichteKavallerieMaxLaufzeit,
                PaladinAnzahlMitAxtkaempfer = PaladinAnzahlMitAxtkaempfer,
                PaladinAnzahlMitBeritteneBogenschuetzen = PaladinAnzahlMitBeritteneBogenschuetzen,
                PaladinAnzahlMitBogenschuetzen = PaladinAnzahlMitBogenschuetzen,
                PaladinAnzahlMitKatapulte = PaladinAnzahlMitKatapulte,
                PaladinAnzahlMitLeichteKavallerie = PaladinAnzahlMitLeichteKavallerie,
                PaladinAnzahlMitRammboecke = PaladinAnzahlMitRammboecke,
                PaladinAnzahlMitSchwereKavallerie = PaladinAnzahlMitSchwereKavallerie,
                PaladinAnzahlMitSchwertkaempfer = PaladinAnzahlMitSchwertkaempfer,
                PaladinAnzahlMitSpaeher = PaladinAnzahlMitSpaeher,
                PaladinAnzahlMitSpeertraeger = PaladinAnzahlMitSpeertraeger,
                PaladinMaxLaufzeit = PaladinMaxLaufzeit,
                RammboeckeAnzahlMitPaladin = RammboeckeAnzahlMitPaladin,
                RammboeckeAnzahlMitAxtkaempfer = RammboeckeAnzahlMitAxtkaempfer,
                RammboeckeAnzahlMitBeritteneBogenschuetzen = RammboeckeAnzahlMitBeritteneBogenschuetzen,
                RammboeckeAnzahlMitBogenschuetzen = RammboeckeAnzahlMitBogenschuetzen,
                RammboeckeAnzahlMitKatapulte = RammboeckeAnzahlMitKatapulte,
                RammboeckeAnzahlMitLeichteKavallerie = RammboeckeAnzahlMitLeichteKavallerie,
                RammboeckeAnzahlMitSchwereKavallerie = RammboeckeAnzahlMitSchwereKavallerie,
                RammboeckeAnzahlMitSchwertkaempfer = RammboeckeAnzahlMitSchwertkaempfer,
                RammboeckeAnzahlMitSpaeher = RammboeckeAnzahlMitSpaeher,
                RammboeckeAnzahlMitSpeertraeger = RammboeckeAnzahlMitSpeertraeger,
                RammboeckeMaxLaufzeit = RammboeckeMaxLaufzeit,
                SchwereKavallerieAnzahlMitAxtkaempfer = SchwereKavallerieAnzahlMitAxtkaempfer,
                SchwereKavallerieAnzahlMitBeritteneBogenschuetzen = SchwereKavallerieAnzahlMitBeritteneBogenschuetzen,
                SchwereKavallerieAnzahlMitBogenschuetzen = SchwereKavallerieAnzahlMitBogenschuetzen,
                SchwereKavallerieAnzahlMitKatapulte = SchwereKavallerieAnzahlMitKatapulte,
                SchwereKavallerieAnzahlMitLeichteKavallerie = SchwereKavallerieAnzahlMitLeichteKavallerie,
                SchwereKavallerieAnzahlMitPaladin = SchwereKavallerieAnzahlMitPaladin,
                SchwereKavallerieAnzahlMitRammboecke = SchwereKavallerieAnzahlMitRammboecke,
                SchwereKavallerieAnzahlMitSchwertkaempfer = SchwereKavallerieAnzahlMitSchwertkaempfer,
                SchwereKavallerieAnzahlMitSpaeher = SchwereKavallerieAnzahlMitSpaeher,
                SchwereKavallerieAnzahlMitSpeertraeger = SchwereKavallerieAnzahlMitSpeertraeger,
                SchwereKavallerieMaxLaufzeit = SchwereKavallerieMaxLaufzeit,
                SchwertkaempferAnzahlMitAxtkaempfer = SchwertkaempferAnzahlMitAxtkaempfer,
                SchwertkaempferAnzahlMitBeritteneBogenschuetzen = SchwertkaempferAnzahlMitBeritteneBogenschuetzen,
                SchwertkaempferAnzahlMitBogenschuetzen = SchwertkaempferAnzahlMitBogenschuetzen,
                SchwertkaempferAnzahlMitKatapulte = SchwertkaempferAnzahlMitKatapulte,
                SchwertkaempferAnzahlMitLeichteKavallerie = SchwertkaempferAnzahlMitLeichteKavallerie,
                SchwertkaempferAnzahlMitPaladin = SchwertkaempferAnzahlMitPaladin,
                SchwertkaempferAnzahlMitRammboecke = SchwertkaempferAnzahlMitRammboecke,
                SchwertkaempferAnzahlMitSchwereKavallerie = SchwertkaempferAnzahlMitSchwereKavallerie,
                SchwertkaempferAnzahlMitSpaeher = SchwertkaempferAnzahlMitSpaeher,
                SchwertkaempferAnzahlMitSpeertraeger = SchwertkaempferAnzahlMitSpeertraeger,
                SchwertkaempferMaxLaufzeit = SchwertkaempferMaxLaufzeit,
                SpaeherAnzahlMitSchwereKavallerie = SpaeherAnzahlMitSchwereKavallerie,
                SpaeherAnzahlMitAxtkaempfer = SpaeherAnzahlMitAxtkaempfer,
                SpaeherAnzahlMitBeritteneBogenschuetzen = SpaeherAnzahlMitBeritteneBogenschuetzen,
                SpaeherAnzahlMitBogenschuetzen = SpaeherAnzahlMitBogenschuetzen,
                SpaeherAnzahlMitKatapulte = SpaeherAnzahlMitKatapulte,
                SpaeherAnzahlMitLeichteKavallerie = SpaeherAnzahlMitLeichteKavallerie,
                SpaeherAnzahlMitPaladin = SpaeherAnzahlMitPaladin,
                SpaeherAnzahlMitRammboecke = SpaeherAnzahlMitRammboecke,
                SpaeherAnzahlMitSchwertkaempfer = SpaeherAnzahlMitSchwertkaempfer,
                SpaeherAnzahlMitSpeertraeger = SpaeherAnzahlMitSpeertraeger,
                SpaeherMaxLaufzeit = SpaeherMaxLaufzeit,
                SpeertraegerAnzahlMitAxtkaempfer = SpeertraegerAnzahlMitAxtkaempfer,
                SpeertraegerAnzahlMitBeritteneBogenschuetzen = SpeertraegerAnzahlMitBeritteneBogenschuetzen,
                SpeertraegerAnzahlMitBogenschuetzen = SpeertraegerAnzahlMitBogenschuetzen,
                SpeertraegerAnzahlMitKatapulte = SpeertraegerAnzahlMitKatapulte,
                SpeertraegerAnzahlMitLeichteKavallerie = SpeertraegerAnzahlMitLeichteKavallerie,
                SpeertraegerAnzahlMitPaladin = SpeertraegerAnzahlMitPaladin,
                SpeertraegerAnzahlMitRammboecke = SpeertraegerAnzahlMitRammboecke,
                SpeertraegerAnzahlMitSchwereKavallerie = SpeertraegerAnzahlMitSchwereKavallerie,
                SpeertraegerAnzahlMitSchwertkaempfer = SpeertraegerAnzahlMitSchwertkaempfer,
                SpeertraegerAnzahlMitSpaeher = SpeertraegerAnzahlMitSpaeher,
                SpeertraegerMaxLaufzeit = SpeertraegerMaxLaufzeit,
                BogenschuetzenActive = BogenschuetzenActive,
                PaladinActive = PaladinActive,
                AxtkaempferActive = AxtkaempferActive,
                BeritteneBogenschuetzenActive = BeritteneBogenschuetzenActive,
                KatapulteActive = KatapulteActive,
                LeichteKavallerieActive = LeichteKavallerieActive,
                RammboeckeActive = RammboeckeActive,
                SchwereKavallerieActive = SchwereKavallerieActive,
                SchwertkaempferActive = SchwertkaempferActive,
                SpaeherActive = SpaeherActive,
                SpeertraegerActive = SpeertraegerActive,
                AxtkaempferAgainstWallActive = AxtkaempferAgainstWallActive,
                BeritteneBogenschuetzenAgainstWallActive = BeritteneBogenschuetzenAgainstWallActive,
                BogenschuetzenAgainstWallActive = BogenschuetzenAgainstWallActive,
                KatapulteAgainstWallActive = KatapulteAgainstWallActive,
                LeichteKavallerieAgainstWallActive = LeichteKavallerieAgainstWallActive,
                PaladinAgainstWallActive = PaladinAgainstWallActive,
                RammboeckeAgainstWallActive = RammboeckeAgainstWallActive,
                SchwereKavallerieAgainstWallActive = SchwereKavallerieAgainstWallActive,
                SchwertkaempferAgainstWallActive = SchwertkaempferAgainstWallActive,
                SpaeherAgainstWallActive = SpaeherAgainstWallActive,
                SpeertraegerAgainstWallActive = SpeertraegerAgainstWallActive,
                AxtkaempferAgainstBarbsActive = AxtkaempferAgainstBarbsActive,
                AxtkaempferAgainstPlayerActive = AxtkaempferAgainstPlayerActive,
                BeritteneBogenschuetzenAgainstBarbsActive = BeritteneBogenschuetzenAgainstBarbsActive,
                BeritteneBogenschuetzenAgainstPlayerActive = BeritteneBogenschuetzenAgainstPlayerActive,
                BogenschuetzenAgainstBarbsActive = BogenschuetzenAgainstBarbsActive,
                BogenschuetzenAgainstPlayerActive = BogenschuetzenAgainstPlayerActive,
                KatapulteAgainstBarbsActive = KatapulteAgainstBarbsActive,
                KatapulteAgainstPlayerActive = KatapulteAgainstPlayerActive,
                LeichteKavallerieAgainstBarbsActive = LeichteKavallerieAgainstBarbsActive,
                LeichteKavallerieAgainstPlayerActive = LeichteKavallerieAgainstPlayerActive,
                PaladinAgainstBarbsActive = PaladinAgainstBarbsActive,
                PaladinAgainstPlayerActive = PaladinAgainstPlayerActive,
                RammboeckeAgainstBarbsActive = RammboeckeAgainstBarbsActive,
                RammboeckeAgainstPlayerActive = RammboeckeAgainstPlayerActive,
                SchwereKavallerieAgainstBarbsActive = SchwereKavallerieAgainstBarbsActive,
                SchwereKavallerieAgainstPlayerActive = SchwereKavallerieAgainstPlayerActive,
                SchwertkaempferAgainstBarbsActive = SchwertkaempferAgainstBarbsActive,
                SchwertkaempferAgainstPlayerActive = SchwertkaempferAgainstPlayerActive,
                SpaeherAgainstBarbsActive = SpaeherAgainstBarbsActive,
                SpaeherAgainstPlayerActive = SpaeherAgainstPlayerActive,
                SpeertraegerAgainstBarbsActive = SpeertraegerAgainstBarbsActive,
                SpeertraegerAgainstPlayerActive = SpeertraegerAgainstPlayerActive,
                AxtkaempferWithCatActive = AxtkaempferWithCatActive,
                AxtkaempferWithRamActive = AxtkaempferWithRamActive,
                BeritteneBogenschuetzenWithCatActive = BeritteneBogenschuetzenWithCatActive,
                BeritteneBogenschuetzenWithRamActive = BeritteneBogenschuetzenWithRamActive,
                BogenschuetzenWithCatActive = BogenschuetzenWithCatActive,
                BogenschuetzenWithRamActive = BogenschuetzenWithRamActive,
                LeichteKavallerieWithCatActive = LeichteKavallerieWithCatActive,
                LeichteKavallerieWithRamActive = LeichteKavallerieWithRamActive,
                SchwereKavallerieWithCatActive = SchwereKavallerieWithCatActive,
                SchwereKavallerieWithRamActive = SchwereKavallerieWithRamActive,
                SchwertkaempferWithCatActive = SchwertkaempferWithCatActive,
                SchwertkaempferWithRamActive = SchwertkaempferWithRamActive,
                SpeertraegerWithCatActive = SpeertraegerWithCatActive,
                SpeertraegerWithRamActive = SpeertraegerWithRamActive
            };
            return newFarmenTruppenTemplate;
        }
    }
}