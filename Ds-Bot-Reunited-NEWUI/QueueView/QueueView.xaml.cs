﻿using System.Windows;

namespace Ds_Bot_Reunited_NEWUI.QueueView
{
    /// <summary>
    /// Interaktionslogik für QueueView.xaml
    /// </summary>
    public partial class QueueView
    {
        public QueueView(Window owner)
        {
            Owner = owner;
            DataContext = new QueueViewModel();
            InitializeComponent();
        }
        private void Save(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
