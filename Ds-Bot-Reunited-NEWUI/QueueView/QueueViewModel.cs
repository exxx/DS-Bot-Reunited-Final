﻿using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Data;
using Ds_Bot_Reunited_NEWUI.ActionModul;
using Ds_Bot_Reunited_NEWUI.Annotations;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;

namespace Ds_Bot_Reunited_NEWUI.QueueView {
    public class QueueViewModel : INotifyPropertyChanged {
        private ICollectionView _queueView;
        private AsyncObservableCollection<ActionModulViewModel> _queue;

        private object QueueLock { get; set; } = new object();

        public AsyncObservableCollection<ActionModulViewModel> Queue {
            get => _queue;
            set {
                _queue = value;
                OnPropertyChanged();
            }
        }

        public ICollectionView QueueView {
            get => _queueView;
            set {
                _queueView = value;
                OnPropertyChanged();
            }
        }

        public QueueViewModel() {
            Queue = new AsyncObservableCollection<ActionModulViewModel>(App.PriorityQueue);
            QueueView = CollectionViewSource.GetDefaultView(Queue);
            BindingOperations.EnableCollectionSynchronization(Queue,
                QueueLock);
            Task task = new Task(async () => {
                while (true) {
                    Refresh();
                    await Task.Delay(2000);
                }
            }, TaskCreationOptions.LongRunning);
            task.Start();
        }

        public void Refresh() {
            _queue.Clear();
            foreach (var ff in App.PriorityQueue.ToList()) {
                _queue.Add(ff);
            }
            OnPropertyChanged("");
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}