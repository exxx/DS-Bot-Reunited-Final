﻿using System.Linq;
using System.Threading.Tasks;
using Ds_Bot_Reunited_NEWUI.Accountsettings.Ds_Bot_Reunited.Accountsettings;
using Ds_Bot_Reunited_NEWUI.Botcaptcha;
using Ds_Bot_Reunited_NEWUI.BrowserManager;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.Properties;
using Ds_Bot_Reunited_NEWUI.Settings;
using OpenQA.Selenium;

namespace Ds_Bot_Reunited_NEWUI.FlaggenModul {
    public static class FlaggenModul {
        public static async Task SetRohstoffFlagge(Browser browser, Accountsetting accountsettings, AccountDorfSettings dorf, bool alternative = false) {
            await UpgradeRohstoffFlaggen(browser, accountsettings, dorf);
            if (!accountsettings.MultiaccountingDaten.FlageSet) {
                string linkFlaggen = App.Settings.OperateLink + (dorf != null
                                         ? "village=" +
                                           dorf.Dorf.VillageId
                                         : "") + "" +
                                     accountsettings.UvZusatzFuerLink + "&screen=flags";
                browser.WebDriver.Navigate().GoToUrl(linkFlaggen);
                browser.WebDriver.WaitForPageload();
                await Botschutzerkennung.CheckIfBotschutz(browser, accountsettings);
                await Rndm.Sleep(500, 1000);
                if (
                    browser.WebDriver.FindElements(By.XPath("//div[@class='flag_box' and @data-name='"+Resources.Rohstoffproduktion+"']"))
                        .Any()) {
                    browser.WebDriver.FindElements(By.XPath("//div[@class='flag_box' and @data-name='" + Resources.Rohstoffproduktion + "']"))
                        .Last()
                        .Click();
                    browser.WebDriver.WaitForPageload();
                    browser.WebDriver.FindElement(By.XPath("//button[@class='btn evt-confirm-btn btn-confirm-yes']")).Click();
                    browser.WebDriver.WaitForPageload();
                }
                accountsettings.MultiaccountingDaten.FlageSet = true;
            }
        }

        public static async Task UpgradeRohstoffFlaggen(Browser browser, Accountsetting accountsettings, AccountDorfSettings dorf) {
            if (!accountsettings.MultiaccountingDaten.FlageSet) {
                string linkFlaggen = App.Settings.OperateLink + (dorf != null ? "village=" + dorf.Dorf.VillageId : "") + "" + accountsettings.UvZusatzFuerLink + "&screen=flags";
                browser.WebDriver.Navigate().GoToUrl(linkFlaggen);
                browser.WebDriver.WaitForPageload();
                if (
                    browser.WebDriver.FindElements(By.XPath("//div[@class='flag_box' and @data-name='" + Resources.Rohstoffproduktion + "']"))
                        .Any()) {
                    var element = browser.WebDriver.FindElements(By.XPath("//div[@class='flag_box' and @data-name='" + Resources.Rohstoffproduktion + "']")).Last();
                    await Rndm.Sleep(300, 500);
                    if (element.FindElements(By.XPath("//div[@class='flag_upgrade']")).Any()) {
                        browser.WebDriver.FindElement(By.XPath("//div[@class='flag_upgrade']")).Click();
                    }
                    await Rndm.Sleep(300, 500);
                }
            }
        }
    }
}