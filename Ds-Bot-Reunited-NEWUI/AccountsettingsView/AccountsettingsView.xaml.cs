﻿using System.Windows;
using System.Windows.Controls;
using Ds_Bot_Reunited_NEWUI.Accountsettings.Ds_Bot_Reunited.Accountsettings;

namespace Ds_Bot_Reunited_NEWUI.AccountsettingsView {
    public partial class AccountsettingsView {
        public AccountsettingsView(Window owner, AccountsettingsViewModel datacontext) {
            Owner = owner;
            DataContext = datacontext;
            InitializeComponent();
        }

        private void Save(object sender, RoutedEventArgs e) {
            Close();
        }

        private void MyGrid_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            (DataContext as AccountsettingsViewModel)?.SelectedAccounts?.Clear();
            foreach (var item in DataGrid.SelectedItems) ((AccountsettingsViewModel) DataContext).SelectedAccounts?.Add(item as Accountsetting);
        }
    }
}
