﻿using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using Ds_Bot_Reunited_NEWUI.Accountsettings.Ds_Bot_Reunited.Accountsettings;
using Ds_Bot_Reunited_NEWUI.Annotations;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.Properties;
using Framework.UI.Controls;
using Window = System.Windows.Window;

namespace Ds_Bot_Reunited_NEWUI.AccountsettingsView {
    [Obfuscation(Exclude = true, Feature = "renaming")]
    public class AccountsettingsViewModel : INotifyPropertyChanged {
        private ICollectionView _accounts;
        private ICommand _addCommand;
        private ICommand _copyAccountsFromAccountsTxtCommand;
        private ICommand _copyCommand;
        private ICommand _removeCommand;
        private ICommand _resetCommand;
        private Accountsetting _selectedAccount;
        private Accountsetting _selectedTemplateAccount;

        public AccountsettingsViewModel(Window window) {
            Accounts = CollectionViewSource.GetDefaultView(App.Settings.Accountsettingslist);
            if (App.Settings.Accountsettingslist.Any())
                SelectedAccount = App.Settings.Accountsettingslist.First();
            SelectedAccounts = new AsyncObservableCollection<Accountsetting>();
            Window = window;
            BindingOperations.EnableCollectionSynchronization(SelectedAccounts, SelectedAccountsLock);
        }

        public Window Window { get; set; }

        public object SelectedAccountsLock { get; set; } = new object();

        public ICollectionView Accounts {
            get => _accounts;
            set {
                _accounts = value;
                OnPropertyChanged();
            }
        }

        public AsyncObservableCollection<Accountsetting> SelectedAccounts { get; set; }


        public Accountsetting SelectedAccount {
            get => _selectedAccount;
            set {
                if (value == null)
                    return;
                _selectedAccount = value;
                OnPropertyChanged();
            }
        }

        public Accountsetting SelectedTemplateAccount {
            get => _selectedTemplateAccount;
            set {
                _selectedTemplateAccount = value;
                OnPropertyChanged();
            }
        }

        public ICommand AddCommand => _addCommand ?? (_addCommand = new RelayCommand(Add));
        public ICommand RemoveCommand => _removeCommand ?? (_removeCommand = new RelayCommand(Remove));
        public ICommand ResetCommand => _resetCommand ?? (_resetCommand = new RelayCommand(ResetAllAccountVillages));
        public ICommand CopyCommand => _copyCommand ?? (_copyCommand = new RelayCommand(Copy));

        public ICommand CopyAccountsFromAccountsTxtCommand => _copyAccountsFromAccountsTxtCommand ?? (_copyAccountsFromAccountsTxtCommand = new RelayCommand(CopyAccountsFromTxt));


        public event PropertyChangedEventHandler PropertyChanged;

        private async void CopyAccountsFromTxt() {
            if (!File.Exists("accounts.txt")) {
                await MessageDialog.ShowAsync("", "Keine Accounts.txt Datei vorhanden?", MessageBoxButton.YesNo, MessageDialogType.Accent, Window);
                return;
            }
            if (await MessageDialog.ShowAsync("", "Wirklich Accounts importieren?", MessageBoxButton.YesNo, MessageDialogType.Accent, Window) == MessageBoxResult.No)
                return;
            try {
                var zeilen = File.ReadAllLines("accounts.txt");
                foreach (var zeile in zeilen) {
                    var splitted = zeile.Split(';');
                    var settings = new Accountsetting(splitted[0]) {Passwort = splitted[1], ProxyEins = splitted[2]};
                    App.Settings.Accountsettingslist.Add(settings);
                }
            } catch {
                // ignored
            }
        }


        private void Add() {
            var index = 0;
            if (App.Settings.Accountsettingslist.Any(x => x.Accountname.Contains("New")))
                index += App.Settings.Accountsettingslist.Count(x => x.Accountname.Contains("New"));
            var mainaccount = App.Settings.Accountsettingslist.Count == 0;
            var account = new Accountsetting("New" + index, mainaccount);
            account.ProxyEins = RegistrierungsModul.RegistrierungsModul.GetProxy();
            App.Settings.Accountsettingslist.Add(account);
            Accounts.MoveCurrentToNext();
            OnPropertyChanged("");
        }

        private async void Remove() {
            if (await MessageDialog.ShowAsync("", Resources.ReallyDeleteAccount, MessageBoxButton.YesNo, MessageDialogType.Accent, Window) == MessageBoxResult.No)
                return;

            if (SelectedAccount != null)
                App.Settings.Accountsettingslist.Remove(SelectedAccount);
            if (App.Settings.Accountsettingslist.Any())
                SelectedAccount = App.Settings.Accountsettingslist.FirstOrDefault();
        }

        private async void ResetAllAccountVillages() {
            if (await MessageDialog.ShowAsync("", "Reset all Accounts?", MessageBoxButton.YesNo, MessageDialogType.Accent, Window) == MessageBoxResult.No)
                return;

            if (SelectedAccounts != null)
                foreach (var account in SelectedAccounts)
                    account.Reset();
            OnPropertyChanged("");
        }

        private void Copy() {
            if (SelectedAccounts != null && SelectedTemplateAccount != null)
                foreach (var account in SelectedAccounts)
                    if (!account.Accountname.Equals(SelectedTemplateAccount.Accountname)) {
                        account.Farmeninterval = SelectedTemplateAccount.Farmeninterval;
                        account.Baueninterval = SelectedTemplateAccount.Baueninterval;
                        account.Rekrutiereninterval = SelectedTemplateAccount.Rekrutiereninterval;
                        account.FarmenActive = SelectedTemplateAccount.FarmenActive;
                        account.RekrutierenActive = SelectedTemplateAccount.RekrutierenActive;
                        account.BauenActive = SelectedTemplateAccount.BauenActive;
                        account.PremiumDepotActive = SelectedTemplateAccount.PremiumDepotActive;
                        account.MaxLoginsInEinerStunde = SelectedTemplateAccount.MaxLoginsInEinerStunde;
                        account.Registrierungszeitpunkt = SelectedTemplateAccount.Registrierungszeitpunkt;
                        account.Registrierungsemail = SelectedTemplateAccount.Registrierungsemail;
                        account.Registrierungsemailpasswort = SelectedTemplateAccount.Registrierungsemailpasswort;
                        account.SendingActive = SelectedTemplateAccount.SendingActive;
                        account.HandelnActive = SelectedTemplateAccount.HandelnActive;
                        account.Handelninterval = SelectedTemplateAccount.Handelninterval;
                        account.MassenAngriffeActive = SelectedTemplateAccount.MassenAngriffeActive;
                        account.OverallActive = SelectedTemplateAccount.OverallActive;
                        account.NachrichtenAntwortenActive = SelectedTemplateAccount.NachrichtenAntwortenActive;
                        account.NachrichtenLesenActive = SelectedTemplateAccount.NachrichtenLesenActive;
                        account.NachrichtenLeseninterval = SelectedTemplateAccount.NachrichtenLeseninterval;
                        account.QuestsActive = SelectedTemplateAccount.QuestsActive;
                        account.ReadForumActive = SelectedTemplateAccount.ReadForumActive;
                        account.AccountPrioritaet = SelectedTemplateAccount.AccountPrioritaet;
                        account.AutomaticLogoutActive = SelectedTemplateAccount.AutomaticLogoutActive;
                        account.LastReportReading = SelectedTemplateAccount.LastReportReading;
                        account.NextAvailableBauschleife = SelectedTemplateAccount.NextAvailableBauschleife;
                        account.NextAvailableTroopAtHomeAt = SelectedTemplateAccount.NextAvailableTroopAtHomeAt;
                        account.LastBauenZeitpunkt = SelectedTemplateAccount.LastBauenZeitpunkt;
                        account.LastFarmenZeitpunkt = SelectedTemplateAccount.LastFarmenZeitpunkt;
                        account.LastRekrutierenZeitpunkt = SelectedTemplateAccount.LastRekrutierenZeitpunkt;
                        account.LoginActive = SelectedTemplateAccount.LoginActive;
                        account.LastNachrichtenZeitpunkt = SelectedTemplateAccount.LastNachrichtenZeitpunkt;
                        account.LastHandelZeitpunkt = SelectedTemplateAccount.LastHandelZeitpunkt;
                        account.ReloggingActive = SelectedTemplateAccount.ReloggingActive;
                        account.RelogOnSessionLostAfterMinutes = SelectedTemplateAccount.RelogOnSessionLostAfterMinutes;
                        account.GoldCoinsActive = SelectedTemplateAccount.GoldCoinsActive;
                        account.GoldCoinsinterval = SelectedTemplateAccount.GoldCoinsinterval;
                        account.LastGoldCoinsZeitpunkt = SelectedTemplateAccount.LastGoldCoinsZeitpunkt;
                        account.SetFlaggeActive = SelectedTemplateAccount.SetFlaggeActive;
                        account.SetBoosterActive = SelectedTemplateAccount.SetBoosterActive;
                        account.SetInventoryItemsActive = SelectedTemplateAccount.SetInventoryItemsActive;
                        account.SellMode = SelectedTemplateAccount.SellMode;
                        account.AutoPilot = SelectedTemplateAccount.AutoPilot;
                        account.ForschenActive = SelectedTemplateAccount.ForschenActive;
                        account.Forscheninterval = SelectedTemplateAccount.Forscheninterval;
                    }
            OnPropertyChanged("");
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
