﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using Ds_Bot_Reunited_NEWUI.Annotations;
using Ds_Bot_Reunited_NEWUI.BerichtModul;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.Properties;
using Framework.UI.Controls;
using Window = System.Windows.Window;

namespace Ds_Bot_Reunited_NEWUI.Berichteliste {
    public class BerichtelisteViewModel : INotifyPropertyChanged {
        public Window Window { get; set; }
        private ICollectionView _berichteView;
        private AsyncObservableCollection<Bericht> _berichte;
        private ICommand _removeBerichtCommand;
		
        public AsyncObservableCollection<Bericht> SelectedBerichte { get; set; }
		
        public AsyncObservableCollection<Bericht> Berichte {
            get => _berichte;
            set {
                _berichte = value;
                OnPropertyChanged();
            }
        }

        public ICollectionView BerichteView {
            get => _berichteView;
            set {
                _berichteView = value;
                OnPropertyChanged();
            }
        }

        public int BogenColumnWidth => App.Settings.Weltensettings != null ? (App.Settings.Weltensettings.BogenschuetzenActive ? 30 : 0) : 30;
        public int PaladinColumnWidth => App.Settings.Weltensettings != null ? (App.Settings.Weltensettings.PaladinActive ? 30 : 0) : 30;

        public ICommand RemoveBerichtCommand => _removeBerichtCommand ?? (_removeBerichtCommand = new RelayCommand(DoRemoveBericht));


        public BerichtelisteViewModel(Window window) {
            Window = window;
            Berichte = App.Berichte;
            BerichteView = CollectionViewSource.GetDefaultView(Berichte);
            SelectedBerichte = new AsyncObservableCollection<Bericht>();
        }

        private async void DoRemoveBericht() {
            if (
                await MessageDialog.ShowAsync(Resources.YouReallyWantToDeleteReports, Resources.AreYouSure, MessageBoxButton.YesNo, MessageDialogType.Accent,
                    Window) == MessageBoxResult.No)
                return;
            foreach (var farmtarget in SelectedBerichte) {
                    Berichte.Remove(farmtarget);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}