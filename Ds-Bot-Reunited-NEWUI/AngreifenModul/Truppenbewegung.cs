﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Data;
using Ds_Bot_Reunited_NEWUI.Annotations;
using Ds_Bot_Reunited_NEWUI.Farmen;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.SpieldatenViewModel;

namespace Ds_Bot_Reunited_NEWUI.AngreifenModul {
    [Serializable]
    public class Truppenbewegung : INotifyPropertyChanged {
        private bool _active;
        private bool _sent;
        private bool _queued;
        private bool _getsQueued;
        private int _bewegungId;
        private string _bewegungsart;
        private bool _genauTimenIsActive;
        private bool _rausstellen;
        private DorfViewModel _senderDorf;
        private DorfViewModel _empfaengerDorf;
        private DateTime _abschickzeitpunkt;
        private DateTime _ankunftszeitpunkt;
        private DateTime _actualAnkunftszeitpunkt;
        private DateTime _rueckkehrzeitpunkt;
        private int _speertraegerAnzahl;
        private int _schwertkaempferAnzahl;
        private int _axtkaempferAnzahl;
        private int _bogenschuetzenAnzahl;
        private int _spaeherAnzahl;
        private int _leichteKavallerieAnzahl;
        private int _beritteneBogenschuetzenAnzahl;
        private int _schwereKavallerieAnzahl;
        private int _rammboeckeAnzahl;
        private int _katapulteAnzahl;
        private int _paladinAnzahl;
        private int _adelsgeschlechterAnzahl;
        private string _fehlermeldungen;
        private string _notizen;
        private string _angriffsinformationen;
        private string _kattaZiel;
        private bool _enabled;

        private string _falseImageSource = @"pack://siteoforigin:,,,/TroopImages\X.png";
        private string _trueImageSource = @"pack://siteoforigin:,,,/TroopImages\OK.png";

        public string SentImgSource => (Sent) ? _trueImageSource : _falseImageSource;
        public string ActiveImgSource => (Active) ? _trueImageSource : _falseImageSource;
        public string HasFehlerImgSource=> string.IsNullOrEmpty(Fehlermeldungen) ? _trueImageSource : _falseImageSource;

        public string QueuedImgSource => (Queued) ? _trueImageSource : _falseImageSource;
        public bool Active {
            get => _active;
            set {
                _active = value;
                OnPropertyChanged("");
            }
        }

        public bool Sent {
            get => _sent;
            set {
                _sent = value;
                OnPropertyChanged("");
            }
        }

        public bool Queued {
            get => _queued;
            set {
                _queued = value;
                Enabled = !_queued && !_getsQueued;
                OnPropertyChanged("");
            }
        }

        public bool Enabled {
            get => _enabled;
            set {
                _enabled = value;
                OnPropertyChanged();
            }
        }

        public bool GetsQueued {
            get => _getsQueued;
            set {
                _getsQueued = value;
                Enabled = !_queued && !_getsQueued;
                OnPropertyChanged("");
            }
        }

        public int BewegungId {
            get => _bewegungId;
            set {
                _bewegungId = value;
                OnPropertyChanged();
            }
        }

        public string Bewegungsart {
            get => _bewegungsart;
            set {
                _bewegungsart = value;
                OnPropertyChanged();
            }
        }

        public bool GenauTimenIsActive {
            get => _genauTimenIsActive;
            set {
                _genauTimenIsActive = value;
                OnPropertyChanged();
            }
        }

        public bool Rausstellen {
            get => _rausstellen;
            set {
                _rausstellen = value;
                OnPropertyChanged();
            }
        }

        public DorfViewModel SenderDorf {
            get => _senderDorf;
            set {
                _senderDorf = value;
                OnPropertyChanged();
            }
        }

        public DorfViewModel EmpfaengerDorf {
            get => _empfaengerDorf;
            set {
                _empfaengerDorf = value;
                OnPropertyChanged();
            }
        }

        public DateTime Abschickzeitpunkt {
            get => _abschickzeitpunkt;
            set {
                _abschickzeitpunkt = value;
                OnPropertyChanged();
            }
        }

        public DateTime Ankunftszeitpunkt {
            get => _ankunftszeitpunkt;
            set {
                _ankunftszeitpunkt = value;
                OnPropertyChanged();
            }
        }

        public DateTime ActualAnkunftszeitpunkt {
            get => _actualAnkunftszeitpunkt;
            set {
                _actualAnkunftszeitpunkt = value;
                OnPropertyChanged();
            }
        }

        public DateTime Rueckkehrzeitpunkt {
            get => _rueckkehrzeitpunkt;
            set {
                _rueckkehrzeitpunkt = value;
                OnPropertyChanged();
            }
        }

        public int SpeertraegerAnzahl {
            get => _speertraegerAnzahl;
            set {
                _speertraegerAnzahl = value;
                OnPropertyChanged();
            }
        }

        public int SchwertkaempferAnzahl {
            get => _schwertkaempferAnzahl;
            set {
                _schwertkaempferAnzahl = value;
                OnPropertyChanged();
            }
        }

        public int AxtkaempferAnzahl {
            get => _axtkaempferAnzahl;
            set {
                _axtkaempferAnzahl = value;
                OnPropertyChanged();
            }
        }

        public int BogenschuetzenAnzahl {
            get => _bogenschuetzenAnzahl;
            set {
                _bogenschuetzenAnzahl = value;
                OnPropertyChanged();
            }
        }

        public int SpaeherAnzahl {
            get => _spaeherAnzahl;
            set {
                _spaeherAnzahl = value;
                OnPropertyChanged();
            }
        }

        public int LeichteKavallerieAnzahl {
            get => _leichteKavallerieAnzahl;
            set {
                _leichteKavallerieAnzahl = value;
                OnPropertyChanged();
            }
        }

        public int BeritteneBogenschuetzenAnzahl {
            get => _beritteneBogenschuetzenAnzahl;
            set {
                _beritteneBogenschuetzenAnzahl = value;
                OnPropertyChanged();
            }
        }

        public int SchwereKavallerieAnzahl {
            get => _schwereKavallerieAnzahl;
            set {
                _schwereKavallerieAnzahl = value;
                OnPropertyChanged();
            }
        }

        public int RammboeckeAnzahl {
            get => _rammboeckeAnzahl;
            set {
                _rammboeckeAnzahl = value;
                OnPropertyChanged();
            }
        }

        public int KatapulteAnzahl {
            get => _katapulteAnzahl;
            set {
                _katapulteAnzahl = value;
                OnPropertyChanged();
            }
        }

        public int PaladinAnzahl {
            get => _paladinAnzahl;
            set {
                _paladinAnzahl = value;
                OnPropertyChanged();
            }
        }

        public int AdelsgeschlechterAnzahl {
            get => _adelsgeschlechterAnzahl;
            set {
                _adelsgeschlechterAnzahl = value;
                OnPropertyChanged();
            }
        }
        public object AnkunftszeitAuswirkungenAufListLock { get; set; } = new object();
        public object BegleittruppenLock { get; set; } = new object();

        public AsyncObservableCollection<int> AnkunftszeitAuswirkungenAufList { get; set; }
        public AsyncObservableCollection<TruppenTemplate> Begleittruppen { get; set; }

        public string Fehlermeldungen {
            get => _fehlermeldungen;
            set {
                _fehlermeldungen = value;
                OnPropertyChanged();
            }
        }

        public string Notizen {
            get => _notizen;
            set {
                _notizen = value;
                OnPropertyChanged();
            }
        }

        public string Angriffsinformationen {
            get => _angriffsinformationen;
            set {
                _angriffsinformationen = value;
                OnPropertyChanged();
            }
        }

        public string KattaZiel {
            get => _kattaZiel;
            set {
                _kattaZiel = value;
                OnPropertyChanged();
            }
        }

        public int ProzentAbziehenVonLaufzeit { get; set; }

        // ReSharper disable once EmptyConstructor
        public Truppenbewegung() {
            AnkunftszeitAuswirkungenAufList = new AsyncObservableCollection<int>();
            Begleittruppen = new AsyncObservableCollection<TruppenTemplate>();

            BindingOperations.EnableCollectionSynchronization(AnkunftszeitAuswirkungenAufList, AnkunftszeitAuswirkungenAufListLock);
            BindingOperations.EnableCollectionSynchronization(Begleittruppen, BegleittruppenLock);
            Enabled = true;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public void Refresh() {
            OnPropertyChanged("");
        }
    }
}