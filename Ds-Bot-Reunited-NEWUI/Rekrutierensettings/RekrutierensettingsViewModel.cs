﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Input;
using Ds_Bot_Reunited_NEWUI.Annotations;
using Ds_Bot_Reunited_NEWUI.Constants;
using Ds_Bot_Reunited_NEWUI.Hilfsklassen;
using Ds_Bot_Reunited_NEWUI.Properties;
using Ds_Bot_Reunited_NEWUI.Spieldaten;

namespace Ds_Bot_Reunited_NEWUI.Rekrutierensettings {
    public class RekrutierensettingsViewModel : INotifyPropertyChanged {
        public System.Windows.Window Window { get; set; }
        private ICommand _copyCommand;
        private RekrutierenSettingsDorfViewModel _selectedTemplateVillage;
        private RekrutierenSettingsDorfViewModel _selectedVillage;
        private ICollectionView _villages;
        private GruppeDoerfer _selectedGroup;
        public AsyncObservableCollection<RekrutierenSettingsDorfViewModel> SelectedVillages { get; set; }

        private AsyncObservableCollection<RekrutierenSettingsDorfViewModel> Rekrutierensettings { get; }
        public List<GruppeDoerfer> Groups => App.Settings.Gruppen;
        public object RekrutierensettingsLock { get; set; } = new object();

        public ICollectionView Villages {
            get => _villages;
            set {
                _villages = value;
                OnPropertyChanged();
            }
        }

        public RekrutierenSettingsDorfViewModel SelectedVillage {
            get => _selectedVillage;
            set {
                if (value == null)
                    return;
                _selectedVillage = value;
                OnPropertyChanged();
            }
        }

        public GruppeDoerfer SelectedGroup {
            get => _selectedGroup;
            set {
                _selectedGroup = value;
                Task.Run(async () => { await LoadPlayerVillages(); });
                OnPropertyChanged("");
            }
        }

        public RekrutierenSettingsDorfViewModel SelectedTemplateVillage {
            get => _selectedTemplateVillage;
            set {
                _selectedTemplateVillage = value;
                OnPropertyChanged();
            }
        }

        private async Task LoadPlayerVillages() {
            await Task.Run(() => {
                               try {
                                   Rekrutierensettings.Clear();
                                   foreach (var liste in App.Settings.Accountsettingslist.Select(x => x.DoerferSettings.Where(y => y.Dorf.PlayerId.Equals(x.Playerid) && (SelectedGroup?.DoerferIds.Any(k => k.Equals(y.Dorf.VillageId)) ?? true)))) {
                                       foreach (var dorf in liste.OrderBy(x => x.Dorf.Name)) {
                                           Rekrutierensettings.Add(new RekrutierenSettingsDorfViewModel(dorf, Window));
                                       }
                                   }
                               } catch {
                                       App.LogString(Resources.FailureLoadingPlayervillages);
                               }
                               if (Rekrutierensettings.Any())
                                   SelectedVillage = Rekrutierensettings.FirstOrDefault();
                               OnPropertyChanged("");
                           });
        }

        public ICommand CopyCommand => _copyCommand ?? (_copyCommand = new RelayCommand(Copy));

        public RekrutierensettingsViewModel(System.Windows.Window window) {
            Rekrutierensettings = new AsyncObservableCollection<RekrutierenSettingsDorfViewModel>();
            Villages = CollectionViewSource.GetDefaultView(Rekrutierensettings);
            BindingOperations.EnableCollectionSynchronization(Rekrutierensettings, RekrutierensettingsLock);
            SelectedVillages = new AsyncObservableCollection<RekrutierenSettingsDorfViewModel>();
            SelectedGroup = Groups.FirstOrDefault(x => x.Gruppenname.Equals(NAMECONSTANTS.GetNameForServer(Names.Alle)));
        }

        private void Copy() {
            if (SelectedVillages != null && SelectedTemplateVillage != null) {
                foreach (var village in SelectedVillages) {
                    village.Rekrutieren = SelectedTemplateVillage.Rekrutieren.Clone();
                    village.RekrutierenEisenUebrigLassen = SelectedTemplateVillage.RekrutierenEisenUebrigLassen;
                    village.RekrutierenHolzUebrigLassen = SelectedTemplateVillage.RekrutierenHolzUebrigLassen;
                    village.RekrutierenLehmUebrigLassen = SelectedTemplateVillage.RekrutierenLehmUebrigLassen;
                    village.RekrutierenActive = SelectedTemplateVillage.RekrutierenActive;
                    village.KasernePercent = SelectedTemplateVillage.KasernePercent;
                    village.StallPercent = SelectedTemplateVillage.StallPercent;
                    village.WerkstattPercent = SelectedTemplateVillage.WerkstattPercent;
                }
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
