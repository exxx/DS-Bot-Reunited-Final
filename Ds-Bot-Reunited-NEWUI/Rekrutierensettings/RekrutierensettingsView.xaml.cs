﻿using System.Windows;
using System.Windows.Controls;

namespace Ds_Bot_Reunited_NEWUI.Rekrutierensettings {
    public partial class RekrutierensettingsView {
        public RekrutierensettingsView(Window owner, RekrutierensettingsViewModel datacontext) {
            Owner = owner;
            DataContext = datacontext;
            InitializeComponent();
        }

        private void Save(object sender, RoutedEventArgs e) {
            Close();
        }

        void MyGrid_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            if (((RekrutierensettingsViewModel) DataContext)?.SelectedVillages != null) {
                ((RekrutierensettingsViewModel) DataContext).SelectedVillages.Clear();
                foreach (var item in DataGrid.SelectedItems) {
                    ((RekrutierensettingsViewModel) DataContext).SelectedVillages.Add((RekrutierenSettingsDorfViewModel) item);
                }
            }
        }
    }
}