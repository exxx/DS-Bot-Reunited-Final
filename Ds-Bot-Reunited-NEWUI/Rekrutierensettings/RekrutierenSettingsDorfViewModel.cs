﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using Ds_Bot_Reunited_NEWUI.Annotations;
using Ds_Bot_Reunited_NEWUI.Farmen;
using Ds_Bot_Reunited_NEWUI.Settings;

namespace Ds_Bot_Reunited_NEWUI.Rekrutierensettings {
    public class RekrutierenSettingsDorfViewModel : INotifyPropertyChanged {
        public Window Window { get; set; }
        private readonly AccountDorfSettings _accountDorfSettings;
        public string Villagename => _accountDorfSettings.Dorf.Name;
        public bool IsEnabled => App.Permissions.RecruitingIsEnabled;

        public int MuenzenPraegen {
            get => _accountDorfSettings.MuenzenPraegen;
            set {
                _accountDorfSettings.MuenzenPraegen = value;
                OnPropertyChanged();
            }
        }
        public bool ForschenActive {
            get => _accountDorfSettings.ForschenActive;
            set {
                _accountDorfSettings.ForschenActive = value;
                OnPropertyChanged();
            }
        }
        public bool BalanceForschenActive {
            get => _accountDorfSettings.BalanceForschenActive;
            set {
                _accountDorfSettings.BalanceForschenActive = value;
                OnPropertyChanged();
            }
        }
        public int Forscheninterval {
            get => _accountDorfSettings.Forscheninterval;
            set {
                _accountDorfSettings.Forscheninterval = value;
                OnPropertyChanged();
            }
        }
        public double KasernePercent {
            get => _accountDorfSettings.KasernePercent;
            set {
                _accountDorfSettings.KasernePercent = value;
                var gesamt = _accountDorfSettings.KasernePercent + _accountDorfSettings.StallPercent + _accountDorfSettings.WerkstattPercent;
                double unterschied = gesamt - 100;
                double geteilt = (double)unterschied / 2;
                if (gesamt > 100) {
                    _accountDorfSettings.StallPercent -= geteilt;
                    _accountDorfSettings.WerkstattPercent -= geteilt;
                } else if(gesamt < 100) {
                    _accountDorfSettings.StallPercent -= geteilt;
                    _accountDorfSettings.WerkstattPercent -= geteilt;
                }
                OnPropertyChanged("");
            }
        }

        public double StallPercent {
            get => _accountDorfSettings.StallPercent;
            set {
                _accountDorfSettings.StallPercent = value;
                var gesamt = _accountDorfSettings.KasernePercent + _accountDorfSettings.StallPercent + _accountDorfSettings.WerkstattPercent;
                double unterschied = gesamt - 100;
                double geteilt = (double)unterschied / 2;
                if (gesamt > 100) {
                    _accountDorfSettings.KasernePercent -= geteilt;
                    _accountDorfSettings.WerkstattPercent -= geteilt;
                } else if (gesamt < 100) {
                    _accountDorfSettings.KasernePercent -= geteilt;
                    _accountDorfSettings.WerkstattPercent -= geteilt;
                }
                OnPropertyChanged("");
            }
        }
        public double WerkstattPercent {
            get => _accountDorfSettings.WerkstattPercent;
            set {
                _accountDorfSettings.WerkstattPercent = value;
                var gesamt = _accountDorfSettings.KasernePercent + _accountDorfSettings.StallPercent + _accountDorfSettings.WerkstattPercent;
                double unterschied = gesamt - 100;
                double geteilt = (double)unterschied / 2;
                if (gesamt > 100) {
                    _accountDorfSettings.StallPercent -= geteilt;
                    _accountDorfSettings.KasernePercent -= geteilt;
                } else if (gesamt < 100) {
                    _accountDorfSettings.StallPercent -= geteilt;
                    _accountDorfSettings.KasernePercent -= geteilt;
                }
                OnPropertyChanged("");
            }
        }

        public int KaserneMaxHours {
            get => _accountDorfSettings.KaserneMaxHours;
            set {
                _accountDorfSettings.KaserneMaxHours = value;
                OnPropertyChanged();
            }
        }
        public int StallMaxHours {
            get => _accountDorfSettings.StallMaxHours;
            set {
                _accountDorfSettings.StallMaxHours = value;
                OnPropertyChanged();
            }
        }
        public int WerkstattMaxHours {
            get => _accountDorfSettings.WerkstattMaxHours;
            set {
                _accountDorfSettings.WerkstattMaxHours = value;
                OnPropertyChanged();
            }
        }

        public bool RekrutierenActive {
            get => _accountDorfSettings.RekrutierenActive;
            set {
                _accountDorfSettings.RekrutierenActive = value;
                OnPropertyChanged();
            }
        }

        public int Rekrutiereninterval {
            get => _accountDorfSettings.Rekrutiereninterval;
            set {
                _accountDorfSettings.Rekrutiereninterval = value;
                OnPropertyChanged();
            }
        }

        public TruppenTemplate Rekrutieren {
            get => _accountDorfSettings.Rekrutieren;
            set {
                _accountDorfSettings.Rekrutieren = value;
                OnPropertyChanged();
            }
        }

        public int RekrutierenHolzUebrigLassen {
            get => _accountDorfSettings.RekrutierenHolzUebrigLassen;
            set {
                _accountDorfSettings.RekrutierenHolzUebrigLassen = value;
                OnPropertyChanged();
            }
        }

        public int RekrutierenLehmUebrigLassen {
            get => _accountDorfSettings.RekrutierenLehmUebrigLassen;
            set {
                _accountDorfSettings.RekrutierenLehmUebrigLassen = value;
                OnPropertyChanged();
            }
        }

        public int RekrutierenEisenUebrigLassen {
            get => _accountDorfSettings.RekrutierenEisenUebrigLassen;
            set {
                _accountDorfSettings.RekrutierenEisenUebrigLassen = value;
                OnPropertyChanged();
            }
        }

        public bool PaladinOverallActive => App.Settings.Weltensettings.PaladinActive;
        public bool BogenschuetzenOverallActive => App.Settings.Weltensettings.BogenschuetzenActive;

        public RekrutierenSettingsDorfViewModel(AccountDorfSettings accountDorfSettings, Window window) {
            Window = window;
            _accountDorfSettings = accountDorfSettings;
        }


        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}